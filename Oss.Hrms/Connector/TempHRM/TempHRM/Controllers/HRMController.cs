﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TempHRM.Models;

namespace TempHRM.Controllers
{
    public class HRMController : Controller
    {
        // GET: HRM
        public ActionResult Index()
        {
            return View();
        }

        // GET: HRM/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HRM/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HRM/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HRM/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HRM/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HRM/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HRM/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



        //
        public JsonResult GetData(int id = 0)
        {
            HRMContext db = new HRMContext();
            var reTry = db.checkInOut.Where(x => x.ID > id).Count() > 10000 ? true : false;
            
            var a = (from t1 in db.checkInOut
                     where t1.ID > id
                     select new
                     {
                         ID=t1.ID,
                         UID = t1.UserId,
                         Checktime = t1.CheckTime,
                         Checktype = t1.checktype,
                         VeryfyCode = t1.VeryfyCode,
                         CardNo = t1.CardNo,
                         Retry = reTry

                     }).Take(10000).ToList();

            return Json(a, JsonRequestBehavior.AllowGet);

        }

    }
}
