﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace TempHRM.Models
{
    public class HRMContext  : DbContext
    {
        public HRMContext() : base("name=HRM")
        {
        }
        public DbSet<checkInOut> checkInOut { get; set; }
        public DbSet<RMS> RMS { get; set; }
    }
}
