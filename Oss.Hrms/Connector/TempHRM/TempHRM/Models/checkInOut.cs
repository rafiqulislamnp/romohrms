﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TempHRM.Models
{
    public class checkInOut
    {


        public int ID { get; set; }
        [StringLength(50)]
        public string UserId { get; set; }
        public DateTime CheckTime { get; set; }
        [StringLength(50)]
        public string checktype { get; set; }
        [StringLength(50)]
        public string VeryfyCode { get; set; }
        [StringLength(50)]
        public string CardNo { get; set; }
        

    }

    public class RMS
    {
        public int ID { get; set; }
        [StringLength(50)]
        public string d_card { get; set; }
        [StringLength(50)]
        public string t_card { get; set; }
        [StringLength(50)]
        public string card_no { get; set; }
    }
}