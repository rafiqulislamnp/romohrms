﻿using Oss.Hrms.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Services
{
    public class SessionHandler
    {
        public CurrentUser CurrentUser { get; set; }
        public SessionHandler()
        {

        }
        public void Adjust()
        {
            if (System.Web.HttpContext.Current.Session["User"] == null)
            {               
                var xxx = System.Web.HttpContext.Current.Request.Cookies["UserLogin"];
                if (System.Web.HttpContext.Current.Request.Cookies["UserLogin"] != null && System.Web.HttpContext.Current.Request.Cookies["UserLogin"].Value != null)
                {
                    var s = System.Web.HttpContext.Current.Request.Cookies["UserLogin"];
                    VM_User vM_User = new VM_User();
                    vM_User.UserName = s.Values["Name"];
                    vM_User.Password = s.Values["Password"];
                    //cu.UserName = s.Values["Name"];
                    //cu.Password = s.Values["Password"];
                    bool hasCokkie = false;
                    int theId = 0;
                    bool Athourised = false;
                    theId = vM_User.LoginUser1(Athourised);
                    if (theId > 0)
                    { 
                        CurrentUser cu = new CurrentUser { ID = theId, UserName = vM_User.UserName };
                        System.Web.HttpContext.Current.Session["User"] = cu;
                        HttpCookie cookie = new HttpCookie("UserLogin");
                        cookie.Values.Add("Name", cu.UserName);
                        cookie.Values.Add("Password", cu.Password);
                        cookie.Expires.AddMinutes(300);
                        this.CurrentUser = cu;
                    }
                }

            }
            else
            {
                this.CurrentUser = (CurrentUser)System.Web.HttpContext.Current.Session["User"];
            }
            
        }

    }
}