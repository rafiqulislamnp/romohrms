﻿using Oss.Hrms.Models.ViewModels.HRMS;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using System.Web.UI.WebControls;

namespace Oss.Hrms.Models.Services
{
    public class DropDownData
    {

        private HrmsContext db = new HrmsContext();
     
        public string Text { get; set; }

        public string Value { get; set; }

        public DropDownData()
        {

        }

        public DropDownData(string txt, string val)
        {
            Text = txt;
            Value = val;
        }



        public IEnumerable<DropDownData> GetCountrylistAll()
        {
            List<string> cultureList = new List<string>();
            CultureInfo[] getCultureInfo = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo getculture in getCultureInfo)
            {
                RegionInfo getRegionInfo = new RegionInfo(getculture.LCID);

                if (!(cultureList.Contains(getRegionInfo.EnglishName)))
                {
                    cultureList.Add(getRegionInfo.EnglishName);
                }
            }
            cultureList.Sort();
            var list = new List<DropDownData> { }; // { new DropDownData("Select Country Name", "") }};
            if (cultureList.Count > 0)
            {
                for (int i = 0; i < cultureList.Count; i++)
                {
                    list.Add(new DropDownData(cultureList[i], cultureList[i]));
                }
            }
            return list;
        }

        public List<object> GetCountryList()
        {
            var CountryList = new List<object>();

            foreach (var Countries in db.Countries.Where(d => d.Status == true))
            {
                CountryList.Add(new { Text = Countries.Name, Value = Countries.ID });
            }
            return CountryList;
        }

        public List<object> GetBusinessUnitList()
        {
            var BusinessUnitList = new List<object>();

            foreach (var businessUnit in db.BusinessUnits.Where(d => d.Status == true))
            {
                BusinessUnitList.Add(new { Text = businessUnit.Name, Value = businessUnit.ID });
            }
            return BusinessUnitList;
        }

        public List<object> GetUnitList()
        {
            var List = new List<object>();

            foreach (var list in db.Units.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.UnitName, Value = list.ID });
            }
            return List;
        }

        public List<object> GetDesignationList()
        {
            var List = new List<object>();

            foreach (var list in db.Designations.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.Name, Value = list.ID });
            }
            return List;
        }
        // 

        public List<object> GetDepartmentListforEmployeeSearch()
        {

            //List.Add(new
            //{
            //    Text = string.Format("{0}" + "-:-" + "{1}", list.EmployeeIdentity, list.Name),
            //    Value = list.ID
            //});

            var List = new List<object>();
            List.Add(new { Text = "All", Value = 0 });
            foreach (var list in db.Departments.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.DeptName, Value = list.ID });
            }
            return List;
        }

        public List<object> GetDepartmentList()
        {
            var List = new List<object>();

            foreach (var list in db.Departments.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.DeptName, Value = list.ID });
            }
            return List;
        }

        public List<object> GetSectionList()
        {
            var List = new List<object>();

            foreach (var list in db.Sections.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.SectionName, Value = list.ID });
            }
            return List;
        }


        /// <summary>
        /// Line Number dropdown list-----
        /// </summary>
        /// <returns></returns>
        public List<object> GetActiveCardTitleList()
        {
            var List = new List<object>();
            foreach (var list in db.HrmsCardTitle.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.CardTitle, Value = list.CardTitle });
            }
            return List;
        }

        /// <summary>
        /// Line Number dropdown list-----
        /// </summary>
        /// <returns></returns>
        public List<object> GetLineList()
        {
            var List = new List<object>();

            foreach (var list in db.Lines.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.Line, Value = list.ID });
            }
            return List;
        }
        public List<object> GetActiveEmployeeList()
        {
            var List = new List<object>();

            foreach (var list in db.Employees.Where(d => d.Present_Status == 1))
            {
                List.Add(new { Text = list.EmployeeIdentity, Value = list.Name });
            }
            return List;
        }
        public List<object> GetHrmsShiftTypesDropDown()
        {
            var List = new List<object>();
            List.Add(new { Text = "All", Value = 0 });

            foreach (var list in db.HrmsShiftTypes.Where(x => x.Status == true))
            {
                List.Add(new { Text = list.ShiftName, Value = list.ID });
            }
            return List;
        }
        public List<object> GetSalaryCreateEmployeeList()
        {
            var List = new List<object>();

            var ActiveEmployee = (from o in db.Employees
                                  where o.Present_Status == 1
                                  select new
                                  {
                                      Id = o.ID,
                                      Title = o.CardTitle,
                                      Card = o.EmployeeIdentity,
                                      Name = o.Name
                                  }).ToList();
            var salaryEmployee = (from o in db.HrmsEodRecords
                                  join p in db.Employees on o.EmployeeId equals p.ID
                                  select new
                                  {
                                      Id = p.ID,
                                      Title = p.CardTitle,
                                      Card = p.EmployeeIdentity,
                                      Name = p.Name
                                  }).ToList();

            var notSalary = ActiveEmployee.Except(salaryEmployee).ToList();

            foreach (var list in notSalary)
            {
                List.Add(new { Text = list.Title + " " + list.Card + "-" + list.Name, Value = list.Id });
            }
            return List;
        }

        public List<object> GetActiveEmployeeListforRecord()
        {
            var List = new List<object>();
            foreach (var list in db.Employees.Where(d => d.Present_Status == 1))
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + " -:- " + "{1}" + " -:- " + "{2}", list.CardTitle, list.EmployeeIdentity, list.Name),
                    Value = list.ID
                });
            }
            return List;
        }

        public List<object> GetAuditSalaryCreateEmployeeList()
        {
            var List = new List<object>();

            var ActiveEmployee = (from o in db.Employees
                                  where o.Present_Status == 1
                                  select new
                                  {
                                      Id = o.ID,
                                      Title = o.CardTitle,
                                      Card = o.EmployeeIdentity,
                                      Name = o.Name
                                  }).ToList();

            var salaryEmployee = (from o in db.HRMHistoryEOD
                                  join p in db.Employees on o.EmployeeId equals p.ID
                                  select new
                                  {
                                      Id = p.ID,
                                      Title = p.CardTitle,
                                      Card = p.EmployeeIdentity,
                                      Name = p.Name
                                  }).ToList();

            var notSalary = ActiveEmployee.Except(salaryEmployee).ToList();

            foreach (var list in notSalary)
            {
                List.Add(new { Text = list.Title + " " + list.Card + "-" + list.Name, Value = list.Id });
            }
            return List;
        }

        //public List<object> GetActiveJobTitleList()
        //{
        //    var List = new List<object>();
        //    foreach (var list in db.HrmsJobCirculars.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.Job_Title, Value = string.Format("{0}__{1}", list.ID, list.Job_Identification) });
        //    }
        //    return List;
        //}

        public List<object> GetActiveEmployeeListforLink()
        {
            var List = new List<object>();
            foreach (var list in db.Employees.Where(d => d.Present_Status == 1))
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}", list.EmployeeIdentity, list.Name),
                    Value = list.ID
                });
            }
            return List;
        }


        //var salaryEmployee = (from o in db.HrmsEodRecords
        //                      join p in db.Employees on o.EmployeeId equals p.ID
        //                      join q in db.Designations on p.Designation_Id equals q.ID
        //                      where o.Eod_RefFk == 46 || o.Eod_RefFk == 47
        //                      select new
        //                      {
        //                          Id = q.ID,
        //                          Name = q.Name
        //                      }).ToList();

        public List<object> GetActiveEmployeeListforOffDayAssign()
        {
            var List = new List<object>();

            var ActiveEmployee = (from o in db.Employees
                                  where o.Present_Status == 1
                                  select new
                                  {
                                      Id = o.ID,
                                      CardTitle = o.CardTitle,
                                      EmployeeIdentity = o.EmployeeIdentity,
                                      Name = o.Name
                                  }).ToList();

            var datalist = (from t1 in db.Employees 
                                  join t2 in db.HrmsEmployeeOffDays on t1.ID equals t2.EmployeeId
                                  where t1.Present_Status==1
                                  select new
                                  {
                                      Id = t1.ID,
                                      CardTitle = t1.CardTitle,
                                      EmployeeIdentity = t1.EmployeeIdentity,
                                      Name = t1.Name
                                  }).ToList();

            var notfoundEmployeeList = ActiveEmployee.Except(datalist).OrderBy(o=> o.Name).ToList();
            foreach (var list in notfoundEmployeeList)
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}"+"-:-"+ "{2}", list.EmployeeIdentity, list.CardTitle, list.Name),
                    Value = list.Id
                });
            }
            return List;
            
        }


        public List<object> GetEmployeeListforLeaveApplication()
        {
            var List = new List<object>();
            foreach (var list in db.Employees.Where(d => d.Present_Status == 1))
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}" + "-:-" + "{2}", list.CardTitle, list.EmployeeIdentity, list.Name),
                    Value = list.ID
                });
            }
            return List;
        }


        public List<object> GetEResignEmployeeListforEarnLeaveReport()
        {
            var List = new List<object>();
            foreach (var list in db.Employees.Where(d => d.Present_Status != 1))
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}" + "-:-" + "{2}", list.CardTitle, list.EmployeeIdentity, list.Name),
                    Value = list.ID
                });
            }
            return List;
        }


        public List<object> GetAllEmployeeList()
        {
            var List = new List<object>();
            List.Add(new { Text = "All", Value = 10000000 });
            foreach (var list in db.Employees)
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}" + "-:-" + "{2}", list.CardTitle, list.EmployeeIdentity, list.Name),
                    Value = list.ID
                });
            }
            return List;
        }

        public List<object> GetAllEmployeeListForEarnLeave()
        {
            var List = new List<object>();
            foreach (var list in db.Employees )
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}" + "-:-" + "{2}", list.CardTitle, list.EmployeeIdentity, list.Name),
                    Value = list.ID
                });
            }
            return List;
        }


        public List<object> GetAllEmployeeListForServiceBenefit(bool token)
        {

            var List = new List<object>();

            var ActiveEmployee = (from o in db.Employees
                where o.Present_Status != 1
                select new
                {
                    Id = o.ID,
                    CardTitle = o.CardTitle,
                    EmployeeIdentity = o.EmployeeIdentity,
                    Name = o.Name
                }).ToList();
            if (token == false)
            {
                var datalist = (from t1 in db.Employees
                    join t2 in db.HRMS_Service_Benefit on t1.ID equals t2.EmployeeId
                    where t1.Present_Status != 1 && t2.IsAudit == false
                    select new
                    {
                        Id = t1.ID,
                        CardTitle = t1.CardTitle,
                        EmployeeIdentity = t1.EmployeeIdentity,
                        Name = t1.Name
                    }).ToList();

                var notfoundEmployeeList = ActiveEmployee.Except(datalist).OrderBy(o => o.Name).ToList();
                foreach (var list in notfoundEmployeeList)
                {
                    List.Add(new
                    {
                        Text = string.Format("{0}" + "-:-" + "{1}" + "-:-" + "{2}", list.EmployeeIdentity, list.CardTitle, list.Name),
                        Value = list.Id
                    });
                }
            }
            else
            {
                var datalist = (from t1 in db.Employees
                    join t2 in db.HRMS_Service_Benefit on t1.ID equals t2.EmployeeId
                    where t1.Present_Status != 1 && t2.IsAudit == true
                    select new
                    {
                        Id = t1.ID,
                        CardTitle = t1.CardTitle,
                        EmployeeIdentity = t1.EmployeeIdentity,
                        Name = t1.Name
                    }).ToList();

                var notfoundEmployeeList = ActiveEmployee.Except(datalist).OrderBy(o => o.Name).ToList();
                foreach (var list in notfoundEmployeeList)
                {
                    List.Add(new
                    {
                        Text = string.Format("{0}" + "-:-" + "{1}" + "-:-" + "{2}", list.EmployeeIdentity, list.CardTitle, list.Name),
                        Value = list.Id
                    });
                }
            }
            return List;
        }

        public List<object> GetAllEmployeeListForServiceBenefitAudit()
        {

            var List = new List<object>();

            var ActiveEmployee = (from o in db.Employees
                where o.Present_Status != 1
                select new
                {
                    Id = o.ID,
                    CardTitle = o.CardTitle,
                    EmployeeIdentity = o.EmployeeIdentity,
                    Name = o.Name
                }).ToList();

            var datalist = (from t1 in db.Employees
                join t2 in db.HRMS_Service_Benefit on t1.ID equals t2.EmployeeId
                where t1.Present_Status != 1
                select new
                {
                    Id = t1.ID,
                    CardTitle = t1.CardTitle,
                    EmployeeIdentity = t1.EmployeeIdentity,
                    Name = t1.Name
                }).ToList();

            var notfoundEmployeeList = ActiveEmployee.Except(datalist).OrderBy(o => o.Name).ToList();
            foreach (var list in notfoundEmployeeList)
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}" + "-:-" + "{2}", list.EmployeeIdentity, list.CardTitle, list.Name),
                    Value = list.Id
                });
            }
            return List;
        }



        public List<object> GetEmployeeListforLeaveApplicationwithAll()
        {
            var List = new List<object>();
            List.Add(new { Text = "All", Value = 10000000 });
            foreach (var list in db.Employees.Where(d => d.Present_Status == 1))
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}" + "-:-" + "{2}", list.CardTitle, list.EmployeeIdentity, list.Name),
                    Value = list.ID
                });
            }
            return List;
        }


        public List<object> GetActiveEmployeeListwithAll()
        {
            var List = new List<object>();
            List.Add(new { Text = "All", Value = 0 });
            foreach (var list in db.Employees.Where(d => d.Present_Status == 1))
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}", list.EmployeeIdentity, list.Name),
                    Value = list.ID
                });
            }
            return List;
        }

        /// <summary>
        /// Form Controller 
        /// </summary>
        /// <returns></returns>
        public List<object> GetActiveEmployeeListforForm()
        {
            var List = new List<object>();
            foreach (var list in db.Employees.Where(d => d.Present_Status == 1))
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + "-:-" + "{1}" + "-:-" + "{2}", list.EmployeeIdentity, list.Name, list.Name_BN),
                    Value = list.ID
                });
            }
            return List;
        }

        public List<object> GetActiveEmployee()
        {
            var List = new List<object>();
            foreach (var list in db.Employees.Where(d => d.Present_Status == 1))
            {
                List.Add(new
                {
                    Text = string.Format("{0}" + " - " + "{1}" + " - " + "{2}", list.CardTitle, list.EmployeeIdentity, list.Name),
                    Value = list.ID
                });
            }
            return List;
        }

        /// <summary>
        /// Used all in employee Criteria....
        /// </summary>
        /// <returns></returns>
        public List<object> GetEmployeeList()
        {
            var List = new List<object>();

            foreach (var list in db.Employees.Where(d => d.Present_Status == 1))
            {
                List.Add(new { Text = list.EmployeeIdentity, Value = list.ID });
            }
            return List;
        }


        public List<object> GetShiftTypeName()
        {
            var List = new List<object>();

            foreach (var list in db.HrmsShiftTypes.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.ShiftName, Value = list.ID });
            }
            return List;
        }

        /// <summary>
        /// Load Shift type for dropdown used in Shift wise Attendance Search
        /// </summary>
        /// <returns></returns>
        public List<object> GetShiftTypeNamewithAll()
        {
            var List = new List<object>();
            List.Add(new { Text = "All", Value = 100 });
            foreach (var list in db.HrmsShiftTypes.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.ShiftName, Value = list.ID });
            }
            return List;
        }


        /// <summary>
        /// Load leave type for dropdown used in leave application form
        /// </summary>
        /// <returns></returns>
        public List<object> GetLeaveTypeName()
        {
            var List = new List<object>();

            foreach (var list in db.HrmsLeaveTypes.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.Leave_Name, Value = list.ID });
            }
            return List;
        }

        /// <summary>
        /// Load leave type for dropdown used in leave application form
        /// </summary>
        /// <returns></returns>
        public List<object> GetLeaveTypeNameWithAll()
        {
            var List = new List<object>();
            List.Add(new { Text = "All", Value = 1000 });
            foreach (var list in db.HrmsLeaveTypes.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.Leave_Name, Value = list.ID });
            }
            return List;
        }

        /// <summary>
        /// Load leave type for dropdown used in leave application form
        /// </summary>
        /// <returns></returns>
        public List<object> GetLeaveTypeAssignYearList()
        {
            var List = new List<object>();
            foreach (var list in db.HrmsLeaveAssigns.DistinctBy(x => x.Year).ToList())
            {
                List.Add(new { Text = list.Year, Value = list.Year });
            }
            return List;
        }


        /// <summary>
        /// Load Leave Type for Dropdown used in Employee Leave Assign 
        /// </summary>
        /// <returns></returns>
        public List<object> GetLeaveTypeNameWithAmount()
        {
            var List = new List<object>();

            foreach (var list in db.HrmsLeaveTypes.Where(d => d.Status == true))
            {
                // List.Add(new { Text = string.Format("{0}" + "-:-" + "{1}", list.ID, list.Leave_Amount), Value = list.ID });
                List.Add(new { Text = list.Leave_Name, Value = string.Format("{0}" + "-:-" + "{1}", list.ID, list.Leave_Amount) });
            }
            return List;
        }

        /// <summary>
        /// Load Leave Year for Dropdown used in Employee Leave Assign 
        /// </summary>
        /// <returns></returns>

        public List<object> GetAssignLeaveYear()
        {
            var List = new List<object>();
            int currentYear = DateTime.Now.Year;
            for (int i = currentYear; i < currentYear + 5; i++)
            {
                List.Add(new DropDownData(i.ToString(), i.ToString()));
            }
            return List;
        }


        /// <summary>
        /// Load Leave Year for Dropdown used in Employee Leave Assign 
        /// </summary>
        /// <returns></returns>

        public List<object> GetAssignPreviousLeaveYear()
        {
            var List = new List<object>();
            int currentYear = DateTime.Now.Year;
            int year = currentYear - 10;
            for (int i = year; i < year + 10; i++)
            {
                List.Add(new DropDownData(i.ToString(), i.ToString()));
            }
            return List;
        }


        /// <summary>
        /// Load Attendance Remars Dropdown used in Employee Attendance Edit 
        /// </summary>
        /// <returns></returns>

        public List<object> GetAttendanceRemarks()
        {

            var List = new List<object>();
            foreach (var list in db.HrmsAttendanceRemarks.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.Remarks, Value = list.Remarks });
            }
            return List;
        }

        //public List<VMApplications> GetApplicationForms()
        //{
        //    var List = new List<VMApplications>();
        //    List.Add(new VMApplications { Name = "Maternity Leave Form", ID = 1 });
        //    List.Add(new VMApplications { Name = "Earn Leave Form", ID = 2 });
        //    List.Add(new VMApplications { Name = "Maternity Leave Form", ID = 3 });
        //    List.Add(new VMApplications { Name = "Maternity Leave Form", ID = 4 });
        //    List.Add(new VMApplications { Name = "Maternity Leave Form", ID = 5 });

        //    return List;
        //}

        ///// <summary>
        ///// Load All Grade Data for dropdown to use View All and Grade wise Salary----
        ///// </summary>
        ///// <returns></returns>
        //public List<object> GetSalaryGradeAll()
        //{
        //    var List = new List<object>();
        //    List.Add(new { Text = "All", Value = 1000 });
        //    foreach (var list in db.HrmsSalaryGrades.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.GradeName, Value = list.ID });
        //    }
        //    return List;
        //}


        //public List<object> GetSalaryGrade()
        //{
        //    var List = new List<object>();
        //    foreach (var list in db.HrmsSalaryGrades.Where(d => d.Status == true).OrderBy(g => g.GradeName))
        //    {
        //        List.Add(new { Text = list.GradeName, Value = list.ID });
        //    }
        //    return List;
        //}

        ///// <summary>
        ///// Load dropdown with All Keyword and all overtime grades-----It is used in only to view page0-------
        ///// </summary>
        ///// <returns></returns>
        //public List<object> GetOvertimeGradeAll()
        //{
        //    var List = new List<object>();
        //    List.Add(new { Text = "All", Value = 1000 });
        //    foreach (var list in db.HrmsOverTimesGrades.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.GradeName, Value = list.ID });
        //    }
        //    return List;
        //}

        ///// <summary>
        ///// It is used in submit form,
        ///// </summary>
        ///// <returns></returns>
        //public List<object> GetOvertimeGrade()
        //{
        //    var List = new List<object>();
        //    foreach (var list in db.HrmsOverTimesGrades.Where(d => d.Status == true))
        //    {
        //        List.Add(new {Text = list.GradeName, Value = list.ID});
        //    }
        //    return List;
        //}

        public List<object> GetClosePayrollList()
        {
            var PayrollMonthList = new List<object>();
            foreach (var payroll in db.HrmsPayrolls.Where(d => d.ClosePayroll == true))
            {
                PayrollMonthList.Add(new { Text = payroll.Note + "|(" + payroll.FromDate.ToShortDateString() + ")" + "-" + "(" + payroll.ToDate.ToShortDateString() + ")", Value = payroll.ID });
            }
            return PayrollMonthList;
        }

        public List<object> GetOpenPayrollList()
        {
            var PayrollMonthList = new List<object>();

            foreach (var payroll in db.HrmsPayrolls.Where(d => d.ClosePayroll == false))
            {
                PayrollMonthList.Add(new { Text = payroll.Note + "|(" + payroll.FromDate.ToShortDateString() + ")" + "-" + "(" + payroll.ToDate.ToShortDateString() + ")", Value = payroll.ID });
            }
            return PayrollMonthList;
        }

        public List<object> GetPayrollList()
        {
            var PayrollMonthList = new List<object>();

            foreach (var payroll in db.HrmsPayrolls.Where(d => d.Status == true))
            {
                PayrollMonthList.Add(new { Text = payroll.Note + "|(" + payroll.FromDate.ToShortDateString() + ")" + "-" + "(" + payroll.ToDate.ToShortDateString() + ")", Value = payroll.ID });
            }
            return PayrollMonthList;
        }

        //public List<object> GetSectionList()
        //{
        //    var PayrollMonthList = new List<object>();

        //    foreach (var payroll in db.HrmsPayrolls.Where(d => d.Status == true)
        //            /* .GroupBy(x => x.FromDate.Month).Select(x => x.FirstOrDefault())*/)
        //    {
        //        PayrollMonthList.Add(new { Text = "(" + payroll.FromDate.ToShortDateString() + ")" + "-" + "(" + payroll.ToDate.ToShortDateString() + ")" + "/" + payroll.Note, Value = payroll.ID });
        //    }
        //    return PayrollMonthList;
        //}
        public List<object> GetLeaveStatusList()
        {
            var LeaveStatuslist = new List<object>();
            LeaveStatuslist.Add(new { Text = "ALL", Value = "3" });
            LeaveStatuslist.Add(new { Text = "Pending", Value = "0" });
            LeaveStatuslist.Add(new { Text = "Approved", Value = "1" });
            return LeaveStatuslist;
        }

        public List<object> GetMonthlyList()
        {
            var MonthlyList = new List<object>();

            MonthlyList.Add(new { Text = "Yes", Value = "1" });
            MonthlyList.Add(new { Text = "No", Value = "2" });

            return MonthlyList;
        }

        public List<object> GetFastivalList()
        {
            var FastivalList = new List<object>();
            FastivalList.Add(new { Text = "No", Value = "0" });
            FastivalList.Add(new { Text = "Yes", Value = "1" });

            return FastivalList;
        }

        public List<object> GetOTEligibleList()
        {
            var FastivalList = new List<object>();
            FastivalList.Add(new { Text = "OT & Attendance Bonus Alocate", Value = "1" });
            FastivalList.Add(new { Text = "Only OT Alocate", Value = "0" });
            FastivalList.Add(new { Text = "Only Attendance Bonus Alocate", Value = "2" });
            FastivalList.Add(new { Text = "No Bonus Alocate", Value = "3" });
            return FastivalList;
        }
        /// <summary>
        /// Employee Grade For Reports
        /// </summary>

        public List<object> GetEmployeeGradeList()
        {
            var List = new List<object>();
            var vData = db.Employees.Where(d => d.Present_Status == 1);
            if (vData.Any())
            {
                foreach (var list in vData)
                {
                    List.Add(new { Text = list.Grade, Value = list.ID });
                }
            }
            return List;
        }
        /// <summary>
        /// Employee Designations For Reports
        /// </summary>

        public List<object> GetEmployeeDesignationsList()
        {
            var List = new List<object>();

            foreach (var list in db.Designations.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.DesigNameBangla, Value = list.ID });
            }
            return List;
        }
        /// <summary>
        /// Employee Sections For Reports
        /// </summary>
        /// <returns></returns>
        public List<object> GetEmployeeSectionsList()
        {
            var List = new List<object>();
            foreach (var list in db.Sections.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.SectionName, Value = list.ID });
            }
            return List;
        }

        public List<object> GetSalaryGradeList()
        {
            var List = new List<object>();

            foreach (var list in db.SalaryGrade.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.Name, Value = list.ID });
            }
            return List;
        }

        /// <summary>
        /// Child Name For Maternity Leave Application
        /// </summary>
        /// <returns></returns>

        public List<object> GetChildGender()
        {
            var MonthlyList = new List<object>();
            MonthlyList.Add(new { Text = "কন্যা", Value = "কন্যা" });
            MonthlyList.Add(new { Text = "পুত্র", Value = "পুত্র" });


            return MonthlyList;
        }

        public List<object> GetReportList()
        {
            var reportList = new List<object>();
            reportList.Add(new { Text = "Present Employee Information", Value = "1" });
            reportList.Add(new { Text = "Employee Address Information -1", Value = "2" });
            reportList.Add(new { Text = "Employee Address Information -2", Value = "3" });
            reportList.Add(new { Text = "Employee Gender Information", Value = "4" });
            reportList.Add(new { Text = "Periodic Joining List", Value = "5" });
            reportList.Add(new { Text = "Staff Worker Summary", Value = "6" });
            reportList.Add(new { Text = "Male Female Report", Value = "7" });
            reportList.Add(new { Text = "Desingation Wise Employee Report", Value = "8" });
            reportList.Add(new { Text = "Grade Wise Employee Report", Value = "9" });
            reportList.Add(new { Text = "Age Wise Employee Report", Value = "10" });
            reportList.Add(new { Text = "Salary Wise Employee Report", Value = "11" });
            reportList.Add(new { Text = "Job Duration Employee Report", Value = "12" });
            return reportList;
        }


        public List<object> GetGradeList()
        {
            var reportList = new List<object>();
            reportList.Add(new { Text = "All", Value = "All" });
            reportList.Add(new { Text = "Grade 1", Value = "Grade 1" });
            reportList.Add(new { Text = "Grade 2", Value = "Grade 2" });
            reportList.Add(new { Text = "Grade 3", Value = "Grade 3" });
            reportList.Add(new { Text = "Grade 4", Value = "Grade 4" });
            reportList.Add(new { Text = "Grade 5", Value = "Grade 5" });
            reportList.Add(new { Text = "Grade 6", Value = "Grade 6" });
            reportList.Add(new { Text = "Grade 7", Value = "Grade 7" });
            reportList.Add(new { Text = "None", Value = "None" });
            return reportList;
        }
        
        /// <summary>
        /// Employee Sections For Reports
        /// </summary>
        /// <returns></returns>
        public List<object> GetEmployeeSectionsListWithAll()
        {
            var List = new List<object>();
            List.Add(new { Text = "All", Value = "25000000" });
            foreach (var list in db.Sections.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.SectionName, Value = list.ID });
            }
            return List;
        }

        /// <summary>
        /// Employee Designations For Reports
        /// </summary>

        public List<object> GetEmployeeDesignationsListwithAll()
        {
            var List = new List<object>();
            List.Add(new { Text = "All", Value = "0" });
            foreach (var list in db.Designations.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.DesigNameBangla, Value = list.ID });
            }
            return List;
        }


        public List<object> GetLeaveReportList()
        {
            var reportList = new List<object>();
            reportList.Add(new { Text = "Date Range Wise Leave Report", Value = "20" });
            reportList.Add(new { Text = "Section Wise Leave Report", Value = "21" });
            reportList.Add(new { Text = "Meternity Leave Report", Value = "22" });
          //  reportList.Add(new { Text = "Resignation Earn Leave Reprt", Value = "23" });
            return reportList;
        }

        public List<object> GetAttendanceReportList()
        {
            //Oss.Hrms.Models.Services.CurrentUser user1 = (Oss.Hrms.Models.Services.CurrentUser)Session["User"];
            var reportList = new List<object>();
            
            reportList.Add(new { Text = "Daily Attendance Details(EXCEL)", Value = "7" });
            reportList.Add(new { Text = "Daily Attendance Details(PDF)", Value = "30" });
            reportList.Add(new { Text = "Daily Attendance Summary(PDF)", Value = "28" });
            reportList.Add(new { Text = "Personal Attendance Summary", Value = "8" });
            // reportList.Add(new { Text = "Daily Invalid Attendance", Value = "9" });
            reportList.Add(new { Text = "Daily Attendance", Value = "10" });
            reportList.Add(new { Text = "Daily Late Report", Value = "11" });
            reportList.Add(new { Text = "Daily Absent Report", Value = "12" });
            reportList.Add(new { Text = "Daily Invalid Report", Value = "13" });
            reportList.Add(new { Text = "Section Wise Tiffin List", Value = "14" });
            reportList.Add(new { Text = "Section Wise Tiffin List Summary", Value = "18" });
            reportList.Add(new { Text = "Personal Monthly Attendance Summary", Value = "15" });
            reportList.Add(new { Text = "Personal Monthly Attendance Summary", Value = "9" });
            reportList.Add(new { Text = "Overtime Report", Value = "16" });
            reportList.Add(new { Text = "Total Monthly Attendance Report With In Out and Overtime", Value = "17" });
            reportList.Add(new { Text = "Employee Job Card", Value = "26" });
            reportList.Add(new { Text = "Yearly Attendance Summary(PDF)", Value = "29" });
            reportList.Add(new { Text = "Monthly Employee Job Card Summary", Value = "27" });
          
     
     
            return reportList;
        }


        public List<object> GetAttendanceAReportList()
        {
            //Oss.Hrms.Models.Services.CurrentUser user1 = (Oss.Hrms.Models.Services.CurrentUser)Session["User"];
            var reportList = new List<object>();

            reportList.Add(new { Text = "Daily Attendance Summary", Value = "7" });
          //  reportList.Add(new { Text = "Personal Attendance Summery", Value = "8" });
            // reportList.Add(new { Text = "Daily Invalid Attendance", Value = "9" });
           // reportList.Add(new { Text = "Daily Attendance", Value = "10" });
           // reportList.Add(new { Text = "Daily Late Report", Value = "11" });
            reportList.Add(new { Text = "Daily Absent Report", Value = "12" });
           // reportList.Add(new { Text = "Daily Invalid Report", Value = "13" });
           // reportList.Add(new { Text = "Section Wise Tiffin List", Value = "14" });
            reportList.Add(new { Text = "Section Wise Tiffin List Summary", Value = "18" });
            reportList.Add(new { Text = "Personal Monthly Attendance Summary", Value = "15" });
            reportList.Add(new { Text = "Employee Job Card", Value = "25" });
           // reportList.Add(new { Text = "Personal Monthly Attendance Summery", Value = "9" });
          //  reportList.Add(new { Text = "Overtime Report", Value = "16" });
         //   reportList.Add(new { Text = "Total Monthly Attendance Report With In Out and Overtime", Value = "17" });
            return reportList;
        }


        public List<object> GetMaternityLawStep()
        {
            var MaternityStep = new List<object>();
            MaternityStep.Add(new { Text = "প্রথম", Value = "প্রথম" });
            MaternityStep.Add(new { Text = "দ্বিতীয়", Value = "দ্বিতীয়" });
            MaternityStep.Add(new { Text = "তৃতীয়", Value = "তৃতীয়" });
            return MaternityStep;
        }

        public List<object> PhysicalEligibility()
        {
            var PE = new List<object>();
            PE.Add(new { Text = "না", Value = "না" });
            PE.Add(new { Text = "হ্যাঁ", Value = "হ্যাঁ" });

            return PE;
        }

        public List<object> GetPayrollReportList()
        {
            var reportList = new List<object>();
            reportList.Add(new { Text = "Salary Sheet", Value = "1" });
            reportList.Add(new { Text = "Salary with Extra OT Pay", Value = "2" });
            reportList.Add(new { Text = "Worker Night Payable Sheet", Value = "3" });
            reportList.Add(new { Text = "Salary Increment Report", Value = "4" });
            reportList.Add(new { Text = "Staff Night & HD Details Bill", Value = "5" });
            reportList.Add(new { Text = "Staff Night & HD Summary Bill", Value = "9" });
            reportList.Add(new { Text = "Extra OT,Night & Salary Payable", Value = "6" });
            reportList.Add(new { Text = "Extra OT,Night & Salary Payable Summary", Value = "10" });
            reportList.Add(new { Text = "Salary Summary Sheet", Value = "7" });
            reportList.Add(new { Text = "Salary Lefty Summary Sheet", Value = "11" });
            reportList.Add(new { Text = "Lefty Extra OT Night & Salary Payable", Value = "8" });
            return reportList;
        }

        public List<object> GetCardTitleList()
        {
            var List = new List<object>();
            var vData = (from o in db.Employees
                         where o.Present_Status == 1 && o.Status == true
                         group o by new { o.CardTitle } into all
                         select new
                         {
                             CardTitle = all.Key.CardTitle
                         }).OrderBy(a => a.CardTitle).ToList();
            List.Add(new { Text = "All Card", Value = 1 });
            int id = 1;
            foreach (var list in vData)
            {
                ++id;
                List.Add(new { Text = list.CardTitle, Value = id });
            }
            return List;
        }

        public string GetCardTitle(int ID)
        {
            string CT = string.Empty;
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            var List = new List<object>();
            var vData = (from o in db.Employees
                         where o.Present_Status == 1 && o.Status == true
                         group o by new { o.CardTitle } into all
                         select new
                         {
                             CardTitle = all.Key.CardTitle
                         }).OrderBy(a => a.CardTitle).ToList();
            dictionary.Add(1, "All Card");
            int id = 1;
            foreach (var list in vData)
            {
                ++id;
                dictionary.Add(id, list.CardTitle);
            }

            CT = dictionary[ID];
            return CT;
        }

        public List<object> GetNightBillCreateDesignationList()
        {
            var List = new List<object>();

            var ActiveEmployee = (from o in db.Designations
                                  where o.Status == true
                                  select new
                                  {
                                      Id = o.ID,
                                      Name = o.Name
                                  }).ToList();

            var salaryEmployee = (from o in db.HrmsEodRecords
                                  join p in db.Employees on o.EmployeeId equals p.ID
                                  join q in db.Designations on p.Designation_Id equals q.ID
                                  where o.Eod_RefFk == 46 || o.Eod_RefFk == 47
                                  select new
                                  {
                                      Id = q.ID,
                                      Name = q.Name
                                  }).ToList();

            var notfoundNightBill = ActiveEmployee.Except(salaryEmployee).OrderBy(a => a.Name).ToList();

            foreach (var list in notfoundNightBill)
            {
                List.Add(new { Text = list.Name, Value = list.Id });
            }
            return List;
        }

        public List<object> GetGradeStaticList()
        {
            var listGrade = new List<object>();
            List<string> lstGrade = new List<string>() { "None", "Grade 1", "Grade 2", "Grade 3", "Grade 4", "Grade 5", "Grade 6", "Grade 7", "First" };
            
            int id = 0;
            foreach (var v in lstGrade)
            {
                listGrade.Add(new { Text = v, Value = id });
                ++id;
            }
            return listGrade;
        }

        public string GetGradeById(int GradeId)
        {
            List<string> lstGrade = new List<string>() { "None", "Grade 1", "Grade 2", "Grade 3", "Grade 4", "Grade 5", "Grade 6", "Grade 7", "First" };

            return lstGrade.ElementAt(GradeId);
        }

        public List<object> GetEmployeeStatus()
        {
            var listStatus = new List<object>();
            List<string> lstStatus = new List<string>() { "Resign", "Lefty", "Terminate", "Join" };
            int id = 0;
            foreach (var v in lstStatus)
            {
                listStatus.Add(new { Text = v, Value =  ++id });
            }
            
            return listStatus;
        }

        public List<object> GetEmployeePayrollStatus()
        {
            var listStatus = new List<object>();
            List<string> lstStatus = new List<string>() { "Resign", "Lefty and Terminate" };
            int id = 0;
            foreach (var v in lstStatus)
            {
                listStatus.Add(new { Text = v, Value = ++id });
            }

            return listStatus;
        }

        public List<object> GetEmployeeStatusList()
        {
            var listStatus = new List<object>();
            List<string> lstStatus = new List<string>() { "Active", "Resign", "Lefty", "Terminate" };
            int id = 0;
            foreach (var v in lstStatus)
            {
                listStatus.Add(new { Text = v, Value = ++id });
            }

            return listStatus;
        }

        /// <summary>
        /// Load Active District Dropdown used in Police Station Add, edit 
        /// </summary>
        /// <returns></returns>
        public List<object> GetEducationList()
        {
            var educationList = new List<object>();

            foreach (var educations in db.EducationDegree.Where(d => d.Status).OrderBy(x => x.Level))
            {
                educationList.Add(new { Text = educations.Name, Value = educations.ID });
            }
            return educationList;
        }
        public List<object> GetActiveDistrictList()
        {

            var List = new List<object>();
            foreach (var list in db.District.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.DistrictName, Value = list.ID });
            }
            return List;
        }

        /// <summary>
        /// Load Active District Dropdown used in Police Station Add, edit 
        /// </summary>
        /// <returns></returns>

        public List<object> GetActivePoliceStationList()
        {

            var List = new List<object>();
            foreach (var list in db.PoliceStation.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.PoliceStationName, Value = list.ID });
            }
            return List;
        }
        
        /// <summary>
        /// Load Active District Dropdown used in Police Station Add, edit 
        /// </summary>
        /// <returns></returns>

        public List<object> GetActivePoliceStationList1()
        {

            var List = new List<object>();
            foreach (var list in db.PoliceStation.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.PoliceStationName, Value = list.PoliceStationName });
            }
            return List;
        }

        /// <summary>
        /// Load Active District Dropdown used in Police Station Add, edit 
        /// </summary>
        /// <returns></returns>

        public List<object> GetSpecificActivePoliceStationList(int id)
        {

            var List = new List<object>();
            foreach (var list in db.PoliceStation.Where(d => d.District_ID == id && d.Status == true))
            {
                List.Add(new { Text = list.PoliceStationName, Value = list.ID });
            }
            return List;
        }
        
        /// <summary>
        /// Load Active District Dropdown used in Police Station Add, edit 
        /// </summary>
        /// <returns></returns>

        public List<object> GetActivePostOfficeList()
        {

            var List = new List<object>();
            foreach (var list in db.PostOffice.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.PostOfficeName, Value = list.ID });
            }
            return List;
        }

        /// <summary>
        /// Load Active District Dropdown used in Police Station Add, edit 
        /// </summary>
        /// <returns></returns>

        public List<object> GetSpecificActivePostOfficeList(int id)
        {

            var List = new List<object>();
            foreach (var list in db.PostOffice.Where(d => d.PoliceStation_ID == id && d.Status == true))
            {
                List.Add(new { Text = list.PostOfficeName, Value = list.ID });
            }
            return List;
        }

        /// <summary>
        /// Load Active District Dropdown used in Police Station Add, edit 
        /// </summary>
        /// <returns></returns>

        public List<object> GetActiveVillageList()
        {

            var List = new List<object>();
            foreach (var list in db.Village.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.VillageName, Value = list.ID });
            }
            return List;
        }
        
        public IEnumerable<object> GetPoliceStationlistforEmployeeEdit(int id)
        {
            var List = new List<object> ();
            foreach (var list in db.PoliceStation.Where(u => u.District_ID == id))
            {
                List.Add(new { Text = list.PoliceStationName, Value = list.ID });
            }
            return List;
        }
        
        public IEnumerable<object> GetPostOfficelistforEmployeeEdit(int id)
        {
            var List = new List<object>();
            foreach (var list in db.PostOffice.Where(u => u.PoliceStation_ID == id))
            {
                List.Add(new { Text = list.PostOfficeName, Value = list.ID });
            }
            return List;
        }
        
        public IEnumerable<object> GetVillagelistforEmployeeEdit(int id)
        {
            var List = new List<object>();
            foreach (var list in db.Village.Where(u => u.PostOffice_ID == id))
            {
                List.Add(new { Text = list.VillageName, Value = list.ID });
            }
            return List;
        }
    }
}

    
