﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_EodReference:RootModel
    {
        public bool Eod { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string EnglishName { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Name { get; set; }
        public decimal CalculativeAmount { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Condition { get; set; }
        public int Start { get; set; }
        public int Finish { get; set; }
        [Index("Ux_EodRefpriority", IsUnique = true)]
        public int priority { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string TypeBangla { get; set; }
        public bool Regular { get; set; }
        public bool IsHour { get; set; }
    }
}