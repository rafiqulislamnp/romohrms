﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class CheckInOut
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(50)]
        public string UserId { get; set; }
        public DateTime CheckTime { get; set; }
        [StringLength(50)]
        public string Checktype { get; set; }
        [StringLength(50)]
        public string VeryfyCode { get; set; }
        [StringLength(50)]
        public string CardNo { get; set; }
        public int SourceId { get; set; }
        [NotMapped]
        public bool  Retry { get; set; } = true;
    }
}