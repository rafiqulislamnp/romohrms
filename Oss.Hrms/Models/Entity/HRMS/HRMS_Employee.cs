using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Services;


namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_Employee : RootModel
    {
       // [Index("Ux_Employee", 2, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string CardTitle { get; set; }

       // [Index("Ux_Employee", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string EmployeeIdentity { get; set; }

        //[Index("Ux_Employee_CardNo", 3, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string CardNo { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Name { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Name_BN { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Father_Name { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(10)]
        public string FH { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Mother_Name { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "DateTime")]
        public DateTime DOB { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(25)]
        public string Gender { get; set; }
        //[ForeignKey("Country_Id")]
        public int Country_Id { get; set; }

        [ForeignKey("Country_Id")]
        public Country Country { get; set; }

        public int Designation_Id { get; set; }
        [ForeignKey("Designation_Id")]
        public Designation Designation { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Designation_Name { get; set; }

        public int Unit_Id { get; set; }
        [ForeignKey("Unit_Id")]
        public Unit Unit { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Unit_Name { get; set; }
        
        public int Department_Id { get; set; }
        [ForeignKey("Department_Id")]
        public Department Department { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Department_Name { get; set; }
       
        public int Section_Id { get; set; }
        [ForeignKey("Section_Id")]
        public Section Section { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Section_Name { get; set; }

        public int  LineId { get; set; }
        [ForeignKey("LineId")]
        public LineNo LineNo { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Line_Name { get; set; }

        public int Business_Unit_Id { get; set; }
        [ForeignKey("Business_Unit_Id")]
        public BusinessUnit BusinessUnit { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(25)]
        public string Blood_Group { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Joining_Date { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Religion { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Mobile_No { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Alt_Mobile_No { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Alt_Person_Name { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Alt_Person_Contact_No { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Alt_Person_Address { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Emergency_Contact_No { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Office_Contact { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Land_Phone { get; set; }
        [DataType(DataType.EmailAddress)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Email { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Employement_Type { get; set; }

       // [Index("Ux_NID", 2, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string NID_No { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Passport_No { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(200)]
        public string Present_Address { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(200)]
        public string Permanent_Address { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Marital_Status { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Photo { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string ImagePath { get; set; }
        public int Present_Status { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Staff_Type { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Overtime_Eligibility { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Grade { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "DateTime")]
        public DateTime QuitDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "DateTime")]
        public DateTime JobPermanent_Date { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string AlertRemark { get; set; }
        public bool AlertActive { get; set; }

        public int ? PermanentDistrict_ID { get; set; }
        [ForeignKey("PermanentDistrict_ID")]
        public District PermanentDistrict { get; set; }
        public int ? PresentDistrict_ID { get; set; }
        [ForeignKey("PresentDistrict_ID")]
        public District PresentDistrict { get; set; }

        public int? PermanentPoliceStation_ID { get; set; }
        [ForeignKey("PermanentPoliceStation_ID")]
        public PoliceStation PermanentPoliceStation { get; set; }
        public int? PresentPoliceStation_ID { get; set; }
        [ForeignKey("PresentPoliceStation_ID")]
        public PoliceStation PresentPoliceStation { get; set; }

        public int? PermanentPostOffice_ID { get; set; }
        [ForeignKey("PermanentPostOffice_ID")]
        public PostOffice PermanentPostOffice { get; set; }
        public int? PresentPostOffice_ID { get; set; }
        [ForeignKey("PresentPostOffice_ID")]
        public PostOffice PresentPostOffice { get; set; }


        public int? PermanentVillage_ID { get; set; }
        [ForeignKey("PermanentVillage_ID")]
        public Village PermanentVillage { get; set; }
        public int? PresentVillage_ID { get; set; }
        [ForeignKey("PresentVillage_ID")]
        public Village PresentVillage { get; set; }

      

        //public int PermanentVillage_ID { get; set; }
        //[ForeignKey("PermanentVillage_ID")]
        //public int PresentVillage_ID { get; set; }
        //[ForeignKey("PresentVillage_ID")]
        //public Village Village { get; set; }



        // public District District { get; set; }


        // public PoliceStation PoliceStation { get; set; }


        // public PostOffice PostOffice { get; set; }


        //public Village Village { get; set; }

    }

    public class HRMS_Spouse : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string SpouseName { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime SpouseDOB { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Spouse_Contact_No { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Spouse_Blood_Group { get; set; }
    }

    public class HRMS_Child : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Child_Name { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Child_Blood_Group { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Child_DOB { get; set; }
    }

    public class HRMS_Education : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Degree { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(150)]
        public string Institute { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string GroupOrSubject { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(10)]
        public string Passing_Year { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Result { get; set; }
    }

    public class HRMS_Skill : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Skill_Type { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Skill_Name { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(500)]
        public string Skill_Description { get; set; }
    }

    public class HRMS_Training : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Course_Name { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(200)]
        public string Institute { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Country { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Course_Start_Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Course_End_Date { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Course_Duration { get; set; }
    }

    public class HRMS_Reference : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Referee_Name { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Contact_No { get; set; }
        [DataType(DataType.EmailAddress)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Email_Id { get; set; }
        //[Column(TypeName = "NVARCHAR")]
        //[StringLength(200)]
        //public string Address { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Relation { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Referee_FatherName { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Referee_MotherName { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(200)]
        public string PresentAddress { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(200)]
        public string PermanentAddress { get; set; }

        [DefaultValue(true)]
        public bool IsReference { get; set; }
        public string Address { get; internal set; }
        //[IsReference] [bit] 
    }

    public class HRMS_Salary : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Salary_Grade { get; set; }
        public decimal Joining_Salary { get; set; }
        public decimal Basic { get; set; }
        public decimal House { get; set; }
        public decimal Health { get; set; }
        public decimal Food { get; set; }
        public decimal Transport { get; set; }
        public decimal OTRate { get; set; }
        public decimal Current_Salary { get; set; }
        public string Account_No { get; internal set; }
        public string Bank_Name { get; internal set; }
        public string Payment_Mode { get; internal set; }
    }
    
    public class HRMS_Salary_History : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime JoiningDate { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public char Salary_Grade { get; set; }
        public decimal Joining_Salary { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Payment_Mode { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(200)]
        public string Bank_Name { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Account_No { get; set; }
        public decimal Current_Salary { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Action_Name { get; set; }
        public string HrmsSalaries { get; internal set; }
        public string EodRecords { get; internal set; }
    }

    public class HRMS_Experience : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Company_Name { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Designation { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Department { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(500)]
        public string Responsibility { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Job_Start_Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Job_End_Date { get; set; }
    }

    public class HRMS_Facility : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Item_Name { get; set; }
        public decimal Cost_Per_Month { get; set; }
    }

    public class HRMS_Facility_History : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Item_Name { get; set; }
        public decimal Cost_Per_Month { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Action_Name { get; set; }
    }

    public class HRMS_Hobby : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(200)]
        public string Hobby_Name { get; set; }
        public string Remarks { get; set; }
    }

    public class HRMS_LinkSuperior : RootModel
    {
        public int EmployeeId { get; set; }
        public int SuperiorId { get; set; }
        public int Type { get; set; }

    }

    /// <summary>
    /// New Linking Class-----to Employee
    /// </summary>

    //public class HRMS_LinkSuperior : RootModel
    //{
    //    public int EmployeeId { get; set; }
    //    [Display(Name = "Admin")]
    //    public int AdminId { get; set; }
    //    [Display(Name = "Line Manager")]
    //    public int LineManagerId { get; set; }
    //    [Display(Name = "Dept. Head")]
    //    public int DeptHeadId { get; set; }

    //}
    //public class HRMS_LinkSuperior : RootModel
    //{
    //    public int EmployeeId { get; set; }
    //    [Display(Name = "Admin")]
    //    public int AdminId { get; set; }
    //    [Display(Name = "Line Manager")]
    //    public int LineManagerId { get; set; }
    //    [Display(Name = "Dept. Head")]
    //    public int DeptHeadId { get; set; }
    //}
    /// <summary>
    /// Linking Class-----to Employee
    /// </summary>
    public class HRMS_LineManagerToEmployeeLink : RootModel
    {
        public int LineManagerId { get; set; }
        public int EmployeeId { get; set; }
    }

    public class HRMS_AdminToEmployeeLink : RootModel
    {
        public int AdminId { get; set; }
        public int EmployeeId { get; set; }
    }
    public class HRMS_DepartmentHeadToEmployeeLink : RootModel
    {
        public int DeptHeadId { get; set; }
        public int EmployeeId { get; set; }
    }



    public class HRMS_Upload_Document :RootModel
    {

        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Document_Type { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Document_Name { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Document { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string DocumentPath { get; set; }
    }


    public class EducationDegree : RootModel
    {
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Name { get; set; }
        public int Level { get; set; }
    }
    public class HRMS_CardTitle : RootModel
    {
        public string CardTitle { get; set; }
    }

}