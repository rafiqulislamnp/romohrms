﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_Bonus : RootModel
    {
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string BonusTitle { get; set; }
        public int? BonusYear { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime PaymentDate { get; set; }
        public bool CloseBonus { get; set; }
    }
}