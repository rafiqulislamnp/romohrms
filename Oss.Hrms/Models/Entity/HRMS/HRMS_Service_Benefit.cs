﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_Service_Benefit:RootModel
    {
     
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Days { get; set; }
        public int PayableDays { get; set; }
        public decimal PresentSalary { get; set; }
        public decimal PresentBasic { get; set; }
        public decimal Stamp { get; set; }
        public decimal CalculatedTotalAmount { get; set; }
        public decimal PayableTotalAmount { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime PaymentDate { get; set; }
        public bool IsAudit { get; set; }
    }
    
}