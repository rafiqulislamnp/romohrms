﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_Advance : RootModel
    {
        public DateTime PaymentDate { get; set; }
        public int EmployeeIdFK { get; set; }
        public decimal Amount { get; set; }
    }
}