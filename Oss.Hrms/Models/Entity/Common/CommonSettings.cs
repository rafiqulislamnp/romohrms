﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.Entity.Common
{
    public class CommonSettings
    {

    }

    public class LineNo: RootModel
    {
        [Index("UX_LineNo", 1, IsUnique = true)]
        [StringLength(50)]
        public string Line { get; set; }
    }

    public class Country : RootModel
    {
        [Index("UX_Country", 1, IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }
    }

    public class BusinessUnit : RootModel
    {
        [Index("UX_BU", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Name { get; set; }
        public int Country_ID { get; set; }
        [ForeignKey("Country_ID")]
        public Country Country { get; set; }
    }


    public class Unit : RootModel
    {
        [Index("UX_Unit", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string UnitName { get; set; }
        public int BusinessUnit_ID { get; set; }
        [ForeignKey("BusinessUnit_ID")]
        public BusinessUnit BusinessUnit { get; set; }
    }

    public class Department : RootModel
    {
         [Index("UX_Dept", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string DeptName { get; set; } 
        public string DeptNameBangla { get; set; }
         
        public int Unit_ID { get; set; }
        [ForeignKey("Unit_ID")]
        public Unit Unit { get; set; }
    }

    public class Section : RootModel
    {
        [Index("UX_Section", 1, IsUnique = true)]
        [StringLength(100)]
        public string SectionName { get; set; }
        [StringLength(100)]
        public string SectionNameBangla { get; set; }

        public int Dept_ID { get; set; }
        [ForeignKey("Dept_ID")]
        public Department Department { get; set; }
    }

    public class NotificationUser : RootModel
    {
        
        public string Employee_Id { get; set; }
        
        public string Name { get; set; }
        
        [Index("Ux_Notification", IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Subject { get; set; }
        [DisplayName("Business Unit")]
        public int BusinessUnit_ID { get; set; }
        [ForeignKey("BusinessUnit_ID")]
        public BusinessUnit BusinessUnit { get; set; }

    }


    public class Designation : RootModel
    {
        [Index("UX_Designation", 1, IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }
        public string DesigNameBangla { get; set; }
    }

    public class SalaryGrade : RootModel
    {
        [Index("UX_SalaryGrade", 1, IsUnique = true)]
        [StringLength(100)]
        public string Name { get; set; }

        public decimal Grade_Amount { get; set; }
    }


    public class District : RootModel
    {
        [Index("UX_DistrictName", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string DistrictName { get; set; }

        public virtual ICollection<HRMS_Employee> PresentDistrict { get; set; }
        public virtual ICollection<HRMS_Employee> PermanentDistrict { get; set; }

    }

    public class PoliceStation : RootModel
    {
       // [Index("UX_PoliceStationName", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PoliceStationName { get; set; }

        public int District_ID { get; set; }
        [ForeignKey("District_ID")]
        public District District { get; set; }


        public virtual ICollection<HRMS_Employee> PresentPoliceStation { get; set; }
        public virtual ICollection<HRMS_Employee> PermanentPoliceStation { get; set; }
    }

    public class PostOffice : RootModel
    {
       // [Index("UX_PostOfficeName", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PostOfficeName { get; set; }

        public int PoliceStation_ID { get; set; }
        [ForeignKey("PoliceStation_ID")]
        public PoliceStation PoliceStation { get; set; }


        public virtual ICollection<HRMS_Employee> PresentPostOffice { get; set; }
        public virtual ICollection<HRMS_Employee> PermanentPostOffice { get; set; }
    }

    public class Village : RootModel
    {
       // [Index("UX_VillageName", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string VillageName { get; set; }

        public int PostOffice_ID { get; set; }
        [ForeignKey("PostOffice_ID")]
        public PostOffice PostOffice { get; set; }

        public virtual ICollection<HRMS_Employee> PresentVillage { get; set; }
        public virtual ICollection<HRMS_Employee> PermanentVillage { get; set; }
    }
}