﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using System.ComponentModel.DataAnnotations;

namespace Oss.Hrms.Models.ViewModels.Attendance
{
    public class VMShiftRule : RootModel
    {
        private HrmsContext db = new HrmsContext();
        [Required(ErrorMessage ="Shift Rule is required"), Display(Name = "Rule Name")]
        public string RuleName { get; set; }
        [Required(ErrorMessage = "Hour is required")]
        public int Hour { get; set; }
        [Required(ErrorMessage = "DeductionMinute is required"), Display(Name = "Deduction Minute")]
        public int DeductionMinute { get; set; }

        [Display(Name = "Shift Type")]
        public int ShiftTypeId { get; set; }
        [Display(Name = "Shift Type")]
        public string ShiftName { get; set; }

        public HRMS_Shift_Rules HRMSShiftRules { get; set; }
        
        public IEnumerable<VMShiftRule> DataListShiftRule { get; set; }

        public void LoadShiftRule(int Id)
        {
            var a = (from t1 in db.HrmsShiftTypes
                     join t2 in db.HrmsShiftRules on t1.ID equals t2.ShiftTypeId
                     where t2.ShiftTypeId == Id 
                     select new VMShiftRule
                     {
                         ID = t2.ID,
                         ShiftTypeId=ID,
                         ShiftName = t1.ShiftName,
                         RuleName = t2.RuleName,
                         Hour = t2.Hour,
                         DeductionMinute = t2.DeductionMinute
                     }).AsEnumerable();
            this.DataListShiftRule = a;
        }

        public VMShiftRule LoadSpecificShiftRuleInfo(int id)
        {
            var a = (from t1 in db.HrmsShiftRules
                     where t1.ID == id
                     select new VMShiftRule
                     {
                         ID = t1.ID,
                         ShiftTypeId=t1.ShiftTypeId,
                         RuleName = t1.RuleName,
                         Hour = t1.Hour,
                         DeductionMinute = t1.DeductionMinute,
                         Entry_Date = t1.Entry_Date,
                         Entry_By = t1.Entry_By

                     }).FirstOrDefault();
            return a;
        }
    }
}