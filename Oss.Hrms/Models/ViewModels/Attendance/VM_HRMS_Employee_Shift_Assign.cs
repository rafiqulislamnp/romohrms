﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity.SqlServer;
using System.Web;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.Attendance
{
    public class VM_HRMS_Employee_Shift_Assign:RootModel
    {
        private HrmsContext db = new HrmsContext();


        [DisplayName("Shift Name")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        //[Required(ErrorMessage = "Please Shift Name")]
        public int ShiftTypeId { get; set; }                //Shift Type Id as Foreign Key
        [DisplayName("Shift Name")]
        public string ShiftName { get; set; }                //Shift Type Id as Foreign Key
      //  [ForeignKey("ShiftTypeId")]
        public HRMS_Shift_Type HRMSShiftType { get; set; }

        [DisplayName("Business Unit")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        //[Required(ErrorMessage = "Please Shift Name")]
        public int BU_Id { get; set; }
        [DisplayName("Business Unit")]
        public string  BusinessUnitName { get; set; }
      //  [ForeignKey("BU_Id")]
        public BusinessUnit BusinessUnit { get; set; }

        [DisplayName("Card Title")]
        public string CardTitle { get; set; }
        [DisplayName("Employee ID")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        //[Required(ErrorMessage = "Please Shift Name")]
        public int EmployeeId { get; set; }            // Employee Id Foreign key
        public string Name { get; set; }
        // Employee Id Foreign key
        [DisplayName("Employee ID")]
        public string EmployeeIdentity { get; set; }
        //  [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DisplayName("Start Date")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        [Required(ErrorMessage = "Please Enter Start Date")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        [Required(ErrorMessage = "Please Enter End Date")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        //[DataType(DataType.Time)]
        //public DateTime InTime { get; set; }

        //[DataType(DataType.Time)]
        //public DateTime OutTime { get; set; }
        //public int ActiveStatus { get; set; }

        [DisplayName("Department Name")]
        public int Dept_ID { get; set; }
        [DisplayName("Department Name")]
        public string Dept_Name { get; set; }

        [DisplayName("Designation Name")]
        public string DesignationName { get; set; }

        [DisplayName("Section Name")]
        public int Section_ID { get; set; }
        [DisplayName("Section Name")]
        public string Section_Name { get; set; }
        
        [DisplayName("Unit Name")]
        public int Unit_ID { get; set; }
        [DisplayName("Unit Name")]
        public string Unit_Name { get; set; }

        public List<string> EmployeeIds { get; set; }



        public IEnumerable<VM_HRMS_Employee_Shift_Assign> DataListHrmsEmployeeShiftAssigns { get; set; }


        public void SearchEmployeeShiftInformation(int id, DateTime fromdate, DateTime todate)
        {
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");

            var ds = db.Database.SqlQuery<VM_HRMS_Employee_Shift_Assign>("EXECUTE[dbo].[sp_ViewShiftData] "
                                                  + "@shiftId='" + id + "',"
                                                  + "@fromDate='" + dt1 + "',"
                                                  + "@todate='" + dt2 + "'").ToList();
            DataListHrmsEmployeeShiftAssigns = ds;
        }



        public void ShiftNotAssignEmployeeInformation(DateTime fromdate, DateTime todate)
        {
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");

            var ds = db.Database.SqlQuery<VM_HRMS_Employee_Shift_Assign>("EXECUTE[dbo].[sp_ViewShiftNotAssignEmployeeData] "
                                                                         + "@fromDate='" + dt1 + "',"
                                                                         + "@todate='" + dt2 + "'").ToList();
            DataListHrmsEmployeeShiftAssigns = ds;
        }






        //public List<Attendance> GetSpecificEmpAttDataCategory(string joiningStation, string startDate, string endDate, string late, string EmployeeId)
        //{
        //    string resp = "";
        //    var query = "";
        //    List<Attendance> list = new List<Attendance>();

        //    if (joiningStation != "Select All")
        //    {
        //        if (EmployeeId == "Select All")
        //        {
        //            query = "Select ROW_NUMBER() over (order by a.[Employee_Id] asc) as [Id],a.[Employee_Id],a.[EmployeeName],Convert(varchar(10),a.[Date],126) as [Date],a.[InTime],a.[OutTime],RIGHT('0' + CONVERT(varchar(3),DATEDIFF(minute,[InTime],[OutTime])/60),2) + ':' + RIGHT('0' + CONVERT(varchar(2),DATEDIFF(minute,[InTime],[OutTime])%60),2)as [TotalTime],a.[Late],a.[Early],a.[OverTime],a.[Status],a.[Present],a.[Absent],a.[CardNo],a.[Remarks],a.[ShiftName],b.department,b.Joining_Station from SIM_AttendanceInfoNew a inner join employee b on a.Employee_Id=b.Employee_ID where b.Joining_Station ='" +
        //                joiningStation + "' and b.Present_Status ='Active' and Date between '" + startDate +
        //                "' and '" + endDate + "'";
        //        }
        //        else
        //        {
        //            query = "Select ROW_NUMBER() over (order by a.[Employee_Id] asc) as [Id],a.[Employee_Id],a.[EmployeeName],Convert(varchar(10),a.[Date],126) as [Date],a.[InTime],a.[OutTime],RIGHT('0' + CONVERT(varchar(3),DATEDIFF(minute,[InTime],[OutTime])/60),2) + ':' + RIGHT('0' + CONVERT(varchar(2),DATEDIFF(minute,[InTime],[OutTime])%60),2)as [TotalTime],a.[Late],a.[Early],a.[OverTime],a.[Status],a.[Present],a.[Absent],a.[CardNo],a.[Remarks],a.[ShiftName],b.department,b.Joining_Station from SIM_AttendanceInfoNew a inner join employee b on a.Employee_Id=b.Employee_ID where b.Joining_Station ='" +
        //                    joiningStation + "' and b.Present_Status ='Active' and Date between '" + startDate +
        //                    "' and '" + endDate + "' and b.Employee_ID='" + EmployeeId + "'";
        //        }
        //    }
        //    else
        //    {   //a.[Date]
        //        query = "Select ROW_NUMBER() over (order by a.[Employee_Id]  asc) as [Id],a.[Employee_Id],a.[EmployeeName],Convert(varchar(10),a.[Date],126) as [Date],a.[InTime],a.[OutTime],RIGHT('0' + CONVERT(varchar(3),DATEDIFF(minute,[InTime],[OutTime])/60),2) + ':' + RIGHT('0' + CONVERT(varchar(2),DATEDIFF(minute,[InTime],[OutTime])%60),2)as [TotalTime],a.[Late],a.[Early],a.[OverTime],a.[Status],a.[Present],a.[Absent],a.[CardNo],a.[Remarks],a.[ShiftName],b.department,b.Joining_Station from SIM_AttendanceInfoNew a inner join employee b on a.Employee_Id=b.Employee_ID where b.Present_Status ='Active' and Date  between '" +
        //            startDate + "' and '" + endDate + "'";
        //    }
        //    var ds = SqlDataAccess.SQL_ExecuteReader(query, out resp);
        //    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //        {
        //            //,a.[Status],a.[Present],a.[Absent],
        //            var attinfo = new Attendance
        //            {
        //                id = ds.Tables[0].Rows[i]["Id"].ToString(),
        //                ADate = ds.Tables[0].Rows[i]["Date"].ToString(),
        //                EmployeeId = ds.Tables[0].Rows[i]["Employee_Id"].ToString(),
        //                Name = ds.Tables[0].Rows[i]["EmployeeName"].ToString(),
        //                Department = ds.Tables[0].Rows[i]["Department"].ToString(),
        //                JoiningStation = ds.Tables[0].Rows[i]["Joining_Station"].ToString(),
        //                Intime = ds.Tables[0].Rows[i]["InTime"].ToString(),
        //                OutTime = ds.Tables[0].Rows[i]["OutTime"].ToString(),
        //                TotalTime = ds.Tables[0].Rows[i]["TotalTime"].ToString(),
        //                Late = ds.Tables[0].Rows[i]["Late"].ToString(),
        //                Early = ds.Tables[0].Rows[i]["Early"].ToString(),
        //                OverTime = ds.Tables[0].Rows[i]["OverTime"].ToString(),
        //                Status = ds.Tables[0].Rows[i]["Status"].ToString(),
        //                Present = ds.Tables[0].Rows[i]["Present"].ToString(),
        //                Absent = ds.Tables[0].Rows[i]["Absent"].ToString(),
        //                Remarks = ds.Tables[0].Rows[i]["Remarks"].ToString(),
        //                ShiftName = ds.Tables[0].Rows[i]["ShiftName"].ToString()
        //            };
        //            list.Add(attinfo);
        //        }
        //    }
        //    return list;
        //}



        //public void LoadEmployeeShiftAssignInfo()
        //{
        //    var a = (from t1 in db.HrmsEmployeeShiftAssigns
        //            join t2 in db.HrmsShiftTypes on t1.ShiftTypeId equals t2.ID
        //            join t3 in db.BusinessUnits on t1.BU_Id equals t3.ID
        //            where t1.Status == true
        //        select new VM_HRMS_Employee_Shift_Assign()
        //        {
        //            ID = t1.ID,
        //            ShiftTypeId = t2.ID,
        //            ShiftName = t2.ShiftName,
        //            BU_Id=t1.BU_Id,
        //            BusinessUnitName = t3.Name,
        //            StartDate  = t1.StartDate,
        //            EndDate = t1.EndDate,
        //            ActiveStatus  = t1.ActiveStatus,
        //            Entry_Date =t1.EndDate,
        //            Entry_By=t1.Entry_By,
        //            Update_Date=t1.Update_Date,
        //            Update_By=t1.Update_By
        //        }).AsEnumerable();
        //    this.DataListHrmsEmployeeShiftAssigns = a;
        //}

        //public VM_HRMS_Employee_Shift_Assign LoadSpecificEmployeeShiftAssignInfo(int id)
        //{
        //    var a = (from t1 in db.HrmsEmployeeShiftAssigns
        //        join t2 in db.HrmsShiftTypes on t1.ShiftTypeId equals t2.ID
        //        join t3 in db.BusinessUnits on t1.BU_Id equals t3.ID
        //        where t1.Status == true  && t1.ID == id
        //        select new VM_HRMS_Employee_Shift_Assign()
        //        {
        //            ID = t1.ID,
        //            ShiftTypeId = t2.ID,
        //            ShiftName = t2.ShiftName,
        //            BU_Id = t1.BU_Id,
        //            BusinessUnitName = t3.Name,
        //            StartDate = t1.StartDate,
        //            EndDate = t1.EndDate,
        //            ActiveStatus = t1.ActiveStatus,
        //            Entry_Date = t1.EndDate,
        //            Entry_By = t1.Entry_By,
        //            Update_Date = t1.Update_Date,
        //            Update_By = t1.Update_By
        //        }).FirstOrDefault();
        //    return a;
        //}
    }
}