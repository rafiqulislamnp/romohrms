﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.Attendance
{
    public class VM_HRMS_Shift_Type : RootModel
    {
        private HrmsContext db = new HrmsContext();

        [DisplayName("Shift Name")]
        [StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        [Required(ErrorMessage = "Please Shift Name")]
        public string ShiftName { get; set; }
        [DisplayName("In Time")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "In Time should be minimum 1 characters and Maximum 50 characters")]
        [Required(ErrorMessage = "Please Shift In Time")]
        [DataType(DataType.Time)]
        public DateTime StartTime { get; set; }
        [DisplayName("Out Time")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "In Time should be minimum 1 characters and Maximum 50 characters")]
        [Required(ErrorMessage = "Please Shift Out Time")]
        [DataType(DataType.Time)]
        public DateTime EndTime { get; set; }
        [StringLength(50)]
        public string ShiftNameWithTime { get; set; }

        [DisplayName("Break Time")]
        public int DeductionTime { get; set; }
        [DisplayName("Tolerance Time")]
        public int ToleranceTime { get; set; }

        [DisplayName("Business Unit")]
        public int BU_Id { get; set; }     //Business Unit Id as Foreign Key
        // [ForeignKey("BU_Id")]
        public BusinessUnit BusinessUnit { get; set; }

        public IEnumerable<VM_HRMS_Shift_Type> DataListHrmsShiftTypes { get; set; }


        public void LoadShiftTypeInfo()
        {
            var a = (from t1 in db.HrmsShiftTypes
                        //where t1.Status == true 
                     select new VM_HRMS_Shift_Type()
                     {
                         ID = t1.ID,
                         ShiftName = t1.ShiftName,
                         StartTime = t1.StartTime,
                         EndTime=t1.EndTime,
                         DeductionTime = t1.DeductionTime,
                         ShiftNameWithTime = t1.ShiftNameWithTime
                     }).AsEnumerable();
            this.DataListHrmsShiftTypes = a;
        }

        public VM_HRMS_Shift_Type LoadSpecificShiftTypeInfo(int id)
        {
            var a = (from t1 in db.HrmsShiftTypes
                     where t1.Status == true &&  t1.ID == id
                     select new VM_HRMS_Shift_Type()
                     {
                         ID = t1.ID,
                         ShiftName = t1.ShiftName,
                         StartTime = t1.StartTime,
                         EndTime = t1.EndTime,
                         DeductionTime = t1.DeductionTime,
                         ShiftNameWithTime = t1.ShiftNameWithTime,
                         Entry_Date=t1.Entry_Date,
                         Entry_By=t1.Entry_By

                     }).FirstOrDefault();
            return a;
        }
    }
}