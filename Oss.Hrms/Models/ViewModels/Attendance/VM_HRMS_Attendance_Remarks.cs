﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;

namespace Oss.Hrms.Models.ViewModels.Attendance
{
    public class VM_HRMS_Attendance_Remarks
    {
        HrmsContext db = new HrmsContext();

        [DisplayName("Remarks:")]
        [Required(ErrorMessage ="Please Enter Your Remarks")]
        public string Remarks { get; set; }
        public IEnumerable<VM_HRMS_Attendance_Remarks> DataList { get; set; }

        public HRMS_Attendance_Remarks hrms_attendance_remarks { get; set; }
       


        public void GetLoadData()
        {
            db = new HrmsContext();
            var v = (from o in db.HrmsAttendanceRemarks
                     where (o.Status == true)
                     select new VM_HRMS_Attendance_Remarks
                     {
                         hrms_attendance_remarks = o
                     }).ToList();
            this.DataList = v;
        }

        public void SelectSingle(int id)
        {
            db = new HrmsContext();
            var v = (from o in db.HrmsAttendanceRemarks
                     where (o.Status == true && o.ID == id)
                     select new VM_HRMS_Attendance_Remarks
                     {
                         hrms_attendance_remarks = o
                     }).FirstOrDefault();
            this.hrms_attendance_remarks = v.hrms_attendance_remarks;
        }

    }
}