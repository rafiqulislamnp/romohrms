﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMBusinessUnit:RootModel
    {
        private HrmsContext db = new HrmsContext();
        //  
       // [Remote("IsBusinessUnitExists", "HRMS",HttpMethod ="Post", ErrorMessage = "Business Unit Name already in use")]
        [DisplayName("Buniness Unit Name")]
        [Required(ErrorMessage = "Please Enter Business Unit Name")]
        // [Index("UX_Country", 1, IsUnique = true)]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Name should be minimum 3 characters and Maximum 100 characters")]
        public string Name { get; set; }

        [DisplayName("Country Name")]
        [Required(ErrorMessage = "Please Select Country Name")]
        // [Index("UX_Country", 1, IsUnique = true)]
        // [StringLength(100, MinimumLength = 3, ErrorMessage = "Name should be minimum 3 characters and Maximum 100 characters")]
        public int Country_SL { get; set; }
        public string Country_Name { get; set; }

        //public BusinessUnit BusinessUnit { get; set; }
        //public Country Country { get; set; }
 
        public IEnumerable<VMBusinessUnit> DataList { get; set; }




        //public void LoadBusinessUnit()
        //{
        //    var a  = (from t1 in db.BusinessUnits 
        //              join Country in db.Countries on BusinessUnit.Country_SL equals Country.ID
        //                select new VMBusinessUnit
        //                {
        //                    Country=Country,
        //                    BusinessUnit = BusinessUnit
        //                }).Where(x =>x.BusinessUnit.Status==true).OrderByDescending(x=>x.BusinessUnit.ID).AsEnumerable();
        //    this.DataList = a;
        //}

        //public void InitialDataLoad()
        //{
        //    db = new xOssContext();
        //    var a = (from Commercial_Bank in db.Commercial_Bank
        //             join Common_Country in db.Common_Country on Commercial_Bank.Common_CountryFK equals Common_Country.ID
        //             select new VmCommercial_Bank
        //             {
        //                 Common_Country = Common_Country,
        //                 Commercial_Bank = Commercial_Bank
        //             }).Where(x => x.Commercial_Bank.Active == true).OrderByDescending(x => x.Commercial_Bank.ID).AsEnumerable();
        //    this.DataList = a;

        //}
    }
}