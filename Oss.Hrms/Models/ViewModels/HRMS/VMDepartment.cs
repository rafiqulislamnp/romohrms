﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMDepartment:RootModel
    {
        [DisplayName("Department")]
        [Required(ErrorMessage = "Please Enter Department Name")]
        // [Index("UX_Country", 1, IsUnique = true)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Name should be minimum 3 characters and Maximum 50 characters")]
        public string DeptName { get; set; }

        [DisplayName("Department (Bangla)")]
       // [Required(ErrorMessage = "Please Enter Department Name Bangla")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Name should be minimum 3 characters and Maximum 50 characters")]
        public string DeptNameBangla { get; set; }

        [DisplayName("Unit Name")]
        [Required(ErrorMessage = "Please Select Unit")]
        public int Unit_SL { get; set; }
        public string Unit_Name { get; set; }
        public IEnumerable<VMDepartment> DatalistDepartments { get; set; }
    }
}