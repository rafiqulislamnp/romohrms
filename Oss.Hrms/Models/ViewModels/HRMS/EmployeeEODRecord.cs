﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class EmployeeEODRecord
    {
        public int EODRecordID { get; set; }
        [DisplayName("Employee Name")]
        public int EmployeeID { get; set; }
        [DisplayName("Salary Item")]
        public int EODRefFK { get; set; }
        public string SalaryItem { get; set; }
        [DisplayName("Amount"), Required(ErrorMessage ="Amount Cannot be null or empty.")]
        public decimal Amount { get; set; }
        public int Priority { get; set; }
        public DateTime date { get; set; }
    }
}