﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMEmployee : RootModel
    {
        [DisplayName("Card Title")]
        [Required(ErrorMessage = "Enter Card Title")]
        public string CardTitle { get; set; }

        [DisplayName("Punch Card No")]
        [Required(ErrorMessage = "Enter Punch Card No")]
        public string CardNo { get; set; }

       // [DisplayName("Employee ID")]
        [DisplayName("Card No")]
        [Required(ErrorMessage = "Enter Card No")]
        public string EmployeeIdentity { get; set; }

        [DisplayName("Name")]
        [Required(ErrorMessage = "Enter Name")]
        public string Name { get; set; }

        [DisplayName("Name(Bangla)")]
        [Required(ErrorMessage = "Enter Bangla Name")]
        public string Name_BN { get; set; }

        [DisplayName("Father Name")]
        [Required(ErrorMessage = "Enter Father Name")]
        public string Father_Name { get; set; }

        [DisplayName("Mother Name")]
        //[Required(ErrorMessage = "Enter Mother Name")]
        public string Mother_Name { get; set; }

        [DisplayName("DOB")]
        [Required(ErrorMessage = "Enter Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Enter Gender")]
        public string Gender { get; set; }

        [DisplayName("Country")]
        //[Required(ErrorMessage = "Enter Country")]
        public int Country_Id { get; set; }

        [DisplayName("Country")]
        public string Country_Name { get; set; }

        [DisplayName("Designation")]
        [Required(ErrorMessage = "Enter Designation")]
        public int Designation_Id { get; set; }

        [DisplayName("Designation")]
        public string Designation_Name { get; set; }

        [DisplayName("Unit")]
        //[Required(ErrorMessage = "Enter Unit")]
        public int Unit_Id { get; set; }
        [DisplayName("Unit")]
        public string Unit_Name { get; set; }

        [DisplayName("Department")]
        //[Required(ErrorMessage = "Enter Department")]
        public int Department_Id { get; set; }
        [DisplayName("Department")]
        public string Department_Name { get; set; }

        [DisplayName("Section")]
        [Required(ErrorMessage = "Enter Section")]
        public int Section_Id { get; set; }
        [DisplayName("Section ")]
        public string Section_Name { get; set; }

        [DisplayName("Line No.")]
        [Required(ErrorMessage = "Enter Line No.")]
        public int LineId { get; set; }
        [DisplayName("Line ")]
        public string Line { get; set; }

        [DisplayName("Business Unit")]
        public int Business_Unit_Id { get; set; }
        [DisplayName("Business Unit")]
        public string BU_Name { get; set; }

        [DisplayName("Blood Group")]
        //[Required(ErrorMessage = "Enter Blood Group")]
        public string Blood_Group { get; set; }
          
        [DisplayName("Joining Date")]
       // [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Enter Joining Date")]
        [DataType(DataType.Date)]
        public DateTime Joining_Date { get; set; }

        [DisplayName("JobPermanent Date")]
        [Required(ErrorMessage = "Enter Job Permanent Date")]
        [DataType(DataType.Date)]
        public DateTime JobPermanent_Date { get; set; }

        [DisplayName("Religion")]
        public string Religion { get; set; }

        [DisplayName("Mobile No.")]
        [Required(ErrorMessage = "Enter Mobile No.")]
        public string Mobile_No { get; set; }

        [DisplayName("Alt. Mobile No.")]
        public string Alt_Mobile_No { get; set; }

        [DisplayName("Alt. Person Name")]
        public string Alt_Person_Name { get; set; }

        [DisplayName("Alt. Person Contact No.")]
        public string Alt_Person_Contact_No { get; set; }

        [DisplayName("Alt. Person Address")]
        [StringLength(maximumLength: 100, MinimumLength = 5)]
        public string Alt_Person_Address { get; set; }

        [DisplayName("Emergency Contact No.")]
        public string Emergency_Contact_No { get; set; }

        [DisplayName("Office Contact")]
        public string Office_Contact { get; set; }

        [DisplayName("Land Phone")]
        public string Land_Phone { get; set; }

        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string Email_Id { get; set; } 

        [DisplayName("Employement Type")]
        public string Employement_Type { get; set; }

        [DisplayName("NID No.")]
       // [Required(ErrorMessage = "Enter NID.")]
        public string NID_No { get; set; }

        [DisplayName("Passport No.")]
        public string Passport_No { get; set; }

        [DisplayName("Present Address")]
        [StringLength(maximumLength: 100, MinimumLength = 5)]
        public string Present_Address { get; set; }

        [DisplayName("Permanent Address")]
       // [Required(ErrorMessage = "Enter Address")]
        [StringLength(maximumLength: 100, MinimumLength = 5)]
        public string Permanent_Address { get; set; }

        [DisplayName("Marital Status")]
        public string Marital_Status { get; set; }

        public string Photo { get; set; }
        public string ImagePath { get; set; }

        [DisplayName("Present Status")]
        public int Present_Status { get; set; }

        [DisplayName("Staff Type")]
        [Required(ErrorMessage = "Staff Type is Required.")]
        public string Staff_Type { get; set; }

        [Required(ErrorMessage ="Employee OT Eligibility should be required."),DisplayName("Over Time Eligibility")]
        public string Overtime_Eligibility { get; set; }

        [DisplayName("Grade")]
        [Required(ErrorMessage = "Grade is Required.")]
        public string Grade { get; set; }

        [DisplayName("Quit Date")]
        public DateTime QuitDate { get; set; }
        public string QuitDate1 { get; set; }

        public decimal BasicPay { get; set; }

        public ICollection<VMEmployee> VmEmployees { get; set; }
        public HRMS_Employee Employee { get; set; }
        public VM_HRMS_Upload_Document VmHrmsUploadDocument { get; set; }
        // public List<VMApplications> FormLists { get; set; }

        [DisplayName("District")]
        [Required(ErrorMessage = "District is Required.")]
        public int PermanentDistrict_ID { get; set; }
        [DisplayName("District")]
        [Required(ErrorMessage = "District is Required.")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PermanentDistrictName { get; set; }

        public District PermanentDistrict { get; set; }

        [DisplayName("District")]
        [Required(ErrorMessage = "District is Required.")]
        public int PresentDistrict_ID { get; set; }

        [DisplayName("District")]
        [Required(ErrorMessage = "District is Required.")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PresentDistrictName { get; set; }
        public District PresentDistrict { get; set; }

        [DisplayName("Police Station")]
        [Required(ErrorMessage = "Police Station is Required.")]
        public int PermanentPoliceStation_ID { get; set; }

        [DisplayName("Police Station")]
        [Required(ErrorMessage = "Police Station is Required.")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PermanentPoliceStationName { get; set; }

        public PoliceStation PermanentPoliceStation { get; set; }
        [DisplayName("Police Station")]
        [Required(ErrorMessage = "Police Station is Required.")]
        public int PresentPoliceStation_ID { get; set; }

        [DisplayName("Police Station")]
        [Required(ErrorMessage = "Police Station is Required.")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PresentPoliceStationName { get; set; }

        public PoliceStation PresentPoliceStation { get; set; }

        [DisplayName("Post Office")]
        [Required(ErrorMessage = "Post Office is Required.")]
        public int PermanentPostOffice_ID { get; set; }

        [DisplayName("Post Office")]
        [Required(ErrorMessage = "Post Office is Required.")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PermanentPostOfficeName { get; set; }

        public PostOffice PermanentPostOffice { get; set; }
        [DisplayName("Post Office")]
        [Required(ErrorMessage = "Post Office is Required.")]
        public int PresentPostOffice_ID { get; set; }

        [DisplayName("Post Office")]
        [Required(ErrorMessage = "Post Office is Required.")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PresentPostOfficeName { get; set; }

        public PostOffice PresentPostOffice { get; set; }

        [DisplayName("Village")]
        [Required(ErrorMessage = "Village is Required.")]
        public int PermanentVillage_ID { get; set; }

        [DisplayName("Village")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        [Required(ErrorMessage = "Village is Required.")]
        public string PermanentVillageName { get; set; }
        public Village PermanentVillage { get; set; }
        [DisplayName("Village")]
        [Required(ErrorMessage = "Village is Required.")]
        public int PresentVillage_ID { get; set; }

        [DisplayName("Village")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        [Required(ErrorMessage = "Village is Required.")]
        public string PresentVillageName { get; set; }

        public Village PresentVillage { get; set; }
        
        HrmsContext db = new HrmsContext();

        /// <summary>
        /// Collective List of all employee.....
        /// </summary>
        public void ListVmEmployees(int deptid,int status)
        {
            if (deptid == 0)
            {
                var employees = (from em in db.Employees
                                 join C in db.Countries on em.Country_Id equals C.ID
                                 join d in db.Designations on em.Designation_Id equals d.ID
                                 join u in db.Units on em.Unit_Id equals u.ID
                                 join dept in db.Departments on em.Department_Id equals dept.ID
                                 join s in db.Sections on em.Section_Id equals s.ID
                                 join l in db.Lines on em.LineId equals l.ID
                                 join bu in db.BusinessUnits on em.Business_Unit_Id equals bu.ID
                                 where (em.Present_Status == status)
                                 select new VMEmployee()
                                 {
                                     ID = em.ID,
                                     CardTitle = em.CardTitle,
                                     CardNo = em.CardNo,
                                     EmployeeIdentity = em.EmployeeIdentity,
                                     Name = em.Name,
                                     Name_BN = em.Name_BN,
                                     Father_Name = em.Father_Name,
                                     Mother_Name = em.Mother_Name,
                                     DOB = em.DOB,
                                     Gender = em.Gender,
                                     Designation_Name = d.Name,
                                     Country_Name = C.Name,
                                     BU_Name = bu.Name,
                                     Unit_Name = u.UnitName,
                                     Department_Name = dept.DeptName,
                                     Section_Name = s.SectionName,
                                     Line = l.Line,
                                     Blood_Group = em.Blood_Group,
                                     Joining_Date = em.Joining_Date, //(DateTime)
                                     JobPermanent_Date = em.JobPermanent_Date,
                                     Religion = em.Religion,
                                     Mobile_No = em.Mobile_No,
                                     Alt_Mobile_No = em.Alt_Mobile_No,
                                     Alt_Person_Name = em.Alt_Person_Name,
                                     Alt_Person_Contact_No = em.Alt_Person_Contact_No,
                                     Alt_Person_Address = em.Alt_Person_Address,
                                     Emergency_Contact_No = em.Emergency_Contact_No,
                                     Office_Contact = em.Office_Contact,
                                     Land_Phone = em.Land_Phone,
                                     Email_Id = em.Email,
                                     Employement_Type = em.Employement_Type,
                                     NID_No = em.NID_No,
                                     Passport_No = em.Passport_No,
                                     Present_Address = em.Present_Address,
                                     Permanent_Address = em.Permanent_Address,
                                     Marital_Status = em.Marital_Status,
                                     Photo = em.Photo
                                 }).OrderBy(a => a.CardNo).ToList();

                this.VmEmployees = employees;
            }
            else
            {
                var employees = (from em in db.Employees
                                 join C in db.Countries on em.Country_Id equals C.ID
                                 join d in db.Designations on em.Designation_Id equals d.ID
                                 join u in db.Units on em.Unit_Id equals u.ID
                                 join dept in db.Departments on em.Department_Id equals dept.ID
                                 join s in db.Sections on em.Section_Id equals s.ID
                                 join l in db.Lines on em.LineId equals l.ID
                                 join bu in db.BusinessUnits on em.Business_Unit_Id equals bu.ID
                                 where (em.Present_Status == status && em.Department_Id == deptid)
                                 select new VMEmployee()
                                 {
                                     ID = em.ID,
                                     CardTitle = em.CardTitle,
                                     CardNo = em.CardNo,
                                     EmployeeIdentity = em.EmployeeIdentity,
                                     Name = em.Name,
                                     Name_BN = em.Name_BN,
                                     Father_Name = em.Father_Name,
                                     Mother_Name = em.Mother_Name,
                                     DOB = em.DOB,
                                     Gender = em.Gender,
                                     Designation_Name = d.Name,
                                     Country_Name = C.Name,
                                     BU_Name = bu.Name,
                                     Unit_Name = u.UnitName,
                                     Department_Name = dept.DeptName,
                                     Section_Name = s.SectionName,
                                     Line = l.Line,
                                     Blood_Group = em.Blood_Group,
                                     Joining_Date = (DateTime)em.Joining_Date,
                                     JobPermanent_Date = em.JobPermanent_Date,
                                     Religion = em.Religion,
                                     Mobile_No = em.Mobile_No,
                                     Alt_Mobile_No = em.Alt_Mobile_No,
                                     Alt_Person_Name = em.Alt_Person_Name,
                                     Alt_Person_Contact_No = em.Alt_Person_Contact_No,
                                     Alt_Person_Address = em.Alt_Person_Address,
                                     Emergency_Contact_No = em.Emergency_Contact_No,
                                     Office_Contact = em.Office_Contact,
                                     Land_Phone = em.Land_Phone,
                                     Email_Id = em.Email,
                                     Employement_Type = em.Employement_Type,
                                     NID_No = em.NID_No,
                                     Passport_No = em.Passport_No,
                                     Present_Address = em.Present_Address,
                                     Permanent_Address = em.Permanent_Address,
                                     Marital_Status = em.Marital_Status,
                                     Photo = em.Photo
                                 }).OrderBy(a => a.CardNo).ToList();

                this.VmEmployees = employees;
            }
            
        }
        
        /// <summary>
        /// Collective List of all employee.....
        /// </summary>
        public VMEmployee DetailsVmEmployeesIdWise(int? id)
        {
            var employee = (from em in db.Employees
                join C in db.Countries on em.Country_Id equals C.ID
                join d in db.Designations on em.Designation_Id equals d.ID
                join u in db.Units on em.Unit_Id equals u.ID
                join dept in db.Departments on em.Department_Id equals dept.ID
                join s in db.Sections on em.Section_Id equals s.ID
                join l in db.Lines on em.LineId equals l.ID
                join bu in db.BusinessUnits on em.Business_Unit_Id equals bu.ID
                //join v in db.Village on em.PresentVillage_ID equals v.ID
                //join v1 in db.Village on em.PermanentVillage_ID equals v1.ID
                //join p in db.PostOffice on em.PresentPostOffice_ID equals p.ID
                //join p1 in db.PostOffice on em.PermanentPostOffice_ID equals p1.ID
                //join po in db.PoliceStation on em.PresentPoliceStation_ID equals po.ID
                //join po1 in db.PoliceStation on em.PermanentPoliceStation_ID equals po1.ID
                //join di in db.District on em.PresentDistrict_ID equals di.ID
                //join di1 in db.District on em.PermanentDistrict_ID equals di1.ID
                            where em.ID == id

                select new VMEmployee()
                {
                    ID = em.ID,
                    EmployeeIdentity = em.EmployeeIdentity, Name = em.Name, CardTitle = em.CardTitle, CardNo = em.CardNo,
                    Name_BN = em.Name_BN, Father_Name = em.Father_Name, Mother_Name = em.Mother_Name,
                    DOB = em.DOB, Gender = em.Gender, Designation_Name = d.Name,
                    Country_Name = C.Name, BU_Name = bu.Name, Unit_Name = u.UnitName,
                    Department_Name = dept.DeptName, Section_Name = s.SectionName,Line = l.Line,
                    Blood_Group = em.Blood_Group, Joining_Date = (DateTime)em.Joining_Date,
                    JobPermanent_Date=em.JobPermanent_Date,//==null? (DateTime)em.Joining_Date : em.JobPermanent_Date,
                    Religion = em.Religion, Mobile_No = em.Mobile_No, Alt_Mobile_No = em.Alt_Mobile_No,
                    Alt_Person_Name = em.Alt_Person_Name, Alt_Person_Contact_No = em.Alt_Person_Contact_No,
                    Alt_Person_Address = em.Alt_Person_Address, Emergency_Contact_No = em.Emergency_Contact_No,
                    Office_Contact = em.Office_Contact, Land_Phone = em.Land_Phone, Email_Id = em.Email,
                    Employement_Type = em.Employement_Type, NID_No = em.NID_No, Passport_No = em.Passport_No,
                    Present_Address = em.Present_Address, Permanent_Address = em.Permanent_Address,
                    Staff_Type=em.Staff_Type,
                    Marital_Status = em.Marital_Status, Photo = em.Photo,Overtime_Eligibility = em.Overtime_Eligibility,Grade = em.Grade,
                    PresentVillageName = em.PresentVillage_ID==null? string.Empty: db.Village.FirstOrDefault(x=>x.ID==em.PresentVillage_ID).VillageName,
                    PresentPostOfficeName = em.PresentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == em.PresentPostOffice_ID).PostOfficeName,
                    PresentPoliceStationName = em.PresentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == em.PresentPoliceStation_ID).PoliceStationName,
                    PresentDistrictName = em.PresentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == em.PresentDistrict_ID).DistrictName,
                    PermanentVillageName = em.PermanentVillage_ID == null ? string.Empty : db.Village.FirstOrDefault(x => x.ID == em.PermanentVillage_ID).VillageName,
                    PermanentPostOfficeName = em.PermanentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == em.PermanentPostOffice_ID).PostOfficeName,
                    PermanentPoliceStationName = em.PermanentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == em.PermanentPoliceStation_ID).PoliceStationName,
                    PermanentDistrictName = em.PermanentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == em.PermanentDistrict_ID).DistrictName,
                }).FirstOrDefault();

            return employee;
        }

        public void AddPayrollRecord(HRMS_Employee emp)
        {
            HrmsContext db = new HrmsContext();
            var vData = db.HrmsEodReferences.Where(a => a.Regular == true && a.Status == true).OrderBy(a => a.priority).ToList();

            foreach (var item in vData)
            {
                HRMS_EodRecord model = new HRMS_EodRecord
                {
                    EmployeeId = emp.ID,
                    Eod_RefFk = item.ID,
                    ActualAmount = 0,
                    EffectiveDate = DateTime.Now,
                    Entry_Date = DateTime.Now,
                    Entry_By = emp.Entry_By
                };

                db.HrmsEodRecords.Add(model);
            }
        }

        public VMEmployee GetEmployeeDetails(int EmployeeID)
        {
            var employee = (from em in db.Employees
                            join d in db.Designations on em.Designation_Id equals d.ID
                            join u in db.Units on em.Unit_Id equals u.ID
                            join dept in db.Departments on em.Department_Id equals dept.ID
                            join s in db.Sections on em.Section_Id equals s.ID
                            join l in db.Lines on em.LineId equals l.ID
                            join bu in db.BusinessUnits on em.Business_Unit_Id equals bu.ID
                            where em.ID == EmployeeID
                            select new VMEmployee
                            {
                                ID = em.ID,
                                EmployeeIdentity = em.EmployeeIdentity,
                                Name = em.Name,
                                CardTitle = em.CardTitle,
                                CardNo = em.CardNo,
                                Name_BN = em.Name_BN,
                                
                                DOB = em.DOB,
                                Gender = em.Gender,
                                Designation_Name = d.Name,
                                BU_Name = bu.Name,
                                Unit_Name = u.UnitName,
                                Department_Name = dept.DeptName,
                                Section_Name = s.SectionName,
                                Line = l.Line,
                                Grade = em.Grade,
                                Joining_Date = (DateTime)em.Joining_Date,
                                JobPermanent_Date= em.JobPermanent_Date,
                                Mobile_No = em.Mobile_No,
                                
                                Email_Id = em.Email,
                                Marital_Status = em.Marital_Status,
                                Photo = em.Photo,
                                Overtime_Eligibility = em.Overtime_Eligibility                               
                            }).FirstOrDefault();

            return employee;
        }

        public List<VMEmployee> GetEmployeeByDesignationID(int DesignationID, decimal BasicSalary)
        {
            var vData = (from o in db.Employees
                        join p in db.Designations on o.Designation_Id equals p.ID
                        where o.Designation_Id == DesignationID && o.Status == true
                        select new
                        {
                            ID=o.ID,
                            Name=o.Name,
                            BanglaName=o.Name_BN,
                            CardNo=o.CardNo,
                            CardTitle=o.CardTitle,
                            Grade=o.Grade,
                            Designation=p.Name
                        }).OrderBy(a=>a.Grade);
            List<VMEmployee> lst = new List<VMEmployee>();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    VMEmployee model = new VMEmployee();
                    model.ID = v.ID;
                    model.Name = v.Name;
                    model.Name_BN = v.BanglaName;
                    model.CardTitle = v.CardTitle;
                    model.CardNo = v.CardNo;
                    model.Grade = v.Grade;
                    model.Designation_Name = v.Designation;
                    model.BasicPay = BasicSalary;
                    lst.Add(model);
                }
            }
            return lst;
        }
    }
}