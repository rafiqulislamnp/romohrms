﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Child : RootModel
    {
        [DisplayName("Card No")]
       // [DisplayName("Employee ID")]
        public int Employee_Id { get; set; }
       // [DisplayName("Employee ID")]
        [DisplayName("Card No")]
        public string Employee_Identity { get; set; }
        public string Name { get; set; }

        [DisplayName("Child Name")]
        //[Required(ErrorMessage = "Enter Name")]
       // [StringLength(50,MinimumLength = 5, ErrorMessage = "Enter minimum 5 characters.")]
        public string Child_Name { get; set; }
        [DisplayName("Blood Group")]
        //[Required(ErrorMessage = "Enter Blood Group")]
        public string Child_Blood_Group { get; set; }
        [DisplayName("Date of Birth")]
       // [Required(ErrorMessage = "Enter Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime Child_DOB { get; set; }

        public ICollection<VM_HRMS_Child> Children { get; set; }
        public HRMS_Employee Employee { get; internal set; }

        private HrmsContext db = new HrmsContext();

        public void GetChildren()
        {
            var children = (from ch in db.HrmsChildren
                join em in db.Employees on ch.Employee_Id equals em.ID
                 select new VM_HRMS_Child()
                 {
                     ID = ch.ID, Employee_Id = em.ID, Employee_Identity = em.EmployeeIdentity, 
                     Child_Name = ch.Child_Name, Child_DOB = ch.Child_DOB, Child_Blood_Group = ch.Child_Blood_Group
                 }).ToList();
            Children = children;
        }
    }
}