﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Salary : RootModel
    {
        public VM_HRMS_Salary VMSalary { get; set; }
        [DisplayName("Card No")]
        //[DisplayName("Employee ID")]
        public int Employee_Id { get; set; }
        [DisplayName("Card No")]
        // [DisplayName("Employee ID")]
        public string Employee_Identity { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Salary Grade required"), DisplayName("Salary Grade")]
        public string Salary_Grade { get; set; }
        [DisplayName("Joining Date")]
        [DataType(DataType.Date)]
        public DateTime Joining_Date { get; set; }
        [DisplayName("Joining Salary")]
        public decimal Joining_Salary { get; set; }
        [DisplayName("Payment Mode")]
        public string Payment_Mode { get; set; }
        [DisplayName("Bank Name")]
        public string Bank_Name { get; set; }
        [DisplayName("Account No.")]
        public string Account_No { get; set; }
        [DisplayName("Current Salary")]
        public decimal Current_Salary { get; set; }
        public decimal BasicPay { get; set; }
        public ICollection<VM_HRMS_Salary> Salaries { get; set; }
        public List<EmployeeEODRecord> EODRecordList { get; set; }
        public HRMS_Employee Employee { get; internal set; }

        private HrmsContext db = new HrmsContext();

        public void GetSalaryList()
        {
            var salary = (from s in db.HrmsSalaries
                          join em in db.Employees on s.Employee_Id equals em.ID
                          select new VM_HRMS_Salary()
                          {
                              ID = s.ID,
                              Employee_Id = em.ID,
                              Employee_Identity = em.EmployeeIdentity,
                              //Salary_Grade = s.Salary_Grade,
                              Joining_Salary = s.Joining_Salary,
                              Current_Salary = s.Current_Salary
                          }).ToList();

            Salaries = salary;
        }

        public void GetEmployeeEODRecord(int EmployeeID)
        {
            db = new HrmsContext();
            List<EmployeeEODRecord> lst = new List<EmployeeEODRecord>();
            var eodref = db.HrmsEodReferences.Where(a => a.Regular == true).OrderBy(a => a.priority).ToList();

            foreach (var v in eodref)
            {
                var vData = (from t1 in db.HrmsEodRecords
                             join t2 in db.HrmsEodReferences on t1.Eod_RefFk equals t2.ID
                             where t1.EmployeeId == EmployeeID && t1.Eod_RefFk == v.ID && t2.Finish == 12
                             select new EmployeeEODRecord
                             {
                                 EODRecordID = t1.ID,
                                 EODRefFK = t2.ID,
                                 SalaryItem = t2.Name,
                                 Amount = t1.ActualAmount,
                                 EmployeeID = EmployeeID,
                                 Priority = t2.priority
                             }).OrderByDescending(a => a.EODRecordID).ToList();
                if (vData.Any())
                {
                    lst.Add(vData.FirstOrDefault());
                }

            }

            this.EODRecordList = lst;

        }

        public void GetBasicPay(int EmployeeID, int EODReferenceID)
        {
            var vData = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Eod_RefFk == EODReferenceID && a.Status == true);
            if (vData.Any())
            {
                this.BasicPay = vData.FirstOrDefault().ActualAmount;
            }

        }
        public string GetReferenceName(int id)
        {
            db = new HrmsContext();
            var data = db.HrmsEodReferences.Find(id);
            if (data != null) return data.Name;
            else return "";
        }
        public string GetGradeName(int id)
        {
            db = new HrmsContext();
            var data = db.SalaryGrade.Find(id);
            if (data != null) return data.Name;
            else return "";
        }
    }

    public class HrmsPayrollSalaryWithGrade
    {
        public SalaryGrade SalaryGrade { get; set; }
        public HRMS_Salary HRMS_Salary { get; set; }
    }
    public class HrmsPayrollSalaryWithEod
    {
        public HRMS_EodReference HRMS_EodReference { get; set; }
        public HRMS_EodRecord HRMS_EodRecord { get; set; }
    }
}