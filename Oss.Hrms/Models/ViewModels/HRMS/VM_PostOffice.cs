﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_PostOffice : RootModel
    {
        [DisplayName("Post Office Name")]
        [Required(ErrorMessage = "Please Select Post Office")]
        public string PostOfficeName { get; set; }

        [DisplayName("Police Station Name")]
       // [Required(ErrorMessage = "Please Select Police Station")]
        public int PoliceStation_ID { get; set; }

        [DisplayName("Police Station Name")]
        [Required(ErrorMessage = "Please Select Police Station")]
        public string PoliceStationName { get; set; }


        [DisplayName("District Name")]
        // [Required(ErrorMessage = "Please Select Police Station")]
        public int District_ID { get; set; }

        [DisplayName("District Name")]
        [Required(ErrorMessage = "Please Select District")]
        public string DistrictName { get; set; }

        public ICollection<VM_PostOffice> CollectionVmDistrict { get; set; }
        HrmsContext db = new HrmsContext();

        public void GetPostOfficeList()
        {
            var postoffice = (from t1 in db.District
                join t2 in db.PoliceStation on t1.ID equals t2.District_ID
                select new
                {
                    d = t1,
                    po = t2,
                    post = db.PostOffice.Where(x => x.PoliceStation_ID == t2.ID).ToList()
                }).ToList();
            List<VM_PostOffice> lst = new List<VM_PostOffice>();
            foreach (var p in postoffice)
            {
                if (p.post.Any())
                {
                    foreach (var v1 in p.post)
                    {
                        VM_PostOffice post=new VM_PostOffice();
                        post.ID = v1.ID;
                        post.PostOfficeName = v1.PostOfficeName;
                        post.PoliceStationName = p.po.PoliceStationName;
                        post.PoliceStation_ID = p.po.ID;
                        post.DistrictName = p.d.DistrictName;
                        post.District_ID = p.d.ID;
                        lst.Add(post);
                    }
                }
                else
                {
                    VM_PostOffice post = new VM_PostOffice();
                    post.PoliceStationName = p.po.PoliceStationName;
                    post.PoliceStation_ID = p.po.ID;
                    post.DistrictName = p.d.DistrictName;
                    post.District_ID = p.d.ID;
                    lst.Add(post);
                }
            }
            //// 
            // (select )
            //// join t3 in db.District on t2.District_ID equals t3.ID
            // select new VM_PostOffice()
            // {
            //     ID = t1.ID,
            //     PostOfficeName = t1.PostOfficeName,
            //     PoliceStationName = t2.PoliceStationName,
            //     DistrictName = t3.DistrictName,
            //     District_ID = t3.ID
            // }).ToList();

            CollectionVmDistrict = lst;
        }

        public VM_PostOffice GetSpecificPostOfficeforAddPostOffice(int id)
        {
            var p = (from t1 in db.PoliceStation 
                join t2 in db.District on t1.District_ID equals t2.ID where t1.ID==id
                select new VM_PostOffice()
                {
                    PoliceStation_ID = t1.ID,
                    PoliceStationName = t1.PoliceStationName,
                    DistrictName = t2.DistrictName,
                    District_ID = t2.ID
                }).FirstOrDefault();
            return p;
        }

        public VM_PostOffice GetSpecificPostOffice(int id)
        {
            var p = (from t1 in db.PostOffice
                join t2 in db.PoliceStation on t1.PoliceStation_ID equals t2.ID
                join t3 in db.District on t2.District_ID equals t3.ID
                select new VM_PostOffice()
                {
                    ID = t1.ID,
                    PostOfficeName = t1.PostOfficeName,
                    PoliceStationName = t2.PoliceStationName,
                    DistrictName = t3.DistrictName,
                    District_ID = t3.ID
                }).FirstOrDefault(x => x.ID == id);
            return p;
        }
    }
}