﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.ViewModels.Reports;
using Oss.Hrms.Models.Services;
using System.ComponentModel.DataAnnotations.Schema;
using Oss.Hrms.Helper;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMApplications
    {
        #region ConstantValue
        private decimal Medical = 600;
        private decimal Food = 900;
        private decimal Transport = 350;
        private decimal FoodHealthTrans = 1850;
        private decimal HouseRate = (decimal)0.5;
        private int IncrimentRate = 5;
        private decimal DaysOfMonth = 30;
        #endregion
        #region Properties

        public int ID { get; set; }
        public string Name { get; set; }
        public string EngName { get; set; }
        [Display(Name = "Husband Name BN")]
        public string HusbandName { get; set; }
        [Display(Name = "Land Owner BN")]
        public string LandOwner { get; set; }
        [Display(Name = "Home BN")]
        public string Home { get; set; }
        [Display(Name = "PostOffice BN")]
        public string PostOffice { get; set; }
        [Display(Name = "Thana BN")]
        public string Thana { get; set; }
        [Display(Name = "District BN")]
        public string District { get; set; }
        public DateTime JoinningDate { get; set; }
        public DateTime JobPermanentDate { get; set; }
        public DateTime QuitDate { get; set; }
        public string EmployeeIDNo { get; set; }
        public string Designation { get; set; }
        public string EngDesignation { get; set; }
        public string EmpMail { get; set; }
        public string EmpPhone { get; set; }
        public string Section { get; set; }
        public string Grade { get; set; }
        public string Department { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string HasbandName { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string Gender { get; set; }
        public string CardNo { get; set; }
        public string Line { get; set; }
        public string Unit { get; set; }

        [Display(Name = "Physical Eligibility")]
        public string PhysicalEligibility { get; set; }

        [Display(Name = "Birth Mark")]
        public string BirthMark { get; set; }

        [Display(Name = "Welfare Officer")]
        public string WelfareOfficer { get; set; }

        public string LawStep { get; set; }

        public string MaritalStatus { get; set; }

        [Display(Name = "EDD Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime EDD { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ResignDate { get; set; }

        [Display(Name = "LMP Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime LMP { get; set; }

        [Display(Name = "Designation")]
        public int ChangeDesignation { get; set; }

        [Display(Name = "Section")]
        public int ChangeSection { get; set; }

        [Display(Name = "Grade")]
        public int ChangeGrade { get; set; }

        [Display(Name = "Department")]
        public int ChangeDepartment { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Display(Name = "Child Gender")]
        [StringLength(100)]
        public string ChildGender_BN { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Display(Name = "Nominate Person:")]
        [StringLength(100)]
        public string NominatePerson_BN { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Display(Name = "Relation:")]
        [StringLength(100)]
        public string Relation_BN { get; set; }

        [Display(Name = "From Date:")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Display(Name = "To Date:")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Application Date:")]
        public DateTime ApplicationDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date:")]
        public DateTime StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Payment Date:")]
        public DateTime PaymentDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date:")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Basic Pay")]
        [Required(ErrorMessage = "Basic Pay is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        public decimal BasicPay { get; set; }

        public int Age { get; set; }
        public int PregnantWeek { get; set; }
        public DateTime DOB { get; set; }

        public decimal ActualAmount { get; set; }

        public int ButtonID { get; set; }

        public DateTime TotalJobAge { get; set; }

        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public int PaidDays { get; set; }


        public int TotalYear { get; set; }
        public int TotalMonth { get; set; }
        public int TotalDay { get; set; }

        [Display(Name = "Total Pay")]
        public string TotalPay { get; set; }

        [Display(Name = "Daily Pay")]
        public string DailyPay { get; set; }

        [Display(Name = "Payment Type")]
        public string FirstInstallment { get; set; }

        [Display(Name = "Present Village")]
        public string PresentVill { get; set; }
        [Display(Name = "Present PostOffice")]
        public string PresentPostOffice { get; set; }
        [Display(Name = "Present Thana")]
        public string PresentThana { get; set; }
        [Display(Name = "Present District")]
        public string PresentDistrict { get; set; }
        [Display(Name = "Permanent Village")]
        public string PermanentVill { get; set; }
        [Display(Name = "Permanent PostOffice")]
        public string PermanentPostOffice { get; set; }
        [Display(Name = "Permanent Thana")]
        public string PermanentThana { get; set; }
        [Display(Name = "Permanent District")]
        public string PermanentDistrict { get; set; }

        public string NomineName { get; set; }
        public string NominePresentAddress { get; set; }
        public string NominePermanentAddress { get; set; }
        public string NomineFatherName { get; set; }
        public string NomineMotherName { get; set; }
        public string NomineRelation { get; set; }

        [Display(Name = "Relation With Nominate Person")]
        public string RelationNoPerson { get; set; }
        public List<ReportDocType> ReportSource { get; set; }

        public decimal BasicAmount { get; set; }
        public decimal TotalAmount { get; set; }

        public decimal HouseAmount { get; set; }

        public decimal IncrementAmount { get; set; }

        public decimal IncrementSalary { get; set; }

        public decimal OtherAmount { get; set; }
        [Display(Name = "Payment Type")]
        public string PaymentType { get; set; }
        public decimal GrossPay { get; set; }
        public int PresentDay { get; set; }
        public int AbsentDay { get; set; }
        public int OffDay { get; set; }

        public decimal AttendanceBonus { get; set; }
        public decimal AttendanceTaka { get; set; }
        public decimal OTHour { get; set; }
        public decimal OTTaka { get; set; }
        public decimal MonthlyWages { get; set; }
        public int PayrollId { get; set; }
        public string Title { get; set; }
        #endregion
        #region Report
        public void LoadData(VMApplications model)
        {
            model.BasicPay = GetEmployeeBasic(model.ID);

            EmployeeBasicInfo(model.ID);

            var v = MaternityBenefit(model.ID);

            DateTime ZeroDate = new DateTime(1900, 01, 01, 00, 00, 00);
            CalculateJobAge(model.JoinningDate, model.EDD);
            decimal houseRent = model.BasicPay * this.HouseRate;
            decimal roundhouseRent = Math.Round(houseRent, 0, MidpointRounding.AwayFromZero);
            decimal IncrementTotal = GetIncrementAmount(model.ID);

            if (ZeroDate.Equals(model.ResignDate) || ZeroDate.Equals(model.JoinningDate))
            {
                this.TotalYear = 0;
                this.TotalMonth = 0;
                this.TotalDay = 0;
            }
            else
            {
                TotalWorkingAge(model.JoinningDate, ResignDate);
            }
            List<ReportDocType> lst = new List<ReportDocType>();
            ReportDocType report = new ReportDocType();
            report.HeaderTop1 = model.PresentVill;
            report.HeaderTop2 = model.PresentPostOffice;
            report.HeaderTop3 = model.PresentThana;
            report.HeaderTop4 = model.PresentDistrict;
            report.HeaderTop5 = model.PermanentVill;
            report.HeaderTop6 = model.PermanentPostOffice;
            report.HeaderTop7 = model.PermanentThana;
            report.HeaderTop8 = model.PermanentDistrict;
            report.HeaderTop8 = model.PresentAddress;
            report.HeaderTop9 = model.PermanentAddress;
            report.HeaderLeft1 = model.Name;
            report.HeaderLeft2 = model.EmployeeIDNo;
            report.HeaderLeft3 = model.Designation;
            report.HeaderLeft4 = model.Section;
            report.HeaderLeft5 = model.Department;
            report.HeaderLeft6 = model.Grade;
            report.HeaderLeft7 = model.Line;
            report.HeaderLeft8 = model.EngName;
            report.HeaderLeft9 = model.WelfareOfficer;
            report.HeaderLeft10 = model.LawStep;
            report.HeaderLeft11 = model.MotherName;
            report.HeaderLeft12 = model.FatherName;
            report.HeaderLeft13 = model.CardNo;

            report.HeaderMiddle6 = model.Unit;
            report.HeaderMiddle1 = model.Age.ToString();
            report.HeaderMiddle2 = Helpers.ShortDateString(model.EDD);
            report.HeaderMiddle3 = model.PregnantWeek.ToString();
            report.HeaderMiddle4 = model.EngDesignation;
            report.HeaderMiddle5 = Helpers.ShortDateString(model.LMP);

            report.HeaderRight1 = GetName(model.ChangeDesignation, 1);
            report.HeaderRight2 = GetName(model.ChangeSection, 2);
            //report.HeaderRight3 = GetName(model.ChangeDesignation, 1);
            //report.HeaderRight4 = GetName(model.ChangeGrade, 4);

            report.Body1 = Helpers.ShortDateString(model.ApplicationDate);
            report.Body2 = Helpers.ShortDateString(model.FromDate);
            report.Body3 = Helpers.ShortDateString(model.ToDate);
            report.Body4 = Helpers.ShortDateString(model.StartDate);
            report.Body5 = Helpers.ShortDateString(model.EndDate);
            report.Body6 = model.ChildGender_BN;
            report.Body7 = model.NominatePerson_BN;
            report.Body8 = model.Relation_BN;
            report.Body9 = Helpers.ShortDateString(model.JoinningDate);
            report.Body10 = model.EngName;
            report.Body11 = model.Age.ToString();
            report.Body12 = model.Year.ToString();
            report.Body13 = model.Month.ToString();
            report.Body14 = model.Day.ToString();
            report.Body15 = model.TotalPay;
            report.Body16 = model.DailyPay;
            report.Body17 = model.FirstInstallment;
            report.Body18 = Helpers.ShortDateString(model.DOB);
            report.Body19 = model.Gender;
            report.Body20 = model.PhysicalEligibility;
            report.Body21 = model.BirthMark;
            report.Body22 = model.TotalYear.ToString();
            report.Body23 = model.TotalMonth.ToString();
            report.Body24 = model.TotalDay.ToString();
            report.Body25 = Helpers.ShortDateString(model.ResignDate);

            report.SubBody1 = Math.Round(model.BasicPay).ToString();
            report.SubBody2 = roundhouseRent.ToString();
            report.SubBody3 = this.Medical.ToString();
            report.SubBody4 = this.Transport.ToString();
            report.SubBody5 = this.Food.ToString();
            report.SubBody6 = Math.Round(model.BasicPay + roundhouseRent + this.FoodHealthTrans).ToString();
            report.SubBody7 = model.Name;
            report.SubBody8 = model.EmployeeIDNo;
            report.SubBody9 = model.RelationNoPerson;
            report.SubBody10 = Math.Round(model.BasicPay).ToString();
            report.SubBody11 = roundhouseRent.ToString();
            report.SubBody12 = Math.Round(model.BasicPay + roundhouseRent + this.FoodHealthTrans).ToString();
            report.SubBody13 = Helpers.IncreamentDate(model.JoinningDate);
            report.SubBody14 = IncrementTotal.ToString();
            report.SubBody15 = model.NomineName;
            report.SubBody16 = "56";
            report.SubBody17 = "2019-01-28";
            report.SubBody18 = "11056";
            report.SubBody19 = model.PaymentType;
            report.Footer1 = model.Name;
            report.Footer2 = model.HusbandName;
            report.Footer3 = model.LandOwner;
            report.Footer4 = model.Home;
            report.Footer5 = model.PostOffice;
            report.Footer6 = model.Thana;
            report.Footer7 = model.District;

            report.Duplicate1 = model.Name;
            report.Duplicate2 = model.Designation;
            report.Duplicate3 = model.EmployeeIDNo;
            report.Duplicate4 = model.Section;
            report.Duplicate5 = model.Line;
            report.Duplicate6 = model.WelfareOfficer;

            //Nomine Call AppoinmentLetter
            report.Duplicate7 = model.Name;
            report.Duplicate8 = model.FatherName;
            report.Duplicate9 = model.MotherName;

            report.Duplicate10 = model.PresentAddress;
            report.Duplicate11 = model.PermanentDistrict;

            report.Duplicate12 = model.NomineRelation;
            report.Duplicate13 = model.PermanentVill;
            report.Duplicate14 = model.PermanentPostOffice;
            report.Duplicate15 = model.PermanentThana;
            report.Duplicate16 = model.PermanentDistrict;
            report.Duplicate17 = model.Designation;
            report.Duplicate18 = GetName(model.ChangeDesignation, 1);
            report.Duplicate19 = GetName(model.ChangeSection, 2);
            report.Duplicate20 = model.Age.ToString();
            report.Duplicate21 = model.PresentAddress;
            report.Duplicate22 = Helpers.ShortDateString(model.ApplicationDate);
            report.Duplicate23 = model.BirthMark;
            report.Duplicate24 = model.PresentAddress;
            report.Duplicate25 = model.PermanentAddress;
            report.ReportDocTypeList = new List<ReportDocType>();

            foreach (var e in v)
            {
                ReportDocType s = new ReportDocType();
                s.SubBody27 = e.FromDate.ToString("MMM") + " " + e.FromDate.Year;

                report.ReportDocTypeList.Add(s);
            }

            lst.Add(report);
            this.ReportSource = lst;
        }

        #endregion
        #region Other Methods

        public void EmployeeBasicInfo(int EmployeeID)
        {
            HrmsContext db = new HrmsContext();

            var vData = (from t1 in db.Employees
                         join t2 in db.Designations on t1.Designation_Id equals t2.ID
                         join t3 in db.Sections on t1.Section_Id equals t3.ID
                         join t4 in db.Departments on t1.Department_Id equals t4.ID
                         join t5 in db.Lines on t1.LineId equals t5.ID
                         join t6 in db.Units on t1.Unit_Id equals t6.ID
                         where t1.ID == EmployeeID
                         select new
                         {
                             Name = t1.Name_BN,
                             EmployeeIDNo = t1.EmployeeIdentity,
                             Designation = t2.DesigNameBangla,
                             Section = t3.SectionName,
                             Grade = t1.Grade,
                             Department = t4.DeptNameBangla,
                             Line = t5.Line,
                             JoinningDate = t1.Joining_Date,
                             DOB = t1.DOB,
                             EngName = t1.Name,
                             EngDesignation = t2.Name,
                             Unit = t6.UnitName,
                             MotherName = t1.Mother_Name,
                             FatherName = t1.Father_Name,
                             CardNo = t1.EmployeeIdentity,
                             Gender = t1.Gender,
                             ResignDate = t1.QuitDate,
                             QuitDate = t1.QuitDate,
                             PresentVillageName = t1.PresentVillage_ID == null ? string.Empty : db.Village.FirstOrDefault(x => x.ID == t1.PresentVillage_ID).VillageName,
                             PresentPostOfficeName = t1.PresentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == t1.PresentPostOffice_ID).PostOfficeName,
                             PresentPoliceStationName = t1.PresentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == t1.PresentPoliceStation_ID).PoliceStationName,
                             PresentDistrictName = t1.PresentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == t1.PresentDistrict_ID).DistrictName,
                             PermanentVillageName = t1.PermanentVillage_ID == null ? string.Empty : db.Village.FirstOrDefault(x => x.ID == t1.PermanentVillage_ID).VillageName,
                             PermanentPostOfficeName = t1.PermanentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == t1.PermanentPostOffice_ID).PostOfficeName,
                             PermanentPoliceStationName = t1.PermanentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == t1.PermanentPoliceStation_ID).PoliceStationName,
                             PermanentDistrictName = t1.PermanentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == t1.PermanentDistrict_ID).DistrictName,
                             NomineName = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).Referee_Name : string.Empty,
                             NominePermanentAddress = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).PermanentAddress : string.Empty,
                             NominePresentAddress = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).PresentAddress : string.Empty,
                             NomineRelation = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).Relation : string.Empty,
                             NomineFatherName = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).Referee_FatherName : string.Empty,
                             NomineMotherName = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).Referee_MotherName : string.Empty
                         });

            if (vData.Any())
            {
                var a = vData.FirstOrDefault();

                this.Name = a.Name;
                this.EmployeeIDNo = a.EmployeeIDNo;
                this.Section = a.Section;
                this.Designation = a.Designation;
                this.Department = a.Department;
                this.Grade = a.Grade;
                this.Line = a.Line;
                this.JoinningDate = a.JoinningDate;
                this.JobPermanentDate = a.JoinningDate;
                this.EngName = a.EngName;
                this.EngDesignation = a.EngDesignation;
                this.Unit = a.Unit;
                this.FatherName = a.FatherName;
                this.MotherName = a.MotherName;
                this.CardNo = a.CardNo;
                this.PresentAddress = a.PresentVillageName + "," + a.PresentPostOfficeName + "," + a.PresentPoliceStationName + "," + a.PresentDistrictName;
                this.PermanentAddress = a.PermanentVillageName + "," + a.PermanentPostOfficeName + "," + a.PermanentPoliceStationName + "," + a.PermanentDistrictName;
                this.PresentVill = a.PresentVillageName;
                this.PresentThana = a.PresentPoliceStationName;
                this.PresentPostOffice = a.PresentPostOfficeName;
                this.PresentDistrict = a.PresentDistrictName;
                this.PermanentVill = a.PermanentVillageName;
                this.PermanentPostOffice = a.PermanentPostOfficeName;
                this.PermanentThana = a.PermanentPoliceStationName;
                this.PermanentDistrict = a.PermanentDistrictName;
                this.DOB = a.DOB;
                this.Gender = a.Gender;
                this.ResignDate = a.ResignDate;
                this.NomineFatherName = a.NomineFatherName;
                this.NomineMotherName = a.NomineMotherName;
                this.NominePermanentAddress = a.NominePermanentAddress;
                this.NominePresentAddress = a.NominePresentAddress;
                this.NomineName = a.NomineName;
                this.NomineRelation = a.NomineRelation;
                EmployeeAge(a.DOB);
            }
        }
        public VMApplications GetPayrollInfo(int EmployeeId, int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            VMApplications model = new VMApplications();
            model.PayrollId = PayrollId;
            model.ID = EmployeeId;
            var getPayroll = (from o in db.HrmsPayrolls
                              join p in db.HrmsPayrollInfos on o.ID equals p.PayrollFK
                              where p.EmployeeIdFk == EmployeeId && p.PayrollFK == PayrollId
                              select new { o, p }).ToList();

            if (getPayroll.Any())
            {
                model.BasicPay = getPayroll.Any(a => a.p.Eod_ReferenceFk == 1) == false ? 0 : getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 1).p.Amount;
                model.HouseAmount = getPayroll.Any(a => a.p.Eod_ReferenceFk == 2) == false ? 0 : getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 2).p.Amount;
                model.GrossPay = model.BasicPay + model.HouseAmount + this.FoodHealthTrans;
                model.PresentDay = getPayroll.Any(a => a.p.Eod_ReferenceFk == 31) == false ? 0 : (int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 31).p.Amount;
                model.AttendanceTaka = getPayroll.Any(a => a.p.Eod_ReferenceFk == 36) == false ? 0 : getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 36).p.Amount;
                model.OTHour = getPayroll.Any(a => a.p.Eod_ReferenceFk == 28) == false ? 0 : getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 28).p.Amount;
                model.OTTaka = getPayroll.Any(a => a.p.Eod_ReferenceFk == 39) == false ? 0 : getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 39).p.Amount;
                model.AttendanceBonus = getPayroll.Any(a => a.p.Eod_ReferenceFk == 7) == false ? 0 : getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 7).p.Amount;
                model.FromDate = getPayroll.FirstOrDefault().o.FromDate;
                model.ToDate = getPayroll.FirstOrDefault().o.ToDate;
            }
            else
            {
                model.PayrollId -= 1;
                if (model.PayrollId > 0)
                {
                    model = GetPayrollInfo(model.ID, model.PayrollId);
                }
            }
            return model;
        }

        public VMApplications GetLastPayroll(int EmployeeId)
        {
            HrmsContext db = new HrmsContext();
            VMApplications model = new VMApplications();
            model.ID = EmployeeId;

            var AllPayroll = db.HrmsPayrolls.ToList().OrderByDescending(x => x.ID);
            if (AllPayroll.Any())
            {
                var take = AllPayroll.FirstOrDefault();
                model.PayrollId = take.ID;
                model = GetPayrollInfo(model.ID, model.PayrollId);
            }
            return model;
        }

        public void LoadFinalSettlementData(VMApplications model)
        {
            HrmsContext db = new HrmsContext();
            VMApplications m = new VMApplications();
            int EarnLeave = 0;
            decimal EarnLeaveTaka = decimal.Zero;
            decimal CompanyTaka = decimal.Zero;
            decimal TotalBenefit = 0;
            decimal TotalPayment = 0;
            decimal Deduction = decimal.Zero;
            decimal TotalPayable = 0;
            var getLeaveInfo = db.HRMS_Earn_Leave_Payment.Where(a => a.EmployeeId == model.ID).OrderByDescending(a => a.ID);
            if (getLeaveInfo.Any())
            {
                var t = getLeaveInfo.FirstOrDefault();
                EarnLeave = t.PayableEarnLeave;
                EarnLeaveTaka = t.PayableEarnLeaveTk;
            }

            m = GetLastPayroll(model.ID);

            EmployeeBasicInfo(model.ID);
            DateTime ZeroDate = new DateTime(1900, 01, 01, 00, 00, 00);
            CalculateJobAge(model.JoinningDate, model.EDD);

            if (ZeroDate.Equals(model.ResignDate) || ZeroDate.Equals(model.JoinningDate))
            {
                this.TotalYear = 0;
                this.TotalMonth = 0;
                this.TotalDay = 0;
            }
            else
            {
                TotalWorkingAge(model.JoinningDate, ResignDate);
            }

            if (model.Year >= 10)
            {
                if (model.Month > 6)
                {
                    TotalBenefit = (model.Year + 1) * 45 * (m.BasicPay / 30);
                }
                else
                {
                    TotalBenefit = model.Year * 45 * (m.BasicPay / 30);
                }
            }
            else if (model.Year >= 5 && model.Year < 10)
            {
                if (model.Month > 6)
                {
                    TotalBenefit = (model.Year + 1) * 14 * (m.BasicPay / 30);
                }
                else
                {
                    TotalBenefit = model.Year * 14 * (m.BasicPay / 30);
                }
            }

            TotalPayment = m.AttendanceTaka + m.AttendanceBonus + m.OTTaka + TotalBenefit + model.OtherAmount;
            TotalPayable = TotalPayment + Deduction;
            List<ReportDocType> lst = new List<ReportDocType>();
            ReportDocType report = new ReportDocType();

            //EmployeeInformation
            report.HeaderLeft1 = model.Name;
            report.HeaderLeft2 = model.EmployeeIDNo;
            report.HeaderLeft3 = model.Designation;
            report.HeaderLeft4 = model.Section;

            report.HeaderRight1 = m.GrossPay.ToString();
            report.HeaderRight2 = m.BasicPay.ToString();

            if (!model.ResignDate.Equals(ZeroDate))
            {
                report.HeaderMiddle1 = Helpers.ShortDateString(model.ResignDate);
                report.HeaderMiddle2 = Helpers.ShortDateString(model.ResignDate);
            }
            else if (model.ResignDate.Equals(ZeroDate))
            {
                report.HeaderMiddle1 = string.Empty;
                report.HeaderMiddle2 = string.Empty;
            }

            report.HeaderMiddle3 = Helpers.ShortDateString(model.JoinningDate);
            report.HeaderMiddle4 = model.Year.ToString();
            report.HeaderMiddle5 = model.Month.ToString();
            report.HeaderMiddle6 = model.Day.ToString();
            report.HeaderMiddle7 = model.Age.ToString();

            report.Body1 = Helpers.ShortDateString(m.FromDate);
            report.Body2 = Helpers.ShortDateString(m.ToDate);
            report.Body3 = m.PresentDay.ToString();
            report.Body4 = Math.Round(m.AttendanceTaka).ToString();
            report.Body5 = Math.Round(m.AttendanceBonus).ToString();
            report.Body6 = EarnLeave.ToString();
            report.Body7 = Math.Round(EarnLeaveTaka).ToString();
            report.Body8 = m.OTHour.ToString();
            report.Body9 = Math.Round(m.OTTaka).ToString();
            report.Body10 = Math.Round(TotalBenefit).ToString();
            report.Body11 = Math.Round(model.OtherAmount).ToString();
            report.Body12 = Math.Round(TotalPayment).ToString();
            report.Body13 = CompanyTaka.ToString();
            report.Body14 = Math.Round(TotalPayable).ToString();
            report.Body15 = Helpers.ShortDateString(model.ApplicationDate);

            lst.Add(report);
            this.ReportSource = lst;
        }

        public void LoadFinalSettlementData1(VMApplications model)
        {
            HrmsContext db = new HrmsContext();
            VMApplications m = new VMApplications();
            int LastBasic = 0;
            int LastGross = 0;
            int LastHouse = 0;
            int LastOther = 1850;
            decimal EarnLeaveTaka = decimal.Zero;
            decimal CompanyTaka = decimal.Zero;
            decimal OtherTaka = decimal.Zero;
            decimal OTHour = 0;
            decimal OTTaka = 0;
            int PresentDay = 0;
            decimal AttendanceTaka = 0;
            decimal AttendanceBonus = decimal.Zero;

            decimal TotalBenefit = 0;
            decimal TotalPayment = 0;
            decimal Deduction = decimal.Zero;
            decimal TotalPayable = 0;

            m = GetLastPayroll(model.ID);

            //var takePayroll = db.HrmsPayrolls.OrderByDescending(a=>a.ID).FirstOrDefault();

            //model.FromDate = takePayroll.FromDate;
            //model.ToDate = takePayroll.ToDate;

            //var getPayroll = (from o in db.HrmsPayrolls
            //                 join p in db.HrmsPayrollInfos on o.ID equals p.PayrollFK
            //                 where p.EmployeeIdFk==model.ID && p.PayrollFK==takePayroll.ID
            //                 select new
            //                 {
            //                     o,p
            //                 }).ToList();

            //if (getPayroll.Any())
            //{
            //    LastBasic = (int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 1).p.Amount;
            //    LastHouse = (int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 2).p.Amount;
            //    LastGross = LastBasic + LastHouse + LastOther;
            //    PresentDay = (int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 31).p.Amount;
            //    AttendanceTaka = (int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 36).p.Amount;
            //    OTHour =(int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 28).p.Amount;
            //    OTTaka= getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 39).p.Amount;
            //    AttendanceBonus = getPayroll.Any(a => a.p.Eod_ReferenceFk == 7) == true ? getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 7).p.Amount : 0;
            //}

            EmployeeBasicInfo(model.ID);
            DateTime ZeroDate = new DateTime(1900, 01, 01, 00, 00, 00);
            CalculateJobAge(model.JoinningDate, model.EDD);

            if (ZeroDate.Equals(model.ResignDate) || ZeroDate.Equals(model.JoinningDate))
            {
                this.TotalYear = 0;
                this.TotalMonth = 0;
                this.TotalDay = 0;
            }
            else
            {
                TotalWorkingAge(model.JoinningDate, ResignDate);
            }

            if (model.Year >= 10)
            {
                if (model.Month > 6)
                {
                    TotalBenefit = (model.Year + 1) * 45 * (LastBasic / 30);
                }
                else
                {
                    TotalBenefit = model.Year * 45 * (LastBasic / 30);
                }
            }
            else if (model.Year >= 5 && model.Year < 10)
            {
                if (model.Month > 6)
                {
                    TotalBenefit = (model.Year + 1) * 14 * (LastBasic / 30);
                }
                else
                {
                    TotalBenefit = model.Year * 14 * (LastBasic / 30);
                }
            }

            TotalPayment = AttendanceTaka + AttendanceBonus + OTTaka + TotalBenefit + model.OtherAmount;
            TotalPayable = TotalPayment + Deduction;
            List<ReportDocType> lst = new List<ReportDocType>();
            ReportDocType report = new ReportDocType();

            //EmployeeInformation
            report.HeaderLeft1 = model.Name;
            report.HeaderLeft2 = model.EmployeeIDNo;
            report.HeaderLeft3 = model.Designation;
            report.HeaderLeft4 = model.Section;

            report.HeaderRight1 = LastGross.ToString();
            report.HeaderRight2 = LastBasic.ToString();

            if (model.QuitDate.Equals(ZeroDate) && !model.ResignDate.Equals(ZeroDate))
            {
                report.HeaderMiddle1 = Helpers.ShortDateString(model.ResignDate);
                report.HeaderMiddle2 = Helpers.ShortDateString(model.ResignDate);
            }
            else if (!model.QuitDate.Equals(ZeroDate) && model.ResignDate.Equals(ZeroDate))
            {
                report.HeaderMiddle1 = Helpers.ShortDateString(model.QuitDate);
                report.HeaderMiddle2 = Helpers.ShortDateString(model.QuitDate);
            }
            else if (model.QuitDate.Equals(ZeroDate) && model.ResignDate.Equals(ZeroDate))
            {
                report.HeaderMiddle1 = string.Empty;
                report.HeaderMiddle2 = string.Empty;
            }

            report.HeaderMiddle3 = Helpers.ShortDateString(model.JoinningDate);
            report.HeaderMiddle4 = model.Year.ToString();
            report.HeaderMiddle5 = model.Month.ToString();
            report.HeaderMiddle6 = model.Day.ToString();
            report.HeaderMiddle7 = model.Age.ToString();

            report.Body1 = Helpers.ShortDateString(model.FromDate);
            report.Body2 = Helpers.ShortDateString(model.ToDate);
            report.Body3 = PresentDay.ToString();
            report.Body4 = Math.Round(AttendanceTaka).ToString();
            report.Body5 = Math.Round(AttendanceBonus).ToString();
            report.Body6 = EarnLeaveTaka.ToString();
            report.Body7 = Math.Round(EarnLeaveTaka).ToString();
            report.Body8 = OTHour.ToString();
            report.Body9 = Math.Round(OTTaka).ToString();
            report.Body10 = Math.Round(TotalBenefit).ToString();
            report.Body11 = Math.Round(model.OtherAmount).ToString();
            report.Body12 = Math.Round(TotalPayment).ToString();
            report.Body13 = CompanyTaka.ToString();
            report.Body14 = Math.Round(TotalPayable).ToString();

            lst.Add(report);
            this.ReportSource = lst;
        }

        public void JobLoadData(VMApplications model)
        {
            EmployeeBasicInfo(model.ID);
            List<ReportDocType> lst = new List<ReportDocType>();
            ReportDocType report = new ReportDocType();
            report.HeaderLeft1 = model.Name;
            report.HeaderLeft2 = model.EmployeeIDNo;
            report.HeaderLeft3 = model.Designation;
            report.HeaderLeft4 = model.Section;
            report.HeaderLeft5 = model.Department;
            report.HeaderLeft6 = model.Grade;
            report.HeaderLeft7 = model.Line;
            report.HeaderLeft13 = model.CardNo;
            report.Body9 = Helpers.ShortDateString(model.JoinningDate);
            lst.Add(report);
            this.ReportSource = lst;
        }

        public void JobApplication(int EmployeeId)
        {
            HrmsContext db = new HrmsContext();
            List<ReportDocType> lst = new List<ReportDocType>();

            var vData = (from t1 in db.Employees
                         join t2 in db.Designations on t1.Designation_Id equals t2.ID
                         join t3 in db.Sections on t1.Section_Id equals t3.ID
                         where t1.ID == EmployeeId
                         select new
                         {
                             t1,
                             Designation = t2.DesigNameBangla,
                             Section = t3.SectionName,
                             Education = db.HrmsEducations.Any(a => a.Employee_Id == t1.ID) == true ? db.HrmsEducations.FirstOrDefault(a => a.Employee_Id == t1.ID).Degree : string.Empty,
                             SpouseName = db.HrmsSpouses.Any(a => a.Employee_Id == t1.ID) == true ? db.HrmsSpouses.FirstOrDefault(a => a.Employee_Id == t1.ID).SpouseName : string.Empty,
                             NoOfChildren = db.HrmsChildren.Any(a => a.Employee_Id == t1.ID) == true ? db.HrmsChildren.Where(a => a.Employee_Id == t1.ID).Count() : 0,
                             PresentVillageName = t1.PresentVillage_ID == null ? string.Empty : db.Village.FirstOrDefault(x => x.ID == t1.PresentVillage_ID).VillageName,
                             PresentPostOfficeName = t1.PresentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == t1.PresentPostOffice_ID).PostOfficeName,
                             PresentPoliceStationName = t1.PresentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == t1.PresentPoliceStation_ID).PoliceStationName,
                             PresentDistrictName = t1.PresentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == t1.PresentDistrict_ID).DistrictName,
                             PermanentVillageName = t1.PermanentVillage_ID == null ? string.Empty : db.Village.FirstOrDefault(x => x.ID == t1.PermanentVillage_ID).VillageName,
                             PermanentPostOfficeName = t1.PermanentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == t1.PermanentPostOffice_ID).PostOfficeName,
                             PermanentPoliceStationName = t1.PermanentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == t1.PermanentPoliceStation_ID).PoliceStationName,
                             PermanentDistrictName = t1.PermanentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == t1.PermanentDistrict_ID).DistrictName,

                             Experiance = (from o in db.HrmsExperiences
                                           where o.Employee_Id == t1.ID
                                           select new { id = o.ID, company = o.Company_Name, designamtion = o.Designation, exp = o.Responsibility }).ToList().OrderByDescending(a => a.id),
                             SalaryInfo = (from o in db.HrmsEodRecords
                                           where o.EmployeeId == t1.ID && o.Status == true
                                           select new { RefId = o.Eod_RefFk, Amount = o.ActualAmount }).ToList()
                         }).ToList();
            if (vData.Any())
            {
                var take = vData.FirstOrDefault();

                CalculateJobAge(take.t1.DOB, DateTime.Now);
                ReportDocType vm = new ReportDocType();
                vm.HeaderTop1 = take.Designation;
                vm.HeaderTop2 = take.Section;
                vm.HeaderTop3 = take.t1.Name_BN;
                if (take.t1.FH.Equals("F"))
                {
                    vm.HeaderTop4 = take.t1.Father_Name;
                }
                else if (take.t1.FH.Equals("H"))
                {
                    vm.HeaderTop6 = take.t1.Father_Name;
                }
                if (take.t1.Gender.Equals("Male"))
                {
                    vm.HeaderTop4 = take.t1.Father_Name;
                }
                vm.HeaderTop7 = take.t1.Marital_Status;
                if (take.t1.Marital_Status.Equals("Married") && take.t1.Gender.Equals("Male"))
                {
                    vm.HeaderTop6 = take.SpouseName;
                    vm.HeaderTop9 = take.NoOfChildren.ToString();
                }
                else if (take.t1.Marital_Status.Equals("Unmarried"))
                {
                    vm.HeaderTop6 = "N/A";
                    vm.HeaderTop9 = "N/A";
                }
                vm.HeaderTop5 = take.t1.Mother_Name;
                vm.HeaderTop8 = take.Education;
                vm.HeaderTop10 = take.t1.Religion;

                vm.HeaderLeft1 = take.t1.NID_No;
                vm.HeaderLeft2 = take.t1.Mobile_No;
                vm.HeaderLeft3 = Helpers.ShortDateString(take.t1.DOB);
                vm.HeaderLeft4 = this.Year.ToString();
                vm.HeaderLeft5 = Helpers.ShortDateString(take.t1.Joining_Date);

                if (take.Experiance.Any())
                {
                    var texp = take.Experiance.FirstOrDefault();
                    vm.HeaderLeft6 = texp.company;
                    vm.HeaderLeft7 = texp.designamtion;
                    vm.HeaderLeft8 = texp.exp;
                }
                else
                {
                    vm.HeaderLeft6 = "N/A";
                    vm.HeaderLeft7 = "N/A";
                    vm.HeaderLeft8 = "N/A";
                }
                decimal salary = 0;
                if (take.SalaryInfo.Any(a => a.RefId == 1))
                {
                    salary = this.FoodHealthTrans;
                    foreach (var v in take.SalaryInfo)
                    {
                        if (v.RefId == 1)
                        {
                            salary += v.Amount;
                        }
                        else if (v.RefId == 2)
                        {
                            salary += v.Amount;
                        }
                    }
                    vm.HeaderLeft9 = salary.ToString();
                }
                else
                {
                    vm.HeaderLeft9 = salary.ToString();
                }

                vm.HeaderMiddle1 = take.PermanentVillageName;
                vm.HeaderMiddle2 = take.PermanentPostOfficeName;
                vm.HeaderMiddle3 = take.PermanentPoliceStationName;
                vm.HeaderMiddle4 = take.PermanentDistrictName;

                vm.HeaderMiddle5 = take.PresentVillageName;
                vm.HeaderMiddle6 = take.PresentPostOfficeName;
                vm.HeaderMiddle7 = take.PresentPoliceStationName;
                vm.HeaderMiddle8 = take.PresentDistrictName;

                lst.Add(vm);

            }

            this.ReportSource = lst;
        }

        public void BackgroundChack(int EmployeeId)
        {
            HrmsContext db = new HrmsContext();
            List<ReportDocType> lst = new List<ReportDocType>();

            var vData = (from t1 in db.Employees
                         join t2 in db.Designations on t1.Designation_Id equals t2.ID
                         join t3 in db.Sections on t1.Section_Id equals t3.ID
                         join t4 in db.Lines on t1.LineId equals t4.ID
                         where t1.ID == EmployeeId
                         select new
                         {
                             t1,
                             Designation = t2.DesigNameBangla,
                             Section = t3.SectionName,
                             Line = t4.Line,
                             PresentVillageName = t1.PresentVillage_ID == null ? string.Empty : db.Village.FirstOrDefault(x => x.ID == t1.PresentVillage_ID).VillageName,
                             PresentPostOfficeName = t1.PresentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == t1.PresentPostOffice_ID).PostOfficeName,
                             PresentPoliceStationName = t1.PresentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == t1.PresentPoliceStation_ID).PoliceStationName,
                             PresentDistrictName = t1.PresentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == t1.PresentDistrict_ID).DistrictName,
                             PermanentVillageName = t1.PermanentVillage_ID == null ? string.Empty : db.Village.FirstOrDefault(x => x.ID == t1.PermanentVillage_ID).VillageName,
                             PermanentPostOfficeName = t1.PermanentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == t1.PermanentPostOffice_ID).PostOfficeName,
                             PermanentPoliceStationName = t1.PermanentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == t1.PermanentPoliceStation_ID).PoliceStationName,
                             PermanentDistrictName = t1.PermanentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == t1.PermanentDistrict_ID).DistrictName,
                             Reference = (from o in db.HrmsReferences
                                          where o.IsReference == true && o.Employee_Id == t1.ID
                                          select new
                                          {
                                              Id = o.ID,
                                              RefName = o.Referee_Name,
                                              PresentAdd = o.PresentAddress,
                                              PermanentAdd = o.PermanentAddress,
                                              Mobile = o.Contact_No
                                          }).ToList().OrderBy(a => a.Id)
                         }).ToList();
            if (vData.Any())
            {
                var take = vData.FirstOrDefault();
                ReportDocType vm = new ReportDocType();
                vm.HeaderTop1 = take.t1.Name_BN;
                vm.HeaderTop2 = take.t1.EmployeeIdentity;
                vm.HeaderTop3 = take.Designation;
                vm.HeaderTop4 = take.Line;
                vm.HeaderTop5 = take.Section;
                vm.HeaderTop6 = Helpers.ShortDateString(take.t1.Joining_Date);
                vm.HeaderTop7 = take.t1.Grade;
                vm.HeaderTop8 = take.t1.Father_Name;
                vm.HeaderTop9 = take.t1.Mother_Name;

                vm.HeaderMiddle1 = take.PermanentVillageName;
                vm.HeaderMiddle2 = take.PermanentPostOfficeName;
                vm.HeaderMiddle3 = take.PermanentPoliceStationName;
                vm.HeaderMiddle4 = take.PermanentDistrictName;

                vm.HeaderMiddle5 = take.PresentVillageName;
                vm.HeaderMiddle6 = take.PresentPostOfficeName;
                vm.HeaderMiddle7 = take.PresentPoliceStationName;
                vm.HeaderMiddle8 = take.PresentDistrictName;

                if (take.Reference.Any())
                {
                    var takeFirst = take.Reference.FirstOrDefault();

                    vm.HeaderLeft1 = takeFirst.RefName;
                    vm.HeaderLeft2 = takeFirst.PresentAdd;
                    vm.HeaderLeft3 = takeFirst.PermanentAdd;
                    vm.HeaderLeft4 = takeFirst.Mobile;
                    if (take.Reference.Count() > 1)
                    {
                        var takeLast = take.Reference.OrderByDescending(a => a.Id).FirstOrDefault();

                        vm.HeaderLeft5 = takeLast.RefName;
                        vm.HeaderLeft6 = takeLast.PresentAdd;
                        vm.HeaderLeft7 = takeLast.PermanentAdd;
                        vm.HeaderLeft8 = takeLast.Mobile;
                    }
                }
                lst.Add(vm);

            }

            this.ReportSource = lst;
        }
        #endregion
        #region IDCardGenarate
        /// <summary>
        /// For Specific Id Card For Employee
        /// </summary>
        public DtPayroll IDCardGenarate()
        {
            HrmsContext db = new HrmsContext();

            DtPayroll ds = new DtPayroll();

            var IDData = (from t1 in db.Employees
                          join t2 in db.Designations on t1.Designation_Id equals t2.ID
                          join t3 in db.Sections on t1.Section_Id equals t3.ID
                          join t4 in db.Departments on t1.Department_Id equals t4.ID
                          where t1.Status == true
                          select new
                          {
                              EngName = t1.Name,
                              EngDesignation = t2.Name,
                              Section = t3.SectionName,
                              CardNo = t1.CardNo,
                              JoinningDate = t1.Joining_Date,
                              Grade = t1.Grade,
                              Department = t4.DeptName
                          }).ToList();

            foreach (var v in IDData)
            {
                ds.DtEmployeeID.Rows.Add(
                   v.EngName,
                   v.CardNo,
                   v.Department,
                   v.EngDesignation,
                   v.Section,
                   v.Grade,
                   v.JoinningDate,
                   string.Empty,
                   0
                  );
            }
            return ds;

        }

        #endregion
        #region Methods

        public decimal GetEmployeeBasic(int EmployeeID)
        {
            decimal basicPay = decimal.Zero;
            HrmsContext db = new HrmsContext();
            var Vdata = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Status == true && a.Eod_RefFk == 1);
            if (Vdata.Any())
            {
                basicPay = Vdata.OrderByDescending(a => a.ID).FirstOrDefault().ActualAmount;

            }

            return basicPay;
        }

        public void GetEmployeeGross(int EmployeeID)
        {
            HrmsContext db = new HrmsContext();
            var Vdata = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Status == true || a.Eod_RefFk == 1 || a.Eod_RefFk == 2);
            if (Vdata.Any())
            {
                this.BasicPay = Vdata.Any(a => a.Eod_RefFk == 1) == true ? Vdata.OrderByDescending(a => a.ID).FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount : 0;
                this.HouseAmount = Vdata.Any(a => a.Eod_RefFk == 2) == true ? Vdata.OrderByDescending(a => a.ID).FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount : 0;
                this.GrossPay = this.BasicPay + this.HouseAmount + this.FoodHealthTrans;
            }
        }

        private decimal GetIncrementAmount(int EmployeeID)
        {
            decimal BasicAmount = decimal.Zero;
            decimal HouseAllowance = decimal.Zero;
            decimal IncrementBasic = decimal.Zero;
            decimal IncrementHouse = decimal.Zero;
            decimal PreviousBasicAmount = decimal.Zero;
            decimal PreviousHouseAmount = decimal.Zero;
            decimal IncrementTotal = decimal.Zero;

            HrmsContext db = new HrmsContext();
            var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Status == true);
            if (getRecord.Any())
            {
                BasicAmount = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                HouseAllowance = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;

                PreviousBasicAmount = BasicAmount / (decimal)1.05;
                IncrementBasic = BasicAmount - PreviousBasicAmount;

                PreviousHouseAmount = PreviousBasicAmount * (decimal)0.5;
                IncrementHouse = HouseAllowance - PreviousHouseAmount;

                IncrementTotal = Math.Round(IncrementBasic + IncrementHouse);
            }

            return IncrementTotal;
        }

        /// <summary>
        /// id means primaryKey and Filterid means 
        /// Designations=1,Sections=2,Departments=3,Grade=4
        /// </summary>
        /// <returns></returns>
        private string GetName(int id, int Filterid)
        {
            HrmsContext db = new HrmsContext();
            string name = string.Empty;
            if (Filterid == 1)
            {
                var c = (from t1 in db.Designations
                         where t1.ID == id
                         select new
                         {
                             Name = t1.DesigNameBangla
                         }).FirstOrDefault();
                name = c.Name;


            }
            else if (Filterid == 2)
            {

                var c = (from t1 in db.Sections
                         where t1.ID == id
                         select new
                         {
                             Name = t1.SectionName
                         }).FirstOrDefault();
                name = c.Name;
            }
            else if (Filterid == 3)
            {

                var c = (from t1 in db.Departments
                         where t1.ID == id
                         select new
                         {
                             Name = t1.DeptNameBangla
                         }).FirstOrDefault();
                name = c.Name;
            }
            //else if (Filterid == 4)
            //{
            //    DropDownData d = new DropDownData();

            //    var c = d.GetGradeList().Where(a => a.Value == id.ToString()).Select(x => new { Name = x.Text }).FirstOrDefault();

            //    name = c.Name;
            //}

            return name;
        }

        private void TotalWorkingAge(DateTime FromDate, DateTime Todate)
        {
            //int Years = new DateTime(DateTime.Now.Subtract(FromDate).Ticks).Year - 1;
            int Years = new DateTime(Todate.Subtract(FromDate).Ticks).Year - 1;
            DateTime PastYearDate = FromDate.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (PastYearDate.AddMonths(i) == Todate)
                {
                    Months = i;
                    break;
                }
                else if (PastYearDate.AddMonths(i) >= Todate)
                {
                    Months = i - 1;
                    break;
                }
            }
            int Days = Todate.Subtract(PastYearDate.AddMonths(Months)).Days;

            this.TotalYear = Years;
            this.TotalMonth = Months;
            this.TotalDay = Days;
        }

        private void CalculateJobAge(DateTime FromDate, DateTime Todate)
        {
            //int Years = new DateTime(DateTime.Now.Subtract(FromDate).Ticks).Year - 1;
            int Years = new DateTime(Todate.Subtract(FromDate).Ticks).Year - 1;
            DateTime PastYearDate = FromDate.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (PastYearDate.AddMonths(i) == Todate)
                {
                    Months = i;
                    break;
                }
                else if (PastYearDate.AddMonths(i) >= Todate)
                {
                    Months = i - 1;
                    break;
                }
            }
            int Days = Todate.Subtract(PastYearDate.AddMonths(Months)).Days;

            this.Year = Years;
            this.Month = Months;
            this.Day = Days;
        }

        private void DurationAgeForEdd(DateTime FromDate, DateTime Todate)
        {
            //int Years = new DateTime(DateTime.Now.Subtract(FromDate).Ticks).Year - 1;
            int Years = new DateTime(Todate.Subtract(FromDate).Ticks).Year - 1;
            DateTime PastYearDate = FromDate.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (PastYearDate.AddMonths(i) == Todate)
                {
                    Months = i;
                    break;
                }
                else if (PastYearDate.AddMonths(i) >= Todate)
                {
                    Months = i - 1;
                    break;
                }
            }
            int Days = Todate.Subtract(PastYearDate.AddMonths(Months)).Days;

            this.TotalYear = Years;
            this.TotalMonth = Months;
            this.TotalDay = Days;
        }

        private void EmployeeAge(DateTime Birthdate)
        {
            int age = DateTime.Now.Year - Birthdate.Year;
            if (DateTime.Now.DayOfYear < Birthdate.DayOfYear)
            {
                age = age - 1;
            }

            this.Age = age;
        }

        #endregion
        #region MaternityBenefit

        public DsMaternityBenefitReport MaternityBenefitPolicy(VMApplications model)
        {
            HrmsContext db = new HrmsContext();
            model.PaidDays = 56;

            DsMaternityBenefitReport ds = new DsMaternityBenefitReport();
            var Header = (from t1 in db.Employees
                          join t2 in db.Designations on t1.Designation_Id equals t2.ID
                          join t3 in db.Sections on t1.Section_Id equals t3.ID
                          join t4 in db.Departments on t1.Department_Id equals t4.ID
                          where t1.ID == model.ID && t1.Present_Status == 1
                          select new
                          {
                              EngName = t1.Name,
                              EngDesignation = t2.Name,
                              CardNo = t1.CardNo,
                              JoinningDate = t1.Joining_Date,
                              Section = t3.SectionName,


                          }).ToList();
            if (Header.Any())
            {
                var v = Header.FirstOrDefault();
                CalculateJobAge(v.JoinningDate, model.EDD);
                string r = this.Year + " Years " + this.Month + " Months " + this.Day + " Days";
                var Referance = db.HrmsReferences.Where(a => a.Employee_Id == model.ID && a.IsReference == true).Select(c => new { refName = c.Referee_Name, refRelation = c.Relation }).ToList();
                string refName = string.Empty;
                string refRelation = string.Empty;
                if (Referance.Any())
                {
                    var takeRef = Referance.FirstOrDefault();
                    refName = takeRef.refName;
                    refRelation = takeRef.refRelation;
                }
                GetEmployeeGross(model.ID);
                ds.DtEmployeeDetails.Rows.Add(
                      v.EngName,
                      v.EngDesignation,
                      v.CardNo,
                      v.JoinningDate,
                      r,
                      v.Section,
                      refName,
                      refRelation,
                      model.StartDate,
                      model.EDD,
                      model.EndDate,
                      model.PaidDays,
                      model.PaymentDate,
                      model.PaymentType,
                      0, //1st Payment 
                     this.GrossPay);

            }
            //............Details Data..............//
            var benifit = MaternityBenefit(model.ID);
            model.TotalAmount = benifit.Sum(a => a.TotalAmount);
            model.PresentDay = benifit.Sum(c => c.PresentDay);
            decimal stamp = 10;

            decimal wagesrate = 0;
            if (model.PresentDay > 0)
            {
                wagesrate = model.TotalAmount / model.PresentDay;
            }

            decimal TotalPayable = (112 * wagesrate) / 2;
            decimal NetTotal = (TotalPayable - stamp);

            if (benifit.Any())
            {
                foreach (var b in benifit)
                {
                    ds.DtSalaryPaymentStatus.Rows.Add(

                        b.FromDate,
                        b.PresentDay,
                        b.GrossPay,
                        0.00,
                        b.AttendanceBonus,
                        b.OTTaka,
                        b.TotalAmount);
                }
            }
            //..............Grand Total...................//
            string taka = NumberToWords(Math.Round(NetTotal), CurrencyType.BDT);

            ds.DtSalaryPaymentTotal.Rows.Add(
            wagesrate,
            Math.Round(TotalPayable),
            stamp,
            Math.Round(NetTotal),
            taka);

            return ds;
        }
        public List<VMApplications> MaternityBenefit(int EmployeeId)
        {
            HrmsContext db = new HrmsContext();
            List<VMApplications> lstmodel = new List<VMApplications>();

            var vPayroll = db.HrmsPayrolls
                .Where(o => o.IsTest == false)
                .Select(o => new { o })
                .OrderByDescending(a => a.o.ID)
                .ToList()
                .Take(3);

            if (vPayroll.Any())
            {
                foreach (var pay in vPayroll)
                {
                    var vData = db.HrmsPayrollInfos
                        .Where(a => a.PayrollFK == pay.o.ID && a.EmployeeIdFk == EmployeeId)
                        .Select(a => new { a }).ToList();

                    if (vData.Any())
                    {
                        VMApplications model = new VMApplications();
                        model.FromDate = pay.o.FromDate;
                        model.ToDate = pay.o.ToDate;
                        foreach (var v in vData)
                        {
                            if (v.a.Eod_ReferenceFk == 1)
                            {
                                model.BasicPay = v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 2)
                            {
                                model.HouseAmount = v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 3)
                            {
                                model.GrossPay = v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 7)
                            {
                                model.AttendanceBonus = v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 31)
                            {
                                model.TotalDay = (int)v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 32)
                            {
                                model.OffDay = (int)v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 34)
                            {
                                model.AbsentDay = (int)v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 28)
                            {
                                model.OTHour = v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 36)
                            {
                                model.AttendanceTaka = v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 39)
                            {
                                model.OTTaka = v.a.Amount;
                            }
                            else if (v.a.Eod_ReferenceFk == 40)
                            {
                                model.MonthlyWages = v.a.Amount;
                            }


                        }
                        model.PresentDay = model.TotalDay - (model.OffDay + model.AbsentDay);
                        model.GrossPay = model.BasicPay + model.HouseAmount + this.FoodHealthTrans;
                        model.TotalAmount = model.MonthlyWages + model.AttendanceBonus + model.OTTaka;

                        lstmodel.Add(model);

                    }
                    else
                    {
                        VMApplications model = new VMApplications();
                        model.FromDate = pay.o.FromDate;
                        model.ToDate = pay.o.ToDate;
                        lstmodel.Add(model);
                    }
                }
            }
            return lstmodel;
        }
        #region Convert Amount
        public enum CurrencyType
        {
            BDT
        }

        public string NumberToWords(decimal d, CurrencyType ct)
        {
            string s = d.ToString("F");
            string s1 = "";
            int n1 = 0;

            if (s.Contains("."))
            {
                s1 = s.Substring(0, s.IndexOf("."));
            }
            else
            {
                s1 = s;
            }
            int.TryParse(s1, out n1);

            s1 = NumberToWords(n1);

            if (ct == CurrencyType.BDT)
            {
                s = s1 + " Taka  Only ";
            }
            return s;
        }
        private string NumberToWords(int number)
        {
            if (number == 0)
                return "Zero";

            if (number < 0)
                return "Minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[]
                {
                    "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven",
                    "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"
                };
                var tensMap = new[]
                    {"Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)

                        words += unitsMap[number % 10];
                }
            }

            return words;
        }

        #endregion

        #endregion
    }
}