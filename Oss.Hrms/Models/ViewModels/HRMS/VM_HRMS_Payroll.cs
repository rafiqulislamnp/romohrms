﻿using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using Oss.Hrms.Helper;
using System.Threading.Tasks;
using Oss.Hrms.Models.Entity.Compliance;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Payroll : RootModel
    {
        #region ConstantValue
        private decimal Medical = 600;
        private decimal Food = 900;
        private decimal Transport = 350;
        private decimal HouseRate = (decimal)0.5;
        private decimal Rate = (decimal)1.5;
        private decimal DaysOfMonth = 30;
        #endregion

        #region Property
        public List<VM_HRMS_Payroll> PayrollDataList { get; set; }
        HrmsContext db = new HrmsContext();
        public string EmployeeName { get; set; }
        public string Title { get; set; }
        public string EmployeeType { get; set; }
        public string Designation { get; set; }
        public string Section { get; set; }
        public string Grade { get; set; }
        public string EmployeeIDNo { get; set; }
        public string EmployeeIdentity { get; set; }
        public string Name { get; set; }
        public string PayrollStatus { get; set; }
        public bool TestStatus { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime EffectDate { get; set; }
        [Display(Name = "Designation")]
        public int DesignationId { get; set; }
        [Display(Name = "Night Rate"), Required(ErrorMessage = "Night rate is required.")]
        public decimal NightRateAmount { get; set; }

        public int TotalEmployee { get; set; }
        public decimal Amount { get; set; }
        public int Priority { get; set; }
        public bool AlertActive { get; set; }
        [Display(Name = "Section")]
        public int? SectionID { get; set; }
        [Display(Name = "Unit"), Required(ErrorMessage = "Unit is required.")]
        public int UnitID { get; set; }
        [Display(Name = "Card Title"), Required(ErrorMessage = "Card title is required.")]
        public string CardTitle { get; set; }
        public int EODRefFk { get; set; }
        [Display(Name = "Payroll Title"), Required(ErrorMessage = "Payroll is required.")]
        public int PayRollID { get; set; }
        [DisplayName("Payroll Title")]
        public string PayRollName { get; set; }
        [Required(ErrorMessage = "Note is required")]
        public string Note { get; set; }
        [DisplayName("From Date"), Required(ErrorMessage = "From date is required.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayName("To Date"), Required(ErrorMessage = "To date is required.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        [Required(ErrorMessage = "Month is required")]
        public string Month { get; set; }
        [DisplayName("Payment Date")]
        [Required(ErrorMessage = "Payment date is required"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PaymentDate { get; set; }
        public int Monthly { get; set; }
        [Display(Name = "Has Fastival Bonus")]
        public int HasFastivalBonus { get; set; }
        public bool AddAllowance { get; set; }
        public bool Default { get; set; }
        public int EmpIDFK { get; set; }
        [Display(Name = "Card Title"), Required(ErrorMessage = "Card title is required.")]
        public int CardID { get; set; }
        public int NoOfHolyday { get; set; }
        [Display(Name = "Payroll Status")]
        public int? PayrollStatusId { get; set; }
        public DateTime ResignDate { get; set; }
        public DateTime QuitDate { get; set; }
        public bool IsReprocessExecute { get; set; }
        public bool IsRegularEOD { get; set; }
        [Display(Name = "Add Compliance")]
        public bool AddCompliance { get; set; }

        #region SalaryModel
        [Display(Name = "Employee"), Required(ErrorMessage = "Employee isRequired.")]
        public int EmployeeID { get; set; }
        [Display(Name = "Gross Salary")]
        public decimal GrossSalary { get; set; }
        [Display(Name = "Basic Pay"), Required(ErrorMessage = "Basic Pay required.")]
        public decimal BasicAmount { get; set; }
        [Display(Name = "House Allowance (%)"), Required(ErrorMessage = "Parcentage amount for house allowance is required.")]
        public decimal HouseAllowance { get; set; }
        public decimal HealthAllowance { get; set; }
        public decimal FoodAllowance { get; set; }
        public decimal TransportAllowance { get; set; }
        public decimal OtherAllowance { get; set; }
        [Display(Name = "Employee Alocation"), Required(ErrorMessage = "Employee Alocation is isRequired.")]
        public int? OTAllow { get; set; }

        [Display(Name = "Attendence Bonus"), Required(ErrorMessage = "Attendence Bonus is required.")]
        public decimal AttendenceBonus { get; set; }
        public decimal HalfNightRate { get; set; }
        public decimal FullNightRate { get; set; }
        #endregion

        [Display(Name = "Basic")]
        public string BasicPay { get; set; }
        [Display(Name = "House")]
        public string HousePay { get; set; }
        [Display(Name = "Other")]
        public string OtherPay { get; set; }
        public string OTRate { get; set; }
        public bool AllowOT { get; set; }
        [Display(Name = "Att.Bonus")]
        public string AttendenceBonuses { get; set; }
        [Display(Name = "Total")]
        public string TotalPay { get; set; }
        public decimal OTPerRate { get; set; }

        #endregion

        public List<ReportDocType> PayslipSource { get; set; }
        public HRMS_PayrollInfo HRMS_PayrollInfo { get; set; }
        public VMEmployee VMEmployee { get; set; }
        public List<VMEmployee> ListEmployee { get; set; }
        public List<EmployeeEODRecord> EODRecordList { get; set; }
        public VMNightAndHolyDay VMNightAndHolyDay { get; set; }
        public List<VMNightAndHolyDay> ListNightAndHolyDayBill { get; set; }

        public void GeneralPayrollList(int id)
        {
            var payroll = (from t1 in db.HrmsPayrollInfos
                           join t2 in db.Employees on t1.EmployeeIdFk equals t2.ID
                           join t3 in db.HrmsPayrolls on t1.PayrollFK equals t3.ID
                           join t4 in db.Sections on t2.Section_Id equals t4.ID
                           where t1.PayrollFK == id
                           select new VM_HRMS_Payroll
                           {
                               EmpIDFK = t1.EmployeeIdFk,
                               EmployeeIdentity = t2.EmployeeIdentity,
                               Title = t2.CardTitle,
                               Name = t2.Name,
                               Amount = t1.Amount,
                               PayRollName = t3.Note,
                               FromDate = t3.FromDate,
                               ToDate = t3.ToDate,
                               ID = t1.PayrollFK,
                               Section = t4.SectionName,
                               AlertActive = t2.AlertActive
                           }).OrderByDescending(s => s.ID).GroupBy(x => x.EmpIDFK).Select(x => x.FirstOrDefault());
            if (payroll.Any())
            {
                var vfirst = payroll.FirstOrDefault();
                this.ID = id;
                this.PayRollName = vfirst.PayRollName + " (" + vfirst.FromDate.ToShortDateString() + ")" + "-" + "(" + vfirst.ToDate.ToShortDateString() + ")";
                this.PayrollDataList = payroll.ToList();
            }
            else
            {
                this.PayrollDataList = new List<VM_HRMS_Payroll>();
            }
        }

        public void ListVmPayrollDetails(int id, int EmployeeId)
        {
            var payroll = (from t1 in db.HrmsPayrollInfos
                           join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                           join t4 in db.HrmsPayrolls on t1.PayrollFK equals t4.ID
                           where t1.Status == true && t4.ID == id && t1.EmployeeIdFk == EmployeeId
                           select new VM_HRMS_Payroll()
                           {
                               EmpIDFK = t1.EmployeeIdFk,
                               Name = t2.Name,
                               Amount = t1.Amount,
                               ID = t1.ID,
                               Priority = t2.priority
                           }).OrderBy(a => a.Priority).ToList();

            this.PayrollDataList = payroll;
        }

        public decimal GetTotalPay(int PayrollId, int EmpId)
        {
            decimal total = decimal.Zero;

            var payroll = (from t1 in db.HrmsPayrollInfos
                           where t1.Status == true && t1.EmployeeIdFk == EmpId
                           join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                           select new
                           {
                               status = t2.Eod,
                               Amount = t1.Amount,
                           });

            if (payroll.Any())
            {
                foreach (var v in payroll)
                {
                    if (v.status)
                    {
                        total += v.Amount;
                    }
                    else
                    {
                        total -= v.Amount;
                    }
                }
            }
            return total;
        }

        public void GetPayRollInfoByID(int PayrollId, int EmployeeId)
        {
            var vData = (from t1 in db.HrmsPayrollInfos
                         join t2 in db.Employees on t1.EmployeeIdFk equals t2.ID
                         join t3 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t3.ID
                         join t4 in db.HrmsPayrolls on t1.PayrollFK equals t4.ID
                         where t1.PayrollFK == PayrollId && t1.EmployeeIdFk == EmployeeId
                         select new VM_HRMS_Payroll
                         {
                             ID = t1.ID,
                             EmployeeName = t2.Name_BN,
                             EODRefFk = (int)t1.Eod_ReferenceFk,
                             EmpIDFK = EmployeeId,
                             PayRollID = PayRollID,
                             Name = t3.Name,
                             Amount = t1.Amount,
                             Title = t4.Note,
                             FromDate = t4.FromDate,
                             ToDate = t4.ToDate,
                             Priority = t3.priority
                         }).OrderBy(a => a.Priority).ToList();


            var takeInfo = vData.FirstOrDefault();
            this.PayrollDataList = vData;
            this.EmployeeName = takeInfo.EmployeeName;
            this.Title = "Payroll : " + takeInfo.Title + "|" + Helpers.PeriodicShortDateString(takeInfo.FromDate, takeInfo.ToDate);
        }

        /// <summary>
        /// This Function is used to payslip generate for html format
        /// </summary>
        /// <param name="model"></param>
        public void GeneratePaySlip(VM_HRMS_Payroll model)
        {
            var vData = (from t1 in db.HrmsPayrolls
                         join t2 in db.HrmsPayrollInfos on t1.ID equals t2.PayrollFK
                         join a in db.Employees on t2.EmployeeIdFk equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join c in db.Departments on a.Department_Id equals c.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         join e in db.Lines on a.LineId equals e.ID
                         join p in db.HrmsEodReferences on t2.Eod_ReferenceFk equals p.ID
                         where t1.ID == model.ID && t2.Status == true && t2.Eod_ReferenceFk != 42 && t2.Eod_ReferenceFk != 43 && t2.Eod_ReferenceFk != 44
                         select new
                         {
                             PayrollId = t1.ID,
                             PayMonth = t1.FromDate,
                             InfoID = t2.ID,
                             EmpID = a.ID,
                             name = a.Name_BN,
                             CardNo = a.EmployeeIdentity,
                             Grade = a.Grade,
                             Line = e.Line,
                             Designation = b.DesigNameBangla,
                             Department = c.DeptNameBangla,
                             Section = d.SectionName,
                             RefId = p.ID,
                             HeadName = p.Name,
                             Amount = t2.Amount,
                             IsAmountValue = p.IsHour,
                             Eod = p.Eod,
                             IsRegular = p.Regular,
                             Type = p.TypeBangla,
                             priority = p.priority

                         }).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.name, o.CardNo, o.Department, o.Section, o.Designation, o.Grade, o.PayMonth, o.Line } into all
                            select new
                            {
                                MonthName = all.Key.PayMonth,
                                EmpID = all.Key.EmpID,
                                Name = all.Key.name,
                                CardNo = all.Key.CardNo,
                                Depertment = all.Key.Department,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                Grade = all.Key.Grade,
                                Line = all.Key.Line,
                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID && p.PayrollId == model.ID
                                            select new
                                            {
                                                InfoID = p.InfoID,
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                IsAmountValue = p.IsAmountValue,
                                                Type = p.Type,
                                                priority = p.priority,
                                                IsDeductedEod = p.Eod,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList()
                            }).ToList();


            List<ReportDocType> lst = new List<ReportDocType>();
            if (getGroup.Any())
            {

                foreach (var v in getGroup)
                {
                    //decimal totalPay = decimal.Zero;
                    ReportDocType report = new ReportDocType();
                    report.HeaderLeft1 = v.EmpID.ToString();
                    report.HeaderLeft2 = v.Name;
                    report.HeaderLeft3 = v.CardNo;
                    report.HeaderLeft4 = v.Grade;
                    report.HeaderLeft5 = v.Designation;
                    report.HeaderLeft6 = v.Depertment;
                    report.HeaderLeft7 = v.Section;
                    report.HeaderLeft8 = v.MonthName.ToString("Y");
                    report.HeaderRight1 = DateTime.Now.ToString("Y");
                    report.HeaderRight2 = v.Line;
                    if (v.HeadItem.Any())
                    {
                        report.ReportDocTypeList = new List<ReportDocType>();
                        foreach (var v1 in v.HeadItem)
                        {
                            ReportDocType report1 = new ReportDocType()
                            {
                                Body1 = v1.HeadName,
                                Body2 = v1.Amount.ToString(),
                                Body3 = v1.Type

                            };
                            report.ReportDocTypeList.Add(report1);
                        }
                    }

                    lst.Add(report);
                }
            }

            this.PayslipSource = lst;
        }

        /// <summary>
        /// This Function is used to payslip generate for crystal report format
        /// Not used any where
        /// </summary>
        /// <param name="model"></param>
        public DtPayroll LoadPaySlip(VM_HRMS_Payroll model)
        {
            DtPayroll ds = new DtPayroll();
            ds.DtEmployee.Rows.Clear();
            int counter = 10;
            if (model.EmployeeID > 0)
            {
                #region DataReceiveQuery
                var vData = (from t1 in db.HrmsPayrolls
                             join t2 in db.HrmsPayrollInfos on t1.ID equals t2.PayrollFK
                             join a in db.Employees on t2.EmployeeIdFk equals a.ID
                             join b in db.Designations on a.Designation_Id equals b.ID
                             join c in db.Departments on a.Department_Id equals c.ID
                             join d in db.Sections on a.Section_Id equals d.ID
                             join e in db.Lines on a.LineId equals e.ID
                             join p in db.HrmsEodReferences on t2.Eod_ReferenceFk equals p.ID
                             where t1.ID == model.ID
                             && t2.Status == true
                             && a.ID == model.EmployeeID
                             && t2.Eod_ReferenceFk != 42
                             && t2.Eod_ReferenceFk != 43
                             && t2.Eod_ReferenceFk != 44
                             select new
                             {
                                 PayrollId = t1.ID,
                                 PayMonth = t1.FromDate,
                                 InfoID = t2.ID,
                                 EmpID = a.ID,
                                 name = a.Name_BN,
                                 CardNo = a.EmployeeIdentity,
                                 Grade = a.Grade,
                                 Line = e.Line,
                                 Designation = b.DesigNameBangla,
                                 Department = c.DeptNameBangla,
                                 Section = d.SectionNameBangla,
                                 PaymentDate = t2.PaymentDate,
                                 RefId = p.ID,
                                 HeadName = p.Name,
                                 Amount = t2.Amount,
                                 IsAmountValue = p.IsHour,
                                 Eod = p.Eod,
                                 IsRegular = p.Regular,
                                 Type = p.TypeBangla,
                                 priority = p.priority

                             }).OrderBy(a => a.CardNo).ToList();

                var getGroup = (from o in vData
                                group o by new { o.EmpID, o.name, o.CardNo, o.Department, o.Section, o.Designation, o.Grade, o.PayMonth, o.Line, o.PaymentDate } into all
                                select new
                                {
                                    MonthName = all.Key.PayMonth,
                                    PaymentDate = all.Key.PaymentDate,
                                    EmpID = all.Key.EmpID,
                                    Name = all.Key.name,
                                    CardNo = all.Key.CardNo,
                                    Depertment = all.Key.Department,
                                    Section = all.Key.Section,
                                    Designation = all.Key.Designation,
                                    Grade = all.Key.Grade,
                                    Line = all.Key.Line,
                                    HeadItem = (from p in all
                                                where p.EmpID == all.Key.EmpID && p.PayrollId == model.ID
                                                select new
                                                {
                                                    InfoID = p.InfoID,
                                                    RefId = p.RefId,
                                                    HeadName = p.HeadName,
                                                    Amount = p.Amount,
                                                    IsAmountValue = p.IsAmountValue,
                                                    Type = p.Type,
                                                    priority = p.priority,
                                                    IsDeductedEod = p.Eod,
                                                    IsRegular = p.IsRegular,
                                                }).OrderBy(a => a.priority).ToList()
                                }).OrderBy(a => a.CardNo).ToList();
                #endregion

                #region FillDataSet

                if (getGroup.Any())
                {
                    int count = 0;
                    foreach (var v in getGroup)
                    {
                        ds.DtEmployee.Rows.Add(
                            v.EmpID,
                            v.Name,
                            v.CardNo,
                            v.Depertment,
                            v.Designation,
                            v.Section,
                            v.Grade,
                            v.MonthName,
                            v.PaymentDate,
                            v.Line,
                            0,//basic
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,//attbonus
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                            );
                        if (v.HeadItem.Any())
                        {
                            counter = 10;
                            for (int i = 0; i < v.HeadItem.Count(); i++)
                            {
                                if (v.HeadItem[i].RefId == 1)
                                {
                                    ds.DtEmployee.Rows[count][30] = v.HeadItem[i].Type;
                                }
                                else if (v.HeadItem[i].RefId == 28)
                                {
                                    ds.DtEmployee.Rows[count][31] = v.HeadItem[i].Type;
                                }
                                else if (v.HeadItem[i].RefId == 31)
                                {
                                    ds.DtEmployee.Rows[count][32] = v.HeadItem[i].Type;
                                }

                                if (i == 10)
                                {
                                    if (v.HeadItem[i].RefId == 7)
                                    {
                                        ds.DtEmployee.Rows[count][i + counter] = v.HeadItem[i].Amount;
                                    }
                                    else
                                    {
                                        ds.DtEmployee.Rows[count][i + counter + 1] = v.HeadItem[i].Amount;
                                        ++counter;
                                    }
                                }
                                else
                                {
                                    ds.DtEmployee.Rows[count][i + counter] = v.HeadItem[i].Amount;
                                }
                            }
                        }
                        ++count;
                    }
                }
                #endregion
            }
            else if (model.SectionID > 0)
            {
                #region DataReceiveQuery
                var vData = (from t1 in db.HrmsPayrolls
                             join t2 in db.HrmsPayrollInfos on t1.ID equals t2.PayrollFK
                             join a in db.Employees on t2.EmployeeIdFk equals a.ID
                             join b in db.Designations on a.Designation_Id equals b.ID
                             join c in db.Departments on a.Department_Id equals c.ID
                             join d in db.Sections on a.Section_Id equals d.ID
                             join e in db.Lines on a.LineId equals e.ID
                             join p in db.HrmsEodReferences on t2.Eod_ReferenceFk equals p.ID
                             where t1.ID == model.ID
                             && t2.Status == true
                             && a.Section_Id == model.SectionID
                             && t2.Eod_ReferenceFk != 42
                             && t2.Eod_ReferenceFk != 43
                             && t2.Eod_ReferenceFk != 44
                             select new
                             {
                                 PayrollId = t1.ID,
                                 PayMonth = t1.FromDate,
                                 InfoID = t2.ID,
                                 EmpID = a.ID,
                                 name = a.Name_BN,
                                 CardNo = a.EmployeeIdentity,
                                 Grade = a.Grade,
                                 Line = e.Line,
                                 Designation = b.DesigNameBangla,
                                 Department = c.DeptNameBangla,
                                 Section = d.SectionNameBangla,
                                 PaymentDate = t2.PaymentDate,
                                 RefId = p.ID,
                                 HeadName = p.Name,
                                 Amount = t2.Amount,
                                 IsAmountValue = p.IsHour,
                                 Eod = p.Eod,
                                 IsRegular = p.Regular,
                                 Type = p.TypeBangla,
                                 priority = p.priority

                             }).OrderBy(a => a.CardNo).ToList();

                var getGroup = (from o in vData
                                group o by new { o.EmpID, o.name, o.CardNo, o.Department, o.Section, o.Designation, o.Grade, o.PayMonth, o.Line, o.PaymentDate } into all
                                select new
                                {
                                    MonthName = all.Key.PayMonth,
                                    PaymentDate = all.Key.PaymentDate,
                                    EmpID = all.Key.EmpID,
                                    Name = all.Key.name,
                                    CardNo = all.Key.CardNo,
                                    Depertment = all.Key.Department,
                                    Section = all.Key.Section,
                                    Designation = all.Key.Designation,
                                    Grade = all.Key.Grade,
                                    Line = all.Key.Line,
                                    HeadItem = (from p in all
                                                where p.EmpID == all.Key.EmpID && p.PayrollId == model.ID
                                                select new
                                                {
                                                    InfoID = p.InfoID,
                                                    RefId = p.RefId,
                                                    HeadName = p.HeadName,
                                                    Amount = p.Amount,
                                                    IsAmountValue = p.IsAmountValue,
                                                    Type = p.Type,
                                                    priority = p.priority,
                                                    IsDeductedEod = p.Eod,
                                                    IsRegular = p.IsRegular,
                                                }).OrderBy(a => a.priority).ToList()
                                }).OrderBy(a => a.CardNo).ToList();
                #endregion

                #region FillDataSet

                if (getGroup.Any())
                {
                    int count = 0;
                    foreach (var v in getGroup)
                    {
                        ds.DtEmployee.Rows.Add(
                            v.EmpID,
                            v.Name,
                            v.CardNo,
                            v.Depertment,
                            v.Designation,
                            v.Section,
                            v.Grade,
                            v.MonthName,
                            v.PaymentDate,
                            v.Line,
                            0,//basic
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,//attbonus
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                            );
                        if (v.HeadItem.Any())
                        {
                            counter = 10;
                            for (int i = 0; i < v.HeadItem.Count(); i++)
                            {
                                if (v.HeadItem[i].RefId == 1)
                                {
                                    ds.DtEmployee.Rows[count][30] = v.HeadItem[i].Type;
                                }
                                else if (v.HeadItem[i].RefId == 28)
                                {
                                    ds.DtEmployee.Rows[count][31] = v.HeadItem[i].Type;
                                }
                                else if (v.HeadItem[i].RefId == 31)
                                {
                                    ds.DtEmployee.Rows[count][32] = v.HeadItem[i].Type;
                                }
                                if (i == 10)
                                {
                                    if (v.HeadItem[i].RefId == 7)
                                    {
                                        ds.DtEmployee.Rows[count][i + counter] = v.HeadItem[i].Amount;
                                    }
                                    else
                                    {
                                        ds.DtEmployee.Rows[count][i + counter + 1] = v.HeadItem[i].Amount;
                                        ++counter;
                                    }
                                }
                                else
                                {
                                    ds.DtEmployee.Rows[count][i + counter] = v.HeadItem[i].Amount;
                                }
                            }
                        }
                        ++count;
                    }
                }
                #endregion
            }
            return ds;
        }

        public DtPayroll LoadPaySlipAll(VM_HRMS_Payroll model)
        {
            DtPayroll ds = new DtPayroll();

            var vData = (from t1 in db.HrmsPayrolls
                         join t2 in db.HrmsPayrollInfos on t1.ID equals t2.PayrollFK
                         join a in db.Employees on t2.EmployeeIdFk equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join c in db.Departments on a.Department_Id equals c.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         join e in db.Lines on a.LineId equals e.ID
                         join p in db.HrmsEodReferences on t2.Eod_ReferenceFk equals p.ID
                         where t1.ID == model.ID
                         && t2.Status == true
                         && t2.Eod_ReferenceFk != 42
                         && t2.Eod_ReferenceFk != 43
                         && t2.Eod_ReferenceFk != 44
                         select new
                         {
                             PayrollId = t1.ID,
                             PayMonth = t1.FromDate,
                             InfoID = t2.ID,
                             EmpID = a.ID,
                             name = a.Name_BN,
                             CardNo = a.EmployeeIdentity,
                             Grade = a.Grade,
                             Line = e.Line,
                             Designation = b.DesigNameBangla,
                             Department = c.DeptNameBangla,
                             Section = d.SectionNameBangla,
                             RefId = p.ID,
                             HeadName = p.Name,
                             Amount = t2.Amount,
                             IsAmountValue = p.IsHour,
                             Eod = p.Eod,
                             IsRegular = p.Regular,
                             Type = p.TypeBangla,
                             priority = p.priority

                         }).OrderBy(a => a.CardNo).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.name, o.CardNo, o.Department, o.Section, o.Designation, o.Grade, o.PayMonth, o.Line } into all
                            select new
                            {
                                MonthName = all.Key.PayMonth,
                                EmpID = all.Key.EmpID,
                                Name = all.Key.name,
                                CardNo = all.Key.CardNo,
                                Depertment = all.Key.Department,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                Grade = all.Key.Grade,
                                Line = all.Key.Line,
                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID && p.PayrollId == model.ID
                                            select new
                                            {
                                                InfoID = p.InfoID,
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                IsAmountValue = p.IsAmountValue,
                                                Type = p.Type,
                                                priority = p.priority,
                                                IsDeductedEod = p.Eod,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList()
                            }).OrderBy(a => a.CardNo).ToList();

            ds.DtEmployee.Rows.Clear();
            if (getGroup.Any())
            {
                int count = 0;
                foreach (var v in getGroup)
                {
                    ds.DtEmployee.Rows.Add(
                        v.EmpID,
                        v.Name,
                        v.CardNo,
                        v.Depertment,
                        v.Designation,
                        v.Section,
                        v.Grade,
                        v.MonthName,
                        model.PaymentDate,
                        v.Line,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0
                        );
                    if (v.HeadItem.Any())
                    {
                        for (int i = 0; i < v.HeadItem.Count(); i++)
                        {
                            ds.DtEmployee.Rows[count][i + 10] = v.HeadItem[i].Amount;
                            if (v.HeadItem[i].RefId == 1)
                            {
                                ds.DtEmployee.Rows[count][29] = v.HeadItem[i].Type;
                            }
                            else if (v.HeadItem[i].RefId == 31)
                            {
                                ds.DtEmployee.Rows[count][31] = v.HeadItem[i].Type;
                            }
                            else if (v.HeadItem[i].RefId == 28)
                            {
                                ds.DtEmployee.Rows[count][30] = v.HeadItem[i].Type;
                            }
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }

        public void GetPayrollInfo(int Id)
        {
            HrmsContext db = new HrmsContext();
            var vData = db.HrmsPayrolls.Where(a => a.ID == Id).FirstOrDefault();
            this.PayRollName = vData.Note + "|" + Helpers.PeriodicShortDateString(vData.FromDate, vData.ToDate);
        }

        public void GetEmployeeEODRecord(int EmployeeID)
        {
            db = new HrmsContext();
            List<EmployeeEODRecord> lst = new List<EmployeeEODRecord>();
            var eodref = db.HrmsEodReferences.Where(a => a.Start == 1).OrderBy(a => a.priority).ToList();

            foreach (var v in eodref)
            {
                var vData = (from t1 in db.HrmsEodRecords
                             join t2 in db.HrmsEodReferences on t1.Eod_RefFk equals t2.ID
                             where t1.EmployeeId == EmployeeID
                             && t1.Eod_RefFk == v.ID
                             && t2.Start == 1
                             && t1.Status == true
                             select new EmployeeEODRecord
                             {
                                 EODRecordID = t1.ID,
                                 EODRefFK = t2.ID,
                                 SalaryItem = t2.Name,
                                 Amount = t1.ActualAmount,
                                 EmployeeID = EmployeeID,
                                 Priority = t2.priority
                             }).OrderByDescending(a => a.EODRecordID).ToList();
                if (vData.Any())
                {
                    lst.Add(vData.FirstOrDefault());
                }
                else
                {
                    EmployeeEODRecord model = new EmployeeEODRecord
                    {
                        EODRecordID = 0,
                        EODRefFK = v.ID,
                        SalaryItem = v.Name,
                        Amount = 0,
                        EmployeeID = EmployeeID,
                        Priority = v.priority
                    };
                    lst.Add(model);
                }
            }
            this.EODRecordList = lst;
        }

        public void GetEmployeeGrossSalary(int EmployeeID)
        {
            decimal gSalary = decimal.Zero;
            var vData = (from t1 in db.HrmsEodRecords
                         join t2 in db.HrmsEodReferences on t1.Eod_RefFk equals t2.ID
                         where t1.EmployeeId == EmployeeID && t2.Start == 1 && t1.Status == true
                         select new
                         {
                             EODRecordID = t1.ID,
                             EODRefFK = t2.ID,
                             SalaryItem = t2.Name,
                             Amount = t1.ActualAmount,
                             EmployeeID = EmployeeID,
                             EffectDate = t1.EffectiveDate,
                             Priority = t2.priority
                         }).OrderByDescending(a => a.EODRecordID).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    if (v.EODRefFK == 1 || v.EODRefFK == 2 || v.EODRefFK == 3)
                    {
                        gSalary += v.Amount;
                    }
                }
            }
            if (vData.Any(a => a.EODRefFK == 1))
            {
                this.EffectDate = vData.Where(a => a.EODRefFK == 1).OrderByDescending(a => a.EODRecordID).FirstOrDefault().EffectDate;
            }
            if (vData.Any(a => a.EODRefFK == 11 && a.Amount > 0) && vData.Any(a => a.EODRefFK == 7 && a.Amount > 0))
            {
                this.OTAllow = 1;
                this.OTPerRate = vData.FirstOrDefault(a => a.EODRefFK == 11).Amount;
                this.AttendenceBonus = vData.FirstOrDefault(a => a.EODRefFK == 7).Amount;
            }
            else if (vData.Any(a => a.EODRefFK == 11 && a.Amount > 0) && !vData.Any(a => a.EODRefFK == 7 && a.Amount > 0))
            {
                this.OTAllow = 0;
                this.OTPerRate = vData.FirstOrDefault(a => a.EODRefFK == 11).Amount;
            }
            else if (!vData.Any(a => a.EODRefFK == 11 && a.Amount > 0) && vData.Any(a => a.EODRefFK == 7 && a.Amount > 0))
            {
                this.OTAllow = 2;
                this.AttendenceBonus = vData.FirstOrDefault(a => a.EODRefFK == 7).Amount;
            }
            else
            {
                this.OTAllow = null;
            }
            this.GrossSalary = gSalary;
        }
        
        #region GetAuditGrossSalary
        public void GetAuditGrossSalary(int EmployeeID)
        {
            decimal gSalary = decimal.Zero;
            var vData = (from t1 in db.HRMHistoryEOD
                         join t2 in db.HrmsEodReferences on t1.Eod_RefFk equals t2.ID
                         where t1.EmployeeId == EmployeeID && t2.Start == 1 && t1.Status == true
                         select new
                         {
                             EODRecordID = t1.ID,
                             EODRefFK = t2.ID,
                             SalaryItem = t2.Name,
                             Amount = t1.ActualAmount,
                             EmployeeID = EmployeeID,
                             EffectDate = t1.EffectiveDate,
                             Priority = t2.priority
                         }).OrderByDescending(a => a.EODRecordID).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    if (v.EODRefFK == 1 || v.EODRefFK == 2 || v.EODRefFK == 3)
                    {
                        gSalary += v.Amount;
                    }
                }
            }
            if (vData.Any(a => a.EODRefFK == 1))
            {
                this.EffectDate = vData.Where(a => a.EODRefFK == 1).OrderByDescending(a => a.EODRecordID).FirstOrDefault().EffectDate;
            }
            if (vData.Any(a => a.EODRefFK == 11 && a.Amount > 0) && vData.Any(a => a.EODRefFK == 7 && a.Amount > 0))
            {
                this.OTAllow = 1;
                this.OTPerRate = vData.FirstOrDefault(a => a.EODRefFK == 11).Amount;
                this.AttendenceBonus = vData.FirstOrDefault(a => a.EODRefFK == 7).Amount;
            }
            else if (vData.Any(a => a.EODRefFK == 11 && a.Amount > 0) && !vData.Any(a => a.EODRefFK == 7 && a.Amount > 0))
            {
                this.OTAllow = 0;
                this.OTPerRate = vData.FirstOrDefault(a => a.EODRefFK == 11).Amount;
            }
            else if (!vData.Any(a => a.EODRefFK == 11 && a.Amount > 0) && vData.Any(a => a.EODRefFK == 7 && a.Amount > 0))
            {
                this.OTAllow = 2;
                this.AttendenceBonus = vData.FirstOrDefault(a => a.EODRefFK == 7).Amount;
            }
            else
            {
                this.OTAllow = null;
            }
            this.GrossSalary = gSalary;
        }
        #endregion

        public void GetEmployeeSalaryDelete(int EmployeeID)
        {
            var vData = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Status == true);

            if (vData.Any(a => a.Eod_RefFk == 1 && a.ActualAmount == 0))
            {
                foreach (var delete in vData)
                {
                    db.HrmsEodRecords.Remove(delete);
                }
            }

            db.SaveChanges();
        }

        public void GetEmployeeByDesignationID(int DesignationID, decimal BasicSalary)
        {
            var vData = db.Employees.Where(a => a.Designation_Id == DesignationID && a.Status == true);
            List<VMEmployee> lst = new List<VMEmployee>();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    VMEmployee model = new VMEmployee();
                    model.ID = v.ID;
                    model.Name = v.Name;
                    model.Name_BN = v.Name_BN;
                    model.CardTitle = v.CardTitle;
                    model.CardNo = v.CardNo;
                    model.Grade = v.Grade;
                    model.BasicPay = BasicSalary;
                    lst.Add(model);
                }
            }
            this.ListEmployee = lst;
        }

        public void GetPayrollDetails(int PayrollId)
        {
            var getPayroll = from o in db.HrmsPayrolls
                             where o.ID == PayrollId
                             select new
                             {
                                 o
                             };
            if (getPayroll.Any())
            {
                var takePayroll = getPayroll.FirstOrDefault();
                this.PayRollName = takePayroll.o.Note + "|" + Helpers.PeriodicShortDateString(takePayroll.o.FromDate, takePayroll.o.ToDate);
                this.FromDate = takePayroll.o.FromDate;
                this.ToDate = takePayroll.o.ToDate;
                this.PayRollID = takePayroll.o.ID;
                this.PaymentDate = DateTime.Today;
            }
        }

        public void CreatePayroll1(VM_HRMS_Payroll vModel)
        {
            if (db.HrmsPayrolls.Any(a => a.ClosePayroll == false))
            {
                this.PayrollStatus = "Another Payroll Already running. First Close that payroll than create a new";
            }
            else
            {
                HRMS_Payroll model = new HRMS_Payroll()
                {
                    Note = vModel.Note,
                    FromDate = vModel.FromDate,
                    ToDate = vModel.ToDate,
                    ClosePayroll = false,
                    Entry_By = vModel.Entry_By,
                    Entry_Date = vModel.Entry_Date,
                    Update_By = vModel.Entry_By,
                    Update_Date = vModel.Entry_Date
                };

                db.HrmsPayrolls.Add(model);
            }
        }

        public void ClosePayroll(int ID)
        {
            
            var closePayroll = db.HrmsPayrolls.Where(a => a.ID == ID);
            if (closePayroll.Any())
            {
                closePayroll.FirstOrDefault().ClosePayroll = true;
            }
        }

        public void TestPayroll(int ID)
        {
            HrmsContext db = new HrmsContext();
            var test = db.HrmsPayrolls.Where(a => a.ID == ID);
            if (test.Any())
            {
                var a = test.FirstOrDefault();
                a.IsTest = true;
                db.SaveChanges();
            }
          
          

        }

        public void GetProcessPayroll(VM_HRMS_Payroll model)
        {
            DropDownData dr = new DropDownData();
            model.CardTitle = dr.GetCardTitle(model.CardID);

            #region PayrollProcessForGeneralAndNewJoined
            var empList = db.Employees
                //.Where(a => a.ID == 3382)
                .Where(a => a.Present_Status == 1 && a.CardTitle.Equals(model.CardTitle) && a.Unit_Id == model.UnitID && a.Joining_Date <= model.ToDate)
                .Select(a => new { ID = a.ID, joindate = a.Joining_Date }).ToList();
            if (empList.Any())
            {
                foreach (var emp in empList)
                {
                    var check = db.HrmsPayrollInfos.Where(a => a.EmployeeIdFk == emp.ID && a.PayrollFK == model.PayRollID);
                    if (!check.Any())
                    {
                        var eodRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == emp.ID && a.Status == true).ToList();
                        if (eodRecord.Any())
                        {
                            foreach (var eod in eodRecord)
                            {
                                if (eod.Eod_RefFk != 7)
                                {
                                    HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                                    {
                                        PayrollFK = model.PayRollID,
                                        EmployeeIdFk = emp.ID,
                                        Eod_ReferenceFk = eod.Eod_RefFk,
                                        Amount = eod.ActualAmount,
                                        PaymentDate = model.PaymentDate,
                                        Status = true,
                                        Entry_By = model.Entry_By,
                                        Entry_Date = model.Entry_Date,
                                        Update_Date = model.Update_Date,
                                        Update_By = model.Update_By
                                    };
                                    db.HrmsPayrollInfos.Add(payrollInfo);
                                }
                            }
                        }
                        CalculateAttendanceEOD(model, emp.ID, emp.joindate);
                    }
                }
            }
            #endregion

            #region PayrollProcessForLeftyTerminate
            var leftyEmpList = db.Employees
                .Where(a => a.Present_Status == 3 || a.Present_Status == 4 && a.CardTitle.Equals(model.CardTitle) && a.Unit_Id == model.UnitID && a.QuitDate > model.FromDate && a.QuitDate <= ToDate)
                .Select(a => new { ID = a.ID, leftDate = a.QuitDate }).ToList();

            if (leftyEmpList.Any())
            {
                foreach (var emp in leftyEmpList)
                {
                    var check = db.HrmsPayrollInfos.Where(a => a.EmployeeIdFk == emp.ID && a.PayrollFK == model.PayRollID);
                    if (!check.Any())
                    {
                        var eodRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == emp.ID && a.Status == true).ToList();
                        if (eodRecord.Any())
                        {
                            foreach (var eod in eodRecord)
                            {
                                if (eod.Eod_RefFk != 7)
                                {
                                    HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                                    {
                                        PayrollFK = model.PayRollID,
                                        EmployeeIdFk = emp.ID,
                                        Eod_ReferenceFk = eod.Eod_RefFk,
                                        Amount = eod.ActualAmount,
                                        PaymentDate = model.PaymentDate,
                                        Status = true,
                                        Entry_By = model.Entry_By,
                                        Entry_Date = model.Entry_Date,
                                        Update_Date = model.Update_Date,
                                        Update_By = model.Update_By
                                    };
                                    db.HrmsPayrollInfos.Add(payrollInfo);
                                }
                            }
                        }
                        CalculateAttendanceEODForLefty(model, emp.ID, emp.leftDate);
                    }
                }
            }
            #endregion

            #region PayrollProcessResign

            var regineEmpList = db.Employees
                .Where(a => a.Present_Status == 2 && a.CardTitle.Equals(model.CardTitle) && a.Unit_Id == model.UnitID && a.QuitDate > model.FromDate && a.QuitDate <= ToDate)
                .Select(a => new { ID = a.ID, resignDate = a.QuitDate }).ToList();
            if (regineEmpList.Any())
            {
                foreach (var emp in regineEmpList)
                {
                    var check = db.HrmsPayrollInfos.Where(a => a.EmployeeIdFk == emp.ID && a.PayrollFK == model.PayRollID);
                    if (!check.Any())
                    {
                        var eodRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == emp.ID && a.Status == true).ToList();
                        if (eodRecord.Any())
                        {
                            foreach (var eod in eodRecord)
                            {
                                if (eod.Eod_RefFk != 7)
                                {
                                    HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                                    {
                                        PayrollFK = model.PayRollID,
                                        EmployeeIdFk = emp.ID,
                                        Eod_ReferenceFk = eod.Eod_RefFk,
                                        Amount = eod.ActualAmount,
                                        PaymentDate = model.PaymentDate,
                                        Status = true,
                                        Entry_By = model.Entry_By,
                                        Entry_Date = model.Entry_Date,
                                        Update_Date = model.Update_Date,
                                        Update_By = model.Update_By
                                    };
                                    db.HrmsPayrollInfos.Add(payrollInfo);
                                }
                            }
                        }
                        CalculateAttendanceEODForResign(model, emp.ID, emp.resignDate);
                    }
                }
            }
            #endregion
        }

        public void GetProcessRegularPayroll(VM_HRMS_Payroll model)
        {
            DropDownData dr = new DropDownData();
            model.CardTitle = dr.GetCardTitle(model.CardID);
            #region PayrollProcess

            var vEmployee = db.Employees
                //.Where(a => a.ID == 3382)
                .Where(a => a.CardTitle.Equals(model.CardTitle) && a.Unit_Id == model.UnitID)
                .Select(a => new { ID = a.ID, joindate = a.Joining_Date, resignDate = a.QuitDate, Status = a.Present_Status }).ToList();

            var AllPayrollInfo = db.HrmsPayrollInfos.Where(x => x.PayrollFK == model.PayRollID).Join(db.Employees.Where(x => x.CardTitle.Equals(model.CardTitle)), a => a.EmployeeIdFk, b => b.ID, (a, b) => new { EmployeeID = a.EmployeeIdFk, RefId = a.Eod_ReferenceFk }).ToList();

            var AllEmployeeEOD = db.HrmsEodRecords.Where(x => x.Status == true).Join(db.Employees.Where(x => x.CardTitle.Equals(model.CardTitle)), a => a.EmployeeId, b => b.ID, (a, b) => new { EmployeeID = a.EmployeeId, RefId = a.Eod_RefFk, Amount = a.ActualAmount }).ToList();

            if (vEmployee.Any())
            {
                foreach (var emp in vEmployee)
                {
                    model.EmployeeID = emp.ID;
                    model.JoinDate = emp.joindate;
                    model.ResignDate = emp.resignDate;
                    model.QuitDate = emp.resignDate;

                    var GetEmployeePayroll = AllPayrollInfo.Where(a => a.EmployeeID == model.EmployeeID).ToList();
                    if (!GetEmployeePayroll.Any())
                    {
                        #region ProcessPayroll
                        var eodRecord = AllEmployeeEOD.Where(a => a.EmployeeID == model.EmployeeID).ToList();
                        if (eodRecord.Any())
                        {
                            if (model.FromDate > model.JoinDate && emp.Status == 1)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.RefId != 7)
                                    {
                                        HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.RefId,
                                            Amount = eod.Amount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HrmsPayrollInfos.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Regular Employee
                                ProcessRegularAttendanceEOD(model);
                            }
                            else if (model.FromDate <= model.JoinDate && model.ToDate >= model.JoinDate && emp.Status == 1)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.RefId != 7)
                                    {
                                        HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.RefId,
                                            Amount = eod.Amount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HrmsPayrollInfos.Add(payrollInfo);
                                    }
                                }
                                //Calculate For NewJoined Employee
                                ProcessNewJoinedAttendanceEOD(model);
                            }
                            else if (model.FromDate < model.ResignDate && model.ToDate >= model.ResignDate && emp.Status == 2)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.RefId != 7)
                                    {
                                        HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.RefId,
                                            Amount = eod.Amount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HrmsPayrollInfos.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Resign Employee
                                ProcessResignAttendanceEOD(model);
                            }
                            else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && emp.Status == 3)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.RefId != 7)
                                    {
                                        HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.RefId,
                                            Amount = eod.Amount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HrmsPayrollInfos.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Lefty Employee
                                ProcessLeftyAttendanceEOD(model);
                            }
                            else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && emp.Status == 4)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.RefId != 7)
                                    {
                                        HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.RefId,
                                            Amount = eod.Amount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HrmsPayrollInfos.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Terminate Employee
                                ProcessLeftyAttendanceEOD(model);
                            }
                        }

                        #endregion
                    }
                    #region DeedCode
                    //if (GetEmployeePayroll.Any())
                    //{
                    //    model.IsReprocessExecute = true;
                    //    #region ReprocessPayroll

                    //    var GetEmployeeEOD = AllEmployeeEOD.Where(a=>a.EmployeeID==model.EmployeeID).ToList();
                    //    if (GetEmployeeEOD.Any())
                    //    {
                    //        foreach (var v in GetEmployeeEOD)
                    //        {
                    //            if (v.RefId != 7)
                    //            {
                    //                var GetUpdate = db.HrmsPayrollInfos.Where(a => a.EmployeeIdFk == v.EmployeeID && a.PayrollFK == model.PayRollID && a.Eod_ReferenceFk == v.RefId);
                    //                if (GetUpdate.Any())
                    //                {
                    //                    var update = GetUpdate.FirstOrDefault(a => a.EmployeeIdFk == v.EmployeeID && a.PayrollFK == model.PayRollID && a.Eod_ReferenceFk == v.RefId);
                    //                    update.Amount = v.Amount;
                    //                }
                    //                else
                    //                {
                    //                    HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                    //                    {
                    //                        PayrollFK = model.PayRollID,
                    //                        EmployeeIdFk = model.EmployeeID,
                    //                        Eod_ReferenceFk = v.RefId,
                    //                        Amount = v.Amount,
                    //                        PaymentDate = model.PaymentDate,
                    //                        Status = true,
                    //                        Entry_By = model.Entry_By,
                    //                        Entry_Date = model.Entry_Date,
                    //                        Update_Date = model.Update_Date,
                    //                        Update_By = model.Update_By
                    //                    };
                    //                    db.HrmsPayrollInfos.Add(payrollInfo);
                    //                }
                    //            }
                    //        }

                    //        #region ReprocessAttendance
                    //        if (model.FromDate > model.JoinDate && emp.Status == 1)
                    //        {
                    //            //Calculate For Regular Employee
                    //            ProcessRegularAttendanceEOD(model);
                    //        }
                    //        else if (model.FromDate =< model.JoinDate && model.ToDate >= model.JoinDate && emp.Status == 1)
                    //        {
                    //            //Calculate For NewJoined Employee
                    //            ProcessNewJoinedAttendanceEOD(model);
                    //        }
                    //        else if (model.FromDate < model.ResignDate && model.ToDate >= model.ResignDate && emp.Status == 2)
                    //        {
                    //            //Calculate For Resign Employee
                    //            ProcessResignAttendanceEOD(model);
                    //        }
                    //        else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && emp.Status == 3)
                    //        {
                    //            //Calculate For Lefty Employee
                    //            ProcessLeftyAttendanceEOD(model);
                    //        }
                    //        else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && emp.Status == 4)
                    //        {
                    //            //Calculate For Terminate Employee
                    //            ProcessLeftyAttendanceEOD(model);
                    //        }
                    //        #endregion
                    //    }

                    //    #endregion
                    //}
                    #endregion
                }
            }
            #endregion
        }

        private void ProcessRegularAttendanceEOD(VM_HRMS_Payroll model)
        {
            VMAttendance m = new VMAttendance();
            m = GetRegularPrimaryEOD(model.EmployeeID);

            #region ManageAttendanceRecord
            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAttendance(model.EmployeeID, model.FromDate, model.ToDate);
            var attendance = dbhelpers.VMAttendanceSummary;
            m.OffDays = attendance.OffDays + attendance.HoliDays;
            m.TotalWorkDays = attendance.WorkDays;
            m.TotalAdsentDays = attendance.AbsentDays;
            m.TotalPaidLeave = attendance.TotalLeaveDays;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.TotalDeduction = m.TotalAdsentDays * m.PerDayPay;
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;

            if (m.TotalAdsentDays > 0 || attendance.TotalLeaveDays > 0 || attendance.LateDay > 2)
            {
                m.AttendanceBonus = 0;
            }
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;

            #endregion
            List<EODReference> lstEod = new List<EODReference>();
            lstEod = SetEODRefecence(m, model);
            if (model.IsReprocessExecute)
            {
                UpdatePayroll(lstEod, true);
            }
            else
            {
                SavePayroll(lstEod, true);
            }
        }

        private void ProcessNewJoinedAttendanceEOD(VM_HRMS_Payroll model)
        {
            int beforeJoinAbsentDays = 0;
            decimal beforeJoinAbsentTaka = 0;
            decimal afterJoinAbsentTaka = 0;
            VMAttendance m = new VMAttendance();
            m = GetRegularPrimaryEOD(model.EmployeeID);

            #region ManageAttendanceRecord
            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAttendanceForNewJoin(model.EmployeeID, model.FromDate, model.ToDate, model.JoinDate);
            var attendance = dbhelpers.VMAttendanceSummary;

            m.AttendanceBonus = 0;
            m.TotalWorkDays = attendance.WorkDays;
            m.TotalAdsentDays = attendance.AbsentDays;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.OffDays = attendance.OffDays + attendance.HoliDays;
            m.GrossPerDayPay = m.GrossPay / this.DaysOfMonth;
            beforeJoinAbsentDays = m.TotalAdsentDays - attendance.JoinedAbsentDays;
            beforeJoinAbsentTaka = m.GrossPerDayPay * beforeJoinAbsentDays;
            afterJoinAbsentTaka = m.PerDayPay * attendance.JoinedAbsentDays;
            m.TotalDeduction = Math.Round(beforeJoinAbsentTaka + afterJoinAbsentTaka);
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion

            List<EODReference> lstEod = new List<EODReference>();
            lstEod = SetEODRefecence(m, model);
            if (model.IsReprocessExecute)
            {
                UpdatePayroll(lstEod, true);
            }
            else
            {
                SavePayroll(lstEod, true);
            }
        }

        private void ProcessResignAttendanceEOD(VM_HRMS_Payroll model)
        {
            VMAttendance m = new VMAttendance();
            m = GetRegularPrimaryEOD(model.EmployeeID);

            #region ManageAttendanceRecord
            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAttendanceForLefty(model.EmployeeID, model.FromDate, model.ToDate, model.ResignDate);
            var attendance = dbhelpers.VMAttendanceSummary;

            m.TotalWorkDays = attendance.WorkDays;
            m.TotalAdsentDays = attendance.AbsentDays;
            m.OffDays = attendance.OffDays + attendance.HoliDays;
            if (model.ToDate > model.ResignDate && model.FromDate <= model.ResignDate)
            {
                m.TotalDeduction = m.TotalAdsentDays * m.GrossPerDayPay;
                m.AttendanceBonus = 0;
            }
            else if (model.ToDate == model.ResignDate)
            {
                m.TotalDeduction = m.TotalAdsentDays * m.PerDayPay;
            }
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion

            List<EODReference> lstEod = new List<EODReference>();
            lstEod = SetEODRefecence(m, model);
            if (model.IsReprocessExecute)
            {
                UpdatePayroll(lstEod, true);
            }
            else
            {
                SavePayroll(lstEod, true);
            }
        }

        private void ProcessLeftyAttendanceEOD(VM_HRMS_Payroll model)
        {
            VMAttendance m = new VMAttendance();
            m = GetRegularPrimaryEOD(model.EmployeeID);

            #region ManageAttendanceRecord
            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAttendanceForLefty(model.EmployeeID, model.FromDate, model.ToDate, model.QuitDate);
            var attendance = dbhelpers.VMAttendanceSummary;

            m.OffDays = attendance.OffDays + attendance.HoliDays;
            m.TotalAdsentDays = attendance.AbsentDays;
            m.TotalWorkDays = attendance.WorkDays;
            m.TotalDeduction = m.GrossPerDayPay * m.TotalAdsentDays;
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion
            List<EODReference> lstEod = new List<EODReference>();
            lstEod = SetEODRefecence(m, model);
            if (model.IsReprocessExecute)
            {
                UpdatePayroll(lstEod, false);
            }
            else
            {
                SavePayroll(lstEod, false);
            }
        }

        private void EmployeeEODUpdateAlert(int EmployeeID)
        {
            var getEmployeeEODRecord = (from eod in db.HrmsEodRecords
                                        join emp in db.Employees on eod.EmployeeId equals emp.ID
                                        where emp.ID == EmployeeID
                                        && eod.Eod_RefFk == 1
                                        && eod.Status == true
                                        select new
                                        {
                                            eod,
                                            emp
                                        }).ToList();

            if (getEmployeeEODRecord.Any())
            {
                var takeFirst = getEmployeeEODRecord.FirstOrDefault();

                DateTime date = takeFirst.eod.EffectiveDate.AddDays(365);
                DateTime today = DateTime.Today;
                if (date >= today)
                {
                    takeFirst.emp.AlertRemark = "Joined before 1 year but record is not updated";
                    takeFirst.emp.AlertActive = true;
                }
            }
        }

        public void GetUpdateEmployeePayroll(VM_HRMS_Payroll model)
        {
            var vPayroll = (from o in db.HrmsPayrollInfos
                            join p in db.Employees on o.EmployeeIdFk equals p.ID
                            join q in db.HrmsPayrolls on o.PayrollFK equals q.ID
                            where o.PayrollFK==model.PayRollID
                            select new
                            {
                                ID = p.ID,
                                joindate = p.Joining_Date,
                                resignDate = p.QuitDate,
                                Status = p.Present_Status,
                                FromDate = q.FromDate,
                                ToDate = q.ToDate,
                                PaymentDate = o.PaymentDate
                            }).FirstOrDefault();

            model.FromDate = vPayroll.FromDate;
            model.ToDate = vPayroll.ToDate;
            model.PaymentDate = vPayroll.PaymentDate;
            model.JoinDate = vPayroll.joindate;
            model.ResignDate = vPayroll.resignDate;
            model.QuitDate = vPayroll.resignDate;

            var empSalaryRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmployeeID && a.Status == true).ToList();
            if (empSalaryRecord.Any())
            {
                model.IsReprocessExecute = true;
                var GetEmployeePayroll = db.HrmsPayrollInfos.Where(a => a.EmployeeIdFk == model.EmployeeID && a.PayrollFK == model.PayRollID);
                foreach (var eod in empSalaryRecord)
                {
                    if (eod.Eod_RefFk != 7)
                    {
                        if (GetEmployeePayroll.Any(a => a.Eod_ReferenceFk == eod.Eod_RefFk))
                        {
                            var updateInfo = GetEmployeePayroll.FirstOrDefault(a => a.Eod_ReferenceFk == eod.Eod_RefFk);
                            updateInfo.Amount = eod.ActualAmount;
                            updateInfo.Update_By = model.Update_By;
                            updateInfo.Update_Date = model.Update_Date;
                        }
                        else
                        {
                            HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                            {
                                PayrollFK = model.PayRollID,
                                EmployeeIdFk = model.EmployeeID,
                                Eod_ReferenceFk = eod.Eod_RefFk,
                                Amount = eod.ActualAmount,
                                PaymentDate = model.PaymentDate,
                                Status = true,
                                Update_Date = model.Update_Date,
                                Update_By = model.Update_By,
                                Entry_By = model.Update_By,
                                Entry_Date = model.Update_Date
                            };
                            db.HrmsPayrollInfos.Add(payrollInfo);
                        }
                    }
                }

                #region ReprocessAttendance
                if (model.FromDate > model.JoinDate && vPayroll.Status == 1)
                {
                    //Calculate For Regular Employee
                    ProcessRegularAttendanceEOD(model);
                }
                else if (model.FromDate < model.JoinDate && model.ToDate >= model.JoinDate && vPayroll.Status == 1)
                {
                    //Calculate For NewJoined Employee
                    ProcessNewJoinedAttendanceEOD(model);
                }
                else if (model.FromDate < model.ResignDate && model.ToDate >= model.ResignDate && vPayroll.Status == 2)
                {
                    //Calculate For Resign Employee
                    ProcessResignAttendanceEOD(model);
                }
                else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && vPayroll.Status == 3)
                {
                    //Calculate For Lefty Employee
                    ProcessLeftyAttendanceEOD(model);
                }
                else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && vPayroll.Status == 4)
                {
                    //Calculate For Terminate Employee
                    ProcessLeftyAttendanceEOD(model);
                }
                #endregion

                db.SaveChanges();
            }
        }

        #region GeneralPayroll

        public void GetSalaryList()
        {
            var vData = (from d in db.HrmsEodRecords
                         join a in db.Employees on d.EmployeeId equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join c in db.Sections on a.Section_Id equals c.ID
                         join e in db.HrmsEodReferences on d.Eod_RefFk equals e.ID
                         where a.Present_Status == 1 && d.Status == true
                         select new
                         {
                             EmpID = a.ID,
                             name = a.Name_BN,
                             engName = a.Name,
                             CardNo = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Designation = b.Name,
                             Section = c.SectionName,
                             RefId = e.ID,
                             HeadName = e.Name,
                             Amount = d.ActualAmount,
                             IsRegular = e.Regular,
                             priority = e.priority
                         }).OrderBy(a => a.Section).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.engName, o.CardNo, o.CardTitle, o.Section, o.Designation } into all
                            select new
                            {
                                EmpID = all.Key.EmpID,
                                Name = all.Key.engName,
                                CardNo = all.Key.CardNo,
                                CardTitle = all.Key.CardTitle,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID
                                            select new
                                            {
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                priority = p.priority,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList(),
                                //IsCompliance=db.HRMHistoryEOD.Any(a=>a.EmployeeId== all.Key.EmpID && a.Status==true)
                            }).OrderBy(a => a.Section).ToList();

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("BasicPay", typeof(string));
            ds.Columns.Add("House", typeof(string));
            ds.Columns.Add("Other", typeof(string));
            ds.Columns.Add("Total", typeof(string));
            ds.Columns.Add("OT.Rate", typeof(string));
            ds.Columns.Add("AttendanceBonus", typeof(string));
            ds.Columns.Add("Compliance", typeof(Boolean));
            ds.Rows.Clear();
            if (getGroup.Any())
            {
                int count = 0;
                foreach (var v in getGroup)
                {
                    ds.Rows.Add(
                        v.EmpID,
                        v.CardTitle + " " + v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.Designation,
                        v.Section,
                        decimal.Zero,//BasicPay6
                        decimal.Zero,//HouseAllowance7
                        decimal.Zero,//HealthFoodTravel8
                        decimal.Zero,//TotalSalary9
                        decimal.Zero,//OTRate10
                        decimal.Zero//AttendanceBonus11
                                    //v.IsCompliance
                        );
                    if (v.HeadItem.Any(a => a.RefId == 1))
                    {
                        var BasicPay = v.HeadItem.FirstOrDefault(a => a.RefId == 1);
                        ds.Rows[count][6] = BasicPay.Amount;
                        var HouseAllowance = v.HeadItem.FirstOrDefault(a => a.RefId == 2);
                        ds.Rows[count][7] = HouseAllowance.Amount;
                        var OtherAllowance = v.HeadItem.FirstOrDefault(a => a.RefId == 3);
                        ds.Rows[count][8] = OtherAllowance.Amount;
                        ds.Rows[count][9] = BasicPay.Amount + HouseAllowance.Amount + OtherAllowance.Amount;
                        if (v.HeadItem.Any(a => a.RefId == 11))
                        {
                            var OTRate = v.HeadItem.FirstOrDefault(a => a.RefId == 11);
                            ds.Rows[count][10] = OTRate.Amount;
                        }

                        if (v.HeadItem.Any(a => a.RefId == 7))
                        {
                            var AttendanceBonus = v.HeadItem.FirstOrDefault(a => a.RefId == 7);
                            ds.Rows[count][11] = AttendanceBonus.Amount;
                        }
                    }
                    ++count;
                }
            }
            List<VM_HRMS_Payroll> lst = new List<VM_HRMS_Payroll>();
            if (ds.Rows.Count > 0)
            {
                for (int counter = 0; counter < ds.Rows.Count; counter++)
                {
                    decimal basic = 0;
                    decimal.TryParse(ds.Rows[counter][6].ToString(), out basic);
                    VM_HRMS_Payroll model = new VM_HRMS_Payroll();
                    model.BasicAmount = basic;
                    model.EmployeeID = (int)ds.Rows[counter][0];
                    model.EmployeeIdentity = ds.Rows[counter][1].ToString();
                    model.Title = ds.Rows[counter][2].ToString();
                    model.EmployeeName = ds.Rows[counter][3].ToString();
                    model.Designation = ds.Rows[counter][4].ToString();
                    model.Section = ds.Rows[counter][5].ToString();
                    model.BasicPay = ds.Rows[counter][6].ToString();
                    model.HousePay = ds.Rows[counter][7].ToString();
                    model.OtherPay = ds.Rows[counter][8].ToString();
                    model.TotalPay = ds.Rows[counter][9].ToString();
                    model.OTRate = ds.Rows[counter][10].ToString();
                    model.AttendenceBonuses = ds.Rows[counter][11].ToString();
                    //model.AddCompliance= (bool)ds.Rows[counter][12];
                    lst.Add(model);
                }
            }
            this.PayrollDataList = lst.OrderBy(a => a.Section).ThenBy(a => a.EmployeeIdentity).ToList();
        }

        public void GetPayrollList(int PayrollId)
        {
            var vData = (from d in db.HrmsPayrollInfos
                         join a in db.Employees on d.EmployeeIdFk equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join c in db.Sections on a.Section_Id equals c.ID
                         join e in db.HrmsEodReferences on d.Eod_ReferenceFk equals e.ID
                         where d.PayrollFK == PayrollId && d.Status == true
                         && d.Status == true
                         select new
                         {
                             EmpID = a.ID,
                             name = a.Name_BN,
                             engName = a.Name,
                             CardNo = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Designation = b.Name,
                             Section = c.SectionName,
                             RefId = e.ID,
                             HeadName = e.Name,
                             Amount = d.Amount,
                             IsRegular = e.Regular,
                             priority = e.priority,
                             AlertActive = a.AlertActive,
                         }).OrderBy(a => a.Section).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.engName, o.CardNo, o.CardTitle, o.Section, o.Designation, o.AlertActive } into all
                            select new
                            {
                                EmpID = all.Key.EmpID,
                                Name = all.Key.engName,
                                CardNo = all.Key.CardNo,
                                CardTitle = all.Key.CardTitle,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                AlertActive = all.Key.AlertActive,
                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID
                                            select new
                                            {
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                priority = p.priority,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList()
                            }).OrderBy(a => a.Section).ToList();

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("BasicPay", typeof(string));
            ds.Columns.Add("House", typeof(string));
            ds.Columns.Add("Other", typeof(string));
            ds.Columns.Add("Total", typeof(string));
            ds.Columns.Add("OT.Rate", typeof(string));
            ds.Columns.Add("AttendanceBonus", typeof(string));
            //ds.Columns.Add("AlertActive", typeof(string));
            ds.Rows.Clear();
            if (getGroup.Any())
            {
                int count = 0;
                foreach (var v in getGroup)
                {
                    ds.Rows.Add(
                        v.EmpID,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.Designation,
                        v.Section,
                        decimal.Zero,//BasicPay6
                        decimal.Zero,//HouseAllowance7
                        decimal.Zero,//HealthFoodTravel8
                        decimal.Zero,//TotalSalary9
                        decimal.Zero,//OTRate10
                        decimal.Zero//AttendanceBonus11,
                                    //v.AlertActive
                        );
                    if (v.HeadItem.Any())
                    {
                        var BasicPay = v.HeadItem.FirstOrDefault(a => a.RefId == 1);
                        ds.Rows[count][6] = BasicPay.Amount;
                        var HouseAllowance = v.HeadItem.FirstOrDefault(a => a.RefId == 2);
                        ds.Rows[count][7] = HouseAllowance.Amount;
                        var OtherAllowance = v.HeadItem.FirstOrDefault(a => a.RefId == 3);
                        ds.Rows[count][8] = OtherAllowance.Amount;
                        ds.Rows[count][9] = BasicPay.Amount + HouseAllowance.Amount + OtherAllowance.Amount;
                        var OTRate = v.HeadItem.FirstOrDefault(a => a.RefId == 11);
                        ds.Rows[count][10] = OTRate.Amount;
                        if (v.HeadItem.Any(a => a.RefId == 7))
                        {
                            var AttendanceBonus = v.HeadItem.FirstOrDefault(a => a.RefId == 7);
                            ds.Rows[count][11] = AttendanceBonus.Amount;
                        }
                    }
                    ++count;
                }
            }

            if (ds.Rows.Count > 0)
            {
                List<VM_HRMS_Payroll> lst = new List<VM_HRMS_Payroll>();

                for (int counter = 0; counter < ds.Rows.Count; counter++)
                {
                    VM_HRMS_Payroll model = new VM_HRMS_Payroll();
                    model.EmployeeID = (int)ds.Rows[counter][0];
                    model.EmployeeIdentity = ds.Rows[counter][1].ToString();
                    model.Title = ds.Rows[counter][2].ToString();
                    model.EmployeeName = ds.Rows[counter][3].ToString();
                    model.Designation = ds.Rows[counter][4].ToString();
                    model.Section = ds.Rows[counter][5].ToString();
                    model.BasicPay = ds.Rows[counter][6].ToString();
                    model.HousePay = ds.Rows[counter][7].ToString();
                    model.OtherPay = ds.Rows[counter][8].ToString();
                    model.TotalPay = ds.Rows[counter][9].ToString();
                    model.OTRate = ds.Rows[counter][10].ToString();
                    model.AttendenceBonuses = ds.Rows[counter][11].ToString();
                    //model.AlertActive = ds.Rows[counter][11].ToString();
                    lst.Add(model);
                }
                this.PayrollDataList = lst.OrderBy(a => a.Section).ThenBy(a => a.EmployeeIdentity).ToList();
            }
        }

        public void GetEmployeeInfo(int EmployeeID)
        {
            var vData = from o in db.Employees
                        where o.ID == EmployeeID
                        select new
                        {
                            empID = o.ID,
                            name = o.CardTitle + "-" + o.EmployeeIdentity + " - " + o.Name,
                            Cardtitle = o.CardTitle,
                            type = o.Staff_Type,
                            joinDate = o.Joining_Date
                        };

            if (vData.Any())
            {
                var takeOne = vData.FirstOrDefault();
                this.EmployeeID = takeOne.empID;
                this.EmployeeName = takeOne.name;
                this.Title = takeOne.Cardtitle;
                this.EmployeeType = takeOne.type;
                this.JoinDate = takeOne.joinDate;
                if (this.EmployeeType.Equals("Worker"))
                {
                    if (this.Title.Equals("RFSH"))
                    {
                        this.AttendenceBonus = 300;
                    }
                    else
                    {
                        this.AttendenceBonus = 400;
                    }
                }
            }
        }

        public void GetCreateSalary(VM_HRMS_Payroll model)
        {
            model.HealthAllowance = this.Medical;
            model.FoodAllowance = this.Food;
            model.TransportAllowance = this.Transport;
            model.OtherAllowance = model.HealthAllowance + model.FoodAllowance + model.TransportAllowance;
            decimal basic = (model.GrossSalary - model.OtherAllowance) / this.Rate;
            //model.BasicAmount = Math.Round(basic);
            model.BasicAmount = basic;
            decimal house = decimal.Multiply(model.BasicAmount, this.HouseRate);
            //model.HouseAllowance = Math.Ceiling(house);
            model.HouseAllowance = house;
            model.OTPerRate = decimal.Multiply(decimal.Divide(model.BasicAmount,208),2);

            var referenceExistEod = db.HrmsEodReferences.Where(a => a.Status == true && a.Finish == 12).ToList();
            if (referenceExistEod.Any())
            {
                List<EODReference> createRecordList = new List<EODReference>();

                foreach (var v in referenceExistEod)
                {
                    EODReference item = new EODReference();
                    item.EODRefID = v.ID;
                    item.EmpID = model.EmployeeID;
                    item.SalaryHead = v.Name;
                    if (v.ID == 1)
                    {
                        item.Amount = model.BasicAmount;
                    }
                    else if (v.ID == 2)
                    {
                        item.Amount = model.HouseAllowance;
                    }
                    else if (v.ID == 3)
                    {
                        item.Amount = model.OtherAllowance;
                    }
                    else if (v.ID == 42)
                    {
                        item.Amount = model.TransportAllowance;
                    }
                    else if (v.ID == 43)
                    {
                        item.Amount = model.HealthAllowance;
                    }
                    else if (v.ID == 44)
                    {
                        item.Amount = model.FoodAllowance;
                    }
                    else if (v.ID == 11)
                    {
                        if (model.OTAllow != 2 && model.OTAllow != 3)
                        {
                            item.Amount = Math.Round(model.OTPerRate, 2);
                        }
                    }
                    else if (v.ID == 12)
                    {
                        item.Amount = v.CalculativeAmount;
                    }
                    else if (v.ID == 7)
                    {
                        if (model.OTAllow == 1 || model.OTAllow == 2)
                        {
                            item.Amount = model.AttendenceBonus;
                        }
                    }
                    createRecordList.Add(item);
                }

                if (createRecordList.Any())
                {
                    foreach (var v in createRecordList)
                    {
                        HRMS_EodRecord eodrecord = new HRMS_EodRecord
                        {
                            EmployeeId = v.EmpID,
                            Eod_RefFk = v.EODRefID,
                            ActualAmount = v.Amount,
                            EffectiveDate = model.JoinDate,
                            Status = true,
                            Entry_Date = model.Entry_Date,
                            Entry_By = model.Entry_By
                        };

                        db.HrmsEodRecords.Add(eodrecord);
                    }

                    //if (model.AddCompliance)
                    //{
                    //    foreach (var v in createRecordList)
                    //    {
                    //        if (v.EODRefID == 11)
                    //        {
                    //            v.Amount = Math.Round(model.OTPerRate, 2);
                    //        }
                    //        HRMHistoryEOD eodrecord = new HRMHistoryEOD
                    //        {
                    //            EmployeeId = v.EmpID,
                    //            Eod_RefFk = v.EODRefID,
                    //            ActualAmount = v.Amount,
                    //            EffectiveDate = model.JoinDate,
                    //            Status = true,
                    //            Entry_Date = model.Entry_Date,
                    //            Entry_By = model.Entry_By
                    //        };

                    //        db.HRMHistoryEOD.Add(eodrecord);
                    //    }
                    //}
                }

            }
        }

        public void GetUpdateSalary(VM_HRMS_Payroll model)
        {
            model.HealthAllowance = this.Medical;
            model.FoodAllowance = this.Food;
            model.TransportAllowance = this.Transport;
            model.OtherAllowance = model.HealthAllowance + model.FoodAllowance + model.TransportAllowance;
            decimal basic = decimal.Divide((model.GrossSalary - model.OtherAllowance), this.Rate);
            //model.BasicAmount = Math.Round(basic);
            model.BasicAmount =basic;
            decimal house = decimal.Multiply(model.BasicAmount, this.HouseRate);
            //model.HouseAllowance = Math.Ceiling(house);
            model.HouseAllowance = house;
            decimal OTRate = decimal.Multiply(decimal.Divide(model.BasicAmount, 208), 2);

            List<EODReference> updateRecordList = new List<EODReference>();
            #region ManageSalary
            var referenceExistEod = db.HrmsEodReferences.Where(a => a.Status == true && a.Start == 1).ToList();
            if (referenceExistEod.Any())
            {
                foreach (var itemRef in referenceExistEod)
                {
                    EODReference item = new EODReference();
                    item.EODRefID = itemRef.ID;
                    item.EmpID = model.EmployeeID;
                    if (itemRef.ID == 1)
                    {
                        item.Amount = model.BasicAmount;
                    }
                    else if (itemRef.ID == 2)
                    {
                        item.Amount = model.HouseAllowance;
                    }
                    else if (itemRef.ID == 3)
                    {
                        item.Amount = model.OtherAllowance;
                    }
                    else if (itemRef.ID == 42)
                    {
                        item.Amount = model.Transport;
                    }
                    else if (itemRef.ID == 43)
                    {
                        item.Amount = model.Medical;
                    }
                    else if (itemRef.ID == 44)
                    {
                        item.Amount = model.Food;
                    }
                    else if (itemRef.ID == 7)
                    {
                        if (model.OTAllow == 1 || model.OTAllow == 2)
                        {
                            item.Amount = model.AttendenceBonus;
                        }
                    }
                    else if (itemRef.ID == 11)
                    {
                        if (model.OTAllow != 2 && model.OTAllow != 3)
                        {
                            item.Amount = OTRate;
                        }
                    }

                    updateRecordList.Add(item);
                }
            }
            #endregion

            #region GeneralSalary
            var allExistRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmployeeID && a.Status == true).ToList();
            if (updateRecordList.Any())
            {
                foreach (var v in updateRecordList)
                {
                    var checkItemMultiple = allExistRecord.Where(a => a.Eod_RefFk == v.EODRefID).Count();
                    if (checkItemMultiple > 1)
                    {
                        var takeLast = allExistRecord.Where(a => a.Eod_RefFk == v.EODRefID).OrderByDescending(a => a.ID).FirstOrDefault();

                        foreach (var d in allExistRecord.Where(a => a.Eod_RefFk == v.EODRefID && a.ID!=takeLast.ID))
                        {
                            var update = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmployeeID && a.ID == d.ID).FirstOrDefault();
                            update.Status = false;
                            update.Update_By = model.Update_By;
                            update.Update_Date = model.Update_Date;
                        }
                    }
                    else if(checkItemMultiple==1)
                    {
                        var previousRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmployeeID && a.Eod_RefFk == v.EODRefID && a.Status==true).FirstOrDefault();
                        if (previousRecord.ActualAmount != v.Amount)
                        {
                            previousRecord.Status = false;
                            previousRecord.Update_By = model.Update_By;
                            previousRecord.Update_Date = model.Update_Date;

                            HRMS_EodRecord recordAdd = new HRMS_EodRecord()
                            {
                                EmployeeId = v.EmpID,
                                Eod_RefFk = v.EODRefID,
                                ActualAmount = v.Amount,
                                Status = true,
                                EffectiveDate = model.EffectDate,
                                Entry_Date = model.Update_Date,
                                Entry_By = model.Update_By
                            };
                            db.HrmsEodRecords.Add(recordAdd);
                        }
                    }
                    else if(checkItemMultiple <= 0)
                    {
                        HRMS_EodRecord recordAdd = new HRMS_EodRecord()
                        {
                            EmployeeId = v.EmpID,
                            Eod_RefFk = v.EODRefID,
                            ActualAmount = v.Amount,
                            Status = true,
                            EffectiveDate = model.EffectDate,
                            Entry_Date = model.Update_Date,
                            Entry_By = model.Update_By
                        };
                        db.HrmsEodRecords.Add(recordAdd);
                    }
                }

                db.SaveChanges();
            }
            #endregion
            
            #region ComplianceSalary
            //var checkComRecord = db.HRMHistoryEOD.Where(a => a.EmployeeId == model.EmployeeID && a.Status == true);
            //if (model.AddCompliance)
            //{
            //    if (checkComRecord.Any())
            //    {
            //        #region UpdateRecord
            //        if (updateRecordList.Any())
            //        {
            //            foreach (var updateRecord in updateRecordList)
            //            {
            //                if (updateRecord.EODRefID == 11)
            //                {
            //                    updateRecord.Amount = Math.Round(OTRate, 2);
            //                }

            //                if (checkComRecord.Any(a => a.Eod_RefFk == updateRecord.EODRefID))
            //                {
            //                    var previousRecord = checkComRecord.FirstOrDefault(a => a.Eod_RefFk == updateRecord.EODRefID);
            //                    if (previousRecord.ActualAmount != updateRecord.Amount)
            //                    {
            //                        previousRecord.Status = false;
            //                        previousRecord.Update_By = model.Update_By;
            //                        previousRecord.Update_Date = model.Update_Date;

            //                        HRMHistoryEOD recordAdd = new HRMHistoryEOD()
            //                        {
            //                            EmployeeId = updateRecord.EmpID,
            //                            Eod_RefFk = updateRecord.EODRefID,
            //                            ActualAmount = updateRecord.Amount,
            //                            Status = true,
            //                            EffectiveDate = model.EffectDate,
            //                            Entry_Date = model.Update_Date,
            //                            Entry_By = model.Update_By
            //                        };
            //                        db.HRMHistoryEOD.Add(recordAdd);
            //                    }
            //                }
            //                else
            //                {
            //                    #region AddRecordReference
            //                    HRMHistoryEOD recordAdd = new HRMHistoryEOD()
            //                    {
            //                        EmployeeId = updateRecord.EmpID,
            //                        Eod_RefFk = updateRecord.EODRefID,
            //                        ActualAmount = updateRecord.Amount,
            //                        Status = true,
            //                        EffectiveDate = model.EffectDate,
            //                        Entry_Date = model.Update_Date,
            //                        Entry_By = model.Update_By
            //                    };
            //                    db.HRMHistoryEOD.Add(recordAdd);
            //                    #endregion
            //                }
            //            }
            //        }
            //        #endregion
            //    }
            //    else
            //    {
            //        #region AddAllRecord
            //        var refAddExistEod = db.HrmsEodReferences.Where(a => a.Status == true && a.Finish == 12).ToList();
            //        if (refAddExistEod.Any())
            //        {

            //            model.OTPerRate = (model.BasicAmount / 208) * 2;
            //            model.HealthAllowance = 250;
            //            model.FoodAllowance = 650;
            //            model.TransportAllowance = 200;
            //            List<EODReference> createRecordList = new List<EODReference>();

            //            foreach (var v in refAddExistEod)
            //            {
            //                EODReference item = new EODReference();
            //                item.EODRefID = v.ID;
            //                item.EmpID = model.EmployeeID;
            //                item.SalaryHead = v.Name;
            //                if (v.ID == 1)
            //                {
            //                    item.Amount = model.BasicAmount;
            //                }
            //                else if (v.ID == 2)
            //                {
            //                    item.Amount = model.HouseAllowance;
            //                }
            //                else if (v.ID == 3)
            //                {
            //                    item.Amount = model.OtherAllowance;
            //                }
            //                else if (v.ID == 7)
            //                {
            //                    item.Amount = model.AttendenceBonus;
            //                }
            //                else if (v.ID == 11)
            //                {
            //                    item.Amount = Math.Round(model.OTPerRate, 2);
            //                }
            //                else if (v.ID == 12)
            //                {
            //                    item.Amount = v.CalculativeAmount;
            //                }
            //                else if (v.ID == 42)
            //                {
            //                    item.Amount = model.TransportAllowance;
            //                }
            //                else if (v.ID == 43)
            //                {
            //                    item.Amount = model.HealthAllowance;
            //                }
            //                else if (v.ID == 44)
            //                {
            //                    item.Amount = model.FoodAllowance;
            //                }
            //                createRecordList.Add(item);
            //            }

            //            if (createRecordList.Any())
            //            {
            //                foreach (var v in createRecordList)
            //                {
            //                    HRMHistoryEOD eodrecord = new HRMHistoryEOD
            //                    {
            //                        EmployeeId = v.EmpID,
            //                        Eod_RefFk = v.EODRefID,
            //                        ActualAmount = v.Amount,
            //                        Status = true,
            //                        EffectiveDate = model.EffectDate,
            //                        Entry_Date = model.Update_Date,
            //                        Entry_By = model.Update_By
            //                    };

            //                    db.HRMHistoryEOD.Add(eodrecord);
            //                }
            //            }

            //        }
            //        #endregion
            //    }
            //}
            //else
            //{
            //    #region DeActiveRecord
            //    if (checkComRecord.Any())
            //    {
            //        foreach (var update in checkComRecord)
            //        {
            //            update.Status = false;
            //            update.EffectiveDate = model.EffectDate;
            //            update.Entry_Date = model.Update_Date;
            //            update.Entry_By = model.Update_By;
            //        }
            //    }
            //    #endregion
            //}
            #endregion
        }

        #endregion

        #region PreviousMethod

        private void CalculateAttendanceEOD(VM_HRMS_Payroll model, int EmployeeID, DateTime JoinDate)
        {
            var allAttendanceEODRef = db.HrmsEodReferences.Where(a => a.Finish == 31 || a.ID == 7).OrderBy(a => a.priority).ToList();
            int beforeJoinAbsentDays = 0;
            decimal beforeJoinAbsentTaka = 0;
            decimal afterJoinAbsentTaka = 0;
            VMAttendance m = new VMAttendance();
            DBHelpers dbhelpers = new DBHelpers();

            if (model.FromDate < JoinDate && model.ToDate >= JoinDate)
            {
                m.IsNewJoined = true;
                dbhelpers.GetAttendanceForNewJoin(EmployeeID, model.FromDate, model.ToDate, JoinDate);
            }
            else
            {
                dbhelpers.GetAttendance(EmployeeID, model.FromDate, model.ToDate);
            }

            var attendance = dbhelpers.VMAttendanceSummary;

            #region ManageGeneralRecord
            var empSalaryRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Status == true).ToList();
            if (empSalaryRecord.Any())
            {
                m.BasicPay = empSalaryRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                m.GrossPay = empSalaryRecord.Where(a => a.Eod_RefFk == 1 || a.Eod_RefFk == 2 || a.Eod_RefFk == 3).Sum(a => a.ActualAmount);
                m.PerDayPay = m.BasicPay / this.DaysOfMonth;

                var OverTimeRate1 = empSalaryRecord.Where(a => a.Eod_RefFk == 11);
                if (OverTimeRate1.Any())
                {
                    m.OverTimeRate = OverTimeRate1.FirstOrDefault().ActualAmount;
                }

                if (m.IsNewJoined)
                {
                    m.AttendanceBonus = 0;
                }
                else
                {
                    var AttendanceBonus1 = empSalaryRecord.Where(a => a.Eod_RefFk == 7);
                    if (AttendanceBonus1.Any())
                    {
                        m.AttendanceBonus = AttendanceBonus1.FirstOrDefault().ActualAmount;
                    }
                }

                var StampDeduction1 = empSalaryRecord.Where(a => a.Eod_RefFk == 12);
                if (StampDeduction1.Any())
                {
                    m.StampDeduction = StampDeduction1.FirstOrDefault().ActualAmount;
                }
            }
            #endregion

            #region ManageAttendanceRecord

            m.TotalWorkDays = attendance.WorkDays;
            m.TotalAdsentDays = attendance.AbsentDays;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;

            if (m.IsNewJoined)
            {
                m.GrossPerDayPay = m.GrossPay / this.DaysOfMonth;
                beforeJoinAbsentDays = m.TotalAdsentDays - attendance.JoinedAbsentDays;
                beforeJoinAbsentTaka = m.GrossPerDayPay * beforeJoinAbsentDays;
                afterJoinAbsentTaka = m.PerDayPay * attendance.JoinedAbsentDays;
                m.TotalDeduction = Math.Round(beforeJoinAbsentTaka + afterJoinAbsentTaka);
            }
            else
            {
                m.TotalDeduction = m.TotalAdsentDays * m.PerDayPay;
            }

            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;

            if (m.TotalAdsentDays > 0 || attendance.TotalLeaveDays > 0 || attendance.LateDay > 2 || m.IsNewJoined)
            {
                m.AttendanceBonus = 0;
            }
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion

            #region ManageAttendanceEODReference
            List<EODReference> payrollInfolist = new List<EODReference>();
            foreach (var v in allAttendanceEODRef)
            {
                EODReference item = new EODReference();
                item.EODRefID = v.ID;
                item.SalaryHead = v.Name;
                if (v.ID == 30)
                {
                    item.Amount = Math.Round(m.GrossPay, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Salary";
                }
                else if (v.ID == 31)
                {
                    item.Amount = m.TotalWorkDays;
                    item.Note = "Total Present Days";
                }
                else if (v.ID == 32)
                {
                    item.Amount = attendance.OffDays + attendance.HoliDays;
                    item.Note = "Total OffDays and HolyDays";
                }
                else if (v.ID == 33)
                {
                    item.Amount = m.TotalPaidLeave;
                    item.Note = "Total Leave Approved";
                }
                else if (v.ID == 34)
                {
                    item.Amount = m.TotalAdsentDays;
                    item.Note = "Total Adsent Days";
                }
                else if (v.ID == 35)
                {
                    item.Amount = Math.Round(m.TotalDeduction, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Deduction";
                }
                else if (v.ID == 28)
                {
                    item.Amount = m.TotalOverTime;
                    item.Note = "Total OverTime Hours";
                }
                else if (v.ID == 11)
                {
                    item.Amount = m.OverTimeRate;
                    item.Note = "OverTime Rate";
                }
                else if (v.ID == 39)
                {
                    item.Amount = Math.Round(m.OverTimeAmount, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total OverTime Value";
                }
                else if (v.ID == 7)
                {
                    item.Amount = m.AttendanceBonus;
                    item.Note = "Attendance Bonus";
                }
                else if (v.ID == 36)
                {
                    item.Amount = Math.Round(m.TotalPayableSalary, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Payable Salary";
                }
                else if (v.ID == 40)
                {
                    item.Amount = Math.Round(m.TotalSalaryPay, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Payable Salary + OT";
                }
                payrollInfolist.Add(item);
            }

            #endregion

            #region AddDataBase
            foreach (var v in payrollInfolist)
            {
                HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                {
                    PayrollFK = model.PayRollID,
                    EmployeeIdFk = EmployeeID,
                    Eod_ReferenceFk = v.EODRefID,
                    Amount = v.Amount,
                    Note = v.Note,
                    PaymentDate = model.PaymentDate,
                    Status = true,
                    Update_Date = model.Update_Date,
                    Update_By = model.Update_By,
                    Entry_By = model.Entry_By,
                    Entry_Date = model.Entry_Date
                };
                db.HrmsPayrollInfos.Add(payrollInfo);
            }
            #endregion
        }

        /// <summary>
        /// Lefty Perday Calculation in GrossSalary/30
        /// Termination Perday Calculation in GrossSalary/30
        /// </summary>
        /// <param name="model"></param>
        /// <param name="EmployeeID"></param>
        /// <param name="leftDate"></param>
        private void CalculateAttendanceEODForLefty(VM_HRMS_Payroll model, int EmployeeID, DateTime leftDate)
        {
            var allAttendanceEODRef = db.HrmsEodReferences.Where(a => a.Finish == 31 || a.ID == 7).OrderBy(a => a.priority).ToList();

            VMAttendance m = new VMAttendance();
            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAttendanceForLefty(EmployeeID, model.FromDate, model.ToDate, leftDate);

            var attendance = dbhelpers.VMAttendanceSummary;

            #region ManageGeneralRecord
            var empSalaryRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Status == true).ToList();
            if (empSalaryRecord.Any())
            {
                m.BasicPay = empSalaryRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                m.GrossPay = empSalaryRecord.Where(a => a.Eod_RefFk == 1 || a.Eod_RefFk == 2 || a.Eod_RefFk == 3).Sum(a => a.ActualAmount);
                m.GrossPerDayPay = m.GrossPay / this.DaysOfMonth;
                m.AttendanceBonus = 0;
                var OverTimeRate1 = empSalaryRecord.Where(a => a.Eod_RefFk == 11);
                if (OverTimeRate1.Any())
                {
                    m.OverTimeRate = OverTimeRate1.FirstOrDefault().ActualAmount;
                }

                var StampDeduction1 = empSalaryRecord.Where(a => a.Eod_RefFk == 12);
                if (StampDeduction1.Any())
                {
                    m.StampDeduction = StampDeduction1.FirstOrDefault().ActualAmount;
                }
            }
            #endregion

            #region ManageAttendanceRecord
            m.TotalAdsentDays = attendance.AbsentDays;
            m.TotalWorkDays = attendance.WorkDays;
            m.TotalDeduction = m.GrossPerDayPay * m.TotalAdsentDays;
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion

            #region ManageAttendanceEODReference
            List<EODReference> payrollInfolist = new List<EODReference>();
            foreach (var v in allAttendanceEODRef)
            {
                EODReference item = new EODReference();
                item.EODRefID = v.ID;
                item.SalaryHead = v.Name;
                if (v.ID == 30)
                {
                    item.Amount = Math.Round(m.GrossPay, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Salary";
                }
                else if (v.ID == 31)
                {
                    item.Amount = m.TotalWorkDays;
                    item.Note = "Total Present Days";
                }
                else if (v.ID == 32)
                {
                    item.Amount = attendance.OffDays + attendance.HoliDays;
                    item.Note = "Total OffDays and HolyDays";
                }
                else if (v.ID == 33)
                {
                    item.Amount = m.TotalPaidLeave;
                    item.Note = "Total Leave Approved";
                }
                else if (v.ID == 34)
                {
                    item.Amount = m.TotalAdsentDays;
                    item.Note = "Total Adsent Days";
                }
                else if (v.ID == 35)
                {
                    item.Amount = Math.Round(m.TotalDeduction, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Deduction";
                }
                else if (v.ID == 28)
                {
                    item.Amount = m.TotalOverTime;
                    item.Note = "Total OverTime Hours";
                }
                else if (v.ID == 11)
                {
                    item.Amount = m.OverTimeRate;
                    item.Note = "OverTime Rate";
                }
                else if (v.ID == 39)
                {
                    item.Amount = Math.Round(m.OverTimeAmount, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total OverTime Value";
                }
                else if (v.ID == 7)
                {
                    item.Amount = m.AttendanceBonus;
                    item.Note = "Attendance Bonus";
                }
                else if (v.ID == 36)
                {
                    item.Amount = Math.Round(m.TotalPayableSalary, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Payable Salary";
                }
                else if (v.ID == 40)
                {
                    item.Amount = Math.Round(m.TotalSalaryPay, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Payable Salary + OT";
                }
                payrollInfolist.Add(item);
            }

            #endregion

            #region AddDataBase
            foreach (var v in payrollInfolist)
            {
                HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                {
                    PayrollFK = model.PayRollID,
                    EmployeeIdFk = EmployeeID,
                    Eod_ReferenceFk = v.EODRefID,
                    Amount = v.Amount,
                    Note = v.Note,
                    PaymentDate = model.PaymentDate,
                    Status = true,
                    Update_Date = model.Update_Date,
                    Update_By = model.Update_By,
                    Entry_By = model.Entry_By,
                    Entry_Date = model.Entry_Date
                };
                db.HrmsPayrollInfos.Add(payrollInfo);
            }
            #endregion
        }

        /// <summary>
        /// Resign Last date of Payroll Perday Calculation in BasicSalary/30
        /// Resign Before Last date Perday Calculation in GrossSalary/30
        /// </summary>
        /// <param name="model"></param>
        /// <param name="EmployeeID"></param>
        /// <param name="resignDate"></param>
        private void CalculateAttendanceEODForResign(VM_HRMS_Payroll model, int EmployeeID, DateTime resignDate)
        {
            var allAttendanceEODRef = db.HrmsEodReferences.Where(a => a.Finish == 31 || a.ID == 7).OrderBy(a => a.priority).ToList();

            VMAttendance m = new VMAttendance();
            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAttendanceForLefty(EmployeeID, model.FromDate, model.ToDate, resignDate);

            var attendance = dbhelpers.VMAttendanceSummary;

            #region ManageGeneralRecord
            var empSalaryRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Status == true).ToList();
            if (empSalaryRecord.Any())
            {
                m.BasicPay = empSalaryRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                m.GrossPay = empSalaryRecord.Where(a => a.Eod_RefFk == 1 || a.Eod_RefFk == 2 || a.Eod_RefFk == 3).Sum(a => a.ActualAmount);
                m.GrossPerDayPay = m.GrossPay / this.DaysOfMonth;
                m.PerDayPay = m.BasicPay / this.DaysOfMonth;
                m.AttendanceBonus = 0;
                var OverTimeRate1 = empSalaryRecord.Where(a => a.Eod_RefFk == 11);
                if (OverTimeRate1.Any())
                {
                    m.OverTimeRate = OverTimeRate1.FirstOrDefault().ActualAmount;
                }

                var StampDeduction1 = empSalaryRecord.Where(a => a.Eod_RefFk == 12);
                if (StampDeduction1.Any())
                {
                    m.StampDeduction = StampDeduction1.FirstOrDefault().ActualAmount;
                }
            }
            #endregion

            #region ManageAttendanceRecord

            m.TotalWorkDays = attendance.WorkDays;
            m.TotalAdsentDays = attendance.AbsentDays;

            if (model.ToDate > resignDate && model.FromDate <= resignDate)
            {
                m.TotalDeduction = m.TotalAdsentDays * m.GrossPerDayPay;
                m.AttendanceBonus = 0;
            }
            else if (model.ToDate == resignDate)
            {
                m.TotalDeduction = m.TotalAdsentDays * m.PerDayPay;

                var AttendanceBonus1 = empSalaryRecord.Where(a => a.Eod_RefFk == 7);
                if (AttendanceBonus1.Any())
                {
                    m.AttendanceBonus = AttendanceBonus1.FirstOrDefault().ActualAmount;
                }
            }
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion

            #region ManageAttendanceEODReference
            List<EODReference> payrollInfolist = new List<EODReference>();
            foreach (var v in allAttendanceEODRef)
            {
                EODReference item = new EODReference();
                item.EODRefID = v.ID;
                item.SalaryHead = v.Name;
                if (v.ID == 30)
                {
                    item.Amount = Math.Round(m.GrossPay, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Salary";
                }
                else if (v.ID == 31)
                {
                    item.Amount = m.TotalWorkDays;
                    item.Note = "Total Present Days";
                }
                else if (v.ID == 32)
                {
                    item.Amount = attendance.OffDays + attendance.HoliDays;
                    item.Note = "Total OffDays and HolyDays";
                }
                else if (v.ID == 33)
                {
                    item.Amount = m.TotalPaidLeave;
                    item.Note = "Total Leave Approved";
                }
                else if (v.ID == 34)
                {
                    item.Amount = m.TotalAdsentDays;
                    item.Note = "Total Adsent Days";
                }
                else if (v.ID == 35)
                {
                    item.Amount = Math.Round(m.TotalDeduction, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Deduction";
                }
                else if (v.ID == 28)
                {
                    item.Amount = m.TotalOverTime;
                    item.Note = "Total OverTime Hours";
                }
                else if (v.ID == 11)
                {
                    item.Amount = m.OverTimeRate;
                    item.Note = "OverTime Rate";
                }
                else if (v.ID == 39)
                {
                    item.Amount = Math.Round(m.OverTimeAmount, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total OverTime Value";
                }
                else if (v.ID == 7)
                {
                    item.Amount = m.AttendanceBonus;
                    item.Note = "Attendance Bonus";
                }
                else if (v.ID == 36)
                {
                    item.Amount = Math.Round(m.TotalPayableSalary, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Payable Salary";
                }
                else if (v.ID == 40)
                {
                    item.Amount = Math.Round(m.TotalSalaryPay, 0, MidpointRounding.AwayFromZero);
                    item.Note = "Total Payable Salary + OT";
                }
                payrollInfolist.Add(item);
            }

            #endregion

            #region AddDataBase
            foreach (var v in payrollInfolist)
            {
                HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                {
                    PayrollFK = model.PayRollID,
                    EmployeeIdFk = EmployeeID,
                    Eod_ReferenceFk = v.EODRefID,
                    Amount = v.Amount,
                    Note = v.Note,
                    PaymentDate = model.PaymentDate,
                    Status = true,
                    Update_Date = model.Update_Date,
                    Update_By = model.Update_By,
                    Entry_By = model.Entry_By,
                    Entry_Date = model.Entry_Date
                };
                db.HrmsPayrollInfos.Add(payrollInfo);
            }
            #endregion
        }

        #endregion

        #region AuditPayroll

        public void GetCompliance(int EmployeeID)
        {
            var vData = db.HRMHistoryEOD.Where(a => a.EmployeeId == EmployeeID && a.Status == true);

            if (vData.Any())
            {
                this.AddCompliance = true;
            }
        }

        public void GetAuditSalaryList()
        {
            var vData = (from d in db.HRMHistoryEOD
                         join a in db.Employees on d.EmployeeId equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join c in db.Sections on a.Section_Id equals c.ID
                         join e in db.HrmsEodReferences on d.Eod_RefFk equals e.ID
                         where a.Present_Status == 1 && d.Status == true
                         select new
                         {
                             EmpID = a.ID,
                             name = a.Name_BN,
                             engName = a.Name,
                             CardNo = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Designation = b.Name,
                             Section = c.SectionName,
                             RefId = e.ID,
                             HeadName = e.Name,
                             Amount = d.ActualAmount,
                             IsRegular = e.Regular,
                             priority = e.priority
                         }).OrderBy(a => a.Section).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.engName, o.CardNo, o.CardTitle, o.Section, o.Designation } into all
                            select new
                            {
                                EmpID = all.Key.EmpID,
                                Name = all.Key.engName,
                                CardNo = all.Key.CardNo,
                                CardTitle = all.Key.CardTitle,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID
                                            select new
                                            {
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                priority = p.priority,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList(),
                                //IsCompliance = db.ComplianceEODRecord.Any(a => a.EmployeeId == all.Key.EmpID && a.Status == true)
                            }).OrderBy(a => a.Section).ToList();

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("BasicPay", typeof(string));
            ds.Columns.Add("House", typeof(string));
            ds.Columns.Add("Other", typeof(string));
            ds.Columns.Add("Total", typeof(string));
            ds.Columns.Add("OT.Rate", typeof(string));
            ds.Columns.Add("AttendanceBonus", typeof(string));
            //ds.Columns.Add("Compliance", typeof(Boolean));
            ds.Rows.Clear();
            if (getGroup.Any())
            {
                int count = 0;
                foreach (var v in getGroup)
                {
                    ds.Rows.Add(
                        v.EmpID,
                        v.CardTitle + " " + v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.Designation,
                        v.Section,
                        decimal.Zero,//BasicPay6
                        decimal.Zero,//HouseAllowance7
                        decimal.Zero,//HealthFoodTravel8
                        decimal.Zero,//TotalSalary9
                        decimal.Zero,//OTRate10
                        decimal.Zero//AttendanceBonus11
                                    //v.IsCompliance
                        );
                    if (v.HeadItem.Any(a => a.RefId == 1))
                    {
                        var BasicPay = v.HeadItem.FirstOrDefault(a => a.RefId == 1);
                        ds.Rows[count][6] = BasicPay.Amount;
                        var HouseAllowance = v.HeadItem.FirstOrDefault(a => a.RefId == 2);
                        ds.Rows[count][7] = HouseAllowance.Amount;
                        var OtherAllowance = v.HeadItem.FirstOrDefault(a => a.RefId == 3);
                        ds.Rows[count][8] = OtherAllowance.Amount;
                        ds.Rows[count][9] = BasicPay.Amount + HouseAllowance.Amount + OtherAllowance.Amount;
                        if (v.HeadItem.Any(a => a.RefId == 11))
                        {
                            var OTRate = v.HeadItem.FirstOrDefault(a => a.RefId == 11);
                            ds.Rows[count][10] = OTRate.Amount;
                        }

                        if (v.HeadItem.Any(a => a.RefId == 7))
                        {
                            var AttendanceBonus = v.HeadItem.FirstOrDefault(a => a.RefId == 7);
                            ds.Rows[count][11] = AttendanceBonus.Amount;
                        }
                    }
                    ++count;
                }
            }
            List<VM_HRMS_Payroll> lst = new List<VM_HRMS_Payroll>();
            if (ds.Rows.Count > 0)
            {
                for (int counter = 0; counter < ds.Rows.Count; counter++)
                {
                    decimal basic = 0;
                    decimal.TryParse(ds.Rows[counter][6].ToString(), out basic);
                    VM_HRMS_Payroll model = new VM_HRMS_Payroll();
                    model.BasicAmount = basic;
                    model.EmployeeID = (int)ds.Rows[counter][0];
                    model.EmployeeIdentity = ds.Rows[counter][1].ToString();
                    model.Title = ds.Rows[counter][2].ToString();
                    model.EmployeeName = ds.Rows[counter][3].ToString();
                    model.Designation = ds.Rows[counter][4].ToString();
                    model.Section = ds.Rows[counter][5].ToString();
                    model.BasicPay = ds.Rows[counter][6].ToString();
                    model.HousePay = ds.Rows[counter][7].ToString();
                    model.OtherPay = ds.Rows[counter][8].ToString();
                    model.TotalPay = ds.Rows[counter][9].ToString();
                    model.OTRate = ds.Rows[counter][10].ToString();
                    model.AttendenceBonuses = ds.Rows[counter][11].ToString();
                    //model.AddCompliance = (bool)ds.Rows[counter][12];
                    lst.Add(model);
                }
            }
            this.PayrollDataList = lst.OrderBy(a => a.Section).ThenBy(a => a.EmployeeIdentity).ToList();
        }

        public void GetCreateASalary(VM_HRMS_Payroll model)
        {
            model.HealthAllowance = this.Medical;
            model.FoodAllowance = this.Food;
            model.TransportAllowance = this.Transport;
            model.OtherAllowance = model.HealthAllowance + model.FoodAllowance + model.TransportAllowance;
            decimal basic = (model.GrossSalary - model.OtherAllowance) / this.Rate;
            model.BasicAmount = Math.Round(basic);
            decimal house = model.BasicAmount * this.HouseRate;
            model.HouseAllowance = Math.Round(house);
            model.OTPerRate = (model.BasicAmount / 208) * 2;

            var referenceExistEod = db.HrmsEodReferences.Where(a => a.Status == true && a.Finish == 12).ToList();
            if (referenceExistEod.Any())
            {
                List<EODReference> createRecordList = new List<EODReference>();

                foreach (var v in referenceExistEod)
                {
                    EODReference item = new EODReference();
                    item.EODRefID = v.ID;
                    item.EmpID = model.EmployeeID;
                    item.SalaryHead = v.Name;
                    if (v.ID == 1)
                    {
                        item.Amount = model.BasicAmount;
                    }
                    else if (v.ID == 2)
                    {
                        item.Amount = model.HouseAllowance;
                    }
                    else if (v.ID == 3)
                    {
                        item.Amount = model.OtherAllowance;
                    }
                    else if (v.ID == 42)
                    {
                        item.Amount = model.TransportAllowance;
                    }
                    else if (v.ID == 43)
                    {
                        item.Amount = model.HealthAllowance;
                    }
                    else if (v.ID == 44)
                    {
                        item.Amount = model.FoodAllowance;
                    }
                    else if (v.ID == 11)
                    {
                        if (model.OTAllow != 2 && model.OTAllow != 3)
                        {
                            item.Amount = Math.Round(model.OTPerRate, 2);
                        }
                    }
                    else if (v.ID == 12)
                    {
                        item.Amount = v.CalculativeAmount;
                    }
                    else if (v.ID == 7)
                    {
                        if (model.OTAllow == 1 || model.OTAllow == 2)
                        {
                            item.Amount = model.AttendenceBonus;
                        }
                    }
                    createRecordList.Add(item);
                }

                if (createRecordList.Any())
                {
                    foreach (var v in createRecordList)
                    {
                        if (v.EODRefID == 11)
                        {
                            v.Amount = Math.Round(model.OTPerRate, 2);
                        }
                        HRMHistoryEOD eodrecord = new HRMHistoryEOD
                        {
                            EmployeeId = v.EmpID,
                            Eod_RefFk = v.EODRefID,
                            ActualAmount = v.Amount,
                            EffectiveDate = model.JoinDate,
                            Status = true,
                            Entry_Date = model.Entry_Date,
                            Entry_By = model.Entry_By
                        };

                        db.HRMHistoryEOD.Add(eodrecord);
                    }
                }
            }
        }

        public void GetUpdateASalary(VM_HRMS_Payroll model)
        {
            model.HealthAllowance = this.Medical;
            model.FoodAllowance = this.Food;
            model.TransportAllowance = this.Transport;
            model.OtherAllowance = model.HealthAllowance + model.FoodAllowance + model.TransportAllowance;
            decimal basic = (model.GrossSalary - model.OtherAllowance) / this.Rate;
            model.BasicAmount = Math.Round(basic);
            decimal house = model.BasicAmount * this.HouseRate;
            model.HouseAllowance = Math.Round(house);
            decimal OTRate = (model.BasicAmount / 208) * 2;

            List<EODReference> updateRecordList = new List<EODReference>();
            #region ManageSalary
            var referenceExistEod = db.HrmsEodReferences.Where(a => a.Status == true && a.Start == 1).ToList();
            if (referenceExistEod.Any())
            {
                foreach (var itemRef in referenceExistEod)
                {
                    EODReference item = new EODReference();
                    item.EODRefID = itemRef.ID;
                    item.EmpID = model.EmployeeID;
                    if (itemRef.ID == 1)
                    {
                        item.Amount = model.BasicAmount;
                    }
                    else if (itemRef.ID == 2)
                    {
                        item.Amount = model.HouseAllowance;
                    }
                    else if (itemRef.ID == 3)
                    {
                        item.Amount = model.OtherAllowance;
                    }
                    else if (itemRef.ID == 42)
                    {
                        item.Amount = model.Transport;
                    }
                    else if (itemRef.ID == 43)
                    {
                        item.Amount = model.Medical;
                    }
                    else if (itemRef.ID == 44)
                    {
                        item.Amount = model.Food;
                    }
                    else if (itemRef.ID == 7)
                    {
                        if (model.OTAllow == 1 || model.OTAllow == 2)
                        {
                            item.Amount = model.AttendenceBonus;
                        }
                    }
                    else if (itemRef.ID == 11)
                    {
                        if (model.OTAllow != 2 && model.OTAllow != 3)
                        {
                            item.Amount = OTRate;
                        }
                    }

                    updateRecordList.Add(item);
                }
            }
            #endregion
            #region ComplianceSalary
            var allExistRecord = db.HRMHistoryEOD.Where(a => a.EmployeeId == model.EmployeeID && a.Status == true).ToList();
            if (updateRecordList.Any())
            {
                foreach (var v in updateRecordList)
                {
                    var checkItemMultiple = allExistRecord.Where(a => a.Eod_RefFk == v.EODRefID).Count();
                    if (checkItemMultiple > 1)
                    {
                        var takeLast = allExistRecord.Where(a => a.Eod_RefFk == v.EODRefID).OrderByDescending(a => a.ID).FirstOrDefault();

                        foreach (var d in allExistRecord.Where(a => a.Eod_RefFk == v.EODRefID && a.ID != takeLast.ID))
                        {
                            var update = db.HRMHistoryEOD.Where(a => a.EmployeeId == model.EmployeeID && a.ID == d.ID).FirstOrDefault();
                            update.Status = false;
                            update.Update_By = model.Update_By;
                            update.Update_Date = model.Update_Date;
                        }
                    }
                    else if (checkItemMultiple == 1)
                    {
                        var previousRecord = db.HRMHistoryEOD.Where(a => a.EmployeeId == model.EmployeeID && a.Eod_RefFk == v.EODRefID && a.Status == true).FirstOrDefault();
                        if (previousRecord.ActualAmount != v.Amount)
                        {
                            previousRecord.Status = false;
                            previousRecord.Update_By = model.Update_By;
                            previousRecord.Update_Date = model.Update_Date;

                            HRMHistoryEOD recordAdd = new HRMHistoryEOD()
                            {
                                EmployeeId = v.EmpID,
                                Eod_RefFk = v.EODRefID,
                                ActualAmount = v.Amount,
                                Status = true,
                                EffectiveDate = model.EffectDate,
                                Entry_Date = model.Update_Date,
                                Entry_By = model.Update_By
                            };
                            db.HRMHistoryEOD.Add(recordAdd);
                        }
                    }
                    else if (checkItemMultiple <= 0)
                    {
                        HRMHistoryEOD recordAdd = new HRMHistoryEOD()
                        {
                            EmployeeId = v.EmpID,
                            Eod_RefFk = v.EODRefID,
                            ActualAmount = v.Amount,
                            Status = true,
                            EffectiveDate = model.EffectDate,
                            Entry_Date = model.Update_Date,
                            Entry_By = model.Update_By
                        };
                        db.HRMHistoryEOD.Add(recordAdd);
                    }
                }

                db.SaveChanges();
            }
            #endregion
        }

        public void GetUpdateAuditEmployeePayroll(VM_HRMS_Payroll model)
        {
            var vPayroll = (from o in db.HRMHistoryPay
                            join p in db.Employees on o.EmployeeIdFk equals p.ID
                            join q in db.HrmsPayrolls on o.PayrollFK equals q.ID
                            select new
                            {
                                ID = p.ID,
                                joindate = p.Joining_Date,
                                resignDate = p.QuitDate,
                                Status = p.Present_Status,
                                FromDate = q.FromDate,
                                ToDate = q.ToDate,
                                PaymentDate = o.PaymentDate
                            }).FirstOrDefault();

            model.FromDate = vPayroll.FromDate;
            model.ToDate = vPayroll.ToDate;
            model.PaymentDate = vPayroll.PaymentDate;
            model.JoinDate = vPayroll.joindate;
            model.ResignDate = vPayroll.resignDate;
            model.QuitDate = vPayroll.resignDate;

            var empSalaryRecord = db.HRMHistoryEOD.Where(a => a.EmployeeId == model.EmployeeID && a.Status == true).ToList();
            if (empSalaryRecord.Any())
            {
                model.IsReprocessExecute = true;
                var GetEmployeePayroll = db.HRMHistoryPay.Where(a => a.EmployeeIdFk == model.EmployeeID && a.PayrollFK == model.PayRollID);
                foreach (var eod in empSalaryRecord)
                {
                    if (eod.Eod_RefFk != 7)
                    {
                        if (GetEmployeePayroll.Any(a => a.Eod_ReferenceFk == eod.Eod_RefFk))
                        {
                            var updateInfo = GetEmployeePayroll.FirstOrDefault(a => a.Eod_ReferenceFk == eod.Eod_RefFk);
                            updateInfo.Amount = eod.ActualAmount;
                            updateInfo.Update_By = model.Update_By;
                            updateInfo.Update_Date = model.Update_Date;
                        }
                        else
                        {
                            HRMHistoryPay payrollInfo = new HRMHistoryPay
                            {
                                PayrollFK = model.PayRollID,
                                EmployeeIdFk = model.EmployeeID,
                                Eod_ReferenceFk = eod.Eod_RefFk,
                                Amount = eod.ActualAmount,
                                PaymentDate = model.PaymentDate,
                                Status = true,
                                Update_Date = model.Update_Date,
                                Update_By = model.Update_By,
                                Entry_By = model.Update_By,
                                Entry_Date = model.Update_Date
                            };
                            db.HRMHistoryPay.Add(payrollInfo);
                        }
                    }
                }

                #region ReprocessAttendance
                if (model.FromDate > model.JoinDate && vPayroll.Status == 1)
                {
                    //Calculate For Regular Employee
                    CalculateAttendanceAuditEODForRegular(model);
                }
                else if (model.FromDate < model.JoinDate && model.ToDate >= model.JoinDate && vPayroll.Status == 1)
                {
                    //Calculate For NewJoined Employee
                    CalculateAttendanceAuditEODForNewJoined(model);
                }
                else if (model.FromDate < model.ResignDate && model.ToDate <= model.ResignDate && vPayroll.Status == 2)
                {
                    //Calculate For Resign Employee
                    CalculateAttendanceAuditEODForResign(model);
                }
                else if (model.FromDate < model.QuitDate && model.ToDate <= model.QuitDate && vPayroll.Status == 3)
                {
                    //Calculate For Lefty Employee
                    CalculateAttendanceAuditEODForLefty(model);
                }
                else if (model.FromDate < model.QuitDate && model.ToDate <= model.QuitDate && vPayroll.Status == 4)
                {
                    //Calculate For Terminate Employee
                    CalculateAttendanceAuditEODForLefty(model);
                }
                #endregion

                db.SaveChanges();
            }
        }

        public void GetProcessAuditPayroll(VM_HRMS_Payroll model)
        {
            DropDownData dr = new DropDownData();
            model.CardTitle = dr.GetCardTitle(model.CardID);
            #region AuditPayrollProcess

            var vEmployee = (from o in db.Employees
                             join p in db.HrmsPayrollInfos on o.ID equals p.EmployeeIdFk
                             where p.PayrollFK == model.PayRollID && o.CardTitle.Equals(model.CardTitle)
                             group o by new { o.ID, o.Joining_Date, resignDate = o.QuitDate, PresentStatus = o.Present_Status } into a
                             select new { ID = a.Key.ID, joindate = a.Key.Joining_Date, resignDate = a.Key.resignDate, Status = a.Key.PresentStatus }).ToList();

            if (vEmployee.Any())
            {
                foreach (var emp in vEmployee)
                {
                    model.EmployeeID = emp.ID;
                    model.JoinDate = emp.joindate;
                    model.ResignDate = emp.resignDate;
                    model.QuitDate = emp.resignDate;
                    var CheckAuditRecored = db.HRMHistoryEOD.Where(a => a.EmployeeId == model.EmployeeID && a.Status == true).ToList();
                    if (CheckAuditRecored.Any())
                    {
                        model.IsRegularEOD = false;
                        var check = db.HRMHistoryPay.Any(a => a.EmployeeIdFk == model.EmployeeID && a.PayrollFK == model.PayRollID);
                        if (check)
                        {
                            model.IsReprocessExecute = true;
                            #region ReprocessAuditPayroll
                            if (model.FromDate > model.JoinDate && emp.Status == 1)
                            {
                                //Calculate For Regular Employee
                                CalculateAttendanceAuditEODForRegular(model);
                            }
                            else if (model.FromDate <= model.JoinDate && model.ToDate >= model.JoinDate && emp.Status == 1)
                            {
                                //Calculate For NewJoined Employee
                                CalculateAttendanceAuditEODForNewJoined(model);
                            }
                            else if (model.FromDate < model.ResignDate && model.ToDate >= model.ResignDate && emp.Status == 2)
                            {
                                //Calculate For Resign Employee
                                CalculateAttendanceAuditEODForResign(model);
                            }
                            else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && emp.Status == 3)
                            {
                                //Calculate For Lefty Employee
                                CalculateAttendanceAuditEODForLefty(model);
                            }
                            else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && emp.Status == 4)
                            {
                                //Calculate For Terminate Employee
                                CalculateAttendanceAuditEODForLefty(model);
                            }
                            #endregion
                        }
                        else
                        {
                            #region ProcessAuditPayroll

                            if (model.FromDate > model.JoinDate && emp.Status == 1)
                            {
                                foreach (var eod in CheckAuditRecored)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Regular Employee
                                CalculateAttendanceAuditEODForRegular(model);
                            }
                            else if (model.FromDate <= model.JoinDate && model.ToDate >= model.JoinDate && emp.Status == 1)
                            {
                                foreach (var eod in CheckAuditRecored)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For NewJoined Employee
                                CalculateAttendanceAuditEODForNewJoined(model);
                            }
                            else if (model.FromDate < model.ResignDate && model.ToDate >= model.ResignDate && emp.Status == 2)
                            {
                                foreach (var eod in CheckAuditRecored)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Resign Employee
                                CalculateAttendanceAuditEODForResign(model);
                            }
                            else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && emp.Status == 3)
                            {
                                foreach (var eod in CheckAuditRecored)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Lefty Employee
                                CalculateAttendanceAuditEODForLefty(model);
                            }
                            else if (model.FromDate < model.QuitDate && model.ToDate >= model.QuitDate && emp.Status == 4)
                            {
                                foreach (var eod in CheckAuditRecored)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Terminate Employee
                                CalculateAttendanceAuditEODForLefty(model);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        #region ProcessGeneralPayroll
                        model.IsRegularEOD = true;
                        var eodRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmployeeID && a.Status == true).ToList();
                        if (eodRecord.Any())
                        {
                            if (model.FromDate > emp.joindate && emp.Status == 1)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Regular Employee
                                CalculateAttendanceAuditEODForRegular(model);
                            }
                            else if (model.FromDate <= emp.joindate && model.ToDate >= emp.joindate && emp.Status == 1)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For NewJoined Employee
                                CalculateAttendanceAuditEODForNewJoined(model);
                            }
                            else if (model.FromDate < emp.resignDate && model.ToDate >= emp.resignDate && emp.Status == 2)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Resign Employee
                                CalculateAttendanceAuditEODForResign(model);
                            }
                            else if (model.FromDate < emp.resignDate && model.ToDate >= emp.resignDate && emp.Status == 3)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Lefty Employee
                                CalculateAttendanceAuditEODForLefty(model);
                            }
                            else if (model.FromDate < emp.resignDate && model.ToDate >= emp.resignDate && emp.Status == 4)
                            {
                                foreach (var eod in eodRecord)
                                {
                                    if (eod.Eod_RefFk != 7)
                                    {
                                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                                        {
                                            PayrollFK = model.PayRollID,
                                            EmployeeIdFk = model.EmployeeID,
                                            Eod_ReferenceFk = eod.Eod_RefFk,
                                            Amount = eod.ActualAmount,
                                            PaymentDate = model.PaymentDate,
                                            Status = true,
                                            Entry_By = model.Entry_By,
                                            Entry_Date = model.Entry_Date,
                                            Update_Date = model.Update_Date,
                                            Update_By = model.Update_By
                                        };
                                        db.HRMHistoryPay.Add(payrollInfo);
                                    }
                                }
                                //Calculate For Terminate Employee
                                CalculateAttendanceAuditEODForLefty(model);
                            }
                        }
                        
                        #endregion
                    }
                }

                db.SaveChanges();
            }
            #endregion
        }

        private void CalculateAttendanceAuditEODForRegular(VM_HRMS_Payroll model)
        {
            VMAttendance m = new VMAttendance();
            if (model.IsRegularEOD)
            {
                m = GetRegularPrimaryEOD(model.EmployeeID);
            }
            else
            {
                m = GetPrimaryEOD(model.EmployeeID);
            }

            #region ManageAttendanceRecord
            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAuditRegularAttendance(EmployeeID, model.FromDate, model.ToDate);
            var attendance = dbhelpers.VMAttendanceSummary;
            m.TotalWorkDays = attendance.WorkDays;
            m.TotalAdsentDays = attendance.AbsentDays;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.OffDays = attendance.OffDays + attendance.HoliDays;
            m.TotalDeduction = m.TotalAdsentDays * m.PerDayPay;
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;

            if (m.TotalAdsentDays > 0 || attendance.TotalLeaveDays > 0 || attendance.LateDay > 2)
            {
                m.AttendanceBonus = 0;
            }
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion

            List<EODReference> lstEod = new List<EODReference>();
            lstEod = SetEODRefecence(m, model);
            if (model.IsReprocessExecute)
            {
                UpdatePayroll(lstEod, false);
            }
            else
            {
                SavePayroll(lstEod, false);
            }
        }

        private void CalculateAttendanceAuditEODForNewJoined(VM_HRMS_Payroll model)
        {
            int beforeJoinAbsentDays = 0;
            decimal beforeJoinAbsentTaka = 0;
            decimal afterJoinAbsentTaka = 0;
            VMAttendance m = new VMAttendance();
            if (model.IsRegularEOD)
            {
                m = GetRegularPrimaryEOD(model.EmployeeID);
            }
            else
            {
                m = GetPrimaryEOD(model.EmployeeID);
            }

            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAuditNewJoinAttendance(model.EmployeeID, model.FromDate, model.ToDate, model.JoinDate);
            var attendance = dbhelpers.VMAttendanceSummary;

            #region ManageAttendanceRecord
            m.AttendanceBonus = 0;
            m.TotalWorkDays = attendance.WorkDays;
            m.TotalAdsentDays = attendance.AbsentDays;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.OffDays = attendance.OffDays + attendance.HoliDays;
            m.GrossPerDayPay = m.GrossPay / this.DaysOfMonth;
            beforeJoinAbsentDays = m.TotalAdsentDays - attendance.JoinedAbsentDays;
            beforeJoinAbsentTaka = m.GrossPerDayPay * beforeJoinAbsentDays;
            afterJoinAbsentTaka = m.PerDayPay * attendance.JoinedAbsentDays;
            m.TotalDeduction = Math.Round(beforeJoinAbsentTaka + afterJoinAbsentTaka);
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion

            List<EODReference> lstEod = new List<EODReference>();
            lstEod = SetEODRefecence(m, model);
            if (model.IsReprocessExecute)
            {
                UpdatePayroll(lstEod, false);
            }
            else
            {
                SavePayroll(lstEod, false);
            }
        }

        private void CalculateAttendanceAuditEODForResign(VM_HRMS_Payroll model)
        {
            VMAttendance m = new VMAttendance();
            if (model.IsRegularEOD)
            {
                m = GetRegularPrimaryEOD(model.EmployeeID);
            }
            else
            {
                m = GetPrimaryEOD(model.EmployeeID);
            }
            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAuditLeftyAttendance(model.EmployeeID, model.FromDate, model.ToDate, model.ResignDate);
            var attendance = dbhelpers.VMAttendanceSummary;

            #region ManageAttendanceRecord

            m.TotalWorkDays = attendance.WorkDays;
            m.TotalAdsentDays = attendance.AbsentDays;
            m.OffDays = attendance.OffDays + attendance.HoliDays;
            if (model.ToDate > model.ResignDate && model.FromDate <= model.ResignDate)
            {
                m.TotalDeduction = m.TotalAdsentDays * m.GrossPerDayPay;
                m.AttendanceBonus = 0;
            }
            else if (model.ToDate == model.ResignDate)
            {
                m.TotalDeduction = m.TotalAdsentDays * m.PerDayPay;
            }
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion

            List<EODReference> lstEod = new List<EODReference>();
            lstEod = SetEODRefecence(m, model);
            if (model.IsReprocessExecute)
            {
                UpdatePayroll(lstEod, false);
            }
            else
            {
                SavePayroll(lstEod, false);
            }
        }

        private void CalculateAttendanceAuditEODForLefty(VM_HRMS_Payroll model)
        {
            VMAttendance m = new VMAttendance();
            if (model.IsRegularEOD)
            {
                m = GetRegularPrimaryEOD(model.EmployeeID);
            }
            else
            {
                m = GetPrimaryEOD(model.EmployeeID);
            }

            #region ManageAttendanceRecord
            DBHelpers dbhelpers = new DBHelpers();
            dbhelpers.GetAuditLeftyAttendance(model.EmployeeID, model.FromDate, model.ToDate, model.QuitDate);
            var attendance = dbhelpers.VMAttendanceSummary;

            m.OffDays = attendance.OffDays + attendance.HoliDays;
            m.TotalAdsentDays = attendance.AbsentDays;
            m.TotalWorkDays = attendance.WorkDays;
            m.TotalDeduction = m.GrossPerDayPay * m.TotalAdsentDays;
            m.TotalPayableSalary = m.GrossPay - m.TotalDeduction;
            m.TotalOverTime = attendance.OTHour;
            m.OverTimeAmount = attendance.OTHour * m.OverTimeRate;
            m.TotalSalaryPay = (m.TotalPayableSalary + m.AttendanceBonus + m.OverTimeAmount) - m.StampDeduction;
            #endregion
            List<EODReference> lstEod = new List<EODReference>();
            lstEod = SetEODRefecence(m, model);
            if (model.IsReprocessExecute)
            {
                UpdatePayroll(lstEod, false);
            }
            else
            {
                SavePayroll(lstEod, false);
            }
        }
        
        private VMAttendance GetPrimaryEOD(int EmployeeID)
        {
            VMAttendance m = new VMAttendance();
            var empSalaryRecord = db.HRMHistoryEOD.Where(a => a.EmployeeId == EmployeeID && a.Status == true).ToList();
            if (empSalaryRecord.Any())
            {
                m.BasicPay = empSalaryRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                m.GrossPay = empSalaryRecord.Where(a => a.Eod_RefFk == 1 || a.Eod_RefFk == 2 || a.Eod_RefFk == 3).Sum(a => a.ActualAmount);
                m.GrossPerDayPay = m.GrossPay / this.DaysOfMonth;
                m.PerDayPay = m.BasicPay / this.DaysOfMonth;

                var OverTimeRate1 = empSalaryRecord.Where(a => a.Eod_RefFk == 11);
                if (OverTimeRate1.Any())
                {
                    m.OverTimeRate = OverTimeRate1.FirstOrDefault().ActualAmount;
                }

                var AttendanceBonus1 = empSalaryRecord.Where(a => a.Eod_RefFk == 7);
                if (AttendanceBonus1.Any())
                {
                    m.AttendanceBonus = AttendanceBonus1.FirstOrDefault().ActualAmount;
                }

                var StampDeduction1 = empSalaryRecord.Where(a => a.Eod_RefFk == 12);
                if (StampDeduction1.Any())
                {
                    m.StampDeduction = StampDeduction1.FirstOrDefault().ActualAmount;
                }
            }
            return m;
        }

        private VMAttendance GetRegularPrimaryEOD(int EmployeeID)
        {
            VMAttendance m = new VMAttendance();
            var empSalaryRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Status == true).ToList();
            if (empSalaryRecord.Any())
            {
                m.BasicPay = empSalaryRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                m.GrossPay = empSalaryRecord.Where(a => a.Eod_RefFk == 1 || a.Eod_RefFk == 2 || a.Eod_RefFk == 3).Sum(a => a.ActualAmount);
                m.GrossPerDayPay = m.GrossPay / this.DaysOfMonth;
                m.PerDayPay = m.BasicPay / this.DaysOfMonth;

                var OverTimeRate1 = empSalaryRecord.Where(a => a.Eod_RefFk == 11);
                if (OverTimeRate1.Any())
                {
                    m.OverTimeRate = OverTimeRate1.FirstOrDefault().ActualAmount;
                }

                var AttendanceBonus1 = empSalaryRecord.Where(a => a.Eod_RefFk == 7);
                if (AttendanceBonus1.Any())
                {
                    m.AttendanceBonus = AttendanceBonus1.FirstOrDefault().ActualAmount;
                }

                var StampDeduction1 = empSalaryRecord.Where(a => a.Eod_RefFk == 12);
                if (StampDeduction1.Any())
                {
                    m.StampDeduction = StampDeduction1.FirstOrDefault().ActualAmount;
                }
            }
            return m;
        }

        private List<EODReference> SetEODRefecence(VMAttendance m, VM_HRMS_Payroll model)
        {
            List<EODReference> payrollInfolist = new List<EODReference>();
            var allAttendanceEODRef = db.HrmsEodReferences.Where(a => a.Finish == 31 || a.ID == 7).OrderBy(a => a.priority).ToList();
            foreach (var v in allAttendanceEODRef)
            {
                EODReference item = new EODReference();
                item.EODRefID = v.ID;
                item.EmpID = model.EmployeeID;
                item.PayrollId = model.PayRollID;
                item.PaymentDate = model.PaymentDate;
                item.UpdateDate = model.Update_Date;
                item.EntryDate = model.Entry_Date;
                if (v.ID == 30)
                {
                    //item.Amount = Math.Round(m.GrossPay, 0, MidpointRounding.AwayFromZero);
                    item.Amount = Math.Round(m.GrossPay,2);
                    item.Note = "Total Salary";
                }
                else if (v.ID == 31)
                {
                    item.Amount = m.TotalWorkDays;
                    item.Note = "Total Present Days";
                }
                else if (v.ID == 32)
                {
                    item.Amount = m.OffDays;
                    item.Note = "Total OffDays and HolyDays";
                }
                else if (v.ID == 33)
                {
                    item.Amount = m.TotalPaidLeave;
                    item.Note = "Total Leave Approved";
                }
                else if (v.ID == 34)
                {
                    item.Amount = m.TotalAdsentDays;
                    item.Note = "Total Adsent Days";
                }
                else if (v.ID == 35)
                {
                    //item.Amount = Math.Round(m.TotalDeduction, 0, MidpointRounding.AwayFromZero);
                    item.Amount = Math.Round(m.TotalDeduction, 2);
                    item.Note = "Total Deduction";
                }
                else if (v.ID == 28)
                {
                    item.Amount = m.TotalOverTime;
                    item.Note = "Total OverTime Hours";
                }
                else if (v.ID == 11)
                {
                    item.Amount = m.OverTimeRate;
                    item.Note = "OverTime Rate";
                }
                else if (v.ID == 39)
                {
                    //item.Amount = Math.Round(m.OverTimeAmount, 0, MidpointRounding.AwayFromZero);
                    item.Amount = Math.Round(m.OverTimeAmount,2);
                    item.Note = "Total OverTime Value";
                }
                else if (v.ID == 7)
                {
                    item.Amount = m.AttendanceBonus;
                    item.Note = "Attendance Bonus";
                }
                else if (v.ID == 36)
                {
                    //item.Amount = Math.Round(m.TotalPayableSalary, 0, MidpointRounding.AwayFromZero);
                    item.Amount = Math.Round(m.TotalPayableSalary, 2);
                    item.Note = "Total Payable Salary";
                }
                else if (v.ID == 40)
                {
                    //item.Amount = Math.Round(m.TotalSalaryPay, 0, MidpointRounding.AwayFromZero);
                    item.Amount = Math.Round(m.TotalSalaryPay, 2);
                    item.Note = "Total Payable Salary + OT";
                }
                payrollInfolist.Add(item);
            }

            return payrollInfolist;
        }

        private void SavePayroll(List<EODReference> lstEODReference, bool IsGeneral)
        {
            if (lstEODReference.Any())
            {
                if (IsGeneral)
                {
                    foreach (var v in lstEODReference)
                    {
                        HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                        {
                            PayrollFK = v.PayrollId,
                            EmployeeIdFk = v.EmpID,
                            Eod_ReferenceFk = v.EODRefID,
                            Amount = v.Amount,
                            Note = v.Note,
                            PaymentDate = v.PaymentDate,
                            Status = true,
                            Update_Date = v.UpdateDate,
                            Entry_Date = v.EntryDate
                        };
                        db.HrmsPayrollInfos.Add(payrollInfo);
                    }
                }
                else
                {
                    foreach (var v in lstEODReference)
                    {
                        HRMHistoryPay payrollInfo = new HRMHistoryPay
                        {
                            PayrollFK = v.PayrollId,
                            EmployeeIdFk = v.EmpID,
                            Eod_ReferenceFk = v.EODRefID,
                            Amount = v.Amount,
                            Note = v.Note,
                            PaymentDate = v.PaymentDate,
                            Status = true,
                            Update_Date = v.UpdateDate,
                            Entry_Date = v.EntryDate
                        };
                        db.HRMHistoryPay.Add(payrollInfo);
                    }
                }
            }
        }

        private void UpdatePayroll(List<EODReference> lstEODReference, bool IsGeneral)
        {
            if (lstEODReference.Any())
            {
                if (IsGeneral)
                {
                    foreach (var v in lstEODReference)
                    {
                        var getEmployeePayrollInfo = db.HrmsPayrollInfos.Where(a => a.EmployeeIdFk == v.EmpID && a.PayrollFK == v.PayrollId && a.Eod_ReferenceFk == v.EODRefID);
                        if (getEmployeePayrollInfo.Any())
                        {
                            var updateInfo = getEmployeePayrollInfo.FirstOrDefault();
                            updateInfo.Amount = v.Amount;
                            updateInfo.Update_Date = v.UpdateDate;
                        }
                        else
                        {
                            HRMS_PayrollInfo payrollInfo = new HRMS_PayrollInfo
                            {
                                PayrollFK = v.PayrollId,
                                EmployeeIdFk = v.EmpID,
                                Eod_ReferenceFk = v.EODRefID,
                                Amount = v.Amount,
                                Note = "Data add in Reprocess",
                                PaymentDate = v.PaymentDate,
                                Status = true,
                                Update_Date = v.UpdateDate,
                                Entry_Date = v.UpdateDate
                            };
                            db.HrmsPayrollInfos.Add(payrollInfo);
                        }
                    }
                }
                else
                {

                    foreach (var v in lstEODReference)
                    {
                        var getEmployeePayrollInfo = db.HRMHistoryPay.Where(a => a.EmployeeIdFk == v.EmpID && a.PayrollFK == v.PayrollId && a.Eod_ReferenceFk == v.EODRefID);

                        if (getEmployeePayrollInfo.Any())
                        {
                            var updateInfo = getEmployeePayrollInfo.FirstOrDefault();
                            updateInfo.Amount = v.Amount;
                            updateInfo.Update_Date = v.UpdateDate;
                        }
                        else
                        {
                            HRMHistoryPay payrollInfo = new HRMHistoryPay
                            {
                                PayrollFK = v.PayrollId,
                                EmployeeIdFk = v.EmpID,
                                Eod_ReferenceFk = v.EODRefID,
                                Amount = v.Amount,
                                Note = "Data add in Reprocess",
                                PaymentDate = v.PaymentDate,
                                Status = true,
                                Update_Date = v.UpdateDate,
                                Entry_Date = v.UpdateDate
                            };
                            db.HRMHistoryPay.Add(payrollInfo);
                        }
                    }
                }
            }
        }

        public void AuditPayrollList(int id)
        {
            var payroll = (from t1 in db.HRMHistoryPay
                           join t2 in db.Employees on t1.EmployeeIdFk equals t2.ID
                           join t3 in db.HrmsPayrolls on t1.PayrollFK equals t3.ID
                           join t4 in db.Sections on t2.Section_Id equals t4.ID
                           where t1.PayrollFK == id
                           select new VM_HRMS_Payroll
                           {
                               EmpIDFK = t1.EmployeeIdFk,
                               EmployeeIdentity = t2.EmployeeIdentity,
                               Title = t2.CardTitle,
                               Name = t2.Name,
                               Amount = t1.Amount,
                               PayRollName = t3.Note,
                               FromDate = t3.FromDate,
                               ToDate = t3.ToDate,
                               ID = t1.PayrollFK,
                               Section = t4.SectionName,
                               AlertActive = t2.AlertActive
                           }).OrderByDescending(s => s.ID).GroupBy(x => x.EmpIDFK).Select(x => x.FirstOrDefault());
            if (payroll.Any())
            {
                var vfirst = payroll.FirstOrDefault();
                this.ID = id;
                this.PayRollName = vfirst.PayRollName + " (" + vfirst.FromDate.ToShortDateString() + ")" + "-" + "(" + vfirst.ToDate.ToShortDateString() + ")";
                this.PayrollDataList = payroll.ToList();
            }
            else
            {
                this.PayrollDataList = new List<VM_HRMS_Payroll>();
            }
        }
        #endregion

        public void GetPayrollList()
        {
            HrmsContext db = new HrmsContext();
            var vData = (from o in db.HrmsPayrolls
                         where o.Status == true && o.IsTest == false
                         select new VM_HRMS_Payroll
                         {
                             PayRollID = o.ID,
                             Title = o.Note,
                             FromDate = o.FromDate,
                             ToDate = o.ToDate,
                             PayrollStatus = o.ClosePayroll == false ? "Running" : "Closed"

                         }).OrderByDescending(a => a.PayRollID).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.TotalEmployee = GetCountPayrollMember(v.PayRollID);
                }
            }

            this.PayrollDataList = vData;
        }

        public async Task<int> Save()
        {
            return await db.SaveChangesAsync();
        }

        public int GetCountPayrollMember(int PayrollID)
        {
            int count = 0;
            count = (from o in db.HrmsPayrollInfos
                     where o.PayrollFK == PayrollID
                     group o by new { o.EmployeeIdFk } into all
                     select new
                     {
                         empId = all.Key.EmployeeIdFk
                     }).Count();

            return count;
        }

        public void GetBillList()
        {
            var vData = (from d in db.HrmsEodRecords
                         join a in db.Employees on d.EmployeeId equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join c in db.Sections on a.Section_Id equals c.ID
                         join e in db.HrmsEodReferences on d.Eod_RefFk equals e.ID
                         where
                         e.ID == 1
                         || e.ID == 2
                         || e.ID == 3
                         || e.Finish == 3
                         && a.Present_Status == 1
                         && d.Status == true
                         select new
                         {
                             EmpID = a.ID,
                             name = a.Name_BN,
                             engName = a.Name,
                             CardNo = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Designation = b.Name,
                             Section = c.SectionName,
                             RefId = e.ID,
                             HeadName = e.Name,
                             Amount = d.ActualAmount,
                             IsRegular = e.Regular,
                             priority = e.priority
                         }).OrderBy(a => a.Section).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.engName, o.CardNo, o.CardTitle, o.Section, o.Designation } into all
                            select new
                            {
                                EmpID = all.Key.EmpID,
                                Name = all.Key.engName,
                                CardNo = all.Key.CardNo,
                                CardTitle = all.Key.CardTitle,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID
                                            select new
                                            {
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                priority = p.priority,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList()
                            }).OrderBy(a => a.Section).ToList();

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("CardTitle", typeof(string));
            ds.Columns.Add("CardNo", typeof(string));
            ds.Columns.Add("EmployeeName", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("BasicPay", typeof(string));
            ds.Columns.Add("GrossPay", typeof(string));
            ds.Columns.Add("NightRate", typeof(string));
            ds.Columns.Add("HolydayRate", typeof(string));
            ds.Rows.Clear();
            if (getGroup.Any())
            {
                int count = 0;
                foreach (var v in getGroup)
                {
                    if (v.HeadItem.Any(a => a.RefId == 46 || a.RefId == 47))
                    {
                        ds.Rows.Add(
                        v.EmpID,
                        v.CardTitle,
                        v.CardNo,
                        v.Name,
                        v.Designation,
                        v.Section,
                        decimal.Zero,//BasicPay6
                        decimal.Zero,//Gross
                        decimal.Zero,//NightRate
                        decimal.Zero//HolydayRate
                        );
                        if (v.HeadItem.Any())
                        {
                            if (v.HeadItem.Any(a => a.RefId == 1))
                            {
                                var BasicPay = v.HeadItem.FirstOrDefault(a => a.RefId == 1);
                                var HouseAllowance = v.HeadItem.FirstOrDefault(a => a.RefId == 2);
                                var OtherAllowance = v.HeadItem.FirstOrDefault(a => a.RefId == 3);
                                var NightRate = v.HeadItem.Where(a => a.RefId == 46);
                                var HDRate = v.HeadItem.Where(a => a.RefId == 47);
                                ds.Rows[count][6] = BasicPay.Amount;
                                ds.Rows[count][7] = BasicPay.Amount + HouseAllowance.Amount + OtherAllowance.Amount;
                                if (NightRate.Any())
                                {
                                    ds.Rows[count][8] = v.HeadItem.FirstOrDefault(a => a.RefId == 46).Amount;
                                }
                                if (HDRate.Any())
                                {
                                    ds.Rows[count][9] = v.HeadItem.FirstOrDefault(a => a.RefId == 47).Amount;
                                }
                            }
                        }
                        ++count;
                    }
                }
            }
            List<VMNightAndHolyDay> lst = new List<VMNightAndHolyDay>();
            if (ds.Rows.Count > 0)
            {
                for (int counter = 0; counter < ds.Rows.Count; counter++)
                {
                    decimal basic = 0;
                    decimal.TryParse(ds.Rows[counter][6].ToString(), out basic);
                    VMNightAndHolyDay model = new VMNightAndHolyDay();
                    model.BasicPay = basic;
                    model.EmployeeID = (int)ds.Rows[counter][0];
                    model.CardTitle = ds.Rows[counter][1].ToString();
                    model.CardNo = ds.Rows[counter][1] + " " + ds.Rows[counter][2];
                    model.EmployeeName = ds.Rows[counter][3].ToString();
                    model.Designation = ds.Rows[counter][4].ToString();
                    model.Section = ds.Rows[counter][5].ToString();
                    model.BasicAmount = ds.Rows[counter][6].ToString();
                    model.GrossAmount = ds.Rows[counter][7].ToString();
                    model.NRate = ds.Rows[counter][8].ToString();
                    model.HDRate = ds.Rows[counter][9].ToString();
                    lst.Add(model);
                }
            }
            this.ListNightAndHolyDayBill = lst.OrderBy(a => a.Section).ThenBy(a => a.CardNo).ToList();
        }

        public void GetUnassignBill()
        {
            var vAssignData = (from d in db.HrmsEodRecords
                               join a in db.Employees on d.EmployeeId equals a.ID
                               join b in db.Designations on a.Designation_Id equals b.ID
                               join c in db.Sections on a.Section_Id equals c.ID
                               join e in db.HrmsEodReferences on d.Eod_RefFk equals e.ID
                               where
                               e.Finish == 3
                               && a.Present_Status == 1
                               && d.Status == true
                               group new { d.Eod_RefFk, d.ActualAmount } by new { a, b.Name, c.SectionName } into a
                               select new
                               {
                                   EmpID = a.Key.a.ID,
                                   Name = a.Key.a.Name_BN,
                                   engName = a.Key.a.Name,
                                   CardNo = a.Key.a.EmployeeIdentity,
                                   CardTitle = a.Key.a.CardTitle,
                                   Designation = a.Key.Name,
                                   Section = a.Key.SectionName,
                                   HeadItem = a.ToList()
                               }).OrderBy(a => a.Section).ToList();

            var vData = (from d in db.HrmsEodRecords
                         join a in db.Employees on d.EmployeeId equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join c in db.Sections on a.Section_Id equals c.ID
                         join e in db.HrmsEodReferences on d.Eod_RefFk equals e.ID
                         where
                         a.Present_Status == 1
                         && d.Status == true
                         group new { d.Eod_RefFk, d.ActualAmount } by new { a, b.Name, c.SectionName } into a
                         select new
                         {
                             EmpID = a.Key.a.ID,
                             Name = a.Key.a.Name_BN,
                             engName = a.Key.a.Name,
                             CardNo = a.Key.a.EmployeeIdentity,
                             CardTitle = a.Key.a.CardTitle,
                             Designation = a.Key.Name,
                             Section = a.Key.SectionName,
                             HeadItem = a.ToList()
                         }).OrderBy(a => a.Section).ToList();

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("CardTitle", typeof(string));
            ds.Columns.Add("CardNo", typeof(string));
            ds.Columns.Add("EmployeeName", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("BasicPay", typeof(string));
            ds.Columns.Add("GrossPay", typeof(string));
            ds.Columns.Add("NightRate", typeof(string));
            ds.Columns.Add("HolydayRate", typeof(string));
            ds.Rows.Clear();

            if (vData.Any())
            {
                int count = 0;
                foreach (var v in vData)
                {
                    if (!vAssignData.Any(a => a.EmpID == v.EmpID))
                    {
                        ds.Rows.Add(
                        v.EmpID,
                        v.CardTitle,
                        v.CardNo,
                        v.Name,
                        v.Designation,
                        v.Section,
                        decimal.Zero,//BasicPay6
                        decimal.Zero,//Gross
                        decimal.Zero,//NightRate
                        decimal.Zero//HolydayRate
                        );
                        if (v.HeadItem.Any())
                        {
                            if (v.HeadItem.Any(a => a.Eod_RefFk == 1))
                            {
                                var BasicPay = v.HeadItem.FirstOrDefault(a => a.Eod_RefFk == 1);
                                var HouseAllowance = v.HeadItem.FirstOrDefault(a => a.Eod_RefFk == 2);
                                var OtherAllowance = v.HeadItem.FirstOrDefault(a => a.Eod_RefFk == 3);
                                ds.Rows[count][6] = BasicPay.ActualAmount;
                                ds.Rows[count][7] = BasicPay.ActualAmount + HouseAllowance.ActualAmount + OtherAllowance.ActualAmount;
                            }
                        }
                        ++count;
                    }
                }
            }
            List<VMNightAndHolyDay> lst = new List<VMNightAndHolyDay>();
            if (ds.Rows.Count > 0)
            {
                for (int counter = 0; counter < ds.Rows.Count; counter++)
                {
                    VMNightAndHolyDay model = new VMNightAndHolyDay();
                    model.EmployeeID = (int)ds.Rows[counter][0];
                    model.CardTitle = ds.Rows[counter][1].ToString();
                    model.CardNo = ds.Rows[counter][1] + " " + ds.Rows[counter][2];
                    model.EmployeeName = ds.Rows[counter][3].ToString();
                    model.Designation = ds.Rows[counter][4].ToString();
                    model.Section = ds.Rows[counter][5].ToString();
                    model.BasicAmount = ds.Rows[counter][6].ToString();
                    model.GrossAmount = ds.Rows[counter][7].ToString();
                    model.NRate = ds.Rows[counter][8].ToString();
                    model.HDRate = ds.Rows[counter][9].ToString();
                    lst.Add(model);
                }
            }
            this.ListNightAndHolyDayBill = lst.OrderBy(a => a.Section).ThenBy(a => a.CardNo).ToList();
        }

        public void GetCreateNightBill(VM_HRMS_Payroll model)
        {
            List<EODReference> createRecordList = new List<EODReference>();
            var getBasic = (from o in db.HrmsEodRecords
                            join p in db.Employees on o.EmployeeId equals p.ID
                            where o.Eod_RefFk == 1 && o.Status == true && p.Designation_Id == model.DesignationId
                            select new
                            {
                                empId = o.EmployeeId,
                                basicPay = o.ActualAmount
                            }).ToList();

            var checkRecordEodBill = (from o in db.HrmsEodRecords
                                      join p in db.HrmsEodReferences on o.Eod_RefFk equals p.ID
                                      where p.Finish == 3 && p.Status == true
                                      select new
                                      {
                                          o
                                      }).ToList();

            var getEmployee = db.Employees.Where(a => a.Designation_Id == model.DesignationId && a.Present_Status == 1).ToList();
            if (getEmployee.Any())
            {
                var referenceExistEod = db.HrmsEodReferences.Where(a => a.Status == true && a.Finish == 3).ToList();

                foreach (var emp in getEmployee)
                {
                    if (referenceExistEod.Any() && getBasic.Any(a => a.empId == emp.ID) && !checkRecordEodBill.Any(a => a.o.EmployeeId == emp.ID))
                    {
                        foreach (var v in referenceExistEod)
                        {
                            EODReference item = new EODReference();
                            item.EODRefID = v.ID;
                            item.EmpID = emp.ID;
                            item.SalaryHead = v.Name;
                            if (v.ID == 46)
                            {
                                item.Amount = model.NightRateAmount;
                            }
                            else if (v.ID == 47)
                            {
                                decimal basic = (decimal)getBasic.FirstOrDefault(a => a.empId == emp.ID).basicPay / this.DaysOfMonth;
                                item.Amount = Math.Round(basic, 0, MidpointRounding.AwayFromZero);
                            }

                            createRecordList.Add(item);
                        }
                    }
                }

                if (createRecordList.Any())
                {
                    foreach (var v in createRecordList)
                    {
                        HRMS_EodRecord eodrecord = new HRMS_EodRecord
                        {
                            EmployeeId = v.EmpID,
                            Eod_RefFk = v.EODRefID,
                            ActualAmount = v.Amount,
                            EffectiveDate = DateTime.Today,
                            Status = true,
                            Entry_Date = model.Entry_Date,
                            Entry_By = model.Entry_By
                        };

                        db.HrmsEodRecords.Add(eodrecord);
                    }
                }
            }
        }

        public void GetDeleteNightBill(int Id)
        {
            var vData = db.HrmsEodRecords.Where(a => a.EmployeeId == Id && a.Status == true);

            if (vData.Any(a => a.Eod_RefFk == 46))
            {
                db.HrmsEodRecords.Remove(vData.FirstOrDefault(a => a.Eod_RefFk == 46));
            }
            if (vData.Any(a => a.Eod_RefFk == 47))
            {
                db.HrmsEodRecords.Remove(vData.FirstOrDefault(a => a.Eod_RefFk == 47));
            }

            db.SaveChanges();
        }

        public void GetEmployeeBill(int EmployeeID)
        {
            HrmsContext db = new HrmsContext();

            var vData = (from t1 in db.HrmsEodRecords
                         join t2 in db.HrmsEodReferences on t1.Eod_RefFk equals t2.ID
                         where t1.EmployeeId == EmployeeID && t1.Status == true
                         select new EmployeeEODRecord
                         {
                             EODRecordID = t1.ID,
                             EODRefFK = t2.ID,
                             SalaryItem = t2.Name,
                             Amount = t1.ActualAmount,
                             EmployeeID = EmployeeID,
                             Priority = t2.priority
                         }).OrderByDescending(a => a.EODRecordID).ToList();

            var getBasic = vData.Where(a => a.EODRefFK == 1);
            if (getBasic.Any())
            {
                this.BasicAmount = getBasic.FirstOrDefault().Amount;
            }

            List<EmployeeEODRecord> lst = new List<EmployeeEODRecord>();
            var eodref = db.HrmsEodReferences.Where(a => a.Finish == 3).OrderBy(a => a.priority).ToList();

            foreach (var v in eodref)
            {
                var getBill = vData.Where(a => a.EODRefFK == v.ID);

                if (getBill.Any())
                {
                    lst.Add(getBill.FirstOrDefault());
                }
            }

            this.EODRecordList = lst;
        }

        public void GetUpdateNightBill(VM_HRMS_Payroll model)
        {
            var allExistRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmployeeID && a.Status == true);
            if (model.EODRecordList.Any())
            {
                foreach (var updateRecord in model.EODRecordList)
                {
                    if (allExistRecord.Any(a => a.Eod_RefFk == updateRecord.EODRefFK))
                    {
                        var previousRecord = allExistRecord.FirstOrDefault(a => a.Eod_RefFk == updateRecord.EODRefFK);
                        if (previousRecord.ActualAmount != updateRecord.Amount)
                        {
                            previousRecord.Status = false;
                            previousRecord.Update_By = model.Update_By;
                            previousRecord.Update_Date = model.Update_Date;

                            HRMS_EodRecord recordAdd = new HRMS_EodRecord()
                            {
                                EmployeeId = updateRecord.EmployeeID,
                                Eod_RefFk = updateRecord.EODRefFK,
                                ActualAmount = updateRecord.Amount,
                                Status = true,
                                EffectiveDate = DateTime.Now,
                                Entry_Date = model.Update_Date,
                                Entry_By = model.Update_By
                            };
                            db.HrmsEodRecords.Add(recordAdd);
                        }
                    }
                }
            }
        }
    }
}

public class VMAttendance
{
    public int TotalOverTime { get; set; }
    public decimal OverTimeRate { get; set; }
    public decimal OverTimeAmount { get; set; }
    public decimal PerDayPay { get; set; }
    public decimal GrossPerDayPay { get; set; }
    public decimal AttendanceBonus { get; set; }
    public decimal BasicPay { get; set; }
    public decimal GrossPay { get; set; }
    public decimal TotalDeduction { get; set; }
    public decimal TotalMonthlyPay { get; set; }
    public decimal TotalPayableSalary { get; set; }
    public decimal TotalSalaryPay { get; set; }
    public decimal StampDeduction { get; set; }

    public int TotalWorkDays { get; set; }
    public int TotalAdsentDays { get; set; }
    public int PresentDays { get; set; }
    public int LateDays { get; set; }
    public int AdsentDays { get; set; }
    public int LeaveDays { get; set; }
    public int OffDays { get; set; }
    public int HolyDays { get; set; }
    public int TotalUnpaidLeave { get; set; }
    public int TotalPaidLeave { get; set; }

    public int TotalExtraOTs { get; set; }
    public int TotalOTHours { get; set; }

    public bool IsNewJoined { get; set; }
}

public class VMNightAndHolyDay
{
    public int EmployeeID { get; set; }
    public string EmployeeName { get; set; }
    public string CardNo { get; set; }
    public string CardTitle { get; set; }
    public string EmployeeType { get; set; }
    public string Designation { get; set; }
    public string Section { get; set; }
    public string EmpBanglaName { get; set; }
    public decimal BasicPay { get; set; }
    public decimal GrossPay { get; set; }
    public decimal NightRate { get; set; }
    public decimal HolydayRate { get; set; }
    public decimal NightWorkDay { get; set; }
    public decimal OffdayWorkDay { get; set; }
    public decimal HolydayWorkDay { get; set; }
    public decimal NightTaka { get; set; }
    public decimal HolydayTaka { get; set; }
    public decimal TotalDays { get; set; }
    public decimal TotalTaka { get; set; }

    public string BasicAmount { get; set; }
    public string GrossAmount { get; set; }
    public string NRate { get; set; }
    public string HDRate { get; set; }
}

public class EODReference
{
    public int EODRefID { get; set; }

    public int EmpID { get; set; }

    public int PayrollId { get; set; }

    public int Priority { get; set; }

    public string SalaryHead { get; set; }

    public decimal Amount { get; set; }

    public string Note { get; set; }

    public DateTime PaymentDate { get; set; }

    public DateTime UpdateDate { get; set; }

    public DateTime EntryDate { get; set; }
}