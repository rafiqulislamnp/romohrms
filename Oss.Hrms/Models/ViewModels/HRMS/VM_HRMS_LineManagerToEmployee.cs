﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_LinkToEmployee : RootModel
    {
        [DisplayName("Manager ID.")]
        public int LineManagerId { get; set; }

        [DisplayName("Line Manager.")]
        public int LineManagerId1 { get; set; }
        [DisplayName("Admin")]
        public int LineManagerId2 { get; set; }
        [DisplayName("Depratment Head")]
        public int LineManagerId3 { get; set; }
        [DisplayName("Superior")]
        public int SuperiorId { get; set; }

        [DisplayName("Superior Type")]
        public int Type { get; set; }

        [DisplayName("Superior Type")]
        public string SuperiorName { get; set; }

        [DisplayName("Superior Type")]
        public string SuperiorType { get; set; }

        public int LineManagerTableId { get; set; }
        [DisplayName("L.M. Id.")]
        public string LineManagerIdentity { get; set; }
        public string LineManagerName { get; set; }
        // [DisplayName("Employee ID.")]
        [DisplayName("Card No")]
        public int EmployeeId { get; set; }
        [DisplayName("Card No")]
       // [DisplayName("Employee ID.")]
        public string EmployeeIdentity { get; set; }
        public string EmployeeName { get; set; }

        [DisplayName("Admin ID.")]
        public int AdminId { get; set; }
        public int AdminTableId { get; set; }
        [DisplayName("Admin ID.")]
        public string AdminIdentity { get; set; }
        public string AdminName { get; set; }

        [DisplayName("Dept Head ID.")]
        public int DeptHeadId { get; set; }
        public int DeptHeadTableId { get; set; }

        [DisplayName("Dept. Head ID.")]
        public string DeptHeadIdentity { get; set; }
        public string DeptHeadName { get; set; }
        public HRMS_Employee Employee { get; set; }
        public HRMS_Employee LineManager { get; set; }
        public HRMS_Employee Admin { get; set; }
        public HRMS_Employee DeptHead { get; set; }
        public HRMS_LinkSuperior HrmsLinkSuperior { get; set; }
        public ICollection<VM_HRMS_LinkToEmployee> LinkToEmployees { get; set; }

        private HrmsContext db = new HrmsContext();


        public void GetLineManagerList1()
        {
            // DateDiff dd = new DateDiff();
            var a = new List<VM_HRMS_LinkToEmployee>();
           
                a = db.Database.SqlQuery<VM_HRMS_LinkToEmployee>(
                    "Exec [dbo].[GetEmployeeSuperiorList]").ToList();
           
            this.LinkToEmployees = a;
        }
        //public void GetLineManagerList1(int id)
        //{
        //    db = new HrmsContext();
        //    var a = (from t1 in db.HrmsLinkSuperior
        //             join t2 in db.Employees on t1.SuperiorId equals t2.ID && t1.Type == 1
        //             join t3 in db.Employees on t1.SuperiorId equals t3.ID && t1.Type == 2
        //             join t4 in db.Employees on t1.SuperiorId equals t4.ID && t1.Type == 3
        //             where t1.EmployeeId == id
        //             select new VM_HRMS_LinkToEmployee
        //             {
        //                 HrmsLinkSuperior = t1,
        //                 LineManager = t2,
        //                 Admin = t3,
        //                 DeptHead = t4,
        //             }).ToList();

        //    this.LinkToEmployees = a;
        //}


        public List<VM_HRMS_LinkToEmployee> GetSpecificEmployeeSuperiorList(int? id)
        {
            var a = new List<VM_HRMS_LinkToEmployee>();
            if (true)
            {
                a = db.Database
                    .SqlQuery<VM_HRMS_LinkToEmployee>("Exec [dbo].[GetSpecificEmployeeSuperior] @id='" +
                                                      id + "'").ToList();
                //a = (from t1 in db.HrmsLinkSuperior
                //     join t2 in db.Employees on t1.EmployeeId equals t2.ID
                //    select new
                //    {

                //    }).ToList();
            }
            return a;

        }


        public VM_HRMS_LinkToEmployee GetSpecificLineManagerList(int id)
        {
            var a = new VM_HRMS_LinkToEmployee();
            if (true)
            {
                a = db.Database
                    .SqlQuery<VM_HRMS_LinkToEmployee>("Exec [dbo].[GetSpecificEmployeeSuperior1] @id='" +
                                                         id + "'").FirstOrDefault();
            }
            return a;
            
        }

        /// <summary>
        /// Retrieve all employees with assigned superior
        /// </summary>
        public void GetLineManagerList()
        {
            //var lineManager = db.Database.SqlQuery<VM_HRMS_LinkToEmployee>("exec sp_LinkToEmployees").ToList();
            var lineManager = (from em in db.Employees
                join lm in db.ManagerToEmployeeLinks on em.ID equals lm.EmployeeId
                join ad in db.AdminToEmployeeLinks on em.ID equals ad.EmployeeId
                join dh in db.HeadToEmployeeLinks on em.ID equals dh.EmployeeId
                select new VM_HRMS_LinkToEmployee()
                {
                    EmployeeId = em.ID,
                    EmployeeName = em.Name, EmployeeIdentity= em.EmployeeIdentity,
                    LineManagerName = (from emp in db.Employees where emp.ID == lm.LineManagerId select emp.Name).FirstOrDefault(),
                    AdminName = (from emp in db.Employees where emp.ID == ad.AdminId select emp.Name).FirstOrDefault(),
                    DeptHeadName = (from emp in db.Employees where emp.ID == dh.DeptHeadId select emp.Name).FirstOrDefault()
                }).ToList();
            LinkToEmployees = lineManager;
        }


        public List<VM_HRMS_LinkToEmployee> LineManagerList(int? id)
        {
            //var lineManager = db.Database.SqlQuery<VM_HRMS_LinkToEmployee>("exec sp_LinkToEmployees").ToList();
            var lineManager = (from em in db.Employees
                join lm in db.ManagerToEmployeeLinks on em.ID equals lm.EmployeeId
                join ad in db.AdminToEmployeeLinks on em.ID equals ad.EmployeeId
                join dh in db.HeadToEmployeeLinks on em.ID equals dh.EmployeeId
                where em.ID == id
                    select new VM_HRMS_LinkToEmployee()
                {
                    EmployeeName = em.Name,
                    EmployeeIdentity = em.EmployeeIdentity,
                    LineManagerName = (from emp in db.Employees where emp.ID == lm.LineManagerId select emp.Name).FirstOrDefault(),
                    LineManagerIdentity = (from emp in db.Employees where emp.ID == lm.LineManagerId select emp.EmployeeIdentity).FirstOrDefault(),
                    AdminName = (from emp in db.Employees where emp.ID == ad.AdminId select emp.Name).FirstOrDefault(),
                    AdminIdentity = (from emp in db.Employees where emp.ID == ad.AdminId select emp.EmployeeIdentity).FirstOrDefault(),
                    DeptHeadName = (from emp in db.Employees where emp.ID == dh.DeptHeadId select emp.Name).FirstOrDefault(),
                    DeptHeadIdentity = (from emp in db.Employees where emp.ID == dh.DeptHeadId select emp.EmployeeIdentity).FirstOrDefault(),
                }).ToList();
           return lineManager;
        }
    }
}