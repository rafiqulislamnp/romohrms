﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Reference :RootModel
    {
        [DisplayName("Card No")]
        // [DisplayName("Employee ID")]
        public int Employee_Id { get; set; }
        [DisplayName("Card No")]
        // [DisplayName("Employee ID")]
        public string Employee_Identity { get; set; }
        public string Name { get; set; }
        [DisplayName("Referee")]
        public string Referee_Name { get; set; }
        [DisplayName("Contact No.")]
        public string Contact_No { get; set; }
        [DisplayName("Email Id.")]
        [DataType(DataType.EmailAddress)]
        public string Email_Id { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }
        [DisplayName("Relation")]
        public string Relation { get; set; }

        public ICollection<VM_HRMS_Reference> References { get; set; }
        public HRMS_Employee Employee { get; internal set; }

        private HrmsContext db = new HrmsContext();

        public void GetReferenceList()
        {
            var refe = (from r in db.HrmsReferences
                join em in db.Employees on r.Employee_Id equals em.ID
                select new VM_HRMS_Reference()
                {
                    ID = r.ID,
                    Employee_Id = em.ID,
                    Employee_Identity = em.EmployeeIdentity, Referee_Name = r.Referee_Name, Contact_No = r.Contact_No, Email_Id = r.Email_Id,
                    Address = r.PresentAddress, Relation = r.Relation
                }).ToList();
            References = refe;
        }
    }
}