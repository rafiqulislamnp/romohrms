﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMNotificationUser:RootModel
    {

        private HrmsContext db = new HrmsContext();
        //[ForeignKey("Id")]
        [DisplayName("Employee ID")]
        [Required(ErrorMessage = "Please Select Employee ID")]
        public string Employee_Id { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Email Id")]
        [Required(ErrorMessage = "Please Enter Email")]
        [Index("Ux_Notification", IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        [DataType(DataType.EmailAddress, ErrorMessage="Email Id is invalid")]
        public string Email { get; set; }
        [DisplayName("Subject")]
        [Required(ErrorMessage = "Please Enter Subject")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50,MinimumLength=2, ErrorMessage = "Subject should be minimum 2 characters and maximum 80 characters")]
        public string Subject { get; set; }
        [DisplayName("Business Unit")]
        [Required(ErrorMessage = "Please Select Business Unit")]
        public int BusinessUnit_ID { get; set; }

        [DisplayName("Business Unit")]
        public string BusinessUnit_Name { get; set; }
        public VMNotificationUser VmNotificationUser { get; set; }

        public IEnumerable<VMNotificationUser> DatalistNotificationUsers { get; set; }

        
        public void LoadNotificationUser()
        {
            var a = (from t1 in db.NotificationUsers
                     join b in db.BusinessUnits on t1.BusinessUnit_ID equals b.ID where t1.Status == true
                     select new VMNotificationUser()
                     {
                         ID = t1.ID,
                         Employee_Id = t1.Employee_Id,
                         Name = t1.Name,
                         Email = t1.Email,
                         Subject = t1.Subject,
                         BusinessUnit_ID = t1.BusinessUnit_ID,
                         BusinessUnit_Name = b.Name
                     }).AsEnumerable();
            this.DatalistNotificationUsers = a;
        }

        public VMNotificationUser LoadSpecificNotificationUser(int id)
        {
            var a = (from t1 in db.NotificationUsers
                join b in db.BusinessUnits on t1.BusinessUnit_ID equals b.ID
                where t1.ID==id
                select new VMNotificationUser()
                {
                    ID = t1.ID,
                    Employee_Id = t1.Employee_Id,
                    Name = t1.Name,
                    Email = t1.Email,
                    Subject = t1.Subject,
                    BusinessUnit_ID = t1.BusinessUnit_ID,
                    BusinessUnit_Name = b.Name
                }).FirstOrDefault();
             return a;
        }



        //model = (from n in db.NotificationUsers
        //    join b in db.BusinessUnits on n.BusinessUnit_ID equals b.ID
        //    where n.ID == id
        //    select new VMNotificationUser
        //{
        //    Employee_Id = model.Employee_Id,
        //    Name = model.Name,
        //    Email = model.Email,
        //    BusinessUnit_ID = model.BusinessUnit_ID,
        //    BusinessUnit_Name = model.BusinessUnit_Name,
        //    Subject = model.Subject
        //}).FirstOrDefault();
        //public void string getNotificationUsers()
        //{
        //    var list = new VMNotificationUser();

        //    list = (from n in db.NotificationUsers
        //            join b in db.BusinessUnits on n.BusinessUnit_ID equals b.ID
        //            select new VMNotificationUser()
        //            {
        //                ID = n.ID,
        //                Employee_Id = n.Employee_Id,
        //                Name = n.Name,
        //                Email = n.Email
        //            }).AsEnumerable();

        //    return list;
        //}
    }
}