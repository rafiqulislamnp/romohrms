﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_EducationInfo : RootModel
    {
        [DisplayName("Card No")]
        // [DisplayName("Employee ID")]
        public int Employee_Id { get; set; }

        // [DisplayName("Employee ID")]
        [DisplayName("Card No")]
        public string Employee_Identity { get; set; }
        public string Employee_Name { get; set; }

        [DisplayName("Exam/Degree")]
        //[Required(ErrorMessage = "Enter Degree")]
        //[StringLength(60,MinimumLength = 3,ErrorMessage = "Enter minimum 3 characters.")]
        public string Degree { get; set; }
        
        [DisplayName("Institute")]
        //[Required(ErrorMessage = "Enter Institute Name")]
        //[StringLength(60, MinimumLength = 3, ErrorMessage = "Enter minimum 3 characters.")]
        public string Institute { get; set; }

        [DisplayName("Group/Subject")]
        //[Required(ErrorMessage = "Enter Group/Subject")]
        //[StringLength(60, MinimumLength = 6, ErrorMessage = "Enter minimum 6 characters.")]
        public string GroupOrSubject { get; set; }

        [DisplayName("Passing Year")]
        //[Required(ErrorMessage = "Enter Passing Year.")]
        //[StringLength(60, MinimumLength = 4, ErrorMessage = "Enter minimum 4 characters.")]
        public string Passing_Year { get; set; }

        [DisplayName("Result")]
        //[Required(ErrorMessage = "Enter Result.")]
        //[StringLength(60, MinimumLength = 3, ErrorMessage = "Enter minimum 4 characters.")]
        public string Result { get; set; }

        public IEnumerable<VM_HRMS_EducationInfo> EducationInfos { get; set; }
        public HRMS_Employee Employee { get; internal set; }

        HrmsContext db = new HrmsContext();

        public void GetEducaionalInfos()
        {
            var edu = (from ed in db.HrmsEducations
                join em in db.Employees on ed.Employee_Id equals em.ID
                select new VM_HRMS_EducationInfo()
                {
                    ID = ed.ID, Employee_Id = em.ID,
                    Employee_Identity = em.EmployeeIdentity,
                    Degree = ed.Degree,
                    Institute = ed.Institute,
                    GroupOrSubject = ed.GroupOrSubject,
                    Passing_Year = ed.Passing_Year,
                    Result = ed.Result
                }).AsEnumerable().ToList();
            EducationInfos = edu;
        }
    }
}