﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_District:RootModel
    {

        [DisplayName("District Name")]
        [Required(ErrorMessage = "Please Select District")]
        public string DistrictName { get; set; }

        public ICollection<VM_District> CollectionVmDistrict { get; set; }
        HrmsContext db = new HrmsContext();

        public void GetDistrictName()
        {
            var district = (from l in db.District
                select new VM_District()
                {
                    ID = l.ID,
                    DistrictName = l.DistrictName
                }).ToList();

            CollectionVmDistrict = district;
        }
    }
}