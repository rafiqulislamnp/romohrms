﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.Leave
{
    public class VM_HRMS_Leave_Type :RootModel
    {
        HrmsContext db = new HrmsContext();

        [DisplayName("Leave Name:")]
        [Required(ErrorMessage = "Please Enter Your Leave Name")]
        public string Leave_Name { get; set; }
        [DisplayName("Leave Amount:")]
        [Required(ErrorMessage = "Please Enter Your Amount of Leave")]
        public int Leave_Amount { get; set; }
        public IEnumerable<VM_HRMS_Leave_Type> DataList { get; set; }

        public VM_HRMS_Leave_Type hrms_leave_type { get; set; }


        public void GetLoadData()
        {
           // db = new CrimsonContext();
            var v = (from o in db.HrmsLeaveTypes
                //where (o.Status == true)
                select new VM_HRMS_Leave_Type
                {
                    ID = o.ID,
                    Leave_Name=o.Leave_Name,
                    Leave_Amount = o.Leave_Amount,
                    Entry_By=o.Entry_By,
                    Entry_Date =o.Entry_Date,
                    Update_By=o.Update_By,
                    Update_Date =o.Update_Date
                }).ToList();
            this.DataList = v;
        }

        public void SelectSingle(int id)
        {
            db = new HrmsContext();
            var v = (from o in db.HrmsLeaveTypes
                //where (o.Status == true && o.ID == id)
                where (o.ID == id)
                select new VM_HRMS_Leave_Type
                {
                    ID = o.ID,
                    Leave_Name = o.Leave_Name,
                    Leave_Amount = o.Leave_Amount,
                    Entry_By = o.Entry_By,
                    Entry_Date = o.Entry_Date,
                    Update_By = o.Update_By,
                    Update_Date = o.Update_Date
                }).FirstOrDefault();
            this.hrms_leave_type = v;
        }
    }
}