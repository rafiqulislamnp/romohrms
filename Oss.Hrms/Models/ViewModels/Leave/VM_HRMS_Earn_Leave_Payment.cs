﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;

namespace Oss.Hrms.Models.ViewModels.Leave
{
    public class VM_HRMS_Earn_Leave_Payment
    {
        private HrmsContext db = new HrmsContext();
        public List<VM_HRMS_Earn_Leave_Payment> DataList { get; set; }

        public List<string> EmployeeIds { get; set; }

        [DisplayName("Employee ID")]
        public int EmployeeId { get; set; }
        public int ID { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DisplayName("Total Month")]
        public int Total_Month { get; set; }

        [DisplayName("Working Days")]
        public int WorkingDays { get; set; }

        [DisplayName("Holiday")]
        public int Holiday { get; set; }

        [DisplayName("OffDay")]
        public int OffDay { get; set; }

        [DisplayName("Total Leave")]
        public int TotalLeave { get; set; }
        [DisplayName("Absent")]
        public int TotalAbsent { get; set; }
        [DisplayName("Total Deduction Days")]
        public int TotalDeductionDays { get; set; }

        [DisplayName("Payable Working Days")]
        public int? PayableWorkingDays { get; set; }
        [DisplayName("Total Earn Leave")]
        public int TotalEarnLeave { get; set; }

        [DisplayName("Enjoy Able Earn Leave")]
        public int EnjoyAbleEarnLeave { get; set; }

        [DisplayName("Payable Earn Leave")]
        public int PayableEarnLeave { get; set; }

        [DisplayName("Stamp")]
        public int Stamp { get; set; }

        [DisplayName("Present Salary")]
        public decimal? PresentSalary { get; set; }

        [DisplayName("Payable Earn Leave TK")]
        public decimal? PayableEarnLeaveTk { get; set; }

        public decimal? Present_Salary { get; set; }

        // [DisplayName("Last Payment Date"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}"), Required(ErrorMessage = "Last Payment Date is required")]
        [DisplayName("Last Payment Date")]
        
        public string PaymentDate { get; set; }

        [DisplayName("Card Title")]
        public string CardTitle { get; set; }

        [DisplayName("Section Name")]
        public string SectionName { get; set; }
        public int SectionId { get; set; }

        [DisplayName("Employee ID")]
        public string EmployeeIdentity { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Designation")]
        public string Designation { get; set; }

        [DisplayName("Joining Date")]
        public string Joining_Date { get; set; }

        [DisplayName("Quite Date")]
        public string QuiteDate { get; set; }
        public bool IsAudit { get; set; }

        //  [DisplayName("From Date"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}"), Required(ErrorMessage = "From date date is required")]
        public DateTime Fromdate { get; set; }
      //  [DisplayName("To Date"), Required(ErrorMessage = "To date date is required")]
        public DateTime ToDate { get; set; }

        public VM_HRMS_Earn_Leave_Payment EarnLeaveCalculation(VM_HRMS_Earn_Leave_Payment model)
        {
            var leaveInfo = new VM_HRMS_Earn_Leave_Payment();
            var dt1 = model.ToDate.ToString("yyyy-MM-dd");
            int t = 0;
            if (model.IsAudit == false)
            {
                t = 0;
            }
            else if (model.IsAudit == true)
            {
                t = 1;
            }
            if (model.IsAudit == false)
            {
                var EarnleaveInfo = db.Database.SqlQuery<VM_HRMS_Earn_Leave_Payment>($@"Select ID, SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate as PaymentDate, 
                                cast(Isnull(Total_Month,0) as int) as Total_Month,
                                cast(Isnull(Working_Days,0) as int) as WorkingDays,                           
                                cast(Isnull(Holiday,0) as int) as Holiday,
                                cast(Isnull(OffDay,0) as int) as OffDay,                           
                                cast(Isnull(Leave,0) as int) as TotalLeave,                          
                                cast(Isnull(Absent,0) as int) as [TotalAbsent],
                                cast(Isnull(TotalDeductionDays,0) as int) as TotalDeductionDays,                           
                                cast(Isnull(PayableWorkingDays,0) as int) as PayableWorkingDays, 
                                cast(Isnull(sum(PayableWorkingDays / 18),0) as int) as TotalEarnLeave, 
                                cast(Isnull(EarnLeave,0) as int) as EnjoyableLeave, 
                                cast(Isnull(sum(PayableWorkingDays / 18),0) as int)-(Isnull(EarnLeave,0)) as PayableEarnLeave,
                                cast(Isnull(Present_Salary,0) as decimal(18,2))as Present_Salary,
                                cast(Isnull(Stamp,0) as int)as Stamp,                    
                                (cast(Isnull(sum(((PayableWorkingDays / 18)-EarnLeave) * OneDaySalary),0) as decimal(18,2))-Stamp) as PayableEarnLeaveTK, '' As [Signature]
                                from( Select ID, SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, Convert(varchar(10),LastPaymentDate,126) as LastPaymentDate,
                                Total_Month, Working_Days,Holiday,OffDay, Leave,EarnLeave,[Absent],
                                sum(Holiday+OffDay + Leave + [Absent]) as TotalDeductionDays,
                                sum(Working_Days - (Holiday+ OffDay + Leave + [Absent])) as PayableWorkingDays,sum(BasicSalary + HouseRent+ Others) as Present_Salary,
                                sum((BasicSalary + HouseRent+ Others) / 30) as OneDaySalary, Stamp from 					                             
                                (Select e.ID, s.SectionName, e.CardTitle,e.EmployeeIdentity, e.Name, d.Name as Designation,                          
                                convert(varchar(10), Joining_Date, 126) as Joining_Date,                          
                                (Select [dbo].[fn_LastPaymentDate](e.ID," + t + ")) as LastPaymentDate, " +
                                "(select [dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID,'" + dt1 + "', 1," + t + ")) as Total_Month,  " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 2," + t + ")) as Working_Days, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 3," + t + ")) as Holiday, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 4," + t + ")) as Leave, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 5," + t + ")) as EarnLeave, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 6," + t + ")) as OffDay, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 7," + t + ")) as [Absent], " +
                               "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID = e.ID and Eod_RefFk = 1 and[Status] = 1) as BasicSalary, " +
                               "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID = e.ID and Eod_RefFk = 2 and[Status] = 1) as HouseRent," +
                               "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID = e.ID and Eod_RefFk = 3 and[Status] = 1) as Others, " +
                               "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID = e.ID and Eod_RefFk = 12 and[Status] = 1) as Stamp " +
                               "from HRMS_Employee e join Sections s on e.Section_Id = s.Id inner join Designations d on e.Designation_Id = d.ID where  e.ID = " + model.EmployeeId + ")t " +
                               "group by ID, SectionName, CardTitle, EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate, Total_Month, Working_Days, OffDay, Holiday, Leave, EarnLeave,[Absent], BasicSalary, HouseRent, Stamp, Stamp ) tbl  where tbl.Working_Days>17 " +
                               "group by ID, SectionName, CardTitle, EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate, Total_Month, Working_Days, OffDay, Holiday, Leave, EarnLeave,[Absent], Present_Salary, Stamp, TotalDeductionDays, PayableWorkingDays ").ToList();


                if (EarnleaveInfo.Any())
                {
                    leaveInfo.DataList = EarnleaveInfo;
                }
            }
            else if (model.IsAudit == true)
            {
                var EarnleaveInfo = db.Database.SqlQuery<VM_HRMS_Earn_Leave_Payment>($@"Select ID, SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate as PaymentDate, 
                                cast(Isnull(Total_Month,0) as int) as Total_Month,
                                cast(Isnull(Working_Days,0) as int) as WorkingDays,                           
                                cast(Isnull(Holiday,0) as int) as Holiday,
                                cast(Isnull(OffDay,0) as int) as OffDay,                           
                                cast(Isnull(Leave,0) as int) as TotalLeave,                          
                                cast(Isnull(Absent,0) as int) as [TotalAbsent],
                                cast(Isnull(TotalDeductionDays,0) as int) as TotalDeductionDays,                           
                                cast(Isnull(PayableWorkingDays,0) as int) as PayableWorkingDays, 
                                cast(Isnull(sum(PayableWorkingDays / 18),0) as int) as TotalEarnLeave, 
                                cast(Isnull(EarnLeave,0) as int) as EnjoyableLeave, 
                                cast(Isnull(sum(PayableWorkingDays / 18),0) as int)-(Isnull(EarnLeave,0)) as PayableEarnLeave,
                                cast(Isnull(Present_Salary,0) as decimal(18,2))as Present_Salary,
                                cast(Isnull(Stamp,0) as int)as Stamp,                    
                                (cast(Isnull(sum(((PayableWorkingDays / 18)-EarnLeave) * OneDaySalary),0) as decimal(18,2))-Stamp) as PayableEarnLeaveTK, '' As [Signature]
                                from( Select ID, SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, Convert(varchar(10),LastPaymentDate,126) as LastPaymentDate,
                                Total_Month, Working_Days,Holiday,OffDay, Leave,EarnLeave,[Absent],
                                sum(Holiday+OffDay + Leave + [Absent]) as TotalDeductionDays,
                                sum(Working_Days - (Holiday+ OffDay + Leave + [Absent])) as PayableWorkingDays,sum(BasicSalary + HouseRent+ Others) as Present_Salary,
                                sum((BasicSalary + HouseRent+ Others) / 30) as OneDaySalary, Stamp from 					                             
                                (Select e.ID, s.SectionName, e.CardTitle,e.EmployeeIdentity, e.Name, d.Name as Designation,                          
                                convert(varchar(10), Joining_Date, 126) as Joining_Date,                          
                                (Select [dbo].[fn_LastPaymentDate](e.ID," + t + ")) as LastPaymentDate, " +
                               "(select [dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID,'" + dt1 + "', 1," +t+ ")) as Total_Month,  " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 2," +t+ ")) as Working_Days, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 3," +t+ ")) as Holiday, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 4," +t+ ")) as Leave, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 5," +t+ ")) as EarnLeave, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 6," +t+ ")) as OffDay, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 7," +t+ ")) as [Absent], " +
                               "(select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID = e.ID and Eod_RefFk = 1 and[Status] = 1) as BasicSalary, " +
                               "(select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID = e.ID and Eod_RefFk = 2 and[Status] = 1) as HouseRent," +
                               "(select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID = e.ID and Eod_RefFk = 3 and[Status] = 1) as Others, " +
                               "(select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID = e.ID and Eod_RefFk = 12 and[Status] = 1) as Stamp " +
                               "from HRMS_Employee e join Sections s on e.Section_Id = s.Id inner join Designations d on e.Designation_Id = d.ID where e.ID = " + model.EmployeeId + ")t " +
                               "group by ID, SectionName, CardTitle, EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate, Total_Month, Working_Days, OffDay, Holiday, Leave, EarnLeave,[Absent], BasicSalary, HouseRent, Stamp, Stamp ) tbl  where tbl.Working_Days>17 " +
                               "group by ID, SectionName, CardTitle, EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate, Total_Month, Working_Days, OffDay, Holiday, Leave, EarnLeave,[Absent], Present_Salary, Stamp, TotalDeductionDays, PayableWorkingDays ").ToList();

                if (EarnleaveInfo.Any())
                {
                    leaveInfo.DataList = EarnleaveInfo;
                }
            }
           
             return leaveInfo;
        }

        public VM_HRMS_Earn_Leave_Payment SectionWiseEarnLeaveCalculation(VM_HRMS_Earn_Leave_Payment model)
        {
            var leaveInfo = new VM_HRMS_Earn_Leave_Payment();
            var dt1 = model.ToDate.ToString("yyyy-MM-dd");
            int t = 0;
            if (model.IsAudit == false)
            {
                t = 0;
            }
            else if (model.IsAudit == true)
            {
                t = 1;
            }
            if (model.IsAudit == false)
            {
                var EarnleaveInfo = db.Database.SqlQuery<VM_HRMS_Earn_Leave_Payment>($@"Select ID, SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate as PaymentDate, 
                                cast(Total_Month as int) as Total_Month,cast(Working_Days as int) as WorkingDays,                           
                                cast(Holiday as int) as Holiday,
                                cast(OffDay as int) as OffDay,                           
                                cast(Leave as int) as TotalLeave,                          
                                cast(Absent as int) as [TotalAbsent],
                                cast(TotalDeductionDays as int) as TotalDeductionDays,                           
                                cast(PayableWorkingDays as int) as PayableWorkingDays, 
                                cast(sum(PayableWorkingDays / 18)as int) as TotalEarnLeave, 
                                cast(EarnLeave as int) as EnjoyableLeave, cast(sum(PayableWorkingDays / 18) as int)-(EarnLeave) as PayableEarnLeave,
                                cast(Present_Salary as decimal(18,2))as Present_Salary,cast(Stamp as int)as Stamp,                    
                                (cast(sum(((PayableWorkingDays / 18)-EarnLeave) * OneDaySalary) as decimal(18,2))-Stamp) as PayableEarnLeaveTK, '' As [Signature]
                                from( Select ID, SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, Convert(varchar(100),LastPaymentDate,126) as LastPaymentDate,
                                Total_Month, Working_Days,Holiday,OffDay, Leave,EarnLeave,[Absent],
                                sum(Holiday+OffDay + Leave + [Absent]) as TotalDeductionDays,
                                sum(Working_Days - (Holiday+ OffDay + Leave + [Absent])) as PayableWorkingDays,sum(BasicSalary + HouseRent+ Others) as Present_Salary,
                                sum((BasicSalary + HouseRent+ Others) / 30) as OneDaySalary, Stamp from 					                             
                                (Select e.ID, s.SectionName, e.CardTitle,e.EmployeeIdentity, e.Name, d.Name as Designation,                          
                                convert(varchar(100), Joining_Date, 126) as Joining_Date,                          
                                (Select [dbo].[fn_LastPaymentDate](e.ID," + t + ")) as LastPaymentDate, " +
                                "(select [dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID,'" + dt1 + "', 1," + t + ")) as Total_Month,  " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 2," + t + ")) as Working_Days, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 3," + t + ")) as Holiday, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 4," + t + ")) as Leave, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 5," + t + ")) as EarnLeave, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 6," + t + ")) as OffDay, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 7," + t + ")) as [Absent], " +
                               "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID = e.ID and Eod_RefFk = 1 and[Status] = 1) as BasicSalary, " +
                               "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID = e.ID and Eod_RefFk = 2 and[Status] = 1) as HouseRent," +
                               "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID = e.ID and Eod_RefFk = 3 and[Status] = 1) as Others, " +
                               "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID = e.ID and Eod_RefFk = 12 and[Status] = 1) as Stamp " +
                               "from HRMS_Employee e join Sections s on e.Section_Id = s.Id inner join Designations d on e.Designation_Id = d.ID where  e.Section_Id = " + model.SectionId + " and e.Present_Status=1)t " +
                               "group by ID, SectionName, CardTitle, EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate, Total_Month, Working_Days, OffDay, Holiday, Leave, EarnLeave,[Absent], BasicSalary, HouseRent, Stamp, Stamp ) tbl  where tbl.Working_Days > 17 " +
                               "group by ID, SectionName, CardTitle, EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate, Total_Month, Working_Days, OffDay, Holiday, Leave, EarnLeave,[Absent], Present_Salary, Stamp, TotalDeductionDays, PayableWorkingDays ").ToList();


                if (EarnleaveInfo.Any())
                {
                    leaveInfo.DataList = EarnleaveInfo;
                }
            }
            else if (model.IsAudit == true)
            {
                var EarnleaveInfo = db.Database.SqlQuery<VM_HRMS_Earn_Leave_Payment>($@"Select ID, SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate as PaymentDate, 
                                cast(Total_Month as int) as Total_Month,cast(Working_Days as int) as WorkingDays,                           
                                cast(Holiday as int) as Holiday,cast(OffDay as int) as OffDay,                           
                                cast(Leave as int) as TotalLeave,                          
                                cast(Absent as int) as [TotalAbsent],cast(TotalDeductionDays as int) as TotalDeductionDays,                           
                                cast(PayableWorkingDays as int) as PayableWorkingDays, cast(sum(PayableWorkingDays / 18)as int) as TotalEarnLeave, 
                                cast(EarnLeave as int) as EnjoyableLeave, cast(sum(PayableWorkingDays / 18) as int)-(EarnLeave) as PayableEarnLeave,
                                cast(Present_Salary as decimal(18,2))as Present_Salary,cast(Stamp as int)as Stamp,                    
                                (cast(sum(((PayableWorkingDays / 18)-EarnLeave) * OneDaySalary) as decimal(18,2))-Stamp) as PayableEarnLeaveTK, '' As [Signature]
                                from( Select ID, SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, Convert(varchar(10),LastPaymentDate,126) as LastPaymentDate,
                                Total_Month, Working_Days,Holiday,OffDay, Leave,EarnLeave,[Absent],
                                sum(Holiday+OffDay + Leave + [Absent]) as TotalDeductionDays,
                                sum(Working_Days - (Holiday+ OffDay + Leave + [Absent])) as PayableWorkingDays,sum(BasicSalary + HouseRent+ Others) as Present_Salary,
                                sum((BasicSalary + HouseRent+ Others) / 30) as OneDaySalary, Stamp from 					                             
                                (Select e.ID, s.SectionName, e.CardTitle,e.EmployeeIdentity, e.Name, d.Name as Designation,                          
                                convert(varchar(10), Joining_Date, 126) as Joining_Date,                          
                                (Select [dbo].[fn_LastPaymentDate](e.ID," + t + ")) as LastPaymentDate, " +
                               "(select [dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID,'" + dt1 + "', 1," + t + ")) as Total_Month,  " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 2," + t + ")) as Working_Days, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 3," + t + ")) as Holiday, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 4," + t + ")) as Leave, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 5," + t + ")) as EarnLeave, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 6," + t + ")) as OffDay, " +
                               "(select[dbo].[fn_Total_Month_WorkingDays_Holiday_OffDay_Absent_Leave_Count](e.ID, '" + dt1 + "', 7," + t + ")) as [Absent], " +
                               "(select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID = e.ID and Eod_RefFk = 1 and[Status] = 1) as BasicSalary, " +
                               "(select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID = e.ID and Eod_RefFk = 2 and[Status] = 1) as HouseRent," +
                               "(select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID = e.ID and Eod_RefFk = 3 and[Status] = 1) as Others, " +
                               "(select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID = e.ID and Eod_RefFk = 12 and[Status] = 1) as Stamp " +
                               "from HRMS_Employee e join Sections s on e.Section_Id = s.Id inner join Designations d on e.Designation_Id = d.ID where e.Section_Id = " + model.SectionId + " and e.Present_Status=1)t " +
                               "group by ID, SectionName, CardTitle, EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate, Total_Month, Working_Days, OffDay, Holiday, Leave, EarnLeave,[Absent], BasicSalary, HouseRent, Stamp, Stamp ) tbl  where tbl.Working_Days > 17  " +
                               "group by ID, SectionName, CardTitle, EmployeeIdentity, Name, Designation, Joining_Date, LastPaymentDate, Total_Month, Working_Days, OffDay, Holiday, Leave, EarnLeave,[Absent], Present_Salary, Stamp, TotalDeductionDays, PayableWorkingDays ").ToList();


                if (EarnleaveInfo.Any())
                {
                    leaveInfo.DataList = EarnleaveInfo;
                }
            }

            return leaveInfo;
        }

        public void EarnLeaveCalculationSave(VM_HRMS_Earn_Leave_Payment model)
        {
            var dt1 = model.ToDate.ToString("yyyy-MM-dd");
            var ds = 0;
            ds = db.Database.ExecuteSqlCommand("[dbo].[Sp_EmployeeEarnLeaveCalculationAndSave] @eid=" + model.EmployeeId + ", @dt='"+ dt1 + "', @isAudit='" + model.IsAudit + "'");
            if (ds > 0)
            {
                var vl = 1;
            }
        }

        public void SectionWiseEarnLeaveCalculationSave(VM_HRMS_Earn_Leave_Payment model)
        {
            var dt1 = model.ToDate.ToString("yyyy-MM-dd");
            model.EmployeeIds = model.EmployeeIds[0].TrimEnd(',').Split(',').ToList();
            var list = "";
            list = String.Join(",", model.EmployeeIds);
            var ds = 0;
            ds = db.Database.ExecuteSqlCommand("[dbo].[Sp_SectionWiseEmployeeEarnLeaveCalculationAndSave] @EmployeeId='" + list + "', @dt='" + dt1 + "', @isAudit='" + model.IsAudit + "'");
            if (ds > 0)
            {
                var vl = 1;
            }
        }

        public VM_HRMS_Earn_Leave_Payment EarnLeaveCalculationInformation(VM_HRMS_Earn_Leave_Payment model)
        {
            var leaveInfo = new VM_HRMS_Earn_Leave_Payment();
            var dt1 = model.Fromdate.ToString("yyyy-MM-dd");
            var dt2 = model.ToDate.ToString("yyyy-MM-dd");
            int t = 0;
            if (model.IsAudit == false)
            {
                t = 0;
            }
            else if (model.IsAudit == true)
            {
                t = 1;
            }

            var EarnleaveInfo = db.Database.SqlQuery<VM_HRMS_Earn_Leave_Payment>($@"Select hh.[EmployeeId],s.SectionName,e.EmployeeIdentity,e.Name,d.Name as Designation,convert(varchar(10),e.Joining_Date,126) as [Joining_Date],hh.[Total_Month],hh.[WorkingDays],hh.[Holiday],hh.[OffDay],
                    hh.[TotalLeave],hh.[TotalAbsent],hh.[TotalDeductionDays],hh.[PayableWorkingDays],hh.[TotalEarnLeave],
                    hh.[EnjoyAbleEarnLeave],hh.[PayableEarnLeave],hh.[Stamp],hh.[PresentSalary],hh.[PayableEarnLeaveTk],
                    convert(varchar(10),hh.[PaymentDate],126) as [PaymentDate]
                    from [dbo].[HRMS_Earn_Leave_Payment] hh inner join HRMS_Employee e on hh.EmployeeId=e.ID 
                    inner Join Sections s on e.Section_Id=s.ID inner Join Designations d on e.Designation_Id =d.ID
                    where Convert(varchar(10), hh.PaymentDate, 126) between'" + dt1 + "' and '"+ dt2 + "' and hh.IsAudit = " +t+"").ToList();

            if (EarnleaveInfo.Any())
            {
                leaveInfo.DataList = EarnleaveInfo;
            }
            return leaveInfo;
        }

    }
}