﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.Leave
{
    public class VM_HRMS_Employee_OffDay :RootModel
    {
        [DisplayName("Employee Id.")]
        public int EmployeeId { get; set; }
        [DisplayName("Employee Name")]
        public string EmployeeName { get; set; }
        [DisplayName("Employee Id.")]
        public string EmployeeIdentity { get; set; }

        [DisplayName("Off Day")]
        public string OffDay { get; set; }

        public ICollection<VM_HRMS_Employee_OffDay> OffDays { get; set; }

        private HrmsContext db = new HrmsContext();
        public void OffDaysList()
        {
            var employeeOffdays = (from em in db.Employees
                join od in db.HrmsEmployeeOffDays on em.ID equals od.EmployeeId
                select new VM_HRMS_Employee_OffDay()
                {
                    ID = od.ID, OffDay = od.OffDay, EmployeeId = em.ID, EmployeeIdentity = em.EmployeeIdentity,
                    EmployeeName = em.Name
                }).ToList();

            OffDays = employeeOffdays;
        }

        public List<VM_HRMS_Employee_OffDay> EmployeeOffDaysList()
        {
            var employeeOffdays = (from em in db.Employees
                join od in db.HrmsEmployeeOffDays on em.ID equals od.EmployeeId
                select new VM_HRMS_Employee_OffDay()
                {
                    ID = od.ID,
                    OffDay = od.OffDay,
                    EmployeeId = em.ID,
                    EmployeeIdentity = em.EmployeeIdentity,
                    EmployeeName = em.Name
                }).ToList();

            return employeeOffdays;
        }
    }
}