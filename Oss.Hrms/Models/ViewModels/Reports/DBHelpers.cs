﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Oss.Hrms.Models.ViewModels.Reports
{
    public class DBHelpers
    {
        public List<DateTime> WeekendDayList { get; set; }
        public List<DateTime> HolidayList { get; set; }
        public List<DateTime> AllOffDayList { get; set; }
        public List<VMAttendanceSummary> SummaryList { get; set; }
        public VMAttendanceSummary VMAttendanceSummary { get; set; }

        public void GetAllOffDay(DateTime FromDate, DateTime ToDate, int EmployeeId)
        {
            HrmsContext db = new HrmsContext();
            List<DateTime> lstDay = new List<DateTime>();
            
            var getOffDay = db.HrmsEmployeeOffDays.Where(a=>a.EmployeeId== EmployeeId && a.Status==true);
            if (getOffDay.Any())
            {
                string OffDays = getOffDay.FirstOrDefault().OffDay;
                GetAllWeekendDays(FromDate, ToDate, OffDays);
            }
            else
            {
                GetAllWeekendDays(FromDate, ToDate, "Friday");
            }
            
            if (this.WeekendDayList.Any())
            {
                foreach (var v in this.WeekendDayList)
                {
                    lstDay.Add(v.Date);
                }
                this.AllOffDayList = lstDay;
            }

            var recordHoliday = db.HrmsHolidays.Where(a => a.OffDay >= FromDate && a.OffDay <= ToDate && a.Status == true).Select(a => a.OffDay).ToList();

            var attendanceHoliday = db.HrmsAttendanceHistory.Where(a => a.EmployeeId == EmployeeId).Select(a => a.Date).ToList();
            List<DateTime> lst = new List<DateTime>();
            if (recordHoliday.Any() && attendanceHoliday.Any())
            {
                var holiday = attendanceHoliday.Intersect(recordHoliday).ToList();
                
                if (holiday.Any())
                {
                    foreach (var v in holiday)
                    {
                        lst.Add(v.Date);
                        if (!AllOffDayList.Any(a => a.Date == v.Date))
                        {
                            AllOffDayList.Add(v.Date);
                        }
                    }
                    this.HolidayList = lst;
                }
            }
            else if (recordHoliday.Any() && !attendanceHoliday.Any())
            {
                foreach (var v in recordHoliday)
                {
                    lst.Add(v.Date);
                    if (!AllOffDayList.Any(a => a.Date == v.Date))
                    {
                        AllOffDayList.Add(v.Date);
                    }
                }
                this.HolidayList = lst;
            }
            else if (!recordHoliday.Any() && !attendanceHoliday.Any())
            {
                this.HolidayList = lst;
            }
        }

        public void GetAllWeekendDays(DateTime StartDate,DateTime EndDate, string WeekendDay)
        {
            #region FindDay
            if (WeekendDay.Equals("Saturday"))
            {
                this.WeekendDayList = GetDays(StartDate, EndDate, DayOfWeek.Saturday);
            }
            else if (WeekendDay.Equals("Sunday"))
            {
                this.WeekendDayList = GetDays(StartDate, EndDate, DayOfWeek.Sunday);
            }
            else if (WeekendDay.Equals("Monday"))
            {
                this.WeekendDayList = GetDays(StartDate, EndDate, DayOfWeek.Monday);
            }
            else if (WeekendDay.Equals("Tuesday"))
            {
                this.WeekendDayList = GetDays(StartDate, EndDate, DayOfWeek.Tuesday);
            }
            else if (WeekendDay.Equals("Wednesday"))
            {
                this.WeekendDayList = GetDays(StartDate, EndDate, DayOfWeek.Wednesday);
            }
            else if (WeekendDay.Equals("Thursday"))
            {
                this.WeekendDayList = GetDays(StartDate, EndDate, DayOfWeek.Thursday);
            }
            else if (WeekendDay.Equals("Friday"))
            {
                this.WeekendDayList = GetDays(StartDate, EndDate, DayOfWeek.Friday);
            }
            #endregion
        }

        public List<DateTime> GetDays(DateTime FromDate,DateTime ToDate, DayOfWeek day)
        {
            List<DateTime> lstDay = new List<DateTime>();
            for (DateTime date = FromDate; date <= ToDate; date = date.AddDays(1))
            {
                if (date.DayOfWeek == day)
                {
                    lstDay.Add(date);
                }
            }

            return lstDay;
        }

        public int GetHolidays(DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            int holiday = db.HrmsHolidays.Where(a => a.OffDay >= FromDate && a.OffDay <= ToDate && a.Status == true).Select(a => a.OffDay).ToList().Count();
            return holiday;
        }

        public void GetAttendanceSummary(int SectionId, DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            List<VMAttendanceSummary> lstSum = new List<VMAttendanceSummary>();
            int TotalDays = (ToDate - FromDate).Days + 1;
            int presentCount = 0;
            int lateCount = 0;
            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.Date >= FromDate && a.Date <= ToDate).ToList();
            var getEmployee = db.Employees.Where(a=>a.Section_Id==SectionId && a.Present_Status==1).ToList();
            var getAllleave = db.HrmsLeaveApplications.Where(o=>o.From >= FromDate && o.To <= ToDate).ToList();
            if (getEmployee.Any())
            {
                foreach (var emp in getEmployee)
                {
                    presentCount = 0;
                    //var lateDays = allAttendance.Where(a =>a.EmployeeId==emp.ID && a.AttendanceStatus == 2).Count();
                    var abDays = allAttendance.Where(a =>a.EmployeeId==emp.ID && a.AttendanceStatus == 3).Count();
                    var levDays = allAttendance.Where(a =>a.EmployeeId==emp.ID && a.AttendanceStatus == 4).Count();
                    var EL = getAllleave.Where(a => a.EmployeeId == emp.ID && a.LeaveTypeId == 1).Sum(a => a.Days);
                    var CL= getAllleave.Where(a => a.EmployeeId == emp.ID && a.LeaveTypeId==192).Sum(a=>a.Days);
                    //var SL = getAllleave.Where(a => a.EmployeeId == emp.ID && a.LeaveTypeId == 193).Sum(a => a.Days);
                    var ML = getAllleave.Where(a => a.EmployeeId == emp.ID && a.LeaveTypeId == 196).Sum(a => a.Days);
                    
                    GetAllOffDay(FromDate, ToDate, emp.ID);
                    
                    VMAttendanceSummary m = new VMAttendanceSummary();
                    m.EmployeeID = emp.ID;
                    m.CL = (int)CL;
                    m.EL = (int)EL;
                    m.ML = (int)ML;
                    m.TotalLeaveDays = levDays;
                    foreach (var v in allAttendance.Where(a => a.EmployeeId == emp.ID))
                    {
                        m.PresentDays = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 1 || v.AttendanceStatus == 2 ? ++presentCount : presentCount : presentCount;
                        m.LateDay = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 2 ? ++lateCount : lateCount : lateCount;
                        if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                        {
                            if (v.PayableOverTime <= 2)
                            {
                                m.LegalOTHour += (int)v.PayableOverTime;
                            }
                            else if (v.PayableOverTime > 2)
                            {
                                m.LegalOTHour += 2;
                                m.ExtraOTHour += (int)v.PayableOverTime - 2;
                            }
                        }
                        else
                        {
                            m.ExtraOTHour += (int)v.PayableOverTime;
                        }
                        m.TotalOTHour += (int)v.PayableOverTime;
                    }
                    m.OTHour = m.TotalOTHour - m.ExtraOTHour;
                    m.TotalPresentDays = m.PresentDays;
                    m.MonthDays = TotalDays;
                    m.HoliDays = this.HolidayList != null ? this.HolidayList.Count() : 0;
                    m.OffDays = this.WeekendDayList != null ? this.WeekendDayList.Count() : 0;

                    m.WorkDays = m.HoliDays + m.OffDays + m.PresentDays;
                    m.AbsentDays = TotalDays - (m.WorkDays + m.TotalLeaveDays);

                    lstSum.Add(m);
                }
            }
            this.SummaryList = lstSum;
        }

        public void GetAttendance(int EmployeeId, DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            int TotalDays = (ToDate - FromDate).Days + 1;
            int presentCount = 0;
            int lateCount = 0;
            TimeSpan Empty = new TimeSpan(00, 00, 00);
            
            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.EmployeeId == EmployeeId && a.Date >= FromDate && a.Date <= ToDate).ToList();
            var getAllleave = db.HrmsLeaveApplications.Where(o =>o.EmployeeId== EmployeeId && o.From >= FromDate && o.To <= ToDate).ToList();
            var abDays = allAttendance.Where(a =>a.AttendanceStatus == 3).Count();
            var levDays = allAttendance.Where(a =>a.AttendanceStatus == 4).Count();

            var EL = getAllleave.Where(a => a.LeaveTypeId == 1).Sum(a => a.Days);
            var CL = getAllleave.Where(a => a.LeaveTypeId == 192).Sum(a => a.Days);
            //var SL = getAllleave.Where(a => a.LeaveTypeId == 193).Sum(a => a.Days);
            var ML = getAllleave.Where(a => a.LeaveTypeId == 196).Sum(a => a.Days);

            GetAllOffDay(FromDate, ToDate, EmployeeId);

            VMAttendanceSummary m = new VMAttendanceSummary();
            m.EmployeeID = EmployeeId;
            m.CL = (int)CL;
            m.EL = (int)EL;
            m.ML = (int)ML;
            m.TotalLeaveDays = levDays;
            foreach (var v in allAttendance.Where(a => a.EmployeeId == EmployeeId))
            {
                //if(v.InTime.TimeOfDay== Empty && v.OutTime.TimeOfDay== Empty && v.Status)
                m.PresentDays = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 1 && v.InTime.TimeOfDay!=Empty && v.OutTime.TimeOfDay!=Empty  || v.AttendanceStatus==2 ? ++presentCount : presentCount : presentCount;
                m.LateDay = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 2 ? ++lateCount : lateCount : lateCount;
                if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                {
                    if (v.PayableOverTime <= 2)
                    {
                        m.LegalOTHour += (int)v.PayableOverTime;
                    }
                    else if (v.PayableOverTime > 2)
                    {
                        m.LegalOTHour += 2;
                        m.ExtraOTHour += (int)v.PayableOverTime - 2;
                    }
                }
                else
                {
                    m.ExtraOTHour += (int)v.PayableOverTime;
                }
                m.TotalOTHour += (int)v.PayableOverTime;
            }

            m.OTHour = m.TotalOTHour - m.ExtraOTHour;
            m.TotalPresentDays = m.PresentDays;
            m.MonthDays = TotalDays;
            m.HoliDays = this.HolidayList != null ? this.HolidayList.Count() : 0;
            m.OffDays = this.WeekendDayList != null ? this.WeekendDayList.Count() : 0;

            m.WorkDays = m.HoliDays + m.OffDays + m.PresentDays + m.TotalLeaveDays;
            m.AbsentDays = m.MonthDays - m.WorkDays;
            
            this.VMAttendanceSummary = m;
        }

        public void GetAttendanceForNewJoin(int EmployeeId, DateTime FromDate, DateTime ToDate, DateTime JoinDate)
        {
            HrmsContext db = new HrmsContext();
            TimeSpan Empty = new TimeSpan(00, 00, 00);
            int PayrollDays = (ToDate - FromDate).Days + 1;
            int presentCount = 0;
            int lateCount = 0;
            int HoliDayCounter = 0;
            int OffDayCounter = 0;

            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.EmployeeId == EmployeeId && a.Date >= JoinDate && a.Date <= ToDate).ToList();
            var getAllleave = db.HrmsLeaveApplications.Where(o => o.EmployeeId == EmployeeId && o.From >= JoinDate && o.To <= ToDate).ToList();
            var abDays = allAttendance.Where(a => a.AttendanceStatus == 3).Count();
            var levDays = allAttendance.Where(a => a.AttendanceStatus == 4).Count();

            var EL = getAllleave.Where(a => a.LeaveTypeId == 1).Sum(a => a.Days);
            var CL = getAllleave.Where(a => a.LeaveTypeId == 192).Sum(a => a.Days);
            //var SL = getAllleave.Where(a => a.LeaveTypeId == 193).Sum(a => a.Days);
            var ML = getAllleave.Where(a => a.LeaveTypeId == 196).Sum(a => a.Days);

            GetAllOffDay(FromDate, ToDate, EmployeeId);

            VMAttendanceSummary m = new VMAttendanceSummary();
            m.EmployeeID = EmployeeId;
            m.CL = (int)CL;
            m.EL = (int)EL;
            m.ML = (int)ML;
            m.TotalLeaveDays = levDays;
            foreach (var v in allAttendance.Where(a => a.EmployeeId == EmployeeId))
            {
                m.PresentDays = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 1 && v.InTime.TimeOfDay != Empty && v.OutTime.TimeOfDay != Empty || v.AttendanceStatus == 2 ? ++presentCount : presentCount : presentCount;
                //m.PresentDays = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 1 || v.AttendanceStatus == 2 ? ++presentCount : presentCount : presentCount;
                m.LateDay = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 2 ? ++lateCount : lateCount : lateCount;
                if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                {
                    if (v.PayableOverTime <= 2)
                    {
                        m.LegalOTHour += (int)v.PayableOverTime;
                    }
                    else if (v.PayableOverTime > 2)
                    {
                        m.LegalOTHour += 2;
                        m.ExtraOTHour += (int)v.PayableOverTime - 2;
                    }
                }
                else
                {
                    m.ExtraOTHour += (int)v.PayableOverTime;
                }
                m.TotalOTHour += (int)v.PayableOverTime;
            }
            
            HoliDayCounter = 0;
            OffDayCounter = 0;
            for (DateTime date = FromDate; date <= JoinDate; date = date.AddDays(1))
            {
                HoliDayCounter = this.HolidayList != null ? this.HolidayList.Any(a => a.Date == date) == true ? ++HoliDayCounter : HoliDayCounter : HoliDayCounter;
                OffDayCounter = this.WeekendDayList != null ? this.WeekendDayList.Any(a => a.Date == date) == true ? ++OffDayCounter : OffDayCounter : OffDayCounter;              
            }
            
            m.MonthDays = PayrollDays;
            m.EmployeeWorkDays = (ToDate - JoinDate).Days + 1;
            m.OTHour = m.TotalOTHour - m.ExtraOTHour;
            m.TotalPresentDays = m.PresentDays;
            
            m.HoliDays = this.HolidayList != null ? this.HolidayList.Count() : 0;
            m.OffDays = this.WeekendDayList != null ? this.WeekendDayList.Count() : 0;

            m.WorkDays = (m.HoliDays-HoliDayCounter) + (m.OffDays-OffDayCounter) + m.PresentDays + m.TotalLeaveDays;
            m.JoinedAbsentDays = m.EmployeeWorkDays - m.WorkDays;
            m.AbsentDays = m.MonthDays - m.WorkDays;

            this.VMAttendanceSummary = m;
        }

        public void GetAttendanceForLefty(int EmployeeId, DateTime FromDate, DateTime ToDate, DateTime LeftDate)
        {
            HrmsContext db = new HrmsContext();
            TimeSpan Empty = new TimeSpan(00, 00, 00);
            int PayrollDays = (ToDate - FromDate).Days + 1;
            int presentCount = 0;
            int lateCount = 0;
            int HoliDayCounter = 0;
            int OffDayCounter = 0;

            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.EmployeeId == EmployeeId && a.Date >= FromDate && a.Date <= LeftDate).ToList();
            var getAllleave = db.HrmsLeaveApplications.Where(o => o.EmployeeId == EmployeeId && o.From >= FromDate && o.To <= LeftDate).ToList();
            var abDays = allAttendance.Where(a => a.AttendanceStatus == 3).Count();
            var levDays = allAttendance.Where(a => a.AttendanceStatus == 4).Count();

            var EL = getAllleave.Where(a => a.LeaveTypeId == 1).Sum(a => a.Days);
            var CL = getAllleave.Where(a => a.LeaveTypeId == 192).Sum(a => a.Days);
            //var SL = getAllleave.Where(a => a.LeaveTypeId == 193).Sum(a => a.Days);
            var ML = getAllleave.Where(a => a.LeaveTypeId == 196).Sum(a => a.Days);

            GetAllOffDay(FromDate, ToDate, EmployeeId);

            VMAttendanceSummary m = new VMAttendanceSummary();
            m.EmployeeID = EmployeeId;
            m.CL = (int)CL;
            m.EL = (int)EL;
            m.ML = (int)ML;
            m.TotalLeaveDays = levDays;
            foreach (var v in allAttendance.Where(a => a.EmployeeId == EmployeeId))
            {
                //m.PresentDays = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 1 || v.AttendanceStatus == 2 ? ++presentCount : presentCount : presentCount;
                m.PresentDays = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 1 && v.InTime.TimeOfDay != Empty && v.OutTime.TimeOfDay != Empty || v.AttendanceStatus == 2 ? ++presentCount : presentCount : presentCount;
                m.LateDay = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 2 ? ++lateCount : lateCount : lateCount;
                if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                {
                    if (v.PayableOverTime <= 2)
                    {
                        m.LegalOTHour += (int)v.PayableOverTime;
                    }
                    else if (v.PayableOverTime > 2)
                    {
                        m.LegalOTHour += 2;
                        m.ExtraOTHour += (int)v.PayableOverTime - 2;
                    }
                }
                else
                {
                    m.ExtraOTHour += (int)v.PayableOverTime;
                }
                m.TotalOTHour += (int)v.PayableOverTime;
            }

            HoliDayCounter = 0;
            OffDayCounter = 0;
            for (DateTime date = LeftDate; date <= ToDate; date = date.AddDays(1))
            {
                HoliDayCounter = this.HolidayList != null ? this.HolidayList.Any(a => a.Date == date) == true ? ++HoliDayCounter : HoliDayCounter : HoliDayCounter;
                OffDayCounter = this.WeekendDayList != null ? this.WeekendDayList.Any(a => a.Date == date) == true ? ++OffDayCounter : OffDayCounter : OffDayCounter;
            }

            m.MonthDays = PayrollDays;
            m.EmployeeWorkDays = (LeftDate - FromDate).Days;
            m.OTHour = m.TotalOTHour - m.ExtraOTHour;
            m.TotalPresentDays = m.PresentDays;

            m.HoliDays = this.HolidayList != null ? this.HolidayList.Count() : 0;
            m.OffDays = this.WeekendDayList != null ? this.WeekendDayList.Count() : 0;

            m.WorkDays = (m.HoliDays - HoliDayCounter) + (m.OffDays - OffDayCounter) + m.PresentDays + m.TotalLeaveDays;
            m.JoinedAbsentDays = m.EmployeeWorkDays - m.WorkDays;
            m.AbsentDays = m.MonthDays - m.WorkDays;

            this.VMAttendanceSummary = m;
        }

        #region AuditAttendance

        public void GetAuditRegularAttendance(int EmployeeId, DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            int TotalDays = (ToDate - FromDate).Days + 1;
            int presentCount = 0;
            int lateCount = 0;

            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.EmployeeId == EmployeeId && a.Date >= FromDate && a.Date <= ToDate).ToList();
            var getAllleave = db.HrmsLeaveApplications.Where(o => o.EmployeeId == EmployeeId && o.From >= FromDate && o.To <= ToDate).ToList();
            var abDays = allAttendance.Where(a => a.AttendanceStatus == 3).Count();
            var levDays = allAttendance.Where(a => a.AttendanceStatus == 4).Count();

            var EL = getAllleave.Where(a => a.LeaveTypeId == 1).Sum(a => a.Days);
            var CL = getAllleave.Where(a => a.LeaveTypeId == 192).Sum(a => a.Days);
            //var SL = getAllleave.Where(a => a.LeaveTypeId == 193).Sum(a => a.Days);
            var ML = getAllleave.Where(a => a.LeaveTypeId == 196).Sum(a => a.Days);

            GetAllOffDay(FromDate, ToDate, EmployeeId);

            VMAttendanceSummary m = new VMAttendanceSummary();
            m.EmployeeID = EmployeeId;
            m.CL = (int)CL;
            m.EL = (int)EL;
            m.ML = (int)ML;
            m.TotalLeaveDays = levDays;
            foreach (var v in allAttendance.Where(a => a.EmployeeId == EmployeeId))
            {
                m.PresentDays = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 1 || v.AttendanceStatus == 2 ? ++presentCount : presentCount : presentCount;
                m.LateDay = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 2 ? ++lateCount : lateCount : lateCount;
                if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                {
                    if (v.PayableOverTime <= 2)
                    {
                        m.LegalOTHour += (int)v.PayableOverTime;
                    }
                    else if (v.PayableOverTime > 2)
                    {
                        m.LegalOTHour += 2;
                        //m.ExtraOTHour += (int)v.PayableOverTime - 2;
                        m.ExtraOTHour += 1;
                    }
                }
                //else
                //{
                //    m.ExtraOTHour += (int)v.PayableOverTime;
                //}
                //m.TotalOTHour += (int)v.PayableOverTime;

            }

            //m.OTHour = m.TotalOTHour - m.ExtraOTHour;
            m.OTHour = m.ExtraOTHour;
            m.TotalPresentDays = m.PresentDays;
            m.MonthDays = TotalDays;
            m.HoliDays = this.HolidayList != null ? this.HolidayList.Count() : 0;
            m.OffDays = this.WeekendDayList != null ? this.WeekendDayList.Count() : 0;

            m.WorkDays = m.HoliDays + m.OffDays + m.PresentDays + m.TotalLeaveDays;
            m.AbsentDays = m.MonthDays - m.WorkDays;

            this.VMAttendanceSummary = m;
        }

        public void GetAuditNewJoinAttendance(int EmployeeId, DateTime FromDate, DateTime ToDate, DateTime JoinDate)
        {
            HrmsContext db = new HrmsContext();
            int PayrollDays = (ToDate - FromDate).Days + 1;
            int presentCount = 0;
            int lateCount = 0;
            int HoliDayCounter = 0;
            int OffDayCounter = 0;

            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.EmployeeId == EmployeeId && a.Date >= JoinDate && a.Date <= ToDate).ToList();
            var getAllleave = db.HrmsLeaveApplications.Where(o => o.EmployeeId == EmployeeId && o.From >= JoinDate && o.To <= ToDate).ToList();
            var abDays = allAttendance.Where(a => a.AttendanceStatus == 3).Count();
            var levDays = allAttendance.Where(a => a.AttendanceStatus == 4).Count();

            var EL = getAllleave.Where(a => a.LeaveTypeId == 1).Sum(a => a.Days);
            var CL = getAllleave.Where(a => a.LeaveTypeId == 192).Sum(a => a.Days);
            //var SL = getAllleave.Where(a => a.LeaveTypeId == 193).Sum(a => a.Days);
            var ML = getAllleave.Where(a => a.LeaveTypeId == 196).Sum(a => a.Days);

            GetAllOffDay(FromDate, ToDate, EmployeeId);

            VMAttendanceSummary m = new VMAttendanceSummary();
            m.EmployeeID = EmployeeId;
            m.CL = (int)CL;
            m.EL = (int)EL;
            m.ML = (int)ML;
            m.TotalLeaveDays = levDays;
            foreach (var v in allAttendance.Where(a => a.EmployeeId == EmployeeId))
            {
                m.PresentDays = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 1 || v.AttendanceStatus == 2 ? ++presentCount : presentCount : presentCount;
                m.LateDay = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 2 ? ++lateCount : lateCount : lateCount;
                if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                {
                    if (v.PayableOverTime <= 2)
                    {
                        m.LegalOTHour += (int)v.PayableOverTime;
                    }
                    else if (v.PayableOverTime > 2)
                    {
                        m.LegalOTHour += 2;
                        //m.ExtraOTHour += (int)v.PayableOverTime - 2;
                        m.ExtraOTHour += 1;
                    }
                }
                //else
                //{
                //    m.ExtraOTHour += (int)v.PayableOverTime;
                //}
                //m.TotalOTHour += (int)v.PayableOverTime;
            }

            HoliDayCounter = 0;
            OffDayCounter = 0;
            for (DateTime date = FromDate; date <= JoinDate; date = date.AddDays(1))
            {
                HoliDayCounter = this.HolidayList != null ? this.HolidayList.Any(a => a.Date == date) == true ? ++HoliDayCounter : HoliDayCounter : HoliDayCounter;
                OffDayCounter = this.WeekendDayList != null ? this.WeekendDayList.Any(a => a.Date == date) == true ? ++OffDayCounter : OffDayCounter : OffDayCounter;
            }

            m.MonthDays = PayrollDays;
            m.EmployeeWorkDays = (ToDate - JoinDate).Days + 1;
            m.OTHour = m.ExtraOTHour;
            //m.OTHour = m.TotalOTHour - m.ExtraOTHour;
            m.TotalPresentDays = m.PresentDays;

            m.HoliDays = this.HolidayList != null ? this.HolidayList.Count() : 0;
            m.OffDays = this.WeekendDayList != null ? this.WeekendDayList.Count() : 0;

            m.WorkDays = (m.HoliDays - HoliDayCounter) + (m.OffDays - OffDayCounter) + m.PresentDays + m.TotalLeaveDays;
            m.JoinedAbsentDays = m.EmployeeWorkDays - m.WorkDays;
            m.AbsentDays = m.MonthDays - m.WorkDays;

            this.VMAttendanceSummary = m;
        }

        public void GetAuditLeftyAttendance(int EmployeeId, DateTime FromDate, DateTime ToDate, DateTime LeftDate)
        {
            HrmsContext db = new HrmsContext();
            int PayrollDays = (ToDate - FromDate).Days + 1;
            int presentCount = 0;
            int lateCount = 0;
            int HoliDayCounter = 0;
            int OffDayCounter = 0;

            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.EmployeeId == EmployeeId && a.Date >= FromDate && a.Date <= LeftDate).ToList();
            var getAllleave = db.HrmsLeaveApplications.Where(o => o.EmployeeId == EmployeeId && o.From >= FromDate && o.To <= LeftDate).ToList();
            var abDays = allAttendance.Where(a => a.AttendanceStatus == 3).Count();
            var levDays = allAttendance.Where(a => a.AttendanceStatus == 4).Count();

            var EL = getAllleave.Where(a => a.LeaveTypeId == 1).Sum(a => a.Days);
            var CL = getAllleave.Where(a => a.LeaveTypeId == 192).Sum(a => a.Days);
            //var SL = getAllleave.Where(a => a.LeaveTypeId == 193).Sum(a => a.Days);
            var ML = getAllleave.Where(a => a.LeaveTypeId == 196).Sum(a => a.Days);

            GetAllOffDay(FromDate, ToDate, EmployeeId);

            VMAttendanceSummary m = new VMAttendanceSummary();
            m.EmployeeID = EmployeeId;
            m.CL = (int)CL;
            m.EL = (int)EL;
            m.ML = (int)ML;
            m.TotalLeaveDays = levDays;
            foreach (var v in allAttendance.Where(a => a.EmployeeId == EmployeeId))
            {
                m.PresentDays = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 1 || v.AttendanceStatus == 2 ? ++presentCount : presentCount : presentCount;
                m.LateDay = this.AllOffDayList.Any(a => a.Date == v.Date) == false ? v.AttendanceStatus == 2 ? ++lateCount : lateCount : lateCount;
                if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                {
                    if (v.PayableOverTime <= 2)
                    {
                        m.LegalOTHour += (int)v.PayableOverTime;
                    }
                    else if (v.PayableOverTime > 2)
                    {
                        m.LegalOTHour += 2;
                        //m.ExtraOTHour += (int)v.PayableOverTime - 2;
                        m.ExtraOTHour += 1;
                    }
                }
                //else
                //{
                //    m.ExtraOTHour += (int)v.PayableOverTime;
                //}
                //m.TotalOTHour += (int)v.PayableOverTime;
            }

            HoliDayCounter = 0;
            OffDayCounter = 0;
            for (DateTime date = LeftDate; date <= ToDate; date = date.AddDays(1))
            {
                HoliDayCounter = this.HolidayList != null ? this.HolidayList.Any(a => a.Date == date) == true ? ++HoliDayCounter : HoliDayCounter : HoliDayCounter;
                OffDayCounter = this.WeekendDayList != null ? this.WeekendDayList.Any(a => a.Date == date) == true ? ++OffDayCounter : OffDayCounter : OffDayCounter;
            }

            m.MonthDays = PayrollDays;
            m.EmployeeWorkDays = (LeftDate - FromDate).Days;
            //m.OTHour = m.TotalOTHour - m.ExtraOTHour;
            m.OTHour = m.ExtraOTHour;
            m.TotalPresentDays = m.PresentDays;

            m.HoliDays = this.HolidayList != null ? this.HolidayList.Count() : 0;
            m.OffDays = this.WeekendDayList != null ? this.WeekendDayList.Count() : 0;

            m.WorkDays = (m.HoliDays - HoliDayCounter) + (m.OffDays - OffDayCounter) + m.PresentDays + m.TotalLeaveDays;
            m.JoinedAbsentDays = m.EmployeeWorkDays - m.WorkDays;
            m.AbsentDays = m.MonthDays - m.WorkDays;

            this.VMAttendanceSummary = m;
        }
        public List<VMAttendanceSummary> GetAuditRegularAttendanceForBSCI(int EmployeeId, DateTime FromDate, DateTime ToDate)
        {

            List<VMAttendanceSummary> lstSummery = new List<VMAttendanceSummary>();

            HrmsContext db = new HrmsContext();
            int TotalDays = (ToDate - FromDate).Days + 1;

            var allAttendance = db.HrmsAttendanceHistory.Include(a => a.HrmsShiftType).Where(a => a.EmployeeId == EmployeeId && a.Date >= FromDate && a.Date <= ToDate).ToList();

            if (allAttendance.Any())
            {
                foreach (var v in allAttendance)
                {
                    VMAttendanceSummary m = new VMAttendanceSummary();
                    m.EmployeeID = EmployeeId;
                    m.PresentDate = v.Date;
                    m.InTime = v.InTime.TimeOfDay;
                    TimeSpan fixedTime = new TimeSpan(17, 00, 08);
                    switch (v.AttendanceStatus)
                    {
                        case 1:
                            m.Status = "Present";
                            break;
                        case 2:
                            m.Status = "Late";
                            break;
                        case 3:
                            m.Status = "Absent";
                            break;
                        case 4:
                            m.Status = "Leave";
                            break;
                        case 5:
                            m.Status = "Offday";
                            break;
                        case 6:
                            m.Status = "Holiday";
                            break;
                    }

                    if (v.PayableOverTime <= 3)
                    {
                        TimeSpan time1 = TimeSpan.FromHours((double)v.PayableOverTime);
                        m.OutTime = fixedTime.Add(time1);

                        m.LegalOTHour = (int)v.PayableOverTime;

                        m.OutTime = v.OutTime.TimeOfDay;

                        if (v.PayableOverTime == 3)
                        {
                            m.BuyerOT = 2;
                            m.ExtraOTHour = 1;
                        }
                        else
                        {
                            m.BuyerOT = 0;
                            m.ExtraOTHour = 0;
                        }

                    }
                    else if (v.PayableOverTime > 3)
                    {
                        m.LegalOTHour = 3;
                        m.BuyerOT = 2;
                        m.ExtraOTHour = 1;
                        if (v.PayableOverTime > 3 && v.PayableOverTime <= 4)
                        {
                            m.OutTime = v.OutTime.AddHours(-1).TimeOfDay;
                        }
                        else if (v.PayableOverTime > 4 && v.PayableOverTime <= 5)
                        {
                            m.OutTime = v.OutTime.AddHours(-2).TimeOfDay;
                        }
                        else if (v.PayableOverTime > 5 && v.PayableOverTime <= 6)
                        {
                            m.OutTime = v.OutTime.AddHours(-3).TimeOfDay;
                        }
                        else if (v.PayableOverTime > 6 && v.PayableOverTime <= 7)
                        {
                            m.OutTime = v.OutTime.AddHours(-4).TimeOfDay;
                        }
                        else if (v.PayableOverTime > 7 && v.PayableOverTime <= 8)
                        {
                            m.OutTime = v.OutTime.AddHours(-5).TimeOfDay;
                        }
                        else if (v.PayableOverTime > 8 && v.PayableOverTime <= 9)
                        {
                            m.OutTime = v.OutTime.AddHours(-6).TimeOfDay;
                        }
                        else if (v.PayableOverTime > 9 && v.PayableOverTime <= 10)
                        {
                            m.OutTime = v.OutTime.AddHours(-7).TimeOfDay;
                        }
                        //m.OutTime = v.OutTime.AddMinutes(-30).TimeOfDay;
                        //m.OutTime = v.HrmsShiftType.EndTime.AddHours(3).TimeOfDay;
                    }
                    lstSummery.Add(m);
                }
            }
            return lstSummery;
        }

        #endregion

        public void GetOTSummaryList(int SectionId, DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            List<VMAttendanceSummary> lstSum = new List<VMAttendanceSummary>();
            int HalfNightCount = 0;
            int FullNightCount = 0;
            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.Date >= FromDate && a.Date <= ToDate).ToList();
            var getEmployee = db.Employees.Where(a => a.Section_Id == SectionId && a.Present_Status == 1).ToList();
            if (getEmployee.Any())
            {
                foreach (var emp in getEmployee)
                {
                    HalfNightCount = 0;
                    FullNightCount = 0;
                    GetAllOffDay(FromDate, ToDate, emp.ID);
                    VMAttendanceSummary m = new VMAttendanceSummary();
                    m.EmployeeID = emp.ID;
                    if (allAttendance.Any(a=>a.EmployeeId==emp.ID))
                    {
                        foreach (var v in allAttendance.Where(a=>a.EmployeeId==emp.ID))
                        {
                            if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                            {
                                if (v.PayableOverTime <= 2)
                                {
                                    m.LegalOTHour += (int)v.PayableOverTime;
                                }
                                else if (v.PayableOverTime > 2)
                                {
                                    m.LegalOTHour += 2;
                                    m.ExtraOTHour += (int)v.PayableOverTime - 2;
                                    m.TotalHalfNight = v.PayableOverTime >= 7 && v.PayableOverTime < 9 ? ++HalfNightCount : HalfNightCount;
                                    m.TotalFullNight = v.PayableOverTime >= 9 ? ++FullNightCount : FullNightCount;
                                }
                            }
                            else
                            {
                                m.ExtraOTHour += (int)v.PayableOverTime;
                                m.TotalHalfNight = v.PayableOverTime >= 15 && v.PayableOverTime < 17 ? ++HalfNightCount : HalfNightCount;
                                m.TotalFullNight = v.PayableOverTime >= 17 ? ++FullNightCount : FullNightCount;
                            }
                            m.TotalOTHour += (int)v.PayableOverTime;
                        }
                    }
                    m.TotalNight = m.TotalHalfNight + m.TotalFullNight;
                    m.OTHour = m.TotalOTHour - m.ExtraOTHour;
                    lstSum.Add(m);
                }
            }
            
            this.SummaryList = lstSum;
        }

        public void GetOTSummary(int EmployeeId, DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            
            int HalfNightCount = 0;
            int FullNightCount = 0;
            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.Date >= FromDate && a.Date <= ToDate && a.EmployeeId == EmployeeId).ToList();

            GetAllOffDay(FromDate, ToDate, EmployeeId);

            VMAttendanceSummary m = new VMAttendanceSummary();
            m.EmployeeID = EmployeeId;
            if (allAttendance.Any())
            {
                foreach (var v in allAttendance)
                {
                    if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                    {
                        if (v.PayableOverTime <= 2)
                        {
                            m.LegalOTHour += (int)v.PayableOverTime;
                        }
                        else if (v.PayableOverTime > 2)
                        {
                            m.LegalOTHour += 2;
                            m.ExtraOTHour += (int)v.PayableOverTime - 2;
                            m.TotalHalfNight = v.PayableOverTime >= 7 && v.PayableOverTime < 9 ? ++HalfNightCount : HalfNightCount;
                            m.TotalFullNight = v.PayableOverTime >= 9 ? ++FullNightCount : FullNightCount;
                        }
                    }
                    else
                    {
                        m.ExtraOTHour += (int)v.PayableOverTime;
                        m.TotalHalfNight = v.PayableOverTime >= 15 && v.PayableOverTime < 17 ? ++HalfNightCount : HalfNightCount;
                        m.TotalFullNight = v.PayableOverTime >= 17 ? ++FullNightCount : FullNightCount;
                    }
                    m.TotalOTHour += (int)v.PayableOverTime;
                }
            }
            m.TotalNight = m.TotalHalfNight + m.TotalFullNight;
            m.OTHour = m.TotalOTHour - m.ExtraOTHour;

            this.VMAttendanceSummary = m;
        }

        public void GetOTSummaryForWorker(int EmployeeId, DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            int HalfNightCount = 0;
            int FullNightCount = 0;
            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.Date >= FromDate && a.Date <= ToDate && a.EmployeeId == EmployeeId).ToList();

            GetAllOffDay(FromDate, ToDate, EmployeeId);

            VMAttendanceSummary m = new VMAttendanceSummary();
            m.EmployeeID = EmployeeId;
            if (allAttendance.Any())
            {
                foreach (var v in allAttendance)
                {
                    if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                    {
                        if (v.PayableOverTime <= 2)
                        {
                            m.LegalOTHour += (int)v.PayableOverTime;
                        }
                        else if (v.PayableOverTime > 2)
                        {
                            m.LegalOTHour += 2;
                            m.ExtraOTHour += (int)v.PayableOverTime - 2;
                            m.TotalHalfNight = v.PayableOverTime >= 7 && v.PayableOverTime < 9 ? ++HalfNightCount : HalfNightCount;
                            m.TotalFullNight = v.PayableOverTime >= 9 ? ++FullNightCount : FullNightCount;
                        }
                    }
                    else
                    {
                        m.ExtraOTHour += (int)v.PayableOverTime;
                        m.TotalHalfNight = v.PayableOverTime >= 7 && v.PayableOverTime < 9 ? ++HalfNightCount : HalfNightCount;
                        m.TotalFullNight = v.PayableOverTime >= 9 ? ++FullNightCount : FullNightCount;
                    }
                    m.TotalOTHour += (int)v.PayableOverTime;
                }
            }
            m.TotalNight = m.TotalHalfNight + m.TotalFullNight;
            m.OTHour = m.TotalOTHour - m.ExtraOTHour;

            this.VMAttendanceSummary = m;
        }
        
        public void GetOTSummaryForStaff(int EmployeeId, DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            int HolidayWorkCounter = 0;
            int OffdayWorkCounter = 0;
            int HDCount = 0;
            int FullNightCount = 0;
            var allAttendance = db.HrmsAttendanceHistory.Where(a => a.Date >= FromDate && a.Date <= ToDate && a.EmployeeId == EmployeeId).ToList();

            GetAllOffDay(FromDate, ToDate, EmployeeId);

            VMAttendanceSummary m = new VMAttendanceSummary();
            m.EmployeeID = EmployeeId;
            if (allAttendance.Any())
            {
                foreach (var v in allAttendance)
                {
                    if (!this.AllOffDayList.Any(a => a.Date == v.Date))
                    {
                        if (v.PayableOverTime <= 2)
                        {
                            m.LegalOTHour += (int)v.PayableOverTime;
                        }
                        else if (v.PayableOverTime > 2)
                        {
                            m.LegalOTHour += 2;
                            m.ExtraOTHour += (int)v.PayableOverTime - 2;
                            m.TotalFullNight = v.PayableOverTime >= 6 ? ++FullNightCount : FullNightCount;
                        }
                    }
                    else
                    {
                        //Count Minimum 6 Hours for HD bill and Maximum Limit 8 Hours also 5 Hours for Night Bill Total 13 Hours for Both HD & Night Bill
                        m.ExtraOTHour += (int)v.PayableOverTime;
                        if (v.PayableOverTime >= 13)
                        {
                            m.TotalFullNight = ++FullNightCount;
                        }
                        m.TotalHD = v.PayableOverTime >= 6 ? ++HDCount : HDCount;
                    }
                    m.TotalOTHour += (int)v.PayableOverTime;
                }

                if (this.HolidayList != null && this.HolidayList.Any())
                {
                    foreach (var present in this.HolidayList)
                    {
                        m.HolydayWorkDay = allAttendance.Any(a => a.Date == present.Date && a.EmployeeId == EmployeeId && a.PayableOverTime >= 3) ? ++HolidayWorkCounter : HolidayWorkCounter;
                    }
                }

                if (this.WeekendDayList != null && this.WeekendDayList.Any())
                {
                    foreach (var present in this.WeekendDayList)
                    {
                        m.OffdayWorkDay = allAttendance.Any(a => a.Date == present.Date && a.EmployeeId == EmployeeId && a.PayableOverTime >= 3) ? ++OffdayWorkCounter : OffdayWorkCounter;
                    }
                }
            }
            m.TotalNight = m.TotalFullNight;
            m.OTHour = m.TotalOTHour - m.ExtraOTHour;

            this.VMAttendanceSummary = m;
        }
    }

    public class VMAttendanceSummary
    {
        public int HolydayWorkDay { get; set; }

        public int OffdayWorkDay { get; set; }

        public int MonthDays { get; set; }
        
        public int HoliDays { get; set; }

        public int OffDays { get; set; }

        public int PresentDays { get; set; }

        public int TotalPresentDays { get; set; }

        public int LateDay { get; set; }

        public int AbsentDays { get; set; }

        public int WorkDays { get; set; }

        public int CL { get; set; }

        public int EL { get; set; }

        public int ML { get; set; }

        public int LegalOTHour { get; set; }

        public int BuyerOT { get; set; }

        public int ExtraOTHour { get; set; }

        public int TotalOTHour { get; set; }

        public int TotalHalfNight { get; set; }

        public int TotalFullNight { get; set; }

        public int TotalHD { get; set; }

        public int TotalNight { get; set; }

        public int OTHour { get; set; }

        public int EmployeeID { get; set; }

        public int TotalLeaveDays { get; set; }

        public int EmployeeWorkDays { get; set; }

        public int JoinedAbsentDays { get; set; }

        public bool IsJoined { get; set; }

        public bool IsGross { get; set; }

        public string Status { get; set; }

        public DateTime PresentDate { get; set; }

        public TimeSpan InTime { get; set; }

        public TimeSpan OutTime { get; set; }
    }
}

