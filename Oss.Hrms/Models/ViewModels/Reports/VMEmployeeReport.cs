﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Reports
{
    public class VMEmployeeReport
    {

        private HrmsContext db = new HrmsContext();
        public List<VMEmployeeReport> DataList { get; set; }

        public List<VMEmployeeReport> TempDataList { get; set; }

        public int Counter { get; set; }
        [DisplayName("Employee Status"), Required(ErrorMessage = "Employee Status is required")]
        public int StatusId { get; set; }
        public int EmployeeId { get; set; }
        public int ReportId { get; set; }
        [DisplayName("Section Name")]
        public int? SectionId { get; set; }
        public int? DesignationId { get; set; }

        public bool IsChecked { get; set; }
        [Display(Name = "CardTitle")]
        public string CardTitle { get; set; }
        public string CardNo { get; set; }
        public string EmployeeName { get; set; }
        public string Section { get; set; }
        public string SectionBangla { get; set; }
        public string Designation { get; set; }
        public string DesignationBangla { get; set; }
        public string Grade { get; set; }
        public string Status { get; set; }
        public string BloodGroup { get; set; }
        public string PresentAddress { get; set; }
        public decimal JoiningSalary { get; set; }
        public string NationalID { get; set; }
        public string EmergencyPhone { get; set; }
        public string ImageName { get; set; }
        [Required(ErrorMessage = "Please select file.")]
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] files { get; set; }

        [DisplayName("From Date"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}"), Required(ErrorMessage = "From date date is required")]
        public DateTime Fromdate { get; set; }
        [DisplayName("To Date"), Required(ErrorMessage = "To date date is required")]
        public DateTime ToDate { get; set; }

        public DateTime JoinDate { get; set; }
        public DateTime DOB { get; set; }
        public DateTime ResignDate { get; set; }
        [Display(Name = "Card Issue Date"), Required(ErrorMessage = "Issue Date is required")]
        public DateTime CardIssueDate { get; set; }

        public string ViewJoinDate { get; set; }
        public string ViewResignDate { get; set; }




        public void GetEmployeeListForSelectedEmployeeReport(VMEmployeeReport model)
        {

            if (model.SectionId == 25000000)
            {
                var vData = (from o in db.Employees
                             join p in db.Sections on o.Section_Id equals p.ID
                             join q in db.Designations on o.Designation_Id equals q.ID
                             where o.Present_Status == 1
                             select new VMEmployeeReport
                             {

                                 EmployeeId = o.ID,
                                 CardNo = o.EmployeeIdentity,
                                 CardTitle = o.CardTitle,
                                 EmployeeName = o.Name,
                                 Section = p.SectionName,
                                 SectionBangla = p.SectionNameBangla,
                                 Designation = q.Name,
                                 DesignationBangla = q.DesigNameBangla,
                                 DesignationId = q.ID,
                                 DOB = o.DOB,
                                 JoinDate = o.Joining_Date,
                                 EmergencyPhone = o.Emergency_Contact_No,
                                 BloodGroup = o.Blood_Group,
                                 PresentAddress = o.Present_Address
                             }).OrderBy(a => a.CardNo).ToList();
                if (vData.Any())
                {
                    this.DataList = vData;
                }
            }
            else 
            {
                var vData = (from o in db.Employees
                             join p in db.Sections on o.Section_Id equals p.ID
                             join q in db.Designations on o.Designation_Id equals q.ID
                             where o.Section_Id == model.SectionId && o.Present_Status == 1
                             select new VMEmployeeReport
                             {

                                 EmployeeId = o.ID,
                                 CardNo = o.EmployeeIdentity,
                                 CardTitle = o.CardTitle,
                                 EmployeeName = o.Name,
                                 Section = p.SectionName,
                                 SectionBangla = p.SectionNameBangla,
                                 Designation = q.Name,
                                 DesignationBangla = q.DesigNameBangla,
                                 DesignationId = q.ID,
                                 DOB = o.DOB,
                                 JoinDate = o.Joining_Date,
                                 EmergencyPhone = o.Emergency_Contact_No,
                                 BloodGroup = o.Blood_Group,
                                 PresentAddress = o.Present_Address
                             }).OrderBy(a => a.CardNo).ToList();
                if (vData.Any())
                {
                    this.DataList = vData;
                }
            }
            
        }
    }
}