﻿using Oss.Hrms.Helper;
using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Reports
{
    public class VMReportPayroll
    {
        #region ConstantValue
        private decimal Medical = 600;
        private decimal Food = 900;
        private decimal Transport = 350;
        private decimal FoodHealthTrans = 1850;
        private decimal HouseRate = (decimal)0.5;

        #region Back Dated Salary
        //private decimal Medical = 250;
        //private decimal Food = 650;
        //private decimal Transport = 200;
        //private decimal FoodHealthTrans = 1100;
        //private decimal HouseRate = (decimal)0.4;
        #endregion
        private decimal StampDeduction = 10;
        private decimal DaysOfMonth = 30;

        #endregion

        public DataTable GetExcelSalarySheet(int SectionId, int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            DBHelpers dbh = new DBHelpers();

            DataTable ds1 = new DataTable();
            ds1.Columns.Add("Serial", typeof(string));
            ds1.Columns.Add("Card Title", typeof(string));
            ds1.Columns.Add("Card NO", typeof(string));
            ds1.Columns.Add("Name", typeof(string));
            ds1.Columns.Add("Designation", typeof(string));
            ds1.Columns.Add("Section", typeof(string));
            ds1.Columns.Add("JoinDate", typeof(string));
            ds1.Columns.Add("Grade", typeof(string));
            ds1.Columns.Add("D/M", typeof(string));
            ds1.Columns.Add("H/D", typeof(string));
            ds1.Columns.Add("F/D", typeof(string));
            ds1.Columns.Add("A/D", typeof(string));
            ds1.Columns.Add("A/B", typeof(string));
            ds1.Columns.Add("CL", typeof(string));
            ds1.Columns.Add("EL", typeof(string));
            ds1.Columns.Add("ML", typeof(string));
            ds1.Columns.Add("TP", typeof(string));
            ds1.Columns.Add("BasicPay", typeof(string));
            ds1.Columns.Add("House", typeof(string));
            ds1.Columns.Add("Medical", typeof(string));
            ds1.Columns.Add("Food", typeof(string));
            ds1.Columns.Add("Transport", typeof(string));
            ds1.Columns.Add("GrossWages", typeof(string));
            ds1.Columns.Add("AttendancePay", typeof(string));
            ds1.Columns.Add("AdsentDeduction", typeof(string));
            ds1.Columns.Add("Advance", typeof(string));
            ds1.Columns.Add("TotalDeduction", typeof(string));
            ds1.Columns.Add("OTHours", typeof(string));
            ds1.Columns.Add("OTRate", typeof(string));
            ds1.Columns.Add("OTTaka", typeof(string));
            ds1.Columns.Add("AttendanceBonus", typeof(string));
            ds1.Columns.Add("TotalAmount", typeof(string));
            ds1.Columns.Add("StampDeduction", typeof(string));
            ds1.Columns.Add("TotalPayable", typeof(string));
            ds1.Columns.Add("Remarks", typeof(string));

            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();

            //dbh.GetAttendanceSummary(SectionId, getPayroll.FromDate, getPayroll.ToDate);

            var vData = (from a in db.Employees
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         where a.Section_Id == SectionId && a.Present_Status == 1
                         select new
                         {
                             EmpID = a.ID,
                             EmployeeName = a.Name,
                             EmployeeBngName = a.Name_BN,
                             CardNo = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Grade = a.Grade,
                             Designation = b.Name,
                             Section = d.SectionName,
                             JoinDate = a.Joining_Date,
                             EmployeePayroll = (from t1 in db.HrmsPayrollInfos
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == a.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new
                                                {
                                                    RefId = t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate,
                                                    Priority = t2.priority
                                                }).ToList(),

                             //EmployeeAttBonus = db.HrmsEodRecords.Where(a => a.EmployeeId == a.ID && a.Status == true && a.Eod_RefFk == 7).ToList(),

                         }).OrderBy(a => a.CardNo).ToList();

            decimal perdaysalary = 0;
            decimal attendanceBonus = 0;
            decimal absentDeduction = 0;
            decimal attendanceTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            int count = 0;
            int index = 0;
            if (vData.Any())
            {
                count = 0;
                foreach (var v in vData)
                {
                    perdaysalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;
                    absentDeduction = 0;
                    dbh.GetAttendance(v.EmpID, getPayroll.FromDate, getPayroll.ToDate);
                    var va = dbh.VMAttendanceSummary;

                    ++index;
                    ds1.Rows.Add(
                        index,
                        v.CardTitle,
                        v.CardNo,
                        v.EmployeeName,
                        v.Designation,
                        v.Section,
                        v.JoinDate,
                        v.Grade,
                        va.MonthDays,
                        va.OffDays,
                        va.HoliDays,
                        va.TotalPresentDays,
                        va.AbsentDays,
                        va.CL,
                        va.EL,
                        va.ML,
                        va.WorkDays,
                        0,//Basic17
                        0,//House18
                        this.Medical,//Medical19
                        this.Food,//Food20
                        this.Transport,//Transport21
                        0,//GrossWages22
                        0,//AttendancePay23
                        0,//AdsentDeduction24
                        advanceTaka,//advance25
                        0,//TotalDeduction26
                        va.OTHour,//OTHours27
                        0,//OTRate28
                        0,//OTTaka29
                        0,//AttendanceBonus30
                        0,//TotalAmount31
                        this.StampDeduction,//StampDeduction32
                        0,//TotalPayable33
                        string.Empty
                        );

                    if (v.EmployeePayroll.Any())
                    {
                        if (v.EmployeePayroll.Any(a => a.RefId == 1))
                        {
                            perdaysalary = 0;
                            attendanceTaka = 0;
                            totalDeduction = 0;
                            totalOTTaka = 0;
                            totalAmount = 0;
                            absentDeduction = 0;
                            var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                            var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                            var gross = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 30);
                            var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);
                            var AttenBonus = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 7);

                            perdaysalary = BasicPay.Amount / this.DaysOfMonth;
                            absentDeduction = Math.Round(perdaysalary * va.AbsentDays);
                            attendanceTaka = gross.Amount - absentDeduction;
                            totalDeduction = absentDeduction + advanceTaka;
                            totalOTTaka = Math.Round(OTRate.Amount * va.OTHour);
                            totalAmount = attendanceTaka + totalOTTaka + attendanceBonus;

                            if (va.AbsentDays <= 0 && va.CL <= 0 && va.EL <= 0 && va.LateDay <= 2)
                            {
                                ds1.Rows[count][30] = AttenBonus.Amount;
                            }

                            ds1.Rows[count][17] = BasicPay.Amount;
                            ds1.Rows[count][18] = House.Amount;
                            ds1.Rows[count][22] = gross.Amount;
                            ds1.Rows[count][23] = attendanceTaka;
                            ds1.Rows[count][24] = absentDeduction;
                            ds1.Rows[count][25] = advanceTaka;
                            ds1.Rows[count][26] = totalDeduction;
                            ds1.Rows[count][28] = OTRate.Amount;
                            ds1.Rows[count][29] = totalOTTaka;
                            ds1.Rows[count][31] = totalAmount;
                            ds1.Rows[count][33] = totalAmount - stampDeduction;
                        }
                    }
                    ++count;
                }
            }

            return ds1;
        }

        public DataTable GetSalarySummarySheet1(int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            List<VMSalarySheet> lstSum = new List<VMSalarySheet>();

            var vData = (from t1 in db.HrmsPayrollInfos
                         join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                         join t4 in db.Sections on t3.Section_Id equals t4.ID
                         where t1.PayrollFK == PayrollId && t1.Status == true
                         group new { t1 } by new { t3, t4 } into g
                         select new VMSalarySheet
                         {
                             SectionId = g.Key.t3.Section_Id,
                             Section = g.Key.t4.SectionName,
                             BasicTotal = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 1) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 1).t1.Amount : 0,
                             GrossTotal = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 30) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 30).t1.Amount : 0,
                             TotalDeduction = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 35) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 35).t1.Amount : 0,
                             AttendanceTotal = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 36) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 36).t1.Amount : 0,
                             AttendanceBonus = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 7) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 7).t1.Amount : 0,
                             OTTotalHour = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 28) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 28).t1.Amount : 0,
                             OTTotalTaka = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 39) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 39).t1.Amount : 0,
                             StampTotal = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 12) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 12).t1.Amount : 0,
                             PayableTotal = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 40) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 40).t1.Amount : 0
                         }).ToList();

            var VSection = db.Sections.OrderBy(a => a.SectionName).ToList();
            if (VSection.Any())
            {

                foreach (var v in VSection)
                {
                    if (vData.Any(a => a.SectionId == v.ID))
                    {
                        VMSalarySheet m = new VMSalarySheet();
                        m.Section = v.SectionName;
                        m.Person = vData.Where(a => a.SectionId == v.ID).Count();
                        m.BasicTotal = vData.Where(a => a.SectionId == v.ID).Sum(a => a.BasicTotal);
                        m.GrossTotal = vData.Where(a => a.SectionId == v.ID).Sum(a => a.GrossTotal);
                        m.TotalDeduction = vData.Where(a => a.SectionId == v.ID).Sum(a => a.TotalDeduction);
                        m.AttendanceTotal = vData.Where(a => a.SectionId == v.ID).Sum(a => a.AttendanceTotal);
                        m.AttendanceBonus = vData.Where(a => a.SectionId == v.ID).Sum(a => a.AttendanceBonus);
                        m.OTTotalHour = vData.Where(a => a.SectionId == v.ID).Sum(a => a.OTTotalHour);
                        m.OTTotalTaka = vData.Where(a => a.SectionId == v.ID).Sum(a => a.OTTotalTaka);
                        m.StampTotal = vData.Where(a => a.SectionId == v.ID).Sum(a => a.StampTotal);
                        m.PayableTotal = vData.Where(a => a.SectionId == v.ID).Sum(a => a.PayableTotal);
                        lstSum.Add(m);
                    }
                }
            }
            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Person", typeof(string));
            ds.Columns.Add("Basic Salary", typeof(string));
            ds.Columns.Add("Gross Salary", typeof(string));
            ds.Columns.Add("Deduction", typeof(string));
            ds.Columns.Add("Attendance Pay", typeof(string));
            ds.Columns.Add("Attendance Bonus", typeof(string));
            ds.Columns.Add("OT Hour", typeof(string));
            ds.Columns.Add("OT Taka", typeof(string));
            ds.Columns.Add("Stamp", typeof(string));
            ds.Columns.Add("Payable Amount", typeof(string));

            ds.Rows.Clear();
            if (lstSum.Any())
            {
                int index = 0;
                foreach (var v in lstSum)
                {
                    ds.Rows.Add(++index, v.Section, v.Person, v.BasicTotal, v.GrossTotal, v.TotalDeduction, v.AttendanceTotal, v.AttendanceBonus, v.OTTotalHour, v.OTTotalTaka, v.StampTotal, v.PayableTotal);
                }
            }
            return ds;
        }

        public DataTable GetAllSalarySummarySheet(int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            List<VMSalarySheet> lstSalary = new List<VMSalarySheet>();
            List<VMSalarySheet> lstSum = new List<VMSalarySheet>();
            decimal GrossPay = 0;
            decimal perdayGrossSalary = 0;
            decimal perdayBasicSalary = 0;
            decimal attendanceBonus = 0;
            decimal attendanceTaka = 0;
            decimal beforeJoinAbsentDays = 0;
            decimal beforeJoinAbsentTaka = 0;
            decimal afterJoinAbsentTaka = 0;
            decimal totalAbsentTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            bool NewJoin = false;
            bool Resign = false;
            bool Lefty = false;
            bool Terminate = false;

            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();
            var allPayroll = db.HrmsPayrollInfos.Where(a=>a.PayrollFK==PayrollId).ToList();
            
            var vData = (from t1 in db.HrmsPayrollInfos
                         join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                         join t4 in db.Sections on t3.Section_Id equals t4.ID
                         where t1.PayrollFK == PayrollId && t1.Status == true
                         group new { t1 } by new { t3, t4 } into g
                         select new VMEmployeeModel
                         {
                             SectionId = g.Key.t3.Section_Id,
                             Section = g.Key.t4.SectionName,
                             EmpID= g.Key.t3.ID,
                             JoinDate= g.Key.t3.Joining_Date,
                             ResignDate=g.Key.t3.QuitDate,
                             LeftDate=g.Key.t3.QuitDate,
                             Status=g.Key.t3.Present_Status,
                             EmployeePayroll = (from t1 in db.HrmsPayrollInfos
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == g.Key.t3.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new EmployeePayroll
                                                {
                                                    RefId = (int)t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate
                                                }).ToList(),
                         }).ToList();
            #region ExecutionLoop
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    GrossPay = 0;
                    perdayGrossSalary = 0;
                    perdayBasicSalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    beforeJoinAbsentDays = 0;
                    beforeJoinAbsentTaka = 0;
                    afterJoinAbsentTaka = 0;
                    totalAbsentTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;
                    NewJoin = false;
                    Resign = false;
                    Terminate = false;
                    Lefty = false;

                    DBHelpers dbh = new DBHelpers();

                    if (getPayroll.FromDate > v.JoinDate && v.Status == 1)
                    {
                        //Regular Employee
                        dbh.GetAttendance(v.EmpID, getPayroll.FromDate, getPayroll.ToDate);
                    }
                    else if (getPayroll.FromDate <= v.JoinDate && getPayroll.ToDate >= v.JoinDate && v.Status == 1)
                    {
                        NewJoin = true;
                        dbh.GetAttendanceForNewJoin(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.JoinDate);
                    }
                    else if (getPayroll.FromDate < v.ResignDate && getPayroll.ToDate >= v.ResignDate && v.Status == 2)
                    {
                        //Resign
                        Resign = true;
                        dbh.GetAttendanceForLefty(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.ResignDate);
                    }
                    else if (getPayroll.FromDate < v.LeftDate && getPayroll.ToDate >= v.LeftDate && v.Status == 3)
                    {
                        //Lefty
                        Lefty = true;
                        Terminate = true;
                        dbh.GetAttendanceForLefty(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.LeftDate);
                    }
                    else if (getPayroll.FromDate < v.LeftDate && getPayroll.ToDate >= v.LeftDate && v.Status == 4)
                    {
                        //Terminate
                        Terminate = true;
                        Lefty = true;
                        dbh.GetAttendanceForLefty(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.LeftDate);
                    }
                        
                    var va = dbh.VMAttendanceSummary;
                    
                    VMSalarySheet m = new VMSalarySheet();
                    m.SectionId = v.SectionId;
                    m.Section = v.Section;
                    m.EmployeeId = v.EmpID;

                    if (v.EmployeePayroll.Any())
                    {
                        if (v.EmployeePayroll.Any(a => a.RefId == 1))
                        {
                            perdayGrossSalary = 0;
                            perdayBasicSalary = 0;
                            attendanceTaka = 0;
                            beforeJoinAbsentDays = 0;
                            beforeJoinAbsentTaka = 0;
                            afterJoinAbsentTaka = 0;
                            totalAbsentTaka = 0;
                            totalDeduction = 0;
                            totalOTTaka = 0;
                            totalAmount = 0;

                            var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                            var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                            var Other = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 3);
                            var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);
                            var AttendanceBonus = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 7);
                            GrossPay = BasicPay.Amount + House.Amount + Other.Amount;
                            perdayBasicSalary = BasicPay.Amount / this.DaysOfMonth;
                            totalOTTaka = OTRate.Amount * va.OTHour;

                            if (NewJoin)
                            {
                                //NewJoined
                                perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                beforeJoinAbsentDays = va.AbsentDays - va.JoinedAbsentDays;
                                beforeJoinAbsentTaka = perdayGrossSalary * beforeJoinAbsentDays;
                                afterJoinAbsentTaka = perdayBasicSalary * va.JoinedAbsentDays;
                                totalAbsentTaka = beforeJoinAbsentTaka + afterJoinAbsentTaka;
                            }
                            else if (Lefty && Terminate)
                            {
                                //Lefty
                                perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                totalAbsentTaka = perdayGrossSalary * va.AbsentDays;
                            }
                            else if (Resign)
                            {
                                //Resign
                                if (getPayroll.ToDate > v.ResignDate && getPayroll.FromDate <= v.ResignDate)
                                {
                                    perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                    totalAbsentTaka = perdayGrossSalary * va.AbsentDays;
                                }
                                else if (getPayroll.ToDate == v.ResignDate)
                                {
                                    perdayBasicSalary = BasicPay.Amount / this.DaysOfMonth;
                                    totalAbsentTaka = perdayBasicSalary * va.AbsentDays;
                                }
                            }
                            else
                            {
                                //Regular
                                totalAbsentTaka = perdayBasicSalary * va.AbsentDays;
                                if (va.AbsentDays == 0 && va.TotalLeaveDays == 0 && va.LateDay < 3)
                                {
                                    attendanceBonus = AttendanceBonus == null ? 0 : AttendanceBonus.Amount;
                                    m.AttendanceBonus = attendanceBonus;
                                }
                            }
                            
                            attendanceTaka = GrossPay - totalAbsentTaka;
                            totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                            totalAmount = attendanceTaka + totalOTTaka + attendanceBonus;

                            m.BasicTotal = BasicPay.Amount;
                            m.HouseTotal = House.Amount;
                            m.GrossTotal = GrossPay;
                            m.AttendanceTotal = attendanceTaka;
                            m.AbsentTotal = totalAbsentTaka;
                            m.TotalDeduction = totalDeduction;
                            m.OTTotalHour = va.OTHour;
                            m.OTTotalTaka = totalOTTaka;
                            m.SalaryTaka = totalAmount;
                            m.StampTotal = stampDeduction;
                            m.PayableTotal = totalAmount - stampDeduction;

                            lstSalary.Add(m);
                        }
                    }
                }
            }
            #endregion

            var VSection = db.Sections.OrderBy(a => a.SectionName).ToList();
            if (VSection.Any())
            {
                foreach (var v in VSection)
                {
                    if (lstSalary.Any(a => a.SectionId == v.ID))
                    {
                        VMSalarySheet m = new VMSalarySheet();
                        m.Section = v.SectionName;
                        m.Person = lstSalary.Where(a => a.SectionId == v.ID).Count();
                        m.BasicTotal = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.BasicTotal);
                        m.GrossTotal = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.GrossTotal);
                        m.TotalDeduction = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.TotalDeduction));
                        m.AttendanceTotal = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.AttendanceTotal));
                        m.AttendanceBonus = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.AttendanceBonus));
                        m.OTTotalHour = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.OTTotalHour);
                        m.OTTotalTaka = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.OTTotalTaka));
                        m.StampTotal = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.StampTotal);
                        m.PayableTotal = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.PayableTotal));
                        lstSum.Add(m);
                    }
                }
            }

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Person", typeof(string));
            ds.Columns.Add("Basic Salary", typeof(string));
            ds.Columns.Add("Gross Salary", typeof(string));
            ds.Columns.Add("Deduction", typeof(string));
            ds.Columns.Add("Attendance Pay", typeof(string));
            ds.Columns.Add("Attendance Bonus", typeof(string));
            ds.Columns.Add("OT Hour", typeof(string));
            ds.Columns.Add("OT Taka", typeof(string));
            ds.Columns.Add("Stamp", typeof(string));
            ds.Columns.Add("Payable Amount", typeof(string));

            ds.Rows.Clear();
            if (lstSum.Any())
            {
                int index = 0;
                foreach (var v in lstSum)
                {
                    ds.Rows.Add(++index, v.Section, v.Person, v.BasicTotal, v.GrossTotal, v.TotalDeduction, v.AttendanceTotal, v.AttendanceBonus, v.OTTotalHour, v.OTTotalTaka, v.StampTotal, v.PayableTotal);
                }

                ds.Rows.Add(++index, "Total", lstSum.Sum(a => a.Person), lstSum.Sum(a => a.BasicTotal), lstSum.Sum(a => a.GrossTotal), lstSum.Sum(a => a.TotalDeduction), lstSum.Sum(a => a.AttendanceTotal), lstSum.Sum(a => a.AttendanceBonus), lstSum.Sum(a => a.OTTotalHour), lstSum.Sum(a => a.OTTotalTaka), lstSum.Sum(a => a.StampTotal), lstSum.Sum(a => a.PayableTotal));
            }
            return ds;
        }

        public DataTable GetSalarySummarySheet(int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            List<VMSalarySheet> lstSalary = new List<VMSalarySheet>();
            List<VMSalarySheet> lstSum = new List<VMSalarySheet>();
            decimal GrossPay = 0;
            decimal perdayGrossSalary = 0;
            decimal perdayBasicSalary = 0;
            decimal attendanceBonus = 0;
            decimal attendanceTaka = 0;
            decimal beforeJoinAbsentDays = 0;
            decimal beforeJoinAbsentTaka = 0;
            decimal afterJoinAbsentTaka = 0;
            decimal totalAbsentTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            bool NewJoin = false;
            bool Resign = false;
            bool Regular = false;

            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();
            var allPayroll = db.HrmsPayrollInfos.Where(a => a.PayrollFK == PayrollId).ToList();

            var vData = (from t1 in db.HrmsPayrollInfos
                         join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                         join t4 in db.Sections on t3.Section_Id equals t4.ID
                         where t1.PayrollFK == PayrollId && t1.Status == true
                         group new { t1 } by new { t3, t4 } into g
                         select new VMEmployeeModel
                         {
                             SectionId = g.Key.t3.Section_Id,
                             Section = g.Key.t4.SectionName,
                             EmpID = g.Key.t3.ID,
                             JoinDate = g.Key.t3.Joining_Date,
                             ResignDate = g.Key.t3.QuitDate,
                             LeftDate = g.Key.t3.QuitDate,
                             Status = g.Key.t3.Present_Status,
                             EmployeePayroll = (from t1 in db.HrmsPayrollInfos
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == g.Key.t3.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new EmployeePayroll
                                                {
                                                    RefId = (int)t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate
                                                }).ToList(),
                         }).ToList();
            #region ExecutionLoop
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    GrossPay = 0;
                    perdayGrossSalary = 0;
                    perdayBasicSalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    beforeJoinAbsentDays = 0;
                    beforeJoinAbsentTaka = 0;
                    afterJoinAbsentTaka = 0;
                    totalAbsentTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;
                    Regular = false;
                    NewJoin = false;
                    Resign = false;

                    DBHelpers dbh = new DBHelpers();

                    if (getPayroll.FromDate > v.JoinDate && v.Status == 1)
                    {
                        //Regular Employee
                        Regular = true;
                        dbh.GetAttendance(v.EmpID, getPayroll.FromDate, getPayroll.ToDate);
                    }
                    else if (getPayroll.FromDate <= v.JoinDate && getPayroll.ToDate >= v.JoinDate && v.Status == 1)
                    {
                        NewJoin = true;
                        dbh.GetAttendanceForNewJoin(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.JoinDate);
                    }
                    else if (getPayroll.FromDate < v.ResignDate && getPayroll.ToDate >= v.ResignDate && v.Status == 2)
                    {
                        //Resign
                        Resign = true;
                        dbh.GetAttendanceForLefty(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.ResignDate);
                    }

                    if (Regular || NewJoin || Resign)
                    {
                        var va = dbh.VMAttendanceSummary;

                        VMSalarySheet m = new VMSalarySheet();
                        m.SectionId = v.SectionId;
                        m.Section = v.Section;
                        m.EmployeeId = v.EmpID;

                        if (v.EmployeePayroll.Any())
                        {
                            if (v.EmployeePayroll.Any(a => a.RefId == 1))
                            {
                                perdayGrossSalary = 0;
                                perdayBasicSalary = 0;
                                attendanceTaka = 0;
                                beforeJoinAbsentDays = 0;
                                beforeJoinAbsentTaka = 0;
                                afterJoinAbsentTaka = 0;
                                totalAbsentTaka = 0;
                                totalDeduction = 0;
                                totalOTTaka = 0;
                                totalAmount = 0;

                                var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                                var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                                var Other = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 3);
                                var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);
                                var AttendanceBonus = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 7);
                                GrossPay = BasicPay.Amount + House.Amount + Other.Amount;
                                perdayBasicSalary = BasicPay.Amount / this.DaysOfMonth;
                                totalOTTaka = OTRate.Amount * va.OTHour;

                                if (Regular)
                                {
                                    //Regular
                                    totalAbsentTaka = perdayBasicSalary * va.AbsentDays;
                                    if (va.AbsentDays == 0 && va.TotalLeaveDays == 0 && va.LateDay < 3)
                                    {
                                        attendanceBonus = AttendanceBonus == null ? 0 : AttendanceBonus.Amount;
                                        m.AttendanceBonus = attendanceBonus;
                                    }
                                }
                                else if (NewJoin)
                                {
                                    //NewJoined
                                    perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                    beforeJoinAbsentDays = va.AbsentDays - va.JoinedAbsentDays;
                                    beforeJoinAbsentTaka = perdayGrossSalary * beforeJoinAbsentDays;
                                    afterJoinAbsentTaka = perdayBasicSalary * va.JoinedAbsentDays;
                                    totalAbsentTaka = beforeJoinAbsentTaka + afterJoinAbsentTaka;
                                }
                                else if (Resign)
                                {
                                    //Resign
                                    if (getPayroll.ToDate > v.ResignDate && getPayroll.FromDate <= v.ResignDate)
                                    {
                                        perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                        totalAbsentTaka = perdayGrossSalary * va.AbsentDays;
                                    }
                                    else if (getPayroll.ToDate == v.ResignDate)
                                    {
                                        perdayBasicSalary = BasicPay.Amount / this.DaysOfMonth;
                                        totalAbsentTaka = perdayBasicSalary * va.AbsentDays;
                                    }
                                }

                                attendanceTaka = GrossPay - totalAbsentTaka;
                                totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                                totalAmount = attendanceTaka + totalOTTaka + attendanceBonus;

                                m.BasicTotal = BasicPay.Amount;
                                m.HouseTotal = House.Amount;
                                m.GrossTotal = GrossPay;
                                m.AttendanceTotal = attendanceTaka;
                                m.AbsentTotal = totalAbsentTaka;
                                m.TotalDeduction = totalDeduction;
                                m.OTTotalHour = va.OTHour;
                                m.OTTotalTaka = totalOTTaka;
                                m.SalaryTaka = totalAmount;
                                m.StampTotal = stampDeduction;
                                m.PayableTotal = totalAmount - stampDeduction;
                                lstSalary.Add(m);
                            }
                        }
                    }
                }
            }
            #endregion

            var VSection = db.Sections.OrderBy(a => a.SectionName).ToList();
            if (VSection.Any())
            {
                foreach (var v in VSection)
                {
                    if (lstSalary.Any(a => a.SectionId == v.ID))
                    {
                        VMSalarySheet m = new VMSalarySheet();
                        m.Section = v.SectionName;
                        m.Person = lstSalary.Where(a => a.SectionId == v.ID).Count();
                        m.BasicTotal = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.BasicTotal);
                        m.GrossTotal = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.GrossTotal);
                        m.TotalDeduction = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.TotalDeduction));
                        m.AttendanceTotal = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.AttendanceTotal));
                        m.AttendanceBonus = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.AttendanceBonus));
                        m.OTTotalHour = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.OTTotalHour);
                        m.OTTotalTaka = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.OTTotalTaka));
                        m.StampTotal = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.StampTotal);
                        m.PayableTotal = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.PayableTotal));
                        lstSum.Add(m);
                    }
                }
            }

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Person", typeof(string));
            ds.Columns.Add("Basic Salary", typeof(string));
            ds.Columns.Add("Gross Salary", typeof(string));
            ds.Columns.Add("Deduction", typeof(string));
            ds.Columns.Add("Attendance Pay", typeof(string));
            ds.Columns.Add("Attendance Bonus", typeof(string));
            ds.Columns.Add("OT Hour", typeof(string));
            ds.Columns.Add("OT Taka", typeof(string));
            ds.Columns.Add("Stamp", typeof(string));
            ds.Columns.Add("Payable Amount", typeof(string));

            ds.Rows.Clear();
            if (lstSum.Any())
            {
                int index = 0;
                foreach (var v in lstSum)
                {
                    ds.Rows.Add(++index, v.Section, v.Person, v.BasicTotal, v.GrossTotal, v.TotalDeduction, v.AttendanceTotal, v.AttendanceBonus, v.OTTotalHour, v.OTTotalTaka, v.StampTotal, v.PayableTotal);
                }

                ds.Rows.Add(++index, "Total", lstSum.Sum(a => a.Person), lstSum.Sum(a => a.BasicTotal), lstSum.Sum(a => a.GrossTotal), lstSum.Sum(a => a.TotalDeduction), lstSum.Sum(a => a.AttendanceTotal), lstSum.Sum(a => a.AttendanceBonus), lstSum.Sum(a => a.OTTotalHour), lstSum.Sum(a => a.OTTotalTaka), lstSum.Sum(a => a.StampTotal), lstSum.Sum(a => a.PayableTotal));
            }
            return ds;
        }

        public DataTable GetSalaryLeftySummarySheet(int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            List<VMSalarySheet> lstSalary = new List<VMSalarySheet>();
            List<VMSalarySheet> lstSum = new List<VMSalarySheet>();
            VMAttendanceSummary obj = new VMAttendanceSummary();
            decimal GrossPay = 0;
            decimal perdayGrossSalary = 0;
            decimal perdayBasicSalary = 0;
            decimal attendanceBonus = 0;
            decimal attendanceTaka = 0;
            decimal beforeJoinAbsentDays = 0;
            decimal beforeJoinAbsentTaka = 0;
            decimal afterJoinAbsentTaka = 0;
            decimal totalAbsentTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            bool NewJoin = false;
            bool Resign = false;
            bool Lefty = false;
            bool Terminate = false;

            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();
            var allPayroll = db.HrmsPayrollInfos.Where(a => a.PayrollFK == PayrollId).ToList();

            var vData = (from t1 in db.HrmsPayrollInfos
                         join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                         join t4 in db.Sections on t3.Section_Id equals t4.ID
                         where t1.PayrollFK == PayrollId && t1.Status == true && t3.Present_Status == 3 || t3.Present_Status == 4
                         group new { t1 } by new { t3, t4 } into g
                         select new VMEmployeeModel
                         {
                             SectionId = g.Key.t3.Section_Id,
                             Section = g.Key.t4.SectionName,
                             EmpID = g.Key.t3.ID,
                             JoinDate = g.Key.t3.Joining_Date,
                             ResignDate = g.Key.t3.QuitDate,
                             LeftDate = g.Key.t3.QuitDate,
                             Status = g.Key.t3.Present_Status,
                             EmployeePayroll = (from t1 in db.HrmsPayrollInfos
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == g.Key.t3.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new EmployeePayroll
                                                {
                                                    RefId = (int)t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate
                                                }).ToList(),
                         }).ToList();
            #region ExecutionLoop
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    GrossPay = 0;
                    perdayGrossSalary = 0;
                    perdayBasicSalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    beforeJoinAbsentDays = 0;
                    beforeJoinAbsentTaka = 0;
                    afterJoinAbsentTaka = 0;
                    totalAbsentTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;
                    Terminate = false;
                    Lefty = false;

                    DBHelpers dbh = new DBHelpers();
                    
                    if (getPayroll.FromDate < v.LeftDate && getPayroll.ToDate >= v.LeftDate && v.Status == 3)
                    {
                        //Lefty
                        Lefty = true;
                        Terminate = true;
                        dbh.GetAttendanceForLefty(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.LeftDate);
                    }
                    else if (getPayroll.FromDate < v.LeftDate && getPayroll.ToDate >= v.LeftDate && v.Status == 4)
                    {
                        //Terminate
                        Terminate = true;
                        Lefty = true;
                        dbh.GetAttendanceForLefty(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.LeftDate);
                    }
                    
                    var va = dbh.VMAttendanceSummary!=null? dbh.VMAttendanceSummary: obj;

                    VMSalarySheet m = new VMSalarySheet();
                    m.SectionId = v.SectionId;
                    m.Section = v.Section;
                    m.EmployeeId = v.EmpID;

                    if (v.EmployeePayroll.Any())
                    {
                        if (v.EmployeePayroll.Any(a => a.RefId == 1))
                        {
                            perdayGrossSalary = 0;
                            perdayBasicSalary = 0;
                            attendanceTaka = 0;
                            beforeJoinAbsentDays = 0;
                            beforeJoinAbsentTaka = 0;
                            afterJoinAbsentTaka = 0;
                            totalAbsentTaka = 0;
                            totalDeduction = 0;
                            totalOTTaka = 0;
                            totalAmount = 0;

                            var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                            var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                            var Other = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 3);
                            var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);
                            var AttendanceBonus = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 7);
                            GrossPay = BasicPay.Amount + House.Amount + Other.Amount;
                            perdayBasicSalary = BasicPay.Amount / this.DaysOfMonth;
                            totalOTTaka = OTRate.Amount * va.OTHour;
                            
                            if (Lefty && Terminate)
                            {
                                //Lefty
                                perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                totalAbsentTaka = perdayGrossSalary * va.AbsentDays;
                            }

                            attendanceTaka = GrossPay - totalAbsentTaka;
                            totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                            totalAmount = attendanceTaka + totalOTTaka + attendanceBonus;

                            m.BasicTotal = BasicPay.Amount;
                            m.HouseTotal = House.Amount;
                            m.GrossTotal = GrossPay;
                            m.AttendanceTotal = attendanceTaka;
                            m.AbsentTotal = totalAbsentTaka;
                            m.TotalDeduction = totalDeduction;
                            m.OTTotalHour = va.OTHour;
                            m.OTTotalTaka = totalOTTaka;
                            m.SalaryTaka = totalAmount;
                            m.StampTotal = stampDeduction;
                            m.PayableTotal = totalAmount - stampDeduction;

                            lstSalary.Add(m);
                        }
                    }
                }
            }
            #endregion

            var VSection = db.Sections.OrderBy(a => a.SectionName).ToList();
            if (VSection.Any())
            {
                foreach (var v in VSection)
                {
                    if (lstSalary.Any(a => a.SectionId == v.ID))
                    {
                        VMSalarySheet m = new VMSalarySheet();
                        m.Section = v.SectionName;
                        m.Person = lstSalary.Where(a => a.SectionId == v.ID).Count();
                        m.BasicTotal = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.BasicTotal);
                        m.GrossTotal = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.GrossTotal);
                        m.TotalDeduction = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.TotalDeduction));
                        m.AttendanceTotal = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.AttendanceTotal));
                        m.AttendanceBonus = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.AttendanceBonus));
                        m.OTTotalHour = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.OTTotalHour);
                        m.OTTotalTaka = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.OTTotalTaka));
                        m.StampTotal = lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.StampTotal);
                        m.PayableTotal = Math.Round(lstSalary.Where(a => a.SectionId == v.ID).Sum(a => a.PayableTotal));
                        lstSum.Add(m);
                    }
                }
            }

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Person", typeof(string));
            ds.Columns.Add("Basic Salary", typeof(string));
            ds.Columns.Add("Gross Salary", typeof(string));
            ds.Columns.Add("Deduction", typeof(string));
            ds.Columns.Add("Attendance Pay", typeof(string));
            ds.Columns.Add("Attendance Bonus", typeof(string));
            ds.Columns.Add("OT Hour", typeof(string));
            ds.Columns.Add("OT Taka", typeof(string));
            ds.Columns.Add("Stamp", typeof(string));
            ds.Columns.Add("Payable Amount", typeof(string));

            ds.Rows.Clear();
            if (lstSum.Any())
            {
                int index = 0;
                foreach (var v in lstSum)
                {
                    ds.Rows.Add(++index, v.Section, v.Person, v.BasicTotal, v.GrossTotal, v.TotalDeduction, v.AttendanceTotal, v.AttendanceBonus, v.OTTotalHour, v.OTTotalTaka, v.StampTotal, v.PayableTotal);
                }

                ds.Rows.Add(++index, "Total", lstSum.Sum(a=>a.Person), lstSum.Sum(a => a.BasicTotal), lstSum.Sum(a => a.GrossTotal), lstSum.Sum(a => a.TotalDeduction), lstSum.Sum(a => a.AttendanceTotal), lstSum.Sum(a => a.AttendanceBonus), lstSum.Sum(a => a.OTTotalHour), lstSum.Sum(a => a.OTTotalTaka), lstSum.Sum(a => a.StampTotal), lstSum.Sum(a => a.PayableTotal));

            }
            return ds;
        }
        
        public DataTable GetMonthlyOTSalary(int PayrollId, int SectionId)
        {
            HrmsContext db = new HrmsContext();
            //Active=1||Resign=2||Lefty=3||Terminate=4
            var vData = (from t1 in db.HrmsPayrolls
                         join t2 in db.HrmsPayrollInfos on t1.ID equals t2.PayrollFK
                         join a in db.Employees on t2.EmployeeIdFk equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         join p in db.HrmsEodReferences on t2.Eod_ReferenceFk equals p.ID
                         where t1.ID == PayrollId /*&& a.Present_Status==1 || a.Present_Status==2*/
                         && a.Section_Id == SectionId  
                         select new
                         {
                             PayrollId = t1.ID,
                             InfoID = t2.ID,
                             EmpID = a.ID,
                             name = a.Name,
                             CardNo = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Designation = b.Name,
                             Section = d.SectionName,
                             RefId = p.ID,
                             HeadName = p.Name,
                             Amount = t2.Amount,
                             IsAmountValue = p.IsHour,
                             Eod = p.Eod,
                             IsRegular = p.Regular,
                             Type = p.TypeBangla,
                             priority = p.priority,
                             FromDate = t1.FromDate,
                             Todate = t1.ToDate,
                             TypeStatus=a.Present_Status

                         }).OrderBy(a => a.CardNo).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.name, o.CardNo, o.CardTitle, o.Section, o.Designation, o.FromDate, o.Todate,o.TypeStatus } into all
                            select new
                            {
                                EmpID = all.Key.EmpID,
                                Name = all.Key.name,
                                CardNo = all.Key.CardNo,
                                CardTitle = all.Key.CardTitle,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                FromDate = all.Key.FromDate,
                                Todate = all.Key.Todate,
                                Status=all.Key.TypeStatus,

                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID && p.PayrollId == PayrollId
                                            select new
                                            {
                                                InfoID = p.InfoID,
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                IsAmountValue = p.IsAmountValue,
                                                Type = p.Type,
                                                priority = p.priority,
                                                IsDeductedEod = p.Eod,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList()
                            }).OrderBy(a => a.CardNo).ToList();
            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("BasicPay", typeof(string));
            ds.Columns.Add("TotalSalary", typeof(string));
            ds.Columns.Add("OT.Rate", typeof(string));
            ds.Columns.Add("Total Ext.OT", typeof(string));
            ds.Columns.Add("OT.Taka", typeof(string));
            ds.Columns.Add("Signature", typeof(string));
            ds.Columns.Add("Type", typeof(string));
            ds.Rows.Clear();
            int index = 0;
            if (getGroup.Any())
            {
                int count = 0;
                foreach (var v in getGroup)
                {
                    DBHelpers helperDB = new DBHelpers();
                    helperDB.GetAttendance(v.EmpID, v.FromDate, v.Todate);
                    var ot = helperDB.VMAttendanceSummary;
                    string status = string.Empty;
                    switch (v.Status)
                    {
                        case 1: status = "Active";break;
                        case 2: status = "Resign"; break;
                        case 3: status = "Lefty"; break;
                        case 4: status = "Terminate"; break;
                     
                    }
                    ++index;
                    ds.Rows.Add(
                        index,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.Designation,
                        v.Section,
                        decimal.Zero,//BasicPay6
                        decimal.Zero,//TotalSalary7
                        decimal.Zero,//OTRate8
                        decimal.Zero,//ExOTHours9
                        decimal.Zero,//OTValue10
                        string.Empty,
                        status
                        );
                    if (v.HeadItem.Any())
                    {
                        if (v.HeadItem.Any(a => a.RefId == 1))
                        {
                            var BasicPay = v.HeadItem.FirstOrDefault(a => a.RefId == 1);
                            var HousePay = v.HeadItem.FirstOrDefault(a => a.RefId == 2);
                            var OtherPay = v.HeadItem.FirstOrDefault(a => a.RefId == 3);
                            ds.Rows[count][6] = BasicPay.Amount;
                            var TotalSalary = v.HeadItem.FirstOrDefault(a => a.RefId == 30);
                            ds.Rows[count][7] = TotalSalary == null ? (BasicPay.Amount+HousePay.Amount+OtherPay.Amount) : TotalSalary.Amount;
                            var OTRate = v.HeadItem.FirstOrDefault(a => a.RefId == 11);
                            ds.Rows[count][8] = OTRate.Amount;
                            ds.Rows[count][9] = ot.ExtraOTHour;
                            ds.Rows[count][10] = Math.Round(OTRate.Amount * ot.ExtraOTHour);
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }


        public DataTable GetMonthlyOTSalaryForBSCI(int PayrollId, int SectionId)
        {
            HrmsContext db = new HrmsContext();
            //Active=1||Resign=2||Lefty=3||Terminate=4
            var vData = (from t1 in db.HrmsPayrolls
                         join t2 in db.HrmsPayrollInfos on t1.ID equals t2.PayrollFK
                         join a in db.Employees on t2.EmployeeIdFk equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         join p in db.HrmsEodReferences on t2.Eod_ReferenceFk equals p.ID
                         where t1.ID == PayrollId && a.Section_Id == SectionId

                         select new
                         {
                             PayrollId = t1.ID,
                             InfoID = t2.ID,
                             EmpID = a.ID,
                             name = a.Name,
                             CardNo = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Designation = b.Name,
                             Section = d.SectionName,
                             RefId = p.ID,
                             HeadName = p.Name,
                             Amount = t2.Amount,
                             IsAmountValue = p.IsHour,
                             Eod = p.Eod,
                             IsRegular = p.Regular,
                             Type = p.TypeBangla,
                             priority = p.priority,
                             FromDate = t1.FromDate,
                             Todate = t1.ToDate,
                             TypeStatus = a.Present_Status

                         }).OrderBy(a => a.CardNo).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.name, o.CardNo, o.CardTitle, o.Section,o.Designation, o.FromDate, o.Todate, o.TypeStatus } into all
                            select new
                            {
                                EmpID = all.Key.EmpID,
                                Name = all.Key.name,
                                CardNo = all.Key.CardNo,
                                CardTitle = all.Key.CardTitle,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                FromDate = all.Key.FromDate,
                                Todate = all.Key.Todate,
                                Status = all.Key.TypeStatus,

                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID && p.PayrollId == PayrollId
                                            select new
                                            {
                                                InfoID = p.InfoID,
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                IsAmountValue = p.IsAmountValue,
                                                Type = p.Type,
                                                priority = p.priority,
                                                IsDeductedEod = p.Eod,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList()
                            }).OrderBy(a => a.CardNo).ToList();
            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("BasicPay", typeof(string));
            ds.Columns.Add("TotalSalary", typeof(string));
            ds.Columns.Add("OT.Rate", typeof(string));
            ds.Columns.Add("Total Ext.OT", typeof(string));
            ds.Columns.Add("OT.Taka", typeof(string));
            ds.Columns.Add("Signature", typeof(string));
            ds.Columns.Add("Type", typeof(string));
            ds.Rows.Clear();
            int index = 0;
            if (getGroup.Any())
            {
                int count = 0;
                foreach (var v in getGroup)
                {
                    DBHelpers helperDB = new DBHelpers();
                    var BSCIData= helperDB.GetAuditRegularAttendanceForBSCI(v.EmpID, v.FromDate, v.Todate);
                    int TotalEOT = BSCIData.Sum(a => a.ExtraOTHour);
                 
                    string status = string.Empty;
                    switch (v.Status)
                    {
                        case 1: status = "Active"; break;
                        case 2: status = "Resign"; break;
                        case 3: status = "Lefty"; break;
                        case 4: status = "Terminate"; break;

                    }
                    ++index;
                    ds.Rows.Add(
                        index,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.Designation,
                        v.Section,
                        decimal.Zero,//BasicPay6
                        decimal.Zero,//TotalSalary7
                        decimal.Zero,//OTRate8
                        decimal.Zero,//ExOTHours9
                        decimal.Zero,//OTValue10
                        string.Empty,
                        status
                        );
                    if (v.HeadItem.Any())
                    {
                        if (v.HeadItem.Any(a => a.RefId == 1))
                        {
                            
                            var BasicPay = v.HeadItem.FirstOrDefault(a => a.RefId == 1);
                            var HousePay = v.HeadItem.FirstOrDefault(a => a.RefId == 2);
                            var OtherPay = v.HeadItem.FirstOrDefault(a => a.RefId == 3);
                            ds.Rows[count][6] = BasicPay.Amount;
                            var TotalSalary = v.HeadItem.FirstOrDefault(a => a.RefId == 30);
                            ds.Rows[count][7] = TotalSalary == null ? (BasicPay.Amount + HousePay.Amount + OtherPay.Amount) : TotalSalary.Amount;
                            var OTRate = v.HeadItem.FirstOrDefault(a => a.RefId == 11);
                            ds.Rows[count][8] = OTRate.Amount;
                            ds.Rows[count][9] = TotalEOT;
                            ds.Rows[count][10] = Math.Round(OTRate.Amount * TotalEOT);
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }

        public DataTable GetWorkerNightPayable(VM_Report model)
        {
            HrmsContext db = new HrmsContext();
            DBHelpers helperDB = new DBHelpers();
            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Half Night", typeof(string));
            ds.Columns.Add("H/N Rate", typeof(string));
            ds.Columns.Add("H/N Taka", typeof(string));
            ds.Columns.Add("Full Night", typeof(string));
            ds.Columns.Add("F/N Rate", typeof(string));
            ds.Columns.Add("F/N Taka", typeof(string));
            ds.Columns.Add("Total.N.HD", typeof(string));
            ds.Columns.Add("Total Taka", typeof(string));
            ds.Columns.Add("Signature", typeof(string));
            ds.Rows.Clear();

            if (model.SectionID == 20)
            {
                var vData = (from t1 in db.HrmsPayrollInfos
                             join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                             join t4 in db.Sections on t3.Section_Id equals t4.ID
                             join t5 in db.Designations on t3.Designation_Id equals t5.ID
                             where t1.PayrollFK == model.PayrollID && t1.Status == true && t3.Section_Id == model.SectionID && t3.Staff_Type.Equals("Worker")
                             group new { t1 } by new { t3, t4, t5 } into g
                             select new
                             {
                                 EmpID = g.Key.t3.ID,
                                 Name = g.Key.t3.Name,
                                 Type = g.Key.t3.Staff_Type,
                                 SectionId = g.Key.t3.Section_Id,
                                 Section = g.Key.t4.SectionName,
                                 CardNo = g.Key.t3.EmployeeIdentity,
                                 CardTitle = g.Key.t3.CardTitle,
                                 Designation = g.Key.t5.Name,
                                 HalfNightRate = 30,
                                 FullNightRate = 35
                             }).OrderBy(a => a.CardTitle).ThenBy(a => a.CardNo).ToList();
                
                int index = 0;
                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        helperDB.GetOTSummary(v.EmpID, model.Fromdate, model.Todate);

                        var ot = helperDB.VMAttendanceSummary;
                        ++index;
                        ds.Rows.Add(
                            index,
                            v.CardNo,
                            v.CardTitle,
                            v.Name,
                            v.Designation,
                            v.Section,
                            ot.TotalHalfNight,//Half Night6
                            v.HalfNightRate,//H/N Rate7
                            ot.TotalHalfNight * v.HalfNightRate,//Half Night Taka8
                            ot.TotalFullNight,//Full Night9
                            v.FullNightRate,//F/N Rate10
                            ot.TotalFullNight * v.FullNightRate,//Full Night Taka11
                            ot.TotalNight,//TotalNHD12
                            (ot.TotalHalfNight * v.HalfNightRate) + (ot.TotalFullNight * v.FullNightRate),//TotalTaka13
                            string.Empty
                        );
                    }
                }
            }
            else
            {
                var vData = (from t1 in db.HrmsPayrollInfos
                             join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                             join t4 in db.Sections on t3.Section_Id equals t4.ID
                             join t5 in db.Designations on t3.Designation_Id equals t5.ID
                             where t1.PayrollFK == model.PayrollID && t1.Status == true && t3.Section_Id == model.SectionID && t3.Staff_Type.Equals("Worker")
                             group new { t1 } by new { t3, t4, t5 } into g
                             select new
                             {
                                 EmpID = g.Key.t3.ID,
                                 Name = g.Key.t3.Name,
                                 Type = g.Key.t3.Staff_Type,
                                 SectionId = g.Key.t3.Section_Id,
                                 Section = g.Key.t4.SectionName,
                                 CardNo = g.Key.t3.EmployeeIdentity,
                                 CardTitle = g.Key.t3.CardTitle,
                                 Designation = g.Key.t5.Name,
                                 HalfNightRate = 30,
                                 FullNightRate = 40
                             }).OrderBy(a => a.CardTitle).ThenBy(a => a.CardNo).ToList();
                
                int index = 0;
                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        helperDB.GetOTSummary(v.EmpID, model.Fromdate, model.Todate);

                        var ot = helperDB.VMAttendanceSummary;
                        ++index;
                        ds.Rows.Add(
                            index,
                            v.CardNo,
                            v.CardTitle,
                            v.Name,
                            v.Designation,
                            v.Section,
                            ot.TotalHalfNight,//Half Night6
                            v.HalfNightRate,//H/N Rate7
                            ot.TotalHalfNight * v.HalfNightRate,//Half Night Taka8
                            ot.TotalFullNight,//Full Night9
                            v.FullNightRate,//F/N Rate10
                            ot.TotalFullNight * v.FullNightRate,//Full Night Taka11
                            ot.TotalNight,//TotalNHD12
                            (ot.TotalHalfNight * v.HalfNightRate) + (ot.TotalFullNight * v.FullNightRate),//TotalTaka13
                            string.Empty
                        );
                    }
                }
            }

            return ds;
        }

        /// <summary>
        /// Night Extra OT & Salary Payable Sheet
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public DataTable GetExtraOTNightSalary(VM_Report model)
        {
            HrmsContext db = new HrmsContext();
            DBHelpers helperDB = new DBHelpers();

            var EmployeePayroll = (from t1 in db.HrmsPayrollInfos
                                   join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                                   join t4 in db.Sections on t3.Section_Id equals t4.ID
                                   join t5 in db.Designations on t3.Designation_Id equals t5.ID
                                   where t1.PayrollFK == model.PayrollID && t1.Status == true && t3.Section_Id == model.SectionID
                                   group new { t1 } by new { t3, t4, t5 } into g
                                   select new
                                   {
                                       EmpID = g.Key.t3.ID,
                                       Name = g.Key.t3.Name,
                                       SectionId = g.Key.t3.Section_Id,
                                       Section = g.Key.t4.SectionName,
                                       EmpStatus = g.Key.t3.Staff_Type,
                                       CardNo = g.Key.t3.EmployeeIdentity,
                                       CardTitle = g.Key.t3.CardTitle,
                                       Designation = g.Key.t5.Name,

                                       OTRate = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 11) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 11).t1.Amount : 0,
                                       OTHourTaka = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 39) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 39).t1.Amount : 0,
                                       Stamp = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 12) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 12).t1.Amount : 0,
                                       PayableTotal = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 40) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 40).t1.Amount : 0
                                   }).ToList();

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Type", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Night/HD TK", typeof(string));
            ds.Columns.Add("Extra OT Hour", typeof(string));
            ds.Columns.Add("OT Rate", typeof(string));
            ds.Columns.Add("OT Taka", typeof(string));
            ds.Columns.Add("Salary", typeof(string));
            ds.Columns.Add("Total Payable", typeof(string));
            ds.Columns.Add("Signature", typeof(string));

            ds.Rows.Clear();

            decimal HalfNightTaka = 0;
            decimal FullNightTaka = 0;
            decimal NightTotalTaka = 0;
            decimal TotalOTTaka = 0;
            decimal TotalPayable = 0;
            decimal OTExtraHour = 0;

            //Onlyfor Worker NightRate
            decimal AllHalfNightRate = 30;
            decimal RFSHFullNightRate = 35;
            decimal OtherFullNightRate = 40;

            //Onlyfor Staff 
            decimal OffDayTotalTaka = 0;
            decimal HoliDayTotalTaka = 0;
            decimal TotalNightBill = 0;

            int index = 0;
            if (EmployeePayroll.Any())
            {
                foreach (var v in EmployeePayroll)
                {
                    HalfNightTaka = 0;
                    FullNightTaka = 0;
                    NightTotalTaka = 0;
                    TotalOTTaka = 0;
                    TotalPayable = 0;
                    OTExtraHour = 0;

                    OffDayTotalTaka = 0;
                    HoliDayTotalTaka = 0;
                    TotalNightBill = 0;

                    if (v.EmpStatus.Equals("Worker"))
                    {
                        helperDB.GetOTSummaryForWorker(v.EmpID, model.Fromdate, model.Todate);
                        var ot = helperDB.VMAttendanceSummary;
                        if (v.SectionId == 20)
                        {
                            FullNightTaka = ot.TotalFullNight * RFSHFullNightRate;
                        }
                        else
                        {
                            FullNightTaka = ot.TotalFullNight * OtherFullNightRate;
                        }
                        HalfNightTaka = ot.TotalHalfNight * AllHalfNightRate;
                        NightTotalTaka = HalfNightTaka + FullNightTaka;
                        OTExtraHour = ot.ExtraOTHour;
                        TotalOTTaka = v.OTRate * ot.ExtraOTHour;
                    }
                    else if (v.EmpStatus.Equals("Staff"))
                    {
                        var EODRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmpID && a.Status == true).ToList();
                        helperDB.GetAllOffDay(model.Fromdate, model.Todate, v.EmpID);

                        helperDB.GetOTSummaryForStaff(v.EmpID, model.Fromdate, model.Todate);
                        var ot = helperDB.VMAttendanceSummary;

                        VMNightAndHolyDay objmodel = new VMNightAndHolyDay();
                        if (EODRecord.Any(a => a.EmployeeId == v.EmpID && a.Eod_RefFk == 46))
                        {
                            var getRate = EODRecord.FirstOrDefault(a => a.EmployeeId == v.EmpID && a.Eod_RefFk == 46);
                            objmodel.NightRate = getRate.ActualAmount;
                        }
                        if (EODRecord.Any(a => a.EmployeeId == v.EmpID && a.Eod_RefFk == 47))
                        {
                            var getRate = EODRecord.FirstOrDefault(a => a.EmployeeId == v.EmpID && a.Eod_RefFk == 46);
                            objmodel.HolydayRate = getRate.ActualAmount;
                        }
                        OTExtraHour = ot.ExtraOTHour;
                        TotalNightBill = ot.TotalNight * objmodel.NightRate;
                        OffDayTotalTaka = ot.OffdayWorkDay * objmodel.HolydayRate;
                        HoliDayTotalTaka = ot.HolydayWorkDay * (objmodel.HolydayRate * 2);
                        NightTotalTaka = OffDayTotalTaka + HoliDayTotalTaka + TotalNightBill;
                    }

                    TotalPayable = NightTotalTaka + TotalOTTaka + v.PayableTotal;
                    ++index;
                    ds.Rows.Add(index, v.CardTitle, v.CardNo, v.Name, v.Designation, v.EmpStatus, v.Section, NightTotalTaka, OTExtraHour, v.OTRate, TotalOTTaka, v.PayableTotal, TotalPayable, string.Empty);
                }
            }

            return ds;
        }

        public DataTable GetExtraOTForLeftSalary(VM_Report model)
        {
            HrmsContext db = new HrmsContext();
            DBHelpers helperDB = new DBHelpers();
            
            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Night TotalTaka", typeof(string));
            ds.Columns.Add("Extra OT Hour", typeof(string));
            ds.Columns.Add("OT Rate", typeof(string));
            ds.Columns.Add("OT Taka", typeof(string));
            ds.Columns.Add("Salary", typeof(string));
            ds.Columns.Add("Total Payable", typeof(string));
            ds.Columns.Add("Signature", typeof(string));

            ds.Rows.Clear();

            decimal HalfNightTaka = 0;
            decimal FullNightTaka = 0;
            decimal NightTotalTaka = 0;
            decimal OTRate = 0;
            decimal TotalOTTaka = 0;
            decimal TotalSalary = 0;
            decimal TotalPayable = 0;
            int index = 0;

            if (model.SectionID > 0)
            {
                var vData = (from p in db.Employees
                             join q in db.Designations on p.Designation_Id equals q.ID
                             join r in db.Sections on p.Section_Id equals r.ID
                             //where p.Present_Status == 1 && p.Section_Id == model.SectionID
                             where p.Section_Id == model.SectionID && p.Present_Status != 1
                             && p.QuitDate > model.Fromdate && p.QuitDate <= model.Todate
                             select new
                             {
                                 EmpID = p.ID,
                                 Name = p.Name,
                                 EmpStatus = p.Staff_Type,
                                 CardNo = p.EmployeeIdentity,
                                 CardTitle = p.CardTitle,
                                 Section = r.SectionName,
                                 Designation = q.Name,
                                 HalfNightRate = 30,
                                 FullNightRate = 40,
                                 //Salary = db.HrmsEodRecords.Where(t1 => t1.EmployeeId == p.ID && t1.Status == true && t1.Eod_RefFk==1 || t1.Eod_RefFk==11).ToList(),
                                 paySalary = db.HrmsPayrollInfos.Where(o => o.Status == true && p.Status == true && o.EmployeeIdFk == p.ID && o.PayrollFK == model.PayrollID).ToList()
                             }).OrderBy(a => a.Section).ToList();

                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        HalfNightTaka = 0;
                        FullNightTaka = 0;
                        NightTotalTaka = 0;
                        OTRate = 0;
                        TotalOTTaka = 0;
                        TotalSalary = 0;
                        TotalPayable = 0;

                        helperDB.GetOTSummary(v.EmpID, model.Fromdate, model.Todate);
                        var ot = helperDB.VMAttendanceSummary;

                        if (v.paySalary.Any(a => a.Eod_ReferenceFk == 40))
                        {
                            TotalSalary = v.paySalary.FirstOrDefault(a => a.Eod_ReferenceFk == 40).Amount;
                        }

                        if (v.paySalary.Any(a => a.Eod_ReferenceFk == 11))
                        {
                            OTRate = v.paySalary.Where(a => a.Eod_ReferenceFk == 11).FirstOrDefault().Amount;
                        }

                        HalfNightTaka = ot.TotalHalfNight * v.HalfNightRate;
                        FullNightTaka = ot.TotalFullNight * v.FullNightRate;
                        NightTotalTaka = HalfNightTaka + FullNightTaka;

                        TotalOTTaka = Math.Round(OTRate * ot.ExtraOTHour);
                        TotalPayable = NightTotalTaka + TotalOTTaka + TotalSalary;

                        ++index;
                        ds.Rows.Add(index, v.CardTitle, v.CardNo, v.Name, v.Designation, v.Section, NightTotalTaka, ot.ExtraOTHour, OTRate, TotalOTTaka, TotalSalary, TotalPayable, string.Empty);
                    }
                }
            }
            else
            {
                var vData = (from p in db.Employees
                             join q in db.Designations on p.Designation_Id equals q.ID
                             join r in db.Sections on p.Section_Id equals r.ID
                             //where p.Present_Status == 1 && p.Section_Id == model.SectionID
                             where p.Present_Status == 3 || p.Present_Status == 4
                             && p.QuitDate > model.Fromdate && p.QuitDate <= model.Todate
                             select new
                             {
                                 EmpID = p.ID,
                                 Name = p.Name,
                                 EmpStatus = p.Staff_Type,
                                 CardNo = p.EmployeeIdentity,
                                 CardTitle = p.CardTitle,
                                 Section = r.SectionName,
                                 Designation = q.Name,
                                 HalfNightRate = 30,
                                 FullNightRate = 40,
                                 //Salary = db.HrmsEodRecords.Where(t1 => t1.EmployeeId == p.ID && t1.Status == true && t1.Eod_RefFk==1 || t1.Eod_RefFk==11).ToList(),
                                 paySalary = db.HrmsPayrollInfos.Where(o => o.Status == true && p.Status == true && o.EmployeeIdFk == p.ID && o.PayrollFK == model.PayrollID).ToList()
                             }).OrderBy(a => a.Section).ToList();

                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        HalfNightTaka = 0;
                        FullNightTaka = 0;
                        NightTotalTaka = 0;
                        OTRate = 0;
                        TotalOTTaka = 0;
                        TotalSalary = 0;
                        TotalPayable = 0;

                        helperDB.GetOTSummary(v.EmpID, model.Fromdate, model.Todate);
                        var ot = helperDB.VMAttendanceSummary;

                        if (v.paySalary.Any(a => a.Eod_ReferenceFk == 40))
                        {
                            TotalSalary = v.paySalary.FirstOrDefault(a => a.Eod_ReferenceFk == 40).Amount;
                        }

                        if (v.paySalary.Any(a => a.Eod_ReferenceFk == 11))
                        {
                            OTRate = v.paySalary.Where(a => a.Eod_ReferenceFk == 11).FirstOrDefault().Amount;
                        }

                        HalfNightTaka = ot.TotalHalfNight * v.HalfNightRate;
                        FullNightTaka = ot.TotalFullNight * v.FullNightRate;
                        NightTotalTaka = HalfNightTaka + FullNightTaka;

                        TotalOTTaka = Math.Round(OTRate * ot.ExtraOTHour);
                        TotalPayable = NightTotalTaka + TotalOTTaka + TotalSalary;

                        ++index;
                        ds.Rows.Add(index, v.CardTitle, v.CardNo, v.Name, v.Designation, v.Section, NightTotalTaka, ot.ExtraOTHour, OTRate, TotalOTTaka, TotalSalary, TotalPayable, string.Empty);
                    }
                }
            }
            return ds;
        }

        public DataTable GetSalaryIncrement(VM_Report model)
        {
            HrmsContext db = new HrmsContext();

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Joining Date", typeof(string));
            ds.Columns.Add("Previous Salary", typeof(string));
            ds.Columns.Add("Incremented Salary", typeof(string));
            ds.Columns.Add("Increment Amount", typeof(string));
            ds.Columns.Add("Remarks", typeof(string));

            ds.Rows.Clear();
            //decimal FoodHealthTrans = 1100;
            int index = 0;

            if (model.CardID != null && model.CardID > 1)
            {
                #region CardTitleWise
                DropDownData dr = new DropDownData();
                model.CardTitle = dr.GetCardTitle(model.CardID.Value);

                var getSalary = (from o in db.HrmsEodRecords
                                 join p in db.Employees on o.EmployeeId equals p.ID
                                 join q in db.Sections on p.Section_Id equals q.ID
                                 join r in db.Designations on p.Designation_Id equals r.ID
                                 join s in db.HrmsEodReferences on o.Eod_RefFk equals s.ID
                                 where
                                 s.ID == 1
                                 && o.Update_Date >= model.Fromdate
                                 && o.Update_Date <= model.Todate
                                 && p.CardTitle == model.CardTitle
                                 select new
                                 {
                                     EmpID = p.ID,
                                     Name = p.Name,
                                     CardNo = p.EmployeeIdentity,
                                     CardTitle = p.CardTitle,
                                     JoinDate = p.Joining_Date,
                                     Section = q.SectionName,
                                     Designation = r.Name,
                                     RecordID = o.ID,
                                     Salary = o.ActualAmount,
                                     status = o.Status
                                 }).ToList();

                var getGroup = (from o in getSalary
                                group o by new { o.EmpID, o.Name, o.CardNo, o.CardTitle, o.Section, o.Designation, o.JoinDate } into all
                                select new
                                {
                                    EmpID = all.Key.EmpID,
                                    Name = all.Key.Name,
                                    CardNo = all.Key.CardNo,
                                    CardTitle = all.Key.CardTitle,
                                    Section = all.Key.Section,
                                    Designation = all.Key.Designation,
                                    JoinDate = all.Key.JoinDate,
                                    HeadItem = (from p in all
                                                where p.EmpID == all.Key.EmpID
                                                select new
                                                {
                                                    RecordID = p.RecordID,
                                                    Salary = p.Salary,
                                                    Status = p.status
                                                }).ToList()

                                }).OrderBy(a => a.CardNo).ToList();


                if (getGroup.Any())
                {
                    int count = 0;
                    foreach (var v in getGroup)
                    {
                        if (v.HeadItem.Any(a => a.Status == false))
                        {
                            ++index;
                            ds.Rows.Add(
                                index,
                                v.CardNo,
                                v.CardTitle,
                                v.Name,
                                v.Section,
                                v.Designation,
                                v.JoinDate,
                                0,//PreviousSalary7
                                0,//IncrementSalary8
                                0,//Increment9
                                string.Empty
                                );
                            if (v.HeadItem.Any())
                            {
                                decimal incrementSalary = decimal.Zero;
                                decimal previousSalary = decimal.Zero;
                                decimal curhouse = decimal.Zero;
                                decimal prehouse = decimal.Zero;
                                if (v.HeadItem.Any(a => a.Status == true))
                                {
                                    var getCurrSalary = v.HeadItem.FirstOrDefault(a => a.Status == true);
                                    curhouse = (decimal)getCurrSalary.Salary * this.HouseRate;
                                    incrementSalary = getCurrSalary.Salary + Math.Round(curhouse, 0, MidpointRounding.AwayFromZero) + this.FoodHealthTrans;
                                }
                                if (v.HeadItem.Any(a => a.Status == false))
                                {
                                    var getPreSalary = v.HeadItem.Where(a => a.Status == false).OrderByDescending(a => a.RecordID).FirstOrDefault();
                                    prehouse = (decimal)getPreSalary.Salary * this.HouseRate;
                                    previousSalary = getPreSalary.Salary + Math.Round(prehouse, 0, MidpointRounding.AwayFromZero) + this.FoodHealthTrans;

                                }
                                ds.Rows[count][7] = previousSalary;
                                if (incrementSalary > 0)
                                {
                                    //ds.Rows[count][7] = previousSalary;
                                    ds.Rows[count][8] = incrementSalary;
                                    ds.Rows[count][9] = incrementSalary - previousSalary;
                                }
                                else
                                {
                                    //ds.Rows[count][7] = incrementSalary;
                                    ds.Rows[count][8] = previousSalary;
                                    ds.Rows[count][9] = incrementSalary;
                                }
                            }
                            ++count;
                        }
                    }


                }
                #endregion
            }
            else
            {
                #region OnlyDateRangeWise
                var getSalary = (from o in db.HrmsEodRecords
                                 join p in db.Employees on o.EmployeeId equals p.ID
                                 join q in db.Sections on p.Section_Id equals q.ID
                                 join r in db.Designations on p.Designation_Id equals r.ID
                                 join s in db.HrmsEodReferences on o.Eod_RefFk equals s.ID
                                 where
                                 s.ID == 1
                                 && o.Update_Date >= model.Fromdate
                                 && o.Entry_Date <= model.Todate
                                 select new
                                 {
                                     EmpID = p.ID,
                                     Name = p.Name,
                                     CardNo = p.EmployeeIdentity,
                                     CardTitle = p.CardTitle,
                                     JoinDate = p.Joining_Date,
                                     Section = q.SectionName,
                                     Designation = r.Name,
                                     RecordID = o.ID,
                                     Salary = o.ActualAmount,
                                     status = o.Status
                                 }).ToList();

                var getGroup = (from o in getSalary
                                group o by new { o.EmpID, o.Name, o.CardNo, o.CardTitle, o.Section, o.Designation, o.JoinDate } into all
                                select new
                                {
                                    EmpID = all.Key.EmpID,
                                    Name = all.Key.Name,
                                    CardNo = all.Key.CardNo,
                                    CardTitle = all.Key.CardTitle,
                                    Section = all.Key.Section,
                                    Designation = all.Key.Designation,
                                    JoinDate = all.Key.JoinDate,
                                    HeadItem = (from p in all
                                                where p.EmpID == all.Key.EmpID
                                                select new
                                                {
                                                    RecordID = p.RecordID,
                                                    Salary = p.Salary,
                                                    Status = p.status
                                                }).ToList()

                                }).OrderBy(a => a.CardNo).ToList();

                if (getGroup.Any())
                {
                    int count = 0;
                    foreach (var v in getGroup)
                    {
                        if (v.HeadItem.Any(a => a.Status == false))
                        {
                            ++index;
                            ds.Rows.Add(
                                index,
                                v.CardNo,
                                v.CardTitle,
                                v.Name,
                                v.Section,
                                v.Designation,
                                v.JoinDate,
                                0,//PreviousSalary7
                                0,//IncrementSalary8
                                0,//Increment9
                                string.Empty
                                );
                            if (v.HeadItem.Any())
                            {
                                decimal incrementSalary = decimal.Zero;
                                decimal previousSalary = decimal.Zero;
                                decimal curhouse = decimal.Zero;
                                decimal prehouse = decimal.Zero;
                                if (v.HeadItem.Any(a => a.Status == true))
                                {
                                    var getCurrSalary = v.HeadItem.FirstOrDefault(a => a.Status == true);
                                    curhouse = (decimal)getCurrSalary.Salary * this.HouseRate;
                                    incrementSalary = getCurrSalary.Salary + Math.Round(curhouse, 0, MidpointRounding.AwayFromZero) + this.FoodHealthTrans;
                                }
                                if (v.HeadItem.Any(a => a.Status == false))
                                {
                                    var getPreSalary = v.HeadItem.Where(a => a.Status == false).OrderByDescending(a => a.RecordID).FirstOrDefault();
                                    prehouse = (decimal)getPreSalary.Salary * this.HouseRate;
                                    previousSalary = getPreSalary.Salary + Math.Round(prehouse, 0, MidpointRounding.AwayFromZero) + this.FoodHealthTrans;

                                }
                                ds.Rows[count][7] = previousSalary;
                                if (incrementSalary > 0)
                                {
                                    //ds.Rows[count][7] = previousSalary;
                                    ds.Rows[count][8] = incrementSalary;
                                    ds.Rows[count][9] = incrementSalary - previousSalary;
                                }
                                else
                                {
                                    //ds.Rows[count][7] = incrementSalary;
                                    ds.Rows[count][8] = previousSalary;
                                    ds.Rows[count][9] = incrementSalary;
                                }
                            }
                            ++count;
                        }
                    }
                }
                #endregion
            }

            return ds;
        }

        /// <summary>
        /// Night and Holiday Bill allow only for Staff
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public DataTable GetStaffNightAndHDBill(VM_Report model)
        {
            HrmsContext db = new HrmsContext();
            DBHelpers dhelper = new DBHelpers();
            int HolidayWorkCounter = 0;
            int OffdayWorkCounter = 0;
            int index = 0;

            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Total Night", typeof(string));
            ds.Columns.Add("Night Rate", typeof(string));
            ds.Columns.Add("Night Taka", typeof(string));
            ds.Columns.Add("Total Offday", typeof(string));
            ds.Columns.Add("Offday Rate", typeof(string));
            ds.Columns.Add("Offday Taka", typeof(string));
            ds.Columns.Add("Total Holiday", typeof(string));
            ds.Columns.Add("Holiday Rate", typeof(string));
            ds.Columns.Add("Holiday Taka", typeof(string));
            ds.Columns.Add("Total Night&HD", typeof(string));
            ds.Columns.Add("Total Taka", typeof(string));
            ds.Columns.Add("Signature", typeof(string));
            ds.Rows.Clear();

            var getAttendance = db.HrmsAttendanceHistory.Where(a => a.Date >= model.Fromdate && a.Date <= model.Todate).ToList();

            var getAllRate = (db.HrmsEodRecords.Join(db.HrmsEodReferences, a => a.Eod_RefFk, b => b.ID, (a, b) => new { a, b })
                .Where(x => x.a.Status == true && x.b.Finish == 3)
                .Select(x => new { EmpId = x.a.EmployeeId, EODRefID = x.a.Eod_RefFk, Rate = x.a.ActualAmount })).ToList();

            if (model.SectionID > 0)
            {
                var vData = (from t1 in db.HrmsPayrollInfos
                             join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                             join t4 in db.Sections on t3.Section_Id equals t4.ID
                             join t5 in db.Designations on t3.Designation_Id equals t5.ID
                             where t1.PayrollFK == model.PayrollID && t1.Status == true && t3.Section_Id == model.SectionID && t3.Staff_Type.Equals("Staff")
                             group new { t1 } by new { t3, t4, t5 } into g
                             select new
                             {
                                 EmpID = g.Key.t3.ID,
                                 Name = g.Key.t3.Name,
                                 Type = g.Key.t3.Staff_Type,
                                 SectionId = g.Key.t3.Section_Id,
                                 Section = g.Key.t4.SectionName,
                                 CardNo = g.Key.t3.EmployeeIdentity,
                                 CardTitle = g.Key.t3.CardTitle,
                                 Designation = g.Key.t5.Name
                             }).OrderBy(a => a.CardTitle).ThenBy(a => a.CardNo).ToList();
                
                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        HolidayWorkCounter = 0;
                        OffdayWorkCounter = 0;

                        dhelper.GetAllOffDay(model.Fromdate, model.Todate, v.EmpID);

                        dhelper.GetOTSummaryForStaff(v.EmpID, model.Fromdate, model.Todate);
                        var ot = dhelper.VMAttendanceSummary;

                        VMNightAndHolyDay objmodel = new VMNightAndHolyDay();
                        if (getAllRate.Any(a => a.EmpId == v.EmpID))
                        {
                            var getRate = getAllRate.Where(a => a.EmpId == v.EmpID);
                            objmodel.NightRate = getRate.Any(a => a.EODRefID == 46) ? getRate.FirstOrDefault(a => a.EODRefID == 46).Rate : 0;
                            objmodel.HolydayRate = getRate.Any(a => a.EODRefID == 47) ? getRate.FirstOrDefault(a => a.EODRefID == 47).Rate : 0;
                        }

                        if (dhelper.HolidayList != null && dhelper.HolidayList.Any())
                        {
                            foreach (var present in dhelper.HolidayList)
                            {
                                objmodel.HolydayWorkDay = getAttendance.Any(a => a.Date == present.Date && a.EmployeeId == v.EmpID && a.PayableOverTime >= 7) ? ++HolidayWorkCounter : HolidayWorkCounter;
                            }
                        }

                        if (dhelper.WeekendDayList != null && dhelper.WeekendDayList.Any())
                        {
                            foreach (var present in dhelper.WeekendDayList)
                            {
                                objmodel.OffdayWorkDay = getAttendance.Any(a => a.Date == present.Date && a.EmployeeId == v.EmpID && a.PayableOverTime >= 7) ? ++OffdayWorkCounter : OffdayWorkCounter;
                            }
                        }

                        ++index;
                        ds.Rows.Add(
                            index,
                            v.CardTitle,
                            v.CardNo,
                            v.Name,
                            v.Designation,
                            v.Section,
                            ot.TotalNight,//Total Night6
                            objmodel.NightRate,//Night Bill Rate7
                            ot.TotalNight * objmodel.NightRate,//Night Taka8
                            objmodel.OffdayWorkDay,//Total OffDay9
                            objmodel.HolydayRate,
                            objmodel.OffdayWorkDay * objmodel.HolydayRate,
                            objmodel.HolydayWorkDay,//Total Holiday10
                            objmodel.HolydayRate * 2,//Holiday Rate11
                            objmodel.HolydayWorkDay * (objmodel.HolydayRate * 2),//Holiday Taka12
                            ot.TotalNight + objmodel.OffdayWorkDay + objmodel.HolydayWorkDay,//TotalNHD13
                            (ot.TotalNight * objmodel.NightRate) + (objmodel.OffdayWorkDay * objmodel.HolydayRate) + (objmodel.HolydayWorkDay * objmodel.HolydayRate * 2),//TotalTaka14
                            string.Empty
                            );
                    }
                }
            }

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SectionId"></param>
        /// <param name="PayrollId"></param>
        /// <returns></returns>
        public DsPayrollReport GetSalarySheet(int SectionId, int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            DsPayrollReport ds = new DsPayrollReport();
            ds.DtSalarySheet.Clear();
            ds.DtReportHeader.Clear();

            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();
            string pdate = getPayroll.FromDate.ToString("MMM");
            string ydate = getPayroll.FromDate.Year.ToString();

            ds.DtReportHeader.Rows.Add("Salary Sheet For The Month Of " + pdate + "-" + ydate, DateTime.Today);

            var getData = (from a in db.Employees
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         where a.Section_Id == SectionId && a.Present_Status == 1
                         //where a.ID == 3301
                         select new VMEmployeeModel
                         {
                             EmpID = a.ID,
                             EmployeeName = a.Name,
                             EmployeeBngName = a.Name_BN,
                             EmployeeIdentity=a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Grade = a.Grade,
                             Designation = b.Name,
                             Section = d.SectionName,
                             JoinDate = a.Joining_Date,
                             EmployeePayroll = (from t1 in db.HrmsPayrollInfos
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == a.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new EmployeePayroll
                                                {
                                                    RefId = (int)t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate
                                                }).ToList(),
                         }).ToList();

            if (getData.Any())
            {
                foreach (var v in getData)
                {
                    int ab = 0;
                    int.TryParse((string)v.EmployeeIdentity, out ab);
                    v.CardNo = ab;
                }
            }

            var vData= getData.OrderBy(a => a.CardNo);
            var vAdvance = db.HRMAdvance.Where(a => a.PaymentDate >= getPayroll.FromDate && a.PaymentDate <= getPayroll.ToDate).ToList();
            decimal GrossPay = 0;
            decimal perdayGrossSalary = 0;
            decimal perdayBasicSalary = 0;
            decimal attendanceBonus = 0;
            decimal attendanceTaka = 0;
            decimal beforeJoinAbsentDays = 0;
            decimal beforeJoinAbsentTaka = 0;
            decimal afterJoinAbsentTaka = 0;
            decimal totalAbsentTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            bool NewJoin = false;
            int count = 0;
            int index = 0;
            if (vData.Any())
            {
                DateTime d = vData.FirstOrDefault().EmployeePayroll.Any() == true ? vData.FirstOrDefault().EmployeePayroll.FirstOrDefault().PayDate : DateTime.Today;
                string paydate = d.ToLongDateString();
                count = 0;
                foreach (var v in vData)
                {
                    GrossPay = 0;
                    perdayGrossSalary = 0;
                    perdayBasicSalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    beforeJoinAbsentDays = 0;
                    beforeJoinAbsentTaka = 0;
                    afterJoinAbsentTaka = 0;
                    totalAbsentTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;
                    NewJoin = false;

                    DBHelpers dbh = new DBHelpers();
                    if (v.JoinDate > getPayroll.FromDate && v.JoinDate <= getPayroll.ToDate)
                    {
                        NewJoin = true;
                        dbh.GetAttendanceForNewJoin(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.JoinDate);
                    }
                    else
                    {
                        dbh.GetAttendance(v.EmpID, getPayroll.FromDate, getPayroll.ToDate);
                    }

                    var va = dbh.VMAttendanceSummary;

                    ++index;
                    ds.DtSalarySheet.Rows.Add(
                        index,
                        v.CardTitle,
                        v.CardNo,
                        v.EmployeeName + Environment.NewLine + v.EmployeeBngName,
                        v.Designation,
                        v.Section,
                        v.JoinDate,
                        v.Grade,
                        va.MonthDays,
                        va.OffDays,
                        va.HoliDays,
                        va.TotalPresentDays,
                        va.AbsentDays,
                        va.CL,
                        va.EL,
                        va.ML,
                        va.WorkDays,
                        0,//Basic17
                        0,//House18
                        this.Medical,//Medical19
                        this.Food,//Food20
                        this.Transport,//Transport21
                        0,//GrossWages22
                        0,//AttendancePay23
                        0,//AdsentDeduction24
                        advanceTaka,//advance25
                        0,//TotalDeduction26
                        va.OTHour,//OTHours27
                        0,//OTRate28
                        0,//OTTaka29
                        0,//AttendanceBonus30
                        0,//TotalAmount31
                        this.StampDeduction,//StampDeduction32
                        0,//TotalPayable33
                        paydate
                        );

                    if (v.EmployeePayroll.Any())
                    {
                        if (v.EmployeePayroll.Any(a => a.RefId == 1))
                        {
                            perdayGrossSalary = 0;
                            perdayBasicSalary = 0;
                            attendanceTaka = 0;
                            beforeJoinAbsentDays = 0;
                            beforeJoinAbsentTaka = 0;
                            afterJoinAbsentTaka = 0;
                            totalAbsentTaka = 0;
                            totalDeduction = 0;
                            totalOTTaka = 0;
                            totalAmount = 0;

                            var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                            var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                            var Other = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 3);
                            var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);
                            var AttendanceBonus = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 7);
                            GrossPay = BasicPay.Amount + House.Amount + Other.Amount;
                            perdayBasicSalary = BasicPay.Amount / this.DaysOfMonth;
                            totalOTTaka = OTRate.Amount * va.OTHour;

                            if (NewJoin)
                            {
                                perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                beforeJoinAbsentDays = va.AbsentDays - va.JoinedAbsentDays;
                                beforeJoinAbsentTaka = perdayGrossSalary * beforeJoinAbsentDays;
                                afterJoinAbsentTaka = perdayBasicSalary * va.JoinedAbsentDays;
                                totalAbsentTaka = beforeJoinAbsentTaka + afterJoinAbsentTaka;
                            }
                            else
                            {
                                totalAbsentTaka = perdayBasicSalary * va.AbsentDays;
                                if (va.AbsentDays == 0 && va.TotalLeaveDays == 0 && va.LateDay < 3)
                                {
                                    attendanceBonus = AttendanceBonus == null ? 0 : AttendanceBonus.Amount;
                                    ds.DtSalarySheet.Rows[count][30] = attendanceBonus;
                                }

                            }

                            if (vAdvance.Any(a=>a.EmployeeIdFK==v.EmpID))
                            {
                                advanceTaka = vAdvance.FirstOrDefault(a=>a.EmployeeIdFK== v.EmpID).Amount;
                            }
                            
                            attendanceTaka = GrossPay - totalAbsentTaka;
                            //totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                            totalDeduction = totalAbsentTaka + advanceTaka;
                            totalAmount = (attendanceTaka + totalOTTaka + attendanceBonus)- advanceTaka;

                            ds.DtSalarySheet.Rows[count][17] = BasicPay.Amount;
                            ds.DtSalarySheet.Rows[count][18] = House.Amount;
                            ds.DtSalarySheet.Rows[count][22] = GrossPay;
                            ds.DtSalarySheet.Rows[count][23] = attendanceTaka;
                            ds.DtSalarySheet.Rows[count][24] = totalAbsentTaka;
                            ds.DtSalarySheet.Rows[count][25] = advanceTaka;
                            ds.DtSalarySheet.Rows[count][26] = totalDeduction;
                            ds.DtSalarySheet.Rows[count][28] = OTRate.Amount;
                            ds.DtSalarySheet.Rows[count][29] = totalOTTaka;
                            ds.DtSalarySheet.Rows[count][31] = totalAmount;
                            ds.DtSalarySheet.Rows[count][33] = totalAmount - stampDeduction;
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SectionId"></param>
        /// <param name="PayrollId"></param>
        /// <returns></returns>
        public DsPayrollReport GetSalarySheetForLefty(int SectionId, int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            DsPayrollReport ds = new DsPayrollReport();
            ds.DtSalarySheet.Clear();
            ds.DtReportHeader.Clear();

            var getEmployeeAttBonus = (from o in db.HrmsEodRecords
                                       join p in db.Employees on o.EmployeeId equals p.ID
                                       where p.Section_Id == SectionId
                                       && p.Present_Status == 3 || p.Present_Status == 4 && o.Eod_RefFk == 7
                                       select new
                                       {
                                           EmpID = o.EmployeeId,
                                           ActualAmount = o.ActualAmount,
                                       }).ToList();
            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();
            string pdate = getPayroll.FromDate.ToString("MMM");
            string ydate = getPayroll.FromDate.Year.ToString();

            ds.DtReportHeader.Rows.Add("Salary Sheet For The Month Of " + pdate + "-" + ydate, DateTime.Today);

            var getData = (from a in db.Employees
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         where a.Section_Id == SectionId && a.QuitDate >= getPayroll.FromDate && a.QuitDate <= getPayroll.ToDate && a.Present_Status == 3 || a.Present_Status == 4
                         select new VMEmployeeModel
                         {
                             EmpID = a.ID,
                             EmployeeName = a.Name,
                             EmployeeBngName = a.Name_BN,
                             EmployeeIdentity = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Grade = a.Grade,
                             Designation = b.Name,
                             Section = d.SectionName,
                             JoinDate = a.Joining_Date,
                             LeftDate = a.QuitDate,
                             EmployeePayroll = (from t1 in db.HrmsPayrollInfos
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == a.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new EmployeePayroll
                                                {
                                                    RefId = (int)t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate
                                                }).ToList(),
                         }).ToList();
            if (getData.Any())
            {
                foreach (var v in getData)
                {
                    int ab = 0;
                    int.TryParse((string)v.EmployeeIdentity, out ab);
                    v.CardNo = ab;
                }
            }

            var vData = getData.OrderBy(a => a.CardNo);
            var vAdvance = db.HRMAdvance.Where(a => a.PaymentDate >= getPayroll.FromDate && a.PaymentDate <= getPayroll.ToDate).ToList();
            decimal GrossPay = 0;
            decimal perdayGrossSalary = 0;
            decimal attendanceBonus = 0;
            decimal attendanceTaka = 0;
            decimal totalAbsentTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            int count = 0;
            int index = 0;
            if (vData.Any())
            {
                DateTime d = vData.FirstOrDefault().EmployeePayroll.Any() == true ? vData.FirstOrDefault().EmployeePayroll.FirstOrDefault().PayDate : DateTime.Today;
                string paydate = d.ToLongDateString();
                count = 0;
                foreach (var v in vData)
                {
                    GrossPay = 0;
                    perdayGrossSalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    totalAbsentTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;

                    DBHelpers dbh = new DBHelpers();
                    dbh.GetAttendanceForLefty(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.LeftDate);

                    var va = dbh.VMAttendanceSummary;

                    ++index;
                    ds.DtSalarySheet.Rows.Add(
                        index,
                        v.CardTitle,
                        v.CardNo,
                        v.EmployeeName + Environment.NewLine + v.EmployeeBngName,
                        v.Designation,
                        v.Section,
                        v.JoinDate,
                        v.Grade,
                        va.MonthDays,
                        va.OffDays,
                        va.HoliDays,
                        va.TotalPresentDays,
                        va.AbsentDays,
                        va.CL,
                        va.EL,
                        va.ML,
                        va.WorkDays,
                        0,//Basic17
                        0,//House18
                        this.Medical,//Medical19
                        this.Food,//Food20
                        this.Transport,//Transport21
                        0,//GrossWages22
                        0,//AttendancePay23
                        0,//AdsentDeduction24
                        advanceTaka,//advance25
                        0,//TotalDeduction26
                        va.OTHour,//OTHours27
                        0,//OTRate28
                        0,//OTTaka29
                        0,//AttendanceBonus30
                        0,//TotalAmount31
                        this.StampDeduction,//StampDeduction32
                        0,//TotalPayable33
                        paydate
                        );

                    if (v.EmployeePayroll.Any())
                    {
                        if (v.EmployeePayroll.Any(a => a.RefId == 1))
                        {
                            GrossPay = 0;
                            perdayGrossSalary = 0;
                            attendanceTaka = 0;
                            totalAbsentTaka = 0;
                            totalDeduction = 0;
                            totalOTTaka = 0;
                            totalAmount = 0;

                            var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                            var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                            var Other = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 3);
                            var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);
                            GrossPay = BasicPay.Amount + House.Amount + Other.Amount;
                            perdayGrossSalary = GrossPay / this.DaysOfMonth;
                            totalOTTaka = OTRate.Amount * va.OTHour;

                            totalAbsentTaka = perdayGrossSalary * va.AbsentDays;

                            if (vAdvance.Any(a => a.EmployeeIdFK == v.EmpID))
                            {
                                advanceTaka = vAdvance.FirstOrDefault(a => a.EmployeeIdFK == v.EmpID).Amount;
                            }

                            attendanceTaka = GrossPay - totalAbsentTaka;
                            //totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                            totalDeduction = totalAbsentTaka + advanceTaka;
                            totalAmount = (attendanceTaka + totalOTTaka + attendanceBonus) - advanceTaka;

                            //attendanceTaka = GrossPay - totalAbsentTaka;
                            //totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                            //totalAmount = attendanceTaka + totalOTTaka + attendanceBonus;

                            ds.DtSalarySheet.Rows[count][17] = BasicPay.Amount;
                            ds.DtSalarySheet.Rows[count][18] = House.Amount;
                            ds.DtSalarySheet.Rows[count][22] = GrossPay;
                            ds.DtSalarySheet.Rows[count][23] = attendanceTaka;
                            ds.DtSalarySheet.Rows[count][24] = totalAbsentTaka;
                            ds.DtSalarySheet.Rows[count][25] = advanceTaka;
                            ds.DtSalarySheet.Rows[count][26] = totalDeduction;
                            ds.DtSalarySheet.Rows[count][28] = OTRate.Amount;
                            ds.DtSalarySheet.Rows[count][29] = totalOTTaka;
                            ds.DtSalarySheet.Rows[count][31] = totalAmount;
                            ds.DtSalarySheet.Rows[count][33] = totalAmount - stampDeduction;
                        }
                    }
                    ++count;

                }
            }

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SectionId"></param>
        /// <param name="PayrollId"></param>
        /// <returns></returns>
        public DsPayrollReport GetSalarySheetForResign(int SectionId, int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            DsPayrollReport ds = new DsPayrollReport();
            ds.DtSalarySheet.Clear();
            ds.DtReportHeader.Clear();

            var getEmployeeAttBonus = (from o in db.HrmsEodRecords
                                       join p in db.Employees on o.EmployeeId equals p.ID
                                       where p.Section_Id == SectionId && p.Present_Status == 2 && o.Eod_RefFk == 7
                                       select new
                                       {
                                           EmpID = o.EmployeeId,
                                           ActualAmount = o.ActualAmount,
                                       }).ToList();
            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();
            string pdate = getPayroll.FromDate.ToString("MMM");
            string ydate = getPayroll.FromDate.Year.ToString();

            ds.DtReportHeader.Rows.Add("Salary Sheet For The Month Of " + pdate + "-" + ydate, DateTime.Today);

            var getData = (from a in db.Employees
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         where a.Section_Id == SectionId && a.Present_Status == 2
                         && a.QuitDate > getPayroll.FromDate && a.QuitDate <= getPayroll.ToDate
                         //where a.ID == 1350
                         select new VMEmployeeModel
                         {
                             EmpID = a.ID,
                             EmployeeName = a.Name,
                             EmployeeBngName = a.Name_BN,
                             EmployeeIdentity = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Grade = a.Grade,
                             Designation = b.Name,
                             Section = d.SectionName,
                             JoinDate = a.Joining_Date,
                             ResignDate = a.QuitDate,
                             EmployeePayroll = (from t1 in db.HrmsPayrollInfos
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == a.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new EmployeePayroll
                                                {
                                                    RefId = (int)t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate
                                                }).ToList(),
                         }).ToList();

            if (getData.Any())
            {
                foreach (var v in getData)
                {
                    int ab = 0;
                    int.TryParse((string)v.EmployeeIdentity, out ab);
                    v.CardNo = ab;
                }
            }

            var vData = getData.OrderBy(a => a.CardNo);
            var vAdvance = db.HRMAdvance.Where(a => a.PaymentDate >= getPayroll.FromDate && a.PaymentDate <= getPayroll.ToDate).ToList();
            decimal GrossPay = 0;
            decimal perdayGrossSalary = 0;
            decimal perdayBasicSalary = 0;
            decimal attendanceBonus = 0;
            decimal attendanceTaka = 0;
            decimal totalAbsentTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            int count = 0;
            int index = 0;
            if (vData.Any())
            {
                DateTime d = vData.FirstOrDefault().EmployeePayroll.Any() == true ? vData.FirstOrDefault().EmployeePayroll.FirstOrDefault().PayDate : DateTime.Today;
                string paydate = d.ToLongDateString();
                count = 0;
                foreach (var v in vData)
                {
                    GrossPay = 0;
                    perdayGrossSalary = 0;
                    perdayBasicSalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    totalAbsentTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;

                    DBHelpers dbh = new DBHelpers();
                    dbh.GetAttendanceForLefty(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.ResignDate);

                    var va = dbh.VMAttendanceSummary;

                    ++index;
                    ds.DtSalarySheet.Rows.Add(
                        index,
                        v.CardTitle,
                        v.CardNo,
                        v.EmployeeName + Environment.NewLine + v.EmployeeBngName,
                        v.Designation,
                        v.Section,
                        v.JoinDate,
                        v.Grade,
                        va.MonthDays,
                        va.OffDays,
                        va.HoliDays,
                        va.TotalPresentDays,
                        va.AbsentDays,
                        va.CL,
                        va.EL,
                        va.ML,
                        va.WorkDays,
                        0,//Basic17
                        0,//House18
                        this.Medical,//Medical19
                        this.Food,//Food20
                        this.Transport,//Transport21
                        0,//GrossWages22
                        0,//AttendancePay23
                        0,//AdsentDeduction24
                        advanceTaka,//advance25
                        0,//TotalDeduction26
                        va.OTHour,//OTHours27
                        0,//OTRate28
                        0,//OTTaka29
                        0,//AttendanceBonus30
                        0,//TotalAmount31
                        this.StampDeduction,//StampDeduction32
                        0,//TotalPayable33
                        paydate
                        );

                    if (getPayroll.ToDate == v.ResignDate && va.AbsentDays == 0 && va.TotalLeaveDays == 0 && va.LateDay < 3)
                    {
                        attendanceBonus = getEmployeeAttBonus.Any(a => a.EmpID == v.EmpID) == true ? getEmployeeAttBonus.FirstOrDefault(a => a.EmpID == v.EmpID).ActualAmount : 0;
                        ds.DtSalarySheet.Rows[count][30] = attendanceBonus;
                    }

                    if (v.EmployeePayroll.Any())
                    {
                        if (v.EmployeePayroll.Any(a => a.RefId == 1))
                        {
                            GrossPay = 0;
                            perdayGrossSalary = 0;
                            perdayBasicSalary = 0;
                            attendanceTaka = 0;
                            totalAbsentTaka = 0;
                            totalDeduction = 0;
                            totalOTTaka = 0;
                            totalAmount = 0;

                            var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                            var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                            var Other = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 3);
                            var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);
                            GrossPay = BasicPay.Amount + House.Amount + Other.Amount;

                            if (getPayroll.ToDate > v.ResignDate && getPayroll.FromDate <= v.ResignDate)
                            {
                                perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                totalAbsentTaka = perdayGrossSalary * va.AbsentDays;
                            }
                            else if (getPayroll.ToDate == v.ResignDate)
                            {
                                perdayBasicSalary = BasicPay.Amount / this.DaysOfMonth;
                                totalAbsentTaka = perdayBasicSalary * va.AbsentDays;
                            }

                            totalOTTaka = OTRate.Amount * va.OTHour;
                            //attendanceTaka = GrossPay - totalAbsentTaka;
                            //totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                            //totalAmount = attendanceTaka + totalOTTaka + attendanceBonus;
                            if (vAdvance.Any(a => a.EmployeeIdFK == v.EmpID))
                            {
                                advanceTaka = vAdvance.FirstOrDefault(a => a.EmployeeIdFK == v.EmpID).Amount;
                            }

                            attendanceTaka = GrossPay - totalAbsentTaka;
                            totalDeduction = totalAbsentTaka + advanceTaka;
                            totalAmount = (attendanceTaka + totalOTTaka + attendanceBonus) - advanceTaka;

                            ds.DtSalarySheet.Rows[count][17] = BasicPay.Amount;
                            ds.DtSalarySheet.Rows[count][18] = House.Amount;
                            ds.DtSalarySheet.Rows[count][22] = GrossPay;
                            ds.DtSalarySheet.Rows[count][23] = attendanceTaka;
                            ds.DtSalarySheet.Rows[count][24] = totalAbsentTaka;
                            ds.DtSalarySheet.Rows[count][25] = advanceTaka;
                            ds.DtSalarySheet.Rows[count][26] = totalDeduction;
                            ds.DtSalarySheet.Rows[count][28] = OTRate.Amount;
                            ds.DtSalarySheet.Rows[count][29] = totalOTTaka;
                            ds.DtSalarySheet.Rows[count][31] = totalAmount;
                            ds.DtSalarySheet.Rows[count][33] = totalAmount - stampDeduction;
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }

        public DsPayrollReport GetAuditSalarySheet(int SectionId, int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            DsPayrollReport ds = new DsPayrollReport();
            ds.DtSalarySheet.Clear();
            ds.DtReportHeader.Clear();

            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();
            string pdate = getPayroll.FromDate.ToString("MMM");
            string ydate = getPayroll.FromDate.Year.ToString();

            ds.DtReportHeader.Rows.Add("Salary Sheet For The Month Of " + pdate + "-" + ydate, DateTime.Today);

            var getData = (from a in db.Employees
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         where a.Section_Id == SectionId && a.Present_Status == 1
                         //where a.ID == 3471
                         select new VMEmployeeModel
                         {
                             EmpID = a.ID,
                             EmployeeName = a.Name,
                             EmployeeBngName = a.Name_BN,
                             EmployeeIdentity = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Grade = a.Grade,
                             Designation = b.Name,
                             Section = d.SectionName,
                             JoinDate = a.Joining_Date,
                             EmployeePayroll = (from t1 in db.HRMHistoryPay
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == a.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new EmployeePayroll
                                                {
                                                    RefId = (int)t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate
                                                }).ToList(),
                         }).ToList();

            if (getData.Any())
            {
                foreach (var v in getData)
                {
                    int ab = 0;
                    int.TryParse((string)v.EmployeeIdentity, out ab);
                    v.CardNo = ab;
                }
            }

            var vData = getData.OrderBy(a => a.CardNo);

            decimal GrossPay = 0;
            decimal perdayGrossSalary = 0;
            decimal perdayBasicSalary = 0;
            decimal attendanceBonus = 0;
            decimal attendanceTaka = 0;
            decimal beforeJoinAbsentDays = 0;
            decimal beforeJoinAbsentTaka = 0;
            decimal afterJoinAbsentTaka = 0;
            decimal totalAbsentTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            bool NewJoin = false;
            int count = 0;
            int index = 0;
            if (vData.Any())
            {
                DateTime d = vData.FirstOrDefault().EmployeePayroll.Any() == true ? vData.FirstOrDefault().EmployeePayroll.FirstOrDefault().PayDate : DateTime.Today;
                string paydate = d.ToLongDateString();
                count = 0;
                foreach (var v in vData)
                {
                    GrossPay = 0;
                    perdayGrossSalary = 0;
                    perdayBasicSalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    beforeJoinAbsentDays = 0;
                    beforeJoinAbsentTaka = 0;
                    afterJoinAbsentTaka = 0;
                    totalAbsentTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;
                    NewJoin = false;

                    DBHelpers dbh = new DBHelpers();
                    if (v.JoinDate > getPayroll.FromDate && v.JoinDate <= getPayroll.ToDate)
                    {
                        NewJoin = true;
                        dbh.GetAuditNewJoinAttendance(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.JoinDate);
                    }
                    else
                    {
                        dbh.GetAuditRegularAttendance(v.EmpID, getPayroll.FromDate, getPayroll.ToDate);
                    }

                    var va = dbh.VMAttendanceSummary;

                    ++index;
                    ds.DtSalarySheet.Rows.Add(
                        index,
                        v.CardTitle,
                        v.CardNo,
                        v.EmployeeName + Environment.NewLine + v.EmployeeBngName,
                        v.Designation,
                        v.Section,
                        v.JoinDate,
                        v.Grade,
                        va.MonthDays,
                        va.OffDays,
                        va.HoliDays,
                        va.TotalPresentDays,
                        va.AbsentDays,
                        va.CL,
                        va.EL,
                        va.ML,
                        va.WorkDays,
                        0,//Basic17
                        0,//House18
                        this.Medical,//Medical19
                        this.Food,//Food20
                        this.Transport,//Transport21
                        0,//GrossWages22
                        0,//AttendancePay23
                        0,//AdsentDeduction24
                        advanceTaka,//advance25
                        0,//TotalDeduction26
                        va.OTHour,//OTHours27
                        0,//OTRate28
                        0,//OTTaka29
                        0,//AttendanceBonus30
                        0,//TotalAmount31
                        this.StampDeduction,//StampDeduction32
                        0,//TotalPayable33
                        paydate
                        );

                    if (v.EmployeePayroll.Any())
                    {
                        if (v.EmployeePayroll.Any(a => a.RefId == 1))
                        {
                            GrossPay = 0;
                            perdayGrossSalary = 0;
                            perdayBasicSalary = 0;
                            attendanceTaka = 0;
                            beforeJoinAbsentDays = 0;
                            beforeJoinAbsentTaka = 0;
                            afterJoinAbsentTaka = 0;
                            totalAbsentTaka = 0;
                            totalDeduction = 0;
                            totalOTTaka = 0;
                            totalAmount = 0;

                            var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                            var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                            var Other = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 3);
                            var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);
                            var AttendanceBonus = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 7);

                            GrossPay = BasicPay.Amount + House.Amount + Other.Amount;
                            perdayBasicSalary = BasicPay.Amount / this.DaysOfMonth;
                            totalOTTaka = OTRate.Amount * va.OTHour;

                            if (NewJoin)
                            {
                                perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                beforeJoinAbsentDays = va.AbsentDays - va.JoinedAbsentDays;
                                beforeJoinAbsentTaka = perdayGrossSalary * beforeJoinAbsentDays;
                                afterJoinAbsentTaka = perdayBasicSalary * va.JoinedAbsentDays;
                                totalAbsentTaka = beforeJoinAbsentTaka + afterJoinAbsentTaka;
                            }
                            else
                            {
                                totalAbsentTaka = perdayBasicSalary * va.AbsentDays;
                                if (va.AbsentDays == 0 && va.TotalLeaveDays == 0 && va.LateDay < 3)
                                {
                                    attendanceBonus = AttendanceBonus.Amount;
                                    ds.DtSalarySheet.Rows[count][30] = attendanceBonus;
                                }

                            }
                            attendanceTaka = GrossPay - totalAbsentTaka;
                            totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                            totalAmount = attendanceTaka + totalOTTaka + attendanceBonus;

                            ds.DtSalarySheet.Rows[count][17] = BasicPay.Amount;
                            ds.DtSalarySheet.Rows[count][18] = House.Amount;
                            ds.DtSalarySheet.Rows[count][22] = GrossPay;
                            ds.DtSalarySheet.Rows[count][23] = attendanceTaka;
                            ds.DtSalarySheet.Rows[count][24] = totalAbsentTaka;
                            ds.DtSalarySheet.Rows[count][25] = advanceTaka;
                            ds.DtSalarySheet.Rows[count][26] = totalDeduction;
                            ds.DtSalarySheet.Rows[count][28] = OTRate.Amount;
                            ds.DtSalarySheet.Rows[count][29] = totalOTTaka;
                            ds.DtSalarySheet.Rows[count][31] = totalAmount;
                            ds.DtSalarySheet.Rows[count][33] = totalAmount - stampDeduction;
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }

        public DsPayrollReport GetAuditSalarySheetForLefty(int SectionId, int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            DsPayrollReport ds = new DsPayrollReport();
            ds.DtSalarySheet.Clear();
            ds.DtReportHeader.Clear();

            var getEmployeeAttBonus = (from o in db.HRMHistoryEOD
                                       join p in db.Employees on o.EmployeeId equals p.ID
                                       where p.Section_Id == SectionId && o.Eod_RefFk == 7 && p.Present_Status == 3 || p.Present_Status == 4 
                                       select new
                                       {
                                           EmpID = o.EmployeeId,
                                           ActualAmount = o.ActualAmount,
                                       }).ToList();
            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();
            string pdate = getPayroll.FromDate.ToString("MMM");
            string ydate = getPayroll.FromDate.Year.ToString();

            ds.DtReportHeader.Rows.Add("Salary Sheet For The Month Of " + pdate + "-" + ydate, DateTime.Today);

            var getData = (from a in db.Employees
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         where a.Section_Id == SectionId && a.QuitDate >= getPayroll.FromDate && a.QuitDate <= getPayroll.ToDate && a.Present_Status == 3 || a.Present_Status == 4
                         select new VMEmployeeModel
                         {
                             EmpID = a.ID,
                             EmployeeName = a.Name,
                             EmployeeBngName = a.Name_BN,
                             EmployeeIdentity = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Grade = a.Grade,
                             Designation = b.Name,
                             Section = d.SectionName,
                             JoinDate = a.Joining_Date,
                             LeftDate = a.QuitDate,
                             EmployeePayroll = (from t1 in db.HRMHistoryPay
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == a.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new EmployeePayroll
                                                {
                                                    RefId = (int)t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate
                                                }).ToList(),
                         }).ToList();

            if (getData.Any())
            {
                foreach (var v in getData)
                {
                    int ab = 0;
                    int.TryParse((string)v.EmployeeIdentity, out ab);
                    v.CardNo = ab;
                }
            }

            var vData = getData.OrderBy(a => a.CardNo);

            decimal GrossPay = 0;
            decimal perdayGrossSalary = 0;
            decimal attendanceBonus = 0;
            decimal attendanceTaka = 0;
            decimal totalAbsentTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            int count = 0;
            int index = 0;
            if (vData.Any())
            {
                DateTime d = vData.FirstOrDefault().EmployeePayroll.Any() == true ? vData.FirstOrDefault().EmployeePayroll.FirstOrDefault().PayDate : DateTime.Today;
                string paydate = d.ToLongDateString();
                count = 0;
                foreach (var v in vData)
                {
                    GrossPay = 0;
                    perdayGrossSalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    totalAbsentTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;

                    DBHelpers dbh = new DBHelpers();
                    dbh.GetAuditLeftyAttendance(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.LeftDate);

                    var va = dbh.VMAttendanceSummary;

                    ++index;
                    ds.DtSalarySheet.Rows.Add(
                        index,
                        v.CardTitle,
                        v.CardNo,
                        v.EmployeeName + Environment.NewLine + v.EmployeeBngName,
                        v.Designation,
                        v.Section,
                        v.JoinDate,
                        v.Grade,
                        va.MonthDays,
                        va.OffDays,
                        va.HoliDays,
                        va.TotalPresentDays,
                        va.AbsentDays,
                        va.CL,
                        va.EL,
                        va.ML,
                        va.WorkDays,
                        0,//Basic17
                        0,//House18
                        this.Medical,//Medical19
                        this.Food,//Food20
                        this.Transport,//Transport21
                        0,//GrossWages22
                        0,//AttendancePay23
                        0,//AdsentDeduction24
                        advanceTaka,//advance25
                        0,//TotalDeduction26
                        va.OTHour,//OTHours27
                        0,//OTRate28
                        0,//OTTaka29
                        0,//AttendanceBonus30
                        0,//TotalAmount31
                        this.StampDeduction,//StampDeduction32
                        0,//TotalPayable33
                        paydate
                        );

                    if (v.EmployeePayroll.Any())
                    {
                        if (v.EmployeePayroll.Any(a => a.RefId == 1))
                        {
                            GrossPay = 0;
                            perdayGrossSalary = 0;
                            attendanceTaka = 0;
                            totalAbsentTaka = 0;
                            totalDeduction = 0;
                            totalOTTaka = 0;
                            totalAmount = 0;

                            var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                            var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                            var Other = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 3);
                            var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);

                            GrossPay = BasicPay.Amount + House.Amount + Other.Amount;
                            perdayGrossSalary = GrossPay / this.DaysOfMonth;
                            totalOTTaka = OTRate.Amount * va.OTHour;

                            totalAbsentTaka = perdayGrossSalary * va.AbsentDays;
                            attendanceTaka = GrossPay - totalAbsentTaka;
                            totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                            totalAmount = attendanceTaka + totalOTTaka + attendanceBonus;


                            ds.DtSalarySheet.Rows[count][17] = BasicPay.Amount;
                            ds.DtSalarySheet.Rows[count][18] = House.Amount;
                            ds.DtSalarySheet.Rows[count][22] = GrossPay;
                            ds.DtSalarySheet.Rows[count][23] = attendanceTaka;
                            ds.DtSalarySheet.Rows[count][24] = totalAbsentTaka;
                            ds.DtSalarySheet.Rows[count][25] = advanceTaka;
                            ds.DtSalarySheet.Rows[count][26] = totalDeduction;
                            ds.DtSalarySheet.Rows[count][28] = OTRate.Amount;
                            ds.DtSalarySheet.Rows[count][29] = totalOTTaka;
                            ds.DtSalarySheet.Rows[count][31] = totalAmount;
                            ds.DtSalarySheet.Rows[count][33] = totalAmount - stampDeduction;
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }

        public DsPayrollReport GetAuditSheetForResign(int SectionId, int PayrollId)
        {
            HrmsContext db = new HrmsContext();
            DsPayrollReport ds = new DsPayrollReport();
            ds.DtSalarySheet.Clear();
            ds.DtReportHeader.Clear();

            var getEmployeeAttBonus = (from o in db.HRMHistoryEOD
                                       join p in db.Employees on o.EmployeeId equals p.ID
                                       where p.Section_Id == SectionId
                                       && p.Present_Status == 2 && o.Status == true && o.Eod_RefFk == 7
                                       select new
                                       {
                                           EmpID = o.EmployeeId,
                                           ActualAmount = o.ActualAmount,
                                       }).ToList();
            var getPayroll = db.HrmsPayrolls.Where(a => a.ID == PayrollId).FirstOrDefault();
            string pdate = getPayroll.FromDate.ToString("MMM");
            string ydate = getPayroll.FromDate.Year.ToString();

            ds.DtReportHeader.Rows.Add("Salary Sheet For The Month Of " + pdate + "-" + ydate, DateTime.Today);

            var getData = (from a in db.Employees
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         where a.Section_Id == SectionId && a.Present_Status == 2
                         && a.QuitDate > getPayroll.FromDate && a.QuitDate <= getPayroll.ToDate
                         //where a.ID == 1350
                         select new VMEmployeeModel
                         {
                             EmpID = a.ID,
                             EmployeeName = a.Name,
                             EmployeeBngName = a.Name_BN,
                             EmployeeIdentity = a.EmployeeIdentity,
                             CardTitle = a.CardTitle,
                             Grade = a.Grade,
                             Designation = b.Name,
                             Section = d.SectionName,
                             JoinDate = a.Joining_Date,
                             ResignDate = a.QuitDate,
                             EmployeePayroll = (from t1 in db.HRMHistoryPay
                                                join t2 in db.HrmsEodReferences on t1.Eod_ReferenceFk equals t2.ID
                                                where t1.EmployeeIdFk == a.ID && t1.PayrollFK == PayrollId && t1.Status == true
                                                select new EmployeePayroll
                                                {
                                                    RefId = (int)t1.Eod_ReferenceFk,
                                                    Amount = t1.Amount,
                                                    PayDate = t1.PaymentDate
                                                }).ToList(),
                         }).ToList();
            if (getData.Any())
            {
                foreach (var v in getData)
                {
                    int ab = 0;
                    int.TryParse((string)v.EmployeeIdentity, out ab);
                    v.CardNo = ab;
                }
            }

            var vData = getData.OrderBy(a => a.CardNo);

            decimal GrossPay = 0;
            decimal perdayGrossSalary = 0;
            decimal perdayBasicSalary = 0;
            decimal attendanceBonus = 0;
            decimal attendanceTaka = 0;
            decimal totalAbsentTaka = 0;
            decimal totalDeduction = 0;
            decimal advanceTaka = 0;
            decimal totalOTTaka = 0;
            decimal totalAmount = 0;
            decimal stampDeduction = 10;
            int count = 0;
            int index = 0;
            if (vData.Any())
            {
                DateTime d = vData.FirstOrDefault().EmployeePayroll.Any() == true ? vData.FirstOrDefault().EmployeePayroll.FirstOrDefault().PayDate : DateTime.Today;
                string paydate = d.ToLongDateString();
                count = 0;
                foreach (var v in vData)
                {
                    GrossPay = 0;
                    perdayGrossSalary = 0;
                    perdayBasicSalary = 0;
                    attendanceBonus = 0;
                    attendanceTaka = 0;
                    totalAbsentTaka = 0;
                    totalDeduction = 0;
                    advanceTaka = 0;
                    totalOTTaka = 0;
                    totalAmount = 0;
                    stampDeduction = 10;

                    DBHelpers dbh = new DBHelpers();
                    dbh.GetAuditLeftyAttendance(v.EmpID, getPayroll.FromDate, getPayroll.ToDate, v.ResignDate);

                    var va = dbh.VMAttendanceSummary;

                    ++index;
                    ds.DtSalarySheet.Rows.Add(
                        index,
                        v.CardTitle,
                        v.CardNo,
                        v.EmployeeName + Environment.NewLine + v.EmployeeBngName,
                        v.Designation,
                        v.Section,
                        v.JoinDate,
                        v.Grade,
                        va.MonthDays,
                        va.OffDays,
                        va.HoliDays,
                        va.TotalPresentDays,
                        va.AbsentDays,
                        va.CL,
                        va.EL,
                        va.ML,
                        va.WorkDays,
                        0,//Basic17
                        0,//House18
                        this.Medical,//Medical19
                        this.Food,//Food20
                        this.Transport,//Transport21
                        0,//GrossWages22
                        0,//AttendancePay23
                        0,//AdsentDeduction24
                        advanceTaka,//advance25
                        0,//TotalDeduction26
                        va.OTHour,//OTHours27
                        0,//OTRate28
                        0,//OTTaka29
                        0,//AttendanceBonus30
                        0,//TotalAmount31
                        this.StampDeduction,//StampDeduction32
                        0,//TotalPayable33
                        paydate
                        );

                    if (getPayroll.ToDate == v.ResignDate && va.AbsentDays == 0 && va.TotalLeaveDays == 0 && va.LateDay < 3)
                    {
                        attendanceBonus = getEmployeeAttBonus.Any(a => a.EmpID == v.EmpID) == true ? getEmployeeAttBonus.FirstOrDefault(a => a.EmpID == v.EmpID).ActualAmount : 0;
                        ds.DtSalarySheet.Rows[count][30] = attendanceBonus;
                    }

                    if (v.EmployeePayroll.Any())
                    {
                        if (v.EmployeePayroll.Any(a => a.RefId == 1))
                        {
                            GrossPay = 0;
                            perdayGrossSalary = 0;
                            perdayBasicSalary = 0;
                            attendanceTaka = 0;
                            totalAbsentTaka = 0;
                            totalDeduction = 0;
                            totalOTTaka = 0;
                            totalAmount = 0;

                            var BasicPay = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 1);
                            var House = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 2);
                            var Other = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 3);
                            var OTRate = v.EmployeePayroll.FirstOrDefault(a => a.RefId == 11);

                            GrossPay = BasicPay.Amount + House.Amount + Other.Amount;
                            if (getPayroll.ToDate > v.ResignDate && getPayroll.FromDate <= v.ResignDate)
                            {
                                perdayGrossSalary = GrossPay / this.DaysOfMonth;
                                totalAbsentTaka = perdayGrossSalary * va.AbsentDays;
                            }
                            else if (getPayroll.ToDate == v.ResignDate)
                            {
                                perdayBasicSalary = BasicPay.Amount / this.DaysOfMonth;
                                totalAbsentTaka = perdayBasicSalary * va.AbsentDays;
                            }

                            totalOTTaka = OTRate.Amount * va.OTHour;
                            attendanceTaka = GrossPay - totalAbsentTaka;
                            totalDeduction = GrossPay - attendanceTaka - advanceTaka;
                            totalAmount = attendanceTaka + totalOTTaka + attendanceBonus;

                            ds.DtSalarySheet.Rows[count][17] = BasicPay.Amount;
                            ds.DtSalarySheet.Rows[count][18] = House.Amount;
                            ds.DtSalarySheet.Rows[count][22] = GrossPay;
                            ds.DtSalarySheet.Rows[count][23] = attendanceTaka;
                            ds.DtSalarySheet.Rows[count][24] = totalAbsentTaka;
                            ds.DtSalarySheet.Rows[count][25] = advanceTaka;
                            ds.DtSalarySheet.Rows[count][26] = totalDeduction;
                            ds.DtSalarySheet.Rows[count][28] = OTRate.Amount;
                            ds.DtSalarySheet.Rows[count][29] = totalOTTaka;
                            ds.DtSalarySheet.Rows[count][31] = totalAmount;
                            ds.DtSalarySheet.Rows[count][33] = totalAmount - stampDeduction;
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }


        #region SummaryReport

        public DataTable GetStaffNightAndHDBillSummery(VM_Report model)
        {
            HrmsContext db = new HrmsContext();
            List<EmployeeBenafitModel> lstSection = new List<EmployeeBenafitModel>();
            int index = 0;

            var VSection = db.Sections.OrderBy(a => a.SectionName).ToList();
            if (VSection.Any())
            {
                foreach (var v in VSection)
                {
                    model.SectionID = v.ID;
                    
                    var vData = GetSectionWiseStaffNightAndHDBill(model);
                    if (vData.Any(a => a.SectionId == v.ID))
                    {
                        EmployeeBenafitModel m = new EmployeeBenafitModel();
                        m.Section = v.SectionName;
                        m.TotalNight = vData.Where(a => a.SectionId == v.ID).Sum(a => a.TotalNight);
                        m.NightTaka = vData.Where(a => a.SectionId == v.ID).Sum(a => a.NightTaka);
                        m.TotalOffDay = vData.Where(a => a.SectionId == v.ID).Sum(a => a.TotalOffDay);
                        m.OffDayTaka = Math.Round(vData.Where(a => a.SectionId == v.ID).Sum(a => a.OffDayTaka));
                        m.TotalHoliday = vData.Where(a => a.SectionId == v.ID).Sum(a => a.TotalHoliday);
                        m.HolidayTaka = Math.Round(vData.Where(a => a.SectionId == v.ID).Sum(a => a.HolidayTaka));
                        m.TotalNighyHD = vData.Where(a => a.SectionId == v.ID).Sum(a => a.TotalNighyHD);
                        m.TotalTaka = Math.Round(vData.Where(a => a.SectionId == v.ID).Sum(a => a.TotalTaka));
                        lstSection.Add(m);
                    }
                }
            }
            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Total Night", typeof(string));
            ds.Columns.Add("Night Taka", typeof(string));
            ds.Columns.Add("Total Offday", typeof(string));
            ds.Columns.Add("Offday Taka", typeof(string));
            ds.Columns.Add("Total Holiday", typeof(string));
            ds.Columns.Add("Holiday Taka", typeof(string));
            ds.Columns.Add("Total Night&HD", typeof(string));
            ds.Columns.Add("Total Taka", typeof(string));
            ds.Columns.Add("Signature", typeof(string));
            ds.Rows.Clear();
            if (lstSection.Any())
            {
                index = 0;
                foreach (var v in lstSection)
                {
                    ds.Rows.Add(++index, v.Section, v.TotalNight, v.NightTaka, v.TotalOffDay, v.OffDayTaka, v.TotalHoliday, v.HolidayTaka, v.TotalNighyHD, v.TotalTaka, string.Empty);
                }
            }
            return ds;
        }

        public List<EmployeeBenafitModel> GetSectionWiseStaffNightAndHDBill(VM_Report model)
        {
            HrmsContext db = new HrmsContext();
            DBHelpers dhelper = new DBHelpers();
            int HolidayWorkCounter = 0;
            int OffdayWorkCounter = 0;
            List<EmployeeBenafitModel> lst = new List<EmployeeBenafitModel>();

            var getAttendance = db.HrmsAttendanceHistory.Where(a => a.Date >= model.Fromdate && a.Date <= model.Todate).ToList();

            var getAllRate = (db.HrmsEodRecords.Join(db.HrmsEodReferences, a => a.Eod_RefFk, b => b.ID, (a, b) => new { a, b })
                .Where(x => x.a.Status == true && x.b.Finish == 3)
                .Select(x => new { EmpId = x.a.EmployeeId, EODRefID = x.a.Eod_RefFk, Rate = x.a.ActualAmount })).ToList();

            if (model.SectionID > 0)
            {
                var vData = (from t1 in db.HrmsPayrollInfos
                             join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                             join t4 in db.Sections on t3.Section_Id equals t4.ID
                             join t5 in db.Designations on t3.Designation_Id equals t5.ID
                             where t1.PayrollFK == model.PayrollID && t1.Status == true && t3.Section_Id == model.SectionID && t3.Staff_Type.Equals("Staff")
                             group new { t1 } by new { t3, t4, t5 } into g
                             select new
                             {
                                 EmpID = g.Key.t3.ID,
                                 Name = g.Key.t3.Name,
                                 Type = g.Key.t3.Staff_Type,
                                 SectionId = g.Key.t3.Section_Id,
                                 Section = g.Key.t4.SectionName,
                                 CardNo = g.Key.t3.EmployeeIdentity,
                                 CardTitle = g.Key.t3.CardTitle,
                                 Designation = g.Key.t5.Name
                             }).OrderBy(a => a.CardTitle).ThenBy(a => a.CardNo).ToList();

                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        HolidayWorkCounter = 0;
                        OffdayWorkCounter = 0;

                        dhelper.GetAllOffDay(model.Fromdate, model.Todate, v.EmpID);

                        dhelper.GetOTSummaryForStaff(v.EmpID, model.Fromdate, model.Todate);
                        var ot = dhelper.VMAttendanceSummary;

                        VMNightAndHolyDay objmodel = new VMNightAndHolyDay();
                        if (getAllRate.Any(a => a.EmpId == v.EmpID))
                        {
                            var getRate = getAllRate.Where(a => a.EmpId == v.EmpID);
                            objmodel.NightRate = getRate.Any(a => a.EODRefID == 46) ? getRate.FirstOrDefault(a => a.EODRefID == 46).Rate : 0;
                            objmodel.HolydayRate = getRate.Any(a => a.EODRefID == 47) ? getRate.FirstOrDefault(a => a.EODRefID == 47).Rate : 0;
                        }

                        if (dhelper.HolidayList != null && dhelper.HolidayList.Any())
                        {
                            foreach (var present in dhelper.HolidayList)
                            {
                                objmodel.HolydayWorkDay = getAttendance.Any(a => a.Date == present.Date && a.EmployeeId == v.EmpID && a.PayableOverTime >= 7) ? ++HolidayWorkCounter : HolidayWorkCounter;
                            }
                        }

                        if (dhelper.WeekendDayList != null && dhelper.WeekendDayList.Any())
                        {
                            foreach (var present in dhelper.WeekendDayList)
                            {
                                objmodel.OffdayWorkDay = getAttendance.Any(a => a.Date == present.Date && a.EmployeeId == v.EmpID && a.PayableOverTime >= 7) ? ++OffdayWorkCounter : OffdayWorkCounter;
                            }
                        }

                        EmployeeBenafitModel mo = new EmployeeBenafitModel();
                        mo.SectionId = v.SectionId;
                        mo.Section = v.Section;
                        mo.TotalNight = ot.TotalNight;
                        mo.NightRate = objmodel.NightRate;
                        mo.NightTaka = ot.TotalNight * objmodel.NightRate;
                        mo.TotalOffDay = (int)objmodel.OffdayWorkDay;
                        mo.OffDayRate = objmodel.HolydayRate;
                        mo.OffDayTaka = objmodel.OffdayWorkDay * objmodel.HolydayRate;
                        mo.TotalHoliday = (int)objmodel.HolydayWorkDay;
                        mo.HolidayRate = objmodel.HolydayRate * 2;
                        mo.HolidayTaka = objmodel.HolydayWorkDay * (objmodel.HolydayRate * 2);
                        mo.TotalNighyHD = (int)(ot.TotalNight + objmodel.OffdayWorkDay + objmodel.HolydayWorkDay);
                        mo.TotalTaka = (int)(ot.TotalNight * objmodel.NightRate) + (objmodel.OffdayWorkDay * objmodel.HolydayRate) + (objmodel.HolydayWorkDay * objmodel.HolydayRate * 2);
                        lst.Add(mo);
                    }
                }
            }

            return lst;
        }
        
        public DataTable GetExtraOTNightSalarySummary(VM_Report model)
        {
            HrmsContext db = new HrmsContext();
            List<EmployeeBenafitModel> lstSection = new List<EmployeeBenafitModel>();
            int index = 0;

            var VSection = db.Sections.OrderBy(a => a.SectionName).ToList();
            if (VSection.Any())
            {
                foreach (var v in VSection)
                {
                    model.SectionID = v.ID;

                    var vData = GetSectionWiseExtraOTNightSalary(model);
                    if (vData.Any(a => a.SectionId == v.ID))
                    {
                        EmployeeBenafitModel m = new EmployeeBenafitModel();
                        m.Section = v.SectionName;
                        m.TotalNight = vData.Where(a => a.SectionId == v.ID).Sum(a => a.TotalNight);
                        m.NightTaka = vData.Where(a => a.SectionId == v.ID).Sum(a => a.NightTaka);
                        m.EOTHour = vData.Where(a => a.SectionId == v.ID).Sum(a => a.EOTHour);
                        m.TotalOTTaka = Math.Round(vData.Where(a => a.SectionId == v.ID).Sum(a => a.TotalOTTaka));
                        m.PayableSalary = vData.Where(a => a.SectionId == v.ID).Sum(a => a.PayableSalary);
                        m.TotalPayableTaka = Math.Round(vData.Where(a => a.SectionId == v.ID).Sum(a => a.TotalPayableTaka));
                        lstSection.Add(m);
                    }
                }
            }
            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(Int32));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Night/HD TK", typeof(string));
            ds.Columns.Add("Extra OT Hour", typeof(string));
            ds.Columns.Add("OT Taka", typeof(string));
            ds.Columns.Add("Salary", typeof(string));
            ds.Columns.Add("Total Payable", typeof(string));
            ds.Columns.Add("Signature", typeof(string));
            ds.Rows.Clear();

            if (lstSection.Any())
            {
                index = 0;
                foreach (var v in lstSection)
                {
                    ds.Rows.Add(++index, v.Section, v.NightTaka, v.EOTHour, v.TotalOTTaka, v.PayableSalary, v.TotalPayableTaka, string.Empty);
                }
            }
            return ds;
        }

        public List<EmployeeBenafitModel> GetSectionWiseExtraOTNightSalary(VM_Report model)
        {
            HrmsContext db = new HrmsContext();
            DBHelpers helperDB = new DBHelpers();
            List<EmployeeBenafitModel> lst = new List<EmployeeBenafitModel>();
            var EmployeePayroll = (from t1 in db.HrmsPayrollInfos
                                   join t3 in db.Employees on t1.EmployeeIdFk equals t3.ID
                                   join t4 in db.Sections on t3.Section_Id equals t4.ID
                                   join t5 in db.Designations on t3.Designation_Id equals t5.ID
                                   where t1.PayrollFK == model.PayrollID && t1.Status == true && t3.Section_Id == model.SectionID
                                   group new { t1 } by new { t3, t4, t5 } into g
                                   select new
                                   {
                                       EmpID = g.Key.t3.ID,
                                       Name = g.Key.t3.Name,
                                       SectionId = g.Key.t3.Section_Id,
                                       Section = g.Key.t4.SectionName,
                                       EmpStatus = g.Key.t3.Staff_Type,
                                       CardNo = g.Key.t3.EmployeeIdentity,
                                       CardTitle = g.Key.t3.CardTitle,
                                       Designation = g.Key.t5.Name,

                                       OTRate = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 11) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 11).t1.Amount : 0,
                                       OTHourTaka = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 39) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 39).t1.Amount : 0,
                                       Stamp = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 12) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 12).t1.Amount : 0,
                                       PayableTotal = g.Any(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 40) == true ? g.FirstOrDefault(a => a.t1.EmployeeIdFk == g.Key.t3.ID && a.t1.Eod_ReferenceFk == 40).t1.Amount : 0
                                   }).ToList();
            
            decimal HalfNightTaka = 0;
            decimal FullNightTaka = 0;
            decimal NightTotalTaka = 0;
            decimal TotalOTTaka = 0;
            decimal TotalPayable = 0;
            decimal OTExtraHour = 0;

            //Onlyfor Worker NightRate
            decimal AllHalfNightRate = 30;
            decimal RFSHFullNightRate = 35;
            decimal OtherFullNightRate = 40;

            //Onlyfor Staff 
            decimal OffDayTotalTaka = 0;
            decimal HoliDayTotalTaka = 0;
            decimal TotalNightBill = 0;
            
            if (EmployeePayroll.Any())
            {
                foreach (var v in EmployeePayroll)
                {
                    HalfNightTaka = 0;
                    FullNightTaka = 0;
                    NightTotalTaka = 0;
                    TotalOTTaka = 0;
                    TotalPayable = 0;
                    OTExtraHour = 0;

                    OffDayTotalTaka = 0;
                    HoliDayTotalTaka = 0;
                    TotalNightBill = 0;

                    if (v.EmpStatus.Equals("Worker"))
                    {
                        helperDB.GetOTSummaryForWorker(v.EmpID, model.Fromdate, model.Todate);
                        var ot = helperDB.VMAttendanceSummary;
                        if (v.SectionId == 20)
                        {
                            FullNightTaka = ot.TotalFullNight * RFSHFullNightRate;
                        }
                        else
                        {
                            FullNightTaka = ot.TotalFullNight * OtherFullNightRate;
                        }
                        HalfNightTaka = ot.TotalHalfNight * AllHalfNightRate;
                        NightTotalTaka = HalfNightTaka + FullNightTaka;
                        OTExtraHour = ot.ExtraOTHour;
                        TotalOTTaka = v.OTRate * ot.ExtraOTHour;
                    }
                    else if (v.EmpStatus.Equals("Staff"))
                    {
                        var EODRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmpID && a.Status == true).ToList();
                        helperDB.GetAllOffDay(model.Fromdate, model.Todate, v.EmpID);

                        helperDB.GetOTSummaryForStaff(v.EmpID, model.Fromdate, model.Todate);
                        var ot = helperDB.VMAttendanceSummary;

                        VMNightAndHolyDay objmodel = new VMNightAndHolyDay();
                        if (EODRecord.Any(a => a.EmployeeId == v.EmpID && a.Eod_RefFk == 46))
                        {
                            var getRate = EODRecord.FirstOrDefault(a => a.EmployeeId == v.EmpID && a.Eod_RefFk == 46);
                            objmodel.NightRate = getRate.ActualAmount;
                        }
                        if (EODRecord.Any(a => a.EmployeeId == v.EmpID && a.Eod_RefFk == 47))
                        {
                            var getRate = EODRecord.FirstOrDefault(a => a.EmployeeId == v.EmpID && a.Eod_RefFk == 46);
                            objmodel.HolydayRate = getRate.ActualAmount;
                        }
                        OTExtraHour = ot.ExtraOTHour;
                        TotalNightBill = ot.TotalNight * objmodel.NightRate;
                        OffDayTotalTaka = ot.OffdayWorkDay * objmodel.HolydayRate;
                        HoliDayTotalTaka = ot.HolydayWorkDay * (objmodel.HolydayRate * 2);
                        NightTotalTaka = OffDayTotalTaka + HoliDayTotalTaka + TotalNightBill;
                    }

                    TotalPayable = NightTotalTaka + TotalOTTaka + v.PayableTotal;
                    EmployeeBenafitModel mo = new EmployeeBenafitModel();
                    mo.SectionId = v.SectionId;
                    mo.Section = v.Section;
                    mo.NightTaka = NightTotalTaka;
                    mo.EOTHour = (int)OTExtraHour;
                    mo.TotalOTTaka = TotalOTTaka;
                    mo.PayableSalary = v.PayableTotal;
                    mo.TotalPayableTaka = TotalPayable;
                    lst.Add(mo);
                }
            }

            return lst;
        }
        #endregion


        
    }

    public class VMSalarySheet
    {
        public int SectionId { get; set; }

        public int EmployeeId { get; set; }
        
        public string Section { get; set; }

        public int Person { get; set; }

        public decimal BasicTotal { get; set; }

        public decimal HouseTotal { get; set; }

        public decimal GrossTotal { get; set; }

        public decimal AttendanceTotal { get; set; }

        public decimal AbsentTotal { get; set; }

        public decimal AttendanceBonus { get; set; }

        public decimal OTTotalHour { get; set; }

        public decimal OTTotalTaka { get; set; }

        public decimal StampTotal { get; set; }

        public decimal SalaryTaka { get; set; }

        public decimal TotalDeduction { get; set; }

        public decimal PayableTotal { get; set; }
    }

    public class VMEmployeeModel
    {
        public int EmpID { get; set; }
        public int SectionId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeBngName { get; set; }
        public string EmployeeIdentity { get; set; }
        public int CardNo { get; set; }
        public string CardTitle { get; set; }
        public string Grade { get; set; }
        public string Designation { get; set; }
        public string Section { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime ResignDate { get; set; }
        public DateTime LeftDate { get; set; }
        public int Status { get; set; }
        public List<EmployeePayroll> EmployeePayroll { get; set; }
    }

    public class EmployeePayroll
    {
        public int RefId { get; set; }
        public decimal Amount { get; set; }
        public DateTime PayDate { get; set; }
    }

    public class EmployeeBenafitModel
    {
        public int SectionId { get; set; }
        public string Section { get; set; }

        public int TotalNight { get; set; }

        public decimal NightRate { get; set; }

        public decimal NightTaka { get; set; }

        public int TotalOffDay { get; set; }

        public decimal OffDayRate { get; set; }

        public decimal OffDayTaka { get; set; }

        public int TotalHoliday { get; set; }

        public decimal HolidayRate { get; set; }

        public decimal HolidayTaka { get; set; }
        
        public int TotalNighyHD{ get; set; }

        public decimal TotalTaka { get; set; }

        public int EOTHour { get; set; }

        public decimal TotalOTTaka { get; set; }

        public decimal NightTotalTaka { get; set; }

        public decimal PayableSalary { get; set; }

        public decimal TotalPayableTaka { get; set; }
    }
}

