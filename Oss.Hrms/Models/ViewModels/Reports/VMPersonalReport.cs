﻿using Oss.Hrms.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Reports
{
    public class VMPersonalReport
    {
        private HrmsContext db = new HrmsContext();
        public List<VMPersonalReport> DataList { get; set; }

        public List<VMPersonalReport> TempDataList { get; set; }

        public int Counter { get; set; }
        [DisplayName("Employee Status"), Required(ErrorMessage = "Employee Status is required")]
        public int StatusId { get; set; }
        public int EmployeeId { get; set; }
        public int ReportId { get; set; }
        [DisplayName("Section Name")]
        public int? SectionId { get; set; }
        public int? DesignationId { get; set; }

        public bool IsChecked { get; set; }
        [Display(Name = "CardTitle")]
        public string CardTitle { get; set; }
        public string CardNo { get; set; }
        public string EmployeeName { get; set; }
        public string Section { get; set; }
        public string SectionBangla { get; set; }
        public string Designation { get; set; }
        public string DesignationBangla { get; set; }
        public string Grade { get; set; }
        public string Status { get; set; }
        public string BloodGroup { get; set; }
        public string PresentAddress { get; set; }
        public decimal JoiningSalary { get; set; }
        public string NationalID { get; set; }
        public string EmergencyPhone { get; set; }
        public string ImageName { get; set; }
        [Required(ErrorMessage = "Please select file.")]
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] files { get; set; }

        [DisplayName("From Date"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true), Required(ErrorMessage = "Fromdate date is required")]

        public DateTime Fromdate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("To Date"), Required(ErrorMessage = "Todate date is required")]
        public DateTime ToDate { get; set; }

        public DateTime JoinDate { get; set; }
       
        public DateTime ResignDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Card Issue Date"),Required(ErrorMessage = "Issue Date is required")]
        public DateTime CardIssueDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Card Expired Date"), Required(ErrorMessage = "Expired Date is required")]
        public DateTime CardExpiredDate { get; set; }

        public string ViewJoinDate { get; set; }
        public string ViewResignDate { get; set; }

        public void GetLefty(DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            int index = 0;

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 3 && o.QuitDate >= FromDate && o.QuitDate <= ToDate
                         select new VMPersonalReport
                         {
                             EmployeeId = o.ID,
                             Section = q.SectionName,
                             SectionId=q.ID,
                             CardNo = o.CardTitle +" "+ o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             EmployeeName = o.Name,
                             Designation = p.Name,
                             JoinDate = o.Joining_Date,
                             Grade=o.Grade,
                             ResignDate = o.QuitDate,
                             Status ="Lefty"
                         }).OrderBy(a => a.SectionId).ThenBy(a => a.CardNo).ThenBy(a => a.JoinDate).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    v.Counter = index;
                    v.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                    v.ViewResignDate = Helpers.ShortDateString(v.ResignDate);
                }
            }

            this.DataList = vData;
        }

        public void GetResign(DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            int index = 0;

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 2 && o.QuitDate>=FromDate && o.QuitDate<=ToDate
                         select new VMPersonalReport
                         {
                             EmployeeId = o.ID,
                             Section = q.SectionName,
                             SectionId = q.ID,
                             CardNo = o.CardTitle + " " + o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             EmployeeName = o.Name,
                             Designation = p.Name,
                             JoinDate = o.Joining_Date,
                             Grade = o.Grade,
                             ResignDate=o.QuitDate,
                             Status = "Resign"
                         }).OrderBy(a => a.SectionId).ThenBy(a => a.CardNo).ThenBy(a => a.JoinDate).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    v.Counter = index;
                    v.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                    v.ViewResignDate = Helpers.ShortDateString(v.ResignDate);
                }
            }

            this.DataList = vData;
        }

        public void GetTerminate(DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            int index = 0;

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 4 && o.QuitDate >= FromDate && o.QuitDate <= ToDate
                         select new VMPersonalReport
                         {
                             EmployeeId = o.ID,
                             Section = q.SectionName,
                             SectionId = q.ID,
                             CardNo = o.CardTitle + " " + o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             EmployeeName = o.Name,
                             Designation = p.Name,
                             JoinDate = o.Joining_Date,
                             Grade = o.Grade,
                             ResignDate = o.QuitDate,
                             Status = "Terminate"
                         }).OrderBy(a => a.SectionId).ThenBy(a => a.CardNo).ThenBy(a => a.JoinDate).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    v.Counter = index;
                    v.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                    v.ViewResignDate = Helpers.ShortDateString(v.ResignDate);
                }
            }

            this.DataList = vData;
        }

        public void GetJoin(DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            int index = 0;

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Joining_Date >= Fromdate && o.Joining_Date<=ToDate && o.Present_Status==1
                         select new VMPersonalReport
                         {
                             EmployeeId = o.ID,
                             Section = q.SectionName,
                             SectionId = q.ID,
                             CardNo = o.CardTitle + " " + o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             EmployeeName = o.Name,
                             Designation = p.Name,
                             JoinDate = o.Joining_Date,
                             Grade = o.Grade,
                             Status = "Join"
                         }).OrderBy(a => a.SectionId).ThenBy(a => a.CardNo).ThenBy(a => a.JoinDate).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    v.Counter = index;
                    v.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                }
            }

            this.DataList = vData;
        }
        
        public void GetEmployeeListForID(VMPersonalReport model)
        {

            //if (model.SectionId==null && model.DesignationId==null)
            //{
            //    var vData = (from o in db.Employees
            //                 join p in db.Sections on o.Section_Id equals p.ID
            //                 join q in db.Designations on o.Designation_Id equals q.ID
            //                 where o.Present_Status == 1
            //                 select new VMPersonalReport
            //                 {

            //                     EmployeeId = o.ID,
            //                     CardNo = o.EmployeeIdentity,
            //                     CardTitle = o.CardTitle,
            //                     EmployeeName = o.Name,
            //                     Section = p.SectionName,
            //                     SectionBangla = p.SectionNameBangla,
            //                     Designation = q.Name,
            //                     DesignationBangla = q.DesigNameBangla,
            //                     DesignationId = q.ID,
            //                     JoinDate = o.Joining_Date,
            //                     EmergencyPhone = o.Emergency_Contact_No,
            //                     BloodGroup = o.Blood_Group,
            //                     PresentAddress = o.Present_Address
            //                 }).OrderBy(a => a.CardNo).ToList();

            //    if (vData.Any())
            //    {
            //        this.DataList = vData;
            //    }
            //}
            if (model.SectionId > 0 && model.DesignationId > 0)
            {
                var vData = (from o in db.Employees
                             join p in db.Sections on o.Section_Id equals p.ID
                             join q in db.Designations on o.Designation_Id equals q.ID
                             where o.Section_Id==model.SectionId && o.Designation_Id==model.DesignationId && o.Present_Status == 1
                             select new VMPersonalReport
                             {

                                 EmployeeId = o.ID,
                                 CardNo = o.EmployeeIdentity,
                                 CardTitle = o.CardTitle,
                                 EmployeeName = o.Name,
                                 Section = p.SectionName,
                                 SectionBangla = p.SectionNameBangla,
                                 Designation = q.Name,
                                 DesignationBangla = q.DesigNameBangla,
                                 DesignationId = q.ID,
                                 JoinDate = o.Joining_Date,
                                 EmergencyPhone = o.Emergency_Contact_No,
                                 BloodGroup = o.Blood_Group,
                                 PresentAddress = o.Present_Address
                             }).OrderBy(a => a.CardNo).ToList();
                if (vData.Any())
                {
                    this.DataList = vData;
                }
            }
            else if (model.SectionId > 0 && model.DesignationId == null)
            {
                var vData = (from o in db.Employees
                             join p in db.Sections on o.Section_Id equals p.ID
                             join q in db.Designations on o.Designation_Id equals q.ID
                             where o.Section_Id == model.SectionId && o.Present_Status == 1
                             select new VMPersonalReport
                             {

                                 EmployeeId = o.ID,
                                 CardNo = o.EmployeeIdentity,
                                 CardTitle = o.CardTitle,
                                 EmployeeName = o.Name,
                                 Section = p.SectionName,
                                 SectionBangla = p.SectionNameBangla,
                                 Designation = q.Name,
                                 DesignationBangla = q.DesigNameBangla,
                                 DesignationId = q.ID,
                                 JoinDate = o.Joining_Date,
                                 EmergencyPhone = o.Emergency_Contact_No,
                                 BloodGroup = o.Blood_Group,
                                 PresentAddress = o.Present_Address
                             }).OrderBy(a => a.CardNo).ToList();
                if (vData.Any())
                {
                    this.DataList = vData;
                }
            }
            else if (model.SectionId == null && model.DesignationId > 0)
            {
                var vData = (from o in db.Employees
                             join p in db.Sections on o.Section_Id equals p.ID
                             join q in db.Designations on o.Designation_Id equals q.ID
                             where o.Designation_Id == model.DesignationId && o.Present_Status == 1
                             select new VMPersonalReport
                             {

                                 EmployeeId = o.ID,
                                 CardNo = o.EmployeeIdentity,
                                 CardTitle = o.CardTitle,
                                 EmployeeName = o.Name,
                                 Section = p.SectionName,
                                 SectionBangla = p.SectionNameBangla,
                                 Designation = q.Name,
                                 DesignationBangla = q.DesigNameBangla,
                                 DesignationId = q.ID,
                                 JoinDate = o.Joining_Date,
                                 EmergencyPhone = o.Emergency_Contact_No,
                                 BloodGroup = o.Blood_Group,
                                 PresentAddress = o.Present_Address
                             }).OrderBy(a => a.CardNo).ToList();
                if (vData.Any())
                {
                    this.DataList = vData;
                }
            }
        }

        public void GetAddEmployeeToCard(VMPersonalReport model)
        {
            if (model.TempDataList.Any(a=>a.IsChecked==true))
            {
                foreach (var v in model.TempDataList.Where(a=>a.IsChecked==true))
                {
                    model.DataList.Add(v);
                }
            }
        }
        
        public DsEmployeeReport GetGenerateIDCard(VMPersonalReport model)
        {
            DsEmployeeReport ds = new DsEmployeeReport();
            DsEmployeeReport ds1 = new DsEmployeeReport();
            ds1.DtEmployeeIDCard.Rows.Clear();
            
            if (model.DataList.Any(a=>a.IsChecked==true))
            {
                string imageName = string.Empty;
                string imagePath = "/Assets/Images/EmpImages/";
                
                foreach (var v in model.DataList.Where(a => a.IsChecked == true))
                {
                    imageName = string.Empty;
                    ds.DtEmployeeIDCard.Rows.Clear();
                    var vData = from o in db.Employees
                                join p in db.Sections on o.Section_Id equals p.ID
                                join q in db.Designations on o.Designation_Id equals q.ID
                                where o.ID == v.EmployeeId
                                select new
                                {
                                    EmpID=o.ID,
                                    CardNo=o.EmployeeIdentity,
                                    CardTitle=o.CardTitle,
                                    Designation=q.DesigNameBangla,
                                    Section=p.SectionNameBangla,
                                    Joindate=o.Joining_Date,
                                    NationalID=o.NID_No,
                                    EmergencyPhone=o.Emergency_Contact_No,
                                    BloodGroup=o.Blood_Group,
                                    PresentAddress=o.Present_Address,
                                    ImageName=o.Photo
                                };

                    if (vData.Any())
                    {
                        var takeSingle = vData.FirstOrDefault();
                        imageName = imagePath + takeSingle.ImageName;
                        ds.DtEmployeeIDCard.Rows.Add(
                            takeSingle.EmpID,
                            takeSingle.CardTitle + " " + takeSingle.CardNo,
                            takeSingle.CardTitle,
                            takeSingle.Designation,
                            takeSingle.Section,
                            DateTime.Today,
                            takeSingle.Joindate,
                            takeSingle.NationalID,
                            takeSingle.EmergencyPhone,
                            takeSingle.BloodGroup,
                            DateTime.Today.AddYears(1),
                            takeSingle.PresentAddress,
                            takeSingle.ImageName
                            );

                        if (model.Counter>1)
                        {
                            for (int take=1;take<=model.Counter;take++)
                            {
                                ds1.DtEmployeeIDCard.ImportRow(ds.DtEmployeeIDCard.Rows[0]);
                            }
                        }
                    }
                }
            }
            return ds1;
        }
        
        private DataTable GetLeftyReport(DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            DataTable table = new DataTable();

            table.Columns.Add("Serial", typeof(Int32));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Date Of Birth", typeof(string));
            table.Columns.Add("Joining Salary", typeof(string));


            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 3
                         select new
                         {
                             empId = o.ID,
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             JoinDate = Helpers.DateString(o.Joining_Date),
                             DateOfBirth = Helpers.DateString(o.DOB),
                             PhoneNo = o.Mobile_No
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ThenBy(a => a.JoinDate).ToList();

            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    decimal joinSalary = decimal.Zero;
                    string Qualification = string.Empty;
                    var getSalary = db.HrmsSalaries.Where(a => a.Employee_Id == v.empId && a.Status == true);
                    if (getSalary.Any())
                    {
                        joinSalary = getSalary.FirstOrDefault().Joining_Salary;
                    }

                    var getEdu = db.HrmsEducations.Where(a => a.Employee_Id == v.empId && a.Status == true);
                    if (getEdu.Any())
                    {
                        Qualification = getEdu.FirstOrDefault().Degree;
                    }

                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.JoinDate,
                        v.DateOfBirth,
                        joinSalary
                        );
                }
            }

            return table;
        }

        private DataTable GetResignReport(DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            DataTable table = new DataTable();

            table.Columns.Add("Serial", typeof(Int32));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Date Of Birth", typeof(string));
            table.Columns.Add("Joining Salary", typeof(string));
            table.Columns.Add("Resign Date", typeof(string));


            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 2
                         select new
                         {
                             empId = o.ID,
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             JoinDate = Helpers.DateString(o.Joining_Date),
                             DateOfBirth = Helpers.DateString(o.DOB),
                             PhoneNo = o.Mobile_No,
                             ResignDate= Helpers.DateString(o.QuitDate)
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ThenBy(a => a.JoinDate).ToList();

            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    decimal joinSalary = decimal.Zero;
                    string Qualification = string.Empty;
                    var getSalary = db.HrmsSalaries.Where(a => a.Employee_Id == v.empId && a.Status == true);
                    if (getSalary.Any())
                    {
                        joinSalary = getSalary.FirstOrDefault().Joining_Salary;
                    }

                    var getEdu = db.HrmsEducations.Where(a => a.Employee_Id == v.empId && a.Status == true);
                    if (getEdu.Any())
                    {
                        Qualification = getEdu.FirstOrDefault().Degree;
                    }

                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.JoinDate,
                        v.DateOfBirth,
                        joinSalary,
                        v.ResignDate
                        );
                }
            }

            return table;
        }

        private DataTable GetTerminateReport(DateTime FromDate, DateTime ToDate)
        {
            HrmsContext db = new HrmsContext();
            DataTable table = new DataTable();

            table.Columns.Add("Serial", typeof(Int32));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Date Of Birth", typeof(string));
            table.Columns.Add("Joining Salary", typeof(string));


            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 4
                         select new
                         {
                             empId = o.ID,
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             JoinDate = Helpers.DateString(o.Joining_Date),
                             DateOfBirth = Helpers.DateString(o.DOB),
                             PhoneNo = o.Mobile_No,
                             ResignDate = Helpers.DateString(o.QuitDate)
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ThenBy(a => a.JoinDate).ToList();

            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    decimal joinSalary = decimal.Zero;
                    string Qualification = string.Empty;
                    var getSalary = db.HrmsSalaries.Where(a => a.Employee_Id == v.empId && a.Status == true);
                    if (getSalary.Any())
                    {
                        joinSalary = getSalary.FirstOrDefault().Joining_Salary;
                    }

                    var getEdu = db.HrmsEducations.Where(a => a.Employee_Id == v.empId && a.Status == true);
                    if (getEdu.Any())
                    {
                        Qualification = getEdu.FirstOrDefault().Degree;
                    }

                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.JoinDate,
                        v.DateOfBirth,
                        joinSalary
                        );
                }
            }

            return table;
        }
    }
}