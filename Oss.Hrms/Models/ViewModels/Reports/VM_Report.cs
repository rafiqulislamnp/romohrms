﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.ViewModels.Attendance;
using Oss.Hrms.Helper;

namespace Oss.Hrms.Models.ViewModels.Reports
{
    public class VM_Report
    {

        private HrmsContext db = new HrmsContext();
        //public List<SelectListItem> ReportNameList { get; set; }
        public List<VMPersonalReport> DataList { get; set; }

        [Display(Name = "Select Report")]
        public string ReportName { get; set; }
        [DisplayName("Report")]
        public int ReportId { get; set; }
        public int SectionID { get; set; }
        public int? CardID { get; set; }
        public int Designation_Id { get; set; }
        public string Designation { get; set; }
        public int Posted { get; set; }
        public int Present { get; set; }
        public TimeSpan PresentNow { get; set; }
        public int Late { get; set; }
        public int Absent { get; set; }
        public int Leave { get; set; }
       // public decimal Total_Leave { get; set; }
        public int Holiday { get; set; }
        public int Offday { get; set; }
        public string Remarks { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("From Date")]
        public DateTime Fromdate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("To Date")]
        public DateTime Todate { get; set; }
        public int EmployeeId { get; set; }
        public int EmployeeId1 { get; set; }
        public string Employee_ID { get; set; }
        public string CardTitle { get; set; }
        public string Name { get; set; }
        public string Designation_Name { get; set; }
        public string UnitName { get; set; }
        public string Line { get; set; }
        public string SectionName { get; set; }
        public string Staff_Type { get; set; }
        public string Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}")]
        public TimeSpan InTime { get; set; }
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}")]
        public TimeSpan OutTime { get; set; }
        public string TotalDuration { get; set; }
        public string Status { get; set; }
        public int TotalOT { get; set; }
        public int BOT { get; set; }
        public int EOT { get; set; }
        public int OT { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string Joining_Date { get; set; }
        public string Leave_Name { get; set; }
        public string Leave_From { get; set; }
        public string Leave_To { get; set; }
        public decimal Approved_Days { get; set; }
        public decimal Total_Leave { get; set; }
        public decimal PreviousLeaveTaken { get; set; }
        public decimal Total_Approved_Leave { get; set; }
        public decimal Leave_Balance { get; set; }
        public string CardNo { get; set; }
        public string DrawnSalary { get; set; }
        public int? EmployeeStatus { get; set; }
        // public string TotalDuration { get; set; }

        public string Degree { get; set; }
        public int DateDiff { get; set; }


        public int PayrollID { get; set; }
        public int Total_Month { get; set; }
        public int Working_Days { get; set; }
        public int F_Leave { get; set; }
        public int TotalDeductionDays { get; set; }
        public int TotalWorkingDays { get; set; }
        public decimal TotalEarnLeave { get; set; }
        public int EnjoyableLeave { get; set; }
        public decimal PayableEarnLeave { get; set; }
        public int Stamp { get; set; }
        public decimal Present_Salary { get; set; }
        public decimal OneDaySalary { get; set; }
        public decimal PayableEarnLeaveTK { get; set; }
        public string Signature { get; set; }
        public string Unit { get; set; }
        public string Name_CardNo { get; set; }
        public string Designation_JoinDate { get; set; }
        public string One { get; set; }
        public string Two { get; set; }
        public string Three { get; set; }
        public string Four { get; set; }
        public string Five { get; set; }
        public string Six { get; set; }
        public string Seven { get; set; }
        public string Eight { get; set; }
        public string Nine { get; set; }
        public string Ten { get; set; }
        public string Eleven { get; set; }
        public string Twelve { get; set; }
        public string Thirteen { get; set; }
        public string Forteen { get; set; }
        public string Fifteen { get; set; }
        public string Sixteen { get; set; }
        public string Seventeen { get; set; }
        public string Eighteen { get; set; }
        public string Nineteen { get; set; }
        public string Twenty { get; set; }
        public string TwentyOne { get; set; }
        public string TwentyTwo { get; set; }
        public string TwentyThree { get; set; }
        public string TwentyFour { get; set; }
        public string TwentyFive { get; set; }
        public string TwentySix { get; set; }
        public string TwentySeven { get; set; }
        public string TwentyEight { get; set; }
        public string TwentyNine { get; set; }
        public string Thirty { get; set; }
        public string ThirtyOne { get; set; }
        public int Working_Day { get; set; }
        public int Lunch_Leave { get; set; }
        public int CL_EL_ML { get; set; }
        public int FestivalLeave_SpecialLeave { get; set; }
        public int TotalLeave { get; set; }
        public int Invalid { get; set; }
        public int TotalPresent { get; set; }
        public int GrandTotal { get; set; }
        public Int64 SL { get; set; }
        public int Staff { get; set; }
        public int Worker { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public decimal FemalePercent { get; set; }
        public decimal MalePercent { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal GrossSalary { get; set; }
        public string Grade { get; set; }
        public string DOB { get; set; }
        public string CalculateDate { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Days { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        public int Id { get; set; }

        public DateTime JoinDate { get; set; }

        public IEnumerable<VM_Report> DataListReports { get; set; }

        public List<VM_Report> ListData { get; set; }

        public List<VMAttendanceSummary> ReportList { get; set; }

        #region  Attendance Report

        public DataTable TotalMonthlyAttendnanceSummaryReprt(DateTime fromdate, DateTime todate, int sectionId)
        {

            DataTable table = new DataTable();
            //table.Columns.Add("U", typeof(string));
            //table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("SL", typeof(string));
            table.Columns.Add("Name CardNo", typeof(string));
            table.Columns.Add("Designation JoinDate", typeof(string));
            table.Columns.Add("1", typeof(string));
            table.Columns.Add("2", typeof(string));
            table.Columns.Add("3", typeof(string));
            table.Columns.Add("4", typeof(string));
            table.Columns.Add("5", typeof(string));
            table.Columns.Add("6", typeof(string));
            table.Columns.Add("7", typeof(string));
            table.Columns.Add("8", typeof(string));
            table.Columns.Add("9", typeof(string));
            table.Columns.Add("10", typeof(string));
            table.Columns.Add("11", typeof(string));
            table.Columns.Add("12", typeof(string));
            table.Columns.Add("13", typeof(string));
            table.Columns.Add("14", typeof(string));
            table.Columns.Add("15", typeof(string));
            table.Columns.Add("16", typeof(string));
            table.Columns.Add("17", typeof(string));
            table.Columns.Add("18", typeof(string));
            table.Columns.Add("19", typeof(string));
            table.Columns.Add("20", typeof(string));
            table.Columns.Add("21", typeof(string));
            table.Columns.Add("22", typeof(string));
            table.Columns.Add("23", typeof(string));
            table.Columns.Add("24", typeof(string));
            table.Columns.Add("25", typeof(string));
            table.Columns.Add("26", typeof(string));
            table.Columns.Add("27", typeof(string));
            table.Columns.Add("28", typeof(string));
            table.Columns.Add("29", typeof(string));
            table.Columns.Add("30", typeof(string));
            table.Columns.Add("31", typeof(string));
            table.Columns.Add("W.D", typeof(string));
            table.Columns.Add("P", typeof(string));
            table.Columns.Add("L", typeof(string));
            // table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("L.L", typeof(string));
            table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("T.L", typeof(string));
            table.Columns.Add("F.L S.L", typeof(string));
            table.Columns.Add("H.D", typeof(string));
            table.Columns.Add("AB", typeof(string));
            table.Columns.Add("INV", typeof(string));
            table.Columns.Add("T.P", typeof(string));
            table.Columns.Add("G.T", typeof(string));
            table.Columns.Add("OT", typeof(string));

            //Unit	CardTitle	EmployeeId	Name_CardNo	Designation_JoinDate	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24	25	26	27	28	29	30	31	
            //Working_Day	Present	Late	Lunch_Leave	CL_EL_ML	Total_Leave	FestivalLeave_SpecialLeave	Holiday	Absent	
            //Invalid	TotalPresent	GrandTotal	TotalOT
            table.Rows.Clear();

            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            var monthname = fromdate.ToString("MMMM");
            // DateTime dt = DateTime.Now;
            // Console.WriteLine(dt.ToString("MMMM"));

            var all_rows = db.Database.SqlQuery<VM_Report>("Exec [dbo].[sp_EmployeeMonthly_Attendance_Summary_Report] '" + dt1 + "','" + dt2 + "','"+ sectionId + "' ").ToList();
            //all_rows[i].Unit, all_rows[i].CardTitle, 
            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SL, all_rows[i].Name_CardNo, all_rows[i].Designation_JoinDate,
                    all_rows[i].One, all_rows[i].Two, all_rows[i].Three, all_rows[i].Four, all_rows[i].Five, all_rows[i].Six,
                    all_rows[i].Seven, all_rows[i].Eight, all_rows[i].Nine, all_rows[i].Ten, all_rows[i].Eleven, all_rows[i].Twelve,
                    all_rows[i].Thirteen, all_rows[i].Forteen, all_rows[i].Fifteen, all_rows[i].Sixteen, all_rows[i].Seventeen, all_rows[i].Eighteen,
                    all_rows[i].Nineteen, all_rows[i].Twenty, all_rows[i].TwentyOne, all_rows[i].TwentyTwo, all_rows[i].TwentyThree, all_rows[i].TwentyFour,
                    all_rows[i].TwentyFive, all_rows[i].TwentySix, all_rows[i].TwentySeven, all_rows[i].TwentyEight, all_rows[i].TwentyNine, all_rows[i].Thirty, all_rows[i].ThirtyOne,
                    all_rows[i].Working_Day, all_rows[i].Present, all_rows[i].Late, all_rows[i].Lunch_Leave,
                    all_rows[i].CL_EL_ML, all_rows[i].TotalLeave, all_rows[i].FestivalLeave_SpecialLeave, all_rows[i].Holiday,
                    all_rows[i].Absent, all_rows[i].Invalid, all_rows[i].TotalPresent, all_rows[i].GrandTotal, all_rows[i].TotalOT);
            }
            return table;
        }


        public DataTable TotalMonthlyAttendnanceReprt(DateTime fromdate, DateTime todate)
        {

            DataTable table = new DataTable();
            table.Columns.Add("Unit", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Name CardNo", typeof(string));
            table.Columns.Add("Designation JoinDate", typeof(string));
            table.Columns.Add("1", typeof(string));
            table.Columns.Add("2", typeof(string));
            table.Columns.Add("3", typeof(string));
            table.Columns.Add("4", typeof(string));
            table.Columns.Add("5", typeof(string));
            table.Columns.Add("6", typeof(string));
            table.Columns.Add("7", typeof(string));
            table.Columns.Add("8", typeof(string));
            table.Columns.Add("9", typeof(string));
            table.Columns.Add("10", typeof(string));
            table.Columns.Add("11", typeof(string));
            table.Columns.Add("12", typeof(string));
            table.Columns.Add("13", typeof(string));
            table.Columns.Add("14", typeof(string));
            table.Columns.Add("15", typeof(string));
            table.Columns.Add("16", typeof(string));
            table.Columns.Add("17", typeof(string));
            table.Columns.Add("18", typeof(string));
            table.Columns.Add("19", typeof(string));
            table.Columns.Add("20", typeof(string));
            table.Columns.Add("21", typeof(string));
            table.Columns.Add("22", typeof(string));
            table.Columns.Add("23", typeof(string));
            table.Columns.Add("24", typeof(string));
            table.Columns.Add("25", typeof(string));
            table.Columns.Add("26", typeof(string));
            table.Columns.Add("27", typeof(string));
            table.Columns.Add("28", typeof(string));
            table.Columns.Add("29", typeof(string));
            table.Columns.Add("30", typeof(string));
            table.Columns.Add("31", typeof(string));
            table.Columns.Add("Working Days", typeof(string));
            table.Columns.Add("Present", typeof(string));
            table.Columns.Add("Late", typeof(string));
            // table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("Lunch Leave", typeof(string));
            table.Columns.Add("CL_EL_ML", typeof(string));
            table.Columns.Add("Total_Leave", typeof(string));
            table.Columns.Add("FestivalLeave_SpecialLeave", typeof(string));
            table.Columns.Add("Holiday", typeof(string));
            table.Columns.Add("Absent", typeof(string));
            table.Columns.Add("Invalid", typeof(string));
            table.Columns.Add("Total Present", typeof(string));
            table.Columns.Add("GrandTotal", typeof(string));
            table.Columns.Add("TotalOT", typeof(string));

            //Unit	CardTitle	EmployeeId	Name_CardNo	Designation_JoinDate	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24	25	26	27	28	29	30	31	
            //Working_Day	Present	Late	Lunch_Leave	CL_EL_ML	Total_Leave	FestivalLeave_SpecialLeave	Holiday	Absent	
            //Invalid	TotalPresent	GrandTotal	TotalOT
            table.Rows.Clear();

            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            var monthname = fromdate.ToString("MMMM");
            // DateTime dt = DateTime.Now;
            // Console.WriteLine(dt.ToString("MMMM"));

            var all_rows = db.Database.SqlQuery<VM_Report>("Exec [dbo].[sp_EmployeeMonthly_Attendance_Report] '" + dt1 + "','" + dt2 + "' ").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].Unit, all_rows[i].CardTitle, all_rows[i].Name_CardNo, all_rows[i].Designation_JoinDate,
                    all_rows[i].One, all_rows[i].Two, all_rows[i].Three, all_rows[i].Four, all_rows[i].Five, all_rows[i].Six,
                    all_rows[i].Seven, all_rows[i].Eight, all_rows[i].Nine, all_rows[i].Ten, all_rows[i].Eleven, all_rows[i].Twelve,
                    all_rows[i].Thirteen, all_rows[i].Forteen, all_rows[i].Fifteen, all_rows[i].Sixteen, all_rows[i].Seventeen, all_rows[i].Eighteen,
                    all_rows[i].Nineteen, all_rows[i].Twenty, all_rows[i].TwentyOne, all_rows[i].TwentyTwo, all_rows[i].TwentyThree, all_rows[i].TwentyFour,
                    all_rows[i].TwentyFive, all_rows[i].TwentySix, all_rows[i].TwentySeven, all_rows[i].TwentyEight, all_rows[i].TwentyNine, all_rows[i].Thirty, all_rows[i].ThirtyOne,
                    all_rows[i].Working_Day, all_rows[i].Present, all_rows[i].Late, all_rows[i].Lunch_Leave,
                    all_rows[i].CL_EL_ML, all_rows[i].TotalLeave, all_rows[i].FestivalLeave_SpecialLeave, all_rows[i].Holiday,
                    all_rows[i].Absent, all_rows[i].Invalid, all_rows[i].TotalPresent, all_rows[i].GrandTotal, all_rows[i].TotalOT);
            }
            return table;
        }


        public DataTable TotalPeriodicAttendnanceReport(DateTime fromdate, DateTime todate)
        {
            db = new HrmsContext();
            //DateTime fDate = fromdate;
            //DateTime tDate = todate;
            DataTable table = new DataTable();
            table.Columns.Add("Unit", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Name CardNo", typeof(string));
            table.Columns.Add("Designation JoinDate", typeof(string));
            //int day = 1;
            //while (fDate != tDate)
            //{
            //    table.Columns.Add(day.ToString(), typeof(string));
            //    ++day;
            //    fDate = fDate.AddDays(1);
            //}


            table.Columns.Add("1", typeof(string));
            table.Columns.Add("2", typeof(string));
            table.Columns.Add("3", typeof(string));
            table.Columns.Add("4", typeof(string));
            table.Columns.Add("5", typeof(string));
            table.Columns.Add("6", typeof(string));
            table.Columns.Add("7", typeof(string));
            table.Columns.Add("8", typeof(string));
            table.Columns.Add("9", typeof(string));
            table.Columns.Add("10", typeof(string));
            table.Columns.Add("11", typeof(string));
            table.Columns.Add("12", typeof(string));
            table.Columns.Add("13", typeof(string));
            table.Columns.Add("14", typeof(string));
            table.Columns.Add("15", typeof(string));
            table.Columns.Add("16", typeof(string));
            table.Columns.Add("17", typeof(string));
            table.Columns.Add("18", typeof(string));
            table.Columns.Add("19", typeof(string));
            table.Columns.Add("20", typeof(string));
            table.Columns.Add("21", typeof(string));
            table.Columns.Add("22", typeof(string));
            table.Columns.Add("23", typeof(string));
            table.Columns.Add("24", typeof(string));
            table.Columns.Add("25", typeof(string));
            table.Columns.Add("26", typeof(string));
            table.Columns.Add("27", typeof(string));
            table.Columns.Add("28", typeof(string));
            table.Columns.Add("29", typeof(string));
            table.Columns.Add("30", typeof(string));
            table.Columns.Add("31", typeof(string));
            table.Columns.Add("Working Days", typeof(string));
            table.Columns.Add("Present", typeof(string));
            table.Columns.Add("Late", typeof(string));
            // table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("Lunch Leave", typeof(string));
            table.Columns.Add("CL_EL_ML", typeof(string));
            table.Columns.Add("Total_Leave", typeof(string));
            table.Columns.Add("FestivalLeave_SpecialLeave", typeof(string));
            table.Columns.Add("Holiday", typeof(string));
            table.Columns.Add("Absent", typeof(string));
            table.Columns.Add("Invalid", typeof(string));
            table.Columns.Add("Total Present", typeof(string));
            table.Columns.Add("GrandTotal", typeof(string));
            table.Columns.Add("TotalOT", typeof(string));

            //Unit	CardTitle	EmployeeId	Name_CardNo	Designation_JoinDate	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24	25	26	27	28	29	30	31	
            //Working_Day	Present	Late	Lunch_Leave	CL_EL_ML	Total_Leave	FestivalLeave_SpecialLeave	Holiday	Absent	
            //Invalid	TotalPresent	GrandTotal	TotalOT
            table.Rows.Clear();

            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            var monthname = fromdate.ToString("MMMM");
            // DateTime dt = DateTime.Now;
            // Console.WriteLine(dt.ToString("MMMM"));

            var all_rows = db.Database.SqlQuery<VM_Report>("Exec [dbo].[sp_EmployeeMonthly_Attendance_Report] '" + dt1 + "','" + dt2 + "' ").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].Unit, all_rows[i].CardTitle, all_rows[i].Name_CardNo, all_rows[i].Designation_JoinDate,
                    all_rows[i].One, all_rows[i].Two, all_rows[i].Three, all_rows[i].Four, all_rows[i].Five, all_rows[i].Six,
                    all_rows[i].Seven, all_rows[i].Eight, all_rows[i].Nine, all_rows[i].Ten, all_rows[i].Eleven, all_rows[i].Twelve,
                    all_rows[i].Thirteen, all_rows[i].Forteen, all_rows[i].Fifteen, all_rows[i].Sixteen, all_rows[i].Seventeen, all_rows[i].Eighteen,
                    all_rows[i].Nineteen, all_rows[i].Twenty, all_rows[i].TwentyOne, all_rows[i].TwentyTwo, all_rows[i].TwentyThree, all_rows[i].TwentyFour,
                    all_rows[i].TwentyFive, all_rows[i].TwentySix, all_rows[i].TwentySeven, all_rows[i].TwentyEight, all_rows[i].TwentyNine, all_rows[i].Thirty, all_rows[i].ThirtyOne,
                    all_rows[i].Working_Day, all_rows[i].Present, all_rows[i].Late, all_rows[i].Lunch_Leave,
                    all_rows[i].CL_EL_ML, all_rows[i].TotalLeave, all_rows[i].FestivalLeave_SpecialLeave, all_rows[i].Holiday,
                    all_rows[i].Absent, all_rows[i].Invalid, all_rows[i].TotalPresent, all_rows[i].GrandTotal, all_rows[i].TotalOT);
            }
            return table;
        }
        #region Anis Vai Code
        public DataTable DailyAttendanceDetail(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Posted", typeof(string));
            table.Columns.Add("Present", typeof(string));
            table.Columns.Add("Late", typeof(string));
            table.Columns.Add("Absent", typeof(string));
            table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("Holiday", typeof(string));
            table.Columns.Add("Offday", typeof(string));
            table.Columns.Add("Invalid", typeof(string));
            table.Columns.Add("Remarks", typeof(string));
         
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            TimeSpan Empty = new TimeSpan(00,00,00);

            #region Dead Code
            //var all_rows = db.Database.SqlQuery<VM_Report>($@"Select COALESCE(SectionName,'Grand Total') as SectionName, 
            //                COALESCE(Name,'Total')  as  [Designation],
            //                Sum(Posted) as Posted,Sum(Present +Late + Invalid)as Present,Sum(Late)as Late,
            //                Sum([Absent])as [Absent],Sum(Leave)as Leave,Sum(Holiday)as Holiday, Sum(Offday)as Offday,Sum(Invalid)as Invalid
            //                from(
            //                Select  SectionName, Name ,Posted,Present,Late,[Absent],Leave,Holiday,Remarks,Offday,Invalid
            //                from  (Select (Select count(1) as Posted from HRMS_Employee e where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID ) as Posted,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 1 and b.[Date]='{dt1}') as Present,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 2 and b.[Date]='{dt1}') as Late,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 3 and b.[Date]='{dt1}') as [Absent],
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 4 and b.[Date]='{dt1}') as Leave,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 6 and b.[Date]='{dt1}') as Holiday,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 5 and b.[Date]='{dt1}') as Offday,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 9 and b.[Date]='{dt1}') as Invalid,
            //                    u.ID as Unit_Id,u.UnitName, '' as Remarks, d.Name,s.SectionName from HRMS_Attendance_History a 
            //                    join HRMS_Employee e on a.EmployeeId = e.ID 
            //                    inner Join Designations d on e.Designation_Id = d.ID 
            //                    join Units u on e.Unit_Id = u.ID 
            //                    join Sections s on e.Section_Id=s.ID
            //                    where a.[Date]= '{dt1}' and e.Present_Status = 1) t  
            //                group by Posted, Present, Late,[Absent], Leave, Holiday, Offday, Invalid, Remarks, Name, SectionName 
            //                ) as a 
            //                group by SectionName, name  WITH ROLLUP").ToList();
           

            //for (int i = 0; i < all_rows.Count; i++)
            //{
            //    table.Rows.Add(all_rows[i].SectionName, all_rows[i].Designation, all_rows[i].Posted, all_rows[i].Present, all_rows[i].Late, all_rows[i].Absent, all_rows[i].Leave, all_rows[i].Holiday, all_rows[i].Offday, all_rows[i].Invalid, all_rows[i].Remarks);
            //}
            #endregion


            var VData = db.HrmsAttendanceHistory.Where(a => a.Date == fromdate).AsEnumerable();

            var all_rows = (from t1 in db.Employees
                            join t2 in db.Sections on t1.Section_Id equals t2.ID
                            join t3 in db.Designations on t1.Designation_Id equals t3.ID
                            where t1.Present_Status == 1
                            select new VM_Report
                            {
                                EmployeeId = t1.ID,
                                Designation = t3.Name,
                                SectionName = t2.SectionName,
                                SectionID = t1.Section_Id,
                                Designation_Id = t1.Designation_Id,
                                Present = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 1) ? 1 : 0,
                                Late = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 2) ? 1 : 0,
                                Absent = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 3) ? 1 : 0,
                                Leave = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 4) ? 1 : 0,
                                Offday = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 5) ? 1 : 0,
                                Holiday = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 6) ? 1 : 0,
                                Invalid = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 9) ? 1 : 0

                            }).ToList();



            var data = (from t1 in all_rows
                        group new { t1.Present, t1.Late, t1.Absent, t1.Leave, t1.Offday, t1.Holiday, t1.Invalid, t1.Posted } by new { t1.Designation_Id, t1.Designation, t1.SectionID, t1.SectionName } into g

                        select new VM_Report
                        {
                            SectionID = g.Key.SectionID,
                            SectionName = g.Key.SectionName,
                            Designation = g.Key.Designation,
                            Present = g.Sum(x => x.Present),
                            Late = g.Sum(x => x.Late),
                            Absent = g.Sum(x => x.Absent),
                            Leave = g.Sum(x => x.Leave),
                            Offday = g.Sum(x => x.Offday),
                            Holiday = g.Sum(x => x.Holiday),
                            Invalid = g.Sum(x => x.Invalid),

                            Posted = g.Count()
                        }).OrderBy(v => v.SectionName).ThenBy(s => s.Designation).ToList();

            var gData = (from t1 in data
                         group new { t1.SectionID, t1.Present, t1.Late, t1.Absent, t1.Leave, t1.Offday, t1.Holiday, t1.Invalid, t1.Posted } by new { t1.SectionID, t1.SectionName } into g
                         select new VM_Report
                         {
                             Id = g.Count(),
                             SectionID = g.Key.SectionID,
                             SectionName = g.Key.SectionName,
                             Present = g.Sum(x => x.Present),
                             Late = g.Sum(x => x.Late),
                             Absent = g.Sum(x => x.Absent),
                             Leave = g.Sum(x => x.Leave),
                             Offday = g.Sum(x => x.Offday),
                             Holiday = g.Sum(x => x.Holiday),
                             Invalid = g.Sum(x => x.Invalid),
                             Posted = g.Sum(x => x.Posted)
                         }).OrderBy(v => v.SectionName).ThenBy(s => s.Designation).ToList();

            foreach (var v in gData)
            {
                int number = 1;
                foreach (var a in data.Where(a => a.SectionID == v.SectionID))
                {
                    table.Rows.Add(
                        a.SectionName,
                        a.Designation,
                        a.Posted,
                        a.Present + a.Late + a.Invalid,
                        a.Late,
                        a.Absent,
                        a.Leave,
                        a.Holiday,
                        a.Offday,
                        a.Invalid,
                        ""
                        );
                    ++number;
                }

                if (v.Id < number)
                {
                    table.Rows.Add(
                        v.SectionName,
                        "Total",
                        v.Posted,
                        v.Present + v.Late + v.Invalid,
                        v.Late,
                        v.Absent,
                        v.Leave,
                        v.Holiday,
                        v.Offday,
                        v.Invalid,
                        ""
                        );
                    number = 0;
                }
            }

            if (gData.Any())
            {
                table.Rows.Add(
                        "Grand Total",
                        "Total",
                        gData.Sum(x=>x.Posted),
                        gData.Sum(x=>x.Present + x.Late + x.Invalid),
                        gData.Sum(x => x.Late),
                        gData.Sum(x => x.Absent),
                        gData.Sum(x => x.Leave),
                        gData.Sum(x => x.Holiday),
                        gData.Sum(x => x.Offday),
                        gData.Sum(x => x.Invalid),
                        ""
                        );
            }

            return table;

        }
        #endregion

        #region Sumaya Writting
        public DtDailyAttendanceSummery DailyAttendanceSummery1(DateTime fromdate)
        {
            DtDailyAttendanceSummery table = new DtDailyAttendanceSummery();
           
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            TimeSpan Empty = new TimeSpan(00, 00, 00);
            #region Dead Code
            //var all_rows = db.Database.SqlQuery<VM_Report>($@"Select COALESCE(SectionName,'Grand Total') as SectionName, 
            //                 COALESCE(Name,'Total')  as  [Designation],
            //                 Sum(Posted) as Posted,Sum(Present +Late + Invalid)as Present,Sum(Late)as Late,
            //                 Sum([Absent])as [Absent],Sum(Leave)as Leave,Sum(Holiday)as Holiday, Sum(Offday)as Offday,Sum(Invalid)as Invalid
            //                 from(
            //                 Select  SectionName, Name ,Posted,Present,Late,[Absent],Leave,Holiday,Remarks,Offday,Invalid
            //                 from  (Select (Select count(1) as Posted from HRMS_Employee e where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID ) as Posted,
            //                 (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 1 and b.[Date]='{dt1}') as Present,
            //                 (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 2 and b.[Date]='{dt1}') as Late,
            //                 (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 3 and b.[Date]='{dt1}') as [Absent],
            //                 (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 4 and b.[Date]='{dt1}') as Leave,
            //                 (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 6 and b.[Date]='{dt1}') as Holiday,
            //                 (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 5 and b.[Date]='{dt1}') as Offday,
            //                 (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 9 and b.[Date]='{dt1}') as Invalid,
            //                     u.ID as Unit_Id,u.UnitName, '' as Remarks, d.Name,s.SectionName from HRMS_Attendance_History a 
            //                     join HRMS_Employee e on a.EmployeeId = e.ID 
            //                     inner Join Designations d on e.Designation_Id = d.ID 
            //                     join Units u on e.Unit_Id = u.ID 
            //                     join Sections s on e.Section_Id=s.ID
            //                     where a.[Date]= '{dt1}' and e.Present_Status = 1) t  
            //                 group by Posted, Present, Late,[Absent], Leave, Holiday, Offday, Invalid, Remarks, Name, SectionName 
            //                 ) as a 
            //                 group by SectionName, name  WITH ROLLUP").ToList();
            //foreach (var a in all_rows.Where(a => a.Designation.Equals("Total")))
            //{
            //    table.DtDailyAttendance.Rows.Add(
            //        a.SectionName,
            //        a.Designation,
            //        a.Posted,
            //        a.Present,
            //        a.Late,
            //        a.Absent,
            //        a.Leave,
            //        a.Holiday,
            //        a.Offday,
            //        a.Invalid,
            //        a.Remarks,
            //        dt1);
            //}

            //return table;
            #endregion
      

            var VData = db.HrmsAttendanceHistory.Where(a => a.Date == fromdate).AsEnumerable();

            var all_rows = (from t1 in db.Employees
                            join t2 in db.Sections on t1.Section_Id equals t2.ID
                            join t3 in db.Designations on t1.Designation_Id equals t3.ID
                            where t1.Present_Status==1
                            select new VM_Report
                            {
                                EmployeeId = t1.ID,
                                Designation=t3.Name,
                                SectionName=t2.SectionName,
                                SectionID=t1.Section_Id,
                                Designation_Id=t1.Designation_Id,
                                //InTime= VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 1) ? VData.FirstOrDefault(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 1).InTime :Empty,
                                //Todate= VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 1) ? (DateTime)VData.FirstOrDefault(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 1).OutTime : DateTime.MinValue,
                                Present = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 1 ) ? 1 : 0,
                                Late = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 2) ? 1 : 0,
                                Absent = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 3) ? 1 : 0,
                                Leave = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 4) ? 1 : 0,
                                Offday = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 5) ? 1 : 0,
                                Holiday = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 6) ? 1 : 0,
                                Invalid = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 9) ? 1 : 0
                    

                            }).ToList();

          

            var data = (from t1 in all_rows
                        group new { t1.Present, t1.Late, t1.Absent, t1.Leave, t1.Offday, t1.Holiday, t1.Invalid, t1.Posted } by new { t1.Designation_Id, t1.Designation, t1.SectionID,t1.SectionName } into g
                        
                        select new VM_Report
                        {
                            SectionID = g.Key.SectionID,
                            SectionName = g.Key.SectionName,
                            Designation = g.Key.Designation,
                            Present = g.Sum(x => x.Present),
                            Late = g.Sum(x => x.Late),
                            Absent = g.Sum(x => x.Absent),
                            Leave = g.Sum(x => x.Leave),
                            Offday = g.Sum(x => x.Offday),
                            Holiday = g.Sum(x => x.Holiday),
                            Invalid = g.Sum(x => x.Invalid),
                            Posted = g.Count()
                        }).OrderBy(v=>v.SectionName).ThenBy(s=>s.Designation).ToList();

            var gData= (from t1 in data
                        group new { t1.SectionID, t1.Present, t1.Late, t1.Absent, t1.Leave, t1.Offday, t1.Holiday, t1.Invalid,t1.Posted } by new { t1.SectionID,t1.SectionName} into g
                        select new VM_Report
                        {
                            Id=g.Count(),
                            SectionID=g.Key.SectionID,
                            SectionName=g.Key.SectionName,
                            Present = g.Sum(x=>x.Present),
                            Late = g.Sum(x=>x.Late),
                            Absent = g.Sum(x => x.Absent),
                            Leave = g.Sum(x => x.Leave),
                            Offday = g.Sum(x => x.Offday),
                            Holiday = g.Sum(x => x.Holiday),
                            Invalid = g.Sum(x => x.Invalid),
                            Posted = g.Sum(x=>x.Posted)
                        }).OrderBy(v => v.SectionName).ThenBy(s => s.Designation).ToList();

            foreach (var v in gData)
            {
                 table.DtDailyAttendance.Rows.Add(
                        v.SectionName,
                        "Total",
                        v.Posted,
                        v.Present + v.Late + v.Invalid,
                        v.Late,
                        v.Absent,
                        v.Leave,
                        v.Holiday,
                        v.Offday,
                        v.Invalid,
                        "",
                        dt1);
            }

            

            return table;

        

        }

        public DtDailyAttendanceSummery DailyAttendanceDetails(DateTime fromdate)
        {
            DtDailyAttendanceSummery table = new DtDailyAttendanceSummery();

            var dt1 = fromdate.ToString("yyyy-MM-dd");
            TimeSpan Empty = new TimeSpan(00, 00, 00);
            #region Dead Code
            //var all_rows = db.Database.SqlQuery<VM_Report>($@"Select COALESCE(SectionName,'Grand Total') as SectionName, 
            //                COALESCE(Name,'Total')  as  [Designation],
            //                Sum(Posted) as Posted,Sum(Present +Late + Invalid)as Present,Sum(Late)as Late,
            //                Sum([Absent])as [Absent],Sum(Leave)as Leave,Sum(Holiday)as Holiday, Sum(Offday)as Offday,Sum(Invalid)as Invalid
            //                from(
            //                Select  SectionName, Name ,Posted,Present,Late,[Absent],Leave,Holiday,Remarks,Offday,Invalid
            //                from  (Select (Select count(1) as Posted from HRMS_Employee e where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID ) as Posted,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 1 and b.[Date]='{dt1}') as Present,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 2 and b.[Date]='{dt1}') as Late,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 3 and b.[Date]='{dt1}') as [Absent],
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 4 and b.[Date]='{dt1}') as Leave,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 6 and b.[Date]='{dt1}') as Holiday,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 5 and b.[Date]='{dt1}') as Offday,
            //                (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 9 and b.[Date]='{dt1}') as Invalid,
            //                    u.ID as Unit_Id,u.UnitName, '' as Remarks, d.Name,s.SectionName from HRMS_Attendance_History a 
            //                    join HRMS_Employee e on a.EmployeeId = e.ID 
            //                    inner Join Designations d on e.Designation_Id = d.ID 
            //                    join Units u on e.Unit_Id = u.ID 
            //                    join Sections s on e.Section_Id=s.ID
            //                    where a.[Date]= '{dt1}' and e.Present_Status = 1) t  
            //                group by Posted, Present, Late,[Absent], Leave, Holiday, Offday, Invalid, Remarks, Name, SectionName 
            //                ) as a 
            //                group by SectionName, name  WITH ROLLUP").ToList();

            #endregion

            var VData = db.HrmsAttendanceHistory.Where(a => a.Date == fromdate).AsEnumerable();

            var all_rows = (from t1 in db.Employees
                            join t2 in db.Sections on t1.Section_Id equals t2.ID
                            join t3 in db.Designations on t1.Designation_Id equals t3.ID
                            where t1.Present_Status==1
                            select new VM_Report
                            {
                                EmployeeId = t1.ID,
                                Designation=t3.Name,
                                SectionName=t2.SectionName,
                                SectionID=t1.Section_Id,
                                Designation_Id=t1.Designation_Id,
                                Present = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 1) ? 1 : 0,
                                Late = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 2) ? 1 : 0,
                                Absent = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 3) ? 1 : 0,
                                Leave = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 4) ? 1 : 0,
                                Offday = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 5) ? 1 : 0,
                                Holiday = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 6) ? 1 : 0,
                                Invalid = VData.Any(c => c.EmployeeId == t1.ID && c.AttendanceStatus == 9) ? 1 : 0
                            }).ToList();

          

            var data = (from t1 in all_rows
                        group new { t1.Present, t1.Late, t1.Absent, t1.Leave, t1.Offday, t1.Holiday, t1.Invalid, t1.Posted } by new { t1.Designation_Id, t1.Designation, t1.SectionID,t1.SectionName } into g
                        
                        select new VM_Report
                        {
                            SectionID = g.Key.SectionID,
                            SectionName = g.Key.SectionName,
                            Designation = g.Key.Designation,
                            Present = g.Sum(x => x.Present),
                            Late = g.Sum(x => x.Late),
                            Absent = g.Sum(x => x.Absent),
                            Leave = g.Sum(x => x.Leave),
                            Offday = g.Sum(x => x.Offday),
                            Holiday = g.Sum(x => x.Holiday),
                            Invalid = g.Sum(x => x.Invalid),
                         
                            Posted = g.Count()
                        }).OrderBy(v=>v.SectionName).ThenBy(s=>s.Designation).ToList();

            var gData= (from t1 in data
                        group new { t1.SectionID, t1.Present, t1.Late, t1.Absent, t1.Leave, t1.Offday, t1.Holiday, t1.Invalid,t1.Posted } by new { t1.SectionID,t1.SectionName} into g
                        select new VM_Report
                        {
                            Id=g.Count(),
                            SectionID=g.Key.SectionID,
                            SectionName=g.Key.SectionName,
                            Present = g.Sum(x=>x.Present),
                            Late = g.Sum(x=>x.Late),
                            Absent = g.Sum(x => x.Absent),
                            Leave = g.Sum(x => x.Leave),
                            Offday = g.Sum(x => x.Offday),
                            Holiday = g.Sum(x => x.Holiday),
                            Invalid = g.Sum(x => x.Invalid),
                            Posted = g.Sum(x=>x.Posted)
                        }).OrderBy(v => v.SectionName).ThenBy(s => s.Designation).ToList();

            foreach (var v in gData)
            {
                int number = 1;
                foreach (var a in data.Where(a => a.SectionID == v.SectionID))
                {
                    table.DtDailyAttendance.Rows.Add(
                        a.SectionName,
                        a.Designation,
                        a.Posted,
                        a.Present + a.Late + a.Invalid,
                        a.Late,
                        a.Absent,
                        a.Leave,
                        a.Holiday,
                        a.Offday,
                        a.Invalid,
                        "",
                        dt1);
                    ++number;
                }

                if (v.Id< number)
                {
                    table.DtDailyAttendance.Rows.Add(
                        v.SectionName,
                        "Total",
                        v.Posted,
                        v.Present + v.Late + v.Invalid,
                        v.Late,
                        v.Absent,
                        v.Leave,
                        v.Holiday,
                        v.Offday,
                        v.Invalid,
                        "",
                        dt1);
                    number = 0;
                }
            }

            

            return table;

        }
        #endregion
        public DataTable InstantDailyAttendanceSummery(DateTime fromdate)
        {
            DataTable table = new DataTable();
            // table.Columns.Add("Unit", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Posted", typeof(string));
            table.Columns.Add("Present", typeof(string));
            table.Columns.Add("Late", typeof(string));
            table.Columns.Add("Absent", typeof(string));
            table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("Holiday", typeof(string));
            table.Columns.Add("Offday", typeof(string));
            table.Columns.Add("Invalid", typeof(string));
            table.Columns.Add("Remarks", typeof(string));
            //var dt = "2018-05-01";
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            //case when SUM(Posted) = (Sum(Present)+Sum(Late)+Sum([Absent])+Sum(Leave)+Sum(Holiday)+Sum(Offday)+Sum(Invalid)) 
            //then Sum(Invalid) else SUM(Posted) - (Sum(Present)+Sum(Late)+Sum([Absent])+Sum(Leave)+Sum(Holiday)+Sum(Offday)+Sum(Invalid)) end as Invalid
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select COALESCE(SectionName,'Grand Total') as SectionName, 
                                                            COALESCE(Name,'Total')  as  [Designation],
                                                            Sum(Posted) as Posted,Sum(Present)as Present,Sum(Late)as Late,
                                                            Sum([Absent])as [Absent],Sum(Leave)as Leave,Sum(Holiday)as Holiday, Sum(Offday)as Offday,Sum(Invalid)as Invalid

                                                            from(
                                                            Select  SectionName, Name ,Posted,Present,Late,[Absent],Leave,Holiday,Remarks,Offday,Invalid
                                                            from  (Select (Select count(1) as Posted from HRMS_Employee e where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID ) as Posted,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 1 and b.[Date]='{dt1}') as Present,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 2 and b.[Date]='{dt1}') as Late,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 3 and b.[Date]='{dt1}') as [Absent],
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 4 and b.[Date]='{dt1}') as Leave,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 6 and b.[Date]='{dt1}') as Holiday,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 5 and b.[Date]='{dt1}') as Offday,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 9 and b.[Date]='{dt1}') as Invalid,
                                                                u.ID as Unit_Id,u.UnitName, '' as Remarks, d.Name,s.SectionName from HRMS_Daily_Attendance a 
                                                                join HRMS_Employee e on a.EmployeeId = e.ID 
                                                                inner Join Designations d on e.Designation_Id = d.ID 
                                                                join Units u on e.Unit_Id = u.ID 
                                                                join Sections s on e.Section_Id=s.ID
                                                                where a.[Date]= '{dt1}' and e.Present_Status = 1) t  
                                                            group by Posted, Present, Late,[Absent], Leave, Holiday, Offday, Invalid, Remarks, Name, SectionName 
                                                            ) as a 
                                                            group by SectionName, name  WITH ROLLUP").ToList();
            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].Designation, all_rows[i].Posted, all_rows[i].Present, all_rows[i].Late, all_rows[i].Absent, all_rows[i].Leave, all_rows[i].Holiday, all_rows[i].Offday, all_rows[i].Invalid, all_rows[i].Remarks);
            }
            return table;

        }

        public DataTable PersonalMonthlyAttendanceSummery(DateTime fromdate, DateTime todate, int employeeId)
        {
            DataTable table = new DataTable();
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Employee_ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("UnitName", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            table.Columns.Add("TotalOT", typeof(string));
            table.Columns.Add("BOT", typeof(string));
            table.Columns.Add("EOT", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            TimeSpan Empty = new TimeSpan(00,00,00);
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name],u.UnitName,l.Line,s.SectionName,
                                                         convert(varchar(10),a.[Date],126) as [Date],CAST(a.Intime AS time) as InTime,CAST(a.[OutTime]  AS time)as OutTime,
                                                         (SELECT [dbo].[fn_BuyerStatus](a.AttendanceStatus,e.ID, a.[Date])) as [Status],
                                                         --(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' 
                                                         --WHEN a.AttendanceStatus = 2 THEN 'Late' 
                                                         --WHEN a.AttendanceStatus = 3 THEN 'Absent' 
                                                         --WHEN a.AttendanceStatus = 4 THEN 'Leave' 
                                                         --WHEN a.AttendanceStatus = 5 THEN 'OffDay' 
                                                         --WHEN a.AttendanceStatus = 6 THEN 'Holiday' 
                                                         --ELSE 'Invalid' END) as [Status], 
                                                         PayableOverTime AS [TotalOT], 
                                                        (SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 WHEN a.PayableOverTime >=1 THEN 1 ELSE 0 END),e.ID, a.[Date])) as [BOT],
                                                         --(SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END),e.ID, a.[Date])) as [BOT],
                                                         (SELECT [dbo].[fn_EmployeeOffdayEOT](a.PayableOverTime,e.ID, a.[Date])) as [EOT]
                                                         --[dbo].[fn_EmployeeOffdayEOT]
                                                         --(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [BOT], 
                                                         ---(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE  0 END ) as [EOT]
                                                          from HRMS_Attendance_History a 
                                                         join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID 
                                                         join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID 
                                                         join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID Where[Date] between '" + dt1 + "' and '" + dt2 + "' and e.ID =" + employeeId + " ").ToList();
            //"Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name]," +
            //                                           "u.UnitName,l.Line,s.SectionName, convert(varchar(10),a.[Date],126) as [Date],CAST(a.Intime AS time) as InTime,CAST(a.[OutTime]  AS time)as OutTime, " +
            //                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
            //                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
            //                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
            //                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
            //                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay' " +
            //                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
            //                                           "ELSE 'Invalid' END) as [Status], " +
            //                                           "PayableOverTime AS[TotalOT], " +
            //                                           "(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [BOT], " +
            //                                           "(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END) as [EOT] " +
            //                                           " from HRMS_Attendance_History a " +
            //                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
            //                                           "join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID " +
            //                                           "join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID " +
            //                                           "Where[Date] between '"+ dt1 + "' and '"+ dt2+ "' and e.ID ="+ employeeId + " ").ToList();

            foreach ( var a in all_rows)
            {
                if(a.InTime== Empty && a.OutTime==Empty && a.Status== "Present")
                {

                    a.Status = "Absent";
                }


            }


            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].CardTitle, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].SectionName, all_rows[i].UnitName, all_rows[i].Line, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status, all_rows[i].TotalOT, all_rows[i].BOT, all_rows[i].EOT);
            }
            return table;
        }

        public DataTable ThreeHourPersonalMonthlyAttendanceSummery(DateTime fromdate, DateTime todate, int employeeId)
        {
            DataTable table = new DataTable();
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Employee_ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("UnitName", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            //table.Columns.Add("TotalOT", typeof(string));
            table.Columns.Add("BOT", typeof(string));
            table.Columns.Add("EOT", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name],u.UnitName,l.Line,s.SectionName,
                                                         convert(varchar(10),a.[Date],126) as [Date],
                                                         (SELECT [dbo].[fn_BuyerinTime] (CAST(a.[Intime]  AS time),a.[Date],e.ID)) as InTime,
                                                        (SELECT [dbo].[fn_BuyerOutTime] (CAST(a.[OutTime]  AS time),
                                                        (CASE WHEN a.PayableOverTime >= 3 THEN PayableOverTime-3 ELSE 0 END),a.[Date],e.ID))as OutTime,
                                                         (SELECT [dbo].[fn_BuyerStatus](a.AttendanceStatus,e.ID, a.[Date])) as [Status],
                                                         --(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' 
                                                         --WHEN a.AttendanceStatus = 2 THEN 'Late' 
                                                         --WHEN a.AttendanceStatus = 3 THEN 'Absent' 
                                                         --WHEN a.AttendanceStatus = 4 THEN 'Leave' 
                                                         --WHEN a.AttendanceStatus = 5 THEN 'OffDay' 
                                                         --WHEN a.AttendanceStatus = 6 THEN 'Holiday' 
                                                         --ELSE 'Invalid' END) as [Status], 
                                                       --  PayableOverTime AS [TotalOT], 
                                                        (SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 WHEN a.PayableOverTime >=1 THEN 1 ELSE 0 END),e.ID, a.[Date])) as [BOT],
                                                         --(SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END),e.ID, a.[Date])) as [BOT],
                                                         (SELECT [dbo].[fn_EmployeeOffdayEOT3Hour](a.PayableOverTime,e.ID, a.[Date])) as [EOT]
                                                         --[dbo].[fn_EmployeeOffdayEOT]
                                                         --(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [BOT], 
                                                         ---(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE  0 END ) as [EOT]
                                                          from HRMS_Attendance_History a 
                                                         join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID 
                                                         join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID 
                                                         join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID Where[Date] between '" + dt1 + "' and '" + dt2 + "' and e.ID =" + employeeId + " ").ToList();


            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].CardTitle, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].SectionName, all_rows[i].UnitName, all_rows[i].Line, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status, all_rows[i].BOT, all_rows[i].EOT);
            }
            return table;
        }
        public DataTable PersonalMonthlyAttendanceSummeryBuyerReprt(DateTime fromdate, DateTime todate, int employeeId)
        {
            DataTable table = new DataTable();
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Employee_ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("UnitName", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            table.Columns.Add("OT", typeof(string));
            //table.Columns.Add("BOT", typeof(string));
            //table.Columns.Add("EOT", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name],u.UnitName,l.Line,s.SectionName,
                                         convert(varchar(10),a.[Date],126) as [Date],
                                         --CAST(a.Intime AS time) as InTime,
                                         (SELECT [dbo].[fn_BuyerinTime] (CAST(a.[Intime]  AS time),a.[Date],e.ID)) as InTime,
                                         (SELECT [dbo].[fn_BuyerOutTime] (CAST(a.[OutTime]  AS time),
                                         (CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END),a.[Date],e.ID))as OutTime, 
                                         (SELECT [dbo].[fn_BuyerStatus](a.AttendanceStatus,e.ID, a.[Date])) as [Status],
                                         --(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' WHEN a.AttendanceStatus = 2 THEN 'Late'
                                         -- WHEN a.AttendanceStatus = 3 THEN 'Absent' WHEN a.AttendanceStatus = 4 THEN 'Leave'
                                         --  WHEN a.AttendanceStatus = 5 THEN 'OffDay' WHEN a.AttendanceStatus = 6 THEN 'Holiday' 
                                         --  ELSE 'Invalid' END) as [Status], 
                                           PayableOverTime AS[TotalOT], 
                                           --[dbo].[fn_BuyerOffdayOT]
                                            (SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 WHEN a.PayableOverTime >=1 THEN 1 ELSE 0 END),e.ID, a.[Date])) as [OT],
                                          -- (SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END),e.ID, a.[Date])) as [OT],
                                           --(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [OT], 
                                           (CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END) as [EOT]  
                                           from HRMS_Attendance_History a join HRMS_Employee e on a.EmployeeId = e.ID join 
                                           HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID join 
                                           Units u on e.Unit_Id = u.ID join 
                                           Designations d on e.Designation_Id = d.ID join 
                                           Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID 
                                            Where[Date] between '" + dt1 + "' and '" + dt2 + "' and e.ID =" + employeeId + " ").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].CardTitle, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].SectionName, all_rows[i].UnitName, all_rows[i].Line, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status, all_rows[i].OT);
            }
            return table;
        }

        public void PersonalMonthlyAttendanceSummeryBuyerReprt1(DateTime fromdate, DateTime todate, int employeeId)
        {
            var att  = new List<VM_Report>();
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            TimeSpan Empty = new TimeSpan(00,00,00);
             att = db.Database.SqlQuery<VM_Report>($@"Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name],u.UnitName,l.Line,s.SectionName,e.Staff_Type, convert(varchar(10),e.Joining_Date,126) as [Joining_Date],
                                         convert(varchar(10),a.[Date],126) as [Date],
                                         --CAST(a.Intime AS time) as InTime,
                                         (SELECT [dbo].[fn_BuyerinTime] (CAST(a.[Intime]  AS time),a.[Date],e.ID)) as InTime,
                                         (SELECT [dbo].[fn_BuyerOutTime] (CAST(a.[OutTime]  AS time),
                                         (CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END),a.[Date],e.ID))as OutTime, 
                                         (SELECT [dbo].[fn_BuyerStatus](a.AttendanceStatus,e.ID, a.[Date])) as [Status],
                                         --(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' WHEN a.AttendanceStatus = 2 THEN 'Late'
                                         -- WHEN a.AttendanceStatus = 3 THEN 'Absent' WHEN a.AttendanceStatus = 4 THEN 'Leave'
                                         --  WHEN a.AttendanceStatus = 5 THEN 'OffDay' WHEN a.AttendanceStatus = 6 THEN 'Holiday' 
                                         --  ELSE 'Invalid' END) as [Status], 
                                           PayableOverTime AS[TotalOT], 
                                           --[dbo].[fn_BuyerOffdayOT]
                                            (SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 WHEN a.PayableOverTime >=1 THEN 1 ELSE 0 END),e.ID, a.[Date])) as [OT],
                                          -- (SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END),e.ID, a.[Date])) as [OT],
                                           --(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [OT], 
                                           (CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END) as [EOT]  
                                           from HRMS_Attendance_History a join HRMS_Employee e on a.EmployeeId = e.ID join 
                                           HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID join 
                                           Units u on e.Unit_Id = u.ID join 
                                           Designations d on e.Designation_Id = d.ID join 
                                           Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID 
                                            Where[Date] between '" + dt1 + "' and '" + dt2 + "' and e.ID =" + employeeId + " order by Date ASC").ToList();
            if (att.Any())
            {

                foreach (var a in att)
                {
                    if (a.InTime == Empty && a.OutTime == Empty && a.Status == "Present")
                    {
                        a.Status = "Absent";
                    }
                }

            }

            this.DataListReports = att.OrderBy(a=>a.Date);

        }

       
        public VM_Report PersonalMonthlyAttendanceSummeryBuyerReprtBSCI(VM_Report vModel)
        {
            VM_Report lstData = new VM_Report();
            //var dt1 = fromdate.ToString("yyyy-MM-dd");
            //var dt2 = todate.ToString("yyyy-MM-dd");
            lstData.Fromdate = vModel.Fromdate;
            lstData.Todate = vModel.Todate;
            lstData.EmployeeId = vModel.EmployeeId;
            lstData.ReportList = new List<VMAttendanceSummary>();
            DBHelpers dbHelper = new DBHelpers();

            var vData = (from t1 in db.Employees
                         join t2 in db.Departments on t1.Department_Id equals t2.ID
                         join t3 in db.Sections on t1.Section_Id equals t3.ID
                         join t4 in db.Designations on t1.Designation_Id equals t4.ID
                         join t5 in db.Units on t1.Unit_Id equals t5.ID
                         where t1.ID == vModel.EmployeeId
                         select new VM_Report
                         {
                             Name = t1.Name,
                             CardNo = t1.CardTitle + " " + t1.EmployeeIdentity,
                             SectionName = t3.SectionName,
                             Staff_Type = t1.Staff_Type,
                             Designation = t4.Name,
                             JoinDate = t1.Joining_Date,
                             Line = t1.Line_Name,


                         }).FirstOrDefault();
            var vPresent = dbHelper.GetAuditRegularAttendanceForBSCI(vModel.EmployeeId, vModel.Fromdate, vModel.Todate).OrderBy(a => a.PresentDate);

            if (vPresent.Any())
            {
                JobAgeCalculate(vData.JoinDate, vData.Todate);

                lstData.Name = vData.Name;
                lstData.CardNo = vData.CardNo;
                lstData.SectionName = vData.SectionName;
                lstData.Staff_Type = vData.Staff_Type;
                lstData.Designation = vData.Name;
                lstData.JoinDate = vData.JoinDate;
                lstData.Line = vData.Line;
                lstData.Year = Year;
                lstData.Month = Month;
                lstData.Present = vPresent.Count(a => a.Status == "Present");
                lstData.Late = vPresent.Count(a => a.Status == "Late");
                lstData.Absent = vPresent.Count(a => a.Status == "Absent");
                lstData.Leave = vPresent.Count(a => a.Status == "Leave");
                lstData.Offday = vPresent.Count(a => a.Status == "Offday");
                lstData.Holiday = vPresent.Count(a => a.Status == "Holiday");
                lstData.TotalPresent = lstData.Present + lstData.Late;
                lstData.GrandTotal = vPresent.Count();
                lstData.BOT = vPresent.Sum(a => a.BuyerOT);
                lstData.EOT = vPresent.Sum(a => a.ExtraOTHour);
                lstData.OT = vPresent.Sum(a => a.LegalOTHour);

                lstData.ReportList.AddRange(vPresent);

            }

            return lstData;
        }


        public VM_Report JobCardSummery(VM_Report vModel)
        {
            VM_Report lstData = new VM_Report();
     
            lstData.Fromdate = vModel.Fromdate;
            lstData.Todate = vModel.Todate;
            lstData.EmployeeId = vModel.EmployeeId;
            lstData.ListData = new List<VM_Report>();

            DBHelpers dbHelper = new DBHelpers();

            var vData = (from t1 in db.Employees
                         join t2 in db.Departments on t1.Department_Id equals t2.ID
                         join t3 in db.Sections on t1.Section_Id equals t3.ID
                         join t4 in db.Designations on t1.Designation_Id equals t4.ID
                         join t5 in db.Units on t1.Unit_Id equals t5.ID
                         where t1.Section_Id == vModel.SectionID && t1.Present_Status==1
                         select new VM_Report
                         {
                             EmployeeId=t1.ID,
                             Name = t1.Name,
                             CardTitle = t1.CardTitle ,
                             CardNo= t1.EmployeeIdentity,
                             SectionName = t3.SectionName,
                             Staff_Type = t1.Staff_Type
                         }).ToList();

            if (vData.Any())
            {
                vData.ForEach(x => {
                    VM_Report m = new VM_Report();
                    var vPresent = dbHelper.GetAuditRegularAttendanceForBSCI(x.EmployeeId, vModel.Fromdate, vModel.Todate).OrderBy(a => a.PresentDate);
                    m.Name = x.Name;
                    m.CardTitle = x.CardTitle;
                    m.CardNo = x.CardNo;
                    m.SectionName = x.SectionName;
                    m.Staff_Type = x.Staff_Type;
                    m.Present = vPresent.Count(a => a.Status == "Present");
                    m.Late = vPresent.Count(a => a.Status == "Late");
                    m.Absent = vPresent.Count(a => a.Status == "Absent");
                    m.Leave = vPresent.Count(a => a.Status == "Leave");
                    m.Offday = vPresent.Count(a => a.Status == "Offday");
                    m.Holiday = vPresent.Count(a => a.Status == "Holiday");
                    m.TotalPresent = m.Present + m.Late;
                    m.GrandTotal = vPresent.Count();

                    lstData.ListData.Add(m);
                });
            }

            return lstData;
        }

        public void PersonalMonthlyAttendanceSummery1(DateTime fromdate, DateTime todate, int employeeId)
        {
            var att = new List<VM_Report>();
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            TimeSpan Empty = new TimeSpan(00,00,00);
             att = db.Database.SqlQuery<VM_Report>($@"Select (select top 1 e.Degree from HRMS_Education as e where e.Employee_Id = e.ID) AS Degree, DATEDIFF(MONTH, e.Joining_Date, GETDATE()) AS DateDiff, e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name],u.UnitName,l.Line,s.SectionName,e.Staff_Type,convert(varchar(10),e.Joining_Date,126) as [Joining_Date],
                                                         convert(varchar(10),a.[Date],126) as [Date],CAST(a.Intime AS time) as InTime,CAST(a.[OutTime]  AS time)as OutTime,
                                                         (SELECT [dbo].[fn_BuyerStatus](a.AttendanceStatus,e.ID, a.[Date])) as [Status],
                                                         --(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' 
                                                         --WHEN a.AttendanceStatus = 2 THEN 'Late' 
                                                         --WHEN a.AttendanceStatus = 3 THEN 'Absent' 
                                                         --WHEN a.AttendanceStatus = 4 THEN 'Leave' 
                                                         --WHEN a.AttendanceStatus = 5 THEN 'OffDay' 
                                                         --WHEN a.AttendanceStatus = 6 THEN 'Holiday' 
                                                         --ELSE 'Invalid' END) as [Status], 
                                                         PayableOverTime AS [TotalOT], 
                                                        (SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 WHEN a.PayableOverTime >=1 THEN 1 ELSE 0 END),e.ID, a.[Date])) as [BOT],
                                                         --(SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END),e.ID, a.[Date])) as [BOT],
                                                         (SELECT [dbo].[fn_EmployeeOffdayEOT](a.PayableOverTime,e.ID, a.[Date])) as [EOT]
                                                         --[dbo].[fn_EmployeeOffdayEOT]
                                                         --(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [BOT], 
                                                         ---(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE  0 END ) as [EOT]
                                                          from HRMS_Attendance_History a 
                                                         join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID 
                                                         join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID 
                                                         join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID Where[Date] between '" + dt1 + "' and '" + dt2 + "' and e.ID =" + employeeId + " ").ToList();
            if (att.Any())
            {

                foreach(var a in att)
                {
                    if (a.InTime== Empty && a.OutTime== Empty && a.Status== "Present")
                    {
                        a.Status = "Absent";
                    }
                }

            }

            this.DataListReports = att.OrderBy(a => a.Date);
        }

        #region Yearly Attendance Summary
        public DtYearlyAttendance YearlyAttendanceSummary(DateTime fromdate, DateTime todate, int employeeId)
        {

            DtYearlyAttendance ds = new DtYearlyAttendance();
            //DateTime fdate = fromdate;
            //int fromMonth = fdate.Month;
            //int fromYear = fdate.Year;
            //DateTime tdate = todate;
            //int toMonth = tdate.Month;
            //int toYear = tdate.Year;
            int toMonth = todate.Month;
            int toYear = todate.Year;
            DateTime expectedDate = new DateTime(toYear, toMonth, DateTime.DaysInMonth(toYear, toMonth)); 
            int count = 1;
            int sl = 0;

            TimeSpan empty = new TimeSpan(00,00,00);
            while (fromdate <= todate)
            {
                int fromMonth = fromdate.Month;
                int fromYear = fromdate.Year;
                int monthOfDays = DateTime.DaysInMonth(fromYear, fromMonth);
                DateTime createFDate = new DateTime(fromYear, fromMonth,01);
                DateTime createTDate = new DateTime(fromYear, fromMonth,monthOfDays );
                
                PersonalMonthlyAttendanceSummery1(createFDate, createTDate, employeeId);
                var vData = this.DataListReports;
                if (count == 1)
                {
                    var take = vData.FirstOrDefault();
                    ds.DtEmployee.Rows.Add(take.Name,take.CardTitle,take.Employee_ID,take.Designation_Name,take.SectionName,take.Joining_Date, createFDate, expectedDate);
                }
                VM_Report model = new VM_Report();
                model.Fromdate = createFDate;
                model.Working_Day = vData.GroupBy(c=>c.Date).Count();
                model.Present= vData.Where(a=>a.InTime!=empty && a.OutTime!=empty && a.Status== "Present").Count();
                model.Late=vData.Count(x => x.Status.Equals("Late"));
                model.Leave=vData.Count(x => x.Status.Equals("Leave"));
                model.Absent = vData.Count(x => x.Status.Equals("Absent"));
                model.FestivalLeave_SpecialLeave = vData.Count(x => x.Status.Equals("Holiday"));
                model.TotalPresent = model.Present + model.Late + model.Leave;
                model.GrandTotal = model.Present + model.Late;
                ds.YearAttendance.Rows.Add(

                    ++sl,
                    model.Fromdate,
                    model.Working_Day,
                    model.FestivalLeave_SpecialLeave,
                    0, //Special Leave
                    model.Leave,
                    model.Absent,
                    model.Late,
                    model.Present,
                    model.TotalPresent,
                    model.GrandTotal
                    );


                int addMonth = fromMonth + 1;
                if (addMonth>12)
                {
                    fromYear = fromYear + 1;
                    addMonth = 1;
                }
                DateTime createFDate1 = new DateTime(fromYear, addMonth,01);
                ++count;
                fromdate = createFDate1;
            }
          return ds;


        }

        #endregion

        public DataTable DailyInvalidAttendance(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "' and a.Intime = a.[OutTime] " +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }


        public DataTable DailyAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }



        public DataTable InstantDailyAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            //CAST(a.[OutTime]  AS time) as OutTime
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, '00:00:00' as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Daily_Attendance a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable DailyLateAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 2" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable InstantDailyLateAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            // table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, " + //, CAST(a.[OutTime]  AS time) as OutTime
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Daily_Attendance a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 2" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].Status);
            }
            return table;
        }


        public DataTable DailyAbsentAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 3" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable InstantDailyAbsentAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Daily_Attendance a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 3" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable DailyInvalidAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 9" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable InstantDailyInvalidAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Daily_Attendance a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 9" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable SectionWiseTiffinList(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.PayableOverTime >=5" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable SectionWiseTiffinListSummery(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section Name", typeof(string));
            table.Columns.Add("Designation Name", typeof(string));
            table.Columns.Add("Posted", typeof(string));
            table.Columns.Add("Tiffin", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select COALESCE(SectionName,'Z. Grand Total') as SectionName,  
            COALESCE(Designation_Name, 'Total') as  [Designation_Name], Sum(Posted) as Posted, Sum(Tiffinlist) as Present from
            (Select  SectionName,[Designation_Name], Posted, Tiffinlist from(Select(Select count(1) from HRMS_Employee where designation_Id = d.ID and Present_Status = 1) as Posted,
            (Select Count(1) from HRMS_Attendance_History at inner join HRMS_Employee e1 on at.EmployeeId = e1.ID
            Where convert(varchar(10), at.[Date], 126) = '{dt1}' and at.PayableOverTime >= 5 and e1.Designation_Id = e.Designation_Id and e1.Section_Id = e.Section_Id) as Tiffinlist,
            d.Name as [Designation_Name], u.ID as Unit_Id, u.UnitName, '' as Remarks, d.Name, s.SectionName from HRMS_Attendance_History a
            join HRMS_Employee e on a.EmployeeId = e.ID inner Join Designations d on e.Designation_Id = d.ID
            join Units u on e.Unit_Id = u.ID join Sections s on e.Section_Id = s.ID
            where a.[Date] = '{dt1}') t group by Posted, SectionName,[Designation_Name], Tiffinlist ) as a
            group by SectionName, Designation_Name WITH ROLLUP ORDER BY SectionName").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].Designation_Name, all_rows[i].Posted, all_rows[i].Present);
            }
            return table;
        }


        public DataTable OvertimeReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Employee_ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("UnitName", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            table.Columns.Add("TotalOT", typeof(string));
            table.Columns.Add("BOT", typeof(string));
            table.Columns.Add("EOT", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            // var dt2 = todate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name]," +
                                                           "u.UnitName,l.Line,s.SectionName, convert(varchar(10),a.[Date],126) as [Date],CAST(a.Intime AS time) as InTime,CAST(a.[OutTime]  AS time)as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay' " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status], " +
                                                           "PayableOverTime AS[TotalOT], " +
                                                           "(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [BOT], " +
                                                           "(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END) as [EOT] " +
                                                           " from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID " +
                                                           "join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID " +
                                                           "Where[Date] = '" + dt1 + "'").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].CardTitle, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].SectionName, all_rows[i].UnitName, all_rows[i].Line, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status, all_rows[i].TotalOT, all_rows[i].BOT, all_rows[i].EOT);
            }
            return table;
        }


        #endregion



        #region Employee Report

        //Staff Summery Report

        public DataTable StaffAndWorkerSummery()
        {
            DataTable table = new DataTable();
            table.Columns.Add("SL", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("Total Employee", typeof(string));
            table.Columns.Add("Staff", typeof(string));
            table.Columns.Add("Worker", typeof(string));
            table.Columns.Add("Remarks", typeof(string));
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select ROW_NUMBER() OVER(ORDER BY SectionName DESC) AS SL,
                    COALESCE(SectionName,'Grand Total') as SectionName, sum(TotalPresent) as TotalPresent,
                    sum(Staff)as Staff,sum(Worker) as Worker,'' as Remarks from(Select distinct s.SectionName,
                    (select count(ee.ID) from HRMS_Employee ee where ee.Section_Id=e.Section_Id and ee.Present_Status=1) as TotalPresent,
                    (select count(ee.ID) from HRMS_Employee ee where ee.Section_Id=e.Section_Id and ee.Present_Status=1 and ee.Staff_Type='Staff') as Staff,
                    (select count(ee.ID) from HRMS_Employee ee where ee.Section_Id=e.Section_Id and ee.Present_Status=1 and ee.Staff_Type='Worker') as Worker
                    from HRMS_Employee e
                    inner join Sections s on s.ID=e.Section_Id
                    where e.Present_Status = 1) t
                    group by SectionName  WITH ROLLUP").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SL,all_rows[i].SectionName, all_rows[i].TotalPresent, all_rows[i].Staff, all_rows[i].Worker
                    , all_rows[i].Remarks);
            }
            return table;
        }


        //Male Female Report

        public DataTable MaleFemaleReport()
        {
            DataTable table = new DataTable();
            table.Columns.Add("SL", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("Total Employee", typeof(string));
            table.Columns.Add("Female", typeof(string));
            table.Columns.Add("Female Percent", typeof(string));
            table.Columns.Add("Male", typeof(string));
            table.Columns.Add("Male Percent", typeof(string));
            table.Columns.Add("Remarks", typeof(string));
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select ROW_NUMBER() OVER(ORDER BY SectionName DESC) AS SL,
                                COALESCE(SectionName,'Grand Total') as SectionName, sum(TotalPresent) as TotalPresent, 
                                sum(isnull(Female,0))as Female, 
                                convert(decimal(18,2),(convert(decimal(18,2),sum(isnull(Female,0))*100))/sum(TotalPresent)) as FemalePercent, 
                                sum(isnull(Male,0)) as Male, convert(decimal(18,2),(convert(decimal(18,2),sum(isnull(Male,0))*100))/sum(TotalPresent)) as MalePercent ,
                                '' as Remarks from(
                                Select distinct s.SectionName,
                                (select count(ee.ID) from HRMS_Employee ee where ee.Section_Id=em.Section_Id and ee.Present_Status=1) as TotalPresent,
                                (select count(ee.ID) from HRMS_Employee ee where ee.Section_Id=em.Section_Id and ee.Present_Status=1 and ee.Gender='Female') as Female,
                                (select count(ee.ID) from HRMS_Employee ee where ee.Section_Id=em.Section_Id and ee.Present_Status=1 and ee.Gender='Male') as Male,
                                (Select Count(ee.Section_ID)*100.0 /(Select count(*) from HRMS_Employee e where e.Present_Status=1 and e.Section_Id=em.Section_Id) as MalePercent 
                                from HRMS_Employee ee
                                inner join Sections s on s.ID=ee.Section_Id
                                where ee.Present_Status = 1 and ee.Gender='Male'
                                and ee.Section_ID=em.Section_Id
                                group by ee.Section_ID) as MalePercent,
                                ( Select (Count(ee.Section_ID)*100.0) /(Select count(*) from HRMS_Employee e where e.Present_Status=1 and e.Section_Id=em.Section_Id) as FemalePercent 
                                from HRMS_Employee ee
                                inner join Sections s on s.ID=ee.Section_Id
                                where ee.Present_Status = 1 and ee.Gender='Female'
                                and ee.Section_ID=em.Section_Id
                                group by ee.Section_ID) as FemalePercent
                                from HRMS_Employee em
                                inner join Sections s on s.ID=em.Section_Id
                                where em.Present_Status = 1) t
                                group by SectionName  WITH ROLLUP").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SL, all_rows[i].SectionName, all_rows[i].TotalPresent, all_rows[i].Female, all_rows[i].FemalePercent, 
                    all_rows[i].Male, all_rows[i].MalePercent, all_rows[i].Remarks);
            }
            return table;
        }


        //Designation Wise Report

        public DataTable DesignationWiseEmployeeReport(int id)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SL", typeof(string));
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Grade", typeof(string));
            table.Columns.Add("Basic", typeof(string));
            table.Columns.Add("Gross", typeof(string));
            table.Columns.Add("Remarks", typeof(string));
            if (id == 0)
            {
                var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SL,Employee_ID,Name,Designation,SectionName,Grade,isnull(BasicSalary,0) as BasicSalary,sum(isnull(BasicSalary,0)+isnull(HouseRent,0)+isnull(Others,0)) as GrossSalary from  
                            (Select ROW_NUMBER() OVER(ORDER BY s.SectionName DESC) AS SL,e.EmployeeIdentity+' '+ e.CardTitle as Employee_ID,e.Name,
                            d.Name as Designation,s.SectionName,e.Grade,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=1 and [Status]=1) as BasicSalary,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=2 and [Status]=1) as HouseRent, 
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=3 and [Status]=1) as Others
                            from HRMS_Employee e 
                            inner join Designations d on e.Designation_Id=d.ID
                            inner Join Sections s on e.Section_Id=s.ID
                            where e.Present_Status=1 
                            group by e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,s.SectionName,e.Grade,e.ID
                            ) t
                            group by SL,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,BasicSalary,HouseRent,Others").ToList();
                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SL, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation, all_rows[i].SectionName,
                        all_rows[i].Grade, all_rows[i].BasicSalary, all_rows[i].GrossSalary, all_rows[i].Remarks);
                }
            }
            else
            {
                var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SL,Employee_ID,Name,Designation,SectionName,Grade,isnull(BasicSalary,0) as BasicSalary,sum(isnull(BasicSalary,0)+isnull(HouseRent,0)+isnull(Others,0)) as GrossSalary from  
                            (Select ROW_NUMBER() OVER(ORDER BY s.SectionName DESC) AS SL,e.EmployeeIdentity+' '+ e.CardTitle as Employee_ID,e.Name,
                            d.Name as Designation,s.SectionName,e.Grade,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=1 and [Status]=1) as BasicSalary,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=2 and [Status]=1) as HouseRent, 
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=3 and [Status]=1) as Others
                            from HRMS_Employee e 
                            inner join Designations d on e.Designation_Id=d.ID
                            inner Join Sections s on e.Section_Id=s.ID
                            where e.Present_Status=1 and d.ID=" + id + " group by e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,s.SectionName,e.Grade,e.ID ) t " +
                            "group by SL,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,BasicSalary,HouseRent,Others").ToList();
                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SL, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation, all_rows[i].SectionName,
                        all_rows[i].Grade, all_rows[i].BasicSalary, all_rows[i].GrossSalary, all_rows[i].Remarks);
                }
            }
            

            
            return table;
        }




        //Male Female Report

        public DataTable GradeWiseEmployeeReport(string id)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SL", typeof(string));
            table.Columns.Add("EmployeeID", typeof(string));
            table.Columns.Add("Title", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Grade", typeof(string));
            table.Columns.Add("Basic", typeof(string));
            table.Columns.Add("Gross", typeof(string));
            table.Columns.Add("Remarks", typeof(string));
            if (id == "All")
            {
                var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SL,Employee_ID,CardTitle,Name,Designation,SectionName,Grade,isnull(BasicSalary,0) as BasicSalary,sum(isnull(BasicSalary,0)+isnull(HouseRent,0)+isnull(Others,0)) as GrossSalary from  
                            (Select ROW_NUMBER() OVER(ORDER BY s.SectionName DESC) AS SL,e.EmployeeIdentity as Employee_ID, e.CardTitle as CardTitle,e.Name,
                            d.Name as Designation,s.SectionName,e.Grade,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=1 and [Status]=1) as BasicSalary,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=2 and [Status]=1) as HouseRent, 
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=3 and [Status]=1) as Others
                            from HRMS_Employee e 
                            inner join Designations d on e.Designation_Id=d.ID
                            inner Join Sections s on e.Section_Id=s.ID
                            where e.Present_Status=1 
                            group by e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,s.SectionName,e.Grade,e.ID
                            ) t group by SL,Employee_ID,CardTitle,Name,Designation,SectionName,Grade,BasicSalary,BasicSalary,HouseRent,Others
							order by CardTitle,Employee_ID").ToList();
                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SL, all_rows[i].Employee_ID, all_rows[i].CardTitle,all_rows[i].Name, all_rows[i].Designation, all_rows[i].SectionName,
                        all_rows[i].Grade, all_rows[i].BasicSalary, all_rows[i].GrossSalary, all_rows[i].Remarks);
                }
            }
            else
            {
                var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SL,Employee_ID,Name,Designation,SectionName,Grade,isnull(BasicSalary,0) as BasicSalary,sum(isnull(BasicSalary,0)+isnull(HouseRent,0)+isnull(Others,0)) as GrossSalary from  
                            (Select ROW_NUMBER() OVER(ORDER BY s.SectionName DESC) AS SL,e.EmployeeIdentity+' '+ e.CardTitle as Employee_ID,e.Name,
                            d.Name as Designation,s.SectionName,e.Grade,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=1 and [Status]=1) as BasicSalary,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=2 and [Status]=1) as HouseRent, 
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=3 and [Status]=1) as Others
                            from HRMS_Employee e 
                            inner join Designations d on e.Designation_Id=d.ID
                            inner Join Sections s on e.Section_Id=s.ID
                            where e.Present_Status=1 and e.Grade='" + id + "' group by e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,s.SectionName,e.Grade,e.ID ) t " +
                            "group by SL,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,BasicSalary,HouseRent,Others").ToList();
                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SL, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation, all_rows[i].SectionName,
                        all_rows[i].Grade, all_rows[i].BasicSalary, all_rows[i].GrossSalary, all_rows[i].Remarks);
                }
            }
            return table;
        }

        // Age Range Wise Employee Report
        public DataTable AgeRangeWiseEmployeeReport(int from, int to)
        {

            DataTable table = new DataTable();
           // table.Columns.Add("SL", typeof(string));
            table.Columns.Add("Employee ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Gross", typeof(string));
            table.Columns.Add("Joining_Date", typeof(string));
            table.Columns.Add("Date of Birth", typeof(string));
            table.Columns.Add("Year", typeof(string));
            table.Columns.Add("Month", typeof(string));
            table.Columns.Add("Day", typeof(string));
            //[SL],[Employee_ID],[Name],[Designation],[SectionName],[GrossSalary],[Joining_Date],[DOB],[Year],[Month],[Days]

        var all_rows = db.Database.SqlQuery<VM_Report>("Exec [dbo].[Sp_GetAgeWiseEmployeeList] '" + from + "','" + to + "' ").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                //all_rows[i].Id,
                table.Rows.Add( all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation, all_rows[i].SectionName,
                    all_rows[i].GrossSalary, all_rows[i].Joining_Date, all_rows[i].DOB, all_rows[i].Year, all_rows[i].Month, all_rows[i].Days);
            }
            return table;
        }


        //Salary Range Wise Report

        public DataTable SalaryRangeWiseEmployeeReport(int from, int to)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SL", typeof(string));
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Grade", typeof(string));
            table.Columns.Add("Basic", typeof(string));
            table.Columns.Add("Gross", typeof(string));
            table.Columns.Add("Remarks", typeof(string));

            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SL,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,GrossSalary,'' as Remarks From
                            (Select SL,Employee_ID,Name,Designation,SectionName,Grade,isnull(BasicSalary,0) as BasicSalary,sum(isnull(BasicSalary,0)+isnull(HouseRent,0)+isnull(Others,0)) as GrossSalary from  
                            (Select ROW_NUMBER() OVER(ORDER BY s.SectionName DESC) AS SL,e.EmployeeIdentity+' '+ e.CardTitle as Employee_ID,e.Name,
                            d.Name as Designation,s.SectionName,e.Grade,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=1 and [Status]=1) as BasicSalary,
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=2 and [Status]=1) as HouseRent, 
                            (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=3 and [Status]=1) as Others
                            from HRMS_Employee e 
                            inner join Designations d on e.Designation_Id=d.ID
                            inner Join Sections s on e.Section_Id=s.ID
                            where e.Present_Status=1 
                            group by e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,s.SectionName,e.Grade,e.ID
                            ) t
                            group by SL,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,BasicSalary,HouseRent,Others
                            ) tbl where GrossSalary between '" + from + "' and '"+to+"' " +
                            "group by SL,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,GrossSalary").ToList();
                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SL, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation, all_rows[i].SectionName,
                        all_rows[i].Grade, all_rows[i].BasicSalary, all_rows[i].GrossSalary, all_rows[i].Remarks);
                }
            return table;
        }



        // Job Duration Wise Employee Report
        public DataTable JobDurationWiseEmployeeReport(DateTime todate,int id)
        {

            DataTable table = new DataTable();
            table.Columns.Add("SL", typeof(string));
            table.Columns.Add("Employee ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Gross", typeof(string));
            table.Columns.Add("Joining_Date", typeof(string));
            table.Columns.Add("Calculate Date", typeof(string));
            table.Columns.Add("Year", typeof(string));
            table.Columns.Add("Month", typeof(string));
            table.Columns.Add("Day", typeof(string));

            var dt1 = todate.ToString("yyyy-MM-dd");
            if (id == 25000000)
            {
                var all_rows = db.Database.SqlQuery<VM_Report>($@"(Select SL,Employee_ID,Name,Designation,SectionName,sum(isnull(BasicSalary,0)+isnull(HouseRent,0)+isnull(Others,0)) as GrossSalary,
                            Joining_Date,CalculateDate,[Year],[Month],[Days] from 
                            (Select ROW_NUMBER() OVER(ORDER BY s.SectionName DESC) AS SL,e.EmployeeIdentity+' '+ e.CardTitle as Employee_ID,e.Name,
                            d.Name as Designation,s.SectionName,e.Grade,Convert(varchar(10),Joining_Date,126) as Joining_Date,'" + dt1 + "' as CalculateDate," +
                           "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=1 and [Status]=1) as BasicSalary," +
                           "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=2 and [Status]=1) as HouseRent,  " +
                           "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=3 and [Status]=1) as Others, " +
                           "(Select [dbo].[fn_Job_Duration_Year_Month_Days]('" + dt1 + "',1,e.ID)) as [Year], " +
                           "(Select [dbo].[fn_Job_Duration_Year_Month_Days]('" + dt1 + "',2,e.ID)) as [Month], " +
                           "(Select [dbo].[fn_Job_Duration_Year_Month_Days]('" + dt1 + "',3,e.ID)) as [Days] " +
                           " from HRMS_Employee e  inner join Designations d on e.Designation_Id=d.ID " +
                           " inner Join Sections s on e.Section_Id=s.ID where e.Present_Status=1  " +
                           "group by e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,s.SectionName,e.Grade,e.ID,e.Joining_Date ) t " +
                           "group by SL,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,HouseRent,Others,Joining_Date,CalculateDate,[Year],[Month],[Days])").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    //all_rows[i].Id,
                    table.Rows.Add(all_rows[i].SL, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation, all_rows[i].SectionName,
                        all_rows[i].GrossSalary, all_rows[i].Joining_Date, all_rows[i].CalculateDate, all_rows[i].Year, all_rows[i].Month, all_rows[i].Days);
                }
            }
            else
            {
                var all_rows = db.Database.SqlQuery<VM_Report>($@"(Select SL,Employee_ID,Name,Designation,SectionName,sum(isnull(BasicSalary,0)+isnull(HouseRent,0)+isnull(Others,0)) as GrossSalary,
                            Joining_Date,CalculateDate,[Year],[Month],[Days] from 
                            (Select ROW_NUMBER() OVER(ORDER BY s.SectionName DESC) AS SL,e.EmployeeIdentity+' '+ e.CardTitle as Employee_ID,e.Name,
                            d.Name as Designation,s.SectionName,e.Grade,Convert(varchar(10),Joining_Date,126) as Joining_Date,'" + dt1 + "' as CalculateDate," +
                           "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=1 and [Status]=1) as BasicSalary," +
                           "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=2 and [Status]=1) as HouseRent,  " +
                           "(select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=3 and [Status]=1) as Others, " +
                           "(Select [dbo].[fn_Job_Duration_Year_Month_Days]('" + dt1 + "',1,e.ID)) as [Year], " +
                           "(Select [dbo].[fn_Job_Duration_Year_Month_Days]('" + dt1 + "',2,e.ID)) as [Month], " +
                           "(Select [dbo].[fn_Job_Duration_Year_Month_Days]('" + dt1 + "',3,e.ID)) as [Days] " +
                           " from HRMS_Employee e  inner join Designations d on e.Designation_Id=d.ID " +
                           " inner Join Sections s on e.Section_Id=s.ID where e.Present_Status=1 and e.Section_Id='" + id + "' " +
                           "group by e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,s.SectionName,e.Grade,e.ID,e.Joining_Date ) t " +
                           "group by SL,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,HouseRent,Others,Joining_Date,CalculateDate,[Year],[Month],[Days])").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    //all_rows[i].Id,
                    table.Rows.Add(all_rows[i].SL, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation, all_rows[i].SectionName,
                        all_rows[i].GrossSalary, all_rows[i].Joining_Date, all_rows[i].CalculateDate, all_rows[i].Year, all_rows[i].Month, all_rows[i].Days);
                }

            }
            return table;
        }


        //Get Selected Employee List Report




        public DataTable GetSelectedEmployeeList(VMEmployeeReport model)
        {

            DataTable table = new DataTable();
            table.Columns.Add("SL", typeof(string));
            table.Columns.Add("Employee ID", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Date of Birth", typeof(string));
            table.Columns.Add("Joining_Date", typeof(string));
            

            if (model.DataList.Any(a => a.IsChecked == true))
            {
                int index = 0;
                foreach (var v in model.DataList.Where(a => a.IsChecked == true))
                {
                   // int index = 0;
                    var vData = from o in db.Employees
                        join p in db.Sections on o.Section_Id equals p.ID
                        join q in db.Designations on o.Designation_Id equals q.ID
                        where o.ID == v.EmployeeId
                        select new
                        {
                            EmpID = o.ID,
                            EmployeeName = o.Name,
                            CardNo = o.EmployeeIdentity,
                            CardTitle = o.CardTitle,
                            Designation = q.DesigNameBangla,
                            Section = p.SectionName,
                            Joindate = o.Joining_Date,
                            DOB = o.DOB,
                            NationalID = o.NID_No,
                            EmergencyPhone = o.Emergency_Contact_No,
                            BloodGroup = o.Blood_Group,
                            PresentAddress = o.Present_Address
                        };
                    if(vData.Any())
                        foreach (var vl in vData)
                        {
                            ++index;
                            table.Rows.Add(index,
                                vl.CardNo,
                                vl.CardTitle,
                                vl.EmployeeName,
                                vl.Designation,
                                vl.Section,
                                vl.DOB,
                                vl.Joindate
                            );
                        }
                    
                }
                return table;
            }
            return table;
        }


        #endregion

        #region Leave Report

        #region Dead Code
        public DataTable MonthlyLeaveReprt1(DateTime fromdate, DateTime todate,int eid)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Leave Name", typeof(string));
            table.Columns.Add("Leave From", typeof(string));
            table.Columns.Add("Leave To", typeof(string));
            table.Columns.Add("Approved Leave", typeof(string));
            table.Columns.Add("Total Leave", typeof(string));
            table.Columns.Add("Total Approved Leave", typeof(string));
            table.Columns.Add("Leave Balance", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            int year = fromdate.Year;
            if (eid == 10000000)
            {
                //var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
                //                                           "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
                //                                            "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
                //                                           "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
                //                                           "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                //                                           "And ls.LeaveTypeId = l.LeaveTypeId " +
                //                                           "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                //                                           "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                //                                           "inner Join Designations d on e.Designation_Id = d.ID " +
                //                                           "where  l.Entry_Date between '" + dt1 + "' and '" + dt2 + "' " +
                //                                           "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
                //                                           "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

                var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SectionName,CardNo,Name,Designation_Name,Joining_Date,Leave_Name,Leave_From,Leave_To,Approved_Days,
                                        Total_Leave, Total_Approved_Leave,Leave_Balance from 
                                        (Select l.ID, l.EmployeeId,l.LeaveTypeId,
                                        s.SectionName as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name,
                                        d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name,
                                         convert(varchar(10),l.[From],126)as Leave_From, convert(varchar(10),l.[To],126) as Leave_To,
                                        l.[Days] as Approved_Days,
                                        (Select  ls.Total from HRMS_Leave_Assign ls where  ls.LeaveTypeId=l.LeaveTypeId and ls.[Year]='" + year + "' " +
                                                               "and ls.EmployeeId = l.EmployeeId and ls.LeaveTypeId = l.leaveTypeId) as Total_Leave, " +
                                                               "(Select  ls.Taken from HRMS_Leave_Assign ls where ls.LeaveTypeId = l.LeaveTypeId and ls.[Year] = '"+year+"' " +
                                                               "and ls.EmployeeId = l.EmployeeId and ls.LeaveTypeId = l.leaveTypeId) as Total_Approved_Leave, " +
                                                               "(Select  ls.Remaining from HRMS_Leave_Assign ls where ls.LeaveTypeId = l.LeaveTypeId and ls.[Year]='"+year+"' " +
                                                               "and ls.EmployeeId=l.EmployeeId and ls.LeaveTypeId= l.leaveTypeId) as Leave_Balance " +
                                                               "from HRMS_Leave_Application l " +
                                                               "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                               "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                               "inner Join Designations d on e.Designation_Id = d.ID " +
                                                               "inner Join Sections s on e.Section_Id = s.ID " +
                                                               "where ((convert(varchar(10),l.[From],126) BETWEEN '" + dt1 + "'AND '"+ dt2 + "') OR(convert(varchar(10),l.[To],126) BETWEEN '"+dt1+"'AND '"+dt2+"') OR " +
                                        "(convert(varchar(10),l.[From],126) <= '"+ dt1 + "' AND convert(varchar(10),l.[To],126) >= '"+ dt2 + "'))and l.Status=1 ) Tbl1  ").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
                }
            }
            else
            {

                var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SectionName,CardNo,Name,Designation_Name,Joining_Date,Leave_Name,Leave_From,Leave_To,Approved_Days,
                                        Total_Leave, Total_Approved_Leave,Leave_Balance from 
                                        (Select l.ID, l.EmployeeId,l.LeaveTypeId,
                                        s.SectionName  as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name,
                                        d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name,
                                         convert(varchar(10),l.[From],126)as Leave_From, convert(varchar(10),l.[To],126) as Leave_To,
                                        l.[Days] as Approved_Days,
                                        (Select  ls.Total from HRMS_Leave_Assign ls where  ls.LeaveTypeId=l.LeaveTypeId and ls.[Year]='" + year + "' " +
                                                               "and ls.EmployeeId = l.EmployeeId and ls.LeaveTypeId = l.leaveTypeId) as Total_Leave, " +
                                                               "(Select  ls.Taken from HRMS_Leave_Assign ls where ls.LeaveTypeId = l.LeaveTypeId and ls.[Year] = '" + year + "' " +
                                                               "and ls.EmployeeId = l.EmployeeId and ls.LeaveTypeId = l.leaveTypeId) as Total_Approved_Leave, " +
                                                               "(Select  ls.Remaining from HRMS_Leave_Assign ls where ls.LeaveTypeId = l.LeaveTypeId and ls.[Year]='" + year + "' " +
                                                               "and ls.EmployeeId=l.EmployeeId and ls.LeaveTypeId= l.leaveTypeId) as Leave_Balance " +
                                                               "from HRMS_Leave_Application l " +
                                                               "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                               "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                               "inner Join Designations d on e.Designation_Id = d.ID " +
                                                               "inner Join Sections s on e.Section_Id = s.ID " +
                                                               "where   ((convert(varchar(10),l.[From],126) BETWEEN '" + dt1 + "'AND '" + dt2 + "') OR (convert(varchar(10),l.[To],126) BETWEEN '" + dt1 + "'AND '" + dt2 + "') OR " +
                                        "(convert(varchar(10),l.[From],126) <= '" + dt1 + "' AND convert(varchar(10),l.[To],126) >= '" + dt2 + "')) and e.ID=" + eid + " and l.Status=1 ) Tbl1 ").ToList();


                //var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
                //                                           "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
                //                                            "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
                //                                           "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
                //                                           "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                //                                           "And ls.LeaveTypeId = l.LeaveTypeId " +
                //                                           "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                //                                           "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                //                                           "inner Join Designations d on e.Designation_Id = d.ID " +
                //                                           "where  l.Entry_Date between '" + dt1 + "' and '" + dt2 + "' and e.ID="+eid+" " +
                //                                           "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
                //                                           "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
                }

            }
            return table;

            //var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
            //                                                   "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
            //                                                    "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
            //                                                   "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
            //                                                   "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
            //                                                   "And ls.LeaveTypeId = l.LeaveTypeId " +
            //                                                   "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
            //                                                   "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
            //                                                   "inner Join Designations d on e.Designation_Id = d.ID " +
            //                                                   "where  l.Entry_Date between '"+dt1+"' and '"+dt2+"' " +
            //                                                   "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
            //                                                   "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

            //    for (int i = 0; i < all_rows.Count; i++)
            //    {
            //        table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
            //            , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
            //    }

        }
        #endregion
        #region Dead Code
        public DataTable SectionWiseMonthlyLeaveReprt1(DateTime fromdate, DateTime todate, int sid)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Leave Name", typeof(string));
            table.Columns.Add("Leave From", typeof(string));
            table.Columns.Add("Leave To", typeof(string));
            table.Columns.Add("Approved Leave", typeof(string));
            table.Columns.Add("Total Leave", typeof(string));
            table.Columns.Add("Total Approved Leave", typeof(string));
            table.Columns.Add("Leave Balance", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            int year = fromdate.Year;
            if (sid == 25000000)
            {
                var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SectionName,CardNo,Name,Designation_Name,Joining_Date,Leave_Name,Leave_From,Leave_To,Approved_Days,
                                        Total_Leave, Total_Approved_Leave,Leave_Balance from 
                                        (Select l.ID, l.EmployeeId,l.LeaveTypeId,
                                        s.SectionName as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name,
                                        d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name,
                                         convert(varchar(10),l.[From],126)as Leave_From, convert(varchar(10),l.[To],126) as Leave_To,
                                        l.[Days] as Approved_Days,
                                        (Select  ls.Total from HRMS_Leave_Assign ls where  ls.LeaveTypeId=l.LeaveTypeId and ls.[Year]='" + year + "' " +
                                                              "and ls.EmployeeId = l.EmployeeId and ls.LeaveTypeId = l.leaveTypeId) as Total_Leave, " +
                                                              "(Select  ls.Taken from HRMS_Leave_Assign ls where ls.LeaveTypeId = l.LeaveTypeId and ls.[Year] = '" + year + "' " +
                                                              "and ls.EmployeeId = l.EmployeeId and ls.LeaveTypeId = l.leaveTypeId) as Total_Approved_Leave, " +
                                                              "(Select  ls.Remaining from HRMS_Leave_Assign ls where ls.LeaveTypeId = l.LeaveTypeId and ls.[Year]='" + year + "' " +
                                                              "and ls.EmployeeId=l.EmployeeId and ls.LeaveTypeId= l.leaveTypeId) as Leave_Balance " +
                                                              "from HRMS_Leave_Application l " +
                                                              "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                              "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                              "inner Join Designations d on e.Designation_Id = d.ID " +
                                                              "inner Join Sections s on e.Section_Id = s.ID " +
                                                              "where ((convert(varchar(10),l.[From],126) BETWEEN '" + dt1 + "'AND '" + dt2 + "') OR(convert(varchar(10),l.[To],126) BETWEEN '" + dt1 + "'AND '" + dt2 + "') OR " +
                                       "(convert(varchar(10),l.[From],126) <= '" + dt1 + "' AND convert(varchar(10),l.[To],126) >= '" + dt2 + "')) and l.Status=1 ) Tbl1  ").ToList();
                //var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
                //                                           "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
                //                                            "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
                //                                           "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
                //                                           "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                //                                           "And ls.LeaveTypeId = l.LeaveTypeId " +
                //                                           "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                //                                           "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                //                                           "inner Join Designations d on e.Designation_Id = d.ID " +
                //                                           "where  l.Entry_Date between '" + dt1 + "' and '" + dt2 + "'" +
                //                                           "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
                //                                           "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
                }
            }
            else
            {

                var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SectionName,CardNo,Name,Designation_Name,Joining_Date,Leave_Name,Leave_From,Leave_To,Approved_Days,
                                        Total_Leave, Total_Approved_Leave,Leave_Balance from 
                                        (Select l.ID, l.EmployeeId,l.LeaveTypeId,
                                        s.SectionName as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name,
                                        d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name,
                                         convert(varchar(10),l.[From],126)as Leave_From, convert(varchar(10),l.[To],126) as Leave_To,
                                        l.[Days] as Approved_Days,
                                        (Select  ls.Total from HRMS_Leave_Assign ls where  ls.LeaveTypeId=l.LeaveTypeId and ls.[Year]='" + year + "' " +
                                                              "and ls.EmployeeId = l.EmployeeId and ls.LeaveTypeId = l.leaveTypeId) as Total_Leave, " +
                                                              "(Select  ls.Taken from HRMS_Leave_Assign ls where ls.LeaveTypeId = l.LeaveTypeId and ls.[Year] = '" + year + "' " +
                                                              "and ls.EmployeeId = l.EmployeeId and ls.LeaveTypeId = l.leaveTypeId) as Total_Approved_Leave, " +
                                                              "(Select  ls.Remaining from HRMS_Leave_Assign ls where ls.LeaveTypeId = l.LeaveTypeId and ls.[Year]='" + year + "' " +
                                                              "and ls.EmployeeId=l.EmployeeId and ls.LeaveTypeId= l.leaveTypeId) as Leave_Balance " +
                                                              "from HRMS_Leave_Application l " +
                                                              "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                              "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                              "inner Join Designations d on e.Designation_Id = d.ID " +
                                                              "inner Join Sections s on e.Section_Id = s.ID " +
                                                              "where   ((convert(varchar(10),l.[From],126) BETWEEN '" + dt1 + "'AND '" + dt2 + "') OR (convert(varchar(10),l.[To],126) BETWEEN '" + dt1 + "'AND '" + dt2 + "') OR " +
                                       "(convert(varchar(10),l.[From],126) <= '" + dt1 + "' AND convert(varchar(10),l.[To],126) >= '" + dt2 + "')) and e.Section_Id=" + sid + " and l.Status=1 ) Tbl1 ").ToList();
                //var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
                //                                           "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
                //                                            "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
                //                                           "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
                //                                           "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                //                                           "And ls.LeaveTypeId = l.LeaveTypeId " +
                //                                           "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                //                                           "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                //                                           "inner Join Designations d on e.Designation_Id = d.ID " +
                //                                           "where  l.Entry_Date between '" + dt1 + "' and '" + dt2 + "' and e.Section_Id=" + sid + " " +
                //                                           "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
                //                                           "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
                }

            }
            return table;


        }
        #endregion
        public DataTable MonthlyLeaveReprt(DateTime fromdate, DateTime todate, int eid)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Leave Name", typeof(string));
            table.Columns.Add("Leave From", typeof(string));
            table.Columns.Add("Leave To", typeof(string));
            table.Columns.Add("Approved Leave", typeof(string));
            table.Columns.Add("Total Leave", typeof(string));
            table.Columns.Add("Total Approved Leave", typeof(string));
            table.Columns.Add("Leave Balance", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            int year = fromdate.Year;
            if (eid == 10000000)
            {
                var all_rows = (from t1 in db.HrmsLeaveApplications
                                join t2 in db.Employees on t1.EmployeeId equals t2.ID
                                join t3 in db.Sections on t2.Section_Id equals t3.ID
                                join t4 in db.Designations on t2.Designation_Id equals t4.ID
                                join t5 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t5.ID
                                where t1.From >= fromdate && t1.To <= todate && t1.Status == true
                                select new VM_Report
                                {
                                    SectionName = t3.SectionName,
                                    CardNo = t2.EmployeeIdentity,
                                    Name = t2.Name,
                                    Designation_Name = t4.Name,
                                    JoinDate = t2.Joining_Date,
                                    Leave_Name = t5.Leave_Name,
                                    Fromdate = t1.From,
                                    Todate = t1.To,
                                    Approved_Days = t1.Days,
                                    Total_Leave = t5.Leave_Amount,
                                    PreviousLeaveTaken = db.HrmsLeaveApplications.Where(a => a.From < t1.From && a.Status == true && a.EmployeeId == t1.EmployeeId && a.LeaveTypeId == t1.LeaveTypeId && a.From.Year == Todate.Year).ToList().Any() == true ?db.HrmsLeaveApplications.Where(a => a.From < t1.From && a.Status == true && a.EmployeeId == t1.EmployeeId && a.LeaveTypeId == t1.LeaveTypeId && a.From.Year == Todate.Year).ToList().Sum(a => a.Days) : 0
                                    

                                }).OrderBy(a => a.Fromdate).ToList();

                if (all_rows.Any())
                {
                    all_rows.ForEach(v => {

                        v.Joining_Date = v.JoinDate.ToString("yyyy-MM-dd");
                        v.Leave_From = v.Fromdate.ToString("yyyy-MM-dd");
                        v.Leave_To = v.Todate.ToString("yyyy-MM-dd");
                        v.Total_Approved_Leave = v.Approved_Days + v.PreviousLeaveTaken;
                        v.Leave_Balance = v.Total_Leave - v.Total_Approved_Leave;
                        table.Rows.Add(v.SectionName, v.CardNo, v.Name, v.Designation_Name, v.Joining_Date, v.Leave_Name, v.Leave_From, v.Leave_To
                        , v.Approved_Days, v.Total_Leave, v.Total_Approved_Leave, v.Leave_Balance);
                    });
                }




            }
            else
            {
                var all_rows = (from t1 in db.HrmsLeaveApplications
                                join t2 in db.Employees on t1.EmployeeId equals t2.ID
                                join t3 in db.Sections on t2.Section_Id equals t3.ID
                                join t4 in db.Designations on t2.Designation_Id equals t4.ID
                                join t5 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t5.ID
                                where t1.From >= fromdate && t1.To <= todate && t1.Status == true && t2.ID == eid
                                select new VM_Report
                                {
                                    SectionName = t3.SectionName,
                                    CardNo = t2.EmployeeIdentity,
                                    Name = t2.Name,
                                    Designation_Name = t4.Name,
                                    JoinDate = t2.Joining_Date,
                                    Leave_Name = t5.Leave_Name,
                                    Fromdate = t1.From,
                                    Todate = t1.To,
                                    Approved_Days = t1.Days,
                                    Total_Leave = t5.Leave_Amount,
                                    PreviousLeaveTaken = db.HrmsLeaveApplications.Where(a => a.From < t1.From && a.Status == true && a.EmployeeId == t1.EmployeeId && a.LeaveTypeId == t1.LeaveTypeId && a.From.Year == Todate.Year).ToList().Any() == true ? db.HrmsLeaveApplications.Where(a => a.From < t1.From && a.Status == true && a.EmployeeId == t1.EmployeeId && a.LeaveTypeId == t1.LeaveTypeId && a.From.Year == Todate.Year).ToList().Sum(a => a.Days) : 0

                                }).OrderBy(a => a.Fromdate).ToList();

                if (all_rows.Any())
                {
                    all_rows.ForEach(v => {

                        v.Joining_Date = v.JoinDate.ToString("yyyy-MM-dd");
                        v.Leave_From = v.Fromdate.ToString("yyyy-MM-dd");
                        v.Leave_To = v.Todate.ToString("yyyy-MM-dd");
                        v.Total_Approved_Leave = v.Approved_Days + v.PreviousLeaveTaken;
                        v.Leave_Balance = v.Total_Leave - v.Total_Approved_Leave;
                        table.Rows.Add(v.SectionName, v.CardNo, v.Name, v.Designation_Name, v.Joining_Date, v.Leave_Name, v.Leave_From, v.Leave_To, v.Approved_Days, v.Total_Leave, v.Total_Approved_Leave, v.Leave_Balance);
                    });
                }

            
        }
            return table;

           

        }

  

        public DataTable SectionWiseMonthlyLeaveReprt(DateTime fromdate, DateTime todate, int sid)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Leave Name", typeof(string));
            table.Columns.Add("Leave From", typeof(string));
            table.Columns.Add("Leave To", typeof(string));
            table.Columns.Add("Approved Leave", typeof(string));
            table.Columns.Add("Total Leave", typeof(string));
            table.Columns.Add("Total Approved Leave", typeof(string));
            table.Columns.Add("Leave Balance", typeof(string));

            if (sid == 25000000)
            {
                var all_rows = (from t1 in db.HrmsLeaveApplications
                                join t2 in db.Employees on t1.EmployeeId equals t2.ID
                                join t3 in db.Sections on t2.Section_Id equals t3.ID
                                join t4 in db.Designations on t2.Designation_Id equals t4.ID
                                join t5 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t5.ID
                                where t1.From >= fromdate && t1.To <= todate && t1.Status==true
                                select new VM_Report
                                {
                                    SectionName=t3.SectionName,
                                    CardNo = t2.EmployeeIdentity,
                                    Name =t2.Name,
                                    Designation_Name=t4.Name,
                                    JoinDate=t2.Joining_Date,
                                    Leave_Name=t5.Leave_Name,
                                    Fromdate=t1.From,
                                    Todate=t1.To,
                                    Approved_Days=t1.Days,
                                    Total_Leave=t5.Leave_Amount,
                                    PreviousLeaveTaken = db.HrmsLeaveApplications.Where(a => a.From < t1.From && a.Status == true && a.EmployeeId == t1.EmployeeId && a.LeaveTypeId == t1.LeaveTypeId && a.From.Year == Todate.Year).ToList().Any() == true ? db.HrmsLeaveApplications.Where(a => a.From < t1.From && a.Status == true && a.EmployeeId == t1.EmployeeId && a.LeaveTypeId == t1.LeaveTypeId && a.From.Year == Todate.Year).ToList().Sum(a => a.Days) : 0


                                }).OrderBy(a => a.Fromdate).ToList();

                if (all_rows.Any())
                {
                    all_rows.ForEach(v => {

                        v.Joining_Date=v.JoinDate.ToString("yyyy-MM-dd");
                        v.Leave_From=v.Fromdate.ToString("yyyy-MM-dd");
                        v.Leave_To=v.Todate.ToString("yyyy-MM-dd");
                        v.Total_Approved_Leave = v.Approved_Days + v.PreviousLeaveTaken;
                        v.Leave_Balance = v.Total_Leave - v.Total_Approved_Leave;
                        table.Rows.Add(v.SectionName, v.CardNo, v.Name, v.Designation_Name, v.Joining_Date, v.Leave_Name, v.Leave_From, v.Leave_To
                        , v.Approved_Days, v.Total_Leave, v.Total_Approved_Leave, v.Leave_Balance);
                    });
                }
            }
            else
            {
                var all_rows = (from t1 in db.HrmsLeaveApplications
                                join t2 in db.Employees on t1.EmployeeId equals t2.ID
                                join t3 in db.Sections on t2.Section_Id equals t3.ID
                                join t4 in db.Designations on t2.Designation_Id equals t4.ID
                                join t5 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t5.ID
                                where t1.From >= fromdate && t1.To <= todate && t1.Status == true  && t2.Section_Id == sid
                                select new VM_Report
                                {
                                    SectionName = t3.SectionName,
                                    CardNo = t2.EmployeeIdentity,
                                    Name = t2.Name,
                                    Designation_Name = t4.Name,
                                    JoinDate = t2.Joining_Date,
                                    Leave_Name = t5.Leave_Name,
                                    Fromdate = t1.From,
                                    Todate = t1.To,
                                    Approved_Days = t1.Days,
                                    Total_Leave = t5.Leave_Amount,
                                    PreviousLeaveTaken = db.HrmsLeaveApplications.Where(a => a.From < t1.From && a.Status == true && a.EmployeeId == t1.EmployeeId && a.LeaveTypeId == t1.LeaveTypeId && a.From.Year == Todate.Year).ToList().Any()==true? db.HrmsLeaveApplications.Where(a => a.From < t1.From && a.Status == true && a.EmployeeId == t1.EmployeeId && a.LeaveTypeId == t1.LeaveTypeId && a.From.Year == Todate.Year).ToList().Sum(a=>a.Days):0

                                }).OrderBy(a=>a.Fromdate).ToList();

                if (all_rows.Any())
                {
                    all_rows.ForEach(v => {

                        v.Joining_Date = v.JoinDate.ToString("yyyy-MM-dd");
                        v.Leave_From = v.Fromdate.ToString("yyyy-MM-dd");
                        v.Leave_To = v.Todate.ToString("yyyy-MM-dd");
                        v.Total_Approved_Leave = v.Approved_Days + v.PreviousLeaveTaken;
                        v.Leave_Balance = v.Total_Leave - v.Total_Approved_Leave;
                        table.Rows.Add(v.SectionName, v.CardNo, v.Name, v.Designation_Name, v.Joining_Date, v.Leave_Name, v.Leave_From, v.Leave_To, v.Approved_Days, v.Total_Leave, v.Total_Approved_Leave, v.Leave_Balance);
                    });
                }

            }
            return table;
        }


        public DataTable MeternityMonthlyLeaveReprt(DateTime fromdate, DateTime todate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Leave Name", typeof(string));
            table.Columns.Add("Leave From", typeof(string));
            table.Columns.Add("Leave To", typeof(string));
            table.Columns.Add("Approved Leave", typeof(string));
            table.Columns.Add("Drawn Salary", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
           
                var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, e.CardTitle, e.EmployeeIdentity as CardNo, e.Name, d.Name, convert(varchar(10), e.Joining_date, 126), lp.Leave_Name," +
                                                               " convert(varchar(10), l.[From], 126) as Leave_From, convert(varchar(10), l.[To], 126) as Leave_To, l.[Days] as Approved_Days,'' as DrawnSalary from HRMS_Leave_Application l " +
                                                               "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                                                               "And ls.LeaveTypeId = l.LeaveTypeId " +
                                                               "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                               "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                               "inner Join Designations d on e.Designation_Id = d.ID " +
                                                               "inner Join Sections s on e.Section_Id = s.ID " +
                                                               "where  l.Entry_Date between '" +dt1+"' and '"+dt2+ "' and l.LeaveTypeId=196  " +
                                                               "group by l.EmployeeId, l.[From], l.[To], l.[Days],s.SectionName, " +
                                                               "e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].DrawnSalary);
                }
           
            return table;


        }




        public DataTable ResignationEarnLeaveReprt(int eid)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Total Month", typeof(string));
            table.Columns.Add("Working Days", typeof(string));
            table.Columns.Add("F_Leave", typeof(string));
            table.Columns.Add("Holiday", typeof(string));
            table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("Absent", typeof(string));
            table.Columns.Add("TotalDeductionDays", typeof(string));
            table.Columns.Add("TotalWorkingDays", typeof(string));
            table.Columns.Add("TotalEarnLeave", typeof(string));
            table.Columns.Add("EnjoyableLeave", typeof(string));
            table.Columns.Add("PayableEarnLeave", typeof(string));
            table.Columns.Add("Stamp", typeof(string));
            table.Columns.Add("OneDaySalary", typeof(string));
            table.Columns.Add("Present_Salary", typeof(string));
            table.Columns.Add("PayableEarnLeaveTK", typeof(string));
            table.Columns.Add("Signature", typeof(string));
            //SectionName, CardTitle, Name, Designation, Joining_Date, QuitDate,
            //Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent],TotalDeductionDays,
            //TotalWorkingDays,TotalEarnLeave,EnjoyableLeave,PayableEarnLeave,Stamp,OneDaySalary,PayableEarnLeaveTK,Signature,Present_Salary
            //var dt1 = fromdate.ToString("yyyy-MM-dd");
            //var dt2 = todate.ToString("yyyy-MM-dd");

            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SectionName, CardTitle,EmployeeIdentity as CardNo, Name, Designation, Joining_Date, QuitDate,
                                                        cast(Total_Month as int) as Total_Month,cast(Working_Days as int) as Working_Days, 
                                                        cast(F_Leave as int) as F_Leave,cast(Holiday as int) as Holiday, 
                                                        cast(Leave as decimal(18,2)) as Total_Leave,
                                                        cast(Absent as int) as [Absent],cast(TotalDeductionDays as int) as TotalDeductionDays, 
                                                        cast(TotalWorkingDays as int) as TotalWorkingDays, cast(sum(TotalWorkingDays / 18)as decimal(18,2)) as TotalEarnLeave, 
                                                        cast(0 as int) as EnjoyableLeave, cast(sum(TotalWorkingDays / 18) as decimal(18,2)) as PayableEarnLeave,
                                                        cast(Stamp as int)as Stamp,cast(Present_Salary as decimal(18,2))as Present_Salary,
                                                        cast(OneDaySalary as decimal(18,2))as OneDaySalary, 
                                                        cast(sum(TotalWorkingDays / 18 * OneDaySalary) as decimal(18,2)) as PayableEarnLeaveTK, '' As [Signature] 
                                                        from( Select SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, QuitDate, 
                                                        Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent], 
                                                        sum(F_Leave + Holiday + Leave + [Absent]) as TotalDeductionDays, 
                                                        sum(Working_Days - (F_Leave + Holiday + Leave + [Absent])) as TotalWorkingDays,Present_Salary, 
                                                        sum(Present_Salary / 30) as OneDaySalary, Stamp from 
                                                        (Select s.SectionName, e.CardTitle,e.EmployeeIdentity, e.Name, d.Name as Designation, 
                                                        convert(varchar(10), Joining_Date, 126) as Joining_Date,
                                                        Case when  convert(varchar(10), QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), QuitDate, 126) end as  QuitDate,
                                                         ((SELECT DATEDIFF(mm, e.Joining_Date, Case when  convert(varchar(10), QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), QuitDate, 126) end ) + 1 from HRMS_Employee ee where ee.ID = e.ID)) as Total_Month, 
                                                         ((SELECT DATEDIFF(dd, e.Joining_Date, Case when  convert(varchar(10), QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), QuitDate, 126) end) + 1 from HRMS_Employee ee where ee.ID = e.ID)) as Working_Days, 
                                                         (Select count(1) from HRMS_Holiday hd, HRMS_Employee eee where hd.[Description] Like '%Festival%' and hd.OffDay Between eee.Joining_date and Case when  convert(varchar(10), eee.QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), eee.QuitDate, 126) end and eee.ID= e.ID) as F_Leave, 
                                                         (Select count(1) from HRMS_Attendance_History att inner join HRMS_Employee ate on att.EmployeeId=ate.ID where att.AttendanceStatus=5 and att.[Date] Between ate.Joining_date and Case when  convert(varchar(10), ate.QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), ate.QuitDate, 126) end and ate.ID=e.ID) as Holiday, 
                                                         (Select Isnull(sum(Taken),0) from HRMS_Leave_Assign l where  l.EmployeeId=e.ID) as Leave, 
                                                         (Select count(1) from HRMS_Attendance_History att inner join HRMS_Employee ate on att.EmployeeId=ate.ID where att.AttendanceStatus=3 and att.[Date] Between ate.Joining_date and Case when  convert(varchar(10), ate.QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), ate.QuitDate, 126) end and ate.ID= e.ID) as [Absent], 
                                                         (Select isnull(sum(Basic+House+Health+Food+Transport),0) from HRMS_Salary s where  s.Employee_Id=e.ID) as Present_Salary,10 as Stamp 
                                                         from HRMS_Employee e join Sections s on e.Section_Id=s.Id inner join Designations d on e.Designation_Id=d.ID where e.ID="+eid+" )t " +
                                                           "group by SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, QuitDate, Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent], Present_Salary, Stamp ) tbl  " +
                                                           "group by SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, QuitDate, Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent], Present_Salary, Stamp, TotalDeductionDays, TotalWorkingDays, OneDaySalary ").ToList();
               
            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, 
                    all_rows[i].Total_Month, all_rows[i].Working_Days, all_rows[i].F_Leave, all_rows[i].Holiday, 
                    all_rows[i].Leave, all_rows[i].Absent, all_rows[i].TotalDeductionDays, all_rows[i].TotalWorkingDays, 
                    all_rows[i].EnjoyableLeave, all_rows[i].PayableEarnLeave, all_rows[i].Stamp, all_rows[i].Present_Salary, 
                    all_rows[i].OneDaySalary, all_rows[i].PayableEarnLeaveTK, all_rows[i].Signature);
            }
            return table;


        }

        #endregion


        #region Job Age Calculation
       public void JobAgeCalculate(DateTime joinDate,DateTime todate)
        {
            int Years = new DateTime(Todate.Subtract(joinDate).Ticks).Year - 1;
            DateTime PastYearDate = joinDate.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (PastYearDate.AddMonths(i) == Todate)
                {
                    Months = i;
                    break;
                }
                else if (PastYearDate.AddMonths(i) >= Todate)
                {
                    Months = i - 1;
                    break;
                }
            }

            this.Year = Years;
            this.Month = Months;
         


        }

        #endregion




    }
}