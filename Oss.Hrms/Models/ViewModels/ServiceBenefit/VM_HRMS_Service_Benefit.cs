﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;

namespace Oss.Hrms.Models.ViewModels.ServiceBenefit
{
    public class VM_HRMS_Service_Benefit
    {

        private HrmsContext db = new HrmsContext();

        public List<VM_HRMS_Service_Benefit> DataList { get; set; }

        [DisplayName("Employee ID")]
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DisplayName("Year")]
        public int Year { get; set; }

        [DisplayName("Month")]
        public int Month { get; set; }

        [DisplayName("Days")]
        public int Days { get; set; }

        [DisplayName("Payable Service Benefit Days")]
        public int PayableDays { get; set; }
        [DisplayName("Present Salary")]
        public decimal PresentSalary { get; set; }
        [DisplayName("Present Basic")]
        public decimal PresentBasic { get; set; }

        [DisplayName("Basic Per Day")]
        public decimal BasicPerDay { get; set; }

        [DisplayName("Stamp")]
        public decimal Stamp { get; set; }

        [DisplayName("Total Amount")]
        public decimal CalculatedTotalAmount { get; set; }

        [DisplayName("Payable Amount")]
        public decimal PayableTotalAmount { get; set; }
        [DisplayName("Payment Date")]
       // [DataType(DataType.DateTime)]
        public string PaymentDate { get; set; }


        [DisplayName("Card Title")]
        public string CardTitle { get; set; }

        [DisplayName("Section Name")]
        public string SectionName { get; set; }

        [DisplayName("Employee ID")]
        public string EmployeeIdentity { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Designation")]
        public string Designation { get; set; }

        [DisplayName("Joining Date")]
        public string Joining_Date { get; set; }

        [DisplayName("Resignation Date")]
        public string Resignation_Date { get; set; }




        [DisplayName("To Date")]
        [Required(ErrorMessage = "To Date is Required")]
        //[StringLength(80, MinimumLength = 2, ErrorMessage = "Job Title should be minimum 2 characters and Maximum 100 characters")]
        [DataType(DataType.Date)]
        public DateTime Fromdate { get; set; }

        [DisplayName("From Date")]
        [Required(ErrorMessage = "From Date is Required")]
        //[StringLength(80, MinimumLength = 2, ErrorMessage = "Job Title should be minimum 2 characters and Maximum 100 characters")]
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }

        public bool IsAudit { get; set; }

        #region Service Benefit Original

        public VM_HRMS_Service_Benefit ServiceBenefitCalculation(VM_HRMS_Service_Benefit model)
        {
            var serviceBenefit = new VM_HRMS_Service_Benefit();
            // var dt1 = model.ToDate.ToString("yyyy-MM-dd");

            var serviceBenefitInfo = db.Database.SqlQuery<VM_HRMS_Service_Benefit>($@"Select ID,Employee_ID as EmployeeIdentity,Name,Designation,SectionName,
                        Joining_Date,Resignation_Date,[Year],[Month],[Days],[PayableDays],sum(isnull(BasicSalary,0)+isnull(HouseRent,0)+isnull(Others,0)) as PresentSalary,Isnull(BasicSalary,0) as [PresentBasic],(Isnull([BasicSalary],0)/30) as BasicPerDay,sum(Isnull([PayableDays],0)*(Isnull([BasicSalary],0)/30)) as CalculatedTotalAmount, isnull(Stamp,0) as Stamp,sum(Isnull([PayableDays],0)*(Isnull([BasicSalary],0)/30)-isnull(Stamp,0)) as PayableTotalAmount  from 
                        (Select e.ID,e.EmployeeIdentity+' '+ e.CardTitle as Employee_ID,e.Name,
                        d.Name as Designation,s.SectionName,e.Grade,Convert(varchar(10),Joining_Date,126) as Joining_Date,Convert(varchar(10),e.QuitDate,126) as Resignation_Date,
                        (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=1 and [Status]=1) as BasicSalary,
                        (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=2 and [Status]=1) as HouseRent,
                        (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=3 and [Status]=1) as Others,
                        (select TOP 1 (ActualAmount) from HRMS_EodRecord where EmployeeID=e.ID and Eod_RefFk=12 and [Status]=1) as Stamp,
                        (Select [dbo].[fn_Job_Duration_Year_Month_Days](Convert(varchar(10),e.QuitDate,126),1,e.ID)) as [Year],
                        (Select [dbo].[fn_Job_Duration_Year_Month_Days](Convert(varchar(10),e.QuitDate,126),2,e.ID)) as [Month], 
                        (Select [dbo].[fn_Job_Duration_Year_Month_Days](Convert(varchar(10),e.QuitDate,126),3,e.ID)) as [Days],
                        (Select [dbo].[fn_PayableServiceBenefitDays](e.ID)) as [PayableDays]
                        from HRMS_Employee e  inner join Designations d on e.Designation_Id=d.ID 
                        inner Join Sections s on e.Section_Id=s.ID where  e.ID=" + model.EmployeeId + "  " + "and (DATEDIFF(dd,e.Joining_Date,(Case when(e.QuitDate='1900-01-01') THEN e.Joining_Date ELSE e.QuitDate END)) + 1)>=1826 " +
                        "group by e.ID,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,s.SectionName,e.Grade,e.ID,e.Joining_Date,e.QuitDate ) t " +
                        "group by ID,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,HouseRent,Others,Joining_Date,Resignation_Date,[Year],[Month],[Days],[PayableDays],Stamp").ToList();

            if (serviceBenefitInfo.Any())
            {
                serviceBenefit.DataList = serviceBenefitInfo;
            }
            return serviceBenefit;
        }


        public void ServiceBenefitCalculationSave(VM_HRMS_Service_Benefit model)
        {
            var ds = 0;
            ds = db.Database.ExecuteSqlCommand("[dbo].[Sp_EmployeeServiceBenefitCalculationAndSave] @eid=" + model.EmployeeId + ",@isAudit=" + model.IsAudit +"");
            if (ds > 0)
            {
                var vl = 1;
            }

        }

        public VM_HRMS_Service_Benefit ViewServiceBenfitInformation(VM_HRMS_Service_Benefit model)
        {
            var ServiceBenefit = new VM_HRMS_Service_Benefit();
            var dt1 = model.Fromdate.ToString("yyyy-MM-dd");
            var dt2 = model.ToDate.ToString("yyyy-MM-dd");
            int t = 0;
            if (model.IsAudit == false)
            {
                t = 0;
            }
            else if (model.IsAudit == true)
            {
                t = 1;
            }
            var ServiceBenefitInfo = db.Database.SqlQuery<VM_HRMS_Service_Benefit>($@"Select hsb.[EmployeeId],s.SectionName,e.EmployeeIdentity,e.Name,d.Name as Designation,Convert(varchar(10),e.Joining_Date,126) as Joining_Date,Convert(varchar(10),e.QuitDate,126) as Resignation_Date,hsb.[Year],hsb.[Month],hsb.[Days],hsb.[PayableDays],hsb.[PresentSalary],hsb.[PresentBasic],(hsb.[PresentBasic]/30) as BasicPerDay, hsb.[Stamp],hsb.[CalculatedTotalAmount],hsb.[PayableTotalAmount],Convert(varchar(10),hsb.[PaymentDate],126) as PaymentDate   from HRMS_Service_Benefit hsb
                    inner Join HRMS_Employee e on e.ID=hsb.EmployeeId
                    inner join Sections s on e.Section_Id=s.ID
                    inner Join Designations d on e.Designation_Id=d.ID
                    where Convert(varchar(10),hsb.PaymentDate,126) between'" + dt1 + "' and '" + dt2 + "' and  hsb.IsAudit="+ t + "").ToList();

            if (ServiceBenefitInfo.Any())
            {
                ServiceBenefit.DataList = ServiceBenefitInfo;
            }
            return ServiceBenefit;
        }

        #endregion

        #region Service Benefit for Audit


        public VM_HRMS_Service_Benefit ServiceBenefitACalculation(VM_HRMS_Service_Benefit model)
        {
            var serviceBenefit = new VM_HRMS_Service_Benefit();
            // var dt1 = model.ToDate.ToString("yyyy-MM-dd");

            var serviceBenefitInfo = db.Database.SqlQuery<VM_HRMS_Service_Benefit>($@"Select ID,Employee_ID as EmployeeIdentity,Name,Designation,SectionName,
                        Joining_Date,Resignation_Date,[Year],[Month],[Days],[PayableDays],sum(isnull(BasicSalary,0)+isnull(HouseRent,0)+isnull(Others,0)) as PresentSalary,Isnull(BasicSalary,0) as [PresentBasic],(Isnull([BasicSalary],0)/30) as BasicPerDay,sum(Isnull([PayableDays],0)*(Isnull([BasicSalary],0)/30)) as CalculatedTotalAmount, isnull(Stamp,0) as Stamp,sum(Isnull([PayableDays],0)*(Isnull([BasicSalary],0)/30)-isnull(Stamp,0)) as PayableTotalAmount  from 
                        (Select e.ID,e.EmployeeIdentity+' '+ e.CardTitle as Employee_ID,e.Name,
                        d.Name as Designation,s.SectionName,e.Grade,Convert(varchar(10),Joining_Date,126) as Joining_Date,Convert(varchar(10),e.QuitDate,126) as Resignation_Date,
                        (select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID=e.ID and Eod_RefFk=1 and [Status]=1) as BasicSalary,
                        (select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID=e.ID and Eod_RefFk=2 and [Status]=1) as HouseRent,
                        (select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID=e.ID and Eod_RefFk=3 and [Status]=1) as Others,
                        (select TOP 1 (ActualAmount) from HRMHistoryEODs where EmployeeID=e.ID and Eod_RefFk=12 and [Status]=1) as Stamp,
                        (Select [dbo].[fn_Job_Duration_Year_Month_Days](Convert(varchar(10),e.QuitDate,126),1,e.ID)) as [Year],
                        (Select [dbo].[fn_Job_Duration_Year_Month_Days](Convert(varchar(10),e.QuitDate,126),2,e.ID)) as [Month], 
                        (Select [dbo].[fn_Job_Duration_Year_Month_Days](Convert(varchar(10),e.QuitDate,126),3,e.ID)) as [Days],
                        (Select [dbo].[fn_PayableServiceBenefitDays](e.ID)) as [PayableDays]
                        from HRMS_Employee e  inner join Designations d on e.Designation_Id=d.ID 
                        inner Join Sections s on e.Section_Id=s.ID where  e.ID=" + model.EmployeeId + "  " + "and (DATEDIFF(dd,e.Joining_Date,(Case when(e.QuitDate='1900-01-01') THEN e.Joining_Date ELSE e.QuitDate END)) + 1)>=1826 " +
                        "group by e.ID,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,s.SectionName,e.Grade,e.ID,e.Joining_Date,e.QuitDate ) t " +
                        "group by ID,Employee_ID,Name,Designation,SectionName,Grade,BasicSalary,HouseRent,Others,Joining_Date,Resignation_Date,[Year],[Month],[Days],[PayableDays],Stamp").ToList();

            if (serviceBenefitInfo.Any())
            {
                serviceBenefit.DataList = serviceBenefitInfo;
            }
            return serviceBenefit;
        }


        //public void ServiceBenefitACalculationSave(VM_HRMS_Service_Benefit model)
        //{
        //    var ds = 0;
        //    ds = db.Database.ExecuteSqlCommand("[dbo].[Sp_EmployeeServiceBenefitCalculationAndSave] @eid=" + model.EmployeeId);
        //    if (ds > 0)
        //    {
        //        var vl = 1;
        //    }

        //}

        //public VM_HRMS_Service_Benefit ViewServiceBenfitAInformation(VM_HRMS_Service_Benefit model)
        //{
        //    var ServiceBenefit = new VM_HRMS_Service_Benefit();
        //    var dt1 = model.Fromdate.ToString("yyyy-MM-dd");
        //    var dt2 = model.ToDate.ToString("yyyy-MM-dd");

        //    var ServiceBenefitInfo = db.Database.SqlQuery<VM_HRMS_Service_Benefit>($@"Select hsb.[EmployeeId],s.SectionName,e.EmployeeIdentity,e.Name,d.Name as Designation,Convert(varchar(10),e.Joining_Date,126) as Joining_Date,Convert(varchar(10),e.QuitDate,126) as Resignation_Date,hsb.[Year],hsb.[Month],hsb.[Days],hsb.[PayableDays],hsb.[PresentSalary],hsb.[PresentBasic],(hsb.[PresentBasic]/30) as BasicPerDay, hsb.[Stamp],hsb.[CalculatedTotalAmount],hsb.[PayableTotalAmount],Convert(varchar(10),hsb.[PaymentDate],126) as PaymentDate   from HRMS_Service_Benefit hsb
        //            inner Join HRMS_Employee e on e.ID=hsb.EmployeeId
        //            inner join Sections s on e.Section_Id=s.ID
        //            inner Join Designations d on e.Designation_Id=d.ID
        //            where Convert(varchar(10),hsb.PaymentDate,126) between'" + dt1 + "' and '" + dt2 + "'").ToList();

        //    if (ServiceBenefitInfo.Any())
        //    {
        //        ServiceBenefit.DataList = ServiceBenefitInfo;
        //    }
        //    return ServiceBenefit;
        //}

        #endregion

    }
}