﻿using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using Oss.Hrms.Models.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Salary
{
    public class VMSalaryIncrement : RootModel
    {
        #region ConstantValue
        private decimal Medical = 600;
        private decimal Food = 900;
        private decimal Transport = 350;
        private decimal FoodHealthTrans = 1850;
        private decimal HouseRate = (decimal)0.5;
        private decimal Rate = (decimal)1.5;

        #region  Back Dated Salary
        //private decimal Medical = 250;
        //private decimal Food = 650;
        //private decimal Transport = 200;
        //private decimal FoodHealthTrans = 1100;
        //private decimal HouseRate = (decimal)0.4;
        #endregion

        private int IncrimentRate = 5;
        private decimal DaysOfMonth = 30;

        #endregion

        HrmsContext db = new HrmsContext();
        public List<VMSalaryIncrement> DataList { get; set; }

        #region Property
        [Display(Name = "Employee"), Required(ErrorMessage = "Employee isRequired.")]
        public int EmployeeID { get; set; }
        [Display(Name = "Designation")]
        public int DesignationId { get; set; }
        [Display(Name = "Section")]
        public int? SectionID { get; set; }
        [Display(Name = "Unit"), Required(ErrorMessage = "Unit is required.")]
        public int UnitID { get; set; }
        public bool IsChecked { get; set; }
        public string CardNo { get; set; }
        public string CardTitle { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeType { get; set; }
        public string Designation { get; set; }
        public string Section { get; set; }
        public string Grade { get; set; }
        public string Note { get; set; }

        public string ViewJoinDate { get; set; }
        public string ViewEfectDate { get; set; }
        public string Title { get; set; }
        public int CardTitleId { get; set; }
        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        [DisplayName("Join Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime JoinDate { get; set; }

        [DisplayName("Efect Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EfectDate { get; set; }

        #region SalaryModel
        [Display(Name = "Gross Salary")]
        public decimal Gross { get; set; }
        [Display(Name = "Basic Pay"), Required(ErrorMessage = "Basic Pay required.")]
        public decimal Basic { get; set; }
        public decimal House { get; set; }
        public decimal Other { get; set; }
        public int RateIncrement { get; set; }
        public decimal IncrementAmount { get; set; }
        public decimal IncrementTotal { get; set; }
        public decimal IncrementBasic { get; set; }
        public decimal IncrementHouse { get; set; }
        public decimal IncrementGross { get; set; }
        public decimal ModifyedIncrement { get; set; }
        public decimal AdvanceAmount { get; set; }
        public decimal AdvancePay { get; set; }
        
        public bool OTAllow { get; set; }
        public decimal OTRate { get; set; }
        public bool HDRateAllow { get; set; }
        public decimal HDRate { get; set; }
        public bool IsAlart { get; set; }
        #endregion

        #endregion

        public void GetSalaryIncrementList()
        {
            DateTime today = DateTime.Today;
            string pdate = today.ToString("MMMM");
            string ydate = today.Year.ToString();
            this.Title = "Employee Salary Increment Information For the Month Of "+ pdate+" "+ydate;
            var getEmp = (from o in db.Employees
                          join p in db.Sections on o.Section_Id equals p.ID
                          join q in db.Designations on o.Designation_Id equals q.ID
                          where o.Present_Status==1 && o.Joining_Date.Month==today.Month && o.Joining_Date.Year!=today.Year
                          select new VMSalaryIncrement
                          {
                              EmployeeID = o.ID,
                              EmployeeName=o.Name,
                              CardNo=o.CardTitle +" "+ o.EmployeeIdentity,
                              CardTitle=o.CardTitle,
                              JoinDate = o.Joining_Date,
                              Section=p.SectionName,
                              Designation=q.Name,
                              Grade=o.Grade
                              
                          }).ToList();
            
            if (getEmp.Any())
            {
                foreach (var v in getEmp)
                {
                    v.EfectDate = new DateTime(today.Year, v.JoinDate.Month, v.JoinDate.Day);
                    var getRecord = db.HrmsEodRecords.Where(a=>a.EmployeeId==v.EmployeeID && a.Status==true);
                    v.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                    v.ViewEfectDate = Helpers.ShortDateString(v.EfectDate);
                    if (getRecord.Any())
                    {
                        v.IsAlart = getRecord.Any(a =>a.Eod_RefFk==1 && a.EffectiveDate == v.EfectDate) == true ? false : true;
                        v.Basic = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                        v.House= getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                        v.Other = this.FoodHealthTrans;
                        v.Gross = v.Basic + v.House + v.Other;
                        v.RateIncrement = this.IncrimentRate;
                        v.IncrementAmount = (v.Basic * v.RateIncrement) / 100;
                        v.IncrementBasic = v.Basic + v.IncrementAmount;
                        v.IncrementHouse = v.IncrementBasic * this.HouseRate;
                        v.IncrementGross = Math.Round(v.IncrementBasic + v.IncrementHouse + v.Other);
                        v.ModifyedIncrement = v.IncrementGross;
                        if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                        {
                            v.OTAllow = true;
                            v.OTRate= (v.IncrementBasic / 208) * 2;
                        }
                        else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                        {
                            v.OTAllow = false;
                        }
                    }
                }

                this.DataList = getEmp;
            }
        }

        public void GetIncrementListByTitleWise(int CardTitleID)
        {
            DropDownData d = new DropDownData();
            DateTime today = DateTime.Today;
            string pdate = today.ToString("MMMM");
            string ydate = today.Year.ToString();
            this.Title = "Employee Salary Increment Information For the Month Of " + pdate + " " + ydate;
            List<VMSalaryIncrement> lstIncrement = new List<VMSalaryIncrement>();
            if (CardTitleID > 1)
            {
                string CardTitle = d.GetCardTitle(CardTitleId);
                var getEmp = (from o in db.Employees
                              join p in db.Sections on o.Section_Id equals p.ID
                              join q in db.Designations on o.Designation_Id equals q.ID
                              where o.Present_Status == 1 && o.Joining_Date.Month == today.Month && o.Joining_Date.Year != today.Year && o.CardTitle.Contains(CardTitle)
                              select new VMSalaryIncrement
                              {
                                  EmployeeID = o.ID,
                                  EmployeeName = o.Name,
                                  CardNo = o.EmployeeIdentity,
                                  CardTitle = o.CardTitle,
                                  JoinDate = o.Joining_Date,
                                  Section = p.SectionName,
                                  Designation = q.Name,
                                  Grade = o.Grade
                              }).ToList();
                if (getEmp.Any())
                {
                    foreach (var v in getEmp)
                    {
                        DateTime efectDate = new DateTime(today.Year, v.JoinDate.Month, 1);
                        var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmployeeID && a.Status == true);
                        v.IsAlart = getRecord.Any(a => a.Eod_RefFk == 1 && a.EffectiveDate == efectDate) == true ? false : true;
                        if (v.IsAlart)
                        {
                            VMSalaryIncrement item = new VMSalaryIncrement();
                            item.EmployeeID = v.EmployeeID;
                            item.EmployeeName = v.EmployeeName;
                            item.CardNo = v.CardTitle + " " + v.CardNo;
                            item.CardTitle = v.CardTitle;
                            item.JoinDate = v.JoinDate;
                            item.Section = v.Section;
                            item.Designation = v.Designation;
                            item.Grade = v.Grade;
                            item.EfectDate = efectDate;
                            item.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                            item.ViewEfectDate = Helpers.ShortDateString(item.EfectDate);
                            if (getRecord.Any())
                            {
                                item.Basic = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                                item.House = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                                item.Other = this.FoodHealthTrans;
                                item.Gross = item.Basic + item.House + item.Other;
                                item.RateIncrement = this.IncrimentRate;
                                item.IncrementAmount = (item.Basic * item.RateIncrement) / 100;
                                item.IncrementBasic = item.Basic + item.IncrementAmount;
                                item.IncrementHouse = item.IncrementBasic * this.HouseRate;
                                item.IncrementGross = Math.Round(item.IncrementBasic + item.IncrementHouse + item.Other);
                                item.IncrementTotal = Math.Round(item.IncrementAmount + (item.IncrementHouse - item.House));
                                item.ModifyedIncrement = item.IncrementGross;
                                if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                                {
                                    item.OTAllow = true;
                                    item.OTRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                                {
                                    item.OTAllow = false;
                                }
                                if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount > 0))
                                {
                                    item.HDRateAllow = true;
                                    item.HDRate = item.IncrementBasic / this.DaysOfMonth;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount <= 0))
                                {
                                    item.HDRateAllow = false;
                                }
                            }
                            lstIncrement.Add(item);
                        }

                    }
                }
            }
            else if (CardTitleID==1)
            {
                var getEmp = (from o in db.Employees
                              join p in db.Sections on o.Section_Id equals p.ID
                              join q in db.Designations on o.Designation_Id equals q.ID
                              where o.Present_Status == 1 && o.Joining_Date.Month == today.Month && o.Joining_Date.Year != today.Year
                              select new VMSalaryIncrement
                              {
                                  EmployeeID = o.ID,
                                  EmployeeName = o.Name,
                                  CardNo = o.EmployeeIdentity,
                                  CardTitle = o.CardTitle,
                                  JoinDate = o.Joining_Date,
                                  Section = p.SectionName,
                                  Designation = q.Name,
                                  Grade = o.Grade

                              }).ToList();


                if (getEmp.Any())
                {
                    foreach (var v in getEmp)
                    {
                        DateTime efectDate = new DateTime(today.Year, v.JoinDate.Month, 1);
                        var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmployeeID && a.Status == true);
                        v.IsAlart = getRecord.Any(a => a.Eod_RefFk == 1 && a.EffectiveDate == efectDate) == true ? false : true;
                        if (v.IsAlart)
                        {
                            VMSalaryIncrement item = new VMSalaryIncrement();
                            item.EmployeeID = v.EmployeeID;
                            item.EmployeeName = v.EmployeeName;
                            item.CardNo = v.CardTitle + " " + v.CardNo;
                            item.CardTitle = v.CardTitle;
                            item.JoinDate = v.JoinDate;
                            item.Section = v.Section;
                            item.Designation = v.Designation;
                            item.Grade = v.Grade;
                            item.EfectDate = efectDate;
                            item.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                            item.ViewEfectDate = Helpers.ShortDateString(item.EfectDate);
                            if (getRecord.Any())
                            {
                                item.Basic = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                                item.House = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                                item.Other = this.FoodHealthTrans;
                                item.Gross = item.Basic + item.House + item.Other;
                                item.RateIncrement = this.IncrimentRate;
                                item.IncrementAmount = (item.Basic * item.RateIncrement) / 100;
                                item.IncrementBasic = item.Basic + item.IncrementAmount;
                                item.IncrementHouse = item.IncrementBasic * this.HouseRate;
                                item.IncrementGross = Math.Round(item.IncrementBasic + item.IncrementHouse + item.Other);
                                item.IncrementTotal = item.IncrementAmount + (item.IncrementHouse - item.House);
                                item.ModifyedIncrement = item.IncrementGross;
                                if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                                {
                                    item.OTAllow = true;
                                    item.OTRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                                {
                                    item.OTAllow = false;
                                }
                                if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount > 0))
                                {
                                    item.HDRateAllow = true;
                                    item.HDRate = item.IncrementBasic / this.DaysOfMonth;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount <= 0))
                                {
                                    item.HDRateAllow = false;
                                }
                            }
                            lstIncrement.Add(item);
                        }
                    }
                }
            }
            this.DataList = lstIncrement;
        }

        public void GetIncrementedListByTitleWise(int CardTitleID)
        {
            DropDownData d = new DropDownData();
            DateTime today = DateTime.Today;
            string pdate = today.ToString("MMMM");
            string ydate = today.Year.ToString();
            this.Title = "Employee Salary Increment Information For the Month Of " + pdate + " " + ydate;
            List<VMSalaryIncrement> lstIncrement = new List<VMSalaryIncrement>();
            if (CardTitleID > 1)
            {
                string CardTitle = d.GetCardTitle(CardTitleId);
                var getEmp = (from o in db.Employees
                              join p in db.Sections on o.Section_Id equals p.ID
                              join q in db.Designations on o.Designation_Id equals q.ID
                              where o.Present_Status == 1 && o.Joining_Date.Month == today.Month && o.Joining_Date.Year != today.Year && o.CardTitle.Contains(CardTitle)
                              select new VMSalaryIncrement
                              {
                                  EmployeeID = o.ID,
                                  EmployeeName = o.Name,
                                  CardNo = o.EmployeeIdentity,
                                  CardTitle = o.CardTitle,
                                  JoinDate = o.Joining_Date,
                                  Section = p.SectionName,
                                  Designation = q.Name,
                                  Grade = o.Grade
                              }).ToList();
                if (getEmp.Any())
                {
                    foreach (var v in getEmp)
                    {
                        DateTime efectDate = new DateTime(today.Year, v.JoinDate.Month, 1);
                        var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmployeeID && a.Status == true || a.ID==47);
                        v.IsAlart = getRecord.Any(a => a.Eod_RefFk == 1 && a.EffectiveDate == efectDate) == true ? true : false;
                        if (v.IsAlart)
                        {
                            VMSalaryIncrement item = new VMSalaryIncrement();
                            item.EmployeeID = v.EmployeeID;
                            item.EmployeeName = v.EmployeeName;
                            item.CardNo = v.CardTitle + " " + v.CardNo;
                            item.CardTitle = v.CardTitle;
                            item.JoinDate = v.JoinDate;
                            item.Section = v.Section;
                            item.Designation = v.Designation;
                            item.Grade = v.Grade;
                            item.EfectDate = efectDate;
                            item.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                            item.ViewEfectDate = Helpers.ShortDateString(item.EfectDate);
                            if (getRecord.Any())
                            {
                                item.Basic = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                                item.House = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                                item.Other = this.FoodHealthTrans;
                                item.Gross = item.Basic + item.House + item.Other;
                                if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                                {
                                    item.OTAllow = true;
                                    item.OTRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                                {
                                    item.OTAllow = false;
                                }
                                if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount > 0))
                                {
                                    item.HDRateAllow = true;
                                    item.HDRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount <= 0))
                                {
                                    item.HDRateAllow = false;
                                }
                            }
                            lstIncrement.Add(item);
                        }

                    }
                }
            }
            else if (CardTitleID == 1)
            {
                var getEmp = (from o in db.Employees
                              join p in db.Sections on o.Section_Id equals p.ID
                              join q in db.Designations on o.Designation_Id equals q.ID
                              where o.Present_Status == 1 && o.Joining_Date.Month == today.Month && o.Joining_Date.Year != today.Year
                              select new VMSalaryIncrement
                              {
                                  EmployeeID = o.ID,
                                  EmployeeName = o.Name,
                                  CardNo = o.EmployeeIdentity,
                                  CardTitle = o.CardTitle,
                                  JoinDate = o.Joining_Date,
                                  Section = p.SectionName,
                                  Designation = q.Name,
                                  Grade = o.Grade

                              }).ToList();


                if (getEmp.Any())
                {
                    foreach (var v in getEmp)
                    {
                        DateTime efectDate = new DateTime(today.Year, v.JoinDate.Month, 1);
                        var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmployeeID && a.Status == true || a.ID==47);
                        v.IsAlart = getRecord.Any(a => a.Eod_RefFk == 1 && a.EffectiveDate == efectDate) == true ? true : false;
                        if (v.IsAlart)
                        {
                            VMSalaryIncrement item = new VMSalaryIncrement();
                            item.EmployeeID = v.EmployeeID;
                            item.EmployeeName = v.EmployeeName;
                            item.CardNo = v.CardTitle + " " + v.CardNo;
                            item.CardTitle = v.CardTitle;
                            item.JoinDate = v.JoinDate;
                            item.Section = v.Section;
                            item.Designation = v.Designation;
                            item.Grade = v.Grade;
                            item.EfectDate = efectDate;
                            item.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                            item.ViewEfectDate = Helpers.ShortDateString(item.EfectDate);
                            if (getRecord.Any())
                            {
                                item.Basic = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                                item.House = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                                item.Other = this.FoodHealthTrans;
                                item.Gross = item.Basic + item.House + item.Other;
                                if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                                {
                                    item.OTAllow = true;
                                    item.OTRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                                {
                                    item.OTAllow = false;
                                }
                                if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount > 0))
                                {
                                    item.HDRateAllow = true;
                                    item.HDRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount <= 0))
                                {
                                    item.HDRateAllow = false;
                                }
                            }
                            lstIncrement.Add(item);
                        }
                    }
                }
            }
            this.DataList = lstIncrement;
        }

        public void CreateYearlyIncrement1(VMSalaryIncrement model)
        {
            List<EODReference> updateRecordList = new List<EODReference>();

            if (model.DataList.Any(a => a.IsChecked == true))
            {
                var referenceExistEod = db.HrmsEodReferences.Where(a => a.Status == true && a.Start == 1 || a.ID==47).ToList();
                if (referenceExistEod.Any())
                {
                    foreach (var incSal in model.DataList.Where(a => a.IsChecked == true))
                    {
                        #region AddItemInList
                        foreach (var itemRef in referenceExistEod)
                        {
                            if (itemRef.ID != 3)
                            {
                                EODReference item = new EODReference();
                                item.EODRefID = itemRef.ID;
                                item.EmpID = incSal.EmployeeID;
                                if (itemRef.ID == 1)
                                {
                                    item.Amount = incSal.IncrementBasic;
                                }
                                else if (itemRef.ID == 2)
                                {
                                    item.Amount = incSal.IncrementHouse;
                                }
                                else if (itemRef.ID == 11)
                                {
                                    if (incSal.OTAllow)
                                    {
                                        item.Amount = Math.Round(incSal.OTRate, 2);
                                    }
                                }
                                else if (itemRef.ID == 47)
                                {
                                    if (incSal.HDRateAllow)
                                    {
                                        item.Amount = Math.Round(incSal.HDRate, 2);
                                    }
                                }
                                updateRecordList.Add(item);
                            }
                        }
                        #endregion
                    }

                    foreach (var incSal in model.DataList.Where(a => a.IsChecked == true))
                    {
                        #region SaveData

                        var allExistRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == incSal.EmployeeID && a.Status == true || a.ID==47);
                        if (updateRecordList.Any())
                        {
                            foreach (var updateRecord in updateRecordList)
                            {
                                if (allExistRecord.Any(a => a.Eod_RefFk == updateRecord.EODRefID))
                                {
                                    var previousRecord = allExistRecord.FirstOrDefault(a => a.Eod_RefFk == updateRecord.EODRefID);
                                    if (previousRecord.ActualAmount != updateRecord.Amount)
                                    {
                                        previousRecord.Status = false;
                                        previousRecord.Update_By = model.Entry_By;
                                        previousRecord.Update_Date = model.Entry_Date;

                                        HRMS_EodRecord recordAdd = new HRMS_EodRecord()
                                        {
                                            EmployeeId = updateRecord.EmpID,
                                            Eod_RefFk = updateRecord.EODRefID,
                                            ActualAmount = updateRecord.Amount,
                                            Status = true,
                                            EffectiveDate = incSal.EfectDate,
                                            Update_Date = model.Entry_Date,
                                            Entry_Date = model.Entry_Date,
                                            Entry_By = model.Entry_By
                                        };
                                        db.HrmsEodRecords.Add(recordAdd);
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
        }

        public void CreateYearlyIncrement(VMSalaryIncrement model)
        {
            List<EODReference> updateRecordList = new List<EODReference>();

            if (model.DataList.Any(a => a.IsChecked == true))
            {
                foreach (var incSal in model.DataList.Where(a => a.IsChecked == true))
                {
                    GetUpdateSalary(incSal);
                }
            }
        }

        public async Task<int> Save()
        {
            return await db.SaveChangesAsync();
        }


        public decimal HealthAllowance { get; set; }
        public decimal FoodAllowance { get; set; }
        public decimal TransportAllowance { get; set; }
        public decimal OtherAllowance { get; set; }
        public decimal BasicAmount { get; set; }
        public decimal HouseAllowance { get; set; }
        public int OTAllowed { get; set; }
        public string SaveItem { get; set; }


        private void GetUpdateSalary(VMSalaryIncrement model)
        {
            model.HealthAllowance = this.Medical;
            model.FoodAllowance = this.Food;
            model.TransportAllowance = this.Transport;
            model.OtherAllowance = model.HealthAllowance + model.FoodAllowance + model.TransportAllowance;
            decimal basic = (model.IncrementGross - model.OtherAllowance) / this.Rate;
            model.BasicAmount = Math.Round(basic);
            decimal house = model.BasicAmount * this.HouseRate;
            model.HouseAllowance = Math.Round(house);
            decimal OTRate = (model.BasicAmount / 208) * 2;

            List<EODReference> updateRecordList = new List<EODReference>();
            #region ManageSalary
            var referenceExistEod = db.HrmsEodReferences.Where(a => a.Status == true && a.Start == 1).ToList();
            if (referenceExistEod.Any())
            {
                foreach (var itemRef in referenceExistEod)
                {
                    EODReference item = new EODReference();
                    item.UpdateDate = model.EfectDate;
                    item.EODRefID = itemRef.ID;
                    item.EmpID = model.EmployeeID;
                    if (itemRef.ID == 1)
                    {
                        item.Amount = model.BasicAmount;
                    }
                    else if (itemRef.ID == 2)
                    {
                        item.Amount = model.HouseAllowance;
                    }
                    else if (itemRef.ID == 3)
                    {
                        item.Amount = model.OtherAllowance;
                    }
                    else if (itemRef.ID == 42)
                    {
                        item.Amount = model.Transport;
                    }
                    else if (itemRef.ID == 43)
                    {
                        item.Amount = model.Medical;
                    }
                    else if (itemRef.ID == 44)
                    {
                        item.Amount = model.Food;
                    }
                    else if (itemRef.ID == 11)
                    {
                        if (model.OTAllow)
                        {
                            item.Amount = Math.Round(OTRate, 2);
                        }
                    }
                    else if (itemRef.ID == 47)
                    {
                        if (model.HDRateAllow)
                        {
                            item.Amount = Math.Round(model.HDRate, 2);
                        }
                    }

                    updateRecordList.Add(item);
                }
            }
            #endregion

            #region GeneralSalary
            var allExistRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmployeeID && a.Status == true).ToList();
            if (updateRecordList.Any())
            {
                foreach (var v in updateRecordList)
                {
                    var checkItemMultiple = allExistRecord.Where(a => a.Eod_RefFk == v.EODRefID).Count();
                    if (checkItemMultiple > 1)
                    {
                        var takeLast = allExistRecord.Where(a => a.Eod_RefFk == v.EODRefID).OrderByDescending(a => a.ID).FirstOrDefault();

                        foreach (var d in allExistRecord.Where(a => a.Eod_RefFk == v.EODRefID && a.ID != takeLast.ID))
                        {
                            var update = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmployeeID && a.ID == d.ID).FirstOrDefault();
                            update.Status = false;
                            update.Update_By = model.Update_By;
                            update.Update_Date = model.Update_Date;
                        }
                    }
                    else if (checkItemMultiple == 1)
                    {
                        var previousRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmployeeID && a.Eod_RefFk == v.EODRefID && a.Status == true).FirstOrDefault();
                        if (previousRecord.ActualAmount != v.Amount)
                        {
                            previousRecord.Status = false;
                            previousRecord.Update_By = model.Update_By;
                            previousRecord.Update_Date = model.Update_Date;

                            HRMS_EodRecord recordAdd = new HRMS_EodRecord()
                            {
                                EmployeeId = v.EmpID,
                                Eod_RefFk = v.EODRefID,
                                ActualAmount = v.Amount,
                                Status = true,
                                EffectiveDate = v.UpdateDate,
                                Entry_Date = model.Update_Date,
                                Entry_By = model.Update_By
                            };
                            db.HrmsEodRecords.Add(recordAdd);
                        }
                    }
                    else if (checkItemMultiple <= 0)
                    {
                        HRMS_EodRecord recordAdd = new HRMS_EodRecord()
                        {
                            EmployeeId = v.EmpID,
                            Eod_RefFk = v.EODRefID,
                            ActualAmount = v.Amount,
                            Status = true,
                            EffectiveDate = v.UpdateDate,
                            Entry_Date = model.Update_Date,
                            Entry_By = model.Update_By
                        };
                        db.HrmsEodRecords.Add(recordAdd);
                    }
                }
                db.SaveChanges();
            }
            #endregion
        }

        public void GetEmployeeForAdvancePay(DateTime PayDate, int SectionID)
        {
            var getEmp = (from o in db.Employees
                          join p in db.Sections on o.Section_Id equals p.ID
                          join q in db.Designations on o.Designation_Id equals q.ID
                          where o.Present_Status == 1 && o.Section_Id==SectionID
                          select new VMSalaryIncrement
                          {
                              FromDate=PayDate,
                              EmployeeID = o.ID,
                              EmployeeName = o.Name,
                              CardNo = o.EmployeeIdentity,
                              CardTitle = o.CardTitle,
                              JoinDate = o.Joining_Date,
                              Section = p.SectionName,
                              Designation = q.Name,
                              Grade = o.Grade

                          }).ToList();

            var vData = db.HRMAdvance.Where(a => a.PaymentDate.Month == PayDate.Month && a.PaymentDate.Year==PayDate.Year).ToList();
            if (getEmp.Any())
            {
                foreach (var v in getEmp)
                {
                    var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmployeeID && a.Status == true);
                    v.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                    if (getRecord.Any())
                    {
                        v.Basic = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                        if (v.Basic > 0)
                        {
                            v.House = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                            v.Other = this.FoodHealthTrans;
                            v.Gross = v.Basic + v.House + v.Other;
                            if (vData.Any(a => a.EmployeeIdFK == v.EmployeeID))
                            {
                                var empPay = vData.FirstOrDefault(a => a.EmployeeIdFK == v.EmployeeID);
                                v.AdvancePay= empPay.Amount;
                                v.SaveItem = "savedColor";
                                v.AdvanceAmount = 0;
                                v.ViewEfectDate= Helpers.ShortDateString(empPay.PaymentDate);
                            }
                            else
                            {
                                v.IsChecked = true;
                                v.AdvanceAmount = v.Gross;
                            }
                        }
                        else
                        {
                            v.SaveItem = "noBasicColor";
                        }
                    }
                }

                this.DataList = getEmp;
            }
        }

        public void CreateAdvancePay(VMSalaryIncrement model)
        {
            List<EODReference> updateRecordList = new List<EODReference>();
            var vData = db.HRMAdvance.Where(a=>a.PaymentDate.Month== model.FromDate.Month && a.PaymentDate.Year==model.FromDate.Year);
            if (model.DataList.Any(a => a.IsChecked == true))
            {
                foreach (var aPay in model.DataList.Where(a => a.IsChecked == true))
                {
                    if (vData.Any(a => a.EmployeeIdFK == aPay.EmployeeID))
                    {
                        var update=vData.FirstOrDefault(a => a.EmployeeIdFK == aPay.EmployeeID);
                        update.Amount = aPay.AdvanceAmount;
                    }
                    else
                    {
                        HRMS_Advance add = new HRMS_Advance();
                        add.PaymentDate = aPay.FromDate;
                        add.EmployeeIdFK = aPay.EmployeeID;
                        add.Amount = aPay.AdvanceAmount;
                        db.HRMAdvance.Add(add);
                    }
                }
                db.SaveChanges();
            }
        }
    }
}