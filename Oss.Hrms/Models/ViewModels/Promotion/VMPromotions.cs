﻿using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Promotion
{
    public class VMPromotions : RootModel
    {
        #region ConstantValue
        private decimal Medical = 600;
        private decimal Food = 900;
        private decimal Transport = 350;
        private decimal TotalOther = 1850;
        private decimal HouseRate = (decimal)0.5;
        private decimal Rate = (decimal)1.5;
        private decimal DaysOfMonth = 30;
        #endregion

        HrmsContext db = new HrmsContext();
        public List<VMPromotions> DataList { get; set; }
        public VMEmployee VMEmployee { get; set; }
        public List<VMEmployee> ListEmployee { get; set; }

        public List<string> EmployeeIds { get; set; }
        //public List<VM_HRMS_Earn_Leave_Payment> DataList { get; set; }

        [Display(Name = "Employee"), Required(ErrorMessage = "Employee is Required.")]
        public int EmployeeID { get; set; }
        [Display(Name = "CardTitle"), Required(ErrorMessage = "CardTitle is Required.")]
        public string CardTitle { get; set; }
        [Display(Name = "CardNo"), Required(ErrorMessage = "CardNo is Required.")]
        public string CardNo { get; set; }
        [Display(Name = "Department"), Required(ErrorMessage = "Department is Required.")]
        public int DepartmentId { get; set; }
        [Display(Name = "Section"), Required(ErrorMessage = "Section is Required.")]
        public int SectionId { get; set; }
        [Display(Name = "Grade"), Required(ErrorMessage = "Grade is Required.")]
        public int GradeId { get; set; }
        [Display(Name = "Designation"), Required(ErrorMessage = "Designation is Required.")]
        public int DesignationId { get; set; }
        [Display(Name = "LineNo"), Required(ErrorMessage = "LineNo is Required.")]
        public int LineId { get; set; }
        [Display(Name = "Unit"), Required(ErrorMessage = "Unit is Required.")]
        public int UnitId { get; set; }
        [Display(Name = "Business Unit"), Required(ErrorMessage = "Business Unit is Required.")]
        public int BusinessUnitId { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Promotion Date"), Required(ErrorMessage = "Promotion Date is Required.")]
        public DateTime PromotionDate { get; set; }
        public int EodRecordId { get; set; }
        public string Grade { get; set; }

        [Display(Name = "Name"), Required(ErrorMessage = "Name is Required.")]
        public string EmployeeName { get; set; }
        public string CCardNo { get; set; }
        public string CCardTitle { get; set; }
        public string CSection { get; set; }
        public string CDesignation { get; set; }
        public decimal CSalary { get; set; }

        public string PCardNo { get; set; }
        public string PCardTitle { get; set; }
        public string PSection { get; set; }
        public string PDesignation { get; set; }
        public decimal PSalary { get; set; }
        public string ViewDate { get; set; }

        [Display(Name = "Designation"), Required(ErrorMessage = "Designation is Required.")]
        public string DesignationName { get; set; }
        [Display(Name = "Section"), Required(ErrorMessage = "Section is Required.")]
        public string SectionName { get; set; }
        [Display(Name = "Dept.Name"), Required(ErrorMessage = "Dept. Name is Required.")]
        public string DeptName { get; set; }
        [Display(Name = "Joining Date"), Required(ErrorMessage = "Joining Date is Required.")]
        public string JoiningDate { get; set; }
        [Display(Name = "Job Permanent Date"), Required(ErrorMessage = "Job Permanent Date is Required.")]
        public string JobPermanent_Date { get; set; }
        [Display(Name = "Employement Type"), Required(ErrorMessage = "Employment Type is Required.")]
        public string Employement_Type { get; set; }
        
        public void GetCreatePromotion(VMPromotions model)
        {
            DropDownData d = new DropDownData();
            model.Grade = d.GetGradeById(model.GradeId);
            var getSalary = db.HrmsEodRecords.Where(a =>a.EmployeeId==model.EmployeeID && a.Eod_RefFk == 1 && a.Status == true).OrderBy(a => a.ID);
            if (getSalary.Any())
            {
                model.EodRecordId = getSalary.FirstOrDefault().ID;
            }
            var getHistory = db.HrmsEmploymentHistory.Where(a => a.EmployeeIdFK == model.EmployeeID && a.Current==true);
            if (getHistory.Any())
            {
                var update = getHistory.FirstOrDefault();
                update.Current = false;
            }
            var getEmp = db.Employees.Where(a=>a.ID==model.EmployeeID).FirstOrDefault();
            
            HRMS_EmploymentHistory pro = new HRMS_EmploymentHistory();
            pro.EmployeeIdFK = getEmp.ID;
            pro.CardNo = getEmp.EmployeeIdentity;
            pro.CardTitle = getEmp.CardTitle;
            pro.PunchCardNo = getEmp.CardNo;
            pro.DepartmentFK = getEmp.Department_Id;
            pro.SectionFK = getEmp.Section_Id;
            pro.DesignationFK = getEmp.Designation_Id;
            pro.Grade = getEmp.Grade;
            pro.BusinessUnitFK = getEmp.Business_Unit_Id;
            pro.UnitFK = getEmp.Unit_Id;
            pro.LineFK = getEmp.LineId;
            pro.HistoyType = 1;
            pro.Current = true;
            if (model.EodRecordId>0)
            {
                pro.EodRecordFK = model.EodRecordId;
            }
            pro.Staff_Type = getEmp.Staff_Type;
            pro.PromotionDate = model.PromotionDate;
            pro.Status = true;
            pro.Entry_Date = model.Entry_Date;
            pro.Entry_By = model.Entry_By;
            pro.Update_Date = model.Entry_Date;

            db.HrmsEmploymentHistory.Add(pro);

            db.SaveChanges();

            getEmp.EmployeeIdentity = model.CardNo;
            getEmp.CardTitle = model.CardTitle;
            getEmp.Department_Id = model.DepartmentId;
            getEmp.Section_Id = model.SectionId;
            getEmp.Designation_Id = model.DesignationId;
            getEmp.Grade = model.Grade;
            getEmp.LineId = model.LineId;
            getEmp.Business_Unit_Id = model.BusinessUnitId;
            getEmp.Unit_Id = model.UnitId;
            getEmp.Update_Date = model.PromotionDate;

            db.SaveChanges();
        }

        public void GetPromotionList()
        {
            List<VMPromotions> lstPromotion = new List<VMPromotions>();
            var vData = (from t1 in db.HrmsEmploymentHistory
                         join t3 in db.Sections on t1.SectionFK equals t3.ID
                         join t4 in db.Designations on t1.DesignationFK equals t4.ID
                         where t1.Current == true
                         select new
                         {
                             HID = t1.ID,
                             EmployeeId = t1.EmployeeIdFK,
                             CardTitle = t1.CardTitle,
                             CardNo = t1.CardNo,
                             Section = t3.SectionName,
                             Designation = t4.Name,
                             Salary = db.HrmsEodRecords.Where(a => a.ID == t1.EodRecordFK).ToList(),
                             Date=t1.PromotionDate,
                             CurrentInfo = (from o in db.Employees
                                          join p in db.Sections on o.Section_Id equals p.ID
                                          join q in db.Designations on o.Designation_Id equals q.ID
                                          where o.ID == t1.EmployeeIdFK
                                          select new
                                          {
                                              Name=o.Name,
                                              EmployeeId = o.ID,
                                              CardTitle = o.CardTitle,
                                              CardNo = o.EmployeeIdentity,
                                              Section = p.SectionName,
                                              Designation = q.Name,
                                              Salary = db.HrmsEodRecords.Where(a => a.EmployeeId == o.ID && a.Eod_RefFk == 1 && a.Status == true).ToList()
                                          }).ToList()
                         }).OrderByDescending(a => a.HID);

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    VMPromotions pmodel = new VMPromotions();

                    if (v.CurrentInfo.Any())
                    {
                        var proEmp = v.CurrentInfo.FirstOrDefault();
                        pmodel.ViewDate = v.Date.ToString("yyyy/MM/dd");
                        pmodel.CCardNo = proEmp.CardTitle +" "+ proEmp.CardNo;
                        pmodel.CCardTitle = proEmp.CardTitle;
                        pmodel.EmployeeName = proEmp.Name;
                        pmodel.CSection = proEmp.Section;
                        pmodel.CDesignation = proEmp.Designation;

                        if (proEmp.Salary.Any())
                        {
                            var getBasic = proEmp.Salary.FirstOrDefault().ActualAmount;
                            decimal totalSalary = getBasic + (getBasic * HouseRate) + TotalOther;
                            pmodel.CSalary = Math.Round(totalSalary);
                        }
                    }
                    pmodel.PCardNo = v.CardTitle +" "+ v.CardNo;
                    pmodel.PCardTitle = v.CardTitle;
                    pmodel.PSection = v.Section;
                    pmodel.PDesignation = v.Designation;
                    if (v.Salary.Any())
                    {
                        var getBasic = v.Salary.FirstOrDefault().ActualAmount;
                        decimal totalSalary = getBasic + (getBasic * HouseRate) + TotalOther;
                        pmodel.PSalary = Math.Round(totalSalary);
                    }
                    lstPromotion.Add(pmodel);
                }
            }
            this.DataList = lstPromotion;          
        }

        public VMPromotions EmployeeConfirmationWaitingList()
        {
            var confirmationInfo = new VMPromotions();
            
                var confirmationwaitingInfo = db.Database.SqlQuery<VMPromotions>($@"Select e.ID as EmployeeID,e.CardTitle,e.EmployeeIdentity as CardNo,e.Name as EmployeeName,ds.Name as DesignationName, s.SectionName,d.DeptName,
                                    convert(varchar(10),e.Joining_Date,126) as JoiningDate,convert(varchar(10),e.JobPermanent_Date,126) as JobPermanent_Date,e.Employement_Type
                                    from HRMS_Employee e
                                    inner join Sections s on s.ID=e.Section_Id
                                    inner join Departments d on d.ID=e.Department_Id
                                    inner join Designations ds on ds.ID=e.Designation_Id
                                    where e.Employement_Type='Contractual' and  e.Present_Status=1").ToList();

            if (confirmationwaitingInfo.Any())
            {
                confirmationInfo.DataList = confirmationwaitingInfo;
            }
            return confirmationInfo;
        }

        public void EmployeeConfirmationSave(VMPromotions model)
        {
           
            model.EmployeeIds = model.EmployeeIds[0].TrimEnd(',').Split(',').ToList();
            var list = "";
            list = String.Join(",", model.EmployeeIds);
            var ds = 0;
            ds = db.Database.ExecuteSqlCommand("[dbo].[Sp_EmpoloyeeConfirmation] @EmployeeId='" + list + "'");
            if (ds > 0)
            {
                var vl = 1;
            }
        }
    }
}