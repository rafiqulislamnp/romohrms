﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Entity.User;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.Attendance;

namespace Oss.Hrms.Models.ViewModels
{
    public class VM_User:RootModel
    {

        HrmsContext db = new HrmsContext();
        public User  User { get; set; }
        public string UserName { get; set; }
        public string   Password { get; set; }

        public bool IsAudit { get; set; }
        public string LblMessage { get; set; }


        public IEnumerable<VM_User> DataList { get; set; }




        //public void SelectSingle(string  username, string pass)
        //{
        //    var v = (from o in db.Users
        //             where (o.Status == true && UserName == username && Password== pass)
        //             select new VM_User()
        //             {
        //                 ID = o.ID,
        //                 UserName = o.UserName,
        //                Password=o.Password
        //             }).FirstOrDefault();
        //    this.User = v.User;
        //}

        //public int LoginUser(bool authorised )
        //{
        //    db = new HrmsContext();
        //    int flag = 0;
        //    var v = (from o in db.Users
        //             where (o.Status == true && UserName == this.UserName && Password == this.Password)
        //             select new VM_User()
        //             {
        //                 ID = o.ID,
        //                 UserName = o.UserName,
        //                 Password = o.Password
        //             }).FirstOrDefault();
        //    if (v != null)
        //    {
        //        flag = v.ID;
        //        //User.ID=flag
        //        //this.User.ID = flag;
        //        this.User.UserName = v.UserName;
        //        this.User.Password = v.Password;
        //    }
        //    if (flag > 0)
        //    {
        //        if (v.Status == false)
        //        {
        //            this.LblMessage = "You are not authorised!";
        //            return 0;
        //        }
        //    }
        //    else
        //    {
        //        this.LblMessage = "User / Password does not match!";
        //    }
        //    return flag;
        //}

        public int LoginUser1(bool authorised)
        {
            db = new HrmsContext();
            int flag = 0;
            
            var v = (from t1 in db.Users
                where t1.Status == true && t1.UserName == this.UserName && t1.Password == this.Password
                select new VM_User()
                {
                    ID = t1.ID,
                    UserName = t1.UserName,
                    Password = t1.Password
                }).FirstOrDefault();
            if (v != null)
            {
                flag = v.ID;
                //User.ID=flag
                //this.User.ID = flag;
                //this.User.UserName = v.UserName;
                //this.User.Password = v.Password;
            }
            if (flag > 0)
            {
                if (v.Status == false)
                {
                    this.LblMessage = "You are not authorised!";
                    return 0;
                }
            }
            else
            {
                this.LblMessage = "User / Password does not match!";
            }
            return flag;
        }
    }
}