﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.Leave;
using Oss.Hrms.Models.ViewModels.ServiceBenefit;

namespace Oss.Hrms.Controllers
{
    public class ServiceBenefitController : Controller
    {


        DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();

        CurrentUser user;

        public ServiceBenefitController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        

        #region Service Benefit Original

        // GET: ServiceBenefit
        [HttpGet]
        public ActionResult ServiceBenefitCalcuation()
        {
            ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeListForServiceBenefit(false), "Value", "Text");
            VM_HRMS_Service_Benefit model = new VM_HRMS_Service_Benefit();
            model.DataList = new List<VM_HRMS_Service_Benefit>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> ServiceBenefitCalcuation(VM_HRMS_Service_Benefit model)
        {
            var empid = model.EmployeeId;
            ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeListForServiceBenefit(false), "Value", "Text");
            await Task.Run(() => model = model.ServiceBenefitCalculation(model));
            model.IsAudit =false;
            model.EmployeeId = empid;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> ServiceBenefitCalcuationSave(VM_HRMS_Service_Benefit model)
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    await Task.Run(() => model.ServiceBenefitCalculationSave(model));
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Employee Service Benefit Payment Saved Successfully.";
                    return RedirectToAction("ServiceBenefitCalcuation", "ServiceBenefit");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ServiceBenefitCalcuation", "ServiceBenefit");
                }
            }
        }


        // Below Action used to View Service Benefit Information
        // Database: OSS.HRMS
        // Table Name :HRMS_Service_Benefit
        // Domain Model Name: HRMS_Service_Benefit
        // View Model Name: VM_HRMS_Service_Benefit
        // Developed by: Anis
        // Date: 2018-11-04
        [HttpGet]
        public async Task<ActionResult> ViewServiceBenefitInfo()
        {
            //if (Session["Role"] != null)
            {
                try

                {
                    VM_HRMS_Service_Benefit model = new VM_HRMS_Service_Benefit();
                    model.DataList = new List<VM_HRMS_Service_Benefit>();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewServiceBenefitInfo", "ServiceBenefit");
                }
            }

        }
        // Below Action used to View Service Benefit Information
        // Database: OSS.HRMS
        // Table Name :HRMS_Service_Benefit
        // Domain Model Name: HRMS_Service_Benefit
        // View Model Name: VM_HRMS_Service_Benefit
        // Developed by: Anis
        // Date: 2018-11-04
        [HttpPost]
        public async Task<ActionResult> ViewServiceBenefitInfo(VM_HRMS_Service_Benefit model)
        {
            {
                try
                {
                    model.IsAudit = false;
                    await Task.Run(() => model = model.ViewServiceBenfitInformation(model));
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewServiceBenefitInfo", "ServiceBenefit");
                }
            }

        }


        #endregion


        #region Service Benefit For Audit

        // GET: ServiceBenefit
        [HttpGet]
        public ActionResult ServiceBenefitACalcuation()
        {
            ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeListForServiceBenefit(true), "Value", "Text");
            VM_HRMS_Service_Benefit model = new VM_HRMS_Service_Benefit();
            model.DataList = new List<VM_HRMS_Service_Benefit>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> ServiceBenefitACalcuation(VM_HRMS_Service_Benefit model)
        {
            var empid = model.EmployeeId;
            ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeListForServiceBenefit(true), "Value", "Text");
            await Task.Run(() => model = model.ServiceBenefitACalculation(model));
            model.IsAudit = true;
            model.EmployeeId = empid;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> ServiceBenefitACalcuationSave(VM_HRMS_Service_Benefit model)
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    await Task.Run(() => model.ServiceBenefitCalculationSave(model));
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Employee Service Benefit Payment Saved Successfully.";
                    return RedirectToAction("ServiceBenefitCalcuation", "ServiceBenefit");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ServiceBenefitCalcuation", "ServiceBenefit");
                }
            }
        }


        // Below Action used to View Service Benefit Information
        // Database: OSS.HRMS
        // Table Name :HRMS_Service_Benefit
        // Domain Model Name: HRMS_Service_Benefit
        // View Model Name: VM_HRMS_Service_Benefit
        // Developed by: Anis
        // Date: 2018-11-04
        [HttpGet]
        public async Task<ActionResult> ViewServiceBenefitAInfo()
        {
            //if (Session["Role"] != null)
            {
                try

                {
                    VM_HRMS_Service_Benefit model = new VM_HRMS_Service_Benefit();
                    model.DataList = new List<VM_HRMS_Service_Benefit>();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewServiceBenefitInfo", "ServiceBenefit");
                }
            }

        }
        // Below Action used to View Service Benefit Information
        // Database: OSS.HRMS
        // Table Name :HRMS_Service_Benefit
        // Domain Model Name: HRMS_Service_Benefit
        // View Model Name: VM_HRMS_Service_Benefit
        // Developed by: Anis
        // Date: 2018-11-04
        [HttpPost]
        public async Task<ActionResult> ViewServiceBenefitAInfo(VM_HRMS_Service_Benefit model)
        {
            {
                try
                {
                    model.IsAudit = true;
                    await Task.Run(() => model = model.ViewServiceBenfitInformation(model));
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewServiceBenefitInfo", "ServiceBenefit");
                }
            }

        }


        #endregion


    }
}