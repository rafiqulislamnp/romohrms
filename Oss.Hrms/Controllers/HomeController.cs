﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.User;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels;
using Oss.Hrms.Models.ViewModels.HRMS;

namespace Oss.Hrms.Controllers
{
    public class HomeController : Controller
    {
        private HrmsContext db = new HrmsContext();
        public ActionResult Index()
        {
           // return View();
             return RedirectToAction("Login");
        }
        public HomeController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        public ActionResult Login()
        {
            HttpCookie currentUserCookie = HttpContext.Request.Cookies["UserLogin"];
            HttpContext.Response.Cookies.Remove("UserLogin");
            return View();
        }
        [HttpPost]
        public ActionResult Login(VM_User user)
        {
            if (user.UserName == null || user.Password == null)
            {
                user.LblMessage = "Please enter Username and Password";
                return View(user);
            }

            bool Athourised = false;
            int theId = 0;
            theId = user.LoginUser1(Athourised);
            if ( theId > 0)
            {
                CurrentUser cu = new CurrentUser{ID = theId, UserName =  user.UserName};
                Session["User"] = cu;
                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("ID", cu.ID.ToString());
                cookie.Values.Add("Name", cu.UserName);
                cookie.Values.Add("Password", user.Password);
                cookie.Expires = DateTime.Now.AddMinutes(300);
                Request.Cookies.Add(cookie);

                Response.Cookies.Add(cookie);
                return RedirectToAction("Index", "EmployeeRecord");
            }
            else
            {
                user.LblMessage = "User/Password does not match!";
            }
            return View(user);
        }

        public ActionResult SignOut()
        {
           
            Oss.Hrms.Models.Services.CurrentUser user = (Oss.Hrms.Models.Services.CurrentUser)Session["User"];
            if (user != null)
            {
                Session["User"] = null;
                HttpCookie currentUserCookie = HttpContext.Request.Cookies["UserLogin"];
                HttpContext.Response.Cookies.Remove("UserLogin");
            }

            return RedirectToAction("Login", "Home");
        }


        //public int LoginUser(bool authorised)
        //{
        //    db = new HrmsContext();
        //    int flag = 0;
        //    var v = (from t1 in db.Users
        //        where t1.Status == true && t1.UserName == this.UserName && t1.Password == this.Password)
        //        select new VM_User()
        //        {
        //            ID = o.ID,
        //            UserName = o.UserName,
        //            Password = o.Password
        //        }).FirstOrDefault();
        //    if (v != null)
        //    {
        //        flag = v.ID;
        //        //User.ID=flag
        //        //this.User.ID = flag;
        //        this.User.UserName = v.UserName;
        //        this.User.Password = v.Password;
        //    }
        //    if (flag > 0)
        //    {
        //        if (v.Status == false)
        //        {
        //            this.LblMessage = "You are not authorised!";
        //            return 0;
        //        }
        //    }
        //    else
        //    {
        //        this.LblMessage = "User / Password does not match!";
        //    }
        //    return flag;
        //}

    }
}