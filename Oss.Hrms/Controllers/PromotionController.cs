﻿using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using Oss.Hrms.Models.ViewModels.Promotion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Oss.Hrms.Controllers
{
    public class PromotionController : Controller
    {
        public DropDownData d;
        CurrentUser user;
        public PromotionController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            d = new DropDownData();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        // GET: Promotion
        [HttpGet]
        public ActionResult PromotionDetails()
        {
            if (user != null)
            {
                ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
                ViewBag.Department = new SelectList(d.GetDepartmentList(), "Value", "Text");
                ViewBag.Section = new SelectList(d.GetSectionList(), "Value", "Text");
                ViewBag.Designation = new SelectList(d.GetDesignationList(), "Value", "Text");
                ViewBag.Grade = new SelectList(d.GetGradeStaticList(), "Value", "Text");
                ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                ViewBag.Unit = new SelectList(d.GetUnitList(), "Value", "Text");
                ViewBag.BusinessUnit = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                VMPromotions model = new VMPromotions();
                model.VMEmployee = new VMEmployee();
                model.PromotionDate = DateTime.Now;
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        [HttpPost]
        public async Task<ActionResult> PromotionDetails(VMPromotions model)
        {
            ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
            ViewBag.Department = new SelectList(d.GetDepartmentList(), "Value", "Text");
            ViewBag.Section = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.Designation = new SelectList(d.GetDesignationList(), "Value", "Text");
            ViewBag.Grade = new SelectList(d.GetGradeStaticList(), "Value", "Text");
            ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
            ViewBag.Unit = new SelectList(d.GetUnitList(), "Value", "Text");
            ViewBag.BusinessUnit = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
            VMEmployee VMEmployees = new VMEmployee();

            VMPromotions vmPromotions = new VMPromotions();
            vmPromotions.VMEmployee = new VMEmployee();
            await Task.Run(() => vmPromotions.VMEmployee = VMEmployees.GetEmployeeDetails(model.EmployeeID));
            vmPromotions.PromotionDate = DateTime.Today;
            return View(vmPromotions);
        }

        [HttpPost]
        public async Task<ActionResult> CreatePromotion(VMPromotions model)
        {
            if (user != null)
            {
                ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
                ViewBag.Department = new SelectList(d.GetDepartmentList(), "Value", "Text");
                ViewBag.Section = new SelectList(d.GetSectionList(), "Value", "Text");
                ViewBag.Designation = new SelectList(d.GetDesignationList(), "Value", "Text");
                ViewBag.Grade = new SelectList(d.GetGradeStaticList(), "Value", "Text");
                ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                ViewBag.Unit = new SelectList(d.GetUnitList(), "Value", "Text");
                ViewBag.BusinessUnit = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                //VMPromotions vmPromotions = new VMPromotions();
                model.Entry_Date = DateTime.Today;
                model.Entry_By = user.ID.ToString();
                await Task.Run(() => model.GetCreatePromotion(model));

                return RedirectToAction("PromotionList", "Promotion");
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        [HttpGet]
        public async Task<ActionResult> PromotionList()
        {
            VMPromotions vmPromotions = new VMPromotions();
            await Task.Run(() => vmPromotions.GetPromotionList());
            return View(vmPromotions);
        }
    }
}