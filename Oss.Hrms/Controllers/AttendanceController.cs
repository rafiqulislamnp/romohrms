﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.Attendance;

//using Oss.Hrms.Models.ViewModels.Attendance;

namespace Oss.Hrms.Controllers
{
    public class AttendanceController : Controller
    {
        DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();
        CurrentUser user;
        VMShiftRule VMShiftRule;
        public AttendanceController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        [HttpGet]
        public async Task<ActionResult> AttendenceProcessManually()
        {
            DropDownData ddd = new DropDownData();
            ViewBag.BusinessUnitList = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
            ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
            VM_HRMS_Daily_Instant_Attendance model = new VM_HRMS_Daily_Instant_Attendance();
            return View(model);
        }


        [HttpPost]
        public async Task<ActionResult> AttendenceProcessManually(VM_HRMS_Daily_Instant_Attendance model)
        {



            //var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Employee_Shift_Assign_And_Modification] "
            //                                      + "@EmployeeId='" + list + "',"
            //                                      + "@BusinesUnitId='" + model.BU_Id + "',"
            //                                      + "@ShiftId='" + model.ShiftTypeId + "',"
            //                                      + "@StartDate='" + model.StartDate + "',"
            //                                      + "@EndDate='" + model.EndDate + "',"
            //                                      + "@EntryBy=1");


            return RedirectToAction("AttendenceProcessManually", "Attendance");
        }

        #region   Shift Management


        // Below Action used to View Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> ViewShiftType()
        {
            //if (Session["Role"] != null)
            {
                try
                {

                    VM_HRMS_Shift_Type model = new VM_HRMS_Shift_Type();
                    await Task.Run(() => model.LoadShiftTypeInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewShiftType", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        // Below Action used to Add Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> AddShiftType()
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.BusinessUnitList = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
                    VM_HRMS_Shift_Type model = new VM_HRMS_Shift_Type();
                    await Task.Run(() => model.LoadShiftTypeInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewShiftType", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> AddShiftType(VM_HRMS_Shift_Type model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    // DateTime.Now.ToString("t");
                    var intime = model.StartTime.ToString("T");
                    var outtime = model.EndTime.ToString("T");
                    HRMS_Shift_Type hrmsShiftType = new HRMS_Shift_Type
                    {
                        BU_Id = model.BU_Id,
                        ShiftName = model.ShiftName,
                        StartTime = model.StartTime,
                        EndTime = model.EndTime,
                        DeductionTime = model.DeductionTime,
                        ShiftNameWithTime = model.ShiftName + ' ' + intime + ' ' + outtime,
                        Entry_Date = DateTime.Now,
                        Entry_By = "1"
                    };
                    db.HrmsShiftTypes.Add(hrmsShiftType);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Shift Type Added Successfully.";
                    return RedirectToAction("ViewShiftType", "Attendance");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Shift Type Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewShiftType", "Attendance");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewShiftType", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Edit Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        public async Task<ActionResult> EditShiftType(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.BusinessUnitList = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
                    VM_HRMS_Shift_Type model = new VM_HRMS_Shift_Type();
                    await Task.Run(() => model = model.LoadSpecificShiftTypeInfo(id.Value));
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Shift Type Not Found!";
                return RedirectToAction("ViewShiftType", "Attendance");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Edit Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditShiftType(VM_HRMS_Shift_Type model)
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    var shiftInfo = db.HrmsShiftTypes.Where(x => x.ID == model.ID).FirstOrDefault();

                    var intime = model.StartTime.ToString("T");
                    var outtime = model.EndTime.ToString("T");
                    HRMS_Shift_Type hrmsShiftType = new HRMS_Shift_Type
                    {
                        ID = model.ID,
                        BU_Id = model.BU_Id,
                        ShiftName = model.ShiftName,
                        StartTime = model.StartTime,
                        EndTime = model.EndTime,
                        DeductionTime = model.DeductionTime,
                        ShiftNameWithTime = model.ShiftName + ' ' + intime + ' ' + outtime,
                        //Entry_Date = shiftInfo.Entry_Date,
                        //Entry_By = shiftInfo.Entry_By,
                        //Update_Date = DateTime.Now,
                        Update_By = "1"
                    };
                    db.Entry(hrmsShiftType).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Shift Type Updated Successfully.";
                    return RedirectToAction("ViewShiftType", "Attendance");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewShiftType", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");

        }



        // Below Action used to Delete Shift Type Information
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28

        [HttpGet]
        public async Task<ActionResult> DeleteShiftType(int? id)
        {

            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Shift Type Not Found!";
                    return RedirectToAction("ViewShiftType", "Attendance");
                }
                HRMS_Shift_Type shiftType = await db.HrmsShiftTypes.FindAsync(id);
                if (shiftType != null)
                {
                    try
                    {
                        db.HrmsShiftTypes.Remove(shiftType);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Shift Type Deleted Successfully.";
                        return RedirectToAction("ViewShiftType", "Attendance");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Shift Type Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewShiftType", "Attendance");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewShiftType", "Attendance");
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewShiftType", "Attendance");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Employee Shift Assign
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> EmployeeAttendanceShifing()
        {
            // if (Session["Role"] != null)
            {
                try
                {

                    VM_HRMS_Employee_Shift_Assign model = new VM_HRMS_Employee_Shift_Assign();
                    // await Task.Run(() => model.LoadEmployeeShiftAssignInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("EmployeeAttendanceShifing", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Employee_Shift_Assign
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> EmployeeAttendanceShifing(VM_HRMS_Employee_Shift_Assign model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    var dt1 = model.StartDate.ToString("yyyy-MM-dd");
                    var dt2 = model.EndDate.ToString("yyyy-MM-dd");

                    var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[Sp_Attendance_Shifting] "
                                                              + "@AttendanceDate='" + dt1 + "',"
                                                              + "@ShiftingDate='" + dt2 + "'");
                    // this.db.ExecuteSqlCommand.CommandTimeout = 180;
                    if (a > 0)
                    {
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Employee Attendance Shifted Successfully.";
                        return RedirectToAction("EmployeeAttendanceShifing", "Attendance");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Employee Attendance Shifting Fail!";
                    return RedirectToAction("EmployeeAttendanceShifing", "Attendance");

                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Shift Type Can't Add, It is Already Exist!";
                        return RedirectToAction("EmployeeAttendanceShifing", "Attendance");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("EmployeeAttendanceShifing", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }




        // Below Action used to Employee Shift Assign
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> EmployeeShiftAssign()
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.BusinessUnitList = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                    VM_HRMS_Employee_Shift_Assign model = new VM_HRMS_Employee_Shift_Assign();
                    // await Task.Run(() => model.LoadEmployeeShiftAssignInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("EmployeeShiftAssign", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Employee_Shift_Assign
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> EmployeeShiftAssign(VM_HRMS_Employee_Shift_Assign model)
        {


            try
            {
                if (model.EmployeeIds.Count > 0)
                {
                    model.EmployeeIds = model.EmployeeIds[0].TrimEnd(',').Split(',').ToList();
                    foreach (var b in model.EmployeeIds)
                    {
                        var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Employee_Shift_Assign_And_Modification] "
                                                         + "@EmployeeId='" + b + "',"
                                                         + "@BusinesUnitId='" + model.BU_Id + "',"
                                                         + "@ShiftId='" + model.ShiftTypeId + "',"
                                                         + "@StartDate='" + model.StartDate + "',"
                                                         + "@EndDate='" + model.EndDate + "',"
                                                         + "@EntryBy=1");

                        if (a > 0)
                        {
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Employee Shift Assign/Updated Successfully.";

                        }
                    }
                    #region Old Running Code Writting By Anis vai
                    //var list = "";
                    //list = String.Join(",", model.EmployeeIds);

                    // var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Employee_Shift_Assign_And_Modification] "
                    //                                       + "@EmployeeId='" + list + "',"
                    //                                       + "@BusinesUnitId='" + model.BU_Id + "',"
                    //                                       + "@ShiftId='" + model.ShiftTypeId + "',"
                    //                                       + "@StartDate='" + model.StartDate + "',"
                    //                                       + "@EndDate='" + model.EndDate + "',"
                    //                                       + "@EntryBy=1");
                    //// this.db.ExecuteSqlCommand.CommandTimeout = 180;
                    // if (a > 0)
                    // {
                    //     Session["success_div"] = "true";
                    //     Session["success_msg"] = "Employee Shift Assign/Updated Successfully.";
                    //     return RedirectToAction("EmployeeShiftAssign", "Attendance");
                    // }
                    //Session["warning_div"] = "true";
                    //Session["warning_msg"] = "Employee Shift Can't Assign, It is Already Exist!";
                    //return RedirectToAction("EmployeeShiftAssign", "Attendance");
                    #endregion
                }

                Session["warning_div"] = "true";
                Session["warning_msg"] = "Please Select Employee ID";
                return RedirectToAction("EmployeeShiftAssign", "Attendance");

            }
            catch (Exception ex)
            {
                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Shift Type Can't Add, It is Already Exist!";
                    return RedirectToAction("EmployeeShiftAssign", "Attendance");
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                return RedirectToAction("EmployeeShiftAssign", "Attendance");
            }

            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to View Employee Shift Assign
        // Database: CrimsonERP
        // Table Name :HRMS_Employee_Shift_Assign
        // Domain Model Name: HRMS_Employee_Shift_Assign
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> ViewShiftAssign()
        {
            // if (Session["Role"] != null)
            {
                //try
                //{
                DropDownData ddd = new DropDownData();
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                VM_HRMS_Employee_Shift_Assign model = new VM_HRMS_Employee_Shift_Assign();
                // await Task.Run(() => model.LoadEmployeeShiftAssignInfo());
                return View(model);
                //}
                //catch (Exception ex)
                //{
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Error Happend. " + ex.Message;
                //    return RedirectToAction("Index", "Attendance");
                //}
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to View Employee Shift Assign
        // Database: CrimsonERP
        // Table Name :HRMS_Employee_Shift_Assign
        // Domain Model Name: HRMS_Employee_Shift_Assign
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> ViewShiftAssign(VM_HRMS_Employee_Shift_Assign model)
        {
            // if (Session["Role"] != null)
            {

                //try
                //{
                DropDownData ddd = new DropDownData();
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                await Task.Run(() => model.SearchEmployeeShiftInformation(model.ShiftTypeId, model.StartDate, model.EndDate));
                return View(model);
                //}
                //catch (Exception ex)
                //{
                //    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                //    {
                //        Session["warning_div"] = "true";
                //        Session["warning_msg"] = "Assign Shift Shift Information Not Found !";
                //        return RedirectToAction("ViewShiftAssign", "Attendance");
                //    }
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                //    return RedirectToAction("ViewShiftAssign", "Attendance");
                //}
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        //ShiftNotAssignEmployeeInformation

        // Below Action used to View Employee Shift Assign
        // Database: CrimsonERP
        // Table Name :HRMS_Employee_Shift_Assign
        // Domain Model Name: HRMS_Employee_Shift_Assign
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> ViewNotShiftAssignInfo()
        {
            // if (Session["Role"] != null)
            {
                //try
                //{
                DropDownData ddd = new DropDownData();
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                VM_HRMS_Employee_Shift_Assign model = new VM_HRMS_Employee_Shift_Assign();
                // await Task.Run(() => model.LoadEmployeeShiftAssignInfo());
                return View(model);
                //}
                //catch (Exception ex)
                //{
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Error Happend. " + ex.Message;
                //    return RedirectToAction("Index", "Attendance");
                //}
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to View Employee Shift Assign
        // Database: CrimsonERP
        // Table Name :HRMS_Employee_Shift_Assign
        // Domain Model Name: HRMS_Employee_Shift_Assign
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> ViewNotShiftAssignInfo(VM_HRMS_Employee_Shift_Assign model)
        {
            // if (Session["Role"] != null)
            {

                //try
                //{
                DropDownData ddd = new DropDownData();
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                await Task.Run(() => model.ShiftNotAssignEmployeeInformation(model.StartDate, model.EndDate));
                return View(model);
                //}
                //catch (Exception ex)
                //{
                //    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                //    {
                //        Session["warning_div"] = "true";
                //        Session["warning_msg"] = "Assign Shift Shift Information Not Found !";
                //        return RedirectToAction("ViewShiftAssign", "Attendance");
                //    }
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                //    return RedirectToAction("ViewShiftAssign", "Attendance");
                //}
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public async Task<ActionResult> ViewShiftRule(int Id)
        {
            //if (Session["Role"] != null)
            {
                try
                {

                    VMShiftRule model = new VMShiftRule();
                    model.ShiftTypeId = Id;
                    await Task.Run(() => model.LoadShiftRule(Id));
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewShiftRule", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult AddShiftRule(int Id)
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    VMShiftRule = new VMShiftRule();
                    VMShiftRule.ShiftTypeId = Id;
                    return View(VMShiftRule);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewShiftRule", "Attendance", new { Id = Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<ActionResult> AddShiftRule(VMShiftRule model)
        {
            //if (Session["Role"] != null)
            {
                try
                {

                    HRMS_Shift_Rules hrmsShiftRule = new HRMS_Shift_Rules
                    {
                        ShiftTypeId = model.ShiftTypeId,
                        RuleName = model.RuleName,
                        Hour = model.Hour,
                        DeductionMinute = model.DeductionMinute,
                        Entry_Date = DateTime.Now,
                        Entry_By = "1"
                    };
                    db.HrmsShiftRules.Add(hrmsShiftRule);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Shift Rule Added Successfully.";
                    return RedirectToAction("ViewShiftRule", "Attendance", new { Id = model.ShiftTypeId });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Shift Rule Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewShiftRule", "Attendance", new { Id = model.ShiftTypeId });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewShiftRule", "Attendance", new { Id = model.ShiftTypeId });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> EditShiftRule(int Id)
        {
            //if (Session["Role"] != null)
            {
                VMShiftRule model = new VMShiftRule();
                await Task.Run(() => model = model.LoadSpecificShiftRuleInfo(Id));
                return View(model);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditShiftRule(VMShiftRule model)
        {
            // if (Session["Role"] != null)
            {
                try
                {

                    var update = db.HrmsShiftRules.Where(a => a.ID == model.ID).FirstOrDefault();

                    update.RuleName = model.RuleName;
                    update.Hour = model.Hour;
                    update.DeductionMinute = model.DeductionMinute;

                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Shift Rule Updated Successfully.";
                    return RedirectToAction("ViewShiftRule", "Attendance", new { Id = model.ShiftTypeId });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewShiftRule", "Attendance", new { Id = model.ShiftTypeId });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");

        }

        [HttpGet]
        public async Task<ActionResult> DeleteShiftRule(int id, int sid)
        {

            //if (Session["Role"] != null)
            {

                HRMS_Shift_Rules shiftrule = await db.HrmsShiftRules.FindAsync(id);
                if (shiftrule != null)
                {
                    try
                    {
                        db.HrmsShiftRules.Remove(shiftrule);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Shift Rule Deleted Successfully.";
                        return RedirectToAction("ViewShiftRule", "Attendance", new { Id = sid });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Shift Rule Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewShiftRule", "Attendance", new { Id = sid });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewShiftRule", "Attendance", new { Id = sid });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewShiftRule", "Attendance", new { Id = sid });
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        #endregion


        #region Attendance Management
        // Below Action used to View Remarks over Attendance
        // Database: CrimsonERP
        // Table Name :HRMS_Attendance_Remarks
        // Domain Model Name: HRMS_Attendance_Remarks
        // View Model Name: VM_HRMS_Attendance_Remarks
        // Developed by: Abid
        // Date: 2018-03-29

        [HttpGet]
        public async Task<ActionResult> ViewAttendanceRemarks()
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    VM_HRMS_Attendance_Remarks model = new VM_HRMS_Attendance_Remarks();
                    await Task.Run(() => model.GetLoadData());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAttendanceRemarks", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");


        }

        // Below Action used to Add Remarks over Attendance
        // Database: CrimsonERP
        // Table Name :HRMS_Attendance_Remarks
        // Domain Model Name: HRMS_Attendance_Remarks
        // View Model Name: VM_HRMS_Attendance_Remarks
        // Developed by: Abid
        // Date: 2018-03-31
        [HttpGet]
        public async Task<ActionResult> AddAttendanceRemarks()
        {
            // if (Session["Role"] != null)
            {
                VM_HRMS_Attendance_Remarks model = new VM_HRMS_Attendance_Remarks();
                return View(model);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Add Remarks over Attendance
        // Database: CrimsonERP
        // Table Name :HRMS_Attendance_Remarks
        // Domain Model Name: HRMS_Attendance_Remarks
        // View Model Name: VM_HRMS_Attendance_Remarks
        // Developed by: Abid
        // Date: 2018-03-31
        [HttpPost]
        public async Task<ActionResult> AddAttendanceRemarks(VM_HRMS_Attendance_Remarks model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Attendance_Remarks hrms_attendance_remarks = new HRMS_Attendance_Remarks { Remarks = model.Remarks, Entry_Date = DateTime.Now };
                    db.HrmsAttendanceRemarks.Add(hrms_attendance_remarks);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Remarks Added Successfully.";
                    return RedirectToAction("ViewAttendanceRemarks", "Attendance");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAttendanceRemarks", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Remarks over Attendance
        // Database: CrimsonERP
        // Table Name :HRMS_Attendance_Remarks
        // Domain Model Name: HRMS_Attendance_Remarks
        // View Model Name: VM_HRMS_Attendance_Remarks
        // Developed by: Abid
        // Date: 2018-03-31
        [HttpGet]
        public async Task<ActionResult> EditAttendanceRemarks(int? id)
        {
            // if (Session["Role"] != null)
            {
                if (id != null)
                {
                    VM_HRMS_Attendance_Remarks model = new VM_HRMS_Attendance_Remarks();
                    await Task.Run(() => model.SelectSingle(id.Value));
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Remarks Not Found!";
                return RedirectToAction("ViewAttendanceRemarks", "Attendance");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Remarks over Attendance
        // Database: CrimsonERP
        // Table Name :HRMS_Attendance_Remarks
        // Domain Model Name: HRMS_Attendance_Remarks
        // View Model Name: VM_HRMS_Attendance_Remarks
        // Developed by: Abid
        // Date: 2018-03-31 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAttendanceRemarks(VM_HRMS_Attendance_Remarks model)
        {
            //  if (Session["Role"] != null)
            {
                try
                {
                    db.Entry(model.hrms_attendance_remarks).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Remarks Updated Successfully.";
                    return RedirectToAction("ViewAttendanceRemarks", "Attendance");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAttendanceRemarks", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Delete Remarks on Attendance
        // Database: CrimsonERP
        // Table Name :HRMS_Attendance_Remarks
        // Domain Model Name: HRMS_Attendance_Remarks
        // View Model Name: VM_HRMS_Attendance_Remarks
        // Developed by: Abid
        // Date: 2018-03-31 
        [HttpGet]
        public async Task<ActionResult> DeleteAttendanceRemarks(int? id)
        {
            // if (Session["Role"] != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Remarks ID Not Found!";
                    return RedirectToAction("ViewAttendanceRemarks", "Attendance");
                }
                HRMS_Attendance_Remarks hrms_attendance_remarks = await db.HrmsAttendanceRemarks.FindAsync(id);
                if (hrms_attendance_remarks != null)
                {
                    db.HrmsAttendanceRemarks.Remove(hrms_attendance_remarks);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Remarks Deleted Successfully.";
                    return RedirectToAction("ViewAttendanceRemarks", "Attendance");
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAttendanceRemarks", "Attendance");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        // Below Action used to Add Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> EmployeeAttendanceProcess()
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                    VM_HRMS_Daily_Instant_Attendance model = new VM_HRMS_Daily_Instant_Attendance();
                    //  await Task.Run(() => model.LoadShiftTypeInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("EmployeeAttendanceProcess", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> EmployeeAttendanceProcess(VM_HRMS_Daily_Instant_Attendance model)
        {
            // if (Session["Role"] != null)
            {
                DropDownData ddd = new DropDownData();
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                try
                {
                    await Task.Run(() => model.EmployeeAttendanceProcess(model.Date, model.Late, model.ShiftTypeId, Session["User"].ToString()));
                    // return View(model);
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Employee Attendance Processed Successfully.";
                    return RedirectToAction("EmployeeAttendanceProcess", "Attendance");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Employee Attendance Can't Process !";
                        return RedirectToAction("EmployeeAttendanceProcess", "Attendance");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("EmployeeAttendanceProcess", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Add Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> SingleEmployeeAttendanceProcess()
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                    VM_HRMS_Daily_Instant_Attendance model = new VM_HRMS_Daily_Instant_Attendance();
                    //  await Task.Run(() => model.LoadShiftTypeInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("SingleEmployeeAttendanceProcess", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Shift Type
        // Database: CrimsonERP
        // Table Name :HRMS_Shift_Type
        // Domain Model Name: HRMS_Shift_Type
        // View Model Name: VM_HRMS_Shift_Type
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> SingleEmployeeAttendanceProcess(VM_HRMS_Daily_Instant_Attendance model)
        {
            // if (Session["Role"] != null)
            {
                DropDownData ddd = new DropDownData();
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                try
                {
                    await Task.Run(() => model.SingleEmployeeAttendanceProcess(model.Date, model.CardNo, model.ShiftTypeId, Session["User"].ToString()));
                    // return View(model);
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Employee Attendance Processed Successfully.";
                    return RedirectToAction("SingleEmployeeAttendanceProcess", "Attendance");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Employee Attendance Can't Process !";
                        return RedirectToAction("SingleEmployeeAttendanceProcess", "Attendance");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("SingleEmployeeAttendanceProcess", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to View Employee Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> ViewEmployeeAttendance()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeNamewithAll(), "Value", "Text");
                    //VM_HRMS_Daily_Instant_Attendance model = new VM_HRMS_Daily_Instant_Attendance();

                    // await Task.Run(() => model.LoadEmployeeShiftAssignInfo());
                    return View();
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewEmployeeAttendance", "Attendance");
                }
            }


            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        // Below Action used to View Employee Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> ViewEmployeeAttendance(VM_HRMS_Daily_Instant_Attendance model)
        {


            //if (Session["Role"] != null)
            {
                DropDownData ddd = new DropDownData();
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeNamewithAll(), "Value", "Text");
                try
                {
                    var Attendancelist = new List<VM_HRMS_Daily_Instant_Attendance>();
                    if (model.ShiftTypeId == 100)
                    {
                        Attendancelist = db.Database
                           .SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                               $@"Exec [dbo].[sp_ViewDateRangeWiseAttendance] '{model.Date}', '{model.toDate}'")
                           .ToList();
                        //ViewBag.AttendanceList = Attendancelist;
                    }
                    else
                    {
                        Attendancelist = db.Database
                           .SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                               $@"Exec [dbo].[sp_ViewShiftWiseAttendance] '{model.Date}', '{model.toDate}', '{model.ShiftTypeId}'")
                           .ToList();
                    }
                    ViewBag.AttendanceList = Attendancelist;
                    return View(model);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Attendance Information Not Found !";
                        return RedirectToAction("ViewEmployeeAttendance", "Attendance");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewEmployeeAttendance", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }








        // Below Action used to View Employee Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> ViewAttendanceRecord()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeNamewithAll(), "Value", "Text");
                    //VM_HRMS_Daily_Instant_Attendance model = new VM_HRMS_Daily_Instant_Attendance();

                    // await Task.Run(() => model.LoadEmployeeShiftAssignInfo());
                    return View();
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAttendanceRecord", "Attendance");
                }
            }


            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        // Below Action used to View Employee Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> ViewAttendanceRecord(VM_HRMS_Daily_Instant_Attendance model)
        {

            Oss.Hrms.Models.Services.CurrentUser user1 = (Oss.Hrms.Models.Services.CurrentUser)Session["User"];
            {
                DropDownData ddd = new DropDownData();
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeNamewithAll(), "Value", "Text");
                try
                {
                    var Attendancelist = new List<VM_HRMS_Daily_Instant_Attendance>();

                    if (model.ShiftTypeId == 100)
                    {
                        Attendancelist = db.Database
                            .SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                                $@"Exec [dbo].[sp_ViewDateRangeWiseAttendanceRecord] '{model.Date}', '{model.toDate}','{user1.ID}'")
                            .ToList();
                        //ViewBag.AttendanceList = Attendancelist;
                    }
                    else
                    {
                        Attendancelist = db.Database
                            .SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                                $@"Exec [dbo].[sp_ViewShiftWiseAttendanceRecord] '{model.Date}', '{model.toDate}', '{model.ShiftTypeId}','{user1.ID}'")
                            .ToList();
                    }
                    ViewBag.AttendanceList = Attendancelist;
                    return View(model);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Attendance Information Not Found !";
                        return RedirectToAction("ViewAttendanceRecord", "Attendance");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAttendanceRecord", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to View Employee Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> ViewSpecificEmployeeAttendanceRecord()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeList(), "Value", "Text");

                    //VM_HRMS_Daily_Instant_Attendance model = new VM_HRMS_Daily_Instant_Attendance();

                    // await Task.Run(() => model.LoadEmployeeShiftAssignInfo());
                    return View();
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewSpecificEmployeeAttendanceRecord", "Attendance");
                }
            }


            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        // Below Action used to View Employee Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> ViewSpecificEmployeeAttendanceRecord(VM_HRMS_Daily_Instant_Attendance model)
        {


            //if (Session["Role"] != null)
            {
                Oss.Hrms.Models.Services.CurrentUser user1 = (Oss.Hrms.Models.Services.CurrentUser)Session["User"];

                DropDownData ddd = new DropDownData();
                ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeList(), "Value", "Text");
                try
                {
                    var Attendancelist = new List<VM_HRMS_Daily_Instant_Attendance>();
                    Attendancelist = db.Database
                        .SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                            $@"Exec [dbo].[sp_ViewSpecificEmployeeDateRangeWiseAttendanceRecord] '{model.Date}', '{model.toDate}','{model.EmployeeId}','{user1.ID}'")
                        .ToList();

                    ViewBag.AttendanceList = Attendancelist;
                    return View(model);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Attendance Information Not Found !";
                        return RedirectToAction("ViewSpecificEmployeeAttendanceRecord", "Attendance");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewSpecificEmployeeAttendanceRecord", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to View Employee Personal Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpGet]
        public async Task<ActionResult> ViewPersonalAttendance()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeNamewithAll(), "Value", "Text");
                    VM_HRMS_Daily_Instant_Attendance model = new VM_HRMS_Daily_Instant_Attendance();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewPersonalAttendance", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        // Below Action used to View Employee Personal Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        [HttpPost]
        public async Task<ActionResult> ViewPersonalAttendance(VM_HRMS_Daily_Instant_Attendance model)
        {
            //  if (Session["Role"] != null)
            {
                var empId = 6;
                DropDownData ddd = new DropDownData();
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeNamewithAll(), "Value", "Text");
                try
                {
                    var Attendancelist = new List<VM_HRMS_Daily_Instant_Attendance>();
                    if (model.ShiftTypeId == 100)
                    {
                        Attendancelist = db.Database
                           .SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                               $@"Exec [dbo].[sp_ViewDateRangeWiseAttendancePersonal] '{model.Date}', '{model.toDate}', '{empId}'")
                           .ToList();
                        //ViewBag.AttendanceList = Attendancelist;
                    }
                    else
                    {
                        Attendancelist = db.Database
                           .SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                               $@"Exec [dbo].[sp_ViewShiftWiseAttendancePersonal] '{model.Date}', '{model.toDate}', '{model.ShiftTypeId}', '{empId}'")
                           .ToList();

                    }
                    ViewBag.AttendanceList = Attendancelist;
                    return View(model);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Attendance Information Not Found !";
                        return RedirectToAction("ViewPersonalAttendance", "Attendance");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewPersonalAttendance", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Employee Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        public async Task<ActionResult> EditEmployeeAttendance(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.AttendanceList = new SelectList(ddd.GetAttendanceRemarks(), "Value", "Text");
                    VM_HRMS_Daily_Instant_Attendance model = new VM_HRMS_Daily_Instant_Attendance();
                    //await Task.Run(() => model = model.LoadSpecificEmployeeDailyAttendance(id.Value));
                    await Task.Run(() => model = model.LoadSpecificEmployeeDailyAttendanceRecord(id.Value));
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Attendnance ID Not Found!";
                return RedirectToAction("ViewEmployeeAttendance", "Attendance");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Edit Employee Attendance Information
        // Database: CrimsonERP
        // Table Name :HRMS_Daily_Attendance
        // Domain Model Name: VM_HRMS_Daily_Instant_Attendance
        // View Model Name: HRMS_Daily_Attendance
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-28
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> EditEmployeeAttendance(VM_HRMS_Daily_Instant_Attendance model)
        //{
        //   // if (Session["Role"] != null)
        //    {
        //        try
        //        {
        //            var intime = model.InTime.ToString("T");
        //            var outtime = model.OutTime.ToString("T");
        //            HRMS_Daily_Attendance hrmsDailyAttendance = new HRMS_Daily_Attendance
        //            {
        //                ID = model.ID,
        //                Date = model.Date,
        //                InTime = model.InTime,
        //                OutTime = model.OutTime,
        //                TotalTime = model.TotalTime,
        //                AttendanceStatus = model.AttendanceStatus,
        //                Remarks = model.Remarks,
        //                OverTime = model.Overtime1,
        //                ShiftTypeId = model.ShiftTypeId,
        //                EmployeeId = model.EmployeeId,
        //                Update_Date = DateTime.Now,
        //                Update_By = "1"
        //            };
        //            db.Entry(hrmsDailyAttendance).State = EntityState.Modified;
        //            await db.SaveChangesAsync();
        //            Session["success_div"] = "true";
        //            Session["success_msg"] = "Attendance Updated Successfully.";
        //            return RedirectToAction("ViewEmployeeAttendance", "Attendance");
        //        }
        //        catch (Exception ex)
        //        {
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Error Happened." + ex.Message;
        //            return RedirectToAction("ViewEmployeeAttendance", "Attendance");
        //        }
        //    }
        //    //Session["warning_div"] = "true";
        //    //Session["warning_msg"] = "Please Login to continue..!";
        //    //return RedirectToAction("Login", "Account");

        //}


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditEmployeeAttendance(VM_HRMS_Daily_Instant_Attendance model)
        {
            {
                // DateTime date = DateTime.ParseExact(model.InTime2);

                //var dt3 = DateTime.ParseExact(model.InTime2, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
                //var dt4 = DateTime.ParseExact(model.OutTime2, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
                DateTime dt3 = Convert.ToDateTime(model.InTime2, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                DateTime dt4 = Convert.ToDateTime(model.OutTime2, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                var dt1 = dt3.ToString("G");
                var dt2 = dt4.ToString("G");

                // DateTime.Now.ToString("yyyyMMddHHmmss")
                try
                {
                    var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[Sp_AttendanceUpdate] "
                                                          + "@sl='" + model.ID + "',"
                                                          + "@dt='" + model.Date + "',"
                                                          + "@firstPunch='" + dt1 + "',"
                                                          + "@lastPunch='" + dt2 + "',"
                                                          + "@shifttypeId='" + model.ShiftTypeId + "',"
                                                          + "@attstatus='" + model.AttendanceStatus + "',"
                                                          + "@remarks='" + model.Remarks + "',"
                                                          + "@eid='" + model.EmployeeId + "',"
                                                          + "@updateby ='1'");
                    if (a > 0)
                    {
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Employee Attendance Updated Successfully.";
                        return RedirectToAction("ViewAttendanceRecord", "Attendance");
                    }
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Attendance Updated Successfully.";
                    return RedirectToAction("ViewAttendanceRecord", "Attendance");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAttendanceRecord", "Attendance");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");

        }

        #endregion

        ///<summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetUnitList(int bUnitId)
        {
            var list = db.Units.ToList().Where(u => u.BusinessUnit_ID == bUnitId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This json part will retrive related Dept based on unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetDeptList(int unitId)
        {
            //if (Session["Role"] != null)//&& Session["Role"].ToString() == "Admin")
            //{
            var list = new List<object>();
            foreach (var List in db.Departments.ToList().Where(u => u.Unit_ID == unitId))
            {
                list.Add(new { Text = List.DeptName, Value = List.ID });
            }
            var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
            return retval;

            //}
            //return null;
        }


        /// <summary>
        /// This json part will retrive related section based on Department.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetSectionList(int deptId)
        {
            //if (Session["Role"] != null)
            //{
            HrmsContext db = new HrmsContext();
            var list = new List<object>();
            foreach (var List in db.Sections.ToList().Where(u => u.Dept_ID == deptId))
            {
                list.Add(new { Text = List.SectionName, Value = List.ID });
            }
            var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
            return retval;
            //}
            //return null;

        }


        /// <summary>
        /// This json part will retrive Employee based on section Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public JsonResult GetEmployeeIDforEmployeeShiftAssign(int sectionid)
        {
            //if (Session["Role"] != null)
            //{
            var list = db.Employees.ToList().Where(u => u.Section_Id == sectionid && u.Present_Status == 1);
            var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
            return retval;
            //}
            //return null;
        }
        [HttpGet]
        public async Task<ActionResult> AttendanceProcessManuallyByDate()
        {
            try
            {
                //HRMS_Shift_Type
                DropDownData dropDownData = new DropDownData();

                VMAttendanceProcessManually vmAttendanceProcessManually = new VMAttendanceProcessManually();
                vmAttendanceProcessManually.Date = DateTime.Now;
                ViewBag.HrmsShiftTypes = new SelectList(dropDownData.GetHrmsShiftTypesDropDown(), "Value", "Text");

                return View(vmAttendanceProcessManually);



            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happend. " + ex.Message;
                return RedirectToAction(nameof(AttendanceProcessManuallyByDate));
            }


        }

        [HttpPost]
        public async Task<ActionResult> AttendanceProcessManuallyByDate(VMAttendanceProcessManually vmAttendanceProcessManually)
        {
            try
            {
                DropDownData dropDownData = new DropDownData();

                string date = vmAttendanceProcessManually.Date.ToString("yyyy-MM-dd");
              db.Database.CommandTimeout = 6000;
                if (vmAttendanceProcessManually.HRMS_Shift_TypeFk > 0)
                {                 
                    var b = db.Database.ExecuteSqlCommand($@"Exec [dbo].[Auto_Daily_Attendance_ProcessManually] '{date}', '{0}', '{vmAttendanceProcessManually.HRMS_Shift_TypeFk}', '{"Syatem"}'");
                  
                    if (b > 0)
                    {
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Attendance Proced Successfully.";
                    }
                }
                else
                {
                    int[] departments = { 4, 1014, 1012, 1015, 1010, 1011, 1009, 1013, 1018, 1019, 1020, 1021 };

                    foreach (var department in departments)
                    {
                        var b = db.Database.ExecuteSqlCommand($@"Exec [dbo].[Auto_Daily_Attendance_ProcessManually] '{date}', '{0}', '{department}', '{"Syatem"}'");
                       if (b > 0)
                        {
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Attendance Proced Successfully.";
                        }
                    }
                }
                ViewBag.HrmsShiftTypes = new SelectList(dropDownData.GetHrmsShiftTypesDropDown(), "Value", "Text");

                return View(vmAttendanceProcessManually);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happend. " + ex.Message;
                return RedirectToAction(nameof(AttendanceProcessManuallyByDate));
            }
        }
        [HttpGet]
        public async Task<ActionResult> AttendanceProcessHistoryManuallyByDate()
        {
            try
            {
                //HRMS_Shift_Type
                DropDownData dropDownData = new DropDownData();

                VMAttendanceProcessManually vmAttendanceProcessManually = new VMAttendanceProcessManually();
                vmAttendanceProcessManually.Date = DateTime.Now;
                ViewBag.HrmsShiftTypes = new SelectList(dropDownData.GetHrmsShiftTypesDropDown(), "Value", "Text");

                return View(vmAttendanceProcessManually);



            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happend. " + ex.Message;
                return RedirectToAction(nameof(AttendanceProcessHistoryManuallyByDate));
            }


        }

        [HttpPost]
        public async Task<ActionResult> AttendanceProcessHistoryManuallyByDate(VMAttendanceProcessManually vmAttendanceProcessManually)
        {
            try
            {
                DropDownData dropDownData = new DropDownData();

                string date = vmAttendanceProcessManually.Date.ToString("yyyy-MM-dd");
                db.Database.CommandTimeout = 6000;
                if (vmAttendanceProcessManually.HRMS_Shift_TypeFk > 0)
                {
                    var a = db.Database.ExecuteSqlCommand($@"Exec [dbo].[Auto_Attendance_Process_HistoryManually] '{date}', '{0}', '{vmAttendanceProcessManually.HRMS_Shift_TypeFk}', '{"Syatem"}'");

                    if (a > 0)
                    {
                        //ViewBag.Success_Msg = "Attendance Proced Successfully.";
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Attendance Proced Successfully.";
                    }
                }
                else
                {
                    int[] departments = { 4, 1014, 1012, 1015, 1010, 1011, 1009, 1013, 1018, 1019, 1020, 1021 };

                    foreach (var department in departments)
                    {
                        var a = db.Database.ExecuteSqlCommand($@"Exec [dbo].[Auto_Attendance_Process_HistoryManually] '{date}', '{0}', '{department}', '{"Syatem"}'");
                        if (a > 0)
                        {

                            Session["success_div"] = "true";
                            Session["success_msg"] = "Attendance Proced Successfully.";
                        }
                    }
                }
                ViewBag.HrmsShiftTypes = new SelectList(dropDownData.GetHrmsShiftTypesDropDown(), "Value", "Text");
                return View(vmAttendanceProcessManually);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happend. " + ex.Message;
                return RedirectToAction(nameof(AttendanceProcessHistoryManuallyByDate));
            }
        }


    }
}
