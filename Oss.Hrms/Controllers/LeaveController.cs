﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.Attendance;
using Oss.Hrms.Models.ViewModels.HRMS;
using Oss.Hrms.Models.ViewModels.Leave;
using Oss.Hrms.Helper;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using Microsoft.ApplicationInsights.WindowsServer;


namespace Oss.Hrms.Controllers
{
    public class LeaveController : Controller
    {
        DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();
        CurrentUser user;

        public LeaveController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        // GET: Leave
        public ActionResult Index()
        {
            return View();
        }

        #region HRMS Employee's Offday Assign....

        //Associated View Model----


        /// <summary>
        /// Cumulative view of Assigned Offdays to the Employees-----
        /// Database: CrimsonERP
        /// Table: [HRMS_Employee], [HRMS_Employee_OffDay]
        /// Date: 05.04.18
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> IndexEmployeeOffDay()
        {

            //if (Session["Role"] != null)
            //{
            //    try
            //    {
            VM_HRMS_Employee_OffDay offDayEmp = new VM_HRMS_Employee_OffDay();
            await Task.Run(() => offDayEmp.OffDaysList());
            return View(offDayEmp);
            //    }
            //    catch (Exception ex)
            //    {
            //        Session["warning_div"] = "true";
            //        Session["warning_msg"] = "Error Happened." + ex.Message;
            //        return RedirectToAction("IndexEmployeeOffDay", "Leave");
            //    }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Load form for Assign Employee's Off days....
        /// </summary>
        /// <returns></returns>
        public ActionResult AddEmployeeOffDay()
        {
            //if (Session["Role"] != null)
            //{
            //try
            // {
           // ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
            ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforOffDayAssign(), "Value", "Text");
            VM_HRMS_Employee_OffDay offDayEmp = new VM_HRMS_Employee_OffDay();
            return View(offDayEmp);
            //}
            //catch (Exception ex)
            //{
            //    Session["warning_div"] = "true";
            //    Session["warning_msg"] = "Error Happened." + ex.Message;
            //    return RedirectToAction("IndexEmployeeOffDay", "Leave");
            //}
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Post Employee Off Day Information 
        /// Database: CrimsonERP
        /// Table:[HRMS_Employee_OffDay]
        /// </summary>
        /// <param name="empOffDay"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddEmployeeOffDay(VM_HRMS_Employee_OffDay empOffDay)
        {
            //if (Session["Role"] != null)
            //{
            var offDays = (db.HrmsEmployeeOffDays.Where(o => o.EmployeeId == empOffDay.EmployeeId
                                                             && o.OffDay == empOffDay.OffDay).Select(o => o.OffDay)
                .FirstOrDefault());
            if (offDays != empOffDay.OffDay)
            {
                try
                {
                    //

                  //  ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
                    ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforOffDayAssign(), "Value", "Text");
                    HRMS_Employee_OffDay offDay = new HRMS_Employee_OffDay
                    {
                        EmployeeId = empOffDay.EmployeeId,
                        OffDay = empOffDay.OffDay,
                        Status = empOffDay.Status,
                        Entry_By = "1",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsEmployeeOffDays.Add(offDay);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Off Day Added Successfully.";
                    return RedirectToAction("IndexEmployeeOffDay", "Leave");
                }
                catch (Exception ex)
                {
                    if (ex.Message ==
                        "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Off Day Can't Add, It is Already Exist!";
                        return RedirectToAction("IndexEmployeeOffDay", "Leave");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("IndexEmployeeOffDay", "Leave");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Off Day Can't Add, It is Already Exist!";
            return RedirectToAction("IndexEmployeeOffDay", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        /// <summary>
        /// Below Action used to Delete Employee Off Day Information
        /// Database: CrimsonERP
        /// Table/Domain Name : HRMS_Employee_OffDay
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpGet]
        public async Task<ActionResult> DeleteEmployeeOffDay(int? id)
        {
            //if (Session["Role"] != null)
            //{
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Off Day Info Not Found!";
                return RedirectToAction("IndexEmployeeOffDay", "Leave");
            }
            HRMS_Employee_OffDay offDayEmp = await db.HrmsEmployeeOffDays.FindAsync(id);
            if (offDayEmp != null)
            {
                try
                {
                    db.HrmsEmployeeOffDays.Remove(offDayEmp);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Off Day Info Deleted Successfully.";
                    return RedirectToAction("IndexEmployeeOffDay", "Leave");
                }
                catch (Exception ex)
                {
                    if (ex.Message ==
                        "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Off Day Info Can't Delete, It is Already in Use!";
                        return RedirectToAction("IndexEmployeeOffDay", "Leave");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("IndexEmployeeOffDay", "Leave");
                }
                //return HttpNotFound();
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Something Went Wrong!";
            return RedirectToAction("IndexEmployeeOffDay", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        public async Task<ActionResult> EditEmployeeOffDay(int? id)
        {
            //if (Session["Role"] != null)
            //{
            if (id != null)
            {
                ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
                var offDayEmp = db.HrmsEmployeeOffDays.FirstOrDefault(x => x.ID == id);
                VM_HRMS_Employee_OffDay modelOffDay = new VM_HRMS_Employee_OffDay
                {
                    ID = offDayEmp.ID,
                    EmployeeId = offDayEmp.EmployeeId,
                    OffDay = offDayEmp.OffDay
                };
                return View(modelOffDay);
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Emplyee offday Not Found!";
            return RedirectToAction("IndexEmployeeOffDay", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Below Action used to Edit Employee OffDay Information 
        /// Database: CrimsonERP
        /// Table/Domain Name : Country
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditEmployeeOffDay(VM_HRMS_Employee_OffDay modelOffDay)
        {
            //if (Session["Role"] != null)
            //{
            var offDays = (db.HrmsEmployeeOffDays.Where(o => o.EmployeeId == modelOffDay.EmployeeId
                                                             && o.OffDay == modelOffDay.OffDay).Select(o => o.OffDay)
                .FirstOrDefault());

            if (offDays != modelOffDay.OffDay)
            {
                try
                {
                    HRMS_Employee_OffDay empOffDay = new HRMS_Employee_OffDay()
                    {
                        ID = modelOffDay.ID,
                        EmployeeId = modelOffDay.EmployeeId,
                        OffDay = modelOffDay.OffDay,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(empOffDay).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Off Day Updated Successfully.";
                    return RedirectToAction("IndexEmployeeOffDay", "Leave");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("IndexEmployeeOffDay", "Leave");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Off day is assigned before.";
            return RedirectToAction("IndexEmployeeOffDay", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        #endregion


        #region HRMS Holiday------

        //Associated View Model----


        /// <summary>
        /// Cumulative view of Assigned Offdays to the Employees-----
        /// Database: CrimsonERP
        /// Table: [HRMS_Employee], [HRMS_Employee_OffDay]
        /// Date: 05.04.18
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> IndexHoliday()
        {

            //if (Session["Role"] != null)
            //{
            try
            {
                VM_HRMS_Holiday holiDay = new VM_HRMS_Holiday();
                await Task.Run(() => holiDay.GetHolidayList());
                return View(holiDay);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexHoliday", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Load form for Assign Employee's Off days....
        /// </summary>
        /// <returns></returns>
        public ActionResult AddHoliDay()
        {
            //if (Session["Role"] != null)
            //{
            try
            {
                VM_HRMS_Holiday holiDay = new VM_HRMS_Holiday();
                return View(holiDay);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexHoliday", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Post Holiday Information 
        /// Database: CrimsonERP
        /// Table:[HRMS_HoliDay]
        /// </summary>
        /// <param name="holiDay"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddHoliDay(VM_HRMS_Holiday holiDay)
        {
            //if (Session["Role"] != null)
            //{
            var holi = (db.HrmsHolidays.Where(h => h.OffDay == holiDay.OffDay).Select(h => h.OffDay)).FirstOrDefault();
            if (holi != holiDay.OffDay)
            {
                try
                {
                    HRMS_Holiday holiDays = new HRMS_Holiday
                    {
                        OffDay = holiDay.OffDay,
                        Description = holiDay.Description,
                        Entry_By = "1",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsHolidays.Add(holiDays);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Holiday Added Successfully.";
                    return RedirectToAction("IndexHoliday", "Leave");
                }
                catch (Exception ex)
                {
                    if (ex.Message ==
                        "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Holiday Can't Add, It is Already Exist!";
                        return RedirectToAction("IndexHoliday", "Leave");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("IndexHoliday", "Leave");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Holiday Can't Add, It is Already Exist!";
            return RedirectToAction("IndexHoliday", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        /// <summary>
        /// Below Action used to Delete Holiday Information
        /// Database: CrimsonERP
        /// Table/Domain Name : HRMS_Holiday
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpGet]
        public async Task<ActionResult> DeleteHoliDay(int? id)
        {
            //if (Session["Role"] != null)
            //{
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Holiday Info Not Found!";
                return RedirectToAction("IndexHoliday", "Leave");
            }
            HRMS_Holiday holiday = await db.HrmsHolidays.FindAsync(id);
            if (holiday != null)
            {
                try
                {
                    db.HrmsHolidays.Remove(holiday);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Holiday Info Deleted Successfully.";
                    return RedirectToAction("IndexHoliday", "Leave");
                }
                catch (Exception ex)
                {
                    if (ex.Message ==
                        "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Holiday Info Can't Delete, It is Already in Use!";
                        return RedirectToAction("IndexHoliday", "Leave");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("IndexHoliday", "Leave");
                }
                //return HttpNotFound();
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Something Went Wrong!";
            return RedirectToAction("IndexHoliday", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// Edit Holiday Information.....
        /// Database: CrimsonERP
        /// Table: HRMS_Holiday
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditHoliDay(int? id)
        {
            //if (Session["Role"] != null)
            //{
            if (id != null)
            {
                var holidays = db.HrmsHolidays.FirstOrDefault(x => x.ID == id);
                VM_HRMS_Holiday modelOffDay = new VM_HRMS_Holiday
                {
                    ID = holidays.ID,
                    OffDay = holidays.OffDay,
                    Description = holidays.Description,
                };
                return View(modelOffDay);
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Holiday info not Found!";
            return RedirectToAction("IndexHoliday", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Below Action used to Edit Holiday Information 
        /// Database: CrimsonERP
        /// Table/Domain Name : HRMS_Holiday
        /// </summary>
        /// <param name="holiDay"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditHoliDay(VM_HRMS_Holiday holiDay)
        {
            //if (Session["Role"] != null)
            //{
            try
            {
                HRMS_Holiday holiDayEdit = new HRMS_Holiday()
                {
                    ID = holiDay.ID,
                    OffDay = holiDay.OffDay,
                    Description = holiDay.Description,
                    Update_By = "1",
                    Update_Date = DateTime.Now
                };
                db.Entry(holiDayEdit).State = System.Data.Entity.EntityState.Modified;
                await db.SaveChangesAsync();
                Session["success_div"] = "true";
                Session["success_msg"] = "Holiday Updated Successfully.";
                return RedirectToAction("IndexHoliday", "Leave");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexHoliday", "Leave");
            }


            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        #endregion


        ////#region HRMS Leave Approval Level Type Setting

        /////// <summary>
        /////// Cumulative view of Leave Approval Level
        /////// Database: CrimsonERP
        /////// Table: 
        /////// Date: 07.04.18
        /////// </summary>
        /////// <returns></returns>
        ////public async Task<ActionResult> IndexLeaveApprovalLevel()
        ////{

        ////    //if (Session["Role"] != null)
        ////    //{
        ////        try
        ////        {
        ////            VM_Leave_Approval_Level_Type approvalLevel = new VM_Leave_Approval_Level_Type();
        ////            await Task.Run(() => approvalLevel.GetApprovalLevel());
        ////            return View(approvalLevel);
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            Session["warning_div"] = "true";
        ////            Session["warning_msg"] = "Error Happened." + ex.Message;
        ////            return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////        }
        ////    //}
        ////    //Session["warning_div"] = "true";
        ////    //Session["warning_msg"] = "Please Login to continue..!";
        ////    //return RedirectToAction("Login", "Account");
        ////}

        /////// <summary>
        /////// Load form for Assign Employee's Off days....
        /////// </summary>
        /////// <returns></returns>
        ////public ActionResult AddLeaveApprovalLevel()
        ////{
        ////    //if (Session["Role"] != null)
        ////    //{
        ////        try
        ////        {
        ////            VM_Leave_Approval_Level_Type leaveApprovalLevel = new VM_Leave_Approval_Level_Type();
        ////            return View(leaveApprovalLevel);
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            Session["warning_div"] = "true";
        ////            Session["warning_msg"] = "Error Happened." + ex.Message;
        ////            return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////        }
        ////    //}
        ////    //Session["warning_div"] = "true";
        ////    //Session["warning_msg"] = "Please Login to continue..!";
        ////    //return RedirectToAction("Login", "Account");
        ////}

        /////// <summary>
        /////// Post Leave Approval Level Information 
        /////// Database: CrimsonERP
        /////// Table:Leave_Approval_Level_Type
        /////// </summary>
        /////// <param name="holiDay"></param>
        /////// <returns></returns>
        ////[HttpPost]
        ////[ValidateAntiForgeryToken]
        ////public async Task<ActionResult> AddLeaveApprovalLevel(VM_Leave_Approval_Level_Type approvalLevel)
        ////{
        ////    //if (Session["Role"] != null)
        ////    //{
        ////        var levelApp =
        ////            (db.HrmsLeaveApprovalLevelTypes.Where(l =>
        ////                l.LeaveApprovalLevel == approvalLevel.LeaveApprovalLevel)).Select(h => h.LeaveApprovalLevel)
        ////            .FirstOrDefault();
        ////        if (levelApp != approvalLevel.LeaveApprovalLevel)
        ////        {
        ////            try
        ////            {
        ////                Leave_Approval_Level_Type level = new Leave_Approval_Level_Type
        ////                {
        ////                    LeaveApprovalLevel = approvalLevel.LeaveApprovalLevel,
        ////                    Entry_By = Session["EmpID"].ToString(),
        ////                    Entry_Date = DateTime.Now
        ////                };
        ////                db.HrmsLeaveApprovalLevelTypes.Add(level);
        ////                await db.SaveChangesAsync();

        ////                Session["success_div"] = "true";
        ////                Session["success_msg"] = "Approval Level Added Successfully.";
        ////                return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////            }
        ////            catch (Exception ex)
        ////            {
        ////                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
        ////                {
        ////                    Session["warning_div"] = "true";
        ////                    Session["warning_msg"] = "Approval Level Can't Add, It is Already Exist!";
        ////                    return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////                }
        ////                Session["warning_div"] = "true";
        ////                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
        ////                return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////            }
        ////        }
        ////        Session["warning_div"] = "true";
        ////        Session["warning_msg"] = "Approval Level Can't Add, It is Already Exist!";
        ////        return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////    //}
        ////    //Session["warning_div"] = "true";
        ////    //Session["warning_msg"] = "Please Login to continue..!";
        ////    //return RedirectToAction("Login", "Account");
        ////}



        /////// <summary>
        /////// Below Action used to Delete Approval Level Information
        /////// Database: CrimsonERP
        /////// Table/Domain Name : Leave_Approval_Level_Type
        /////// </summary>
        /////// <param name="id"></param>
        /////// <returns></returns>

        ////[HttpGet]
        ////public async Task<ActionResult> DeleteLeaveApprovalLevel(int? id)
        ////{
        ////    //if (Session["Role"] != null)
        ////    //{
        ////        if (id == null)
        ////        {
        ////            //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        ////            Session["warning_div"] = "true";
        ////            Session["warning_msg"] = "Approval Level Info Not Found!";
        ////            return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////        }
        ////        Leave_Approval_Level_Type approvalLevel = await db.HrmsLeaveApprovalLevelTypes.FindAsync(id);
        ////        if (approvalLevel != null)
        ////        {
        ////            try
        ////            {
        ////                db.HrmsLeaveApprovalLevelTypes.Remove(approvalLevel);
        ////                await db.SaveChangesAsync();
        ////                Session["success_div"] = "true";
        ////                Session["success_msg"] = "Approval Level Info Deleted Successfully.";
        ////                return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////            }
        ////            catch (Exception ex)
        ////            {
        ////                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
        ////                {
        ////                    Session["warning_div"] = "true";
        ////                    Session["warning_msg"] = "Leave Approval Level Info Can't Delete, It is Already in Use!";
        ////                    return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////                }
        ////                Session["warning_div"] = "true";
        ////                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
        ////                return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////            }
        ////            //return HttpNotFound();
        ////        }
        ////        Session["warning_div"] = "true";
        ////        Session["warning_msg"] = "Something Went Wrong!";
        ////        return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////    //}
        ////    //Session["warning_div"] = "true";
        ////    //Session["warning_msg"] = "Please Login to continue..!";
        ////    //return RedirectToAction("Login", "Account");
        ////}


        /////// <summary>
        /////// Edit Holiday Information.....
        /////// Database: CrimsonERP
        /////// Table: HRMS_Holiday
        /////// </summary>
        /////// <param name="id"></param>
        /////// <returns></returns>
        ////public async Task<ActionResult> EditLeaveApprovalLevel(int? id)
        ////{
        ////    //if (Session["Role"] != null)
        ////    //{
        ////        if (id != null)
        ////        {
        ////            var approvalLevel = db.HrmsLeaveApprovalLevelTypes.FirstOrDefault(x => x.ID == id);
        ////            VM_Leave_Approval_Level_Type approval = new VM_Leave_Approval_Level_Type
        ////            {
        ////                ID = approvalLevel.ID,
        ////                LeaveApprovalLevel = approvalLevel.LeaveApprovalLevel
        ////            };
        ////            return View(approval);
        ////        }
        ////        Session["warning_div"] = "true";
        ////        Session["warning_msg"] = "Leave Approval Level info not Found!";
        ////        return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////    //}
        ////    //Session["warning_div"] = "true";
        ////    //Session["warning_msg"] = "Please Login to continue..!";
        ////    //return RedirectToAction("Login", "Account");
        ////}

        /////// <summary>
        /////// Below Action used to Edit Holiday Information 
        /////// Database: CrimsonERP
        /////// Table/Domain Name : HRMS_Holiday
        /////// </summary>
        /////// <param name="holiDay"></param>
        /////// <returns></returns>
        ////[HttpPost]
        ////[ValidateAntiForgeryToken]
        ////public async Task<ActionResult> EditLeaveApprovalLevel(VM_Leave_Approval_Level_Type leaveApproval)
        ////{
        ////    if (Session["Role"] != null)
        ////    {
        ////        //var le = ;
        ////        if (db.HrmsLeaveApprovalLevelTypes.FirstOrDefault(
        ////            s => s.LeaveApprovalLevel == (leaveApproval.LeaveApprovalLevel)) == null)
        ////        {
        ////            try
        ////            {
        ////                Leave_Approval_Level_Type leaveLevel = new Leave_Approval_Level_Type()
        ////                {
        ////                    ID = leaveApproval.ID,
        ////                    LeaveApprovalLevel = leaveApproval.LeaveApprovalLevel,
        ////                    Update_By = Session["Role"].ToString(),
        ////                    Update_Date = DateTime.Now
        ////                };
        ////                db.Entry(leaveLevel).State = System.Data.Entity.EntityState.Modified;
        ////                await db.SaveChangesAsync();
        ////                Session["success_div"] = "true";
        ////                Session["success_msg"] = "Leave Approval Level Updated Successfully.";
        ////                return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////            }
        ////            catch (Exception ex)
        ////            {
        ////                Session["warning_div"] = "true";
        ////                Session["warning_msg"] = "Error Happened." + ex.Message;
        ////                return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////            }
        ////        }
        ////        Session["success_div"] = "true";
        ////        Session["success_msg"] = "Leave Approval Level Exists.";
        ////        return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        ////    }
        ////    Session["warning_div"] = "true";
        ////    Session["warning_msg"] = "Please Login to continue..!";
        ////    return RedirectToAction("Login", "Account");
        ////}

        ////#endregion


        //#region Leave Level Settings

        ///// <summary>
        ///// Cumulative view of Leave Level Settings
        ///// Database: CrimsonERP
        ///// Table: Leave_Level_Setting
        ///// Developed By: Anis & Fazle Rabbi
        ///// Date: 07.04.18
        ///// </summary>
        ///// <returns></returns>
        //public async Task<ActionResult> IndexLeaveLevel()
        //{

        //    if (Session["Role"] != null)
        //    {
        //        try
        //        {
        //            VM_Leave_Level_Setting leaveLevel = new VM_Leave_Level_Setting();
        //            await Task.Run(() => leaveLevel.GetLeaveLevelApprovalList());
        //            return View(leaveLevel);
        //        }
        //        catch (Exception ex)
        //        {
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Error Happened." + ex.Message;
        //            return RedirectToAction("IndexLeaveLevel", "Leave");
        //        }
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}

        //// Below Action used to Add Leave Approval Level Settings Information
        //// Database: CrimsonERP
        //// Table Name :Leave_Level_Setting
        //// Domain Model Name: Leave_Level_Setting
        //// View Model Name: VM_Leave_Level_Setting
        //// Developed by: Anis and Fazle Rabbi
        //// Date: 2018-04-21
        //public ActionResult AddLeaveLevelSetting()
        //{
        //    if (Session["Role"] != null)
        //    {
        //        try
        //        {
        //            ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
        //            ViewBag.LeaveApprovalLevellist = new SelectList(d.GetLeaveApprovalLevelList(), "Value", "Text");
        //            return View();
        //        }
        //        catch (Exception ex)
        //        {
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Error Happened." + ex.Message;
        //            return RedirectToAction("IndexLeaveApprovalLevel", "Leave");
        //        }
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}

        //// Below Action used to Add Leave Approval Level Settings Information
        //// Database: CrimsonERP
        //// Table Name :Leave_Level_Setting
        //// Domain Model Name: Leave_Level_Setting
        //// View Model Name: VM_Leave_Level_Setting
        //// Developed by: Anis and Fazle Rabbi
        //// Date: 2018-04-21
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> AddLeaveLevelSetting(VM_Leave_Level_Setting vm)
        //{
        //    if (Session["Role"] != null)
        //    {
        //        var levellevel =
        //            (db.HrmsLeaveLevelSettings.Where(l =>
        //                l.LeaveApprovalLevelTypeId == vm.LeaveApprovalLevelTypeId && l.EmployeeId == vm.EmployeeId)).Select(h => h.LeaveApprovalLevelTypeId)
        //            .FirstOrDefault();
        //        if (levellevel != vm.LeaveApprovalLevelTypeId)
        //        {
        //            try
        //            {
        //                Leave_Level_Setting level = new Leave_Level_Setting
        //                {
        //                    LeaveApprovalLevelTypeId = vm.LeaveApprovalLevelTypeId,
        //                    EmployeeId = vm.EmployeeId,
        //                    LeaveApprovedBy = vm.LeaveApprovedBy,
        //                    Entry_By = Session["EmpID"].ToString(),
        //                    Entry_Date = DateTime.Now
        //                };
        //                db.HrmsLeaveLevelSettings.Add(level);
        //                await db.SaveChangesAsync();
        //                Session["success_div"] = "true";
        //                Session["success_msg"] = "Approval Level Setting Added Successfully.";
        //                return RedirectToAction("IndexLeaveLevel", "Leave");
        //            }
        //            catch (Exception ex)
        //            {
        //                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
        //                {
        //                    Session["warning_div"] = "true";
        //                    Session["warning_msg"] = "Approval Level Can't Add, It is Already Exist!";
        //                    return RedirectToAction("IndexLeaveLevel", "Leave");
        //                }
        //                Session["warning_div"] = "true";
        //                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
        //                return RedirectToAction("IndexLeaveLevel", "Leave");
        //            }
        //        }
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Approval Leave Level Setting Can't Add, This Employee Already Assign this Level Setting";
        //        return RedirectToAction("IndexLeaveLevel", "Leave");
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}



        //// Below Action used to Delete Leave Approval Level Settings Information
        //// Database: CrimsonERP
        //// Table Name :Leave_Level_Setting
        //// Domain Model Name: Leave_Level_Setting
        //// View Model Name: VM_Leave_Level_Setting
        //// Developed by: Anis and Fazle Rabbi
        //// Date: 2018-04-21

        //[HttpGet]
        //public async Task<ActionResult> DeleteLeaveLevelSetting(int? id)
        //{
        //    if (Session["Role"] != null)
        //    {
        //        if (id == null)
        //        {
        //            //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Approval Level Info Not Found!";
        //            return RedirectToAction("IndexLeaveLevel", "Leave");
        //        }
        //        Leave_Level_Setting approvalLevel = await db.HrmsLeaveLevelSettings.FindAsync(id);
        //        if (approvalLevel != null)
        //        {
        //            try
        //            {
        //                db.HrmsLeaveLevelSettings.Remove(approvalLevel);
        //                await db.SaveChangesAsync();
        //                Session["success_div"] = "true";
        //                Session["success_msg"] = "Approval Level Settings Info Deleted Successfully.";
        //                return RedirectToAction("IndexLeaveLevel", "Leave");
        //            }
        //            catch (Exception ex)
        //            {
        //                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
        //                {
        //                    Session["warning_div"] = "true";
        //                    Session["warning_msg"] = "Leave Approval Level Info Can't Delete, It is Already in Used!";
        //                    return RedirectToAction("IndexLeaveLevel", "Leave");
        //                }
        //                Session["warning_div"] = "true";
        //                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
        //                return RedirectToAction("IndexLeaveLevel", "Leave");
        //            }
        //            //return HttpNotFound();
        //        }
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Something Went Wrong!";
        //        return RedirectToAction("IndexLeaveLevel", "Leave");
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}

        //// Below Action used to Edit Leave Approval Level Settings Information
        //// Database: CrimsonERP
        //// Table Name :Leave_Level_Setting
        //// Domain Model Name: Leave_Level_Setting
        //// View Model Name: VM_Leave_Level_Setting
        //// Developed by: Anis and Fazle Rabbi
        //// Date: 2018-04-21
        //public async Task<ActionResult> EditLeaveLevelSetting(int? id)
        //{
        //    if (Session["Role"] != null)
        //    {
        //        if (id != null)
        //        {
        //            ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
        //            //ViewBag.LeaveApprovalLevellist = new SelectList(d.GetLeaveApprovalLevelList(), "Value", "Text");
        //            try
        //            {
        //                VM_Leave_Level_Setting leaveLevel = new VM_Leave_Level_Setting();
        //                await Task.Run(() => leaveLevel = leaveLevel.GetSpecificLeaveLevelApprovalList(id.Value));
        //                return View(leaveLevel);
        //            }
        //            catch (Exception ex)
        //            {
        //                Session["warning_div"] = "true";
        //                Session["warning_msg"] = "Error Happened." + ex.Message;
        //                return RedirectToAction("IndexLeaveLevel", "Leave");
        //            }

        //        }
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Leave Approval Level info not Found!";
        //        return RedirectToAction("IndexLeaveLevel", "Leave");
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}

        //// Below Action used to Edit Leave Approval Level Settings Information
        //// Database: CrimsonERP
        //// Table Name :Leave_Level_Setting
        //// Domain Model Name: Leave_Level_Setting
        //// View Model Name: VM_Leave_Level_Setting
        //// Developed by: Anis and Fazle Rabbi
        //// Date: 2018-04-21
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> EditLeaveLevelSetting(VM_Leave_Level_Setting vm)
        //{
        //    if (Session["Role"] != null)
        //    {
        //        try
        //        {
        //            Leave_Level_Setting leaveLevel = new Leave_Level_Setting()
        //            {
        //                ID = vm.ID,
        //                LeaveApprovalLevelTypeId = vm.LeaveApprovalLevelTypeId,
        //                EmployeeId = vm.EmployeeId,
        //                LeaveApprovedBy = vm.LeaveApprovedBy,
        //                Update_By = Session["EmpID"].ToString(),
        //                Update_Date = DateTime.Now
        //            };
        //            db.Entry(leaveLevel).State = System.Data.Entity.EntityState.Modified;
        //            await db.SaveChangesAsync();
        //            Session["success_div"] = "true";
        //            Session["success_msg"] = "Leave Approval Level Updated Successfully.";
        //            return RedirectToAction("IndexLeaveLevel", "Leave");
        //        }
        //        catch (Exception ex)
        //        {
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Error Happened." + ex.Message;
        //            return RedirectToAction("IndexLeaveLevel", "Leave");
        //        }
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}


        //#endregion


        #region HRMS Leave Application By Employee

        /// <summary>
        /// Cumulative view of Leave Application Lists
        /// Database: CrimsonERP
        /// Table: 
        /// Date: 07.04.18
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> IndexLeaveApplication()
        {
            //if (Session["Role"] != null)
            //{
            try
            {
                VM_HRMS_Leave_Application leaveApp = new VM_HRMS_Leave_Application();
                await Task.Run(() => leaveApp.GetLeaveApplicationList());
                return View(leaveApp);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexLeaveApplication", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Load form for Applying Leave
        /// </summary>
        /// <returns></returns>
        public ActionResult AddLeaveApplication()
        {
            //if (Session["Role"] != null)
            //{
            try
            {
                // ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
                ViewBag.Employee = new SelectList(d.GetEmployeeListforLeaveApplication(), "Value", "Text");
                ViewBag.LeaveType = new SelectList(d.GetLeaveTypeName(), "Value", "Text");
                VM_HRMS_Leave_Application model = new VM_HRMS_Leave_Application();
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("AddLeaveApplication", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Post Application form....
        /// Database: CrimsonERP
        /// Table: Leave_Application
        /// </summary>
        /// <param name="holiDay"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddLeaveApplication(VM_HRMS_Leave_Application model)
        {
            var dt1 = model.From1.AsDateTime().ToString("yyyy-MM-dd");
            var dt2 = model.To1.AsDateTime().ToString("yyyy-MM-dd");
            var dt3 = Convert.ToDateTime(dt1);
            var dt4 = Convert.ToDateTime(dt2);
            var year = dt4.Year.ToString();
            var remaininingleave = 0;
            var leave = (from t1 in db.HrmsLeaveAssigns
                         where (t1.LeaveTypeId == model.LeaveApplication.LeaveTypeId && t1.EmployeeId == model.LeaveApplication.EmployeeId && t1.Year == year)
                         select new VM_HRMS_Leave_Application()
                         {
                             RemainingLeaveDays = t1.Remaining
                         }).FirstOrDefault();
            if (leave.RemainingLeaveDays >= model.Days1)
            {
                var count = (from t1 in db.HrmsLeaveRepositories
                             where t1.EmployeeId == model.LeaveApplication.EmployeeId && (t1.Date >= dt3 && t1.Date <= dt4)
                             select t1.EmployeeId
                            ).ToList();
                if (!count.Any())
                {
                    var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Manual_Leave_Application]"
                                                      + "@EmployeeId='" + model.LeaveApplication.EmployeeId + "', " +
                                                      "@LeaveTypeId ='" + model.LeaveApplication.LeaveTypeId + "', " +
                                                      "@From='" + dt1 + "', " +
                                                      "@To='" + dt2 + "', " +
                                                      "@Days='" + model.LeaveApplication.Days + "', " +
                                                      "@Purpose='" + model.LeaveApplication.Purpose + "', " +
                                                      "@StayDuringLeave='" + model.LeaveApplication.StayDuringLeave +
                                                      "', " +
                                                      "@Comment='" + model.LeaveApplication.Comment + "', " +
                                                      // "@JoiningWorkAfterLeave='"+ model.LeaveApplication.JoiningWorkAfterLeave+ "', " +
                                                      "@LeaveStatus=0, " +
                                                      "@LeavePaidType='" + model.LeaveApplication.LeavePaidType + "', " +
                                                      "@HRDepartment='" + model.HRDepartment + "', " +
                                                      "@Recommended='" + model.LeaveApplication.Recommended + "', " +
                                                      "@SectionIncharge='" + model.SectionIncharge + "', " +
                                                      "@APMorPM='" + model.APMorPM + "', " +
                                                      "@FM='" + model.FM + "', " +
                                                     // "@ApprovedBy='" + model.ApprovedBy + "', " +
                                                      "@ApprovedBy='N/A', " +
                                                      "@EntryBy=1, @PreviousLeaveId=0");


                    if (a > 0)
                    {
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Leave Application Submitted Successfully.";
                        return RedirectToAction("AddLeaveApplication", "Leave");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Employee Already Applied Leave Between these Date";
                return RedirectToAction("AddLeaveApplication", "Leave");

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "This Employee Leave Amount Not Suffiecient";
            return RedirectToAction("AddLeaveApplication", "Leave");

        }



        /// <summary>
        /// Below Action used to Delete Approval Level Information
        /// Database: CrimsonERP
        /// Table/Domain Name : Leave_Approval_Level_Type
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        //[HttpGet]
        //public async Task<ActionResult> DeleteLeaveApplication(int? id)
        //{


        //    var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Leave_Application_Delete] @Id='" + id + "'");
        //    if (a > 0)
        //    {
        //        Session["success_div"] = "true";
        //        Session["success_msg"] = "Leave Application Deleted Successfully.";
        //        return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Leave Application Deleted Successfully.";
        //    return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
        //}



        /// <summary>
        /// Below Action used to Delete Approval Level Information
        /// Database: CrimsonERP
        /// Table/Domain Name : Leave_Approval_Level_Type
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        //[HttpGet]
        //public async Task<ActionResult> DeleteLeaveApplication(int? id)
        //{
        //    //if (Session["Role"] != null)
        //    //{
        //        if (id == null)
        //        {
        //            //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Leave Application Not Found!";
        //            return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
        //        }
        //    HRMS_Leave_Application leave = await db.HrmsLeaveApplications.FindAsync(id);
        //        if (leave != null)
        //        {
        //            try
        //            {
        //                db.HrmsLeaveApplications.Remove(leave);
        //                await db.SaveChangesAsync();
        //                Session["success_div"] = "true";
        //                Session["success_msg"] = "Leave Application Deleted Successfully.";
        //                return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
        //            }
        //            catch (Exception ex)
        //            {
        //                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
        //                {
        //                    Session["warning_div"] = "true";
        //                    Session["warning_msg"] = "Leave Application Can't Delete, It is Already in Use!";
        //                    return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
        //                }
        //                Session["warning_div"] = "true";
        //                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
        //                return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
        //            }
        //            //return HttpNotFound();
        //        }
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Something Went Wrong!";
        //        return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
        //    //}
        //    //Session["warning_div"] = "true";
        //    //Session["warning_msg"] = "Please Login to continue..!";
        //    //return RedirectToAction("Login", "Account");
        //}


        /// <summary>
        /// Edit Holiday Information.....
        /// Database: CrimsonERP
        /// Table: HRMS_Holiday
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditLeaveApplication(int? id)
        {
            VM_HRMS_Leave_Application model = new VM_HRMS_Leave_Application();
            if (id != null)
            {

                await Task.Run(() => model = model.LoadSpecificLeaveDetails(id.Value));
                ViewBag.ToDate = model.To.ToString("yyyy/MM/dd");
                ViewBag.FromDate = model.From.ToString("yyyy/MM/dd");
                return View(model);


            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Leave Application info not Found!";
            return RedirectToAction("IndexLeaveApplication", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Below Action used to Edit Holiday Information 
        /// Database: CrimsonERP
        /// Table/Domain Name : HRMS_Holiday
        /// </summary>
        /// <param name="holiDay"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditLeaveApplication(VM_HRMS_Leave_Application model)
        {
            //var dt1 = model.From1.AsDateTime().ToString("yyyy-MM-dd");
            //var dt2 = model.To1.AsDateTime().ToString("yyyy-MM-dd");
            //var dt3 = Convert.ToDateTime(dt1);
            //var dt4 = Convert.ToDateTime(dt2);
            //var year = dt4.Year.ToString();

            var dt1 = model.From.ToString("yyyy-MM-dd");
            var dt2 = model.To.ToString("yyyy-MM-dd");
            var dt3 = Convert.ToDateTime(dt1);
            var dt4 = Convert.ToDateTime(dt2);
            var year = dt4.Year.ToString();



            if (model.LeaveStatus==1)
            {

                var leaveApplication = db.HrmsLeaveApplications.FirstOrDefault(x => x.ID == model.ID);
                VM_HRMS_Leave_Application vmodel = new VM_HRMS_Leave_Application();
                vmodel.LeaveApplication = leaveApplication;
                // var p= leaveApplication.P
                var remaininingleave = 0;

                var leave = (from t1 in db.HrmsLeaveAssigns
                             where (t1.LeaveTypeId == model.LeaveTypeId && t1.EmployeeId == model.EmployeeId && t1.Year == model.To.Year.ToString())
                             select new VM_HRMS_Leave_Application()
                             {
                                 RemainingLeaveDays = t1.Remaining
                             }).FirstOrDefault();
                if (leave.RemainingLeaveDays >= model.Days)
                {
                    var count = (from t1 in db.HrmsLeaveRepositories
                                 where t1.EmployeeId == model.EmployeeId && (t1.Date >= model.From && t1.Date <= model.To)
                                 select t1.EmployeeId
                                ).ToList();
                    if (!count.Any())
                    {
                        var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Manual_Leave_Application]"
                                                          + "@EmployeeId='" + vmodel.LeaveApplication.EmployeeId + "', " +
                                                          "@LeaveTypeId ='" + vmodel.LeaveApplication.LeaveTypeId + "', " +
                                                          "@From='" + dt1 + "', " +
                                                          "@To='" + dt2 + "', " +
                                                          "@Days='" + model.Days + "', " +
                                                          "@Purpose='" + vmodel.LeaveApplication.Purpose + "', " +
                                                          "@StayDuringLeave='" + vmodel.LeaveApplication.StayDuringLeave +
                                                          "', " +
                                                          "@Comment='" + model.Comment + "', " +
                                                          // "@JoiningWorkAfterLeave='"+ model.LeaveApplication.JoiningWorkAfterLeave+ "', " +
                                                          "@LeaveStatus='" + model.LeaveStatus + "', " +
                                                          "@LeavePaidType='" + model.LeavePaidType + "', " +
                                                          "@HRDepartment='" + vmodel.LeaveApplication.HRDepartment + "', " +
                                                          "@Recommended='" + vmodel.LeaveApplication.Recommended + "', " +
                                                          "@SectionIncharge='" + vmodel.LeaveApplication.SectionIncharge + "', " +
                                                          "@APMorPM='" + vmodel.LeaveApplication.APMorPM + "', " +
                                                          "@FM='" + vmodel.LeaveApplication.FM + "', " +
                                                          "@ApprovedBy='" + model.ApprovedBy + "', " +
                                                          "@EntryBy=1, @PreviousLeaveId='" + model.ID + "'");


                        if (a > 0)
                        {
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Leave Application Approved Successfully.";
                            return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
                        }
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Employee Already Applied Leave Between these Date";
                    return RedirectToAction("ViewLeaveApplicationInfo", "Leave");

                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "This Employee Leave Amount Not Suffiecient";
                return RedirectToAction("ViewLeaveApplicationInfo", "Leave");

            }
            else if(model.LeaveStatus==2)
            {
                var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Leave_Application_Delete] @Id ='" + model.ID + "'");
                if (a > 0)
                {
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Leave Deleted Successfully.";
                    return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
                }
            }
            return RedirectToAction("ViewLeaveApplicationInfo", "Leave");

        }



        public async Task<ActionResult> ApproveLeaveApplication(int id)
        {
            try
            {
                var user1 = (Oss.Hrms.Models.Services.CurrentUser)Session["User"];

                db = new HrmsContext();
                var leave = db.HrmsLeaveApplications.FirstOrDefault(x => x.ID == id);

                var vmmodel = new VM_HRMS_Leave_Application();
                vmmodel.LeaveApplication = new HRMS_Leave_Application();
                vmmodel.ID = id;
                vmmodel.LeaveApplication = leave;
                vmmodel.LeaveApplication.LeaveStatus = 1;
              
                var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Manual_Leave_Application]"
                                                         + "@EmployeeId='" + vmmodel.LeaveApplication.EmployeeId + "', " +
                                                         "@LeaveTypeId ='" + vmmodel.LeaveApplication.LeaveTypeId + "', " +
                                                         "@From='" + vmmodel.LeaveApplication.From + "', " +
                                                         "@To='" + vmmodel.LeaveApplication.To + "', " +
                                                         "@Days='" + vmmodel.LeaveApplication.Days + "', " +
                                                         "@Purpose='" + vmmodel.LeaveApplication.Purpose + "', " +
                                                         "@StayDuringLeave='" + vmmodel.LeaveApplication.StayDuringLeave +
                                                         "', " +
                                                         "@Comment='" + vmmodel.LeaveApplication.Comment + "', " +
                                                         // "@JoiningWorkAfterLeave='"+ model.LeaveApplication.JoiningWorkAfterLeave+ "', " +
                                                         "@LeaveStatus='" + vmmodel.LeaveApplication.LeaveStatus + "', " +
                                                         "@LeavePaidType='" + 1 + "', " +
                                                         "@HRDepartment='" + vmmodel.LeaveApplication.HRDepartment + "', " +
                                                         "@Recommended='" + vmmodel.LeaveApplication.Recommended + "', " +
                                                         "@SectionIncharge='" + vmmodel.LeaveApplication.SectionIncharge + "', " +
                                                         "@APMorPM='" + vmmodel.LeaveApplication.APMorPM + "', " +
                                                         "@FM='" + vmmodel.LeaveApplication.FM + "', " +
                                                         "@ApprovedBy='" + user1.UserName + "', " +
                                                         "@EntryBy=1, @PreviousLeaveId='" + vmmodel.LeaveApplication.ID + "'");
                if (a > 0)
                {
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Leave Application Approved Successfully.";
                    return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
                }
            }
            catch (Exception ex)
            {
                Session["success_div"] = "true";
                Session["success_msg"] = "Error Happened, Please Check!";
                return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
            }
            return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
        }

        #endregion

        #region  Leave Assign

        // Below Action used to View Attendance Leave Type
        // Database: CrimsonERP
        // Table Name :Leave_Type
        // Domain Model Name: Leave_Type
        // View Model Name: VM_Leave_Type
        // Developed by: Abid
        // Date: 01-04-18

        [HttpGet]
        public async Task<ActionResult> ViewHrmsLeaveType()
        {
            //if (Session["Role"] != null)
            //{
            try
            {
                VM_HRMS_Leave_Type model = new VM_HRMS_Leave_Type();
                await Task.Run(() => model.GetLoadData());
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("ViewHrmsLeaveType", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Leave Type
        // Database: CrimsonERP
        // Table Name :Leave_Type
        // Domain Model Name: Leave_Type
        // View Model Name: VM_Leave_Type
        // Developed by: Abid
        // Date: 01.04.18
        [HttpGet]
        public async Task<ActionResult> AddHrmsLeaveType()
        {
            //if (Session["Role"] != null)
            //{
            VM_HRMS_Leave_Type model = new VM_HRMS_Leave_Type();
            return View(model);
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Add Remarks over Attendance
        // Database: CrimsonERP
        // Table Name :HRMS_Attendance_Remarks
        // Domain Model Name: HRMS_Attendance_Remarks
        // View Model Name: VM_HRMS_Attendance_Remarks
        // Developed by: Abid
        // Date: 01.04.18
        [HttpPost]
        public async Task<ActionResult> AddHrmsLeaveType(VM_HRMS_Leave_Type model)
        {
            //if (Session["Role"] != null)
            //{
            try
            {
                HRMS_Leave_Type hrmsLeaveType = new HRMS_Leave_Type
                {
                    Leave_Name = model.Leave_Name,
                    Leave_Amount = model.Leave_Amount,
                    Entry_Date = DateTime.Now,
                    Entry_By = "1"
                };
                db.HrmsLeaveTypes.Add(hrmsLeaveType);
                await db.SaveChangesAsync();
                return RedirectToAction("ViewHrmsLeaveType", "Leave");
                // model.Leave_type.Status = true;
                //model.Entry_Date = DateTime.Now;

                ////Leave_Type Leave_type = new Leave_Type { Leave_Name=model.Leave_Name,Leave_Amount=model.Leave_Amount, Status= true, Entry_Date = DateTime.Now };

                //Session["success_div"] = "true";
                //Session["success_msg"] = "Remarks Added Successfully.";

            }
            catch (Exception ex)
            {
                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Leave Type Can't Add, It is Already Exist!";
                    return RedirectToAction("ViewHrmsLeaveType", "Leave");
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                return RedirectToAction("ViewHrmsLeaveType", "Leave");
            }
            //catch (Exception ex)
            //{
            //    Session["warning_div"] = "true";
            //    Session["warning_msg"] = "Error Happened." + ex.Message;
            //    return RedirectToAction("ViewHrmsLeaveType", "Leave");
            //}
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Leave Type
        // Database: CrimsonERP
        // Table Name :HRMS_Attendance_Remarks
        // Domain Model Name: HRMS_Attendance_Remarks
        // View Model Name: VM_HRMS_Attendance_Remarks
        // Developed by: Abid
        // Date: 01.04.18
        [HttpGet]
        public async Task<ActionResult> EditHrmsLeaveType(int id)
        {
            //if (Session["Role"] != null)
            //{
            if (id != null)
            {
                VM_HRMS_Leave_Type model = new VM_HRMS_Leave_Type();
                await Task.Run(() => model.SelectSingle(id));
                return View(model);
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Remarks Not Found!";
            return RedirectToAction("ViewHrmsLeaveType", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Leave Type
        // Database: CrimsonERP
        // Table Name :HRMS_Attendance_Remarks
        // Domain Model Name: HRMS_Attendance_Remarks
        // View Model Name: VM_HRMS_Attendance_Remarks
        // Developed by: Abid
        // Date: 01.04.18

        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<ActionResult> EditHrmsLeaveType(VM_HRMS_Leave_Type model)
        {
            //if (Session["Role"] != null)
            //{
            try
            {

                HRMS_Leave_Type hrmsLeaveType = new HRMS_Leave_Type
                {
                    Leave_Name = model.Leave_Name,
                    Leave_Amount = model.Leave_Amount,
                    Update_By = "1"
                };
                db.Entry(hrmsLeaveType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("ViewHrmsLeaveType", "Leave");
                // db.e(model.Leave_type).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                //Session["success_div"] = "true";
                //Session["success_msg"] = "Leave Type Updated Successfully.";

            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("ViewHrmsLeaveType", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Delete Leave Type
        // Database: CrimsonERP
        // Table Name :Leave_Type
        // Domain Model Name: Leave_Type
        // View Model Name: VM_Leave_Type
        // Developed by: Abid
        // Date: 01.04.18
        [HttpGet]
        public async Task<ActionResult> DeleteHrmsLeaveType(int? id)
        {
            //if (Session["Role"] != null)
            //{
            if (id == null)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Leave Type ID Not Found!";
                return RedirectToAction("ViewHrmsLeaveType", "Leave");
            }
            var lid = await db.HrmsLeaveTypes.FindAsync(id);
            if (lid != null)
            {
                try
                {
                    db.HrmsLeaveTypes.Remove(lid);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Leave Type Deleted Successfully.";
                    return RedirectToAction("ViewHrmsLeaveType", "Leave");
                }
                catch (Exception ex)
                {
                    if (ex.Message ==
                        "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Leave Type Can't Delete, It is Currently Used!";
                        return RedirectToAction("ViewHrmsLeaveType", "Leave");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewHrmsLeaveType", "Leave");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Leave Type ID Not Found!";
            return RedirectToAction("ViewHrmsLeaveType", "Leave");

            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }








        // Below Action used to Employee Leave Assign
        // Database: CrimsonERP
        // Table Name :Leave_Assign
        // Domain Model Name: Leave_Assign
        // View Model Name: VM_Leave_Assign
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-04-10
        [HttpGet]
        public async Task<ActionResult> EmployeeLeaveAssign()
        {
            //if (Session["Role"] != null)
            //{
            try
            {
                DropDownData ddd = new DropDownData();
                ViewBag.BusinessUnitList = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
                ViewBag.ShiftTypeList = new SelectList(ddd.GetShiftTypeName(), "Value", "Text");
                ViewBag.LeaveTypeList = new SelectList(ddd.GetLeaveTypeNameWithAmount(), "Value", "Text");
                ViewBag.LeaveYearList = new SelectList(ddd.GetAssignLeaveYear(), "Value", "Text");
                VM_HRMS_Leave_Assign model = new VM_HRMS_Leave_Assign();
                // await Task.Run(() => model.LoadEmployeeShiftAssignInfo());
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happend. " + ex.Message;
                return RedirectToAction("Index", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Employee Leave Assign
        // Database: CrimsonERP
        // Table Name :Leave_Assign
        // Domain Model Name: Leave_Assign
        // View Model Name: VM_Leave_Assign
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-04-10
        [HttpPost]
        public async Task<ActionResult> EmployeeLeaveAssign(VM_HRMS_Leave_Assign model)
        {
            //if (Session["Role"] != null)
            //{
            //try
            //{
            if (model.EmployeeIds.Count > 0)
            {
                try
                {
                    model.EmployeeIds = model.EmployeeIds[0].TrimEnd(',').Split(',').ToList();
                    var list = "";
                    list = String.Join(",", model.EmployeeIds);
                    //var a =
                    db.Database.ExecuteSqlCommand(
                        "EXECUTE[dbo].[sp_Employee_Leave_Assign_And_Modification] @EmployeeId='" + list +
                        "',@LeavetypeId='" + model.LeaveTypeId + "',@Year='" + model.year + "',@Total='" + model.Total +
                        "',@EntryBy='1'");
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Employee Leave Assign/Updated Successfully.";
                    return RedirectToAction("EmployeeLeaveAssign", "Leave");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Employee Leave Can't Assign, Error is " + ex.Message;
                    return RedirectToAction("EmployeeLeaveAssign", "Leave");
                }

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Select Employee ID";
            return RedirectToAction("EmployeeLeaveAssign", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        // Below Action used to View Employee Leave Assign Info
        // Database: CrimsonERP
        // Table Name :Leave_Assign
        // Domain Model Name: Leave_Assign
        // View Model Name: VM_Leave_Assign
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-04-14
        [HttpGet]
        public async Task<ActionResult> ViewEmployeeLeaveAssignInfo()
        {
            //if (Session["Role"] != null)
            //{
            try
            {
                DropDownData ddd = new DropDownData();
                ViewBag.Employee = new SelectList(d.GetEmployeeListforLeaveApplicationwithAll(), "Value", "Text");
                ViewBag.LeaveTypeList = new SelectList(ddd.GetLeaveTypeNameWithAll(), "Value", "Text");
                ViewBag.LeaveYearList = new SelectList(ddd.GetLeaveTypeAssignYearList(), "Value", "Text");
                VM_HRMS_Leave_Assign model = new VM_HRMS_Leave_Assign();
                // await Task.Run(() => model.LoadEmployeeShiftAssignInfo());
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happend. " + ex.Message;
                return RedirectToAction("ViewEmployeeLeaveAssignInfo", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to View Employee Leave Assign Info
        // Database: CrimsonERP
        // Table Name :Leave_Assign
        // Domain Model Name: Leave_Assign
        // View Model Name: VM_Leave_Assign
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-04-14
        [HttpPost]
        public async Task<ActionResult> ViewEmployeeLeaveAssignInfo(VM_HRMS_Leave_Assign model)
        {
            //if (Session["Role"] != null)
            //{
            DropDownData ddd = new DropDownData();
            ViewBag.Employee = new SelectList(d.GetEmployeeListforLeaveApplicationwithAll(), "Value", "Text");
            ViewBag.LeaveTypeList = new SelectList(ddd.GetLeaveTypeNameWithAll(), "Value", "Text");
            ViewBag.LeaveYearList = new SelectList(ddd.GetLeaveTypeAssignYearList(), "Value", "Text");
            try
            {
                await Task.Run(() => model.SearchEmployeeLeaveInformation(model));
                return View(model);
            }
            catch (Exception ex)
            {
                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Leave Assign Information Not Found !";
                    return RedirectToAction("ViewEmployeeLeaveAssignInfo", "Leave");
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                return RedirectToAction("ViewEmployeeLeaveAssignInfo", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Apply Leave Application
        // Database: CrimsonERP
        // Table Name :Leave_Application and Leave_Application_Details
        // Domain Model Name: Leave_Application and Leave_Application_Details
        // View Model Name: VM_Leave_Application
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-04-24
        public ActionResult LeaveApplication()
        {
            //if (Session["ROLE"] != null)
            //{
            ViewBag.Coutries = new SelectList(d.GetCountryList(), "Value", "Text");
            ViewBag.BUnits = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
            //ViewBag.Units = new SelectList(d.GetUnitList(), "Value", "Text");
            ViewBag.Departments = new SelectList(d.GetDepartmentList(), "Value", "Text");
            //ViewBag.Sections = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.Designations = new SelectList(d.GetDesignationList(), "Value", "Text");
            return View();
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to View Employee Leave Assign Info
        // Database: CrimsonERP
        // Table Name :Leave_Assign
        // Domain Model Name: Leave_Assign
        // View Model Name: VM_Leave_Assign
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-04-14
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LeaveApplication(VMEmployee vmEmployee, HttpPostedFileBase Photo)
        {
            //if (Session["ROLE"] != null)
            //{

            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Account");
        }


        #endregion

        ///<summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetUnitList(int bUnitId)
        {
            var list = db.Units.ToList().Where(u => u.BusinessUnit_ID == bUnitId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This json part will retrive related Dept based on unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetDeptList(int unitId)
        {
            //if (Session["Role"] != null)//&& Session["Role"].ToString() == "Admin")
            //{
            var list = new List<object>();
            foreach (var List in db.Departments.ToList().Where(u => u.Unit_ID == unitId))
            {
                list.Add(new { Text = List.DeptName, Value = List.ID });
            }
            var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
            return retval;
            //}
            //return null;
        }


        /// <summary>
        /// This json part will retrive related section based on Department.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetSectionList(int deptId)
        {
            //if (Session["Role"] != null)
            //{
            var list = new List<object>();
            foreach (var List in db.Sections.ToList().Where(u => u.Dept_ID == deptId))
            {
                list.Add(new { Text = List.SectionName, Value = List.ID });
            }
            var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
            return retval;
            //}
            //return null;

        }


        /// <summary>
        /// This json part will retrive Employee based on section Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public JsonResult GetEmployeeIDforEmployeeShiftAssign(int sectionid)
        {
            //if (Session["Role"] != null)
            //{
            var list = db.Employees.ToList().Where(u => u.Section_Id == sectionid && u.Present_Status == 1);
            var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
            return retval;
            //}
            //return null;
        }

        ///<summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetEmployeeInformation(int empId)
        {
            VM_HRMS_Leave_Application vm = new VM_HRMS_Leave_Application();
            var list = vm.GetLeaveApplicantInformation(empId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        ///<summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetLeaveInformationInformation(int empId, int leavetypeid, string leaveyear)
        {
            VM_HRMS_Leave_Application vm = new VM_HRMS_Leave_Application();
            var leaveyr = Convert.ToDateTime(leaveyear);
            var lyear = leaveyr.Year;
            var list = vm.GetLeaveBalanceInformation(empId, leavetypeid, lyear);
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        // Below Action used to View Employee Leave Assign Info
        // Database: CrimsonERP
        // Table Name :Leave_Assign
        // Domain Model Name: Leave_Assign
        // View Model Name: VM_Leave_Assign
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-04-14
        [HttpGet]
        public async Task<ActionResult> ViewLeaveApplicationInfo()
        {
            var Fromdate = DateTime.Today;
            var firstDayOfMonth = Fromdate.AddDays(-30);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            var leaveStatus = 0;
            try
            {
                DropDownData ddd = new DropDownData();
                ViewBag.EmployeeList = new SelectList(ddd.GetActiveEmployeeListwithAll(), "Value", "Text");
                ViewBag.LeaveStatus = new SelectList(ddd.GetLeaveStatusList(), "Value", "Text");
                
                VM_HRMS_Leave_Application model = new VM_HRMS_Leave_Application();
                var Leavelist = db.Database.SqlQuery<VM_HRMS_Leave_Application>($@"Exec [dbo].[sp_ViewDateRangeWiseLeaveInfo] '{firstDayOfMonth}', '{
                                        lastDayOfMonth}','{leaveStatus}'").ToList();
                model.DataList1 = Leavelist;
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happend. " + ex.Message;
                return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to View Employee Leave Assign Info
        // Database: CrimsonERP
        // Table Name :Leave_Assign
        // Domain Model Name: Leave_Assign
        // View Model Name: VM_Leave_Assign
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-04-14
        [HttpPost]
        public async Task<ActionResult> ViewLeaveApplicationInfo(VM_HRMS_Leave_Application model)
        {
            //if (Session["Role"] != null)
            //{
            DropDownData ddd = new DropDownData();
            ViewBag.EmployeeList = new SelectList(ddd.GetActiveEmployeeListwithAll(), "Value", "Text");
            ViewBag.LeaveStatus = new SelectList(ddd.GetLeaveStatusList(), "Value", "Text");
            var Leavelist = new List<VM_HRMS_Leave_Application>();

            if(model.LeaveApplication.EmployeeId !=null && model.LeaveApplication.From != null && model.LeaveApplication.To !=null && model.LeaveApplication.LeaveStatus != null)
            {
                if (model.LeaveApplication.EmployeeId == 0)
                {
                    Leavelist = db.Database
                        .SqlQuery<VM_HRMS_Leave_Application>(
                            $@"Exec [dbo].[sp_ViewDateRangeWiseLeaveInfo] '{model.LeaveApplication.From}', '{
                                    model.LeaveApplication.To}','{model.LeaveApplication.LeaveStatus}'").ToList();
                }
                else
                {
                    Leavelist = db.Database
                        .SqlQuery<VM_HRMS_Leave_Application>(
                            $@"Exec [dbo].[sp_ViewDateRangeWiseSpecificLeaveInfo] '{model.LeaveApplication.From}', '{
                                    model.LeaveApplication.To
                                }', '{model.LeaveApplication.EmployeeId}','{model.LeaveApplication.LeaveStatus}'")
                        .ToList();

                }
                model.DataList1 = Leavelist;
                return View(model);
            }
            //model.DataList1 = Leavelist;
            //return View(model);
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Error Happend. ";
            return RedirectToAction("ViewLeaveApplicationInfo", "Leave");


        }




        /// <summary>
        ///  Particular Employee Details Id Wise.........
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> LeaveDetail(int? id)
        {
            VM_HRMS_Leave_Application model = new VM_HRMS_Leave_Application();
            //if (Session["Role"] != null)
            //{
            if (true)
            {

                await Task.Run(() => model = model.LoadSpecificLeaveDetails(id.Value));
                return View(model);
            }

            // }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Something Went Wrong!";
            //return RedirectToAction("IndexEmployees", "EmployeeRecord");
            // }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// </summary>
        /// <returns></returns>
        public ActionResult AddPreviousLeaveInformation()
        {
            //if (Session["Role"] != null)
            //{
            try
            {
                ViewBag.Employee = new SelectList(d.GetEmployeeListforLeaveApplication(), "Value", "Text");
                ViewBag.LeaveType = new SelectList(d.GetLeaveTypeName(), "Value", "Text");
                ViewBag.LeaveYearList = new SelectList(d.GetAssignPreviousLeaveYear(), "Value", "Text");
                VM_HRMS_Leave_Assign model = new VM_HRMS_Leave_Assign();
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("AddPreviousLeaveInformation", "Leave");
            }
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Post Application form....
        /// Database: CrimsonERP
        /// Table: Leave_Application
        /// </summary>
        /// <param name="holiDay"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPreviousLeaveInformation(VM_HRMS_Leave_Assign model)
        {
            //@EmployeeId int,
            //@LeavetypeId int,
            //@Year varchar(10),
            //@Total decimal(18, 2),
            //@Taken decimal(18, 2),
            //@EntryBy varchar(100)

            var a = db.Database.ExecuteSqlCommand("Exec [dbo].[sp_Employee_Previous_Leave_Assign_And_Modification]"
                                                  + "@EmployeeId='" + model.EmployeeId + "', " +
                                                  "@LeaveTypeId ='" + model.LeaveTypeId + "', " +
                                                  "@Year='" + model.year + "', " +
                                                  "@Total='" + model.Total + "', " +
                                                  "@Taken='" + model.Taken + "', " +
                                                  "@EntryBy=1");


            if (a > 0)
            {
                Session["success_div"] = "true";
                Session["success_msg"] = "Employee Leave Assign/Updated Successfully.";
                return RedirectToAction("AddPreviousLeaveInformation", "Leave");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Employee Leave Assign/Updated Successfully.";
            return RedirectToAction("AddPreviousLeaveInformation", "Leave");

        }

        [HttpGet]
        public async Task<ActionResult> DeleteLeaveApplication(int? id)
        {

            var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Leave_Application_Delete] @Id ='" + id + "'");
            if (a > 0)
            {
                Session["success_div"] = "true";
                Session["success_msg"] = "Leave Deleted Successfully.";
                return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Something Went Wrong!";
            return RedirectToAction("ViewLeaveApplicationInfo", "Leave");
            //}
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        #region


        public ActionResult GetLeaveSlip(int id)
        {
            VM_HRMS_Leave_Application model = new VM_HRMS_Leave_Application();
            DtPayroll ds = new DtPayroll();
            ds = model.LeaveSlip(id);
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/LeaveSlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "Leave Slip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");

        }

        #endregion 

        


        #region
        public ActionResult GetLeaveApplication(int id)
        {
            VM_HRMS_Leave_Application model = new VM_HRMS_Leave_Application();
            DtPayroll ds = new DtPayroll();
            ds = model.LeaveApplicationFrom(id);
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/LeaveApplication.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "Leave Application From";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");

        }
        #endregion

        #region Earn Leave

        [HttpGet]
        public ActionResult SectionWiseEarnLeaveCalcuation()
        {
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            VM_HRMS_Earn_Leave_Payment model = new VM_HRMS_Earn_Leave_Payment();
            model.DataList = new List<VM_HRMS_Earn_Leave_Payment>();
            var date = DateTime.Now;
            //
            model.ToDate = date;
            //   model.Counter = 1;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SectionWiseEarnLeaveCalcuation(VM_HRMS_Earn_Leave_Payment model)
        {
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            //await Task.Run(() => model.EarnLeaveCalculation(model));
            await Task.Run(() => model = model.SectionWiseEarnLeaveCalculation(model));
            model.ToDate = model.ToDate;
            model.EmployeeId = model.EmployeeId;
            model.IsAudit = false;
            return View(model);
        }


        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> SectionWiseEarnLeaveCalcuationSave(VM_HRMS_Earn_Leave_Payment model)
        {
            try
                {
                    if (model.EmployeeIds.Count > 0)
                    {
                        model.IsAudit = false;
                        await Task.Run(() => model.SectionWiseEarnLeaveCalculationSave(model));
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Employee Earn Leave Payment Saved Successfully.";
                        return RedirectToAction("SectionWiseEarnLeaveCalcuation", "Leave");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Employee Id Not Found!";
                    return RedirectToAction("SectionWiseEarnLeaveCalcuation", "Leave");
            }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("SectionWiseEarnLeaveCalcuation", "Leave");
                }
        }




        [HttpGet]
        public ActionResult EarnLeaveCalcuation()
        {
            ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeListForEarnLeave(), "Value", "Text");
            VM_HRMS_Earn_Leave_Payment model = new VM_HRMS_Earn_Leave_Payment();
            model.DataList = new List<VM_HRMS_Earn_Leave_Payment>();
            var date = DateTime.Now;
            model.ToDate = date;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EarnLeaveCalcuation(VM_HRMS_Earn_Leave_Payment model)
        {
            ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeListForEarnLeave(), "Value", "Text");
            //await Task.Run(() => model.EarnLeaveCalculation(model));
            await Task.Run(() => model = model.EarnLeaveCalculation(model));
            model.ToDate = model.ToDate;
            model.EmployeeId = model.EmployeeId;
            model.IsAudit = false;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EarnLeaveCalcuationSave(VM_HRMS_Earn_Leave_Payment model)
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    model.IsAudit = false;
                    await Task.Run(() => model.EarnLeaveCalculationSave(model));
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Employee Earn Leave Payment Saved Successfully.";
                    return RedirectToAction("EarnLeaveCalcuation", "Leave");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("EarnLeaveCalcuation", "Leave");
                }
            }
        }


        // Below Action used to View Employee Attendance Information
        // Database: OSS.HRMS
        // Table Name :HRMS_Earn_Leave_Payment
        // Domain Model Name: HRMS_Leave
        // View Model Name: VM_HRMS_Earn_Leave_Payment
        // Developed by: Anis
        // Date: 2018-10-31
        [HttpGet]
        public async Task<ActionResult> ViewEarnLeavePaymentInfo()
        {
            //if (Session["Role"] != null)
            {
                try

                {
                    VM_HRMS_Earn_Leave_Payment model = new VM_HRMS_Earn_Leave_Payment();
                    model.DataList = new List<VM_HRMS_Earn_Leave_Payment>();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewEarnLeavePaymentInfo", "Leave");
                }
            }

        }
        // Below Action used to View Employee Attendance Information
        // Database: OSS.HRMS
        // Table Name :HRMS_Earn_Leave_Payment
        // Domain Model Name: HRMS_Leave
        // View Model Name: VM_HRMS_Earn_Leave_Payment
        // Developed by: Anis
        // Date: 2018-10-31
        [HttpPost]
        public async Task<ActionResult> ViewEarnLeavePaymentInfo(VM_HRMS_Earn_Leave_Payment model)
        {
            {
                try
                {
                    model.IsAudit = false;
                    await Task.Run(() => model = model.EarnLeaveCalculationInformation(model));
                    //  ViewBag.AttendanceList = Attendancelist;
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewEarnLeavePaymentInfo", "Leave");
                }
            }

        }



        [HttpGet]
        public ActionResult EarnALeaveCalcuation()
        {
            ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeListForEarnLeave(), "Value", "Text");
            VM_HRMS_Earn_Leave_Payment model = new VM_HRMS_Earn_Leave_Payment();
            model.DataList = new List<VM_HRMS_Earn_Leave_Payment>();
            //   model.Counter = 1;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EarnALeaveCalcuation(VM_HRMS_Earn_Leave_Payment model)
        {
            ViewBag.EmployeeList = new SelectList(d.GetAllEmployeeListForEarnLeave(), "Value", "Text");
            model.IsAudit = true;
            await Task.Run(() => model = model.EarnLeaveCalculation(model));
            model.ToDate = model.ToDate;
            model.EmployeeId = model.EmployeeId;
           // model.IsAudit = true;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EarnLeaveCalcuationASave(VM_HRMS_Earn_Leave_Payment model)
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    model.IsAudit = true;
                    await Task.Run(() => model.EarnLeaveCalculationSave(model));
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Employee Earn Leave Payment Saved Successfully.";
                    return RedirectToAction("EarnALeaveCalcuation", "Leave");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("EarnALeaveCalcuation", "Leave");
                }
            }
        }


        // Below Action used to View Employee Attendance Information
        // Database: OSS.HRMS
        // Table Name :HRMS_Earn_Leave_Payment
        // Domain Model Name: HRMS_Leave
        // View Model Name: VM_HRMS_Earn_Leave_Payment
        // Developed by: Anis
        // Date: 2018-10-31
        [HttpGet]
        public async Task<ActionResult> ViewEarnLeavePaymentAInfo()
        {
            //if (Session["Role"] != null)
            {
                try

                {
                    VM_HRMS_Earn_Leave_Payment model = new VM_HRMS_Earn_Leave_Payment();
                    model.DataList = new List<VM_HRMS_Earn_Leave_Payment>();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewEarnLeavePaymentAInfo", "Leave");
                }
            }

        }
        // Below Action used to View Employee Attendance Information
        // Database: OSS.HRMS
        // Table Name :HRMS_Earn_Leave_Payment
        // Domain Model Name: HRMS_Leave
        // View Model Name: VM_HRMS_Earn_Leave_Payment
        // Developed by: Anis
        // Date: 2018-10-31
        [HttpPost]
        public async Task<ActionResult> ViewEarnLeavePaymentAInfo(VM_HRMS_Earn_Leave_Payment model)
        {
            {
                try
                {
                    model.IsAudit = true;
                    await Task.Run(() => model = model.EarnLeaveCalculationInformation(model));
                    //  ViewBag.AttendanceList = Attendancelist;
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewEarnLeavePaymentAInfo", "Leave");
                }
            }

        }

        #endregion

    }
}
