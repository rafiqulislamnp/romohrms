﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using System.Data;
using Oss.Hrms.Models.ViewModels.Reports;
using System.Web.Helpers;
using Oss.Hrms.Models.ViewModels;
using Oss.Hrms.Helper;
using CrystalDecisions.CrystalReports.Engine;
using System.Media;
using System.Web.UI;
using System.Windows.Media.Imaging;
using System.Windows;

namespace Oss.Hrms.Controllers
{
    public class ReportController : Controller
    {
        DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();
        VMReportPayroll reportPayroll = new VMReportPayroll();
        CurrentUser user;

        #region ConstantValue
        private decimal Medical = 600;
        private decimal Food = 900;
        private decimal Transport = 350;
        private decimal FoodMedicalTransport = 1850;
        private decimal HouseRate = (decimal)0.5;
        private decimal Rate = (decimal)1.5;
        private decimal DaysOfMonth = 30;

        #endregion


        public ReportController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        // GET: Report
        public ActionResult Index()
        {

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> AllReport()
        {
            ViewBag.ReportList = new SelectList(d.GetReportList(), "Value", "Text");
            ViewBag.DesignationList = new SelectList(d.GetEmployeeDesignationsListwithAll(), "Value", "Text");
            ViewBag.GradeList = new SelectList(d.GetGradeList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsListWithAll(), "Value", "Text");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AllReport(VM_Report model)
        {
            DataTable table = new DataTable();
            var filename = string.Empty;

            if (model.ReportId == 1)
            {
                filename = "Present Employee Information";
                table = GetPresentEmployeeReport();
            }
            else if (model.ReportId == 2)
            {
                filename = "Employee Address Information-01";
                table = GetEmployeeAddressReport01();

            }
            else if (model.ReportId == 3)
            {
                filename = "Employee Address Information-02";
                table = GetEmployeeAddressReport02();
            }
            else if (model.ReportId == 4)
            {
                filename = "Employee Gender Information";
                table = GetGenderReport();
            }
            else if (model.ReportId == 5)
            {
                filename = "Preodic Joining List";
                table = GetJoiningListReport(model);
            }
            else if (model.ReportId == 6)
            {
                filename = "Staff Worker Summery";
                table = model.StaffAndWorkerSummery();
            }
            else if (model.ReportId == 7)
            {
                filename = "Male Female Report";
                table = model.MaleFemaleReport();
            }
            else if (model.ReportId == 8)
            {
                filename = "Desingation Wise Employee Report";
                table = model.DesignationWiseEmployeeReport(model.Designation_Id);
            }
            else if (model.ReportId == 9)
            {
                filename = "Grade Wise Employee Report";
                table = model.GradeWiseEmployeeReport(model.Grade);
            }
            else if (model.ReportId == 10)
            {
                filename = "Age Wise Employee Report";
                table = model.AgeRangeWiseEmployeeReport(model.From, model.To);
            }
            else if (model.ReportId == 11)
            {
                filename = "Salary Range Wise Employee Report";
                table = model.SalaryRangeWiseEmployeeReport(model.From, model.To);
            }
            else if (model.ReportId == 12)
            {
                filename = "Job Duration Employee Report";
                table = model.JobDurationWiseEmployeeReport(model.Todate, model.SectionID);
            }
            else
            {
                return Content("No One");
            }
            GenerateReport(table, filename);

            return RedirectToAction("AllReport");
        }

        [HttpGet]
        public ActionResult SelectedEmployeeReport()
        {
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsListWithAll(), "Value", "Text");
            // ViewBag.DesignationList = new SelectList(d.GetDesignationList(), "Value", "Text");
            VMEmployeeReport model = new VMEmployeeReport();
            model.DataList = new List<VMEmployeeReport>();
            model.Counter = 1;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SelectedEmployeeReport(VMEmployeeReport model)
        {
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsListWithAll(), "Value", "Text");
            //ViewBag.DesignationList = new SelectList(d.GetDesignationList(), "Value", "Text");
            await Task.Run(() => model.GetEmployeeListForSelectedEmployeeReport(model));
            model.Counter = 1;
            // model.CardIssueDate = DateTime.Today;
            return View(model);
        }

        [HttpPost]
        public ActionResult SelectedEmployeeReportPrint(VMEmployeeReport model)
        {
            VM_Report vmReport = new VM_Report();
            DataTable table = new DataTable();
            var filename = string.Empty;
            filename = "Selected Employee List";
            table = vmReport.GetSelectedEmployeeList(model);
            GenerateReport(table, filename);

            return RedirectToAction("SelectedEmployeeReport");
        }


        [HttpGet]
        public async Task<ActionResult> LeaveReport()
        {
            ViewBag.ReportList = new SelectList(d.GetLeaveReportList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsListWithAll(), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(d.GetEmployeeListforLeaveApplicationwithAll(), "Value", "Text");
            ViewBag.ResignEmployeeList = new SelectList(d.GetEResignEmployeeListforEarnLeaveReport(), "Value", "Text");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LeaveReport(VM_Report model)
        {
            DataTable table = new DataTable();
            var filename = string.Empty;
            if (model.ReportId == 20)
            {
                table = model.MonthlyLeaveReprt(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Monthly Leave Report");
            }
            else if (model.ReportId == 21)
            {
                table = model.SectionWiseMonthlyLeaveReprt(model.Fromdate, model.Todate, model.SectionID);
                GenerateReport(table, "Section Wise Leave Report");
            }
            else if (model.ReportId == 22)
            {
                table = model.MeternityMonthlyLeaveReprt(model.Fromdate, model.Todate);
                GenerateReport(table, "Meternity Leave Report");
            }
            else if (model.ReportId == 23)
            {
                // table = model.MeternityMonthlyLeaveReprt(model.Fromdate, model.Todate);
                table = model.ResignationEarnLeaveReprt(model.EmployeeId1);
                GenerateReport(table, "Resignation Leave Report");
            }
            else
            {
                return Content("No One");
            }
            GenerateReport(table, filename);
            return RedirectToAction("LeaveReport");
        }


        public async Task<ActionResult> AttendanceReport()
        {
            ViewBag.ReportList = new SelectList(d.GetAttendanceReportList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsListWithAll(), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(d.GetEmployeeListforLeaveApplicationwithAll(), "Value", "Text");
            ViewBag.ResignEmployeeList = new SelectList(d.GetEResignEmployeeListforEarnLeaveReport(), "Value", "Text");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AttendanceReport(VM_Report model)
        {
            DataTable table = new DataTable();
            //DtDailyAttendanceSummery ds= new DtDailyAttendanceSummery();
            var filename = string.Empty;
            if (model.ReportId == 7)
            {
                // filename = "Daily Attendance Summery";
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyAttendanceSummery(model.Fromdate);
                }
                else
                {
                    table = model.DailyAttendanceDetail(model.Fromdate);
                }
                GenerateReport(table, "Daily Attendance Excel Formate");
            }
            else if (model.ReportId == 30)
            {
                try
                {
                    DtDailyAttendanceSummery ds = new DtDailyAttendanceSummery();
                    ds = model.DailyAttendanceDetails(model.Fromdate);
                    ReportClass cr = new ReportClass();
                    cr.FileName = Server.MapPath("~/Views/Reports/DailyAttendanceDetail.rpt");
                    cr.Load();
                    cr.SetDataSource(ds);
                    cr.SummaryInfo.ReportTitle = "Daily Attendance Details ";
                    Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    return File(stream, "application/pdf");
                }
                catch (Exception ex)
                {
                    return File(ex.Message, "application/pdf");
                }

            }
            else if (model.ReportId == 28)
            {
                try
                {
                    DtDailyAttendanceSummery ds = new DtDailyAttendanceSummery();
                    ds= model.DailyAttendanceSummery1(model.Fromdate);
                    ReportClass cr = new ReportClass();
                    cr.FileName = Server.MapPath("~/Views/Reports/DailyAttendanceSummary.rpt");
                    cr.Load();
                    cr.SetDataSource(ds);
                    cr.SummaryInfo.ReportTitle = "Daily Attendance Summary ";
                    Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    return File(stream, "application/pdf");
                }
                catch (Exception ex)
                {
                    return File(ex.Message, "application/pdf");
                }

            }
            else if (model.ReportId == 8)
            {
                table = model.PersonalMonthlyAttendanceSummery(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Personal Attendance Summary");
            }

            //else if (model.ReportId == 9)
            //{
            //    table = model.DailyInvalidAttendance(model.Fromdate);
            //    GenerateReport(table, "Daily Invalid Attendance");
            //}
            else if (model.ReportId == 10)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Attendance Report");

            }
            else if (model.ReportId == 11)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyLateAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyLateAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Late Report");
            }
            else if (model.ReportId == 12)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyAbsentAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyAbsentAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Absent Report");
            }
            else if (model.ReportId == 13)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyInvalidAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyInvalidAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Invalid Report");
            }
            else if (model.ReportId == 14)
            {
                table = model.SectionWiseTiffinList(model.Fromdate);
                GenerateReport(table, "Section Wise Tiffin List Report");
            }
            else if (model.ReportId == 15)
            {
                table = model.PersonalMonthlyAttendanceSummeryBuyerReprt(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Personal Monthly Attendance Summery");
            }
            else if (model.ReportId == 9)
            {
                table = model.ThreeHourPersonalMonthlyAttendanceSummery(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Personal Monthly Attendance Summery");
            }
            else if (model.ReportId == 16)
            {
                table = model.OvertimeReport(model.Fromdate);
                GenerateReport(table, "Overtime Report");
            }
            else if (model.ReportId == 17)
            {
                table = model.TotalMonthlyAttendnanceReprt(model.Fromdate, model.Todate);
                GenerateReport(table, "Total Monthly Attendance Report With In Out and Overtime");
            }
            else if (model.ReportId == 18)
            {
                table = model.SectionWiseTiffinListSummery(model.Fromdate);
                GenerateReport(table, "Section Wise Summery Tiffin List Report");
            }
            else if (model.ReportId == 26)
            {
                Session["FromDate"] = model.Fromdate;
                Session["ToDate"] = model.Todate;
                Session["EmployeeId"] = model.EmployeeId;
                return RedirectToAction("EmployeeJobCard", "Report");
            }
            else if (model.ReportId == 27)
            {
                Session["FromDate"] = model.Fromdate;
                Session["ToDate"] = model.Todate;
                Session["SectionId"] = model.SectionID;
                Session["SectionName"] = model.SectionName;
                return RedirectToAction("EmployeeJobCardSummery", "Report");

            }
            else if (model.ReportId == 29)
            {
                try
                {
                    DtYearlyAttendance ds = new DtYearlyAttendance();
                    ds = model.YearlyAttendanceSummary(model.Fromdate, model.Todate, model.EmployeeId);
                    ReportClass cr = new ReportClass();
                    cr.FileName = Server.MapPath("~/Views/Reports/YearlyAttendance.rpt");
                    cr.Load();
                    cr.SetDataSource(ds);
                    cr.SummaryInfo.ReportTitle = "Personal Yearly Attendance Summary ";
                    Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    return File(stream, "application/pdf");
                }
                catch (Exception ex)
                {
                    return File(ex.Message, "application/pdf");
                }

            }
            else
            {
                return Content("No One");
            }
            GenerateReport(table, filename);
            return RedirectToAction("LeaveReport");
        }

        #region Audit Attendance Report

        public async Task<ActionResult> AttendanceAReport()
        {
            ViewBag.ReportList = new SelectList(d.GetAttendanceAReportList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsListWithAll(), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(d.GetEmployeeListforLeaveApplicationwithAll(), "Value", "Text");
            ViewBag.ResignEmployeeList = new SelectList(d.GetEResignEmployeeListforEarnLeaveReport(), "Value", "Text");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AttendanceAReport(VM_Report model)
        {
            DataTable table = new DataTable();
            var filename = string.Empty;
            if (model.ReportId == 7)
            {
                // filename = "Daily Attendance Summery";
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyAttendanceSummery(model.Fromdate);
                }
                else
                {
                    table = model.DailyAttendanceDetail(model.Fromdate);
                }
                GenerateReport(table, "Daily Attendance Detail");
            }
            else if (model.ReportId == 8)
            {
                table = model.PersonalMonthlyAttendanceSummery(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Personal Attendance Summery");
            }

            //else if (model.ReportId == 9)
            //{
            //    table = model.DailyInvalidAttendance(model.Fromdate);
            //    GenerateReport(table, "Daily Invalid Attendance");
            //}
            else if (model.ReportId == 10)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Attendance Report");

            }
            else if (model.ReportId == 11)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyLateAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyLateAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Late Report");
            }
            else if (model.ReportId == 12)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyAbsentAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyAbsentAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Absent Report");
            }
            else if (model.ReportId == 13)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyInvalidAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyInvalidAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Invalid Report");
            }
            else if (model.ReportId == 14)
            {
                table = model.SectionWiseTiffinList(model.Fromdate);
                GenerateReport(table, "Section Wise Tiffin List Report");
            }
            else if (model.ReportId == 15)
            {
                table = model.PersonalMonthlyAttendanceSummeryBuyerReprt(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Personal Monthly Attendance Summery");
            }
            else if (model.ReportId == 9)
            {
                table = model.ThreeHourPersonalMonthlyAttendanceSummery(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Personal Monthly Attendance Summery");
            }
            else if (model.ReportId == 16)
            {
                table = model.OvertimeReport(model.Fromdate);
                GenerateReport(table, "Overtime Report");
            }
            else if (model.ReportId == 17)
            {
                table = model.TotalMonthlyAttendnanceReprt(model.Fromdate, model.Todate);
                GenerateReport(table, "Total Monthly Attendance Report With In Out and Overtime");
            }
            else if (model.ReportId == 18)
            {
                table = model.SectionWiseTiffinListSummery(model.Fromdate);
                GenerateReport(table, "Section Wise Summery Tiffin List Report");
            }
            else if (model.ReportId == 25)
            {
                //table = model.PersonalMonthlyAttendanceSummeryBuyerReprt(model.Fromdate, model.Todate, model.EmployeeId);
                Session["FromDate"] = model.Fromdate;
                Session["ToDate"] = model.Todate;
                Session["EmployeeId"] = model.EmployeeId;
                return RedirectToAction("EmployeeJobCard", "Report");
                //table = model.SectionWiseTiffinListSummery(model.Fromdate);
                //GenerateReport(table, "Section Wise Summery Tiffin List Report");
            }
            else
            {
                return Content("No One");
            }
            GenerateReport(table, filename);
            return RedirectToAction("AttendanceAReport");
        }



        public async Task<ActionResult> EmployeeJobCard()
        {


            VM_Report model = new VM_Report();
            if (Session["FromDate"] != null && Session["ToDate"] != null && Session["EmployeeId"] != null)
            {

                var user1 = (Oss.Hrms.Models.Services.CurrentUser)Session["User"];


                model.Fromdate = Convert.ToDateTime(Session["FromDate"].ToString());
                model.Todate = Convert.ToDateTime(Session["ToDate"].ToString());
                model.EmployeeId = Convert.ToInt32(Session["EmployeeId"].ToString());
                ViewBag.StartDate = model.Fromdate;
                ViewBag.EndDate = model.Todate;
                if (user1.ID == 2)
                {
                    await Task.Run(() => model.PersonalMonthlyAttendanceSummeryBuyerReprt1(model.Fromdate, model.Todate, model.EmployeeId));
                    return View(model);
                }
                else if (user1.ID == 1)
                {
                    await Task.Run(() => model.PersonalMonthlyAttendanceSummery1(model.Fromdate, model.Todate, model.EmployeeId));
                    return View(model);
                }
                else
                {
                    await Task.Run(() => model.PersonalMonthlyAttendanceSummery1(model.Fromdate, model.Todate, model.EmployeeId));
                    return View(model);
                }
                return View(model);
            }
            else
            {
                Session["FromDate"] = null;
                Session["ToDate"] = null;
                Session["EmployeeId"] = null;
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult AuditJobCard()
        {

            VM_Report model = new VM_Report();
            model.ReportList = new List<VMAttendanceSummary>();
            model.Todate = DateTime.Now;
            model.Fromdate = model.Todate.AddMonths(-1);
            ViewBag.EmployeeList = new SelectList(d.GetActiveEmployeeListwithAll(), "Value", "Text");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AuditJobCard(VM_Report model)
        {
            ViewBag.EmployeeList = new SelectList(d.GetActiveEmployeeListwithAll(), "Value", "Text");
            model = model.PersonalMonthlyAttendanceSummeryBuyerReprtBSCI(model);
            return View(model);
        }


        public ActionResult PeriodicJobCard()
        {

            VM_Report model = new VM_Report();
            model.ListData = new List<VM_Report>();
            model.Todate = DateTime.Now;
            model.Fromdate = model.Todate.AddYears(-1);
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PeriodicJobCard(VM_Report model)
        {
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsList(), "Value", "Text");
            model = model.JobCardSummery(model);
            return View(model);
        }
        public ActionResult EmployeeJobCardSummery()
        {
            VM_Report model = new VM_Report();
            if (Session["FromDate"] != null && Session["ToDate"] != null)
            {
                //var user1 = (Oss.Hrms.Models.Services.CurrentUser)Session["User"];
                model.Fromdate = Convert.ToDateTime(Session["FromDate"].ToString());
                model.Todate = Convert.ToDateTime(Session["ToDate"].ToString());
                model.SectionID = Convert.ToInt32(Session["SectionId"].ToString());
                ViewBag.SectionName = Session["SectionName"].ToString();
                var firstDayOfMonth = new DateTime(model.Fromdate.Year, model.Fromdate.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                ViewBag.StartDate = firstDayOfMonth;
                ViewBag.EndDate = lastDayOfMonth;
                try
                {
                    ViewBag.TableData = model.TotalMonthlyAttendnanceSummaryReprt(firstDayOfMonth, lastDayOfMonth, model.SectionID);
                    var list = ViewBag.TableData;
                    return View(model);
                }
                catch (Exception ex)
                {
                    //Session["FromDate"] = null;
                    //Session["ToDate"] = null;
                    //Session["EmployeeId"] = null;
                    //return RedirectToAction("Index", "Home");
                    return View(model);
                }
            }
            else
            {
                //Session["FromDate"] = null;
                //Session["ToDate"] = null;
                //Session["EmployeeId"] = null;
                //return RedirectToAction("Index", "Home");
                return View(model);
            }
        }

        #endregion

        [HttpGet]
        public ActionResult PayrollReport()
        {
            ViewBag.ReportList = new SelectList(d.GetPayrollReportList(), "Value", "Text");
            ViewBag.PayrollList = new SelectList(d.GetPayrollList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.TitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PayrollReport(VM_Report model)
        {
            DataTable table = new DataTable();
            var filename = string.Empty;

            if (model.ReportId == 1)
            {
                if (model.SectionID > 0)
                {
                    filename = "Salary Sheet";
                    table = reportPayroll.GetExcelSalarySheet(model.SectionID, model.PayrollID);
                }
            }
            else if (model.ReportId == 7)
            {
                filename = "Summary Salary Sheet";
                table = reportPayroll.GetSalarySummarySheet(model.PayrollID);
            }
            else if (model.ReportId == 11)
            {
                filename = "Summary Lefty Salary Sheet";
                table = reportPayroll.GetSalaryLeftySummarySheet(model.PayrollID);
            }
            else if (model.ReportId == 2)
            {
                filename = "Salary and Extra OT Sheet";
                table = reportPayroll.GetMonthlyOTSalary(model.PayrollID, model.SectionID);
            }
            else if (model.ReportId == 3)
            {
                filename = "Worker Night Payable Sheet";
                table = reportPayroll.GetWorkerNightPayable(model);
            }
            else if (model.ReportId == 4)
            {
                filename = "Salary Increment Report";
                table = reportPayroll.GetSalaryIncrement(model);
            }
            else if (model.ReportId == 5)
            {
                filename = "Staff Night and Holiday Bill Report";
                table = reportPayroll.GetStaffNightAndHDBill(model);
            }
            else if (model.ReportId == 9)
            {
                filename = "Staff Night & Holiday Summary Report";
                table = reportPayroll.GetStaffNightAndHDBillSummery(model);
            }
            else if (model.ReportId == 6)
            {
                filename = "ExOT Night Salary";
                table = reportPayroll.GetExtraOTNightSalary(model);
            }
            else if (model.ReportId == 10)
            {
                filename = "ExOT Night Salary Summary";
                table = reportPayroll.GetExtraOTNightSalarySummary(model);
            }
            else if (model.ReportId == 8)
            {
                filename = "Lefty Extra OT Salary Payable";
                table = reportPayroll.GetExtraOTForLeftSalary(model);
            }
            GenerateReport(table, filename);
            return RedirectToAction("PayrollReport");
        }

        [HttpGet]
        public ActionResult GenerateIDCard()
        {
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.DesignationList = new SelectList(d.GetDesignationList(), "Value", "Text");
            VMPersonalReport model = new VMPersonalReport();
            model.DataList = new List<VMPersonalReport>();
            model.Counter = 1;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> GenerateIDCard(VMPersonalReport model)
        {
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.DesignationList = new SelectList(d.GetDesignationList(), "Value", "Text");
            await Task.Run(() => model.GetEmployeeListForID(model));
            model.Counter = 1;
            model.CardIssueDate = DateTime.Today;
            model.CardExpiredDate = DateTime.Today.AddMonths(18);
            return View(model);
        }

        [HttpPost]
        public ActionResult GenerateCard(VMPersonalReport model)
        {
            try
            {
                DsEmployeeReport ds = new DsEmployeeReport();
                ds = GetGenerateIDCard(model);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/CrpEmployeeID.rpt");
                cr.Load();
                cr.SetDataSource(ds);
                cr.SummaryInfo.ReportTitle = "IDCard";
                Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {
                return File(ex.Message, "application/pdf");
            }

        }

        #region PersonalReport

        private DataTable GetPresentEmployeeReport()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Serial", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Gender", typeof(string));
            table.Columns.Add("Father/Husband Name", typeof(string));
            table.Columns.Add("Father/Husband", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Date of Birth", typeof(string));

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         join r in db.Lines on o.LineId equals r.ID
                         where o.Present_Status == 1
                         select new
                         {
                             empID = o.ID,
                             mstatus = o.Marital_Status,
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             Line = r.Line,
                             Gender = o.Gender,
                             FatherName = o.Father_Name,
                             JoinDate = o.Joining_Date,
                             BirthDate = o.DOB
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ToList();
            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    string FatherORHusbandName = string.Empty;
                    string FatherORHusband = string.Empty;
                    if (v.Gender == "Female" && !string.IsNullOrEmpty(v.Gender) && v.mstatus == "Married" && !string.IsNullOrEmpty(v.mstatus))
                    {
                        FatherORHusband = "H";
                        var getHusband = db.HrmsSpouses.Where(a => a.Employee_Id == v.empID && a.Status == true);
                        if (getHusband.Any())
                        {
                            FatherORHusbandName = getHusband.FirstOrDefault().SpouseName;
                        }
                    }
                    else
                    {
                        FatherORHusbandName = v.FatherName;
                        FatherORHusband = "F";
                    }


                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.Line,
                        v.Gender,
                        FatherORHusbandName,
                        FatherORHusband,
                        Helpers.DateString(v.JoinDate),
                        Helpers.DateString(v.BirthDate)
                        );
                }
            }

            return table;
        }

        private DataTable GetEmployeeAddressReport01()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Serial", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Father Name", typeof(string));
            table.Columns.Add("Gender", typeof(string));
            table.Columns.Add("Permanent Address", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Mobile", typeof(string));
            table.Columns.Add("Birth Date", typeof(string));

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 1
                         select new
                         {
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             FatherName = o.Father_Name,
                             Gender = o.Gender,
                             Address = o.Permanent_Address,
                             JoinDate = o.Joining_Date,
                             ModileNo = o.Mobile_No,
                             BirthDate = o.DOB
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ToList();
            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.FatherName,
                        v.Gender,
                        v.Address,
                        Helpers.DateString(v.JoinDate),
                        v.ModileNo,
                        Helpers.DateString(v.BirthDate)
                        );
                }
            }

            return table;
        }

        private DataTable GetEmployeeAddressReport02()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Serial", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Present Address", typeof(string));
            table.Columns.Add("Permanent Address", typeof(string));
            table.Columns.Add("Phone", typeof(string));
            table.Columns.Add("Mobile", typeof(string));
            table.Columns.Add("Email", typeof(string));

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 1
                         select new
                         {
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             PresentAddress = o.Present_Address,
                             PermanentAddress = o.Permanent_Address,
                             Phone = o.Photo,
                             ModileNo = o.Mobile_No,
                             Email = o.Email
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ToList();
            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.PresentAddress,
                        v.PermanentAddress,
                        v.Phone,
                        v.ModileNo,
                        v.Email
                        );
                }
            }

            return table;
        }

        private DataTable GetGenderReport()
        {
            DataTable table = new DataTable();

            table.Columns.Add("Serial", typeof(Int32));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Gender", typeof(string));
            table.Columns.Add("Blood Group", typeof(string));
            table.Columns.Add("Present Address", typeof(string));
            table.Columns.Add("Permanent Address", typeof(string));


            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 1
                         select new
                         {
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             Gender = o.Gender,
                             PresentAddress = o.Present_Address,
                             PermanentAddress = o.Permanent_Address,
                             BloodGroup = o.Blood_Group
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ToList();

            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.Gender,
                        v.BloodGroup,
                        v.PresentAddress,
                        v.PermanentAddress
                        );
                }
            }

            return table;
        }

        private DataTable GetJoiningListReport(VM_Report model)
        {
            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Joining_Date >= model.Fromdate && o.Joining_Date <= model.Todate && o.Present_Status == 1
                         select new
                         {
                             empId = o.ID,
                             CardTitle = o.CardTitle,
                             CardNo = o.EmployeeIdentity,
                             PunchCardNo = o.CardNo,
                             Name = o.Name,
                             DesignationName = p.Name,
                             SectionName = q.SectionName,
                             Grade = o.Grade,
                             JoinDate = o.Joining_Date
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ThenBy(a => a.JoinDate).ToList();

            DataTable table = new DataTable();
            table.Columns.Add("Serial", typeof(Int32));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("CardNo", typeof(string));
            table.Columns.Add("Punch CardNo", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Grade", typeof(string));
            table.Columns.Add("JoinDate", typeof(string));
            table.Columns.Add("Joining Salary", typeof(string));
            table.Columns.Add("Current Salary", typeof(string));

            decimal joinBasicSalary = decimal.Zero;
            decimal currentBasicSalary = decimal.Zero;
            decimal joinSalary = decimal.Zero;
            decimal currentSalary = decimal.Zero;
            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    joinBasicSalary = decimal.Zero;
                    currentBasicSalary = decimal.Zero;
                    joinSalary = decimal.Zero;
                    currentSalary = decimal.Zero;

                    var getSalary = db.HrmsEodRecords.Where(a => a.EmployeeId == v.empId && a.Eod_RefFk == 1);
                    if (getSalary.Any())
                    {
                        joinBasicSalary = getSalary.OrderBy(a => a.ID).FirstOrDefault().ActualAmount;
                        currentBasicSalary = getSalary.OrderByDescending(a => a.ID).FirstOrDefault().ActualAmount;
                        joinSalary = joinBasicSalary + (joinBasicSalary * this.HouseRate) + this.FoodMedicalTransport;
                        currentSalary = currentBasicSalary + (currentBasicSalary * this.HouseRate) + this.FoodMedicalTransport;
                    }
                    ++index;
                    table.Rows.Add(
                        index,
                        v.CardTitle,
                        v.CardNo,
                        v.PunchCardNo,
                        v.Name,
                        v.DesignationName,
                        v.SectionName,
                        v.Grade,
                        Helpers.ShortDateString(v.JoinDate),
                        Math.Round(joinSalary, 0, MidpointRounding.AwayFromZero),
                        Math.Round(currentSalary, 0, MidpointRounding.AwayFromZero)
                        );
                }
            }

            return table;
        }

        private void GenerateReport(DataTable table, string filename)
        {
            string attachment = "attachment; filename=" + "" + filename + ".xls";


            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            Response.Write(@"<!DOCTYPE html>");
            Response.AddHeader("content-disposition", attachment);

            Response.Charset = "utf-8";
            //Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            Response.Write("<br><br><br>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <tr>");
            //am getting my grid's column headers

            foreach (DataColumn dc in table.Columns)
            {      //write in new column
                Response.Write("<td style='background-color:#008080;color:#ffffff;font-size:14'>");
                //Get column headers  and make it as bold in excel columns
                Response.Write("<b>");
                Response.Write(dc.ColumnName);
                Response.Write("</b>");
                Response.Write("</td>");
            }
            Response.Write("</tr>");
            Response.Write("\n");
            string tab = "";

            foreach (DataRow row in table.Rows)
            {//write in new row
                int i = 0;
                Response.Write("<tr>");

                if (row[i + 1].ToString() == "Total")
                {
                    for (i = 0; i < table.Columns.Count; i++)
                    {
                        Response.Write(tab);
                        Response.Write("<td style='background-color:#FFB6C1'><strong>");
                        Response.Write(row[i].ToString());
                        Response.Write("</strong></td>");
                        tab = "\t";
                    }
                }
                else
                {
                    for (i = 0; i < table.Columns.Count; i++)
                    {
                        Response.Write(tab);
                        Response.Write("<td>");
                        Response.Write(row[i].ToString());
                        Response.Write("</td>");
                        tab = "\t";
                        //if (i == 0)
                        //{
                        //    Response.Write("<td style='background-color:#90EE90'>");
                        //    Response.Write(row[i].ToString());
                        //    Response.Write("</td>");
                        //}
                    }
                }
                Response.Write("</tr>");
                Response.Write("\n");
            }
            Response.Write("</table>");
            Response.Write("</font>");
            Response.Flush();
            Response.End();




            ////////////////////////
            // Response.ClearContent();
            // Response.Buffer = true;
            // Response.AddHeader("content-disposition", attachment);
            // Response.ContentType = "application/ms-excel";

            // Response.ContentEncoding = System.Text.Encoding.Unicode;
            // Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            //// System.IO.StringWriter sw = new System.IO.StringWriter();
            //// System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);


            // string tab = "";

            // foreach (DataColumn dc in table.Columns)
            // {
            //     Response.Write(tab + dc.ColumnName);
            //     tab = "\t";
            // }

            // Response.Write("\n");
            // //int i;

            // foreach (DataRow dr in table.Rows)
            // {
            //     tab = "";
            //     for (i = 0; i < table.Columns.Count; i++)
            //     {
            //         Response.Write(tab + dr[i].ToString());
            //         tab = "\t";
            //     }
            //     Response.Write("\n");
            // }

            // Response.Flush();
            // Response.End();
        }

        #endregion

        #region EmployeeReport

        [HttpGet]
        public ActionResult StatusReport()
        {
            DateTime today = DateTime.Today;
            DateTime previousMonth = DateTime.Today.AddMonths(-1);
            ViewBag.StatusList = new SelectList(d.GetEmployeeStatus(), "Value", "Text");
            VMPersonalReport model = new VMPersonalReport();
            model.DataList = new List<VMPersonalReport>();
            model.Fromdate = new DateTime(previousMonth.Year, previousMonth.Month, 1);
            model.ToDate = new DateTime(previousMonth.Year, previousMonth.Month, DateTime.DaysInMonth(previousMonth.Year, previousMonth.Month));
            return View(model);
        }

        [HttpPost]
        public ActionResult StatusReport(VMPersonalReport model)
        {
            ViewBag.StatusList = new SelectList(d.GetEmployeeStatus(), "Value", "Text");
            if (model.StatusId == 1)
            {
                model.GetResign(model.Fromdate, model.ToDate);
            }
            else if (model.StatusId == 2)
            {
                model.GetLefty(model.Fromdate, model.ToDate);
            }
            else if (model.StatusId == 3)
            {
                model.GetTerminate(model.Fromdate, model.ToDate);
            }
            else if (model.StatusId == 4)
            {
                model.GetJoin(model.Fromdate, model.ToDate);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult StatusDetails(VMPersonalReport model)
        {
            if (model.StatusId == 2)
            {
                model.GetLefty(model.Fromdate, model.ToDate);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase[] files)
        {
            int Counter = 0;
            if (ModelState.IsValid)
            {
                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        var InputFileName = Path.GetFileName(file.FileName);
                        var ServerSavePath = Server.MapPath("~/Assets/Images/EmpImages/") + InputFileName;
                        //var ServerSavePath = Path.Combine(Server.MapPath("~/UploadedFiles/") + InputFileName);
                        //Save file to server folder  
                        //file.SaveAs(ServerSavePath);
                        string CardNo = file.FileName.Substring(0, file.FileName.LastIndexOf("."));

                        var getEmployee = db.Employees.Where(a => a.EmployeeIdentity == CardNo);
                        if (getEmployee.Any())
                        {
                            file.SaveAs(ServerSavePath);
                            var update = getEmployee.FirstOrDefault();
                            update.Photo = InputFileName;
                            ++Counter;
                        }

                        ViewBag.UploadStatus = Counter.ToString() + "of" + files.Count().ToString() + " files uploaded successfully.";
                    }
                }
                db.SaveChanges();
            }

            return View();
        }
        #endregion

        public byte[] GetImage(string imgName)
        {
            byte[] data;

            string imgPath = string.Empty;
            string img = "Profile1.jpg";

            imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), imgName);

            if (System.IO.File.Exists(imgPath))
            {
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
            else
            {
                imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), img);
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
        }

        public byte[] GetSaveImage(string imgName, string ImagePath)
        {
            byte[] data;

            string imgPath = string.Empty;
            string img = "Profile1.jpg";

            // imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), imgName);

            imgPath = Path.Combine(Server.MapPath("E:/Romo Documents/Picture/Picture/RFCT"), imgName);

            if (System.IO.File.Exists(imgPath))
            {
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
            else
            {
                imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), img);
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
        }

        public DsEmployeeReport GetGenerateIDCard(VMPersonalReport model)
        {
            DsEmployeeReport ds = new DsEmployeeReport();
            DsEmployeeReport ds1 = new DsEmployeeReport();
            ds1.DtEmployeeIDCard.Rows.Clear();

            if (model.DataList.Any(a => a.IsChecked == true))
            {
                string imageName = string.Empty;
                //string imagePath = "~/Assets/Images/EmpImages/";
                string address = string.Empty;

                foreach (var v in model.DataList.Where(a => a.IsChecked == true))
                {
                    imageName = string.Empty;
                    ds.DtEmployeeIDCard.Rows.Clear();
                    var vData = from o in db.Employees
                                join p in db.Sections on o.Section_Id equals p.ID
                                join q in db.Designations on o.Designation_Id equals q.ID
                                join village in db.Village on o.PermanentVillage_ID equals village.ID into v_Join
                                from vill in v_Join.DefaultIfEmpty()
                                join district in db.District on o.PermanentDistrict_ID equals district.ID into d_Join
                                from dis in d_Join.DefaultIfEmpty()
                                join pstation in db.PoliceStation on o.PermanentPoliceStation_ID equals pstation.ID into ps_Join
                                from pst in ps_Join.DefaultIfEmpty()
                                join poffice in db.PostOffice on o.PermanentPostOffice_ID equals poffice.ID into po_Join
                                from post in po_Join.DefaultIfEmpty()
                                where o.ID == v.EmployeeId
                                select new
                                {
                                    EmpID = o.ID,
                                    empName = o.Name_BN,
                                    CardNo = o.EmployeeIdentity,
                                    CardTitle = o.CardTitle,
                                    Designation = q.DesigNameBangla,
                                    Section = p.SectionName,
                                    Joindate = o.Joining_Date,
                                    NationalID = o.NID_No,
                                    EmergencyPhone = o.Emergency_Contact_No,
                                    BloodGroup = o.Blood_Group,
                                    PresentAddress = o.Present_Address,
                                    ImageName = o.Photo,
                                    Grade = o.Grade,
                                    perVillage = vill != null ? vill.VillageName : "",
                                    perDistrict = dis != null ? dis.DistrictName : "",
                                    perPoliceStation = pst != null ? pst.PoliceStationName : "",
                                    perPostOffice = post != null ? post.PostOfficeName : ""
                                    //perVillage = db.Village.Any(a => a.ID == o.PermanentVillage_ID)==true ? db.Village.FirstOrDefault(a => a.ID == o.PermanentVillage_ID).VillageName : "",
                                    //perDistrict=db.District.Any(a=>a.ID==o.PermanentDistrict_ID)==true ? db.District.FirstOrDefault(a => a.ID == o.PermanentDistrict_ID).DistrictName: "",
                                    //perPoliceStation=db.PoliceStation.Any(a=>a.ID==o.PermanentPoliceStation_ID)==true? db.PoliceStation.FirstOrDefault(a => a.ID == o.PermanentPoliceStation_ID).PoliceStationName : "",
                                    //perPostOffice=db.PostOffice.Any(a=>a.ID==o.PermanentPostOffice_ID)==true? db.PostOffice.FirstOrDefault(a => a.ID == o.PermanentPostOffice_ID).PostOfficeName:""
                                };

                    if (vData.Any())
                    {
                        var takeSingle = vData.FirstOrDefault();

                        address = takeSingle.perVillage + ", " + takeSingle.perPoliceStation + ", " + takeSingle.perPostOffice + ", " + takeSingle.perDistrict;

                        byte[] imageData;
                        //imageData = GetImage("demo.jpg");
                        imageName = takeSingle.ImageName;
                        if (string.IsNullOrEmpty(imageName))
                        {
                            imageData = GetImage("demo.jpg");
                        }
                        else
                        {
                            imageData = GetImage(imageName);
                        }

                        ds.DtEmployeeIDCard.Rows.Add(
                            takeSingle.EmpID,
                            takeSingle.CardTitle + " " + takeSingle.CardNo,
                            takeSingle.CardTitle,
                            takeSingle.Designation,
                            takeSingle.Section,
                            model.CardIssueDate,
                            takeSingle.Joindate,
                            takeSingle.NationalID,
                            takeSingle.EmergencyPhone,
                            takeSingle.BloodGroup,
                            Helpers.ShortDateStringSlash(model.CardExpiredDate),
                            address,
                            imageData,
                            takeSingle.empName,
                            takeSingle.Grade
                            );

                        if (model.Counter >= 1)
                        {
                            for (int take = 1; take <= model.Counter; take++)
                            {
                                ds1.DtEmployeeIDCard.ImportRow(ds.DtEmployeeIDCard.Rows[0]);
                            }
                        }
                    }
                }
            }
            return ds1;
        }
        #region BSCI Extra OT Salary
        public ActionResult ExtraOTSalary()
        {

            ViewBag.PayrollList = new SelectList(d.GetPayrollList(), "Value", "Text");

            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExtraOTSalary(VM_Report model)
        {
            DataTable table = new DataTable();
            var filename = string.Empty;
        
            filename = "EOT Salary Sheet";
            table = reportPayroll.GetMonthlyOTSalaryForBSCI( model.PayrollID , model.SectionID);
            GenerateReport(table, filename);
            return RedirectToAction("ExtraOTSalary");
        }
       

        #endregion


    }
}
