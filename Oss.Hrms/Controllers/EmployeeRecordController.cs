﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using Oss.Hrms.Models.ViewModels.Promotion;

namespace Oss.Hrms.Controllers
{

    public class EmployeeRecordController : Controller
    {
        DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();
        VM_HRMS_Salary VMSalary;
        CurrentUser user;

        public EmployeeRecordController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        /// <summary>
        /// Load Form with Employee's All information to edit......
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> UpdateEmployeeImage(int? id)
        {
            // if (Session["Role"] != null)
            {
                if (id != null)
                {
                    //ViewBag.Coutries = new SelectList(d.GetCountryList(), "Value", "Text");
                    //ViewBag.Units = new SelectList(d.GetUnitList(), "Value", "Text");
                    //ViewBag.BUnits = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                    //ViewBag.Departments = new SelectList(d.GetDepartmentList(), "Value", "Text");
                    //ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
                    //ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                    //ViewBag.Designations = new SelectList(d.GetDesignationList(), "Value", "Text");

                    var emp = db.Employees.FirstOrDefault(s => s.ID == id);
                    VMEmployee model = new VMEmployee()
                    {
                        ID = emp.ID,
                        CardTitle = emp.CardTitle,
                        CardNo = emp.CardNo,
                        EmployeeIdentity = emp.EmployeeIdentity,
                        Name = emp.Name,
                        Name_BN = emp.Name_BN,
                        Father_Name = emp.Father_Name,
                        Mother_Name = emp.Mother_Name,
                        DOB = emp.DOB,
                        Gender = emp.Gender,
                        Designation_Id = emp.Designation_Id,
                        Country_Id = emp.Country_Id,
                        Business_Unit_Id = emp.Business_Unit_Id,
                        Unit_Id = emp.Unit_Id,
                        Department_Id = emp.Department_Id,
                        Section_Id = emp.Section_Id,
                        LineId = emp.LineId,
                        Blood_Group = emp.Blood_Group,
                        Joining_Date = /*(DateTime) */emp.Joining_Date,
                        Religion = emp.Religion,
                        Mobile_No = emp.Mobile_No,
                        Alt_Mobile_No = emp.Alt_Mobile_No,
                        Alt_Person_Name = emp.Alt_Person_Name,
                        Alt_Person_Contact_No = emp.Alt_Person_Contact_No,
                        Alt_Person_Address = emp.Alt_Person_Address,
                        Emergency_Contact_No = emp.Emergency_Contact_No,
                        Office_Contact = emp.Office_Contact,
                        Land_Phone = emp.Land_Phone,
                        Email_Id = emp.Email,
                        Employement_Type = emp.Employement_Type,
                        NID_No = emp.NID_No,
                        Passport_No = emp.Passport_No,
                        Present_Address = emp.Present_Address,
                        Permanent_Address = emp.Permanent_Address,
                        Marital_Status = emp.Marital_Status,
                        Staff_Type = emp.Staff_Type,
                        Overtime_Eligibility = emp.Overtime_Eligibility,
                        Grade = emp.Grade,
                        Present_Status = emp.Present_Status
                    };
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Employee Info Not Found!";
                return RedirectToAction("IndexEmployees", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// Load Form with Employee's All information to edit......
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UpdateEmployeeImage(VMEmployee vmEmployee, HttpPostedFileBase Photo)
        {

            var pic = "nopicture.png";
            if (Photo != null && Photo.ContentLength > 0)
            {
                string fileExtension = Path.GetExtension(Photo.FileName).ToLower();
                if (fileExtension == ".jpg" || fileExtension == ".png" || fileExtension == ".gif")
                {
                    // var npic = vmEmployee.ID;// Guid.NewGuid().ToString("N"); //imagefile.FileName;
                    var npic = vmEmployee.CardTitle + vmEmployee.EmployeeIdentity;
                    pic = npic + fileExtension;
                    string fileLocation = Server.MapPath("~/Assets/Images/EmpImages/") + pic;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Photo.SaveAs(fileLocation);
                    vmEmployee.Photo = pic;
                    var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_EmployeeImageUpdate]"
                                                + "@id='" + vmEmployee.ID + "', " +
                                                "@photo ='" + vmEmployee.Photo + "'");

                    if (a > 0)
                    {
                        //Session["success_div"] = "true";
                       // Session["success_msg"] = "Employee Shift Assign/Updated Successfully.";
                        return RedirectToAction("DetailEmployeeIdWise", "EmployeeRecord", new RouteValueDictionary(new { id = vmEmployee.ID }));
                        // return RedirectToAction("AddLeaveApplication", "Leave");
                    }
                    //try
                    //{
                    //    vmEmployee.Photo = pic;
                    //    HRMS_Employee employee = new HRMS_Employee()
                    //    {
                    //        ID = vmEmployee.ID,
                    //        Photo = vmEmployee.Photo,
                    //        //   QuitDate = vmEmployee.QuitDate,
                    //        Entry_By = "1",
                    //        Entry_Date = DateTime.Now
                    //    };
                    //    db.Entry(employee).State = EntityState.Modified;
                    //}
                    //catch (Exception ex)
                    //{
                    //    Session["warning_div"] = "true";
                    //    Session["warning_msg"] = ex.Message;
                    //    return View(vmEmployee);
                    //}

                }
                else
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Please upload only Image type file!";
                    return View(vmEmployee);
                }
            }
            return RedirectToAction("DetailEmployeeIdWise", "EmployeeRecord", new RouteValueDictionary(new { id = vmEmployee.ID }));
        }



        [HttpPost]
        public ActionResult UploadFile(VM_HRMS_Upload_Document model, HttpPostedFileBase Document)
        {
            //if (Session["Role"] != null)//&& Session["Role"].ToString() == "Admin")
            //{
            var pic = "nopicture.png";
            if (Document != null && Document.ContentLength > 0)
            {
                string fileExtension = Path.GetExtension(Document.FileName).ToLower();
                //if (fileExtension == ".jpg" || fileExtension == ".png" || fileExtension == ".gif")
                //{
                var npic = Guid.NewGuid().ToString("N"); //imagefile.FileName;
                pic = npic + fileExtension;
                string fileLocation = Server.MapPath("~/Assets/Upload/") + pic;
                if (System.IO.File.Exists(fileLocation))
                {
                    System.IO.File.Delete(fileLocation);
                }
                Document.SaveAs(fileLocation);
                // }
                model.Document = pic;

                HRMS_Upload_Document upload = new HRMS_Upload_Document()
                {
                    Employee_Id = model.Employee_Id,
                    Document_Type = model.Document_Type,
                    Document_Name = model.Document_Name,
                    Document = model.Document,
                    Entry_By = "1",
                    Entry_Date = DateTime.Now
                };
                db.HrmsUploadDocument.Add(upload);
                db.SaveChanges();
            }

            return RedirectToAction("DetailEmployeeIdWise", "EmployeeRecord", new RouteValueDictionary(new { id = model.Employee_Id }));

            // }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Dear Admin, you must log in to add employee spouse info";
            //return RedirectToAction("Login", "Account");
        }


        #region Employee Information Details

        //private VMEmployee vmEmployee;

        // GET: HRMS_Employee
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This is list view for all employees.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> IndexEmployees()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    VMEmployee  model = new VMEmployee();
                    //  await Task.Run(() => vmEmployee.ListVmEmployees());
                    //ListVmEmployees
                    ViewBag.Departments = new SelectList(d.GetDepartmentList(), "Value", "Text");
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("Index", "EmployeeRecord");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// This is list view for all employees.
        /// </summary>
        /// <returns></returns>
         [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> IndexEmployees(VMEmployee model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    await Task.Run(() => model.ListVmEmployees(model.Department_Id, model.Present_Status));
                    ViewBag.Departments = new SelectList(d.GetDepartmentListforEmployeeSearch(), "Value", "Text");
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("Index", "EmployeeRecord");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// Load form for Entering Employee basic Information. An employee have to provide his/her prettry basic 
        /// information.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddEmployee()
        {
            //if (Session["ROLE"] != null)
            {
                ViewBag.CardTitleList = new SelectList(d.GetActiveCardTitleList(), "Value", "Text");
                ViewBag.Coutries = new SelectList(d.GetCountryList(), "Value", "Text");
                ViewBag.BUnits = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                ViewBag.Departments = new SelectList(d.GetDepartmentList(), "Value", "Text");
                ViewBag.Designations = new SelectList(d.GetDesignationList(), "Value", "Text");
                ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                ViewBag.DistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");
                return View();
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// This action helps to store basic information of an employee. It also stores image file
        /// </summary>
        /// <param name="vmEmployee"></param>
        /// <param name="imageFile"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEmployee(VMEmployee vmEmployee, HttpPostedFileBase Photo)
        {
            //if (Session["ROLE"] != null)

            var dt = "01/01/1900";
            {
                // HRMS_Employee v = new HRMS_Employee();

                //t1.Present_Status == 1 &&
                var vsdf = (from t1 in db.Employees
                         where  t1.EmployeeIdentity == vmEmployee.EmployeeIdentity && t1.CardTitle == vmEmployee.CardTitle
                         select t1.EmployeeIdentity
                         ).SingleOrDefault();
              //  var fsdf = db.Employees.FirstOrDefault(s => s.EmployeeIdentity == vmEmployee.EmployeeIdentity && s.Present_Status == 1 && s.CardTitle == vmEmployee.CardTitle);
                    if (vsdf == null)
                    {
                    //try
                    {
                        ViewBag.CardTitleList = new SelectList(d.GetActiveCardTitleList(), "Value", "Text");
                        ViewBag.Coutries = new SelectList(d.GetCountryList(), "Value", "Text");
                        ViewBag.BUnits = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                        //ViewBag.Units = new SelectList(d.GetUnitList(), "Value", "Text");
                        ViewBag.Departments = new SelectList(d.GetDepartmentList(), "Value", "Text");
                        ViewBag.Line = new SelectList(d.GetLineList(),"Value", "Text");
                        ViewBag.Designations = new SelectList(db.Designations, "Id", "Name");
                        ViewBag.DistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");

                        var pic = "nopicture.png";
                        if (Photo != null && Photo.ContentLength > 0)
                        {
                            string fileExtension = Path.GetExtension(Photo.FileName).ToLower();
                            if (fileExtension == ".jpg" || fileExtension == ".png" || fileExtension == ".gif")
                            {
                              //  var npic = Guid.NewGuid().ToString("N"); //imagefile.FileName;
                                var npic = vmEmployee.CardTitle+vmEmployee.EmployeeIdentity;
                                pic = npic + fileExtension;
                                string fileLocation = Server.MapPath("~/Assets/Images/EmpImages/") + pic;
                                if (System.IO.File.Exists(fileLocation))
                                {
                                    System.IO.File.Delete(fileLocation);
                                }
                                Photo.SaveAs(fileLocation);
                            }
                            else
                            {
                                Session["warning_div"] = "true";
                                Session["warning_msg"] = "Please upload only Image type file!";
                                return View(vmEmployee);
                            }
                        }
                        vmEmployee.Photo = pic;
                        HRMS_Employee employee = new HRMS_Employee()
                        {
                            CardTitle = vmEmployee.CardTitle,
                            CardNo = vmEmployee.CardNo,
                            EmployeeIdentity = vmEmployee.EmployeeIdentity,
                            Name = vmEmployee.Name,
                            Name_BN = vmEmployee.Name_BN,
                            Father_Name = vmEmployee.Father_Name,
                            Mother_Name = vmEmployee.Mother_Name,
                            DOB = vmEmployee.DOB,
                            Gender = vmEmployee.Gender,
                            Designation_Id = vmEmployee.Designation_Id,
                            Country_Id = vmEmployee.Country_Id,
                            Business_Unit_Id = vmEmployee.Business_Unit_Id,
                            Unit_Id = vmEmployee.Unit_Id,
                            Department_Id = vmEmployee.Department_Id,
                            Section_Id = vmEmployee.Section_Id,
                            LineId = vmEmployee.LineId,
                            Blood_Group = vmEmployee.Blood_Group,
                            Joining_Date = /*(DateTime) */vmEmployee.Joining_Date,
                            JobPermanent_Date = vmEmployee.JobPermanent_Date,
                            Religion = vmEmployee.Religion,
                            Mobile_No = vmEmployee.Mobile_No,
                            Alt_Mobile_No = vmEmployee.Alt_Mobile_No,
                            Alt_Person_Name = vmEmployee.Alt_Person_Name,
                            Alt_Person_Contact_No = vmEmployee.Alt_Person_Contact_No,
                            Alt_Person_Address = vmEmployee.Alt_Person_Address,
                            Emergency_Contact_No = vmEmployee.Emergency_Contact_No,
                            Office_Contact = vmEmployee.Office_Contact,
                            Land_Phone = vmEmployee.Land_Phone,
                            Email = vmEmployee.Email_Id,
                            Employement_Type = vmEmployee.Employement_Type,
                            NID_No = vmEmployee.NID_No,
                            Passport_No = vmEmployee.Passport_No,
                            PresentDistrict_ID=vmEmployee.PresentDistrict_ID,
                            PresentPoliceStation_ID = vmEmployee.PresentPoliceStation_ID,
                            PresentPostOffice_ID = vmEmployee.PresentPostOffice_ID,
                            PresentVillage_ID = vmEmployee.PresentVillage_ID,
                            PermanentDistrict_ID = vmEmployee.PermanentDistrict_ID,
                            PermanentPoliceStation_ID = vmEmployee.PermanentPoliceStation_ID,
                            PermanentPostOffice_ID = vmEmployee.PermanentPostOffice_ID,
                            PermanentVillage_ID = vmEmployee.PermanentVillage_ID,
                            // Present_Address = vmEmployee.Present_Address,
                            //Permanent_Address = vmEmployee.Permanent_Address,
                            Marital_Status = vmEmployee.Marital_Status,
                            Photo = pic,
                            Present_Status =1,
                            Staff_Type = vmEmployee.Staff_Type,
                            Overtime_Eligibility = vmEmployee.Overtime_Eligibility,
                            Grade = vmEmployee.Grade,
                            QuitDate = Convert.ToDateTime(dt),//default(DateTime),
                            Entry_By = "1",
                            Entry_Date = DateTime.Now
                        };
                        db.Employees.Add(employee);
                        db.SaveChanges();
                         
                        if (vmEmployee.Marital_Status == "Married")
                        {
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Employee Added Successfully.";
                            return RedirectToAction("IndexEmployees", "EmployeeRecord");
                        }
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Employee Added Successfully.";
                        return RedirectToAction("IndexEmployees", "EmployeeRecord");
                        //return RedirectToAction("AddEducation", "EmployeeRecord");
                    }
                    //catch (Exception ex)
                    //{
                    //    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    //    {
                    //        Session["warning_div"] = "true";
                    //        Session["warning_msg"] = "Employee's info Can't Add, It is Already Exist!";
                    //        return RedirectToAction("AddEmployee", "EmployeeRecord");
                    //    }
                    //    Session["warning_div"] = "true";
                    //    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    //    return RedirectToAction("AddEmployee", "EmployeeRecord");
                    //}
                }
                Session["success_div"] = "true";
                Session["success_msg"] = "Employee Exist!.";
                return RedirectToAction("AddEmployee", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// Remove Employee Information.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> DeleteEmployee(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Employee ID Not Found!";
                    return RedirectToAction("IndexEmployees", "EmployeeRecord");
                }
                HRMS_Employee employee = await db.Employees.FindAsync(id);
                if (employee != null)
                {
                    try
                    {
                        db.Employees.Remove(employee);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Employee info Deleted Successfully.";
                        return RedirectToAction("IndexEmployees", "EmployeeRecord");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Employee info Can't Delete, It is Already in Use!";
                            return RedirectToAction("IndexEmployees", "EmployeeRecord");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("IndexEmployees", "EmployeeRecord");
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("IndexEmployees", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        /// <summary>
        /// Load Form with Employee's All information to edit......
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditEmployee(int? id)
        {
           // if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Coutries = new SelectList(d.GetCountryList(), "Value", "Text");
                    ViewBag.Units = new SelectList(d.GetUnitList(), "Value", "Text");
                    ViewBag.BUnits = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.Departments = new SelectList(d.GetDepartmentList(), "Value", "Text");
                    ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
                    ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                    ViewBag.Designations = new SelectList(d.GetDesignationList(), "Value", "Text");

                    ViewBag.DistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");
                    ViewBag.PoliceStationList = new SelectList(d.GetActivePoliceStationList(), "Value", "Text");
                    ViewBag.PostOfficeList = new SelectList(d.GetActivePostOfficeList(), "Value", "Text");
                    ViewBag.VillageList = new SelectList(d.GetActiveVillageList(), "Value", "Text");

                    var emp = db.Employees.FirstOrDefault(s => s.ID == id);
                    VMEmployee model = new VMEmployee()
                    {
                        ID = emp.ID,
                        CardTitle = emp.CardTitle,
                        CardNo = emp.CardNo,
                        EmployeeIdentity = emp.EmployeeIdentity,
                        Name = emp.Name,
                        Name_BN = emp.Name_BN,
                        Father_Name = emp.Father_Name,
                        Mother_Name = emp.Mother_Name,
                        DOB = emp.DOB,
                        Gender = emp.Gender,
                        Designation_Id = emp.Designation_Id,
                        Country_Id = emp.Country_Id,
                        Business_Unit_Id = emp.Business_Unit_Id,
                        Unit_Id = emp.Unit_Id,
                        Department_Id = emp.Department_Id,
                        Section_Id = emp.Section_Id,
                        LineId = emp.LineId,
                        Blood_Group = emp.Blood_Group,
                        Joining_Date = /*(DateTime) */emp.Joining_Date,
                        JobPermanent_Date = emp.JobPermanent_Date == null ? DateTime.Now : emp.JobPermanent_Date,
                        Religion = emp.Religion,
                        Mobile_No = emp.Mobile_No,
                        Alt_Mobile_No = emp.Alt_Mobile_No,
                        Alt_Person_Name = emp.Alt_Person_Name,
                        Alt_Person_Contact_No = emp.Alt_Person_Contact_No,
                        Alt_Person_Address = emp.Alt_Person_Address,
                        Emergency_Contact_No = emp.Emergency_Contact_No,
                        Office_Contact = emp.Office_Contact,
                        Land_Phone = emp.Land_Phone,
                        Email_Id = emp.Email,
                        Employement_Type = emp.Employement_Type,
                        NID_No = emp.NID_No,
                        Passport_No = emp.Passport_No,
                      //  Present_Address = emp.Present_Address,
                       // Permanent_Address = emp.Permanent_Address,
                        Marital_Status = emp.Marital_Status,
                        Staff_Type = emp.Staff_Type,
                        Overtime_Eligibility = emp.Overtime_Eligibility,
                        Grade = emp.Grade,
                        Present_Status = emp.Present_Status,
                        PresentDistrict_ID = Convert.ToInt32(emp.PresentDistrict_ID),
                        PresentPoliceStation_ID = Convert.ToInt32(emp.PresentPoliceStation_ID),
                        PresentPostOffice_ID = Convert.ToInt32(emp.PresentPostOffice_ID),
                        PresentVillage_ID = Convert.ToInt32(emp.PresentVillage_ID),
                        PermanentDistrict_ID = Convert.ToInt32(emp.PermanentDistrict_ID),
                        PermanentPoliceStation_ID = Convert.ToInt32(emp.PermanentPoliceStation_ID),
                        PermanentPostOffice_ID = Convert.ToInt32(emp.PermanentPostOffice_ID),
                        PermanentVillage_ID = Convert.ToInt32(emp.PermanentVillage_ID),

                        // QuitDate = DateTime.Today
                    };
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Employee Info Not Found!";
                return RedirectToAction("IndexEmployees", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Employee Basic Information.... 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditEmployee(VMEmployee vmEmployee)
        {
            //if (Session["Role"] != null)
            //Date = {1/1/0001 12:00:00 AM}
            //if (vmEmployee.QuitDate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
            //{
                vmEmployee.QuitDate = Convert.ToDateTime(vmEmployee.QuitDate1);
           // }
            {
                try
                {
                    ViewBag.Coutries = new SelectList(d.GetCountryList(), "Value", "Text");
                    ViewBag.BUnits = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.Departments = new SelectList(d.GetDepartmentList(), "Value", "Text");
                    ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                    ViewBag.Designations = new SelectList(d.GetDesignationList(), "Value", "Text");

                 //   var em = db.Employees.Where(x => x.ID == vmEmployee.ID).FirstOrDefault();
                    //village.VillageName = v.VillageName;
                    ////village.PostOffice_ID = v.PostOffice_ID;
                    //village.Update_By = "1";
                    //village.Update_Date = DateTime.Now;
                    //await db.SaveChangesAsync();
                    var em = db.Employees.Where(x => x.ID == vmEmployee.ID).FirstOrDefault();
                    em.CardTitle = vmEmployee.CardTitle;
                    em.CardNo = vmEmployee.CardNo;
                    em.EmployeeIdentity = vmEmployee.EmployeeIdentity;
                    em.Name = vmEmployee.Name;
                    em.Name_BN = vmEmployee.Name_BN;
                    em.Father_Name = vmEmployee.Father_Name;
                    em.Mother_Name = vmEmployee.Mother_Name;
                    em.DOB = vmEmployee.DOB;
                    em.Gender = vmEmployee.Gender;
                    em.Designation_Id = vmEmployee.Designation_Id;
                    em.Country_Id = vmEmployee.Country_Id;
                    em.Business_Unit_Id = vmEmployee.Business_Unit_Id;
                    em.Unit_Id = vmEmployee.Unit_Id;
                    em.Department_Id = vmEmployee.Department_Id;
                    em.Section_Id = vmEmployee.Section_Id;
                    em.LineId = vmEmployee.LineId;
                    em.Blood_Group = vmEmployee.Blood_Group;
                    em.Joining_Date = /*(DateTime) */vmEmployee.Joining_Date;
                    em.JobPermanent_Date = vmEmployee.JobPermanent_Date;
                    em.Religion = vmEmployee.Religion;
                    em.Mobile_No = vmEmployee.Mobile_No;
                    em.Alt_Mobile_No = vmEmployee.Alt_Mobile_No;
                    em.Alt_Person_Name = vmEmployee.Alt_Person_Name;
                    em.Alt_Person_Contact_No = vmEmployee.Alt_Person_Contact_No;
                    em.Alt_Person_Address = vmEmployee.Alt_Person_Address;
                    em.Emergency_Contact_No = vmEmployee.Emergency_Contact_No;
                    em.Office_Contact = vmEmployee.Office_Contact;
                    em.Land_Phone = vmEmployee.Land_Phone;
                    em.Email = vmEmployee.Email_Id;
                    em.Employement_Type = vmEmployee.Employement_Type;
                    em.NID_No = vmEmployee.NID_No;
                    em.Passport_No = vmEmployee.Passport_No;
                    //  Present_Address = vmEmployee.Present_Address;
                    // Permanent_Address = vmEmployee.Permanent_Address;
                    em.Marital_Status = vmEmployee.Marital_Status;
                  //  em.Photo = vmEmployee.Photo;
                    em.Present_Status = vmEmployee.Present_Status;
                    em.Staff_Type = vmEmployee.Staff_Type;
                    em.Overtime_Eligibility = vmEmployee.Overtime_Eligibility;
                    em.Grade = vmEmployee.Grade;
                    em.PresentDistrict_ID = vmEmployee.PresentDistrict_ID;
                    em.PresentPoliceStation_ID = vmEmployee.PresentPoliceStation_ID;
                    em.PresentPostOffice_ID = vmEmployee.PresentPostOffice_ID;
                    em.PresentVillage_ID = vmEmployee.PresentVillage_ID;
                    em.PermanentDistrict_ID = vmEmployee.PermanentDistrict_ID;
                    em.PermanentPoliceStation_ID = vmEmployee.PermanentPoliceStation_ID;
                    em.PermanentPostOffice_ID = vmEmployee.PermanentPostOffice_ID;
                    em.PermanentVillage_ID = vmEmployee.PermanentVillage_ID;
                    if (vmEmployee.QuitDate != Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                    {
                        em.QuitDate = vmEmployee.QuitDate;
                    }
                    em.Entry_By = "1";
                    em.Entry_Date = DateTime.Now;

                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Employee Updated Successfully.";
                    return RedirectToAction("IndexEmployees", "EmployeeRecord");

                    //OLD Code

                    //HRMS_Employee employee = new HRMS_Employee()
                    //{
                    //    ID = vmEmployee.ID,
                    //    CardTitle = vmEmployee.CardTitle,
                    //    CardNo = vmEmployee.CardNo,
                    //    EmployeeIdentity = vmEmployee.EmployeeIdentity,
                    //    Name = vmEmployee.Name,
                    //    Name_BN = vmEmployee.Name_BN,
                    //    Father_Name = vmEmployee.Father_Name,
                    //    Mother_Name = vmEmployee.Mother_Name,
                    //    DOB = vmEmployee.DOB,
                    //    Gender = vmEmployee.Gender,
                    //    Designation_Id = vmEmployee.Designation_Id,
                    //    Country_Id = vmEmployee.Country_Id,
                    //    Business_Unit_Id = vmEmployee.Business_Unit_Id,
                    //    Unit_Id = vmEmployee.Unit_Id,
                    //    Department_Id = vmEmployee.Department_Id,
                    //    Section_Id = vmEmployee.Section_Id,
                    //    LineId = vmEmployee.LineId,
                    //    Blood_Group = vmEmployee.Blood_Group,
                    //    Joining_Date = /*(DateTime) */vmEmployee.Joining_Date,
                    //    Religion = vmEmployee.Religion,
                    //    Mobile_No = vmEmployee.Mobile_No,
                    //    Alt_Mobile_No = vmEmployee.Alt_Mobile_No,
                    //    Alt_Person_Name = vmEmployee.Alt_Person_Name,
                    //    Alt_Person_Contact_No = vmEmployee.Alt_Person_Contact_No,
                    //    Alt_Person_Address = vmEmployee.Alt_Person_Address,
                    //    Emergency_Contact_No = vmEmployee.Emergency_Contact_No,
                    //    Office_Contact = vmEmployee.Office_Contact,
                    //    Land_Phone = vmEmployee.Land_Phone,
                    //    Email = vmEmployee.Email_Id,
                    //    Employement_Type = vmEmployee.Employement_Type,
                    //    NID_No = vmEmployee.NID_No,
                    //    Passport_No = vmEmployee.Passport_No,
                    //  //  Present_Address = vmEmployee.Present_Address,
                    //   // Permanent_Address = vmEmployee.Permanent_Address,
                    //    Marital_Status = vmEmployee.Marital_Status,
                    //    Photo = vmEmployee.Photo,
                    //    Present_Status = vmEmployee.Present_Status,
                    //    Staff_Type = vmEmployee.Staff_Type,
                    //    Overtime_Eligibility = vmEmployee.Overtime_Eligibility,
                    //    Grade = vmEmployee.Grade,
                    //    QuitDate = vmEmployee.QuitDate,
                    //    Entry_By = "1",
                    //    Entry_Date = DateTime.Now,
                    //    PresentDistrict_ID = vmEmployee.PresentDistrict_ID,
                    //    PresentPoliceStation_ID = vmEmployee.PresentPoliceStation_ID,
                    //    PresentPostOffice_ID = vmEmployee.PresentPostOffice_ID,
                    //    PresentVillage_ID = vmEmployee.PresentVillage_ID,
                    //    PermanentDistrict_ID = vmEmployee.PermanentDistrict_ID,
                    //    PermanentPoliceStation_ID = vmEmployee.PermanentPoliceStation_ID,
                    //    PermanentPostOffice_ID = vmEmployee.PermanentPostOffice_ID,
                    //    PermanentVillage_ID = vmEmployee.PermanentVillage_ID
                    //};

                    //  db.Entry(employee).State = EntityState.Modified;
                    
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("IndexEmployees", "EmployeeRecord");
                }
            }

            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        ///  Particular Employee Details Id Wise.........
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> DetailEmployeeIdWise(int? id)
        {
            VM_HRMS_LinkToEmployee link = new VM_HRMS_LinkToEmployee();
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    try
                    {
                        VMEmployee model = new VMEmployee();
                        await Task.Run(() => model = model.DetailsVmEmployeesIdWise(id.Value));
                        ViewBag.Spouse = db.HrmsSpouses.ToList().Where(s => s.Employee_Id == id);
                        ViewBag.Child = db.HrmsChildren.ToList().Where(ch => ch.Employee_Id == id);
                        ViewBag.Education = db.HrmsEducations.ToList().Where(ch => ch.Employee_Id == id);
                        ViewBag.Training = db.HrmsTrainings.ToList().Where(ch => ch.Employee_Id == id);
                        ViewBag.Experience = db.HrmsExperiences.ToList().Where(ch => ch.Employee_Id == id);
                        ViewBag.Skill = db.HrmsSkills.ToList().Where(ch => ch.Employee_Id == id);
                        ViewBag.Reference = db.HrmsReferences.ToList().Where(ch => ch.Employee_Id == id);
                        ViewBag.Hobby = db.HrmsHobbies.ToList().Where(ch => ch.Employee_Id == id);
                        ViewBag.Salary = db.HrmsSalaries.ToList().Where(ch => ch.Employee_Id == id);
                        ViewBag.Facility = db.HrmsFacilities.ToList().Where(ch => ch.Employee_Id == id);
                        ViewBag.Document = db.HrmsUploadDocument.ToList().Where(ch => ch.Employee_Id == id);
                        // var list = link.LineManagerList(id); //.Where(l=>l.ID == id);
                        VM_HRMS_LinkToEmployee linkToEmployee = new VM_HRMS_LinkToEmployee();
                        var listlink = linkToEmployee.GetSpecificEmployeeSuperiorList(id.Value);
                       // var list = link.GetSpecificLineManagerList(id); //.Where(l=>l.ID == id);
                        ViewBag.Superior = listlink;

                        return View(model);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Employee not found!";
                            return RedirectToAction("IndexEmployees", "EmployeeRecord");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("IndexEmployees", "EmployeeRecord");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("IndexEmployees", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        ///// <summary>
        ///// Print  Employee Details ---------------
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //public async Task<ActionResult> PrintDetailEmployeeIdWise(int? id)
        //{
        //    VM_HRMS_LinkToEmployee link = new VM_HRMS_LinkToEmployee();
        //   // if (Session["Role"] != null)
        //    {
        //        if (id != null)
        //        {
        //            try
        //            {
        //                VmEmployee model = new VmEmployee();
        //                await Task.Run(() => model = model.DetailsVmEmployeesIdWise(id.Value));
        //                ViewBag.Spouse = db.HrmsSpouses.ToList().Where(s => s.Employee_Id == id);
        //                ViewBag.Child = db.HrmsChildren.ToList().Where(ch => ch.Employee_Id == id);
        //                ViewBag.Education = db.HrmsEducations.ToList().Where(ch => ch.Employee_Id == id);
        //                ViewBag.Training = db.HrmsTrainings.ToList().Where(ch => ch.Employee_Id == id);
        //                ViewBag.Experience = db.HrmsExperiences.ToList().Where(ch => ch.Employee_Id == id);
        //                ViewBag.Skill = db.HrmsSkills.ToList().Where(ch => ch.Employee_Id == id);
        //                ViewBag.Reference = db.HrmsReferences.ToList().Where(ch => ch.Employee_Id == id);
        //                ViewBag.Hobby = db.HrmsHobbies.ToList().Where(ch => ch.Employee_Id == id);
        //                ViewBag.Salary = db.HrmsSalaries.ToList().Where(ch => ch.Employee_Id == id);
        //                ViewBag.Facility = db.HrmsFacilities.ToList().Where(ch => ch.Employee_Id == id);
        //                var list = link.LineManagerList(id); //.Where(l=>l.ID == id);
        //                ViewBag.Superior = list;

        //                return View(model);
        //            }
        //            catch (Exception ex)
        //            {
        //                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
        //                {
        //                    Session["warning_div"] = "true";
        //                    Session["warning_msg"] = "Employee not found!";
        //                    return RedirectToAction("IndexEmployees", "EmployeeInfo");
        //                }
        //                Session["warning_div"] = "true";
        //                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
        //                return RedirectToAction("IndexEmployees", "EmployeeInfo");
        //            }
        //        }
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Something Went Wrong!";
        //        return RedirectToAction("IndexEmployees", "EmployeeInfo");
        //    }
        //    //Session["warning_div"] = "true";
        //    //Session["warning_msg"] = "Please Login to continue..!";
        //    //return RedirectToAction("Login", "Account");
        //}

        #endregion


        #region Employee's Spouse Information


        VM_HRMS_Spouse vmSpouse;
        /// <summary>
        /// Spouse List View.... 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> IndexSpouse()
        {
            vmSpouse = new VM_HRMS_Spouse();
            await Task.Run(() => vmSpouse.GetSpouseList());
            return View(vmSpouse);
        }


        /// <summary>
        /// Load Form for Spouse Information.... 
        /// </summary>
        /// <returns></returns>
        public ActionResult AddSpouse(int? id)
        {
            //if (Session["Role"] != null)
            {
                ViewBag.Spouse = db.HrmsSpouses.ToList().Where(s => s.Employee_Id == id);
                var emp = db.Employees.FirstOrDefault(s => s.ID == id);
                VM_HRMS_Spouse model = new VM_HRMS_Spouse();
                model.Employee = emp;
                return View(model);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        /// <summary>
        /// Store Spouse Information to the Database 
        /// Database: CrimsonERP
        /// Table: HrmsSpouses
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddSpouse(VM_HRMS_Spouse model)
        {
            //if (Session["Role"] != null)
            {
                var marritalStatus = (db.Employees.Where(s => s.ID == model.Employee_Id).Select(s => s.Marital_Status)).Single();
                if (marritalStatus != "Single")
                {
                    try
                    {
                        HRMS_Spouse spouse = new HRMS_Spouse
                        {
                            Employee_Id = model.Employee_Id,
                            SpouseName = model.SpouseName,
                            SpouseDOB = model.SpouseDOB,
                            Spouse_Contact_No = model.Spouse_Contact_No,
                            Spouse_Blood_Group = model.Spouse_Blood_Group,
                            Status = model.Status,
                            Entry_By = "l",
                            Entry_Date = DateTime.Now
                        };
                        db.HrmsSpouses.Add(spouse);
                        await db.SaveChangesAsync();

                        Session["success_div"] = "true";
                        Session["success_msg"] = "Spouse Added Successfully.";
                        return RedirectToAction("AddSpouse", "EmployeeRecord", new { id = model.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Spouse info Can't Add, It is Already Exist!";
                            return RedirectToAction("AddSpouse", "EmployeeRecord", new { id = model.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("AddSpouse", "EmployeeRecord", new { id = model.Employee_Id });
                    }
                }
                Session["success_div"] = "true";
                Session["success_msg"] = "Spouse info Can't Add.Because, respective employee is single.";
                return RedirectToAction("AddSpouse", "EmployeeRecord", new { id = model.Employee_Id });
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public async Task<ActionResult> DeleteSpouse(int? id)
        {
            // if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Spouses ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Spouse spouse = await db.HrmsSpouses.FindAsync(id);
                if (spouse != null)
                {
                    try
                    {
                        db.HrmsSpouses.Remove(spouse);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Spouse info Deleted Successfully.";
                        return RedirectToAction("AddSpouse", "EmployeeRecord", new { id = spouse.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Spouse info Can't Delete, It is Already in Use!";
                            return RedirectToAction("AddSpouse", "EmployeeRecord", new { id = spouse.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("AddSpouse", "EmployeeRecord", new { id = spouse.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditSpouse(int? id)
        {
            // if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    var a = db.HrmsSpouses.FirstOrDefault(s => s.ID == id);
                    VM_HRMS_Spouse model = new VM_HRMS_Spouse
                    {
                        ID = a.ID,
                        Employee_Id = a.Employee_Id,
                        SpouseName = a.SpouseName,
                        Spouse_Blood_Group = a.Spouse_Blood_Group,
                        SpouseDOB = a.SpouseDOB,
                        Spouse_Contact_No = a.Spouse_Contact_No
                    };
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Spouse info Not Found!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Spouse Information.... 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditSpouse(VM_HRMS_Spouse model)
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Spouse spouse = new HRMS_Spouse
                    {
                        ID = model.ID,
                        SpouseName = model.SpouseName,
                        Employee_Id = model.Employee_Id,
                        Spouse_Contact_No = model.Spouse_Contact_No,
                        SpouseDOB = model.SpouseDOB,
                        Spouse_Blood_Group = model.Spouse_Blood_Group,
                        Update_By = model.Update_By,
                        Update_Date = DateTime.Now
                    };
                    db.Entry(spouse).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Spouse Updated Successfully.";
                    return RedirectToAction("AddSpouse", "EmployeeRecord", new { id = model.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddSpouse", "EmployeeRecord", new { id = model.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        #endregion


        #region Employee's Child(ren) Information.... 

        /// <summary>
        /// Load List of Children... 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> IndexChild()
        {
            //if (Session["Role"] != null)
            {
                VM_HRMS_Child children = new VM_HRMS_Child();
                await Task.Run(() => children.GetChildren());
                return View(children);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// this will load input form for Children Information...
        /// </summary>
        /// <returns></returns>
        public ActionResult AddChildren(int? id)
        {
            ViewBag.Child = db.HrmsChildren.ToList().Where(ch => ch.Employee_Id == id);
            var emp = db.Employees.FirstOrDefault(s => s.ID == id);
            VM_HRMS_Child model = new VM_HRMS_Child();
            model.Employee = emp;
            return View(model);
        }

        /// <summary>
        /// store child information 
        /// databae: CrimsonERP
        /// table: HrmsChildren
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddChildren(VM_HRMS_Child child)
        {
            //if (Session["Role"] != null)
            {
                var marritalStatus = (db.Employees.Where(s => s.ID == child.Employee_Id).Select(s => s.Marital_Status)).Single();
                if (marritalStatus != "Single")
                {
                    try
                    {
                        HRMS_Child children = new HRMS_Child()
                        {
                            Employee_Id = child.Employee_Id,
                            Child_Name = child.Child_Name,
                            Child_DOB = child.Child_DOB,
                            Child_Blood_Group = child.Child_Blood_Group,
                            Status = child.Status,
                            Entry_By = "l",
                            Entry_Date = DateTime.Now
                        };
                        db.HrmsChildren.Add(children);
                        await db.SaveChangesAsync();

                        Session["success_div"] = "true";
                        Session["success_msg"] = "Child Added Successfully.";
                        return RedirectToAction("AddChildren", "EmployeeRecord", new { id = child.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Child info Can't Add, It is Already Exist!";
                            return RedirectToAction("AddChildren", "EmployeeRecord", new { id = child.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("AddChildren", "EmployeeRecord", new { id = child.Employee_Id });
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Child info Can't Add. Because, respective Employee is single!";
                return RedirectToAction("AddChildren", "EmployeeRecord", new { id = child.Employee_Id });
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// delete child information------
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteChildren(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Country ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Child child = await db.HrmsChildren.FindAsync(id);
                if (child != null)
                {
                    try
                    {
                        db.HrmsChildren.Remove(child);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Children info Deleted Successfully.";
                        return RedirectToAction("AddChildren", "EmployeeRecord", new { id = child.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Children info Can't Delete, It is Already in Use!";
                            return RedirectToAction("AddChildren", "EmployeeRecord", new { id = child.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("AddChildren", "EmployeeRecord", new { id = child.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditChildren(int? id)
        {
            // if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    var a = db.HrmsChildren.FirstOrDefault(s => s.ID == id);
                    VM_HRMS_Child model = new VM_HRMS_Child
                    {
                        ID = a.ID,
                        Employee_Id = a.Employee_Id,
                        Child_Name = a.Child_Name,
                        Child_Blood_Group = a.Child_Blood_Group,
                        Child_DOB = a.Child_DOB
                    };
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Children info Not Found!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Child Information.... 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditChildren(VM_HRMS_Child model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Child child = new HRMS_Child
                    {
                        ID = model.ID,
                        Child_Name = model.Child_Name,
                        Employee_Id = model.Employee_Id,
                        Child_DOB = model.Child_DOB,
                        Child_Blood_Group = model.Child_Blood_Group,
                        Update_By = "2",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(child).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Child Updated Successfully.";
                    return RedirectToAction("AddChildren", "EmployeeRecord", new { id = model.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddChildren", "EmployeeRecord", new { id = model.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        #endregion


        #region Employee's Educational Information

        private VM_HRMS_EducationInfo EducationInfo;

        public async Task<ActionResult> IndexEducation()
        {
            // if (Session["Role"] != null)
            {
                EducationInfo = new VM_HRMS_EducationInfo();
                await Task.Run(() => EducationInfo.GetEducaionalInfos());
                return View(EducationInfo);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// this will load input form for Children Information...
        /// </summary>
        /// <returns></returns>
        public ActionResult AddEducation(int? id)
        {
            // if (Session["Role"] != null)
            {
                ViewBag.EducationList = new SelectList(d.GetEducationList(), "Value", "Text");
                ViewBag.Education = db.HrmsEducations.ToList().Where(ch => ch.Employee_Id == id);
                var emp = db.Employees.FirstOrDefault(s => s.ID == id);
                VM_HRMS_EducationInfo model = new VM_HRMS_EducationInfo();
                model.Employee = emp;
                return View(model);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// store child information 
        /// databae: CrimsonERP
        /// table: HrmsChildren
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddEducation(VM_HRMS_EducationInfo education)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Education edu = new HRMS_Education()
                    {
                        Employee_Id = education.Employee_Id,
                        Degree = education.Degree,
                        Institute = education.Institute,
                        GroupOrSubject = education.GroupOrSubject,
                        Result = education.Result,
                        Passing_Year = education.Passing_Year,
                        Status = education.Status,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsEducations.Add(edu);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Educaton info Added Successfully.";
                    return RedirectToAction("AddEducation", "EmployeeRecord", new { id = education.Employee_Id });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Education info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddEducation", "EmployeeRecord", new { id = education.Employee_Id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddEducation", "EmployeeRecord", new { id = education.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// delete Education information------
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteEducation(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Education ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Education education = await db.HrmsEducations.FindAsync(id);
                if (education != null)
                {
                    try
                    {
                        db.HrmsEducations.Remove(education);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Education info Deleted Successfully.";
                        return RedirectToAction("AddEducation", "EmployeeRecord", new { id = education.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Education info Can't Delete, It is Already in Use!";
                            return RedirectToAction("AddEducation", "EmployeeRecord", new { id = education.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("AddEducation", "EmployeeRecord", new { id = education.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditEduction(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.EducationList = new SelectList(d.GetEducationList(), "Value", "Text");
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    var a = db.HrmsEducations.FirstOrDefault(s => s.ID == id);
                    VM_HRMS_EducationInfo model = new VM_HRMS_EducationInfo
                    {
                        ID = a.ID,
                        Employee_Id = a.Employee_Id,
                        Degree = a.Degree,
                        Institute = a.Institute,
                        GroupOrSubject = a.GroupOrSubject,
                        Result = a.Result,
                        Passing_Year = a.Passing_Year
                    };
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Education info Not Found!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Child Information.... 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditEduction(VM_HRMS_EducationInfo model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Education education = new HRMS_Education
                    {
                        ID = model.ID,
                        Employee_Id = model.Employee_Id,
                        Institute = model.Institute,
                        Degree = model.Degree,
                        GroupOrSubject = model.GroupOrSubject,
                        Result = model.Result,
                        Passing_Year = model.Passing_Year,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(education).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Educatonal Info Updated Successfully.";
                    return RedirectToAction("AddEducation", "EmployeeRecord", new { id = model.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddEducation", "EmployeeRecord", new { id = model.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        #endregion


        #region Employee's Trainning Information

        private VM_HRMS_Training trainingInfo;

        public async Task<ActionResult> IndexTraining()
        {
            //if (Session["Role"] != null)
            {
                trainingInfo = new VM_HRMS_Training();
                await Task.Run(() => trainingInfo.GetTrainingInfoList());
                return View(trainingInfo);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// this will load input form for Training Information...
        /// </summary>
        /// <returns></returns>
        public ActionResult AddTraining(int? id)
        {
            // if (Session["Role"] != null)
            {
                ViewBag.Training = db.HrmsTrainings.ToList().Where(ch => ch.Employee_Id == id);
                var emp = db.Employees.FirstOrDefault(s => s.ID == id);
                VM_HRMS_Training model = new VM_HRMS_Training();
                model.Employee = emp;
                return View(model);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// store Training information 
        /// databae: CrimsonERP
        /// table: HrmsTrainings
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddTraining(VM_HRMS_Training training)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Training train = new HRMS_Training()
                    {
                        Employee_Id = training.Employee_Id,
                        Course_Name = training.Course_Name,
                        Institute = training.Institute,
                        Country = training.Country,
                        Course_Start_Date = training.Course_Start_Date,
                        Course_End_Date = training.Course_End_Date,
                        Course_Duration = training.Course_Duration,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsTrainings.Add(train);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Training info Added Successfully.";
                    return RedirectToAction("AddTraining", "EmployeeRecord", new { id = training.Employee_Id });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Training info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddTraining", "EmployeeRecord", new { id = training.Employee_Id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddTraining", "EmployeeRecord", new { id = training.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// delete child information------
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteTraining(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Training ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Training train = await db.HrmsTrainings.FindAsync(id);
                if (train != null)
                {
                    try
                    {
                        db.HrmsTrainings.Remove(train);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Training info Deleted Successfully.";
                        return RedirectToAction("IndexTraining", "EmployeeRecord", new { id = train.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Training info Can't Delete, It is Already in Use!";
                            return RedirectToAction("IndexTraining", "EmployeeRecord", new { id = train.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("IndexTraining", "EmployeeRecord", new { id = train.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditTraining(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    var train = db.HrmsTrainings.FirstOrDefault(s => s.ID == id);
                    VM_HRMS_Training training = new VM_HRMS_Training
                    {
                        ID = train.ID,
                        Employee_Id = train.Employee_Id,
                        Course_Name = train.Course_Name,
                        Institute = train.Institute,
                        Country = train.Country,
                        Course_Start_Date = train.Course_Start_Date,
                        Course_End_Date = train.Course_End_Date,
                        Course_Duration = train.Course_Duration
                    };
                    return View(training);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Training info Not Found!";
                return RedirectToAction("IndexTraining", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Child Information.... 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditTraining(VM_HRMS_Training training)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Training train = new HRMS_Training()
                    {
                        ID = training.ID,
                        Employee_Id = training.Employee_Id,
                        Course_Name = training.Course_Name,
                        Institute = training.Institute,
                        Country = training.Country,
                        Course_Start_Date = training.Course_Start_Date,
                        Course_End_Date = training.Course_End_Date,
                        Course_Duration = training.Course_Duration,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(train).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Training Info Updated Successfully.";
                    return RedirectToAction("AddTraining", "EmployeeRecord", new { id = training.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddTraining", "EmployeeRecord", new { id = training.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        #endregion


        #region Employee's Experience Information

        private VM_HRMS_Experience experience;

        public async Task<ActionResult> IndexExperience()
        {
            //if (Session["Role"] != null)
            {
                experience = new VM_HRMS_Experience();
                await Task.Run(() => experience.GetExperienceList());
                return View(experience);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// this will load input form for Training Information...
        /// </summary>
        /// <returns></returns>
        public ActionResult AddExperience(int? id)
        {
            ViewBag.Experience = db.HrmsExperiences.ToList().Where(ch => ch.Employee_Id == id);
            var emp = db.Employees.FirstOrDefault(s => s.ID == id);
            VM_HRMS_Experience model = new VM_HRMS_Experience();
            model.Employee = emp;
            return View(model);
        }

        /// <summary>
        /// store Training information 
        /// databae: CrimsonERP
        /// table: HrmsTrainings
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddExperience(VM_HRMS_Experience experience)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Experience exp = new HRMS_Experience()
                    {
                        Employee_Id = experience.Employee_Id,
                        Company_Name = experience.Company_Name,
                        Designation = experience.Designation,
                        Department = experience.Department,
                        Responsibility = experience.Responsibility,
                        Job_Start_Date = experience.Job_Start_Date,
                        Job_End_Date = experience.Job_End_Date,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsExperiences.Add(exp);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Experience info Added Successfully.";
                    return RedirectToAction("AddExperience", "EmployeeRecord", new { id = experience.Employee_Id });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Experience info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddExperience", "EmployeeRecord", new { id = experience.Employee_Id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddExperience", "EmployeeRecord", new { id = experience.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// delete child information------
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteExperience(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Experience ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Experience exp = await db.HrmsExperiences.FindAsync(id);
                if (exp != null)
                {
                    try
                    {
                        db.HrmsExperiences.Remove(exp);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Experience info Deleted Successfully.";
                        return RedirectToAction("IndexExperience", "EmployeeRecord", new { id = exp.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Experience info Can't Delete, It is Already in Use!";
                            return RedirectToAction("IndexExperience", "EmployeeRecord", new { id = exp.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("IndexExperience", "EmployeeRecord", new { id = exp.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Edit Experience Information 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditExperience(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    var experience = db.HrmsExperiences.FirstOrDefault(s => s.ID == id);
                    VM_HRMS_Experience exp = new VM_HRMS_Experience
                    {
                        ID = experience.ID,
                        Employee_Id = experience.Employee_Id,
                        Company_Name = experience.Company_Name,
                        Designation = experience.Designation,
                        Department = experience.Department,
                        Responsibility = experience.Responsibility,
                        Job_Start_Date = experience.Job_Start_Date,
                        Job_End_Date = experience.Job_End_Date,

                    };
                    return View(exp);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Experience info Not Found!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Child Information.... 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditExperience(VM_HRMS_Experience experience)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Experience exp = new HRMS_Experience()
                    {
                        ID = experience.ID,
                        Employee_Id = experience.Employee_Id,
                        Company_Name = experience.Company_Name,
                        Designation = experience.Designation,
                        Department = experience.Department,
                        Responsibility = experience.Responsibility,
                        Job_Start_Date = experience.Job_Start_Date,
                        Job_End_Date = experience.Job_End_Date,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(exp).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Experience Info Updated Successfully.";
                    return RedirectToAction("AddExperience", "EmployeeRecord", new { id = experience.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddExperience", "EmployeeRecord", new { id = experience.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        #endregion


        #region Employee's Skill Information

        private VM_HRMS_Skill skills;

        public async Task<ActionResult> IndexSkill()
        {
            //if (Session["Role"] != null)
            {
                skills = new VM_HRMS_Skill();
                await Task.Run(() => skills.GetSkillList());
                return View(skills);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// this will load input form for Training Information...
        /// </summary>
        /// <returns></returns>
        public ActionResult AddSkill(int? id)
        {
            ViewBag.Skill = db.HrmsSkills.ToList().Where(ch => ch.Employee_Id == id);
            var emp = db.Employees.FirstOrDefault(s => s.ID == id);
            VM_HRMS_Skill model = new VM_HRMS_Skill();
            model.Employee = emp;
            return View(model);
        }

        /// <summary>
        /// store Skill information 
        /// databae: CrimsonERP
        /// table: HrmsSkills
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddSkill(VM_HRMS_Skill skill)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Skill skillInfo = new HRMS_Skill()
                    {
                        Employee_Id = skill.Employee_Id,
                        Skill_Type = skill.Skill_Type,
                        Skill_Name = skill.Skill_Name,
                        Skill_Description = skill.Skill_Description,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsSkills.Add(skillInfo);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Skill info Added Successfully.";
                    return RedirectToAction("AddSkill", "EmployeeRecord", new { id = skill.Employee_Id });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Skill info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddSkill", "EmployeeRecord", new { id = skill.Employee_Id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddSkill", "EmployeeRecord", new { id = skill.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// delete Skill information------
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteSkill(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Experience ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Skill skill = await db.HrmsSkills.FindAsync(id);
                if (skill != null)
                {
                    try
                    {
                        db.HrmsSkills.Remove(skill);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Skill info Deleted Successfully.";
                        return RedirectToAction("IndexSkill", "EmployeeRecord", new { id = skill.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Skill info Can't Delete, It is Already in Use!";
                            return RedirectToAction("IndexSkill", "EmployeeRecord", new { id = skill.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("IndexSkill", "EmployeeRecord", new { id = skill.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Load Edit form with Desired data to update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditSkill(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    var skills = db.HrmsSkills.FirstOrDefault(s => s.ID == id);
                    VM_HRMS_Skill skillEdit = new VM_HRMS_Skill
                    {
                        ID = skills.ID,
                        Employee_Id = skills.Employee_Id,
                        Skill_Type = skills.Skill_Type,
                        Skill_Name = skills.Skill_Name,
                        Skill_Description = skills.Skill_Description
                    };
                    return View(skillEdit);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Skill info Not Found!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Skill Information.... 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditSkill(VM_HRMS_Skill skill)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Skill skillEdit = new HRMS_Skill()
                    {
                        ID = skill.ID,
                        Employee_Id = skill.Employee_Id,
                        Skill_Type = skill.Skill_Type,
                        Skill_Name = skill.Skill_Name,
                        Skill_Description = skill.Skill_Description,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(skillEdit).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Skill Info Updated Successfully.";
                    return RedirectToAction("AddSkill", "EmployeeRecord", new { id = skill.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddSkill", "EmployeeRecord", new { id = skill.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        #endregion


        #region Employee's Reference Information

        private VM_HRMS_Reference reference;
        public async Task<ActionResult> IndexReference()
        {
            //if (Session["Role"] != null)
            {
                reference = new VM_HRMS_Reference();
                await Task.Run(() => reference.GetReferenceList());
                return View(reference);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// this will load input form for Training Information...
        /// </summary>
        /// <returns></returns>
        public ActionResult AddReference(int? id)
        {
            ViewBag.Reference = db.HrmsReferences.ToList().Where(ch => ch.Employee_Id == id);
            var emp = db.Employees.FirstOrDefault(s => s.ID == id);
            VM_HRMS_Reference model = new VM_HRMS_Reference();
            model.Employee = emp;
            return View(model);
        }

        /// <summary>
        /// store Skill information 
        /// databae: CrimsonERP
        /// table: HrmsSkills
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddReference(VM_HRMS_Reference reference)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Reference refe = new HRMS_Reference()
                    {
                        Employee_Id = reference.Employee_Id,
                        Referee_Name = reference.Referee_Name,
                        Contact_No = reference.Contact_No,
                        Email_Id = reference.Email_Id,
                        Address = reference.Address,
                        Relation = reference.Relation,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsReferences.Add(refe);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Reference info Added Successfully.";
                    return RedirectToAction("AddReference", "EmployeeRecord", new { id = reference.Employee_Id });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Experience info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddReference", "EmployeeRecord", new { id = reference.Employee_Id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddReference", "EmployeeRecord", new { id = reference.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// delete Referrence information------
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteReference(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Experience ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Reference referee = await db.HrmsReferences.FindAsync(id);
                if (referee != null)
                {
                    try
                    {
                        db.HrmsReferences.Remove(referee);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Reference info Deleted Successfully.";
                        return RedirectToAction("IndexReference", "EmployeeRecord", new { id = referee.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Reference info Can't Delete, It is Already in Use!";
                            return RedirectToAction("IndexReference", "EmployeeRecord", new { id = referee.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("IndexReference", "EmployeeRecord", new { id = referee.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditReference(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    var reference = db.HrmsReferences.FirstOrDefault(s => s.ID == id);
                    VM_HRMS_Reference refer = new VM_HRMS_Reference
                    {
                        ID = reference.ID,
                        Employee_Id = reference.Employee_Id,
                        Referee_Name = reference.Referee_Name,
                        Contact_No = reference.Contact_No,
                        Email_Id = reference.Email_Id,
                        Address = reference.Address,
                        Relation = reference.Relation
                    };
                    return View(refer);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Reference info Not Found!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Skill Information.... 
        /// database: CrimsonERP
        /// table: HRMS_Skill
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditReference(VM_HRMS_Reference vmReference)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Reference reference = new HRMS_Reference()
                    {
                        ID = vmReference.ID,
                        Employee_Id = vmReference.Employee_Id,
                        Referee_Name = vmReference.Referee_Name,
                        Contact_No = vmReference.Contact_No,
                        Email_Id = vmReference.Email_Id,
                        Address = vmReference.Address,
                        Relation = vmReference.Relation,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(reference).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Reference Info Updated Successfully.";
                    return RedirectToAction("AddReference", "EmployeeRecord", new { id = vmReference.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddReference", "EmployeeRecord", new { id = vmReference.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        #endregion


        #region Employee's Hobby Information

        private VM_HRMS_Hobby hobby;
        public async Task<ActionResult> IndexHobby()
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    hobby = new VM_HRMS_Hobby();
                    await Task.Run(() => hobby.getHobbyList());
                    return View(hobby);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("IndexHobby", "EmployeeRecord");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// this will load input form for Training Information...
        /// </summary>
        /// <returns></returns>
        public ActionResult AddHobby(int? id)
        {
            ViewBag.Hobby = db.HrmsHobbies.ToList().Where(ch => ch.Employee_Id == id);
            var emp = db.Employees.FirstOrDefault(s => s.ID == id);
            VM_HRMS_Hobby model = new VM_HRMS_Hobby();
            model.Employee = emp;
            return View(model);
        }

        /// <summary>
        /// store Skill information 
        /// databae: CrimsonERP
        /// table: HrmsSkills
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddHobby(VM_HRMS_Hobby hobbies)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Hobby hobby = new HRMS_Hobby()
                    {
                        Employee_Id = hobbies.Employee_Id,
                        Hobby_Name = hobbies.Hobby_Name,
                        Remarks = hobbies.Remarks,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsHobbies.Add(hobby);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Hobby Added Successfully.";
                    return RedirectToAction("AddHobby", "EmployeeRecord", new { id = hobbies.Employee_Id });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Hobby Can't Add, It is Already Exist!";
                        return RedirectToAction("AddHobby", "EmployeeRecord", new { id = hobbies.Employee_Id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddHobby", "EmployeeRecord", new { id = hobbies.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// delete Skill information------
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteHobby(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Hobby ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Hobby hobby = await db.HrmsHobbies.FindAsync(id);
                if (hobby != null)
                {
                    try
                    {
                        db.HrmsHobbies.Remove(hobby);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Hobby info Deleted Successfully.";
                        return RedirectToAction("AddHobby", "EmployeeRecord", new { id = hobby.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Hobby info Can't Delete, It is Already in Use!";
                            return RedirectToAction("AddHobby", "EmployeeRecord", new { id = hobby.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("AddHobby", "EmployeeRecord", new { id = hobby.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Load from for Edit Hobby Information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditHobby(int? id)
        {
            // if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    var hobby = db.HrmsHobbies.FirstOrDefault(s => s.ID == id);
                    VM_HRMS_Hobby hobbyEdit = new VM_HRMS_Hobby
                    {
                        ID = hobby.ID,
                        Employee_Id = hobby.Employee_Id,
                        Hobby_Name = hobby.Hobby_Name,
                        Remarks = hobby.Remarks
                    };
                    return View(hobbyEdit);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Hobby info Not Found!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Skill Information.... 
        /// database: CrimsonERP
        /// table: HRMS_Skill
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditHobby(VM_HRMS_Hobby hobby)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Hobby hobbies = new HRMS_Hobby()
                    {
                        ID = hobby.ID,
                        Employee_Id = hobby.Employee_Id,
                        Hobby_Name = hobby.Hobby_Name,
                        Remarks = hobby.Remarks,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(hobbies).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Hobby Info Updated Successfully.";
                    return RedirectToAction("AddHobby", "EmployeeRecord", new { id = hobby.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddHobby", "EmployeeRecord", new { id = hobby.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        #endregion


        #region Employee's Salary Information

        private VM_HRMS_Salary salary;

        /// <summary>
        /// Collective Viewof Salary Information of Employee
        /// database: CrimsonERP
        /// table: HRMS_Salary
        /// Developed:: MD. Anisur Rahman & F. Rabbi
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> IndexSalary()
        {
            //if (Session["Role"] != null)
            {
                salary = new VM_HRMS_Salary();
                await Task.Run(() => salary.GetSalaryList());
                return View(salary);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// this will load input form for Training Information...
        /// database: CrimsonERP
        /// table: HRMS_Salary
        /// Developed:: MD. Anisur Rahman &amp; F. Rabbi
        /// </summary>
        /// <returns></returns>
        public ActionResult AddSalary(int? id)
        {
            {
                try
                {
                    var a = (from t1 in db.HrmsSalaries
                             join t2 in db.SalaryGrade on t1.Salary_Grade equals t2.ID.ToString()
                             where t1.Employee_Id == id
                             select new HrmsPayrollSalaryWithGrade
                             {
                                 HRMS_Salary = t1,
                                 SalaryGrade = t2
                             }).ToList();
                    ViewBag.Salary = a;
                    var b = (from t1 in db.HrmsEodRecords
                             join t2 in db.HrmsEodReferences on t1.Eod_RefFk equals t2.ID
                             where t1.EmployeeId == id
                             select new HrmsPayrollSalaryWithEod
                             {
                                 HRMS_EodRecord = t1,
                                 HRMS_EodReference = t2
                             }).ToList();
                    ViewBag.EodRecord = b;

                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    ViewBag.SalaryGrade = new SelectList(d.GetSalaryGradeList(), "Value", "Text");

                    VMSalary = new VM_HRMS_Salary();
                    VMSalary.Employee_Id = id ?? 0;
                    VMSalary.GetEmployeeEODRecord(id ?? 0);

                    var emp = db.Employees.FirstOrDefault(s => s.ID == id);
                    VMSalary.Employee = emp;


                    var salhis = db.HrmsSalaryHistories.Where(s => s.Employee_Id == id).ToList();
                    ViewBag.SalaryHistory = salhis;


                    var hrmssalary = db.HrmsSalaries.FirstOrDefault(s => s.Employee_Id == id);

                    if (hrmssalary != null)
                    {
                        VMSalary.ID = hrmssalary.ID;
                        VMSalary.Salary_Grade = hrmssalary.Salary_Grade;
                        VMSalary.Bank_Name = hrmssalary.Bank_Name;
                        VMSalary.Payment_Mode = hrmssalary.Payment_Mode;
                        VMSalary.Account_No = hrmssalary.Account_No;
                        VMSalary.GetEmployeeEODRecord(hrmssalary.Employee_Id);
                    }

                    return View(VMSalary);
                }
                catch (Exception ex)
                {
                    if (ex.Message ==
                        "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Salary Info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddSalary", "EmployeeRecord", new { id = id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddSalary", "EmployeeRecord", new { id = id });

                }
            }
        }

        /// <summary>
        /// store salary information 
        /// database: CrimsonERP
        /// table: HRMS_Salary
        /// Developed:: MD. Anisur Rahman &amp; F. Rabbi
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddSalary(VM_HRMS_Salary vmSalary)
        {

            // if (Session["Role"] != null)
            {
                try
                {
                    var a = (from t1 in db.HrmsSalaries
                             join t2 in db.SalaryGrade on t1.Salary_Grade equals t2.ID.ToString()
                             where t1.Employee_Id == vmSalary.Employee_Id
                             select new HrmsPayrollSalaryWithGrade
                             {
                                 HRMS_Salary = t1,
                                 SalaryGrade = t2
                             }).ToList();
                    ViewBag.Salary = a;
                    var b = (from t1 in db.HrmsEodRecords
                             join t2 in db.HrmsEodReferences on t1.Eod_RefFk equals t2.ID
                             where t1.EmployeeId == vmSalary.Employee_Id
                             select new HrmsPayrollSalaryWithEod
                             {
                                 HRMS_EodRecord = t1,
                                 HRMS_EodReference = t2
                             }).ToList();
                    ViewBag.EodRecord = b;

                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    ViewBag.SalaryGrade = new SelectList(d.GetSalaryGradeList(), "Value", "Text");

                    VMSalary = new VM_HRMS_Salary();
                    VMSalary.Employee_Id = vmSalary.Employee_Id;
                    await Task.Run(() => VMSalary.GetEmployeeEODRecord(vmSalary.Employee_Id));

                    var emp = db.Employees.FirstOrDefault(s => s.ID == vmSalary.Employee_Id);
                    VMSalary.Employee = emp;

                    return View(VMSalary);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Salary Info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddSalary", "EmployeeRecord", new { id = vmSalary.Employee_Id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddSalary", "EmployeeRecord", new { id = vmSalary.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveSalary(VM_HRMS_Salary vmSalary)
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");

                    var hrmsldata = db.HrmsSalaries.FirstOrDefault(x => x.Employee_Id == vmSalary.Employee_Id);
                    var eoddata = db.HrmsEodRecords.Where(x => x.EmployeeId == vmSalary.Employee_Id).ToList();
                    if (hrmsldata != null)
                    {
                        var salaryhistory = new HRMS_Salary_History
                        {
                            Employee_Id = hrmsldata.Employee_Id,
                            HrmsSalaries = new JavaScriptSerializer().Serialize(hrmsldata),
                            EodRecords = new JavaScriptSerializer().Serialize(eoddata)
                        };
                        db.HrmsSalaryHistories.Add(salaryhistory);

                        hrmsldata.Salary_Grade = vmSalary.Salary_Grade;
                        hrmsldata.Payment_Mode = vmSalary.Payment_Mode;
                        hrmsldata.Bank_Name = vmSalary.Bank_Name;
                        hrmsldata.Account_No = vmSalary.Account_No;
                        hrmsldata.Update_By = "l";
                        hrmsldata.Update_Date = DateTime.Now;
                        db.Entry(hrmsldata).State = EntityState.Modified;
                    }
                    else
                    {
                        HRMS_Salary salary = new HRMS_Salary()
                        {
                            Employee_Id = vmSalary.Employee_Id,
                            Salary_Grade = vmSalary.Salary_Grade,
                            Joining_Salary = 0,
                            Payment_Mode = vmSalary.Payment_Mode,
                            Bank_Name = vmSalary.Bank_Name,
                            Account_No = vmSalary.Account_No,
                            Current_Salary = 0,
                            Entry_By = "l",
                            Entry_Date = DateTime.Now
                        };
                        db.HrmsSalaries.Add(salary);
                    }


                    decimal basicPay = 0;
                    decimal basicRate = 0;
                    int OverTimeRate = 0;
                    if (vmSalary.EODRecordList.Any())
                    {
                        foreach (var v in vmSalary.EODRecordList)
                        {
                            if (v.EODRefFK == 1)
                            {
                                basicPay = v.Amount;
                                basicRate = (basicPay / 26) / 8;
                                OverTimeRate = (int)basicRate * 2;
                            }

                            var update = db.HrmsEodRecords.Where(a => a.ID == v.EODRecordID).FirstOrDefault();
                            if (v.EODRefFK == 11)
                            {
                                update.ActualAmount = OverTimeRate;
                            }
                            else
                            {
                                update.ActualAmount = v.Amount;
                            }
                        }
                    }
                    // Reference SalaryTotal id=30
                    // var tupdate = db.HrmsEodRecords.Where(a => a.Eod_RefFk == 30 && a.EmployeeId == vmSalary.Employee_Id);
                    //tupdate.FirstOrDefault().ActualAmount = totalSalary;

                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Salary Info Added Successfully.";
                    return RedirectToAction("AddSalary", "EmployeeRecord", new { id = vmSalary.Employee_Id });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Salary Info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddSalary", "EmployeeRecord", new { id = vmSalary.Employee_Id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddSalary", "EmployeeRecord", new { id = vmSalary.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        /// <summary>
        /// delete Salary information------
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteSalary(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Salary ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Salary salary = await db.HrmsSalaries.FindAsync(id);
                if (salary != null)
                {
                    try
                    {
                        db.HrmsSalaries.Remove(salary);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Salary info Deleted Successfully.";
                        return RedirectToAction("AddSalary", "EmployeeRecord", new { id = salary.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Salary info Can't Delete, It is Already in Use!";
                            return RedirectToAction("AddSalary", "EmployeeRecord", new { id = salary.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("AddSalary", "EmployeeRecord", new { id = salary.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Load form for Edit Salary Information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditSalary(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    ViewBag.SalaryGrade = new SelectList(d.GetSalaryGradeList(), "Value", "Text");
                    var salary = db.HrmsSalaries.FirstOrDefault(s => s.ID == id);


                    VMSalary = new VM_HRMS_Salary();
                    VMSalary.ID = salary.ID;
                    VMSalary.Employee_Id = salary.Employee_Id;
                    VMSalary.Salary_Grade = salary.Salary_Grade;
                    VMSalary.Joining_Salary = salary.Joining_Salary;
                    VMSalary.Current_Salary = salary.Current_Salary;
                    VMSalary.Bank_Name = salary.Bank_Name;
                    VMSalary.Payment_Mode = salary.Payment_Mode;
                    VMSalary.Account_No = salary.Account_No;
                    await Task.Run(() => VMSalary.GetEmployeeEODRecord(salary.Employee_Id));

                    //VM_HRMS_Salary salaryEdit = new VM_HRMS_Salary
                    //{

                    //    ID = salary.ID,
                    //    Employee_Id = salary.Employee_Id,
                    //    Salary_Grade = salary.Salary_Grade,
                    //    Payment_Mode = salary.Payment_Mode,
                    //    Joining_Date = salary.Joining_Date,
                    //    Joining_Salary = salary.Joining_Salary,
                    //    Bank_Name = salary.Bank_Name,
                    //    Account_No = salary.Account_No,
                    //    Current_Salary = salary.Current_Salary,

                    //};
                    return View(VMSalary);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Salary info Not Found!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Salary Information.... 
        /// database: CrimsonERP
        /// table: HRMS_Salary
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditSalary(VM_HRMS_Salary salary)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    HRMS_Salary editSalary = new HRMS_Salary()
                    {
                        ID = salary.ID,
                        Employee_Id = salary.Employee_Id,
                        Salary_Grade = salary.Salary_Grade,
                        Payment_Mode = salary.Payment_Mode,
                        Bank_Name = salary.Bank_Name,
                        Account_No = salary.Account_No,
                        Joining_Salary = salary.Joining_Salary,
                        Current_Salary = salary.Current_Salary,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(editSalary).State = EntityState.Modified;
                    db.SaveChanges();
                    decimal totalSalary = decimal.Zero;

                    var all = (from o in db.HrmsEodRecords
                               join p in db.HrmsEodReferences on o.Eod_RefFk equals p.ID
                               where o.EmployeeId == salary.Employee_Id
                               select new
                               {
                                   o,
                                   p
                               }).OrderBy(a => a.p.priority).ToList();

                    var regularEOD = all.Where(a => a.p.Finish == 12);

                    if (salary.EODRecordList.Any())
                    {
                        totalSalary = decimal.Zero;
                        foreach (var v in salary.EODRecordList)
                        {

                            var checkAmount = all.FirstOrDefault(a => a.o.ID == v.EODRecordID);
                            if (all.Any(a => a.o.ID == v.EODRecordID))
                            {
                                totalSalary += v.Amount;
                                if (checkAmount.o.ActualAmount != v.Amount)
                                {

                                    var update = db.HrmsEodRecords.FirstOrDefault(a => a.ID == checkAmount.o.ID);
                                    update.Status = false;
                                    HRMS_EodRecord record = new HRMS_EodRecord()
                                    {
                                        EmployeeId = salary.Employee_Id,
                                        Eod_RefFk = checkAmount.o.Eod_RefFk,
                                        ActualAmount = v.Amount,
                                        Status = true,
                                        EffectiveDate = DateTime.Now,
                                        Update_By = "1",
                                        Entry_Date = DateTime.Now,
                                        Entry_By = "1"
                                    };
                                    db.HrmsEodRecords.Add(record);
                                }
                            }
                        }
                    }

                    ////   Reference SalaryTotal id = 30
                    //var tupdate = db.HrmsEodRecords.Where(a => a.Eod_RefFk == 30 && a.EmployeeId == salary.Employee_Id);
                    //tupdate.FirstOrDefault().ActualAmount = totalSalary;

                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Salary Info Updated Successfully.";
                    return RedirectToAction("AddSalary", "EmployeeRecord", new { id = salary.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddSalary", "EmployeeRecord", new { id = salary.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        #endregion


        #region Employee's Facility Information

        private VM_HRMS_Facility facility;

        /// <summary>
        /// Collective View of Facilities
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> IndexFacility()
        {
            //if (Session["Role"] != null)
            {
                facility = new VM_HRMS_Facility();
                await Task.Run(() => facility.GetFacilityList());
                return View(facility);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// this will load input form for Facility Information...
        /// </summary>
        /// <returns></returns>
        public ActionResult AddFacility(int? id)
        {
            ViewBag.Facility = db.HrmsFacilities.ToList().Where(ch => ch.Employee_Id == id);
            VM_HRMS_Facility model = new VM_HRMS_Facility();
            var emp = db.Employees.FirstOrDefault(s => s.ID == id);
            model.Employee = emp;
            return View(model);
        }

        /// <summary>
        /// store facility information 
        /// databae: CrimsonERP
        /// table: Hrms_Facility
        /// Developed: Md. Anisur Rahman & F. Rabbi
        /// </summary>
        /// <param name="vmFacility"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddFacility(VM_HRMS_Facility vmFacility)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Facility facility = new HRMS_Facility()
                    {
                        Employee_Id = vmFacility.Employee_Id,
                        Item_Name = vmFacility.Item_Name,
                        Cost_Per_Month = vmFacility.Cost_Per_Month,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsFacilities.Add(facility);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Facility Info Added Successfully.";
                    return RedirectToAction("AddFacility", "EmployeeRecord", new { id = vmFacility.Employee_Id });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Facility Info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddFacility", "EmployeeRecord", new { id = vmFacility.Employee_Id });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddFacility", "EmployeeRecord", new { id = vmFacility.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// delete specific Facility information------
        /// databae: CrimsonERP
        /// table: Hrms_Facility
        /// Developed: Md. Anisur Rahman & F. Rabbi
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteFacility(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Facility ID Not Found!";
                    return RedirectToAction("EmployeeInformation", "EmployeeRecord");
                }
                HRMS_Facility facility = await db.HrmsFacilities.FindAsync(id);
                if (facility != null)
                {
                    try
                    {
                        db.HrmsFacilities.Remove(facility);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Facility info Deleted Successfully.";
                        return RedirectToAction("AddFacility", "EmployeeRecord", new { id = facility.Employee_Id });
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Facility info Can't Delete, It is Already in Use!";
                            return RedirectToAction("AddFacility", "EmployeeRecord", new { id = facility.Employee_Id });
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("AddFacility", "EmployeeRecord", new { id = facility.Employee_Id });
                    }
                    //return HttpNotFound();
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Load form for Edit Facility Information
        /// databae: CrimsonERP
        /// table: Hrms_Facility
        /// Developed: Md. Anisur Rahman & F. Rabbi
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditFacility(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    ViewBag.Employee = new SelectList(d.GetEmployeeList(), "Value", "Text");
                    var facility = db.HrmsFacilities.FirstOrDefault(s => s.ID == id);
                    VM_HRMS_Facility facilityEdit = new VM_HRMS_Facility
                    {
                        ID = facility.ID,
                        Employee_Id = facility.Employee_Id,
                        Item_Name = facility.Item_Name,
                        Cost_Per_Month = facility.Cost_Per_Month
                    };
                    return View(facilityEdit);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Facility info Not Found!";
                return RedirectToAction("EmployeeInformation", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Salary Information.... 
        /// databae: CrimsonERP
        /// table: Hrms_Facility
        /// Developed: Md. Anisur Rahman & F. Rabbi
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditFacility(VM_HRMS_Facility facility)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    HRMS_Facility editFacility = new HRMS_Facility()
                    {
                        ID = facility.ID,
                        Employee_Id = facility.Employee_Id,
                        Item_Name = facility.Item_Name,
                        Cost_Per_Month = facility.Cost_Per_Month,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(editFacility).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Facility Info Updated Successfully.";
                    return RedirectToAction("AddFacility", "EmployeeRecord", new { id = facility.Employee_Id });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddFacility", "EmployeeRecord", new { id = facility.Employee_Id });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }
        #endregion


        #region Linking To Employee....

        // private VM_HRMS_LinkToEmployee linkEmployee;

        public async Task<ActionResult> IndexLinkToEmployee()
        {
            //if (Session["Role"] != null)
            {
              var  linkEmployee = new VM_HRMS_LinkToEmployee();
                 await Task.Run(() => linkEmployee.GetLineManagerList1());
                //  await Task.Run(() => linkEmployee.GetLineManagerList());
                return View(linkEmployee);

            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Load Input Form for Selecting Employees
        /// </summary>
        /// <returns></returns>
        public ActionResult AddLinkToEmployee(int? id)
        {
            ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");


            VM_HRMS_LinkToEmployee model = new VM_HRMS_LinkToEmployee();
           // model.GetLineManagerList1(id ?? 0);
            model.GetSpecificEmployeeSuperiorList(id);
            ViewBag.Superior = model.GetSpecificEmployeeSuperiorList(id);
            //ViewBag.Superior = model.LinkToEmployees;
            var emp = db.Employees.FirstOrDefault(s => s.ID == id);
           // model.Employee.ID =Convert.ToInt32(id);
            model.Employee = emp;
            return View(model);
        }

        /// <summary>
        /// Store Information of Linked Employee
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddLinkToEmployee(VM_HRMS_LinkToEmployee linkToEmployee)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    
                    HRMS_LinkSuperior linkLM = new HRMS_LinkSuperior()
                    {
                        EmployeeId = linkToEmployee.ID,
                        SuperiorId = linkToEmployee.LineManagerId1,
                        Type = 1,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsLinkSuperior.Add(linkLM);

                    HRMS_LinkSuperior linkAdmin = new HRMS_LinkSuperior()
                    {
                        EmployeeId = linkToEmployee.ID,
                        SuperiorId = linkToEmployee.LineManagerId2,
                        Type = 2,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsLinkSuperior.Add(linkAdmin);

                    HRMS_LinkSuperior linkDeptHead = new HRMS_LinkSuperior()
                    {
                        EmployeeId = linkToEmployee.ID,
                        SuperiorId = linkToEmployee.LineManagerId3,
                        Type = 3,
                        Entry_By = "l",
                        Entry_Date = DateTime.Now
                    };
                    db.HrmsLinkSuperior.Add(linkDeptHead);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Linking Info Added Successfully.";
                    return RedirectToAction("IndexEmployees", "EmployeeRecord", new { id = linkToEmployee.EmployeeId });
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Facility Info Can't Add, It is Already Exist!";
                        return RedirectToAction("AddLinkToEmployee", "EmployeeRecord", new { id = linkToEmployee.EmployeeId });
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("AddLinkToEmployee", "EmployeeRecord", new { id = linkToEmployee.EmployeeId });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        /// <summary>
        /// Store Information of Linked Employee
        /// </summary>
        /// <returns></returns>
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> AddLinkToEmployee(VM_HRMS_LinkToEmployee linkToEmployee)
        //{
        //    try
        //    {
        //        linkToEmployee.HrmsLinkSuperior.Entry_By = "l";
        //        linkToEmployee.HrmsLinkSuperior.Entry_Date = DateTime.Now;
        //        db.HrmsLinkSuperior.Add(linkToEmployee.HrmsLinkSuperior);

        //        await db.SaveChangesAsync();

        //        Session["success_div"] = "true";
        //        Session["success_msg"] = "Linking Info Added Successfully.";
        //        return RedirectToAction("AddLinkToEmployee", "EmployeeRecord");
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
        //        {
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Facility Info Can't Add, It is Already Exist!";
        //            return RedirectToAction("AddLinkToEmployee", "EmployeeRecord");
        //        }
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
        //        return RedirectToAction("AddLinkToEmployee", "EmployeeRecord");
        //    }
        //}


        /// <summary>
        /// Load form for Edit Facility Information
        /// databae: CrimsonERP
        /// table: Hrms_Facility
        /// Developed: Md. Anisur Rahman & F. Rabbi
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditLinkToEmployee(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    VM_HRMS_LinkToEmployee model = new VM_HRMS_LinkToEmployee();
                    ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
                    if (true)
                    {

                        await Task.Run(() => model = model.GetSpecificLineManagerList(id.Value));
                        return View(model);
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Link Info Not Found!";
                return RedirectToAction("AddLinkToEmployee", "EmployeeRecord", new { id =id });
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Update Salary Information.... 
        /// databae: CrimsonERP
        /// table: Hrms_Facility
        /// Developed: Md. Anisur Rahman & F. Rabbi
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditLinkToEmployee(VM_HRMS_LinkToEmployee linkTo)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
                    HRMS_LinkSuperior lm = new HRMS_LinkSuperior()
                    {
                        ID = linkTo.ID,
                        EmployeeId = linkTo.EmployeeId,
                        SuperiorId = linkTo.SuperiorId,
                        Type = linkTo.Type,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(lm).State = EntityState.Modified;
                    db.SaveChanges();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Link To Employee Info Updated Successfully.";
                    return RedirectToAction("AddLinkToEmployee", "EmployeeRecord", new { id = linkTo.EmployeeId });
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("AddLinkToEmployee", "EmployeeRecord", new { id = linkTo.EmployeeId });
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        ///// <summary>
        ///// Load form for Edit Facility Information
        ///// databae: CrimsonERP
        ///// table: Hrms_Facility
        ///// Developed: Md. Anisur Rahman & F. Rabbi
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //public async Task<ActionResult> EditLinkToEmployee(int? id)
        //{
        //    //if (Session["Role"] != null)
        //    {
        //        if (id != null)
        //        {
        //            VM_HRMS_LinkToEmployee model = new VM_HRMS_LinkToEmployee();
        //            ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
        //            if (true)
        //            {

        //                await Task.Run(() => model = model.GetSpecificLineManagerList(id.Value));
        //                model.EmployeeId = model.HrmsLinkSuperior.EmployeeId;
        //                return View(model);
        //            }
        //        }
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Salary info Not Found!";
        //        return RedirectToAction("AddLinkToEmployee", "EmployeeRecord",new { id = id });
        //    }
        //    //Session["warning_div"] = "true";
        //    //Session["warning_msg"] = "Please Login to continue..!";
        //    //return RedirectToAction("Login", "Account");
        //}

        ///// <summary>
        ///// Update Salary Information.... 
        ///// databae: CrimsonERP
        ///// table: Hrms_Facility
        ///// Developed: Md. Anisur Rahman & F. Rabbi
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> EditLinkToEmployee(VM_HRMS_LinkToEmployee linkTo)
        //{
        //    //if (Session["Role"] != null)
        //    {
        //        try
        //        {
        //            ViewBag.Employee = new SelectList(d.GetActiveEmployeeListforLink(), "Value", "Text");
        //            linkTo.HrmsLinkSuperior.Update_By = "1";
        //            linkTo.HrmsLinkSuperior.Update_Date = DateTime.Now;
        //            db.Entry(linkTo.HrmsLinkSuperior).State = EntityState.Modified;

        //            db.SaveChanges();
        //            Session["success_div"] = "true";
        //            Session["success_msg"] = "Link To Employee Info Updated Successfully.";
        //            return RedirectToAction("AddLinkToEmployee", "EmployeeRecord", new { id = linkTo.HrmsLinkSuperior.EmployeeId });
        //        }
        //        catch (Exception ex)
        //        {
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Error Happened." + ex.Message;
        //            return RedirectToAction("EmployeeInformation", "EmployeeRecord",new { id = linkTo.HrmsLinkSuperior.EmployeeId });
        //        }
        //    }
        //    //Session["warning_div"] = "true";
        //    //Session["warning_msg"] = "Please Login to continue..!";
        //    //return RedirectToAction("Login", "Account");
        //}
        #endregion


        #region Temporary Employee Add

        /// <summary>
        /// Load form for Entering Employee basic Information. An employee have to provide his/her prettry basic 
        /// information.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddTemporaryEmployee()
        {
            //if (Session["ROLE"] != null)
            {
                ViewBag.CardTitleList = new SelectList(d.GetActiveCardTitleList(), "Value", "Text");
                ViewBag.Coutries = new SelectList(d.GetCountryList(), "Value", "Text");
                ViewBag.BUnits = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                ViewBag.Departments = new SelectList(d.GetDepartmentList(), "Value", "Text");
                ViewBag.Designations = new SelectList(d.GetDesignationList(), "Value", "Text");
                ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                ViewBag.DistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");
                return View();
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// This action helps to store basic information of an employee. It also stores image file
        /// </summary>
        /// <param name="vmEmployee"></param>
        /// <param name="imageFile"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddTemporaryEmployee(VMEmployee vmEmployee, HttpPostedFileBase Photo)
        {
            //if (Session["ROLE"] != null)

            var dt = "01/01/1900";
            {
                // HRMS_Employee v = new HRMS_Employee();

                //t1.Present_Status == 1 &&
                var vsdf = (from t1 in db.Employees
                            where t1.EmployeeIdentity == vmEmployee.EmployeeIdentity && t1.CardTitle == vmEmployee.CardTitle
                            select t1.EmployeeIdentity
                         ).SingleOrDefault();
                //  var fsdf = db.Employees.FirstOrDefault(s => s.EmployeeIdentity == vmEmployee.EmployeeIdentity && s.Present_Status == 1 && s.CardTitle == vmEmployee.CardTitle);
                if (vsdf == null)
                {
                    //try
                    {
                        ViewBag.CardTitleList = new SelectList(d.GetActiveCardTitleList(), "Value", "Text");
                        ViewBag.Coutries = new SelectList(d.GetCountryList(), "Value", "Text");
                        ViewBag.BUnits = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                        //ViewBag.Units = new SelectList(d.GetUnitList(), "Value", "Text");
                        ViewBag.Departments = new SelectList(d.GetDepartmentList(), "Value", "Text");
                        ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                        ViewBag.Designations = new SelectList(db.Designations, "Id", "Name");
                        ViewBag.DistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");

                        var pic = "nopicture.png";
                        if (Photo != null && Photo.ContentLength > 0)
                        {
                            string fileExtension = Path.GetExtension(Photo.FileName).ToLower();
                            if (fileExtension == ".jpg" || fileExtension == ".png" || fileExtension == ".gif")
                            {
                                //  var npic = Guid.NewGuid().ToString("N"); //imagefile.FileName;
                                var npic = vmEmployee.CardTitle + vmEmployee.EmployeeIdentity;
                                pic = npic + fileExtension;
                                string fileLocation = Server.MapPath("~/Assets/Images/EmpImages/") + pic;
                                if (System.IO.File.Exists(fileLocation))
                                {
                                    System.IO.File.Delete(fileLocation);
                                }
                                Photo.SaveAs(fileLocation);
                            }
                            else
                            {
                                Session["warning_div"] = "true";
                                Session["warning_msg"] = "Please upload only Image type file!";
                                return View(vmEmployee);
                            }
                        }
                        vmEmployee.Photo = pic;
                        HRMS_Employee employee = new HRMS_Employee()
                        {
                            CardTitle = vmEmployee.CardTitle,
                            CardNo = vmEmployee.CardNo,
                            EmployeeIdentity = vmEmployee.EmployeeIdentity,
                            Name = vmEmployee.Name,
                           // Name_BN = vmEmployee.Name_BN,
                            Father_Name = vmEmployee.Father_Name,
                            //Mother_Name = vmEmployee.Mother_Name,
                            DOB = vmEmployee.DOB,
                            //Gender = vmEmployee.Gender,
                            Designation_Id = vmEmployee.Designation_Id,
                            Country_Id = 1,
                            Business_Unit_Id = 3,
                            Unit_Id = 1,
                            Department_Id = vmEmployee.Department_Id,
                            Section_Id = vmEmployee.Section_Id,
                            LineId = 28,
                            //Blood_Group = vmEmployee.Blood_Group,
                            Joining_Date = /*(DateTime) */vmEmployee.Joining_Date,
                            JobPermanent_Date= vmEmployee.JobPermanent_Date,
                            //Religion = vmEmployee.Religion,
                            //Mobile_No = vmEmployee.Mobile_No,
                            //Alt_Mobile_No = vmEmployee.Alt_Mobile_No,
                            //Alt_Person_Name = vmEmployee.Alt_Person_Name,
                            //Alt_Person_Contact_No = vmEmployee.Alt_Person_Contact_No,
                            //Alt_Person_Address = vmEmployee.Alt_Person_Address,
                            //Emergency_Contact_No = vmEmployee.Emergency_Contact_No,
                            //Office_Contact = vmEmployee.Office_Contact,
                            //Land_Phone = vmEmployee.Land_Phone,
                            //Email = vmEmployee.Email_Id,
                            Employement_Type = vmEmployee.Employement_Type,
                            //NID_No = vmEmployee.NID_No,
                            //Passport_No = vmEmployee.Passport_No,
                            PresentDistrict_ID = 71,
                            PresentPoliceStation_ID = 574,
                            PresentPostOffice_ID = 1718,
                            PresentVillage_ID = 418,
                            PermanentDistrict_ID = 71,
                            PermanentPoliceStation_ID = 574,
                            PermanentPostOffice_ID = 1718,
                            PermanentVillage_ID = 418,
                            // Present_Address = vmEmployee.Present_Address,
                            //Permanent_Address = vmEmployee.Permanent_Address,
                            //Marital_Status = vmEmployee.Marital_Status,
                            Photo = pic,
                            Present_Status = 1,
                            Staff_Type = vmEmployee.Staff_Type,
                           // Overtime_Eligibility = vmEmployee.Overtime_Eligibility,
                            Grade = vmEmployee.Grade,
                            QuitDate = Convert.ToDateTime(dt),//default(DateTime),
                            Entry_By = "1",
                            Entry_Date = DateTime.Now
                        };
                        db.Employees.Add(employee);
                        db.SaveChanges();

                        //if (vmEmployee.Marital_Status == "Married")
                        //{
                        //    Session["success_div"] = "true";
                        //    Session["success_msg"] = "Employee Added Successfully.";
                        //    return RedirectToAction("AddSpouse", "EmployeeRecord");
                        //}
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Employee Added Successfully.";
                        return RedirectToAction("AddTemporaryEmployee", "EmployeeRecord");
                    }
                    //catch (Exception ex)
                    //{
                    //    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    //    {
                    //        Session["warning_div"] = "true";
                    //        Session["warning_msg"] = "Employee's info Can't Add, It is Already Exist!";
                    //        return RedirectToAction("AddEmployee", "EmployeeRecord");
                    //    }
                    //    Session["warning_div"] = "true";
                    //    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    //    return RedirectToAction("AddEmployee", "EmployeeRecord");
                    //}
                }
                Session["success_div"] = "true";
                Session["success_msg"] = "Employee Exist!.";
                return RedirectToAction("AddTemporaryEmployee", "EmployeeRecord");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        #endregion
        #region JSON Segment  

        /// <summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetVillageList(int postofficeid)
        {
            //if (Session["Role"] != null)
            {
                var list = db.Village.ToList().Where(u => u.PostOffice_ID == postofficeid);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            //return null;
        }

        /// <summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetPostOfficeList(int policestataionid)
        {
            //if (Session["Role"] != null)
            {
                var list = db.PostOffice.ToList().Where(u => u.PoliceStation_ID == policestataionid);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            //return null;
        }


        /// <summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetPoliceStationList(int districtid)
        {
            //if (Session["Role"] != null)
            {
                var list = db.PoliceStation.ToList().Where(u => u.District_ID == districtid);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            //return null;
        }



        /// <summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetPoliceStationList1(int districtid)
        {
            //if (Session["Role"] != null)
            {
                var ddd = new DropDownData();
                var list = ddd.GetPoliceStationlistforEmployeeEdit(districtid);
               // return Json(list, JsonRequestBehavior.AllowGet);
                var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
                return retval;
            }
            //return null;
        }


        /// <summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetPostOfficeList1(int id)
        {
            //if (Session["Role"] != null)
            {
                var ddd = new DropDownData();
                var list = ddd.GetPostOfficelistforEmployeeEdit(id);
                // return Json(list, JsonRequestBehavior.AllowGet);
                var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
                return retval;
            }
            //return null;
        }


        /// <summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetVillageList1(int id)
        {
            //if (Session["Role"] != null)
            {
                var ddd = new DropDownData();
                var list = ddd.GetVillagelistforEmployeeEdit(id);
                var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
                return retval;
            }
            //return null;
        }

        /// <summary>
        /// This json part will retrive related unit based on business unit.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetUnitList(int bUnitId)
        {
            //if (Session["Role"] != null)
            {
                var list = db.Units.ToList().Where(u => u.BusinessUnit_ID == bUnitId);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            //return null;
        }
        /// <summary>
        /// This json part will retrive related section based on Department.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetSectionList(int deptId)
        {
            //if (Session["Role"] != null)
            {
                var listSection = db.Sections.ToList().Where(u => u.Dept_ID == deptId);
                return Json(listSection, JsonRequestBehavior.AllowGet);
            }
            //return null;

        }

        //public JsonResult GetDepartmentdJson(string BusinessUnit)
        //{
        //    if (Session["Role"] != null)//&& Session["Role"].ToString() == "Admin")
        //    {
        //        var lstList = Department.Instance.GetDepartmentListWithAllforHeadOfficeEmployeeShiftAssign(BusinessUnit);
        //        var retval = Json(new { lstList }, JsonRequestBehavior.AllowGet);
        //        return retval;
        //    }
        //    return null;
        //}

        public JsonResult GetEmployeeByStatus(int Id)
        {
            var vData = (from o in db.Employees
                         where o.Present_Status==Id
                         select new
                         {
                             Name = o.CardTitle+"-"+o.EmployeeIdentity+"-"+o.Name,
                             ID = o.ID
                         }).OrderBy(a => a.ID).ToList();

            if (vData.Any())
            {
                return Json(vData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


        #region Employee Confirmation

        [HttpGet]
        public async Task<ActionResult> EmployeeConfirmationList()
        {
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            VMPromotions model = new VMPromotions();
             await Task.Run(() => model = model.EmployeeConfirmationWaitingList());
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> EmployeeConfirmationList(VMPromotions model)
        {
            try
            {
                if (model.EmployeeIds.Count > 0)
                {
                   await Task.Run(() => model.EmployeeConfirmationSave(model));
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Employee Earn Leave Payment Saved Successfully.";
                    return RedirectToAction("EmployeeConfirmationList", "EmployeeRecord");
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Employee Id Not Found!";
                return RedirectToAction("EmployeeConfirmationList", "EmployeeRecord");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                return RedirectToAction("EmployeeConfirmationList", "EmployeeRecord");
            }
        }

        

        #endregion


    }
}