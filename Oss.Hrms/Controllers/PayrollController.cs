﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using Oss.Hrms.Helper;
using Oss.Hrms.Models.ViewModels.Reports;
using Oss.Hrms.Models.ViewModels.Bonus;
using Oss.Hrms.Models.ViewModels.Salary;
using CrystalDecisions.Shared;


namespace Oss.Hrms.Controllers
{
    public class PayrollController : Controller
    {
        DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();
        private VM_HRMS_Payroll VM_HRMS_Payroll;
        VMEmployee VMEmployees;
        CurrentUser user;

        public PayrollController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            VMEmployees = new VMEmployee();
            this.user = sessionHandler.CurrentUser;
        }
        
        #region ManagePayroll
        public async Task<ActionResult> ManagePayroll()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetPayrollList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreatePayroll()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Monthly = new SelectList(d.GetMonthlyList(), "Value", "Text");
            //DateTime date = DateTime.Now;
            //int totalDay = DateTime.DaysInMonth(date.Year, date.Month-1);

            //DateTime FromDate = new DateTime(date.Year, date.Month-1, 1);
            //DateTime ToDate = new DateTime(date.Year, date.Month-1, totalDay);

            //VM_HRMS_Payroll Payroll = new VM_HRMS_Payroll();
            //Payroll.Monthly = 1;
            //Payroll.HasFastivalBonus = 0;
            //Payroll.FromDate = FromDate;
            //Payroll.ToDate = ToDate;

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreatePayroll(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Monthly = new SelectList(d.GetMonthlyList(), "Value", "Text");
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;

            await Task.Run(() => model.CreatePayroll1(model));
            if (string.IsNullOrEmpty(model.PayrollStatus))
            {
                await Task.Run(() => model.Save());

            }
            return RedirectToAction("ManagePayroll", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> ClosePayroll(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.ClosePayroll(id));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManagePayroll", "Payroll");
        }



        [HttpGet]
        public async Task<ActionResult> TestPayroll(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.TestPayroll(id));

            return RedirectToAction("ManagePayroll", "Payroll");
        }
        #endregion

        #region PayrollProcess
        [HttpGet]
        public async Task<ActionResult> ProcessPayroll(int id, bool AID)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            ViewBag.UnitList = new SelectList(d.GetUnitList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            model.AddCompliance = AID;
            await Task.Run(() => model.GetPayrollDetails(id));
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ProcessPayroll(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = user.ID.ToString();
            model.Update_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            model.Update_Date = DateTime.Now;
            if (model.AddCompliance)
            {
                await Task.Run(() => model.GetProcessAuditPayroll(model));
                await Task.Run(() => model.Save());
                return RedirectToAction("PayrollDetails", "Payroll", new { id = model.PayRollID, AID = model.AddCompliance });
            }
            else
            {
                //await Task.Run(() => model.GetProcessPayroll(model));
                await Task.Run(() => model.GetProcessRegularPayroll(model));
                await Task.Run(() => model.Save());
                return RedirectToAction("PayrollDetails", "Payroll", new { id = model.PayRollID, AID = model.AddCompliance });
            }
        }
        
        public ActionResult PreviousPayrollView()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Month = new SelectList(d.GetOpenPayrollList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult PreviousPayrollView(VM_HRMS_Payroll VM_HRMS_Payroll)
        {
            string id = VM_HRMS_Payroll.Month.ToString();
            try
            {
                return RedirectToAction("PayrollDetails", new { ID = id });
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public async Task<ActionResult> PayrollDetails(int id, bool AID)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.AddCompliance = AID;
            if (VM_HRMS_Payroll.AddCompliance)
            {
                await Task.Run(() => VM_HRMS_Payroll.AuditPayrollList(id));
            }
            else
            {
                await Task.Run(() => VM_HRMS_Payroll.GeneralPayrollList(id));
            }
            
            return View(VM_HRMS_Payroll);
        }

        [HttpGet]
        public async Task<ActionResult> VmPayrollDetails(string id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int PayrollID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("X")), out PayrollID);
            int EmployeeID = 0;
            id = id.Replace(PayrollID.ToString() + "X", "");
            int.TryParse(id, out EmployeeID);

            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.PayRollID = PayrollID;
            await Task.Run(() => VM_HRMS_Payroll.ListVmPayrollDetails(PayrollID, EmployeeID));
            
            return View(VM_HRMS_Payroll);
        }

        [HttpGet]
        public async Task<ActionResult> VmPayrollEdit(string id, bool AID)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int PayrollID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("X")), out PayrollID);
            int EmployeeID = 0;
            id = id.Replace(PayrollID.ToString() + "X", "");
            int.TryParse(id, out EmployeeID);

            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.EmployeeID = EmployeeID;
            VM_HRMS_Payroll.PayRollID = PayrollID;
            VM_HRMS_Payroll.Update_By = user.ID.ToString();
            VM_HRMS_Payroll.Update_Date = DateTime.Today;
            VM_HRMS_Payroll.AddCompliance = AID;
            if (VM_HRMS_Payroll.AddCompliance)
            {
                await Task.Run(() => VM_HRMS_Payroll.GetUpdateAuditEmployeePayroll(VM_HRMS_Payroll));
            }
            else
            {
                await Task.Run(() => VM_HRMS_Payroll.GetUpdateEmployeePayroll(VM_HRMS_Payroll));
            }
            
            return RedirectToAction("PayrollDetails", "Payroll", new { id = VM_HRMS_Payroll.PayRollID, AID = VM_HRMS_Payroll.AddCompliance });
        }
        #endregion

        #region Payslip
        [HttpGet]
        public ActionResult PaySlip()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.PayrollList = new SelectList(d.GetPayrollList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult PaySlip(VM_HRMS_Payroll model)
        {
            DtPayroll ds = new DtPayroll();

            ReportClass cr = new ReportClass();
            if (model.SectionID > 0)
            {
                ds = model.LoadPaySlip(model);
            }
            else
            {
                ds = model.LoadPaySlipAll(model);
            }
            cr.FileName = Server.MapPath("~/Views/Reports/PaySlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "PaySlip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        
        public ActionResult GeneratePaySlip(VM_HRMS_Payroll model)
        {
            DtPayroll ds = new DtPayroll();
            ReportClass cr = new ReportClass();
            if (model.SectionID > 0)
            {
                ds = model.LoadPaySlip(model);
            }
            else
            {
                ds = model.LoadPaySlipAll(model);
            }
            cr.FileName = Server.MapPath("~/Views/Reports/PaySlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "PaySlip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        public ActionResult PaySlipByEmployeeID(string id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int PayrollID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("X")), out PayrollID);
            int EmployeeID = 0;
            id = id.Replace(PayrollID.ToString() + "X", "");
            int.TryParse(id, out EmployeeID);

            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.EmployeeID = EmployeeID;
            VM_HRMS_Payroll.ID = PayrollID;
            
            DtPayroll ds = new DtPayroll();
            ds = VM_HRMS_Payroll.LoadPaySlip(VM_HRMS_Payroll);
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/PaySlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "PaySlip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        #endregion
        
        #region ManageSalary

        [HttpGet]
        public async Task<ActionResult> ManageSalary()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetSalaryList());
            return View(model);
        }
        
        [HttpGet]
        public ActionResult CreateSalary()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetSalaryCreateEmployeeList(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            model.OTAllow = null;
            model.Default = true;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateSalary(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            
            await Task.Run(() => model.GetEmployeeInfo(model.EmployeeID));
            await Task.Run(() => model.GetCreateSalary(model));
            await Task.Run(() => model.Save());
            
            return RedirectToAction("ManageSalary", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateSalary(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            await Task.Run(() => model.GetEmployeeGrossSalary(Id));
            //await Task.Run(() => model.GetCompliance(Id));
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateSalary(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = user.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateSalary(model));
            //await Task.Run(() => model.Save());
            
            return RedirectToAction("ManageSalary", "Payroll");
        }
        
        public ActionResult DeleteSalary(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.GetEmployeeSalaryDelete(Id);

            return RedirectToAction("ManageSalary", "Payroll");
        }

        #endregion
        
        #region AuditManageSalary

        [HttpGet]
        public async Task<ActionResult> ManageASalary()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetAuditSalaryList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateASalary()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetAuditSalaryCreateEmployeeList(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            model.OTAllow = null;
            model.Default = true;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateASalary(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;

            await Task.Run(() => model.GetEmployeeInfo(model.EmployeeID));
            await Task.Run(() => model.GetCreateASalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageASalary", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateASalary(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            //await Task.Run(() => model.GetEmployeeGrossSalary(Id));
            await Task.Run(() => model.GetAuditGrossSalary(Id));
        
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateASalary(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = user.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateASalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageASalary", "Payroll");
        }

        public async Task<ActionResult> ManageAPayroll()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetPayrollList());
            return View(model);
        }

        #endregion
        
        #region StaffNightAndHolydayBill
        public async Task<ActionResult> ManageBill()
        {
            if (user != null)
            {
                VM_HRMS_Payroll model = new VM_HRMS_Payroll();
                await Task.Run(() => model.GetUnassignBill());
                return View(model);
            }
            return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> ManageAssignedBill()
        {
            if (user != null)
            {
                VM_HRMS_Payroll model = new VM_HRMS_Payroll();
                await Task.Run(() => model.GetBillList());
                return View(model);
            }
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult CreateBill()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.DesignationList = new SelectList(d.GetDesignationList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateBill(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            
            await Task.Run(() => model.GetCreateNightBill(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBill", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateBill(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            await Task.Run(() => model.GetEmployeeBill(Id));

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateBill(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = user.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateNightBill(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBill", "Payroll");
        }

        public ActionResult DeleteBill(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.GetDeleteNightBill(Id);

            return RedirectToAction("ManageBill", "Payroll");
        }
        #endregion

        #region ManageBonus
        [HttpGet]
        public async Task<ActionResult> ManageBonus()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VMBonus model = new VMBonus();
            await Task.Run(() => model.GetBonusList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateBonus()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //ViewBag.DesignationList = new SelectList(d.GetNightBillCreateDesignationList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateBonus(VMBonus model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            await Task.Run(() => model.GetCreateBonus(model));

            return RedirectToAction("ManageBonus", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateBonus(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            await Task.Run(() => model.GetEmployeeGrossSalary(Id));

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateBonus(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = user.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateSalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBonus", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> ProcessBonus(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            ViewBag.UnitList = new SelectList(d.GetUnitList(), "Value", "Text");
            //ViewBag.PayrollList = new SelectList(d.GetOpenPayrollList(), "Value", "Text");
            //ViewBag.Fastival = new SelectList(d.GetFastivalList(), "Value", "Text");
            VMBonus model = new VMBonus();
            await Task.Run(() => model.GetBonusInfo(id));
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ProcessBonus(VMBonus model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = user.ID.ToString();
            model.Update_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetProcessBonus(model));
            //await Task.Run(() => model.Save());
            return RedirectToAction("BonusDetails", "Payroll", new { id = model.BonusId });
        }

        [HttpGet]
        public async Task<ActionResult> BonusDetails(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VMBonus model = new VMBonus();
            await Task.Run(() => model.GetEmployeeBonus(id));
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> CloseBonus(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VMBonus model = new VMBonus();
            await Task.Run(() => model.CloseBonus(id));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBonus", "Payroll");
        }

        #endregion
        
        #region SalaryIncrement
        [HttpGet]
        public ActionResult SalaryIncrement()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            VMSalaryIncrement model = new VMSalaryIncrement();
            model.DataList = new List<VMSalaryIncrement>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SalaryIncrement(VMSalaryIncrement model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            model.DataList = new List<VMSalaryIncrement>();
            await Task.Run(() => model.GetIncrementListByTitleWise(model.CardTitleId));
            return View(model);
        }
        
        [HttpPost]
        public async Task<ActionResult> CreateIncrementSalary(VMSalaryIncrement model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            
            await Task.Run(() => model.CreateYearlyIncrement(model));
            await Task.Run(() => model.Save());
            return RedirectToAction("SalaryIncrement");
        }

        [HttpGet]
        public ActionResult IncrementList()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            VMSalaryIncrement model = new VMSalaryIncrement();
            model.DataList = new List<VMSalaryIncrement>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> IncrementList(VMSalaryIncrement model)
        {

            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            model.DataList = new List<VMSalaryIncrement>();
            await Task.Run(() => model.GetIncrementedListByTitleWise(model.CardTitleId));
            return View(model);
        }

        #endregion

        #region Report

        [HttpGet]
        public ActionResult GetSalarySheet(int id,bool AID)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.StatusList = new SelectList(d.GetEmployeePayrollStatus(), "Value", "Text");
            VM_HRMS_Payroll Payroll = new VM_HRMS_Payroll();
            Payroll.PayRollID = id;
            Payroll.AddCompliance = AID;
            return View(Payroll);
        }

        [HttpPost]
        public ActionResult GetSalarySheet(VM_HRMS_Payroll model)
        {
            DsPayrollReport ds = new DsPayrollReport();
            VMReportPayroll vmmodel = new VMReportPayroll();
            ReportClass cr = new ReportClass();
            if (model.AddCompliance)
            {
                if (model.PayrollStatusId == 1)
                {
                    ds = vmmodel.GetAuditSheetForResign(model.SectionID.Value, model.PayRollID);
                }
                else if (model.PayrollStatusId == 2)
                {
                    ds = vmmodel.GetAuditSalarySheetForLefty(model.SectionID.Value, model.PayRollID);
                }
                else
                {
                    ds = vmmodel.GetAuditSalarySheet(model.SectionID.Value, model.PayRollID);
                }
            }
            else
            {
                if (model.PayrollStatusId == 1)
                {
                    ds = vmmodel.GetSalarySheetForResign(model.SectionID.Value, model.PayRollID);
                }
                else if (model.PayrollStatusId == 2)
                {
                    ds = vmmodel.GetSalarySheetForLefty(model.SectionID.Value, model.PayRollID);
                }
                else
                {
                    ds = vmmodel.GetSalarySheet(model.SectionID.Value, model.PayRollID);
                }
            }
            
            
            cr.FileName = Server.MapPath("~/Views/Reports/CrpSalarySheet.rpt");
            cr.SummaryInfo.ReportTitle = "SalarySheet";
            cr.Load();
            cr.SetDataSource(ds);
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        [HttpGet]
        public ActionResult Advance(DateTime? FDate,int? SID)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            VMSalaryIncrement advance = new VMSalaryIncrement();
            if (FDate != null && SID!=null)
            {
                advance.FromDate = FDate.Value;
                advance.SectionID = SID.Value;
                advance.DataList = new List<VMSalaryIncrement>();
                advance.GetEmployeeForAdvancePay(advance.FromDate, (int)advance.SectionID);
            }
            advance.FromDate = DateTime.Today;
            return View(advance);
        }

        [HttpPost]
        public async Task<ActionResult> Advance(VMSalaryIncrement model)
        {
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            model.DataList = new List<VMSalaryIncrement>();
            await Task.Run(() => model.GetEmployeeForAdvancePay(model.FromDate,(int)model.SectionID));
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> AdvancePayment(VMSalaryIncrement model)
        {
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            await Task.Run(() => model.CreateAdvancePay(model));
            return RedirectToAction("Advance",new { FDate = model.FromDate,SID=model.SectionID });
        }
        #endregion
    }
}